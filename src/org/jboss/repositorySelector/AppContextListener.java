package org.jboss.repositorySelector;

import java.util.Timer;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import com.musicnotes.apis.util.ScheduleMail;
import com.musicnotes.apis.util.UsersAutoMailAlert;

public class AppContextListener implements ServletContextListener
{

	public void contextDestroyed(ServletContextEvent contextEvent)
	{
		
	}

	public void contextInitialized(ServletContextEvent contextEvent)
	{
		Timer timer=null;
		try
		{
			timer = new Timer();
			Timer timer1 = new Timer();
			System.out.println("contextEvent.getServletContext() : "+contextEvent.getServletContext());
			AppRepositorySelector.init(contextEvent.getServletContext());
			long times=300*1000;
			timer.schedule(new ScheduleMail(),times,times);
			
			
			//long trailtime=86400*1000;
			//timer1.schedule(new UsersAutoMailAlert(),times,trailtime);
		}
		catch (Exception ex)
		{
			System.err.println(ex);
		}
	}
}