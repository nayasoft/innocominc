package com.musicnotes.apis.util;

import java.util.Date;
import java.util.List;

public class EventDao {
	
	   private String eventName;
	   private String startDate;//: \"10/21/2013 10: 30\",
	   private String eventMembers;//\": \"\",
	   private String eventGroups;//\": \"\",
	   private Integer eventSharedAllContact;//\": 0,
	   private String eventSharedAllContactMembers;//\": \"\",
	   private String status; //\": \"I\",
	   private String ownerEventStatus; // \": \"I\",
	   private String endDate;//\": \"10/21/201311: 30\",
	   private Integer eventId;//\": \"1\",
	   private String description;//\": \"test\",
	   private String location; //\": \"sdfsadfsaf\"
	
	   @Override
	   public String toString(){
		   
		   return "{\"eventName\":\""+getEventName()+"\",\"startDate\":\""+getStartDate()+"\",\"eventMembers\":\""+getEventMembers()+"\",\"eventGroups\":\""+getEventMembers()+"\",\"eventSharedAllContact\":"+getEventSharedAllContact()+",\"eventSharedAllContactMembers\":\""+getEventSharedAllContactMembers()+"\",\"status\":\""+getStatus()+"\",\"ownerEventStatus\":\""+getOwnerEventStatus()+"\",\"vote\":\"\",\"links\":\"\",\"endDate\":\""+getEndDate()+"\",\"eventId\":\""+getEventId().toString()+"\",\"dueDate\":\"\",\"dueTime\":\"\",\"access\":\"private\",\"attachFilePath\":\"\",\"description\":\""+getDescription()+"\",\"comments\":\"\",\"location\":\""+getLocation()+"\"}";
		   
	   }
	   
	    public String getEventName() {
			return eventName;
	    }
		public void setEventName(String eventName) {
			this.eventName = eventName;
		}
		public String getStartDate() {
			return startDate;
		}
		public void setStartDate(String startDate) {
			this.startDate = startDate;
		}
		public String getEventMembers() {
			return eventMembers;
		}
		public void setEventMembers(String eventMembers) {
			this.eventMembers = eventMembers;
		}
		public String getEventGroups() {
			return eventGroups;
		}
		public void setEventGroups(String eventGroups) {
			this.eventGroups = eventGroups;
		}
		public Integer getEventSharedAllContact() {
			return eventSharedAllContact;
		}
		public void setEventSharedAllContact(Integer eventSharedAllContact) {
			this.eventSharedAllContact = eventSharedAllContact;
		}
		public String getEventSharedAllContactMembers() {
			return eventSharedAllContactMembers;
		}
		public void setEventSharedAllContactMembers(String eventSharedAllContactMembers) {
			this.eventSharedAllContactMembers = eventSharedAllContactMembers;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getOwnerEventStatus() {
			return ownerEventStatus;
		}
		public void setOwnerEventStatus(String ownerEventStatus) {
			this.ownerEventStatus = ownerEventStatus;
		}
		public String getEndDate() {
			return endDate;
		}
		public void setEndDate(String endDate) {
			this.endDate = endDate;
		}
		public Integer getEventId() {
			return eventId;
		}
		public void setEventId(Integer eventId) {
			this.eventId = eventId;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getLocation() {
			return location;
		}
		public void setLocation(String location) {
			this.location = location;
		}
}
