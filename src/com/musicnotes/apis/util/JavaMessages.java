package com.musicnotes.apis.util;

public class JavaMessages
{
	public class Spring
	{
		public final static String MUSICNOTEUSER = "musicNoteUser";
		public final static String MNLOGIN = "mnLogin";
		public final static String MNUSER="mnUsers";
		public final static String MNINVOICE="mnInvoice";
		public final static String mnVideo="mnUpload";
		public final static String MNGROUP="mnGroup";
		public final static String MNLIST="mnList";
		public final static String MNFRIEND="mnFriend";
		public final static String MNLOG="mnLog";
		public final static String MNGROUPDETAILS="mnGroupDetails";
		public final static String MNEVENT="mnEvents";
		public final static String MNFILTERS="mnFilters";

		
		// email
		public final static String SENDMAIL = "sendEmail";
		public final static String READMAIL = "readEmail";
		public final static String DELETEMAIL ="deleteEmail";
		
		public final static String vcFileUpload="vcFileUpload";
		public final static String vcAudioUpload="vcAudioUpload";
		
		public final static String amazonFileUpload="amazonFileUpload";
		public final static String amazonAudioUpload="amazonAudioUpload";
		public final static String amazonFileDelete="amazonFileDelete";
		
		//admin
		public final static String MNPAYMENT = "mnPayment";
		public final static String MNADMIN="mnAdmin";
	}

	public class Mongo
	{
		public final static String MNUSERS = "Mn_Users";
		public final static String MNGROUP = "Mn_Group";
		public final static String MNGROUPDETAILS = "Mn_GroupDetails";
		public final static String MNINVOICE = "Mn_Invoice";
		public final static String MnVideoFile = "Mn_VideoFile";
		public final static String MnFiles = "Mn_Files";
		public final static String MNLIST = "Mn_List";
		public final static String MNFRIEND = "Mn_Friends";
		public final static String MNNOTESSHARINGDETAILS = "Mn_NotesSharingDetails";
		public final static String MNNOTEDETAILS = "Mn_NoteDetails";
		public final static String MNCOMMETS="Mn_Comments";
		public final static String MNREMAINDERS="Mn_Remainders";
		public final static String MNLOG="Mn_Log";
		public final static String MNVOTEVIEWCOUNT="Mn_Vote_View_Count";
		public final static String MNEVENTS="Mn_Events";
		public final static String MNESUBVENTS="Mn_Sub_Events";
		public final static String MNEVENTSSHARINGDETAILS="Mn_EventsSharingDetails";
		public final static String MNTAGS = "Mn_Tags";
		public final static String MNTAGDETAILS = "Mn_TagDetails";
		public final static String MNNOTEDUEDATEDETAILS = "Mn_NoteDueDateDetails";
		public final static String MNADMINVOTES = "Mn_AdminVotes";
		public final static String MNPUBLICSHAREDNOTECOPY = "Mn_PublicSharedNoteCopyDetails";
		public final static String MNATTACHMENTDETAILS = "Mn_AttachmentDetails";
		public final static String MNNONUSERSHARINGDETAILS = "Mn_NonUserSharingDetails";
		public final static String MNUSERFEEDBACK = "Mn_UserFeedback";
		public final static String MNUSERLOGDETAIL = "Mn_UserLogDetail";
		public final static String MNLoginData="Mn_LoginData";
		public final static String MNSubscription="Mn_Subscription";
		public final static String MNMailConfiguration="Mn_MailConfiguration";
		public final static String MNInvitedUsers="Mn_InvitedUsers";
		public final static String MNSubscriptionMulti="Mn_SubscriptionMulti";
		public final static String MNAdminConfiguration="Mn_MnAdminConfiguration";
		public final static String MNSecurityQuestion="Mn_SecurityQuestion";
		public final static String MNUsersToken="Mn_MnUsersToken";
		public final static String MNCOMPLAINTS="Mn_MnComplaints";
		public final static String MNMAILNOTIFICATIONLIST="Mn_MnMailNotificationList";
		public final static String MNCOMPLAINTACTION="Mn_MnComplaintAction";
		public final static String MNADMINSECURITYQUESTION="Mn_AdminSecurityQuestion";
		public final static String MNADMINCOMPLAINTMAILTEMPLATE="Mn_MnAdminComplaintMailTemplate";
		public final static String MNBLOCKEDUSERS="Mn_MnBlockedUsers";
		public final static String MNADMINFILES="Mn_MnAdminFiles";
	}
	
	//public static final  String portNo ="25"; 
	public static final  String portNo ="587"; 
	public static final  String host = "email-smtp.us-west-2.amazonaws.com";
	public static final  String userName ="AKIAJK3DDSMNVBOUB6NQ";
	public static final  String password ="AmmwM+00ip8foTDJBjhv6d74Jv6Nguu1Rd8SRwy6MgAr";
	public static final  String fromAddress="support@musicnoteapp.com";
	public static final  String testRecipient="rveeraprathaban@nayasoft.in";
	
	public static final int emailCount = 200;
	public static final String EXCEPTION_ERROR = "Error";
	public static final String BUCKET_NAME="Musicnote";
	//public static final String BUCKET_NAME="MusicnoteTest";
	
	public static final String requestStatus="R";
	public static final String acceptStatus="A";
	public static final String declineStatus="D";
	
	//For authentication filter
	public static final String requestURI="mnApi";
	public static final String requestLoginURI="/login/userlogin";
	public static final String requestUserURI="/user/newToken";
	public static final String requestTokenURI="/login/toGen";
	
	//public static final String downloadUrl="http://musicnoteapp.com/UploadMusicFiles/";  //Godaddy server
	public static final String downloadUrl="https://s3.amazonaws.com/";   //Test server
	public static final String zipFileName="downloads.zip";
	//public static final String uploadPathForGodaddy="/usr/share/jboss-5.1.0.GA/server/default/deploy/UploadMusicFiles.war/"; //Godaddy server
	//public static final String uploadPathForGodaddy="/export/Apps/JBoss/jboss-5.1.0.GA/server/default/deploy/UploadMusicFiles.war/"; //Test server
	public static final String uploadPathForGodaddy="/usr/share/Musicnote/"; //Test server

	
	public static final String Q1="What was your first pet's name?-Q1";
	public static final String Q2="Who was your first school teacher?-Q2";
	public static final String Q3="What was your childhood nickname?-Q3";
	public static final String Q4="What is the name of your favorite childhood friend?-Q4";
	public static final String Q5="Who was your childhood hero?-Q5";
	
	
	public static final String noteTemplate="The Note you posted in crowd \"Note Name\" is violating the \"Music Note terms and conditions\". As the rule we are taking action to delete your note.";
	public static final String descriptionTemplate="(A) Description  posted by Note Author: The description posted by \"Author Name\" in your note \"Note Name\" is violating the \"Music Note terms and conditions\". As the rule we are taking action to delete the description from your note. (B)The description posted by Another User:The description posted by \"User Name\" on the note \"Note Name\" is violating the \"Music Note terms and conditions\". As the rule we are taking action to delete the description on your note.";
	public static final String attachmentTemplate="(A) Attachment  posted by Note Author: The Attachment  posted by \"Author Name\" in your note \"Note Name\" is violating the \"Music Note terms and conditions\". As the rule we are taking action to delete the attachment  from your note. (B)The attachment  posted by Another User:The attachment posted by \"User Name\" on the note \"Note Name\" is violating the \"Music Note terms and conditions\". As the rule we are taking action to delete the attachment on your note.";
	public static final String commentTemplate="(A)	Comment posted by Note Author: The comment posted by \"Author Name\" in your note \"Note Name\" is violating the \"Music Note terms and conditions\". As the rule we are taking action to delete the comment from your note. (B)Comment posted by Another User:The comment posted by \"User Name\" on the note \"Note Name\" is violating the \"Music Note terms and conditions\". As the rule we are taking action to delete the comment on your note.";
	
	
	
	
}
