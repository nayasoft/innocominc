package com.musicnotes.apis.util;

import java.util.Timer;
import java.util.TimerTask;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.musicnotes.apis.dao.interfaces.IEventDao;
import com.musicnotes.apis.interfaces.MnEventable;
import com.musicnotes.apis.interfaces.MnUserable;
import com.musicnotes.apis.resources.MnEvents;
import com.musicnotes.apis.resources.MnUser;


public class ScheduleMail extends TimerTask{
	
		Logger logger=Logger.getLogger(ScheduleMail.class);
		String recipients[],subject,message;
		Timer timer;
		HttpServletRequest request;
		MnEventable mnEventable;
		MnUserable usarable;

		BeanFactory beanFactory = null;
		
		public ScheduleMail() {
			super();
			
			try
			{
			beanFactory = new ClassPathXmlApplicationContext(new String[] {
					"\\resources\\spring\\spring.xml","\\resources\\spring\\mongo-config.xml"});	
			
			mnEventable = (MnEvents) beanFactory.getBean("mnEvents");
			
			}
			catch (Exception e) {
				System.out.println("Exception ScheduleMail class :"+ e);
			}
			
		}
		@Override
		public void run() {
			try {
				String status = mnEventable.getEventMailValues();
				
			} catch (Exception e) {
				System.out.println("exception in run method :"+e.getMessage());
			}
			
		}
		
		/*public static void main(String arg [])
		{
			ScheduleMail sch=new ScheduleMail();
			sch.call();
		}
		
		public void call()
		{
			beanFactory = new ClassPathXmlApplicationContext(new String[] {
					"\\resources\\spring\\spring.xml","\\resources\\spring\\mongo-config.xml"});	
			
			//mnEventable = (MnEvents) beanFactory.getBean("mnEvents");
			usarable = (MnUserable) beanFactory.getBean("mnUsers");
			String status=usarable.setResetPass();
			
		}
	*/

}
