package com.musicnotes.apis.controllers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.musicnotes.apis.resources.MnPayment;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.SpringBeans;

@Controller
@RequestMapping("/payment")

public class PaymentController extends BaseController{
	
	MnPayment mnpayment;
	Logger logger = Logger.getLogger(PaymentController.class);
	
	
	@RequestMapping(value = "/makePayment", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String makePayment(@RequestBody String param, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled()){logger.info("makePayment method called");}
			mnpayment = (MnPayment) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNPAYMENT);
			param = decodeJsonString(param);
			param = removeUnWantedChar(param);
			status=mnpayment.makePayment(param);
			if(status!=null){
			if(logger.isInfoEnabled()){logger.info("Payment Made Successfully");}
			}
		}
		catch (Exception e)
		{
			logger.error("Error in making payment"+e);
		}
		return status;
	}
	
	
	@RequestMapping(value = "/renewalPayment", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String renewalPayment(@RequestBody String param, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled()){logger.info("renewalPayment method called");}
			mnpayment = (MnPayment) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNPAYMENT);
			param = decodeJsonString(param);
			param = removeUnWantedChar(param);
			status=mnpayment.renewalPayment(param);
			if(status!=null){
			if(logger.isInfoEnabled()){logger.info("Successfully renewed payment");}	
			}
		}
		catch (Exception e)
		{
			logger.error("Exception in renewing payment"+e);
		}
		return status;
	}
	
	
	@RequestMapping(value = "/paymentFromPaypal", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String updatePayNo(HttpServletRequest request)
	{
		String status = "";
		String trnType="",subscr_id="";
		try
		{
			// Read all posted request parameters
			String requestParams = this.getAllRequestParams(request);
			if(logger.isInfoEnabled()){logger.info("updatePayNo method called");}
	        
	        // Prepare 'notify-validate' command with exactly the same parameters
            Enumeration en = request.getParameterNames();
            StringBuilder cmd = new StringBuilder("cmd=_notify-validate");
            String paramName;
            String paramValue;
            while (en.hasMoreElements()) {
                paramName = (String) en.nextElement();
                paramValue = request.getParameter(paramName);
                cmd.append("&").append(paramName).append("=")
                        .append(URLEncoder.encode(paramValue, request.getParameter("charset")));
            }

           // Post above command to Paypal IPN URL 
           // URL u = new URL(this.getIpnConfig().getIpnUrl());
            URL u = new URL("https://www.sandbox.paypal.com/cgi-bin/webscr");
            HttpsURLConnection uc = (HttpsURLConnection) u.openConnection();
            uc.setDoOutput(true);
            uc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            uc.setRequestProperty("Host", "www.paypal.com");
            PrintWriter pw = new PrintWriter(uc.getOutputStream());
            pw.println(cmd.toString());
            pw.close();
            
          // Read response from Paypal
            BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            String res = in.readLine();
            in.close();
            
            trnType=request.getParameter("txn_type");
            
            subscr_id=request.getParameter("subscr_id");
            
            String custom=request.getParameter("custom");
            String txnNo=request.getParameter("txn_id");
            String amount=request.getParameter("mc_gross");
            String domain="paypal-"+request.getParameter("payer_id");

          // Validate captured Paypal IPN Information
            if (res.equals("VERIFIED")) {
            	
            	mnpayment = (MnPayment) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNPAYMENT);
    			status=mnpayment.makePaymentFromRest(custom,txnNo,trnType,subscr_id,amount,domain);
    			if(logger.isInfoEnabled()){logger.info("Updated payment number for the corresponding user");}
            }
           
		}
		catch (Exception e)
		{
			logger.error("Exception in upadating payment number");
		}
		return status;
	}
	
	 private String getAllRequestParams(HttpServletRequest request)
	    {
		 if(logger.isInfoEnabled()){logger.info("getAllRequestparams method called");}
	        Map map = request.getParameterMap();
	        StringBuilder sb = new StringBuilder("\nREQUEST PARAMETERS\n");
	    
		 try{
		   
	        for (Iterator it = map.keySet().iterator(); it.hasNext();)
	        {
	            String pn = (String)it.next();
	            sb.append(pn).append("\n");
	            String[] pvs = (String[]) map.get(pn);
	            for (int i = 0; i < pvs.length; i++) {
	                String pv = pvs[i];
	                sb.append("\t").append(pv).append("\n");
	            }
	        }
	        if(logger.isInfoEnabled()){logger.info("Successfully got all requested params");}
		 }catch(Exception e){
			 logger.error("Exception during fetching all requested params"+e);
		 }
	        return sb.toString();
	    }
	
	@RequestMapping(value = "/getOfferMessage", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getOfferMessageForPayment(@RequestBody String param, HttpServletRequest request)
	{
		String status = "";
		try
		{
			if(logger.isInfoEnabled()){logger.info("getOfferMessageForPayment method called");}
			mnpayment = (MnPayment) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNPAYMENT);
			param = decodeJsonString(param);
			param = removeUnWantedChar(param);
			status=mnpayment.getOfferMessageForPayment(param);
			if(logger.isInfoEnabled()){logger.info("successfully got offer message");}
			
		}
		catch (Exception e)
		{
			logger.error("Exception while getting offer message"+e);
		}
		return status;
	}

}
