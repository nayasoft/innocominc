package com.musicnotes.apis.controllers;

import java.io.File;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import upload.VcAudioUpload;


import amazonupload.AmazonAudioUpload;
import amazonupload.AmazonFileUpload;
import amazonupload.AmazonUploadable;

import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.resources.MnUpload;
import com.musicnotes.apis.resources.MnUser;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.SpringBeans;

/**
 * This class acts as Controller for uploading files/audio/video, Which consumes request & produce response 
 * @author - prajesh
 * @since - 09/05/2013
 **/

@Controller
@RequestMapping("/Upload")
public class UploadController
{
	Logger logger = Logger.getLogger(UploadController.class);
	
	/**
	 * Method for video Upload
	 * 
	 * @param userId
	 *            userId
	 * 
	 * @param request
	 *            HttpServletRequest
	 *  
	 */
	
	@RequestMapping(value = "/videoFiles/{userId}/{fileName}/{userTokens}", method = RequestMethod.POST)
	@ResponseBody
	public String videoFilesUpload(@PathVariable("userId") String userId,@PathVariable("fileName") String fileName,@PathVariable("userTokens") String userTokens, HttpServletRequest request,HttpServletResponse response)
	{
		if(logger.isInfoEnabled())
			logger.info("videoFilesUpload method called: "+userId);
		String limitedStatus=null;
		try
		{
			logger.info("request.getContentType()"+request.getContentLength());
			int uploadSize = request.getContentLength();
		    double uploadKilobytes = (uploadSize / 1024);
			double uploadMegabytes = (uploadKilobytes / 1024);
			com.musicnotes.apis.interfaces.MnUploadable videoFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);

			MnUsers mnUser=videoFile.limiteSize(userId);
			if(mnUser.getLimitedSize()!=null && mnUser.getUploadedSize()!=null && (mnUser.getLimitedSize()>(mnUser.getUploadedSize()+uploadMegabytes)|| mnUser.getLimitedSize()==0))
			{
			request.setAttribute("userId", userId);
			if(fileName!=null && fileName.contains(".mov"))
			{
				fileName=fileName+".mov";
			}
			request.setAttribute("fileName", fileName);
			AmazonUploadable fileUpload = (AmazonUploadable) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.amazonFileUpload);
			String filePath = fileUpload.amazonUpload(request,JavaMessages.BUCKET_NAME);
            String name=filePath.substring(filePath.lastIndexOf("/")+1, filePath.length());
        	String fullName=name;
			String type = "";
			int mid = name.lastIndexOf(".");
			name = name.substring(0, mid);
			type = fullName.substring(mid + 1, fullName.length());
			String status="A";
			 int size;
			    URL url = new URL(JavaMessages.downloadUrl+filePath);
			    URLConnection conn = url.openConnection();
			    size = conn.getContentLength();
			    double kilobytes = (size / 1024);
			    System.out.println(kilobytes);
				double megabytes = (kilobytes / 1024);
				  System.out.println(megabytes);
			    if (size < 0)
			      System.out.println("Could not determine file size.");
			    else
			      System.out.println(size);
			    conn.getInputStream().close();
			videoFile.saveVideoFileName(fullName, userId, type, status,filePath,megabytes);
			if(logger.isInfoEnabled())
				logger.info("Return reponse for videoFilesUpload method called: ");
			/*
			 * This is to inform the browser that there is no problem after uploading and returns the Status 200(OK)
			 * Handled for IOS
			 */
			response.setStatus(200);
			limitedStatus="{\"status\" : \"success\"}";
			}
			else
			{
				limitedStatus="{\"status\" : \"The attachement size exceeds the allowable limit\"}";
			}
		}
		catch (Exception e)
		{
			logger.error(" Exception while uploading video : " + e);
		}
		return limitedStatus;
	}

	@RequestMapping(value = "/video/{userId}/{fileName}", method = RequestMethod.POST)
	@ResponseBody
	public String videoUpload(@PathVariable("userId") String userId,@PathVariable("fileName") String fileName, HttpServletRequest request,HttpServletResponse response)
	{
		if(logger.isInfoEnabled())
			logger.info("videoUpload method called: "+userId);
		String limitedStatus=null;
		try
		{
			logger.info("request.getContentType()"+request.getContentLength());
			int uploadSize = request.getContentLength();
		    double uploadKilobytes = (uploadSize / 1024);
			double uploadMegabytes = (uploadKilobytes / 1024);
			com.musicnotes.apis.interfaces.MnUploadable videoFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
			MnUsers mnUser=videoFile.limiteSize(userId);
			if(mnUser.getLimitedSize()!=null && mnUser.getUploadedSize()!=null   && (mnUser.getLimitedSize()>(mnUser.getUploadedSize()+uploadMegabytes)|| mnUser.getLimitedSize()==0))
			{
			request.setAttribute("userId", userId);
			request.setAttribute("fileName", fileName);
			AmazonUploadable fileUpload = (AmazonUploadable) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.amazonFileUpload);
			String filePath = fileUpload.amazonUpload(request,JavaMessages.BUCKET_NAME);
			String name=filePath.substring(filePath.lastIndexOf("/")+1, filePath.length());
			String type = "";
			int mid = name.lastIndexOf(".");
			name = name.substring(0, mid);
			type = name.substring(mid + 1, name.length());
			String status="A";
			 int size;
			    URL url = new URL(JavaMessages.downloadUrl+filePath);
			    URLConnection conn = url.openConnection();
			    size = conn.getContentLength();
			    double kilobytes = (size / 1024);
			    System.out.println(kilobytes);
				double megabytes = (kilobytes / 1024);
				  System.out.println(megabytes);
			    if (size < 0)
			      System.out.println("Could not determine file size.");
			    else
			      System.out.println(size);
			    conn.getInputStream().close();
		    videoFile.saveOtherFileName(name, userId, type, status,filePath,megabytes);
			
			/*
			 * This is to inform the browser that there is no problem after uploading and returns the Status 200(OK)
			 * Handled for IOS
			 */
		    if(logger.isInfoEnabled())
				logger.info("Return reponse for videoUpload method called: ");
			response.setStatus(200);
			limitedStatus="{\"status\" : \"success\"}";
			}
			else
			{
				limitedStatus="{\"status\" : \"The attachement size exceeds the allowable limit\"}";
			}
		}
		catch (Exception e)
		{
			logger.error(" Exception while uploading video : " + e);
		}
		return limitedStatus;
	}

	@RequestMapping(value = "/audio/{userId}/{token}", method = RequestMethod.POST)
	@ResponseBody
	public String audioUpload(@PathVariable("userId") String userId,@PathVariable("token") String token, HttpServletRequest request,HttpServletResponse response)
	{
		if(logger.isInfoEnabled())
			logger.info(" audioUpload method called: "+userId+"  file Name");
		File f=null;
		String limitedStatus=null;
		try
		{
			logger.info("request.getContentType()"+request.getContentLength());
			int uploadSize = request.getContentLength();
		    double uploadKilobytes = (uploadSize / 1024);
			double uploadMegabytes = (uploadKilobytes / 1024);
			com.musicnotes.apis.interfaces.MnUploadable videoFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
			MnUsers mnUser=videoFile.limiteSize(userId);
			if(mnUser.getLimitedSize()!=null && mnUser.getUploadedSize()!=null   && (mnUser.getLimitedSize()>(mnUser.getUploadedSize()+uploadMegabytes)|| mnUser.getLimitedSize()==0))
			{
			request.setAttribute("userId", userId);
			AmazonUploadable fileUpload = (AmazonUploadable) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.amazonFileUpload);
			String filePath = fileUpload.amazonUpload(request,JavaMessages.BUCKET_NAME);
			String name=filePath.substring(filePath.lastIndexOf("/")+1, filePath.length());
			String type = "";
			int mid = name.lastIndexOf(".");
			type = name.substring(mid + 1, name.length());
			type= type.toLowerCase();
			String status="A";
			int size;
		    URL url = new URL(JavaMessages.downloadUrl+filePath);
		    URLConnection conn = url.openConnection();
		    size = conn.getContentLength();
		    double kilobytes = (size / 1024);
		    System.out.println(kilobytes);
			double megabytes = (kilobytes / 1024);
			  System.out.println(megabytes);
		    if (size < 0)
		      System.out.println("Could not determine file size.");
		    else
		      System.out.println(size);
		    conn.getInputStream().close();
			videoFile.saveVideoFileName(name, userId ,type ,status,filePath,megabytes);
			if(logger.isInfoEnabled())
				logger.info("Return response for fileUpload method called: ");
			/*
			 * This is to inform the browser that there is no problem after uploading and returns the Status 200(OK)
			 * Handled for IOS
			 */
			response.setStatus(200);
			
		
			/*
			request.setAttribute("userId", userId);
			request.setAttribute("fileName", fileName);
			VcAudioUpload audioUpload=new VcAudioUpload();
			logger.info("welcome");
			String filePath=audioUpload.writeFile(request, "audio", JavaMessages.uploadPathForGodaddy);
			//AmazonUploadable audioFilUpload = (AmazonUploadable) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.amazonAudioUpload);
			//String filePath=audioFilUpload.amazonUpload(request,JavaMessages.BUCKET_NAME);
			com.musicnotes.apis.interfaces.MnUploadable videoFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
			String name=filePath.substring(filePath.lastIndexOf("/")+1, filePath.length());
			String type = "";
			int mid = name.lastIndexOf(".");
			type = name.substring(mid + 1, name.length());
			String status="A";
			videoFile.saveVideoFileName(name, userId, type, status,filePath);
			f = new File("/Musicnote/"+userId+"/files/"+name);
			if(f.exists())
			{
				f.delete();
			}
			if(logger.isInfoEnabled())
				logger.info("Return response for audioUpload method called: ");		
			
			 * This is to inform the browser that there is no problem after uploading and returns the Status 200(OK)
			 * Handled for IOS
			 
			response.setStatus(200);
		*/
			limitedStatus="{\"status\" : \"success\"}";
			}
			else
			{
				limitedStatus="{\"status\" : \"The attachement size exceeds the allowable limit\"}";
			}
			}
		catch (Exception e)
		{
			logger.error(" Exception while uploading audio : " + e);
		}
		return limitedStatus;
	}
	
	
	@RequestMapping(value = "/androidAudio/{userId}/{fileName}/{userTokens}", method = RequestMethod.POST)
	@ResponseBody
	public String androidAudioUpload(@PathVariable("userId") String userId,@PathVariable("fileName") String fileName,@PathVariable("userTokens") String userTokens, HttpServletRequest request,HttpServletResponse response)
	{
		if(logger.isInfoEnabled())
			logger.info("Android audioUpload method called: "+userId);
		String limitedStatus=null;
		try
		{
			logger.info("request.getContentType()"+request.getContentLength());
			int uploadSize = request.getContentLength();
		    double uploadKilobytes = (uploadSize / 1024);
			double uploadMegabytes = (uploadKilobytes / 1024);
			com.musicnotes.apis.interfaces.MnUploadable videoFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
			MnUsers mnUser=videoFile.limiteSize(userId);
			if(mnUser.getLimitedSize()!=null && mnUser.getUploadedSize()!=null   && (mnUser.getLimitedSize()>(mnUser.getUploadedSize()+uploadMegabytes)|| mnUser.getLimitedSize()==0))
			{
			request.setAttribute("userId", userId);
			request.setAttribute("fileName", fileName);
			AmazonUploadable audioFilUpload = (AmazonUploadable) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.amazonAudioUpload);
			String filePath=audioFilUpload.amazonUpload(request,JavaMessages.BUCKET_NAME);
			String name=filePath.substring(filePath.lastIndexOf("/")+1, filePath.length());
			String type = "";
			int mid = name.lastIndexOf(".");
			type = name.substring(mid + 1, name.length());
			String status="A";
			int size;
		    URL url = new URL(JavaMessages.downloadUrl+filePath);
		    URLConnection conn = url.openConnection();
		    size = conn.getContentLength();
		    double kilobytes = (size / 1024);
		    System.out.println(kilobytes);
			double megabytes = (kilobytes / 1024);
			  System.out.println(megabytes);
		    if (size < 0)
		      System.out.println("Could not determine file size.");
		    else
		      System.out.println(size);
		    conn.getInputStream().close();
			videoFile.saveVideoFileName(name, userId, type, status,filePath,megabytes);
			if(logger.isInfoEnabled())
				logger.info("Return response for androidaudioUpload method called: ");
			/*
			 * This is to inform the browser that there is no problem after uploading and returns the Status 200(OK)
			 * Handled for IOS
			 */
			response.setStatus(200);
			limitedStatus="{\"status\" : \"success\"}";
			}
			else

			{
				limitedStatus="{\"status\" : \"The attachement size exceeds the allowable limit\"}";
			}
		}
		catch (Exception e)
		{
			logger.error(" Exception while uploading audio : " + e);
		}
		return limitedStatus;
	}
	
	/**
	 * This Method file Upload
	 * 
	 *  @param request
	 *            HttpServletRequest
	 * 
	 *  
	 */

	@RequestMapping(value = "/file/{userId}/{userTokens}", method = RequestMethod.POST)
	@ResponseBody
	public String fileUpload(@PathVariable("userId") String userId,@PathVariable("userTokens") String userTokens,HttpServletRequest request, HttpServletResponse response)
	{
		if(logger.isInfoEnabled())
			logger.info("fileUpload method called: "+userId);
		String limitedStatus=null;
		try
		{
			logger.info("request.getContentType()"+request.getContentLength());
			int uploadSize = request.getContentLength();
		    double uploadKilobytes = (uploadSize / 1024);
			double uploadMegabytes = (uploadKilobytes / 1024);
			com.musicnotes.apis.interfaces.MnUploadable videoFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
			MnUsers mnUser=videoFile.limiteSize(userId);
			if(mnUser.getLimitedSize()!=null && mnUser.getUploadedSize()!=null   && (mnUser.getLimitedSize()>(mnUser.getUploadedSize()+uploadMegabytes)|| mnUser.getLimitedSize()==0))
			{
			request.setAttribute("userId", userId);
			AmazonUploadable fileUpload = (AmazonUploadable) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.amazonFileUpload);
			String filePath = fileUpload.amazonUpload(request,JavaMessages.BUCKET_NAME);
			String name=filePath.substring(filePath.lastIndexOf("/")+1, filePath.length());
			String type = "";
			int mid = name.lastIndexOf(".");
			type = name.substring(mid + 1, name.length());
			type= type.toLowerCase();
			 String status="A";
			  int size;
			    URL url = new URL(JavaMessages.downloadUrl+filePath);
			    URLConnection conn = url.openConnection();
			    size = conn.getContentLength();
			    double kilobytes = (size / 1024);
			    System.out.println(kilobytes);
				double megabytes = (kilobytes / 1024);
				  System.out.println(megabytes);
			    if (size < 0)
			      System.out.println("Could not determine file size.");
			    else
			      System.out.println(size);
			    conn.getInputStream().close();
			videoFile.saveOtherFileName(name, userId ,type ,status,filePath,megabytes);
			
			  
			
			if(logger.isInfoEnabled())
				logger.info("Return response for fileUpload method called: ");
			/*
			 * This is to inform the browser that there is no problem after uploading and returns the Status 200(OK)
			 * Handled for IOS
			 */
			response.setStatus(200);
			limitedStatus="{\"status\" : \"success\"}";
			}
			else
			{
				limitedStatus="{\"status\" : \"The attachement size exceeds the allowable limit\"}";
			}
			
		}
		catch (Exception e)
		{
			logger.error(" Exception while uploading file : " + e);
		}
		return limitedStatus;
	}
	
	@RequestMapping(value = "/getAudio/{userId}", consumes = "text/plain", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String  getAudioFilesName(@PathVariable("userId") String userId, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
			logger.info("getAudioFilesName method called: "+userId);
		String fileName = null;
		try
		{
		}
		catch (Exception e)
		{
			logger.error(" Exception while getting Audion File Name : " + e);
			
		}
		return fileName;
				
	}
	
	@RequestMapping(value = "/deleteAudio/{fileName}/{userId}", consumes = "text/plain", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String  deleteAudioFilesName(@PathVariable("fileName") String fileName,@PathVariable("userId") String userId, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
			logger.info("deleteAudioFilesName method called: "+userId);
		String fileNames = null;
		boolean flag=false;
		try
		{
			File f = new File("/mnt/shared/files/upload/"+userId+"/audio/"+fileName);
			if (f.exists()){
				flag = f.delete();
			}
			com.musicnotes.apis.interfaces.MnUploadable videoFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
			videoFile.commonLog(fileName, userId, "deleted audio");
		}
		catch (Exception e)
		{
			logger.error(" Exception while Deleting Audio File Name : " + e);
			
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for deleteAudioFilesName method called: ");
		return fileNames;
				
	}
	@RequestMapping(value = "/fileAttachNote/{userId}/{listId}/{noteId}/{tokens}", method = RequestMethod.POST)
	@ResponseBody
	public String fileUploadAndAttachForNote(@PathVariable("userId") String userId, @PathVariable("listId") String listId,@PathVariable("noteId") String noteId, @PathVariable("tokens") String tokens, HttpServletRequest request, HttpServletResponse response)
	{
		if(logger.isInfoEnabled())
			logger.info("fileUploadAndAttachForNote method called: "+userId);
		String limitedStatus=null;
		try
		{
			logger.info("request.getContentType()"+request.getContentLength());
			int uploadSize = request.getContentLength();
		    double uploadKilobytes = (uploadSize / 1024);
			double uploadMegabytes = (uploadKilobytes / 1024);
			com.musicnotes.apis.interfaces.MnUploadable videoFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
			MnUsers mnUser=videoFile.limiteSize(userId);
			if(mnUser.getLimitedSize()!=null && mnUser.getUploadedSize()!=null   && (mnUser.getLimitedSize()>(mnUser.getUploadedSize()+uploadMegabytes)|| mnUser.getLimitedSize()==0))
			{
			request.setAttribute("userId", userId);
			AmazonUploadable fileUpload = (AmazonUploadable) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.amazonFileUpload);
			String filePath = fileUpload.amazonUpload(request,JavaMessages.BUCKET_NAME);
			String name=filePath.substring(filePath.lastIndexOf("/")+1, filePath.length());
			String type = "";
			int mid = name.lastIndexOf(".");
			type = name.substring(mid + 1, name.length());
			type= type.toLowerCase();
			String status="A";
			 int size;
			    URL url = new URL(JavaMessages.downloadUrl+filePath);
			    URLConnection conn = url.openConnection();
			    size = conn.getContentLength();
			    double kilobytes = (size / 1024);
			    System.out.println(kilobytes);
				double megabytes = (kilobytes / 1024);
				  System.out.println(megabytes);
			    if (size < 0)
			      System.out.println("Could not determine file size.");
			    else
			      System.out.println(size);
			    conn.getInputStream().close();
			videoFile.saveOtherFileNameAttachWithNote(name, userId ,type ,status,listId, noteId,filePath,megabytes);
			if(logger.isInfoEnabled())
				logger.info("Return response for fileUploadAndAttachForNote method called: ");
			/*
			 * This is to inform the browser that there is no problem after uploading and returns the Status 200(OK)
			 * Handled for IOS
			 */
			response.setStatus(200);
			limitedStatus="{\"status\" : \"success\"}";
			}
			else
			{
				limitedStatus="{\"status\" : \"The attachement size exceeds the allowable limit\"}";
			}
			
		}
		catch (Exception e)
		{
			logger.error(" Exception while uploading file : " + e);
		}
		return limitedStatus;
	}
	
	@RequestMapping(value = "/noteAudioUpload/{userId}/{listId}/{noteId}/{token}", method = RequestMethod.POST)
	@ResponseBody
	public String noteAudioUpload(@PathVariable("userId") String userId, @PathVariable("listId") String listId,@PathVariable("noteId") String noteId,@PathVariable("token") String token, HttpServletRequest request,HttpServletResponse response)
	{
		if(logger.isInfoEnabled())
			logger.info(token+" noteAudioUpload method called: "+userId);
		String limitedStatus=null;
		try
		{
			logger.info("request.getContentType()"+request.getContentLength());
			int uploadSize = request.getContentLength();
		    double uploadKilobytes = (uploadSize / 1024);
			double uploadMegabytes = (uploadKilobytes / 1024);
			com.musicnotes.apis.interfaces.MnUploadable videoFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
			MnUsers mnUser=videoFile.limiteSize(userId);
			if(mnUser.getLimitedSize()!=null && mnUser.getUploadedSize()!=null   && (mnUser.getLimitedSize()>(mnUser.getUploadedSize()+uploadMegabytes)|| mnUser.getLimitedSize()==0))
			{
			request.setAttribute("userId", userId);
			AmazonUploadable fileUpload = (AmazonUploadable) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.amazonFileUpload);
			String filePath = fileUpload.amazonUpload(request,JavaMessages.BUCKET_NAME);
			String name=filePath.substring(filePath.lastIndexOf("/")+1, filePath.length());
			String type = "";
			int mid = name.lastIndexOf(".");
			type = name.substring(mid + 1, name.length());
			type= type.toLowerCase();
			String status="A";
			 int size;
			    URL url = new URL(JavaMessages.downloadUrl+filePath);
			    URLConnection conn = url.openConnection();
			    size = conn.getContentLength();
			    double kilobytes = (size / 1024);
			    System.out.println(kilobytes);
				double megabytes = (kilobytes / 1024);
				  System.out.println(megabytes);
			    if (size < 0)
			      System.out.println("Could not determine file size.");
			    else
			      System.out.println(size);
			    conn.getInputStream().close();
			videoFile.saveOtherFileNameAttachWithNoteAudio(name, userId ,type ,status,listId, noteId,filePath,megabytes);
			if(logger.isInfoEnabled())
				logger.info("Return response for fileUploadAndAttachForNote method called: ");
			/*
			 * This is to inform the browser that there is no problem after uploading and returns the Status 200(OK)
			 * Handled for IOS
			 */
			response.setStatus(200);
			limitedStatus="{\"status\" : \"success\"}";
			}
			else
			{
				limitedStatus="{\"status\" : \"The attachement size exceeds the allowable limit\"}";
			}
			
		}
		catch (Exception e)
		{
			logger.error(" Exception while uploading audio : " + e);
		}
		return limitedStatus;
	}
	
	
	
	@RequestMapping(value = "/noteAndroidAudioUpload/{userId}/{listId}/{noteId}/{fileName}/{userTokens}", method = RequestMethod.POST)
	@ResponseBody
	public String noteAndroidAudioUpload(@PathVariable("userId") String userId, @PathVariable("listId") String listId,@PathVariable("noteId") String noteId,@PathVariable("fileName") String fileName,@PathVariable("userTokens") String userTokens, HttpServletRequest request,HttpServletResponse response)
	{
		if(logger.isInfoEnabled())
			logger.info("noteAndroidAudioUpload method called: "+userId);
		String limitedStatus=null;
		try
		{
			logger.info("request.getContentType()"+request.getContentLength());
			int uploadSize = request.getContentLength();
		    double uploadKilobytes = (uploadSize / 1024);
			double uploadMegabytes = (uploadKilobytes / 1024);
			com.musicnotes.apis.interfaces.MnUploadable videoFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
			MnUsers mnUser=videoFile.limiteSize(userId);
			if(mnUser.getLimitedSize()!=null && mnUser.getUploadedSize()!=null   && (mnUser.getLimitedSize()>(mnUser.getUploadedSize()+uploadMegabytes)|| mnUser.getLimitedSize()==0))
			{
			request.setAttribute("userId", userId);
			request.setAttribute("fileName", fileName);
			AmazonUploadable audioFilUpload = (AmazonUploadable) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.amazonAudioUpload);
			String filePath=audioFilUpload.amazonUpload(request,JavaMessages.BUCKET_NAME);
			String name=filePath.substring(filePath.lastIndexOf("/")+1, filePath.length());
			String type = "";
			int mid = name.lastIndexOf(".");
			type = name.substring(mid + 1, name.length());
			String status="A";
			 int size;
			    URL url = new URL(JavaMessages.downloadUrl+filePath);
			    URLConnection conn = url.openConnection();
			    size = conn.getContentLength();
			    double kilobytes = (size / 1024);
			    System.out.println(kilobytes);
				double megabytes = (kilobytes / 1024);
				  System.out.println(megabytes);
			    if (size < 0)
			      System.out.println("Could not determine file size.");
			    else
			      System.out.println(size);
			    conn.getInputStream().close();
			videoFile.saveOtherFileNameAttachWithNoteAudio(name, userId ,type ,status,listId, noteId,filePath,megabytes);
			if(logger.isInfoEnabled())
				logger.info("Return reponse for noteAndroidAudioUpload method called: ");
			/*
			 * This is to inform the browser that there is no problem after uploading and returns the Status 200(OK)
			 * Handled for IOS
			 */
			response.setStatus(200);
			limitedStatus="{\"status\" : \"success\"}";
		}
		else
		{
			limitedStatus="{\"status\" : \"The attachement size exceeds the allowable limit\"}";
		}
		}
		catch (Exception e)
		{
			logger.error(" Exception while uploading audio : " + e);
		}
		return limitedStatus;
	}
	
	
	@RequestMapping(value = "/noteVideoFiles/{userId}/{listId}/{noteId}/{fileName}/{userTokens}", method = RequestMethod.POST)
	@ResponseBody
	public String noteVideoFilesUpload(@PathVariable("userId") String userId, @PathVariable("listId") String listId,@PathVariable("noteId") String noteId,@PathVariable("fileName") String fileName,@PathVariable("userTokens") String userTokens, HttpServletRequest request,HttpServletResponse response)
	{
		if(logger.isInfoEnabled())
			logger.info("noteVideoFilesUpload method called: "+userId);
		String limitedStatus=null;
		try
		{
			logger.info("request.getContentType()"+request.getContentLength());
			int uploadSize = request.getContentLength();
		    double uploadKilobytes = (uploadSize / 1024);
			double uploadMegabytes = (uploadKilobytes / 1024);
			com.musicnotes.apis.interfaces.MnUploadable videoFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
			MnUsers mnUser=videoFile.limiteSize(userId);
			if(mnUser.getLimitedSize()!=null && mnUser.getUploadedSize()!=null   && (mnUser.getLimitedSize()>(mnUser.getUploadedSize()+uploadMegabytes)|| mnUser.getLimitedSize()==0))
			{
			request.setAttribute("userId", userId);
			if(fileName!=null && fileName.contains(".mov"))
			{
				fileName=fileName+".mov";
			}
			request.setAttribute("fileName", fileName);
    		AmazonUploadable fileUpload = (AmazonUploadable) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.amazonFileUpload);
			String filePath = fileUpload.amazonUpload(request,JavaMessages.BUCKET_NAME);
            String name=filePath.substring(filePath.lastIndexOf("/")+1, filePath.length());
        	String fullName=name;
			String type = "";
			int mid = name.lastIndexOf(".");
			name = name.substring(0, mid);
			type = fullName.substring(mid + 1, fullName.length());
			String status="A";
			 int size;
			    URL url = new URL(JavaMessages.downloadUrl+filePath);
			    URLConnection conn = url.openConnection();
			    size = conn.getContentLength();
			    double kilobytes = (size / 1024);
			    System.out.println(kilobytes);
				double megabytes = (kilobytes / 1024);
				  System.out.println(megabytes);
			    if (size < 0)
			      System.out.println("Could not determine file size.");
			    else
			      System.out.println(size);
			    conn.getInputStream().close();
			videoFile.saveOtherFileNameAttachWithNote(fullName, userId ,type ,status,listId, noteId,filePath,megabytes);
			if(logger.isInfoEnabled())
				logger.info("Return response for noteVideoFilesUpload method called: ");		
			/*
			 * This is to inform the browser that there is no problem after uploading and returns the Status 200(OK)
			 * Handled for IOS
			 */
			limitedStatus="{\"status\" : \"success\"}";
			response.setStatus(200);
			}
			else
			{
				limitedStatus="{\"status\" : \"The attachement size exceeds the allowable limit\"}";
			}
		}
		catch (Exception e)
		{
			logger.error(" Exception while uploading video : " + e);
		}
		return limitedStatus;
	}

}
