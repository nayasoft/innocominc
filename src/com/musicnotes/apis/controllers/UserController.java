package com.musicnotes.apis.controllers;

import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import amazonupload.AmazonProfileUpload;
import amazonupload.AmazonUploadable;
import com.musicnotes.apis.domain.MnFeedbacks;
import com.musicnotes.apis.domain.MnMailConfiguration;
import com.musicnotes.apis.domain.MnMailNotificationList;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.interfaces.MnUserable;
import com.musicnotes.apis.resources.MnUser;
import com.musicnotes.apis.util.ConvertUtil;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.MailContent;
import com.musicnotes.apis.util.SendMail;
import com.musicnotes.apis.util.SpringBeans;

@Controller
@RequestMapping("/user")
public class UserController extends BaseController
{
	MnUserable mnUser;
	Logger logger = Logger.getLogger(UserController.class);

	
	@RequestMapping(value = "/createUser", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String createUser(@RequestBody String jsonStr, HttpServletRequest request)
	  {
		if(logger.isInfoEnabled())
		logger.info("while creating a creatuser method called: ");		 
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		String status = "";
		String statusMail="";
		String checkUser=null;
		try
		{
			 jsonStr = decodeJsonString(jsonStr);
			 jsonStr = removeUnWantedChar(jsonStr);
			 
			 MnUsers user = mnUser.convertJsonToUserObj(jsonStr);
			
			 Random random = new Random();
			 String[] startEndPart={"AZ","BX","CY","DB","EL","FM","GO","HU","IP","JR"};
			 String[] part1={"8j","9f","6y","5r","3h","4k","2d","1v","0o","7x"};
			 String[] part2={"#6","#9","#5","@1","@2","@3","$9","$6","$7","$5"};
			 String[] part3={"R0","#W","K@","$T","VZ","@S","X9","1F","M7","5S"};
			 String randomPass=startEndPart[random.nextInt(10)]+part1[random.nextInt(10)]+part2[random.nextInt(10)]+part3[random.nextInt(10)];
			 user.setMailCheckId(randomPass);
			 status = mnUser.insertNewUser(user,jsonStr);
			 statusMail=mnUser.userMailNotificationList(user.getUserId(),jsonStr);
             checkUser=mnUser.checkInviteuser(user);
             } 		
		catch (Exception e)
         {
			logger.error("Exception while creating user : ", e);
			status = "0";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for creatuser method : "+status);
		return status;
	}
	@RequestMapping(value = "/addingUser", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String addingUser(@RequestBody String jsonStr, HttpServletRequest request)
	  {
		if(logger.isInfoEnabled())
		logger.info("while creating a addingUser method called: ");		 
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		String status = "";
		String checkUser=null;
		try
		{
			 jsonStr = decodeJsonString(jsonStr);
			 jsonStr = removeUnWantedChar(jsonStr);
			 status = mnUser.addNewUser(jsonStr);
             //checkUser=mnUser.checkInviteuser(user);
             } 		
		catch (Exception e)
         {
			logger.error("Exception while creating user : ", e);
			status = "0";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for creatuser method : "+status);
		return status;
	}
	@RequestMapping(value = "/fetchAddusersDetails", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String fetchAddusersDetails(@RequestBody String params, HttpServletRequest request){
	String status=null;
	mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
	params = decodeJsonString(params);
	params = removeUnWantedChar(params);
	try{
	if(logger.isInfoEnabled()){logger.info("fetchAddusersDetails method called");}	
	status=mnUser.fetchAddusersDetails(params,request);
	if(logger.isInfoEnabled()){logger.info("fetchAddusersDetails successfully");}
	}catch(Exception e){
	logger.error("Exception during fetchAddusersDetails"+e);
	}
	return status;
	}
	// sending mail
	@RequestMapping(value = "/sendUserCreationMail", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String sendUserCreationMail(@RequestBody String param, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("while sending amail for sendingUserCreationMail method called: ");
		String status="";
		try
		{
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		status=mnUser.sendUserCreationMail(param);
		}
		catch (Exception e) {
			logger.error("Exception while sendingUserCreationMail method : ", e);
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for sendingUserCreationMail method : "+status);
		return status;
	
	}

	@RequestMapping(value = "/userFeedback", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String userFeedback(@RequestBody String param, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("while user feedback for userFeedback method called: ");
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		MnFeedbacks user = mnUser.feedbackDetails(param);
		String status = "";
		try{
			if(user!= null){
				SendMail sendMail=new SendMail();
				String recipient[]={MailContent.clientMailId};//feedback to client
				String recipients[]={user.getUserMailId()};//from client to user
				String userfeedback=user.getUserFeedback();
				if(userfeedback.indexOf("\\\"") != -1){
					userfeedback = userfeedback.replace("\\\"", "\"");
				}
				try{
					String message1=MailContent.userfeedback+"<br><b> User Name : </b>" +user.getUserFirstName()+" "+user.getUserLastName()+
					"<br><br><b> Email : </b>" +user.getUserMailId()+
					"<br><br><b> Description : </b>" +userfeedback+
					MailContent.signature;
					String subject1="Feedback";;
					String message=MailContent.sharingNotification+user.getUserFirstName()+""+MailContent.sharingNotification1+"<br>Thanks for your feedback. Our team is looking into it and will get back to you as soon as possible."+ 
					MailContent.signature1;
					String subject="Thanks for your Feedback";
					sendMail.postEmail(recipient, subject1, message1);
					sendMail.postEmail(recipients, subject,message);
				}catch(Exception e){
					logger.error("Exception while sending user : ", e);
				}
				
				status="success";
			}
		}
		catch (Exception e)
		{
			logger.error("Exception while userFeedback  : ", e);
			status = "0";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for userFeedback method : "+status);
		return status;
	}
	
	@RequestMapping(value = "/getUserDetails",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String getUserDetails(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while fetching getUserDetails method called: ");

		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		
		
		
		
		//MnUsers user = mnUser.convertJsonToUserObj(param);
		String status = "";
		try {
			JSONObject jsonObject = new JSONObject(param);
			String userId=(String) jsonObject.get("userId");
			
			status = mnUser.getUserDetails(userId,request);
		} catch (Exception e) {
			logger.error("Exception while fetching user details: ", e);
			status = "Exception while fetching user details";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for getUserDetails method : "+status);
		return status;
	}
	@RequestMapping(value = "/fetchUserDetails",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String fetchUserDetails(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while fetching fetchUserDetails method: ");
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		MnUsers user = mnUser.convertJsonToUserObj(param);
		String status = "";
		try {
			status = mnUser.fetchUserDetails(user.getUserRole(),request);
		} catch (Exception e) {
			logger.error("Exception while student fetching user details: ", e);
			status = "Exception while student fetching user details";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for fetchUserDetails method : "+status);
		return status;
	}
	
	@RequestMapping(value = "/memberList/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String userMemberList(@PathVariable("userId") String userId, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while fetching userMemberList method: "+userId);
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		String status = "";
		try {
			status = mnUser.memberList(userId,request);
		} catch (Exception e) {
			logger.error("Exception while fetching user details: ", e);
			status = "Exception while fetching user details";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for userMemberList method : "+status);
		return status;
	}
	
	@RequestMapping(value = "/updateUser", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String updateUser(@RequestBody String jsonStr, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("while updating updateUser method: ");
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		String status = "";
		try
		{
			jsonStr = decodeJsonString(jsonStr);
			jsonStr = removeUnWantedChar(jsonStr);
			MnUsers user = mnUser.convertJsonToUpdateUserObj(jsonStr);
		    status = mnUser.updateUser(user);
		  //  status=mnUser.updatUserMailNotificationList(user.getUserId(),jsonStr);
		}
		catch (Exception e)
		{
			logger.error("Exception while updateing user : ", e);
			status = "0";
		}
           if(logger.isInfoEnabled())
		logger.info("Return response for updateUser method : ");
		return status;
	}
	
	@RequestMapping(value = "/getAllActiveUsersListForSearch",  consumes = "text/plain", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String getAllActiveUsersListForSearch(@RequestBody String jsonStr, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while getting a getAllActiveUsersListForSearch method: ");
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		String status = "";
		try {
			jsonStr = decodeJsonString(jsonStr);
			jsonStr = removeUnWantedChar(jsonStr);
			MnUsers user = mnUser.convertJsonToUserObj(jsonStr);
			status = mnUser.getViewAllActiveUserListForSearch(user,request);
		} catch (Exception e) {
			logger.error("Exception while fetching user details: ", e);
			status = "Exception while fetching all user details for search ";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for getAllActiveUsersListForSearch method : ");
		return status;
	}
	
	
	@RequestMapping(value = "/fetchTeachersList",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String fetchTeachersList(HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while getting a fetchTeachersList method: ");
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		String status = mnUser.getTeachersList(request);
		try {
			
		} catch (Exception e) {
			logger.error("Exception while student fetching user details: ", e);
			status = "Exception while student fetching user details";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for fetchTeachersList method : ");
		return status;
	}

	@RequestMapping(value = "/getExistsEmail",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String getExistsEmail(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while getting a getExistsEmail method: ");
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		String status = "";
		try {
			status = mnUser.getExistsEmail(param);
		} catch (Exception e) {
			logger.error("Exception while fetching email info user details: ", e);
			status = "Exception while student fetching email info details";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for getExistsEmail method : "+status);
		return status;
	}
	
	@RequestMapping(value = "/resetPassword",  consumes = "text/plain",  method = RequestMethod.POST)
	@ResponseBody
	public String resetPassword(@RequestBody String params, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while resetting a resetPassword method: ");
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		String status = "";
		try {
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			status = mnUser.resetPassword(params);
				
		} catch (Exception e) {
			logger.error("Exception while reset password controller: ", e);
			status = "wrong";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for resetPassword method : ");
		return "{\"status\":\""+status+"\"}";
	}
	
	@RequestMapping(value = "/forgetPassword",  consumes = "text/plain", method = RequestMethod.POST)
	@ResponseBody
	public String forgetPassword(@RequestBody String params, HttpServletRequest request)
   {	if(logger.isInfoEnabled())	
		logger.info("while forgoting a forgetPassword method: ");
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		String status = "";
		try {
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			JSONObject jsonObject = new JSONObject(params);
			String emailId = (String ) jsonObject.get("emailId");
			if(emailId!= null && !emailId.trim().equals("")){
				status = mnUser.forgetPassword(emailId);
			}else{
				status="empty";
			}
		} catch (Exception e) {
			logger.error("Exception while forget password controller: ", e);
			status="invalid";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for forgetPassword method : ");
		return "{\"status\":\""+status+"\"}";
	}
	
	@RequestMapping(value = "/updateEmail",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String updateEmail(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while updating a updateEmail method: ");
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		String status = "";
		try {
			status = mnUser.updateEmail(param);
		} catch (Exception e) {
			logger.error("Exception while fetching update email info user details: ", e);
			status = "Exception while student fetching update email info details";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for updateEmail method : ");
		return status;
	}
	
	
	@RequestMapping(value = "/updateProfileImg/{userId}/{xAxis}/{yAxis}/{width}/{height}/{token}", method = RequestMethod.POST)
	@ResponseBody
	public String updateProfileImg(@PathVariable("userId") String userId,@PathVariable("xAxis") String xAxis,@PathVariable("yAxis") String yAxis,@PathVariable("width") String width,@PathVariable("height") String height,@PathVariable("token") String token,HttpServletRequest request)
     {
		String status="";
		boolean cropFlag=false;
		if(logger.isInfoEnabled())
		logger.info("while uploading  a updateProfileImg method: "+userId);
		logger.info("while uploading  a updateProfileImg method: "+xAxis);
		logger.info("while uploading  a updateProfileImg method: "+yAxis);
		logger.info("while uploading  a updateProfileImg method: "+width);
		logger.info("while uploading  a updateProfileImg method: "+height);
		logger.info("request.getContentType()"+request.getContentLength());
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		int uploadSize = request.getContentLength();
	    double uploadKilobytes = (uploadSize / 1024);
		double uploadMegabytes = (uploadKilobytes / 1024);
		MnUsers mnUsers=mnUser.limiteSize(userId);
		if(mnUsers.getLimitedSize()!=null && mnUsers.getUploadedSize()!=null   && (mnUsers.getLimitedSize()>(mnUsers.getUploadedSize()+uploadMegabytes)|| mnUsers.getLimitedSize()==0))
		{
		
		request.setAttribute("userId", userId);
		request.setAttribute("xAxis", xAxis);
		request.setAttribute("yAxis", yAxis);
		request.setAttribute("width", width);
		request.setAttribute("height", height);
		if(xAxis!=null && yAxis!=null && width!=null && height!=null && !xAxis.equals("0") && !yAxis.equals("0") && !width.equals("0") && !height.equals("0"))
		{
			
			cropFlag=true;
		}
		
		//AmazonUploadable fileUpload = (AmazonUploadable) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.amazonFileUpload);
		//String filePath = fileUpload.amazonUpload(request,JavaMessages.BUCKET_NAME);
		AmazonProfileUpload  amazonProfileUpload=new AmazonProfileUpload();
		String filePath=amazonProfileUpload.writeFile(request, "video", JavaMessages.uploadPathForGodaddy,JavaMessages.BUCKET_NAME);
		
		
		status=mnUser.updateProfilePicture(userId, filePath, request,cropFlag);
		if(logger.isInfoEnabled())
		logger.info("Return response for updateProfileImg method : "+status);
		}
		else
		{
			status="{\"status\" : \"Upload Error\"}";
		}
		return status;
	}
	
	@RequestMapping(value = "/ExistsUser",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String ExistsUser(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while checking   a ExistsUser method: ");
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		String status = "";
		try {
			status = mnUser.ExistsUser(param);
		} catch (Exception e) {
			logger.error("Exception while fetching email info user details: ", e);
			status = "Exception while student fetching email info details";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for ExistsUser method : "+status);
		return status;
	}
	
	
	@RequestMapping(value = "/updateExistsUserName",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String updateUserName(@RequestBody String param, HttpServletRequest request)
   {
		if(logger.isInfoEnabled())
		logger.info("while updating a updateUserName method: ");
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		param = decodeJsonString(param);
		param = removeUnWantedChar(param);
		String status = "";
		try {
			status = mnUser.updateUserName(param);
		} catch (Exception e) {
			logger.error("Exception while fetching update UserName info user details: ", e);
			status = "Exception while student fetching update UserName info details";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for updateUserName method : ");
		return status;
	}
	
	@RequestMapping(value = "/getTrailcount", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getTrailcount(@RequestBody String params, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("while getting a getTrailcount method: ");
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		String status = "";
		try {
			status = mnUser.getTrailcount(params);
		} catch (Exception e) {
			logger.error("Exception while fetching user details: ", e);
			status = "Exception while fetching user details";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for getTrailcount method : "+status);
		return status;
	}
	
	
	@RequestMapping(value = "/getSecurityQuestion", consumes = "text/plain",  method = RequestMethod.POST)
	@ResponseBody
	public String getSecurityQuestion(HttpServletRequest request){
		if(logger.isInfoEnabled())
		logger.info("while updating  a getSecurityQuestion method: ");
		
		String status = "";
		try {
			status = mnUser.getAdminSecurityQuestion();
			if(status.equals("")){
			String q1=JavaMessages.Q1;
			String q2=JavaMessages.Q2;
			String q3=JavaMessages.Q3;
			String q4=JavaMessages.Q4;
			String q5=JavaMessages.Q5;
			status=q1+"~"+q2+"~"+q3+"~"+q4+"~"+q5;
			}
		} catch (Exception e) {
			logger.error("Exception while fetching user level: ", e);
			status = "Exception while fetching user level";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for getSecurityQuestion method : "+status);
		
		return status;
	}
	
	
	@RequestMapping(value = "/getSecurityQuestions", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getSecurityQuestion(@RequestBody String params,HttpServletRequest request){
		if(logger.isInfoEnabled())
		logger.info("while checking  a getSecurityQuestions method: ");
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		String status = "";
		try {
			status = mnUser.getSecurityQuestion(params);
		} catch (Exception e) {
			logger.error("Exception while security question: ", e);
			status = "Exception while fetching user level";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for checkDefaultConfiguration method : ");
		return status;
	}
	
	
	@RequestMapping(value = "/setResetPassword", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String setResetPassword(@RequestBody String params,HttpServletRequest request){
		if(logger.isInfoEnabled())
		logger.info("while checking  a setResetPassword method: ");
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		String status = "";
		try {
			status = mnUser.setResetPassword(params);
		} catch (Exception e) {
			logger.error("Exception while setResetPassword : ", e);
			status = "Exception while fetching user level";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for setResetPassword method : ");
		return status;
	}
	
	@RequestMapping(value = "/newToken", method = RequestMethod.POST)
	@ResponseBody
	public String newToken(HttpServletRequest request){
		if(logger.isInfoEnabled())
		logger.info("while checking  a newToken method: ");
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		
		String status = "";
		try {
			
			status = mnUser.newToken(request);
		} catch (Exception e) {
			logger.error("Exception while newToken : ", e);
			status = "Exception while newToken";
		}
		if(logger.isInfoEnabled())
			logger.info("Return response for newToken method : ");
		return status;
	}
	
	
	@RequestMapping(value = "/mailConfig/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String mailConfig(@PathVariable("userId") String UserId, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("while fetching  a mailConfig method: ");
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		
		String status = "";
		
		try {
			status = mnUser.mailConfig(UserId);
		} catch (Exception e) {
			logger.error("Exception while fetching mailConfig: ", e);
			status = "Exception while fetching mailConfig";
		}
		if(logger.isInfoEnabled())
		logger.info("Return response for mailConfig method : ");
		return status;
	}
	
	@RequestMapping(value = "/updateMailConfig/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String updateMailConfig(@PathVariable("userId") String userId,@RequestBody String jsonStr, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("while updating updateMailconfig method: ");
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		String status = "";
		try
		{
			jsonStr = decodeJsonString(jsonStr);
			jsonStr = removeUnWantedChar(jsonStr);
			status=mnUser.updatUserMailNotificationList(Integer.parseInt(userId),jsonStr);
		}
		catch (Exception e)
		{
			logger.error("Exception while updateing updateMailconfig : ", e);
			status = "0";
		}
           if(logger.isInfoEnabled())
		logger.info("Return response for updateUser method : ");
		return status;
	}
	/*@RequestMapping(value = "/viewDefaultConfig", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String viewDefaultConfiguration(@RequestBody String params,HttpServletRequest request){
		if(logger.isInfoEnabled()){logger.info("viewDefaultConfiguration method called");};
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		
		String status = "";
		try {
			status = mnUser.viewDefaultConfiguration();
			if(logger.isInfoEnabled()){logger.info("Successfully got data for default configuration");};
		} catch (Exception e) {
			logger.error("Exception while fetching default configuration: ", e);
			status = "Exception while fetching default configuration:";
		}
		return status;
	}*/
	
		
}


