package com.musicnotes.apis.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.musicnotes.apis.domain.MnEventDetails;
import com.musicnotes.apis.interfaces.MnEventable;
import com.musicnotes.apis.interfaces.MnUserable;
import com.musicnotes.apis.mail.DeleteMail;
import com.musicnotes.apis.mail.ReadMail;
import com.musicnotes.apis.mail.SendMail;
import com.musicnotes.apis.resources.MnEvents;
import com.musicnotes.apis.resources.MnUser;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.SpringBeans;

@Controller
@RequestMapping("/mail")
public class EmailController extends BaseController{
	SendMail sendMail;
	ReadMail readMail;
	DeleteMail deleteMail;
	MnUserable mnUser;
	MnEventable mnEventable;

	Logger logger = Logger.getLogger(EmailController.class);

	@RequestMapping(value = "/postEmail", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String sendMail(@RequestBody String params,
			HttpServletRequest request) {
		if(logger.isInfoEnabled())
			logger.info("sendMail method called: ");
		sendMail = (SendMail) SpringBeans.getBeanFromBeanFactory(request,
				JavaMessages.Spring.SENDMAIL);
		mnUser = (MnUser) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNUSER);
		mnEventable = (MnEvents) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNEVENT);
		
		String status = "";
		Map<String,String> map=null;
		MnEventDetails mnEventDetails=null;
		try {
			String recipients = null;
			String message = null;
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);

			JSONObject jsonObject = new JSONObject(params);

			String portNo =JavaMessages.portNo; 
			String host = JavaMessages.host;
			String userName =JavaMessages.userName;
			String password =JavaMessages.password;		
			recipients=  (String) jsonObject.get("to");
			message = (String) jsonObject.get("body");
			String mode = (String) jsonObject.get("mode");
			String noteUserName = (String) jsonObject.get("noteUserName");
			String noteUserFirstName=(String) jsonObject.get("noteUserFirstName");
			String noteName = (String) jsonObject.get("noteName");
			String type = (String) jsonObject.get("type");
			String listType = (String) jsonObject.get("listType");
			String listId = (String) jsonObject.get("listId");
			String noteId = (String) jsonObject.get("noteId");

			message="<div style=\"margin:0;padding:0\" dir=\"ltr\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:98%\" border=\"0\"><tbody><tr><td style=\"font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;font-size:12px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"font-size:16px;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;background:#3b5998;color:#ffffff;font-weight:bold;vertical-align:baseline;letter-spacing:-0.03em;text-align:left;padding:10px 38px 4px\"><a style=\"text-decoration:none\" href=\"http://www.nayasoft.in.previewdns.com/musicnote/index.html\" target=\"_blank\"><span style=\"background:#3b5998;color:#ffffff;font-weight:bold;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;vertical-align:middle;font-size:16px;letter-spacing:-0.03em;text-align:left;vertical-align:baseline\">musicnote</span></a></td></tr></tbody></table><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"border-right:1px solid #ccc;line-height:16px;font-size:11px;border-bottom:1px solid #ccc;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;border-top:1px solid #ccc;padding:10px 20px;border-left:1px solid #ccc\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 20px;line-height:16px;width:620px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\">Hi,</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">Here\'s some activity you may have missed on Musicnote.</td></tr><tr><td height=\"1\" style=\"background-color:#ccc\"></td></tr><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:15px 20px 15px 0\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td valign=\"top\" style=\"padding-right:5px;font-size:0px\"><img style=\"border:0\"></td><td valign=\"top\" style=\"width:100%\"><a href=\"http://www.nayasoft.in.previewdns.com/musicnote/index.html\" style=\"color:#3b5998;text-decoration:none;font-size:13px\" target=\"_blank\">1 new note</a></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\"><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse:collapse;width:100%\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px;background-color:#f2f2f2;border-left:none;border-right:none;border-top:1px solid #ccc;border-bottom:1px solid #ccc\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding-right:10px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"border-width:1px;border-style:solid;border-color:#29447e #29447e #1a356e;background-color:#5b74a8\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:2px 6px 4px;border-top:1px solid #8a9cc2\"><a href=\"http://www.nayasoft.in.previewdns.com/musicnote/index.html\" style=\"color:#3b5998;text-decoration:none\" target=\"_blank\"><span style=\"font-weight:bold;white-space:nowrap;color:#fff;font-size:13px\">Go To Musicnote</span></a></td></tr></tbody></table></td></tr></tbody></table></td><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"border-width:1px;border-style:solid;border-color:#999 #999 #888;background-color:#eee\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:2px 6px 4px;border-top:1px solid #fff\"><a href=\"http://www.nayasoft.in.previewdns.com/musicnote/index.html \" style=\"color:#3b5998;text-decoration:none\" target=\"_blank\"><span style=\"font-weight:bold;white-space:nowrap;color:#333;font-size:13px\">See All Notifications</span></a></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"border-right:none;color:#999999;font-size:11px;border-bottom:none;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;border:none;border-top:none;padding:10px;border-left:none\">This message was sent From Musicnote. If you don\'t want to receive these emails from Musicnote in the future, please <a href=\"http://www.nayasoft.in.previewdns.com/musicnote/index.html\" style=\"color:#3b5998;text-decoration:none\" target=\"_blank\">unsubscribe</a>.<br>Musicnote, Inc.,</td></tr></tbody></table><br><br><table style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"border-right:none;color:red;font-size:11px;border-bottom:none;font-family:\'lucidagrande\',tahoma,verdana,arial,sans-serif;border:none;border-top:none;padding:10px;border-left:none\"><u>Disclaimer</u><br>This message contains confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. E-mail transmission cannot be guaranteed to be secure or error-free as information could be intercepted, corrupted, lost, destroyed, arrive late or incomplete, or contain viruses. The sender therefore does not accept liability for any errors or omissions in the contents of this message, which arise as a result of e-mail transmission. If verification is required mail to support@nayasoft.in</td></tr></tbody></table></table><span style=\"width:620px\"><img style=\"border:0;width:1px;min-height:1px\"></span></td></tr></tbody></table></div>";
			
			if(mnUser!=null && recipients!= null && !recipients.equals(""))
				map= mnUser.getUserName(recipients,listType);
			
			
			
			if(listType.equalsIgnoreCase("schedule"))
			{
				String nameNo=map.get(recipients);
				if(nameNo!=null)
				{
				String arr[]=nameNo.split("~");
				String userId=arr[1].toString();
				mnEventDetails= mnEventable.getEventsForMail(listId,noteId,userId);
				}
				
			}
			
			sendMail.initialize(null, portNo, host, userName, password,mode);

			sendMail.postEmail(recipients, message,mode,noteUserName,noteName,map,type,noteUserFirstName,listType,mnEventDetails);

		} catch (Exception e) {
			logger.error("Exception while sending email : ", e);
			status = "problem while sending email";
		}
		if(logger.isInfoEnabled())
			logger.info("Return reponse for sendMail method called: ");
		return status;
	}

	@RequestMapping(value = "/getMailMessages", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getMailMessages(@RequestBody String params,
			HttpServletRequest request) {
		if(logger.isInfoEnabled())
			logger.info("getMailMessages method called: ");
		String jsonMailStr = null;
		readMail = (ReadMail) SpringBeans.getBeanFromBeanFactory(request,
				JavaMessages.Spring.READMAIL);

		try {
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			JSONObject jsonObject = new JSONObject(params);
			String folderType = (String) jsonObject.get("folderType");
			String portNo = (String) jsonObject.get("portNo");
			String host = (String) jsonObject.get("host");
			String userName = (String) jsonObject.get("userName");
			String password = (String) jsonObject.get("password");
			String mode = (String) jsonObject.get("mode");
			readMail.initialize(folderType, portNo, host, userName, password,mode);
			jsonMailStr = readMail.readMail(params);

		} catch (Exception e) {
			logger.error("Exception while fetching mail messages details: ", e);
		}
		if(logger.isInfoEnabled())
			logger.info("Return reponse for getMailMessages method called: ");
		return jsonMailStr;
	}

	@RequestMapping(value = "/deleteMail", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String deleteMail(@RequestBody String params,
			HttpServletRequest request) {
		if(logger.isInfoEnabled())
			logger.info("deleteMail method called: ");
		deleteMail = (DeleteMail) SpringBeans.getBeanFromBeanFactory(request,
				JavaMessages.Spring.DELETEMAIL);
		String status = "";
		try {
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			JSONObject jsonObject = new JSONObject(params);
			String folderType = (String) jsonObject.get("folderType");
			String portNo = (String) jsonObject.get("portNo");
			String host = (String) jsonObject.get("host");
			String userName = (String) jsonObject.get("userName");
			String password = (String) jsonObject.get("password");
			String mode = (String) jsonObject.get("mode");
			String mailNo=(String)jsonObject.get("mailNo");
			deleteMail.initialize(folderType, portNo, host, userName, password,mode);
			status = deleteMail.deleteMail(mailNo);

		} catch (Exception e) {
			logger.error("Exception while creating user : ", e);
			status = "Exception while creating user";
		}
		if(logger.isInfoEnabled())
			logger.info("Return reponse for deleteMail method called: ");
		return status;
	}

}
