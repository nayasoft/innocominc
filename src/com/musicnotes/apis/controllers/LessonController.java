package com.musicnotes.apis.controllers;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import amazonupload.AmazonUploadable;

import com.musicnotes.apis.resources.MnUpload;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.SpringBeans;

@Controller
@RequestMapping("/Lesson")
public class LessonController extends BaseController
{
	
	Logger logger = Logger.getLogger(LessonController.class);
	@RequestMapping(value = "/getRecordedLessons/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getRecordedLessons(@PathVariable("userId") String userId, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("Getting record lessons entered userId:"+userId);
		String fileName = null;
		try
		{
			com.musicnotes.apis.interfaces.MnUploadable audioFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
			fileName=audioFile.getVideoFileNames(userId);
			
		}
		catch (Exception e)
		{
			logger.error(" Exception while getting video File Name : " + e);
			
		}
		if(logger.isInfoEnabled())
		logger.info("Getting record lessons for record lesson Json{}");
		return fileName;		
	}

	
	@RequestMapping(value = "/getRecordedLessonsForNote/{userId}/{listId}/{noteId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getRecordedLessonsForNote(@PathVariable("userId") String userId,@PathVariable("listId") String listId,@PathVariable("noteId") String noteId, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("Getting record lessons for within note entered userId:"+userId+" noteId: "+noteId);
		String fileName = null;
		try
		{
			com.musicnotes.apis.interfaces.MnUploadable audioFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
			fileName=audioFile.getVideoFileNamesForNote(userId,listId,noteId);
		}
		catch (Exception e)
		{
			logger.error(" Exception while getting video File Name : " + e);
			
		}
		if(logger.isInfoEnabled())
		logger.info("Getting record lessons for within note terminated Json{}");
		return fileName;		
	}
	
	/**
	 * Method to download video file
	 * 
	 * @param userId
	 *            userId 
	 * 
	 * @param request
	 *            HttpServletRequest
	 * 
	 *  
	 */
	

	@RequestMapping(value = "/download/{userId}/{fileName}/{extension}/{tempfileName}/{userTokens}", method = RequestMethod.POST)
	public void downloadFile(@PathVariable("userId") String userId,@PathVariable("fileName") String fileName, @PathVariable("extension") String fileExt ,@PathVariable("tempfileName") String tempfileName,@PathVariable("userTokens") String userTokens,HttpServletResponse response,HttpServletRequest request)
	{
		try
		{
			if(logger.isInfoEnabled())
			logger.info("download file entered "+userId+" filename is: "+fileName);
			String folder = fileExt;
			folder=folder.toLowerCase();
			if(tempfileName.contains("_"))
				tempfileName=tempfileName.substring(0, tempfileName.indexOf("_"));
			response.setHeader("Content-Disposition", "attachment;filename=\""+tempfileName+"."+fileExt+"\"");
			response.setHeader("Pragma", "private");
			response.addHeader("Cache-Control", "no-transform, max-age=0");
			response.setHeader("Accept-Ranges", "bytes");
			InputStream inputStream =null;
			com.musicnotes.apis.interfaces.MnUploadable audioFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
			String filePath=audioFile.getAllFiles(fileName, Integer.parseInt(userId));
			if(fileName!=null && (fileName.contains(".mov") || fileName.contains(".avi") || fileName.contains(".wmv") || fileName.contains(".wma")))
			{
				response.setContentType("video/"+folder);
				
				inputStream = new URL(JavaMessages.downloadUrl+filePath).openStream();
			}
			else
			{
				if(folder.equalsIgnoreCase("pdf"))
					response.setContentType("application/pdf");	
				else if(folder.equalsIgnoreCase("txt"))
					response.setContentType("text/plain");
				else if(folder.equalsIgnoreCase("xls"))
					response.setContentType("application/xls");
				else if(folder.equalsIgnoreCase("xlsx"))	
					response.setContentType("application/vnd.ms-excel");
				else if(folder.equalsIgnoreCase("doc"))
					response.setContentType("application/msword");
				else if(folder.equalsIgnoreCase("docx"))
					response.setContentType("application/vnd.ms-word");
				
				inputStream = new URL(JavaMessages.downloadUrl+filePath).openStream();
			}
				
			
			byte[] buff = new byte[8000];

			int bytesRead = 0;

			ByteArrayOutputStream bao = new ByteArrayOutputStream();

			while ((bytesRead = inputStream.read(buff)) != -1)
			{
				bao.write(buff, 0, bytesRead);
			}

			byte[] videoBytes = bao.toByteArray();
			response.setContentLength(videoBytes.length);
			ServletOutputStream outStream = response.getOutputStream();
			outStream.write(videoBytes);
			outStream.flush();
			outStream.close();
			bao.close();
			response.flushBuffer();
			
		}
		catch (Exception e)
		{
			logger.error(" Exception while downloading file : " + e);
		}
		if(logger.isInfoEnabled())
		logger.info("File download successfully");
	}
	
	@RequestMapping(value = "/downloadFile/{userId}/{fileName}/{userTokens}", method = RequestMethod.POST)
	public void createZipFile(@PathVariable("userId") String userId,@PathVariable("fileName") String fileName,@PathVariable("userTokens") String userTokens,HttpServletResponse response,HttpServletRequest request) {
        
		if(logger.isInfoEnabled())
			logger.info("downloadFile method called for userId:"+userId);
        try {
            //..code to add URLs to the list
            byte[] buf = new byte[2048];
            // Create the ZIP file
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ZipOutputStream out = new ZipOutputStream(baos);
            String zipFileName = JavaMessages.zipFileName;
            com.musicnotes.apis.interfaces.MnUploadable audioFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
        	String[] fileNames = fileName.split(",");
        	InputStream inputStream =null;
            // Compress the files
            for (int i=0; i<fileNames.length; i++) 
            {
            	String filePath=audioFile.getAllFiles(fileNames[i], Integer.parseInt(userId));
            	inputStream = new URL(JavaMessages.downloadUrl+filePath).openStream();
            	BufferedInputStream bis = new BufferedInputStream(inputStream);
            	// Add ZIP entry to output stream.
            	String fileNaming = fileNames[i];
            	File file = new File(fileNaming);
            	String entryname = file.getName();
            	out.putNextEntry(new ZipEntry(entryname));
            	int bytesRead;
            	while ((bytesRead = bis.read(buf)) != -1) {
            		out.write(buf, 0, bytesRead);
            	}
            out.closeEntry();
            bis.close();
            inputStream.close();
            }
            
            out.flush();
            baos.flush();
            out.close();
            baos.close();
            ServletOutputStream sos = response.getOutputStream();
            response.setContentType("application/zip");
            response.setHeader("Pragma", "private");
			response.addHeader("Cache-Control", "no-transform, max-age=0");
			response.setHeader("Accept-Ranges", "bytes");
            response.setHeader("Content-Disposition", "attachment; filename=\""+zipFileName+"\"");
            sos.write(baos.toByteArray());
            out.flush();
            out.close();
            sos.flush();
        	
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
	
	
	@RequestMapping(value = "/getOtherFiles/{userId}", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getOtherFiles(@PathVariable("userId") String userId, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("Getting Other files entered userId:"+userId);
		String fileName = null;
		try
		{
			com.musicnotes.apis.interfaces.MnUploadable audioFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
			fileName=audioFile.getOtherFileNames(userId);
		}
		catch (Exception e)
		{
			logger.error("Exception while getting video File Name : " + e);
			
		}
		if(logger.isInfoEnabled())
		logger.info("Getting Other files Json{}");
		return fileName;		
	}
	
	@RequestMapping(value = "/deleteVideo/{fileName}/{userId}", consumes = "text/plain", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String  deleteFile(@PathVariable("fileName") String fileName,@PathVariable("userId") String userId, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("Delete other files entered "+userId+" filename is: "+fileName);
		String fileNames = null;
		File f=null;
		String success = null;
		try
		{
				com.musicnotes.apis.interfaces.MnUploadable videoFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
				success=videoFile.deleteOtherFileNames(fileName, userId);
				
				if(success!=null && success.contains("success"))
				{
					com.musicnotes.apis.interfaces.MnUploadable audioFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
					String filePath=audioFile.getAllFiles(fileName, Integer.parseInt(userId));
					String name=filePath.substring(filePath.lastIndexOf("/")+1, filePath.length());
					String FullPath=filePath.replace(name, "");
					AmazonUploadable amazonFileDelete=(AmazonUploadable)SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.amazonFileDelete);
					String status=amazonFileDelete.deleteFile(filePath.replaceAll(filePath.substring(filePath.lastIndexOf("/"), filePath.length()), ""), name);
					
				}
		}
		catch (Exception e)
		{
			logger.error(" Exception while Deleting File Name : " + e);
			
		}
		if(logger.isInfoEnabled())
		logger.info("Other files deleted and termination"+success);
		return success;
				
	}
	@RequestMapping(value = "/deleteUploadFile/{fileName}/{userId}", consumes = "text/plain", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String  deleteUploadFileNamesForNote(@RequestBody String params,@PathVariable("fileName") String fileName,@PathVariable("userId") String userId, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("delete uploaded files entered "+userId+" filename is: "+fileName);
		String success = null;
		File f=null;
		try{
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			com.musicnotes.apis.interfaces.MnUploadable videoFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
			success=videoFile.deleteUploadFileNamesForNote(params, fileName, userId);
			
		}catch (Exception e)
		{
			logger.error(" Exception while Deleting File Name : " + e);
			
		}
		if(logger.isInfoEnabled())
		logger.info("uploaded files deleted termination"+success);
		return ""+success;
	}
	
	@RequestMapping(value = "/deleteVideoFiles/{fileName}/{userId}", consumes = "text/plain", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String  deleteVideoFile(@PathVariable("fileName") String fileName,@PathVariable("userId") String userId, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("delete video file entered userId:"+userId+" "+fileName);
		String fileNames = null;
		String success = null;
		try
		{
				com.musicnotes.apis.interfaces.MnUploadable videoFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
				success=videoFile.deleteVideoFileNames(fileName, userId);
				
				if(success!=null && success.contains("success"))
				{
					com.musicnotes.apis.interfaces.MnUploadable audioFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
					String filePath=audioFile.getAllFiles(fileName, Integer.parseInt(userId));
					String name=filePath.substring(filePath.lastIndexOf("/")+1, filePath.length());
					String FullPath=filePath.replace(name, "");
					AmazonUploadable amazonFileDelete=(AmazonUploadable)SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.amazonFileDelete);
					String status=amazonFileDelete.deleteFile(filePath.replaceAll(filePath.substring(filePath.lastIndexOf("/"), filePath.length()), ""), name);
					
				}
		}
		catch (Exception e)
		{
			logger.error(" Exception while Deleting File Name : " + e);
			
		}
		if(logger.isInfoEnabled())
		logger.info("Video File succesfully deleted !!!!!");
		return success;
				
	}
	
	@RequestMapping(value = "/downloadAndroid/{userId}/{fileName}/{extension}", method = RequestMethod.POST)
	public String downloadAndroidFile(@PathVariable("userId") String userId,@PathVariable("fileName") String fileName, @PathVariable("extension") String fileExt ,HttpServletResponse response,HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
		logger.info("Android File download entered "+userId+" filename: "+fileName);
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		try
		{
			String folder = fileExt;
			folder=folder.toLowerCase();
			response.setHeader("Content-Disposition", "attachment;filename="+fileName);
			response.setHeader("Pragma", "private");
			response.addHeader("Cache-Control", "no-transform, max-age=0");
			response.setHeader("Accept-Ranges", "bytes");
			InputStream inputStream =null;
			com.musicnotes.apis.interfaces.MnUploadable audioFile=(MnUpload) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.mnVideo);
			String filePath=audioFile.getAllFiles(fileName, Integer.parseInt(userId));
			if(fileName!=null && (fileName.contains(".mov") || fileName.contains(".avi") || fileName.contains(".wmv") || fileName.contains(".wma")))
			{
				response.setContentType("video/"+folder);
			
				inputStream = new FileInputStream(JavaMessages.downloadUrl+filePath);
			}
			else
			{
				if(folder.equalsIgnoreCase("pdf"))
					response.setContentType("application/pdf");	
				else if(folder.equalsIgnoreCase("txt"))
					response.setContentType("text/plain");
				else if(folder.equalsIgnoreCase("xls"))
					response.setContentType("application/xls");
				else if(folder.equalsIgnoreCase("xlsx"))	
					response.setContentType("application/vnd.ms-excel");
				else if(folder.equalsIgnoreCase("doc"))
					response.setContentType("application/msword");
				else if(folder.equalsIgnoreCase("docx"))
					response.setContentType("application/vnd.ms-word");
				
				inputStream = new FileInputStream(JavaMessages.downloadUrl+filePath);
					
			}
				
			
			byte[] buff = new byte[8000];

			int bytesRead = 0;

			

			while ((bytesRead = inputStream.read(buff)) != -1)
			{
				bao.write(buff, 0, bytesRead);
			}

			byte[] videoBytes = bao.toByteArray();
			response.setContentLength(videoBytes.length);
			ServletOutputStream outStream = response.getOutputStream();
			outStream.write(videoBytes);
			outStream.flush();
			outStream.close();
			
		}
		catch (Exception e)
		{
			logger.error(" Exception while downloading file : " + e);
		}
		if(logger.isInfoEnabled())
		logger.info("File '"+fileName+"' succesfully downloaded !!!!!");
		return bao.toString();
	}
	
	
	
	
}
