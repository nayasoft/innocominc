package com.musicnotes.apis.controllers;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.musicnotes.apis.interfaces.MnFilterable;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.SpringBeans;

public class FilterController {

	MnFilterable mnFilterable;
	Logger logger = Logger.getLogger(FilterController.class);

	public boolean isValidToken(String token, String date,
			HttpServletRequest request) {

		mnFilterable = (MnFilterable) SpringBeans.getBeanFromBeanFactory(
				request, JavaMessages.Spring.MNFILTERS);

		boolean status = mnFilterable.isValidToken(token, date, request);
		return status;
	}

	public boolean isValidTokenForUpload(HttpServletRequest request) {
		boolean status = false;
		logger.info("isValidTokenForUpload");
		mnFilterable = (MnFilterable) SpringBeans.getBeanFromBeanFactory(
				request, JavaMessages.Spring.MNFILTERS);
		if (request.getRequestURI().contains("/Upload/audio/")
				|| request.getRequestURI().contains("/Upload/noteAudioUpload/")) {
			
			String tokens1 = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/") + 1,
					request.getRequestURI().length());
			
			if (tokens1 != null) {
				status = mnFilterable.isValidTokenForUpload(tokens1, "");
			}
		} else if (request.getRequestURI().contains("/Upload/")) {
			String tokens = request.getRequestURI().substring(
					request.getRequestURI().lastIndexOf("/") + 1,
					request.getRequestURI().length());
			if (tokens != null) {
				status = mnFilterable.isValidTokenForUpload(tokens, "");
			}
		} else if (request.getRequestURI().contains("/updateProfileImg/")) {
			logger.debug("updateProfileImg token check called");
			String tokens = request.getRequestURI().substring(
					request.getRequestURI().lastIndexOf("/") + 1,
					request.getRequestURI().length());
			String[] arr = tokens.split(" ");
			if (arr != null) {
				String decodedString = arr[0];
				status = mnFilterable.isValidTokenForUpload(decodedString, "");
			}
			logger.info("updateProfileImg token check status return:::::"+status);
		}else if (request.getRequestURI().contains("Lesson/download")) 
		{
			logger.info("download method called");
			String tokens = request.getRequestURI().substring(
					request.getRequestURI().lastIndexOf("/") + 1,
					request.getRequestURI().length());
			if (tokens != null) {
				status = mnFilterable.isValidTokenForUpload(tokens, "");
			}
		}
		else if (request.getRequestURI().contains("/uploadFiles/")) 
		{
			logger.info("download method called");
			String tokens = request.getRequestURI().substring(
					request.getRequestURI().lastIndexOf("/") + 1,
					request.getRequestURI().length());
			if (tokens != null) {
				status = mnFilterable.isValidTokenForUpload(tokens, "");
			}
		}
		return status;
	}
	
	public boolean isValidTokenForUser(String token, String date,
			HttpServletRequest request) {

		mnFilterable = (MnFilterable) SpringBeans.getBeanFromBeanFactory(
				request, JavaMessages.Spring.MNFILTERS);

		boolean status = mnFilterable.isValidTokenForUser(token, date, request);
		return status;
	}
	
	public boolean isValidTokenForNewGenerate(String token, String date,HttpServletRequest request) 
	{
		mnFilterable = (MnFilterable) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNFILTERS);
		boolean status = mnFilterable.isValidTokenForNewGenerate(token, date, request);
		return status;
	}
}
