package com.musicnotes.apis.controllers;

import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;

import com.musicnotes.apis.domain.MnUserLogs;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.domain.MnUsersToken;
import com.musicnotes.apis.interfaces.MnLoginable;
import com.musicnotes.apis.resources.MnLogin;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.MailContent;
import com.musicnotes.apis.util.SendMail;
import com.musicnotes.apis.util.SpringBeans;

@Controller
@RequestMapping("/login")
public class LoginController extends BaseController
{
	MnLoginable mnLoginable;
	Logger logger = Logger.getLogger(LoginController.class);

	@RequestMapping(value = "/checkuser", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String checkUser(@RequestBody String param, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
			logger.info("Check user entered param:"+param);
		String checkUser = "";
		try
		{
			mnLoginable = (MnLogin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLOGIN);
			param = decodeJsonString(param);
			param = removeUnWantedChar(param);
			MnUsers user = mnLoginable.convertJsonToUserObj(param);
			checkUser = mnLoginable.getUser(user.getUserName(), request);
			
		}
		catch (Exception e)
		{
			checkUser = "fail";
			e.printStackTrace();
		}
		if(logger.isInfoEnabled())
			logger.info("Check user terminated and returned user details"+checkUser);
		return checkUser;
	}

	@RequestMapping(value = "/userlogin", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String checkLogin(@RequestBody String param, HttpServletRequest request)
	{

		if(logger.isInfoEnabled())
			logger.info("checkLogin entered param:"+param);
		String checkUser = "";
		try
		{
			mnLoginable = (MnLogin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLOGIN);
			param = decodeJsonString(param);
			param = removeUnWantedChar(param);
			MnUsers user = mnLoginable.convertJsonToUserObj(param);
			checkUser = mnLoginable.getLogin(user.getEmailId(),user.getPasswordText(), request);
			
		}
		catch (Exception e)
		{
			checkUser = "fail";
			e.printStackTrace();
		}
		if(logger.isInfoEnabled())
			logger.info("checkLogin terminated and userdetails/shared note/expirydate"+checkUser);
		return checkUser;
	}

	@RequestMapping(value = "/resetVerificationCode", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String resetVerificationCode(@RequestBody String param, HttpServletRequest request)
	{

		if(logger.isInfoEnabled())
			logger.info("checkLogin entered param:"+param);
		String checkUser = "";
		try
		{
			mnLoginable = (MnLogin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLOGIN);
			param = decodeJsonString(param);
			param = removeUnWantedChar(param);
			MnUsers user = mnLoginable.convertJsonToUserRObj(param);
			 Random random = new Random();
			 String[] startEndPart={"AZ","BX","CY","DB","EL","FM","GO","HU","IP","JR"};
			 String[] part1={"8j","9f","6y","5r","3h","4k","2d","1v","0o","7x"};
			 String[] part2={"#6","#9","#5","@1","@2","@3","$9","$6","$7","$5"};
			 String[] part3={"R0","#W","K@","$T","VZ","@S","X9","1F","M7","5S"};
			 String randomPass=startEndPart[random.nextInt(10)]+part1[random.nextInt(10)]+part2[random.nextInt(10)]+part3[random.nextInt(10)];
			 user.setMailCheckId(randomPass);
			checkUser = mnLoginable.resetVerificationCode(user);
			
		}
		catch (Exception e)
		{
			checkUser = "fail";
			e.printStackTrace();
		}
		if(logger.isInfoEnabled())
			logger.info("checkLogin terminated and userdetails/shared note/expirydate"+checkUser);
		return checkUser;
	}
	@RequestMapping(value = "/unSubscribe", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String unSubscribeNotification(@RequestBody String param, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
			logger.info("unSubscribeNotification entered param:"+param);
		String checkUser = "";
		try
		{
			mnLoginable = (MnLogin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLOGIN);
			param = decodeJsonString(param);
			param = removeUnWantedChar(param);
			MnUsers user = mnLoginable.convertJsonToUserObj(param);
			checkUser = mnLoginable.unsubcribeNotification(user);
		}
		catch (Exception e)
		{
			checkUser = "fail";
			e.printStackTrace();
		}
		if(logger.isInfoEnabled())
			logger.info("unSubscribeNotification terminated and returned success:"+checkUser);
		return checkUser;
	}
	
	@RequestMapping(value = "/userLoginDetils", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String userLoginDetils(@RequestBody String param, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
			logger.info("userLoginDetils entered param:"+param);
		String checkUser = "";
		try
		{
			mnLoginable = (MnLogin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLOGIN);
			param = decodeJsonString(param);
			param = removeUnWantedChar(param);
			MnUserLogs userLogs = mnLoginable.convertJsonToUserDetailObj(param);
			checkUser = mnLoginable.doUserLoginDetails(userLogs);
		}
		catch (Exception e)
		{
			checkUser = "fail";
			e.printStackTrace();
		}
		if(logger.isInfoEnabled())
			logger.info("userLoginDetils terminated and returned success :"+checkUser);
		return checkUser;
	}
	
	
	
	@RequestMapping(value = "/logintime", consumes = "application/text", produces = "application/text", method = RequestMethod.POST)
	@ResponseBody
	public String checkUserLoginTime(@RequestBody String param, HttpServletRequest request)
	{
		mnLoginable = (MnLogin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLOGIN);
		param=decodeJsonString(param);
		String userId = mnLoginable.loginAuthentication(param,request);
		return userId;
	}
	
	@RequestMapping(value = "/updateLoginCheck", consumes = "application/text", produces = "application/text", method = RequestMethod.POST)
	@ResponseBody
	public String updateLoginCheck(@RequestBody String param, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
			logger.info("updateLoginCheck entered param:"+param);
		String checkUser = "";
		try
		{
			mnLoginable = (MnLogin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLOGIN);
			param = decodeJsonString(param);
			param = removeUnWantedChar(param);
			MnUsers user = mnLoginable.convertJsonToUserObj(param);
			checkUser = mnLoginable.updateLoginCheck(user);
		}
		catch (Exception e)
		{
			checkUser = "fail";
			e.printStackTrace();
		}
		if(logger.isInfoEnabled())
			logger.info("updateLoginCheck terminated and returned success "+checkUser);
		return checkUser;
	}
	
	
	@RequestMapping(value = "/toGen", consumes = "application/text", produces = "application/text", method = RequestMethod.POST)
	@ResponseBody
	public String tokenGenerater(@RequestBody String param, HttpServletRequest request)
	{
		if(logger.isInfoEnabled())
			logger.info("updateLoginCheck entered param:"+param);
		String checkUser = "";
		try
		{
			mnLoginable = (MnLogin) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLOGIN);
			param = decodeJsonString(param);
			param = removeUnWantedChar(param);
			MnUsersToken user = mnLoginable.convertJsonToTokenObj(param);
			checkUser = mnLoginable.toGenerateTokens(user);
		}
		catch (Exception e)
		{
			checkUser = "fail";
			e.printStackTrace();
		}
		if(logger.isInfoEnabled())
			logger.info("updateLoginCheck terminated and returned success "+checkUser);
		return checkUser;
	}
	
}
