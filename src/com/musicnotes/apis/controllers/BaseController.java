package com.musicnotes.apis.controllers;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.musicnotes.apis.domain.MnUsersToken;
import com.musicnotes.apis.util.JavaMessages;

public abstract class BaseController {
	
	Logger logger = Logger.getLogger(BaseController.class);
	public String decodeJsonString(String str)
	{
		if(logger.isDebugEnabled())
		logger.debug("decodeJsonString method called");
		try
		{
			str = java.net.URLDecoder.decode(str, "UTF-8");
		}
		catch (UnsupportedEncodingException e)
		{
			logger.error("Exception in decodeJsonString method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("decodeJsonString method successfully returned");
		return str;
	}

	public String removeUnWantedChar(String str)
	{
		if(logger.isDebugEnabled())
		logger.debug("removeUnWantedChar method called");
		if (str != null && !str.equals(""))
		{
			Character charAt = str.charAt(str.length() - 1);
			if (charAt.toString().equals("="))
			{
				str = str.substring(0, str.length() - 1);
			}
		}
		if(logger.isDebugEnabled())
		logger.debug("removeUnWantedChar method successfully returned");
		return str;
	}
	
	
}
