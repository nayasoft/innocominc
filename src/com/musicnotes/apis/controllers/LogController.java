package com.musicnotes.apis.controllers;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.musicnotes.apis.interfaces.MnLogable;
import com.musicnotes.apis.resources.MnLogs;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.SpringBeans;

@Controller
@RequestMapping("/log")
public class LogController extends BaseController{
	MnLogable mnLogs; 
	Logger logger = Logger.getLogger(LogController.class);
	
	@RequestMapping(value = "/getNotifications/{userId}", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getNotificationsList(@PathVariable ("userId") String userId, HttpServletRequest request)
	{
		String status="";
		
		try{
			if(logger.isInfoEnabled()){logger.info("getNotificationsList method called"+userId);}
			mnLogs = (MnLogs) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLOG);
			status = mnLogs.getNotificationsList(userId);
			if(logger.isInfoEnabled()){logger.info("Successfully got notification list for "+userId);}
		}catch (Exception e) {
			logger.error("Exception while fetching Notification list"+e);
		}
		return status;
	}
	
	
	@RequestMapping(value = "/setNotificationAcceptDecline", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String setNotificationAcceptDecline(@RequestBody String params,HttpServletRequest request)
	{
		String status="";
		
		try{
			if(logger.isInfoEnabled()){logger.info("setNotificationAcceptDecline method called");}
			mnLogs = (MnLogs) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLOG);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			status = mnLogs.setNotificationsAD(params);
			if(logger.isInfoEnabled()){logger.info("Notification Accept/Decline set successfully");}
		}catch (Exception e) {
			logger.error("Exception in setNotificationAcceptDecline method"+e);
		}
		return status;
	}
	
	@RequestMapping(value = "/inactiveNotification/{userId}", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String inactiveNotification(@PathVariable ("userId") String userId,@RequestBody String params, HttpServletRequest request)
	{
		String status="";
		
		try{
			if(logger.isInfoEnabled()){logger.info("inactiveNotification method called"+userId);}
			mnLogs = (MnLogs) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLOG);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			if(logger.isInfoEnabled()){logger.info("Successfully set inactive notification for "+userId);} 
			status = mnLogs.inactivateNotifications(params, userId);
			
		}catch (Exception e) {
			logger.error("Exception in inactiveNotification method"+e);
		}
		return status;
	}
	
	@RequestMapping(value = "/recentActivity/{userId}", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getRecentActivity(@PathVariable ("userId") String userId, HttpServletRequest request)
	{
		String status="";
		
		try{
			if(logger.isInfoEnabled()){logger.info("getRecentActivity method called"+userId);}
			mnLogs = (MnLogs) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLOG);
			if(logger.isInfoEnabled()){logger.info("Successfully got recent Activity for "+userId);}
			status = mnLogs.getRecentActitvity(userId);
			
		}catch (Exception e) {
			logger.error("Exception in getting user activity"+e);
		}
		return status;
	}
	@RequestMapping(value = "/getRemainder/{userId}", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String getRemainder(@PathVariable ("userId") String userId, HttpServletRequest request)
	{
		String status="";
		
		try{
			if(logger.isInfoEnabled()){logger.info("getRemainder method called"+userId);}
			mnLogs = (MnLogs) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLOG);
			if(logger.isInfoEnabled()){logger.info("Successfully got remainder for"+userId);}
			status = mnLogs.getRemainder(userId,request);
			
		}catch (Exception e) {
			logger.error("Exception in getting remainder"+e);
		}
		return status;
	}
	
	
	@RequestMapping(value = "/inactiveRemainders/{userId}", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String inactiveRemainders(@RequestBody String params, @PathVariable ("userId") String userId, HttpServletRequest request)
	{
		String status="";
		
		try{
			if(logger.isInfoEnabled()){logger.info("inactiveRemainders method called"+userId);}
			mnLogs = (MnLogs) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNLOG);
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			if(logger.isInfoEnabled()){logger.info("Got inactiveRemainders for"+userId);}
			status = mnLogs.inactiveRemainders(params,userId);
			
		}catch (Exception e) {
			logger.error("Exception in getting inactive remainders"+e);
		}
		return status;
	}
}
