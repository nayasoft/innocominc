package com.musicnotes.apis.controllers;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.musicnotes.apis.domain.MnGroupDetailsDomain;
import com.musicnotes.apis.domain.MnGroupDomain;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.interfaces.MnGroupable;
import com.musicnotes.apis.interfaces.MnNoteable;
import com.musicnotes.apis.resources.MnGroup;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.MailContent;
import com.musicnotes.apis.util.SendMail;
import com.musicnotes.apis.util.SpringBeans;



@Controller
@RequestMapping("/group")
public class GroupController extends BaseController{
	MnGroupable mnGroup;
	MnNoteable mnNote;
	Logger logger = Logger.getLogger(GroupController.class);

	
	@RequestMapping(value = "/createGroup", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String createGroup(@RequestBody String params, HttpServletRequest request)
	{
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		String status = "";
		try
		{
			if(logger.isInfoEnabled()){logger.info("createGroup method called for group creation");}
			params = decodeJsonString(params);
			params = removeUnWantedChar(params);
			
			MnGroupDomain group = mnGroup.convertJsonToMnGroupDomainObj(params);
		    status = mnGroup.createGroup(group);
		    
			if(status!=null && !status.equalsIgnoreCase("0")){
				
			if(logger.isInfoEnabled()){logger.info("Successfully created group");}	
			}
		}
		catch (Exception e)
		{
			logger.error("Exception while creating Group : ", e);
			status = "0";
		}
		return status;
	}
	
	@RequestMapping(value = "/updateGroup", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String updateGroup(@RequestBody String jsonStr, HttpServletRequest request)
	{
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		String status = "";
		try
		{
			if(logger.isInfoEnabled()){logger.info("updateGroup method called");}
			jsonStr = decodeJsonString(jsonStr);
			jsonStr = removeUnWantedChar(jsonStr);
			MnGroupDomain group = mnGroup.convertJsonToMnGroupDomainObj(jsonStr);
		    status = mnGroup.updateGroup(group);
		    
			if(status!=null && status.equalsIgnoreCase("success")){
				status="200";
				if(logger.isInfoEnabled()){logger.info("Group updated Successfully");}
			}
			else
			{
				status="404";
			
			}


		}
		catch (Exception e)
		{
			logger.error("Exception while updating Group : ", e);
			status = "0";
		}
		return status;
	}
	
	@RequestMapping(value = "/updateGroupDetails", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String updateGroupDetails(@RequestBody String jsonStr, HttpServletRequest request)
	{
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		String status = "";
		try
		{
			if(logger.isInfoEnabled()){logger.info("updateGroupDetails method called");}
			jsonStr = decodeJsonString(jsonStr);
			jsonStr = removeUnWantedChar(jsonStr);
			MnGroupDomain group = mnGroup.convertJsonToMnGroupDomainDetailsObj(jsonStr);
		    status = mnGroup.updateGroupDetails(group);
		    
			if(status!=null && !status.equalsIgnoreCase("0")){
			if(logger.isInfoEnabled()){logger.info("Successfully updated group details: ");}
			}

		}
		catch (Exception e)
		{
			logger.error("Exception while updating Group Details : ", e);
			status = "0";
		}
		return status;
	}
	
	
	@RequestMapping(value = "/deleteGroup", consumes = "text/plain", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public String deleteGroup(@RequestBody String jsonStr, HttpServletRequest request)
	{
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		String status = "";
		try
		{
			if(logger.isInfoEnabled()){logger.info("deleteGroup method called for deleting group");}
			jsonStr = decodeJsonString(jsonStr);
			jsonStr = removeUnWantedChar(jsonStr);
			MnGroupDomain group = mnGroup.convertJsonToMnGroupDomainObj(jsonStr);
		    status = mnGroup.deleteGroup(group);
		    
			if(status!=null && !status.equalsIgnoreCase("0")){
				if(logger.isInfoEnabled()){logger.info("Successfully deleted group");}
			}

		}
		catch (Exception e)
		{
			logger.error("Exception while deleting Group : ", e);
			status = "0";
		}
		return status;
	}

	@RequestMapping(value = "/getGroupInfo",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String fetchGroupInfo(@RequestBody String params, HttpServletRequest request)
   {
		if(logger.isInfoEnabled()){logger.info("fetchGroupInfo method called for fetching group information");}
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		String criteria = null;
		String id = null;
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		MnGroupDomain group = mnGroup.convertJsonToMnGroupDomainObj(params);
		String status = "";
		if(mnGroup.getCriteria().equalsIgnoreCase("loginUserId"))
		{
			criteria="loginUserId";
			id=group.getLoginUserId();
		}
		else if(mnGroup.getCriteria().equalsIgnoreCase("groupId"))
		{
			criteria="groupId";
			id=group.getGroupId().toString();
		}
		
		try {
			status = mnGroup.fetchGroupInfo(criteria,id);
			if(logger.isInfoEnabled()){logger.info("Successfully fetched group information: ");}
		} catch (Exception e) {
			logger.error("Exception while  fetching group details: ", e);
			status = "Exception while  fetching fetching group details";
		}
		return status;
	}

	//Added by r.veeraprathaban for add the group details
	
	@RequestMapping(value = "/addGroupdetails",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String addGroupDetails(@RequestBody String params, HttpServletRequest request)
   {
		String status="";
		try{
		if(logger.isInfoEnabled()){logger.info("addGroupDetails method called");}
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		
		MnGroupDetailsDomain group = mnGroup.convertJsonToMnGroupDomainDetailsObject(params);
		group.setStartDate(new Date().toString());
		group.setStatus("A");
		status=mnGroup.addGroupDetails(group);
		}catch(Exception e){
		logger.error("Exception while adding group details"+e);
		}
		if(logger.isInfoEnabled()){logger.info("Group Details added successfully");}
		return status;
   }
	
	@RequestMapping(value = "/updateMemberGroupDetails",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String updateMemberGroupDetails(@RequestBody String params, HttpServletRequest request)
   {
		String status="";
		try{
		if(logger.isInfoEnabled()){logger.info("updateMemberGroupDetails method called");}
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		MnGroupDetailsDomain group = mnGroup.convertJsonToMnGroupDomainDetailsObject(params);
		
		group.setEndDate(new Date().toString());
		group.setStatus("I");
		status=mnGroup.updateGroupDetails(group);
		if(status!=null){
			if(logger.isInfoEnabled()){logger.info("Member Group Details Updated Successfully");}
		}
		}catch(Exception e){
			logger.error("Exception while updating group member details"+e);
		}
		return status;
   }

	@RequestMapping(value = "/getGroupDetails",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String getGroupDetails(@RequestBody String params, HttpServletRequest request)
   {
		String status="";
		try{
		if(logger.isInfoEnabled()){logger.info("getGroupDetails method called");}	
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		MnGroupDetailsDomain group = mnGroup.convertJsonToMnGroupDomainDetailsObject(params);
		
		status=mnGroup.updateGroupDetails(group);
		if(status!=null){
			if(logger.isInfoEnabled()){logger.info("Successfully fetched group details");}
		}
		}catch(Exception e){
			logger.error("Exception while getting group details"+e);
		}
		return status;
   }
	
	
	@RequestMapping(value = "/getGroupMembers",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String getGroupMembers(@RequestBody String params, HttpServletRequest request)
   {
		String status="";
		try{
			if(logger.isInfoEnabled()){logger.info("getGroupMembers method called");}
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		
		status=mnGroup.getGroupMembers(params);
		}catch(Exception e){
			logger.error("Exception while getting group  members"+e);
		}
		if(status!=null){
			if(logger.isInfoEnabled()){logger.info("Successfully got group members");}
		}
		return status;
   }
	
	@RequestMapping(value = "/getGroupSearch",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String getGroupSearch(@RequestBody String params, HttpServletRequest request)
   {
		String status="";
		try{
		if(logger.isInfoEnabled()){logger.info("getGroupSearch method called");}	
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		
		status=mnGroup.getGroupSearch(params);
		}catch(Exception e){
			logger.error("Exception while getting searched group");
		}
		if(status!=null){
			if(logger.isInfoEnabled()){logger.info("Searched group processed successfully");}
		}
		return status;
   }
	
	@RequestMapping(value = "/getContactSearch",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String getContactSearch(@RequestBody String params, HttpServletRequest request)
   {
		String status="";
		try{
		if(logger.isInfoEnabled()){logger.info("getContactSearch method called");}
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		
		status=mnGroup.getContactSearch(params,request);
		if(status!=null){
			if(logger.isInfoEnabled()){logger.info("searched contacts fetched successfully");}
		}
		}catch(Exception e){
			logger.error("Exception while getting contacts based on search");
		}
		return status;
   }
	
	@RequestMapping(value = "/welcomeEmail",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String welcomeEmail(@RequestBody String params, HttpServletRequest request)
      {
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		String status="";
		String user="";
		String userLastName="";
		String userFirstName="";
		try
		{
		if(logger.isInfoEnabled()){logger.info("welcomeEmail method called");}
		JSONObject jsonObject;
		jsonObject = new JSONObject(params);
		String userEmails=(String) jsonObject.get("inviteUserMail");
		userFirstName=(String) jsonObject.get("userFirstName");
		userLastName=(String) jsonObject.get("userLastName");
		String userMail[]=userEmails.split(",");
		String userId=(String) jsonObject.get("userId");
		String userLevel=(String) jsonObject.get("userLevel");
		String userSelectDate=(String) jsonObject.get("userSelectDate");
		
		//commented below code for beta production
		/*if(userLevel.equalsIgnoreCase("Premium"))
		{
		status=mnGroup.addInvitedUser(Integer.parseInt(userId),userSelectDate,userMail,false);
		status=mnGroup.addFriendRequestForInviteUser(Integer.parseInt(userId),userMail,userFirstName,userLastName);
		
		}else {
			status=mnGroup.addInvitedUser(Integer.parseInt(userId),userSelectDate,userMail,true);
			
			user=mnGroup.addFriendRequestForInviteUser(Integer.parseInt(userId),userMail,userFirstName,userLastName);
			
		}*/
		status=mnGroup.addInvitedUser(Integer.parseInt(userId),userSelectDate,userMail,true);
		
		user=mnGroup.addFriendRequestForInviteUser(Integer.parseInt(userId),userMail,userFirstName,userLastName);
		
		}
		catch (Exception e) {
		logger.error("Exception while sending mail for invited users");
		status="error";
		}
		if(status!=null){
		if(logger.isInfoEnabled()){logger.info("Mail sent to invited users successfully "+status);}
		}
		return status;
   }
	
	
	@RequestMapping(value = "/getGroupNamesForSearch",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String getGroupNamesForSearch(@RequestBody String params, HttpServletRequest request)
   {
		String status="";
		try{
		if(logger.isInfoEnabled()){logger.info("getGroupNamesForSearch method called");}	
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		
		status=mnGroup.getGroupNamesForSearch(params);
		if(status!=null){
		if(logger.isInfoEnabled()){logger.info("Got group names via searching successfully");}	
		}
		}catch(Exception e){
		logger.error("Exception while getting group names for searching"+e);
		}
		return status;
   }
	
	@RequestMapping(value = "/getInvitedUser",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String getInvitedUsers(@RequestBody String params, HttpServletRequest request)
   {
		String status="";
		try{
		if(logger.isInfoEnabled()){logger.info("getInvitedUsers method called");}	
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		status=mnGroup.getInvitedUser(params);
		}catch(Exception e){
		logger.error("Exception while getting invited users");	
		}
		if(status!=null){
		if(logger.isInfoEnabled()){logger.info("Invited users got successfully");}	
		}
		return status;
   }
	
	@RequestMapping(value = "/deletInvite",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String deletInvite(@RequestBody String params, HttpServletRequest request)
   {
		String status="";
		try{
		if(logger.isInfoEnabled()){logger.info("deleteInvite method called");}	
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		status=mnGroup.deletInvite(params);
		if(status!=null){
		if(logger.isInfoEnabled()){logger.info("Invited user deleted successfully");}	
		}
		}catch(Exception e){
		logger.error("Exception while deleting invited users"+e);	
		}
		return status;
   }
	
	@RequestMapping(value = "/updateInviteEmail",  consumes = "application/json", produces = "application/json",  method = RequestMethod.POST)
	@ResponseBody
	public String updateInviteEmail(@RequestBody String params, HttpServletRequest request)
      {
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		String status="";
		String userLastName="";
		String userFirstName="";
		try
		{
		if(logger.isInfoEnabled()){logger.info("updateInviteEmail method called");}	
		JSONObject jsonObject;
		jsonObject = new JSONObject(params);
		String userEmails=(String) jsonObject.get("inviteUserMail");
		String oldEmail=(String) jsonObject.get("oldMailId");
		userFirstName=(String) jsonObject.get("userFirstName");
		userLastName=(String) jsonObject.get("userLastName");
		String userMail[]=userEmails.split(",");
		Integer userId=(Integer) jsonObject.getInt("userId");
		
		String userLevel=(String) jsonObject.get("userLevel");
		
		if(userLevel.equalsIgnoreCase("Premium"))
		{
		status=mnGroup.updateInvitedUser(userId,userMail[0],oldEmail);
		if(status.equals(""))
			status="success";
			if(logger.isInfoEnabled()){logger.info("Invited users updated successfully");}
		}
		
			SendMail sendMail=new SendMail(); 	
			String recipients[]={userMail[0]};
						String message=MailContent.inviteUser+userFirstName+" "+userLastName+MailContent.inviteUser1+" " +
						"</td></tr></tbody></table><br/><br/><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;background:#3b5998;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:2px 6px 4px;border-top:1px solid #8a9cc2\"><a href=\"https://www.musicnoteapp.com/musicnote/index.html\" style=\"color:#c0c0c0;text-decoration:none\" target=\"_blank\"><span style=\"font-weight:bold;white-space:nowrap;color:#fff;font-size:13px\">Accept invite</span></a></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>"+
						MailContent.inviteUser2;
				
					sendMail.postEmail(recipients, userFirstName+" "+userLastName+" has invited you to Musicnote ",message);
		
		}
		catch (Exception e) {
		logger.error("Exception while updating invited users"+e);
		status="error";
		}
			
		return status;
   }
	@RequestMapping(value="/checkPaymentGroup", produces="application/json",consumes="application/json")
	@ResponseBody
	public String checkPaymentGroupForPremiumUsers(@RequestBody String params,HttpServletRequest request){
		String status="";
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		try{
		if(logger.isInfoEnabled()){logger.info("checkPaymentGroupForPremiumUsers method called");}	
		status=mnGroup.checkPaymentGroupForPremiumUsers(params);
		if(logger.isInfoEnabled()){logger.info("payment group checked successfully");}
		}catch(Exception e){
		logger.error("Exception during checking payment group"+e);
		}
		return status;
	}
	@RequestMapping(value="/userInformation", produces="application/json",consumes="application/json")
	@ResponseBody
	public String userInformation(@RequestBody String params,HttpServletRequest request){
		String status="";
		mnGroup = (MnGroup) SpringBeans.getBeanFromBeanFactory(request, JavaMessages.Spring.MNGROUP);
		params = decodeJsonString(params);
		params = removeUnWantedChar(params);
		try{
		if(logger.isInfoEnabled()){logger.info("fetching user information");}	
		status=mnGroup.userInformation(params);	
		if(logger.isInfoEnabled()){logger.info("fetched user information");}
		}catch(Exception e){
			logger.error("Exception while fetching user information"+e);
		}
		return status;
	}
	
}
