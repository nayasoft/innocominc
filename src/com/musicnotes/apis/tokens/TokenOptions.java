package com.musicnotes.apis.tokens;

import java.util.Date;

public class TokenOptions {

	private Date expires;
	private Date notBefore;
	private boolean admin;
	private boolean debug;

	public TokenOptions() {
		expires = null;
		notBefore = null;
		admin = false;
		debug = false;
	}

	public TokenOptions(Date expires, Date notBefore, boolean admin,
			boolean debug) {
		super();
		this.expires = expires;
		this.notBefore = notBefore;
		this.admin = admin;
		this.debug = debug;
	}

	public Date getExpires() {
		return expires;
	}

	public void setExpires(Date expires) {
		this.expires = expires;
	}

	public Date getNotBefore() {
		return notBefore;
	}

	public void setNotBefore(Date notBefore) {
		this.notBefore = notBefore;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

}