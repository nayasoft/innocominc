package com.musicnotes.apis.dao.interfaces;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import com.musicnotes.apis.domain.MnAttachmentDetails;
import com.musicnotes.apis.domain.MnComments;
import com.musicnotes.apis.domain.MnList;
import com.musicnotes.apis.domain.MnLog;
import com.musicnotes.apis.domain.MnNoteDetails;
import com.musicnotes.apis.domain.MnNotesDetails;
import com.musicnotes.apis.domain.MnNotesSharingDetails;
import com.musicnotes.apis.domain.MnRemainders;
import com.musicnotes.apis.domain.MnTag;
import com.musicnotes.apis.domain.MnTagDetails;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.domain.MnVideoFile;
import com.musicnotes.apis.domain.MnVoteViewCount;
import com.musicnotes.apis.domain.MnComplaints;
public interface INoteDao {

	MnList createList(MnList mnList,String listId,String noteId);
	MnList createBook(MnList mnList);
	
	List<MnNoteDetails> fetchList(String userId,String listType);
	
	List<MnList> fetchScheduleList(String userId,String listType);

	List<MnList> fetchSharedNotesBasedOnScheduleList(String userId,String listType);

	List<MnList> fetchGroupSharedNotesBasedOnScheduleList(String userId,String listType);

	List<MnList> fetchEmptyList(String userId,String listType);
	
	List<MnNoteDetails> fetchSharedNotesBasedOnList(String userId,String listType);
	
	List<MnNoteDetails> fetchSharedNotesBasedTag(String userId,String listType,String tagId,String multiFilterBaseNoteName,String multiFilterBaseListId);
	
	List<MnList> fetchNotesBasedDueDate(String userId,String listType,String dueDate);
	
	List<MnNoteDetails> fetchGroupSharedNotesBasedOnList(String userId,String listType);
	
	String createNote(String mnNote,String listId,String userId,String access);
	
	String updateDuedateForNote(String dueDate,String dueTime,String listId,String noteId,String userId,String ownerName);
	
	String updateMembersForNote(String members,String listId,String noteId, String userId, String noteType, String noteLevel);
	
	String updateMembersForBook(String members,String listId, String userId, String noteType, String noteLevel);
	
	String updateAllContactMembersForNote(String userId,String listId,String noteId, String noteType, String fullName, String firstName, String noteName, String noteLevl);
	
	String deleteAllContactMembersForNote(String userId,String listId,String noteId, String noteType);
	
	public String deleteAllContactMembersSharingForSingleUser(String userId, String listId,String noteId, String noteType,String fromUserId);
	
	String updateAllContactMembersForBook(String userId,String listId,String noteType, String fullName, String firstName, String bookName, String noteLevl);
	
	String deleteAllContactMembersForBook(String userId,String listId,String noteType);
	
	String updateTagForNote(String tagId,String listId,String noteId, String userId,String listType);
	
	String updateBookForNote(String bookId,String listId,String noteId, String userId,String listType);
	
	String updateGroupsForNote(String groups,String listId,String noteId,String userId, String noteType, String fullName, String firstName, String noteName, String noteLevl);
	
	String updateGroupsForBook(String groups,String listId,String userId, String noteType, String fullName, String firstName, String bookName, String noteLevl);
	
	String copyNoteToIndividual(List<Integer> members,String listId,String noteId, boolean withComments,String userId);
	
	String copyNoteToAllContact(String userId,String listId,String noteId, boolean withComments, String fullName, String firstName, String noteName);
	
	String copyNoteToGroups(List<Integer> groups,String listId,String noteId, boolean withComments, String userId, String fullName, String firstName, String noteName);

	String updateList(MnList mnList);

	MnList archiveList(String listId,String userId,String sharedType,String listType);
	
	String archiveAllNoteInList(String listId,String userId,String listType);
	
	String archiveNote(String listId,String noteId,String userId,String eventsOrNotes,String shareType,String groupId, String sahreUserId);
	
	String moveNote(String listId,String noteId,String selectedListId,String userId);
	
	String checkListMenuAccess(String noteId,String listId,String userId,String noteOrEvent);
	
	String changeNoteAccess(String listId,String noteId,String access,String userId,String userName,String date);
	
	String copyNote(String listId,String noteId,String selectedListId,String userId,boolean flagForLog);
	
	String copyPublicNote(String listId,String noteId,String userId);
	
	String moveAllNote(String listId,String selectedListId,String userId);
	
	MnList copyList(String listId,String newListName,String userId);
	
	String updateAttachedFileForNote(String fileName, String listId, String noteId, String userId);
	
	String getAttachedFileForNote(String listId, String noteId);
	
	String noteDetails(String listId, String noteId, String type, String userId);
	
	List<MnTag> getTags(String userId);
	
	String createTag(MnTag mnTag);
	
	String updateTag(String userId,String tagName,Integer tagId,String listId,String noteId);
	
	String deleteTag(String userId,Integer tagId);

	String deleteMembersForNote(String userIds, String listId, String noteId,String userId);
	
	String deleteMembersForBook(String groupIds, String listId,String userId);
	
	String updateNoteName(String listId,String noteId,String noteName,String userId, String type); 
	
	String addNoteDescription(String listId,String noteId,String description, String userId); 
	
	String getNote(String listId,String noteId,String userId);
	
	String addComments(MnComments mnComments,String listId,String noteId, String listType);
	
	List<MnComments> getCommentsList(List<Integer> params, String cmtsLevel, String userid);
	
	String getCommentsCountBasedOnNote(String listId,String noteId,String userId);
	
	String updateComments(String cId,String cmts,String cDate,String cTime,String listId,String noteId);

	String deleteComments(String cId, String listId, String noteId, String type);
	
	String castUncastVotes(String params,String listId,String noteId,String userId);
	
	public MnUsers getUserDetails(Integer userId);
	
	public List<MnUsers> getUsersDetails(List<Integer> userIds);

	String deletegroupForNote(String userIds, String listId, String noteId,String userId);
	
	String deletegroupForBook(String groupIds, String listId,String userId);
	
	List<MnList> getList(String userId,String noteAccess);
	
	public String getTimeZone(String timeZoneDate,String userId);
	
	public abstract String insertNewNoteLog(MnLog log);

	public abstract String insertMostViewed(MnVoteViewCount mnVoteViewCount,String userId);

	public abstract String updateMostViewed(MnVoteViewCount mnVoteViewCount,String userId);

	List<MnNotesDetails> getMnMostViewed(String userId,String noteAccess);
	
	List<MnNotesDetails> getSearchMnMostViewed(String searchText,String noteAccess, String userId);
	
	List<MnVoteViewCount> maxMnMostViewed(String noteId,String ListId);
	
	List<MnNotesDetails> getMostViewedDateSearch(String userId,String noteAccess,String date1);
	
	List<MnNotesDetails> getMostVotedDateSearch(String userId,String noteAccess,String date1);
	
	String checkOwnerOfList(String listId,String noteId,String userId);
	
	String changePublicShareWarnMsgFlag(String userId);
	
	public List<MnNoteDetails>  getListBasedOnListId(String userId,String listId,String listType);

	String updatenoteSchedule(String mnNote, String listId, String userId) throws JSONException;
	
	String addRemainder(MnRemainders mnRemainders,String listId,String noteId,Integer userId);
	
	String updateRemainders(String rId,String rNmae,String eventDate,String evetnTime, boolean sendMail,String editDate,String listId,String noteId,String userId);
	
	List<MnRemainders> getRemaindersList(List<Integer> paramsList);
	
	String deleteRemainder(String rId,String listId,String noteId);
	
	List<MnNotesDetails> getMnMostVoted(String userId,String noteAccess);
	
	List<MnNotesDetails> getSearchMnMostVoted(String searchText,String noteAccess, String userId);

	String getVotesBasedNote(String listId,String noteId);

	String listDetails(String listId, String type);
	
	public List<MnNoteDetails> fetchSharingNotesBasedOnListId(String userId ,String listType,String listId);

	public List<MnNotesDetails> getFeatureNotes(String userId);
	
	String bookenableDisable(String userId, String listId);

	String getListNoteNamebyId(String listId,String noteId);

	public List<MnNoteDetails> fetchGroupSharedNotesBasedOnListId(String userId,String listType, String listId);

	String updateCommentLevel(String listId, String noteId, String commentsId);
	
	public MnVideoFile getMnVideoFileDetails(Integer fileId);
	
	public List<MnAttachmentDetails> getAttachedFileForNote(List<Integer> paramsList,String userId);
	
	public List<MnAttachmentDetails> getAttachedFileInNote(String listId,String noteId, String userId);
	
	String searchEngine(String searchingNote);
	
	List<MnNotesDetails> getSearch(String searchingNote, String userId);
	
	public List<MnNoteDetails> byKewordConvertMnListTo(List<MnNoteDetails> mnNoteDetails,String keyWord,String listType );

	public String getBookNames(String userId, String listType);
	
	String createNoteInDefualtList(String mnNote,String listType,String userId,String access,String selectedBookId);
	
	void sahreBasedOnEmailId(String emailIds, String  listId, Integer noteId,String userId,String level, String fullName, String noteName, String noteType);	
	
	List<MnNotesSharingDetails> getIndividualCopyToOwnBook(String userId,String listType, String listId);
	
	List<MnList> getLists(List<Integer> listIds);
	
	List<Integer> getNoteIds(Integer listId,String userId,String listType,String searchingListId);
	
	String getSharedListIds(Integer listId, String userId,String noteId);

	void copyBasedOnEmailId(String nonMailIds, String listId, int parseInt,String userId, String noteLevel, String fullName, String noteName,boolean withComments);
	
	List<MnTag> getAllTags();

	List<MnNotesSharingDetails> fetchSharedNotes(String userId, String listType);
	
	public void createGreetNote(MnNoteDetails mndetail);
	public String complaintReportMail(MnComplaints report);
	
	public String getUserName(String userId);
	
	public List getCommentsListForMail(String listId,String noteId,String userId,String reportLevel,String commentId);
	public String getUserMailId(String string);
	public void sendingMailToFollower(MnUsers mnUsers, String noteName);
	public String getSharedByUserName(String ownerUserId, String listId, String noteId);
	
}
