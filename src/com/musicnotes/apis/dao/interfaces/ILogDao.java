package com.musicnotes.apis.dao.interfaces;

import java.util.List;

import com.musicnotes.apis.domain.MnLog;
import com.musicnotes.apis.domain.MnRemainders;

public interface ILogDao {
	
	List<MnLog> getNotificationsList(String userId);
	
	List<MnLog> getInactiveNotificationsList(String userId);

	String inactivateNotifications(List<Integer> notifiationList,String userId );
	
	List<MnLog> getRecentActitvity(String userId);
	
	List<MnRemainders> getRemainder(String userId);
	
	String inactivateRemainders(List<Integer> notifiationList,String userId );
	
	List<MnRemainders> getinactiveRemainder(String userId);

	String setNotificationsAD(String userId, String noteId, String listId, String logID);
}
