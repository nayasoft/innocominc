package com.musicnotes.apis.dao.interfaces;

import java.util.List;

import com.musicnotes.apis.domain.MnEventDetails;
import com.musicnotes.apis.domain.MnList;
import com.musicnotes.apis.domain.MnNoteDetails;
import com.musicnotes.apis.domain.MnSubEventDetails;
import com.musicnotes.apis.domain.MnUsers;

public interface IEventDao
{
	
String createEvent(MnEventDetails mnEventDetails, String emailFlag);
MnList getMnList(String listId);
List<MnEventDetails> fetchEvents(Integer userId);
List<MnEventDetails> fetchTodayEvents(String startdate, Integer userId,String enddate);
List<MnEventDetails> fetchSharedEventsBasedOnList(Integer userId ,String type);
List<MnEventDetails> fetchGroupSharedEventBasedOnList(Integer userId ,String type);

String deleteEvents(Integer listId,Integer userId);
List<MnList> fetchCalendarSharingMembers(Integer userId,String listType);
List<MnUsers> fetchCalendarSharingGroups(Integer userId,String listType);
List<MnUsers> fetchCalendarSharingContacts(Integer userId,String listType);
List<MnEventDetails> fetchOwnEvents(Integer userId,String listType);
List<MnEventDetails> fetchSharingGroupAndMemberEvents(Integer userId,String listType,Integer SelectedUserId);
List<MnEventDetails> fetchSharingContactEvents(Integer selectedUserId,String listType,Integer userId);
MnEventDetails getEvent(String listId, String noteId,String userId);
String updateEvent(MnEventDetails mnEvents,String emailFlag);

String changeAcceptDecline(String userId, String listId, String noteId,
		String access);

List<MnEventDetails> fetchListsforCalendarCombo(Integer userId,String listType,Integer listId);
List<MnEventDetails> fetchParticularEvent(Integer listId, Integer eventId, Integer userId);
MnEventDetails getEventForMail(String listId, String noteId);
public List<MnEventDetails> getEventsFromList(Integer listId, List<Integer> eventId, Integer userId);
public String updateMnSubEvent(String params);
public String updateMnSingleEvent(String params);
public String fetchParticularEvent(String params);


public String updateAllMnSubEvent(String params);
public String updateSubFollowingEvents(String params);

public String deleteMnSubEvent(String params);

public List<MnSubEventDetails> getSubEventDetails(List<Integer> eventIds);

MnSubEventDetails fetchSubEvents(Integer subEventId,Integer listid,Integer eventId);
MnSubEventDetails getSubEvents(String parms);

	
public List<MnList> byKewordConvertMnListTo(List<MnList> MnListList,String keyWord );

public List<MnList> getListSeachByListId(String userId, String listId);
String getEventMailValues();

public String convertedZoneEvents(Integer userId,Integer ownerId,String date);

}
