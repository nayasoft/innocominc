package com.musicnotes.apis.dao.interfaces;

import java.util.List;

import com.musicnotes.apis.domain.MnFriends;
import com.musicnotes.apis.domain.MnUsers;

public interface IFriendDao
{
	public String addFriendRequest(MnFriends mnUsers);

	public String acceptFriendRequest(MnFriends mnFriends);

	public String declineFriendRequest(MnFriends mnFriends);

	public String addToFriendsList(Integer userId, Integer requestedId);
	
	public List<MnFriends> checkFriendRequest(MnFriends mnFriends);
	
	public MnUsers checkAlreadyFriends(MnFriends mnFriends);
	
	public List<MnFriends> checkAlreadyFriendRequested(Integer userId,Integer requestedId);
	
	public String addFollowers(MnFriends friends);
	
	public String unFollowers(MnUsers mnUsers,MnFriends mnFriends);
	
	public List<MnUsers> getFriendsList(MnUsers mnUsers);
	
	public String getUserDetails(Integer userId);
	
	public String acceptDeclineSharedNoteByMail(Integer userId,String acceptDecline,Integer frdUserId, boolean b, String level, String noteId, String listId);
	
	
	
	public String addFriendInvitedUsers(String userMail,String userId,
			String requestedUserId);

	
}
