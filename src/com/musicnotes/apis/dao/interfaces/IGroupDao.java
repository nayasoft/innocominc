package com.musicnotes.apis.dao.interfaces;

import java.util.List;

import com.musicnotes.apis.domain.MnGroupDetailsDomain;
import com.musicnotes.apis.domain.MnGroupDomain;
import com.musicnotes.apis.domain.MnInvitedUsers;
import com.musicnotes.apis.domain.MnUsers;

public interface IGroupDao {

	String createGroup(MnGroupDomain mnGroupDomain);

	String updateGroup(MnGroupDomain mnGroupDomain);

	String deleteGroup(MnGroupDomain mnGroupDomain);

	List<MnGroupDomain> SearchGroup(String criteria,String id);

	String updateGroupDetails(MnGroupDomain mnGroupDomain);
	
	String addGroupDetails(MnGroupDetailsDomain domain);
	
	String updateGroupDetails(MnGroupDetailsDomain domain);
	
	List<MnGroupDetailsDomain> getGroupDetails(MnGroupDetailsDomain domain);

	String getGroupMembers(String[] group);
	String getGroupSearch(String searchText,Integer userId);
	MnUsers getContactSearch(String searchText);
	
	List<MnGroupDomain> getGroupNamesForSearch(String userId);

	String addInvitedUser(Integer userId, String userSelectDate, String[] userMail,boolean trialCheck);

	List<MnInvitedUsers> getInvitedUser(Integer userId);

	String deletInviteUser(Integer userId, String mailId);

	String updateInvitedUser(Integer userId, String newMail, String oldEmail);
	
	public String checkPaymentGroupForPremiumUsers(String userId);

	String createDefaultGroup(Integer userId);

	String userInformation(String userId);
	
	public MnUsers getExitsEmail(String emailId);

	public String addFriendRequestForInviteUser(String mail,
			String userFirstName, String userLastName);
	
	

}
