package com.musicnotes.apis.dao.interfaces;

import javax.servlet.http.HttpServletRequest;

public interface IFilterDao {
	public boolean isValidToken(String token, String time,
			HttpServletRequest request);

	boolean isValidTokenForUpload(String token, String date);
	boolean isValidTokenForUser(String token, String date,HttpServletRequest request);
	public boolean isValidTokenForNewGenerate(String token, String date,HttpServletRequest request);
}
