package com.musicnotes.apis.dao.interfaces;

import java.util.List;

import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.domain.MnVideoFile;

public interface IUploadDao
{
	public String saveVideoFileName(String userId,String fileName, String fileType, String status,String fullPath,double fileSize);
	public List<MnVideoFile> getVideoFileNames(String userId);
	public boolean deleteVideoFileNames(String fileName,String userId);
	public String saveOtherFileName(String userId,String fileName ,String fileType, String status,String fullPath,double fileSize);
	public String saveOtherFileNameAttachWithNote(String userId,String fileName ,String fileType, String status,String listod,String noteId,String fullPath,double fileSize);
	public String saveOtherFileNameAttachWithNoteAudio(String userId,String fileName ,String fileType, String status,String listod,String noteId,String fullPath,double fileSize);

	List<MnVideoFile> getOtherFileNames(String userId);
	boolean deleteOtherFileNames(String fileName, String userId);
	void commonLog(String fileName, String userId,String label);
	public String deleteUploadFileNamesForNote (String listId,String noteId,String fileName, String userId);
	public List<MnVideoFile> getVideoFileNamesForNote(String userId, String listId, String noteId);
	public String getAllFiles(String fileName,Integer userId);
	public MnUsers limitedUpload(String userId);
	
}
