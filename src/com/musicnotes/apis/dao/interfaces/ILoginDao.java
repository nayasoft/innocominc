package com.musicnotes.apis.dao.interfaces;

import javax.servlet.http.HttpServletRequest;

import com.musicnotes.apis.domain.MnUserLogs;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.domain.MnUsersToken;

public interface ILoginDao
{
	public String CheckLogin(String userName);
	
	public String getLogin(String userName,String password,HttpServletRequest request);
	
	public String unsubcribeNotification(MnUsers mnUsers);
	
	public String doUserLoginDetails(MnUserLogs mnUserLogs);
	
	public String loginAuthentication(String userId,String inTime,String role);

	public String updateLoginCheck(MnUsers mnUsers);

	public String resetVerificationCode(MnUsers user);
	
	public String toGenerateTokens(MnUsersToken mnUsersToken);

}
