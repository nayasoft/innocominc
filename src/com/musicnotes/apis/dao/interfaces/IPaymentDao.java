package com.musicnotes.apis.dao.interfaces;

import com.musicnotes.apis.domain.MnSubscription;

public interface IPaymentDao {

	public String makePayment(MnSubscription subscription, String payUserId, String payOrRenewal);

	public String updatePaidNo(String userId, String paidNo);

	public String getOfferMessageForPayment(String userId);

	public String renewalPayment(MnSubscription subscription, String payUserId);

}
