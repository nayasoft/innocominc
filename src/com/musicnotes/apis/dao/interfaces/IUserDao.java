package com.musicnotes.apis.dao.interfaces;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.musicnotes.apis.domain.MnAdminConfiguration;
import com.musicnotes.apis.domain.MnFeedbacks;
import com.musicnotes.apis.domain.MnFriends;
import com.musicnotes.apis.domain.MnMailConfiguration;
import com.musicnotes.apis.domain.MnMailNotificationList;
import com.musicnotes.apis.domain.MnSecurityQuestions;
import com.musicnotes.apis.domain.MnUsers;

public interface IUserDao
{
public String createUser(MnUsers mnUsers);
public MnUsers getViewUserDetails(String userId);
public List<MnUsers> fetchUserDetails(String roleId);
public MnUsers memberList(Integer userId);
public String updateUser(MnUsers mnUsers);
public List<MnUsers> getAllActiveUserDetailsForSearch(MnUsers user) ;
public MnUsers getExitsEmail(String emailId);
public MnUsers updateEmail(String emailId,String userId);
public String resetPassword(Integer userId, String newPassWord,String oldPassWord);
public String forgetPassword(String emailId);
public void updatePassword(Integer userId ,byte[] passWord) ;
public List<MnUsers> getTeacherDetrails() ;
public MnUsers updateProfilePath(String userId,String filePath,boolean cropFlag);
public MnUsers ExitsUser(String userName);
public MnUsers updateUserName(String userName,String userId);
public Map<String, String> fetchUserName(String recipients, String listType);
public String feedback(MnFeedbacks user);
public String addSubscription(MnUsers mnUsers);
public String getTrailEndMailValues();
public String getTrailcount(String userId);
public String sendMailNotificationsForAutomatically();
public String setUserLevelChanges();
public void inActiveMailconfiguration();
public MnUsers checkInviteuser(MnUsers user);
public String sendUserCreationMail(String userId);
public String addSecurityQuestion(MnSecurityQuestions security);
public MnSecurityQuestions getSecurityQuestion(String userId); 
public List<MnUsers> fetchAddusersDetails(String userId);
public MnSecurityQuestions getSecurityQuestions(String userName);
public String setResetPassword(String userName, String password);
public String addFriendRequestForAddingStudents(MnFriends mnFriends);
public String setResetPass();
public String newToken(HttpServletRequest  request);
public String MnMailNotifications(MnMailNotificationList mailNotify);
public String updateMnMailNotifications(MnMailNotificationList mailNotify);
public Boolean getUserMailNotification(Integer userId, String type);
public MnMailNotificationList fetchMailNotification(String userId);
public MnUsers limitedUpload(String userId);
public String getAdminSecurityQuestion();
public MnUsers ExitsUsers(String userName);
public String sendMailForAddingUser(Map<String, String> userMapForMail, int addedByUserId);
}
