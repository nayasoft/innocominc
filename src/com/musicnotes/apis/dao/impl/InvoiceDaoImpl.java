package com.musicnotes.apis.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.query.Order;
import org.springframework.data.mongodb.core.query.Query;

import com.musicnotes.apis.dao.interfaces.InvoiceDao;
import com.musicnotes.apis.domain.MnInvoices;
import com.musicnotes.apis.util.JavaMessages;

public class InvoiceDaoImpl extends BaseDaoImpl implements InvoiceDao {
	Logger logger = Logger.getLogger(InvoiceDaoImpl.class);

	@Override
	public String createUser(MnInvoices invoices) {
		Query query=null;
		Integer invoiceId = 0;
		try
		{
			query=new Query();
			query.sort().on("_id", Order.ASCENDING);
			List<MnInvoices> listInvoices = mongoOperations.find(query,MnInvoices.class,JavaMessages.Mongo.MNINVOICE);
			logger.info("listInvoices :: " + listInvoices.size());
			if (listInvoices != null && listInvoices.size() != 0)
			{
				MnInvoices lastInvoice = listInvoices.get(listInvoices.size() - 1);
				invoiceId = lastInvoice.getInvoiceId()+1;
			}
			else
			{
				invoiceId = 1000;
			}
			invoices.setInvoiceId(invoiceId);
			mongoOperations.insert(invoices,JavaMessages.Mongo.MNINVOICE);
			return "Success";
		}
		catch (Exception e)
		{
		}
		return "Exception while inserting invoice  details!";
	}

}
