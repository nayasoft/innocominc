package com.musicnotes.apis.dao.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.musicnotes.apis.dao.interfaces.ILogDao;
import com.musicnotes.apis.domain.MnGroupDetailsDomain;
import com.musicnotes.apis.domain.MnLog;
import com.musicnotes.apis.domain.MnNotesSharingDetails;
import com.musicnotes.apis.domain.MnRemainders;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.util.JavaMessages;

public class LogDaoImpl  extends BaseDaoImpl implements ILogDao {
	
	Logger logger = Logger.getLogger(LogDaoImpl.class);
	
	public List<MnLog> getNotificationsList(String userId){
		
		List<MnLog> mnLogs = null;
		String loginUserZone=null;
		List<MnLog> mnLogsNew = new ArrayList<MnLog>();
		
		
		try{
			if(logger.isDebugEnabled()){logger.debug("getNotificationsList method called "+userId);}
			Query query3 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
   			MnUsers mnUser = mongoOperations.findOne(query3, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			loginUserZone=mnUser.getTimeZone();
			
			Query query = new Query(Criteria.where("notUserId").is(Integer.parseInt(userId)).and("logType").is("N").and("status").is("A"));
			mnLogs = mongoOperations.find(query, MnLog.class, JavaMessages.Mongo.MNLOG);
			
			for(MnLog mnLog1:mnLogs)
			{
				Date date=null;
				DateFormat formatter = new SimpleDateFormat("MM/d/yyyy HH:mm:ss");
				TimeZone obj = TimeZone.getTimeZone("GMT"+loginUserZone);
				Calendar c = Calendar.getInstance();
	   			String d=mnLog1.getDate()+" "+mnLog1.getTime();
	   			date=formatter.parse(d);
	   			c.setTime(date);
				formatter.setTimeZone(obj);
				String convertedDate=formatter.format(c.getTime());
				String arr[]=convertedDate.split(" ");
				mnLog1.setDate(arr[0].toString());
				mnLog1.setTime(arr[1].toString());
				
				
				mnLogsNew.add(mnLog1);
				
			}
			if(logger.isDebugEnabled()){logger.debug("Got notifications list");}
						
		}catch (Exception e) {
		
			logger.error("Exception in get Notification in Impl!",e);
			
		}
		return mnLogsNew;
	}
	
	public  List<MnLog> getInactiveNotificationsList(String userId){
		
		List<MnLog> mnLogs = null;
		String loginUserZone=null;
		List<MnLog> mnLogsNew = new ArrayList<MnLog>();
		try{
			// fetch inactive // 
			if(logger.isDebugEnabled()){logger.debug("getinactiveNotificationsList method called "+userId);}
			Query query3 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
   			MnUsers mnUser = mongoOperations.findOne(query3, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			loginUserZone=mnUser.getTimeZone();
			
				Query query2 = new Query(Criteria.where("notUserId").is(Integer.parseInt(userId)).and("logType").is("N").and("status").is("I"));
				mnLogs = mongoOperations.find(query2, MnLog.class, JavaMessages.Mongo.MNLOG);
				
				
				for(MnLog mnLog:mnLogs)
				{
					Date date=null;
					DateFormat formatter = new SimpleDateFormat("MM/d/yyyy HH:mm:ss");
					TimeZone obj = TimeZone.getTimeZone("GMT"+loginUserZone);
					Calendar c = Calendar.getInstance();
		   			String d=mnLog.getDate()+" "+mnLog.getTime();
		   			date=formatter.parse(d);
		   			
		   			c.setTime(date);
					formatter.setTimeZone(obj);
					
					String convertedDate=formatter.format(c.getTime());
					String arr[]=convertedDate.split(" ");
					mnLog.setDate(arr[0].toString());
					mnLog.setTime(arr[1].toString());
					mnLogsNew.add(mnLog);
					
				}
				if(logger.isDebugEnabled()){logger.debug("Got inactive notifications for "+userId);}
		}catch (Exception e) {
			
			logger.error("Exception in get inactive Notification in Impl!",e);
			
		}
		return mnLogsNew;
	}
	public String inactivateNotifications(List<Integer> notifiationList, String userId ){
		String status = "success";
		try{
			if(logger.isDebugEnabled()){logger.debug("inactiveNotifications method called "+userId);}
			Query query = new Query(Criteria.where("notUserId").is(Integer.parseInt(userId)).and("logType").is("N").and("status").is("A").and("logId").in(notifiationList));
			
			Update update = new Update();
			update.set("status", "I");
			
			mongoOperations.updateMulti(query, update,JavaMessages.Mongo.MNLOG );
			if(logger.isDebugEnabled()){logger.debug("got inactive notifications for "+userId);}
		}catch (Exception e) {
			status = "fail";
			logger.error("Exception in inactivate Notification in Impl!",e);
			
		}
		return status;
	}
	
	public List<MnLog> getRecentActitvity(String userId){
		
		List<MnLog> mnLogs = null;
		try{
			if(logger.isDebugEnabled()){logger.debug("getRecentActivity method called "+userId);}
			Query query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("logType").is("A").and("status").is("A"));
			
			mnLogs = mongoOperations.find(query, MnLog.class, JavaMessages.Mongo.MNLOG);
			if(logger.isDebugEnabled()){logger.debug("Got recent Activity for "+userId);}
		}catch (Exception e) {
			
			logger.error("Exception in get Recent Actitvity ",e);
			
		}
		return mnLogs;
	}	
	
	public List<MnRemainders> getRemainder(String userId){
		List<MnRemainders> mnRemainders = null;
		Date currentDate = new Date();
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm  a");
		try{
			if(logger.isDebugEnabled()){logger.debug("getRemainder method called "+userId);}
			Query query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("eventDate").gte(sdf.format(currentDate)).and("status").is("A"));
			mnRemainders = mongoOperations.find(query, MnRemainders.class, JavaMessages.Mongo.MNREMAINDERS);
			
			// fetch sharing...
			query = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("listType").ne("schedule").and("status").is("A").and("sharingGroupId").is(0).and("sharingLevel").ne("individual"));
			List<MnNotesSharingDetails> details = mongoOperations.find(query, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			
			if(details!= null && !details.isEmpty()){
				Query query1 = null;
				for(MnNotesSharingDetails sharingDetails:details){
					query1 = new Query(Criteria.where("listId").is(sharingDetails.getListId().toString()).and("noteId").is(sharingDetails.getNoteId().toString()).and("eventDate").gte(sdf.format(currentDate)).and("status").is("A"));
					MnRemainders remainders = mongoOperations.findOne(query1, MnRemainders.class, JavaMessages.Mongo.MNREMAINDERS);
					if(remainders!= null){
						if(mnRemainders!= null && !mnRemainders.isEmpty()){
							mnRemainders.add(remainders);
						}else{
							mnRemainders = new ArrayList<MnRemainders>();
							mnRemainders.add(remainders);
						}
					}
				}
			}
			//fetchsharing group
			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("status").is("A"));
			List<MnGroupDetailsDomain> mnGroupDetails = mongoOperations.find(query,MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);
			if (mnGroupDetails != null && !mnGroupDetails.isEmpty()) {
				List<Integer> sharingGroupIds =new ArrayList<Integer>();
				for (MnGroupDetailsDomain domain : mnGroupDetails) {
					sharingGroupIds.add(domain.getGroupId());
				}
				if(sharingGroupIds!= null && !sharingGroupIds.isEmpty()){
					query = new Query(Criteria.where("listType").ne("schedule").and("status").is("A").and("sharingGroupId").in(sharingGroupIds).and("sharingLevel").ne("individual"));
					List<MnNotesSharingDetails> gropuDetails = mongoOperations.find(query, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
					if(gropuDetails!= null && !gropuDetails.isEmpty()){
						Query query1 = null;
						for(MnNotesSharingDetails sharingDetails:gropuDetails){
							query1 = new Query(Criteria.where("listId").is(sharingDetails.getListId().toString()).and("noteId").is(sharingDetails.getNoteId().toString()).and("eventDate").gte(sdf.format(currentDate)).and("status").is("A"));
							MnRemainders remainders = mongoOperations.findOne(query1, MnRemainders.class, JavaMessages.Mongo.MNREMAINDERS);
							if(remainders!= null){
								if(mnRemainders!= null && !mnRemainders.isEmpty()){
									mnRemainders.add(remainders);
								}else{
									mnRemainders = new ArrayList<MnRemainders>();
									mnRemainders.add(remainders);
								}
							}
						}
					}
				}
			}
			
			
		}catch (Exception e) {
			
			logger.error("Exception in get Remainderin ",e);
			
		}
		if(logger.isDebugEnabled()){logger.debug("Got Remainders for "+userId);}
		return mnRemainders;
	}
	public String inactivateRemainders(List<Integer> remaindersList,String userId ){
		String status = "success";
		Date currentDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		try{
			if(logger.isDebugEnabled()){logger.debug("inactivateRemainders method called "+userId);}
			Query query = new Query(Criteria.where("eventDate").is(sdf.format(currentDate)).and("status").is("A").and("rId").in(remaindersList));
			
			List<MnRemainders> mnRemaindersList = mongoOperations.find(query, MnRemainders.class,JavaMessages.Mongo.MNREMAINDERS);
			
			List<Integer> inactiveRemainderUserIds=null;
			if(mnRemaindersList!= null && !mnRemaindersList.isEmpty()){
				for(MnRemainders mnRemainders:mnRemaindersList){
					if(mnRemainders.getInactiveRemainderUserId()!=null){
						inactiveRemainderUserIds= mnRemainders.getInactiveRemainderUserId();
						if(!inactiveRemainderUserIds.contains(Integer.parseInt(userId)))
							inactiveRemainderUserIds.add(Integer.parseInt(userId));
					}
					Update update = new Update();
					if(inactiveRemainderUserIds!=null){
						query = new Query(Criteria.where("status").is("A").and("rId").is(mnRemainders.getrId()));
						update.set("inactiveRemainderUserId",inactiveRemainderUserIds);
						mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNREMAINDERS);
					}
				}
			}
			
			
		}catch (Exception e) {
			status = "fail";
			logger.error("Exception in inactivate Notification in Impl!",e);
			
		}
		if(logger.isDebugEnabled()){logger.debug("Got inactive remainders for "+userId);}
		return status;
	}
	public List<MnRemainders> getinactiveRemainder(String userId){
		
		List<MnRemainders> mnRemainders = null;
		
		Date currentDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		
		try{
			if(logger.isDebugEnabled()){logger.debug("getinactiveRemainder method called "+userId);}
			Query query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("eventDate").is(sdf.format(currentDate)).and("status").is("I"));
			
			mnRemainders = mongoOperations.find(query, MnRemainders.class, JavaMessages.Mongo.MNREMAINDERS);
			if(logger.isDebugEnabled()){logger.debug("got inactive remainder for "+userId);}
		}catch (Exception e) {
			
			logger.error("Exception in get inactive Remainderin Impl!",e);
			
		}
		return mnRemainders;
	}

	@Override
	public String setNotificationsAD(String userId, String noteId, String listId, String logID) {
		try{
			if(logger.isDebugEnabled()){logger.debug("setNotificationsAD method called "+userId+" noteId: "+noteId);}
			
			Update update=null;
			Query query = new Query(Criteria.where("logId").is(Integer.parseInt(logID)).and("notUserId").is(Integer.parseInt(userId)).and("noteId").is(noteId).and("listId").is(listId).and("pageName").is("schedule"));
			MnLog mnlog=mongoOperations.findOne(query,MnLog.class,JavaMessages.Mongo.MNLOG);
			if(mnlog!=null && !mnlog.equals(""))
			{
			if(mnlog.getLogDescription().charAt(0)!='X')
			{
			String updated_logdesc="X"+mnlog.getLogDescription();
			update = new Update();
			update.set("logDescription", updated_logdesc);
			mongoOperations.updateMulti(query, update,JavaMessages.Mongo.MNLOG );	
			}
			}
				
			if(logger.isDebugEnabled()){logger.debug("setNotifications Accept/Decline for "+userId);}
			
		}catch(Exception e){
			logger.error("Error while updating log identifier for accept/decline"+e);
		}
		return "success";
	}
}
