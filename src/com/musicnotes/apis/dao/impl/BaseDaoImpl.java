package com.musicnotes.apis.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.musicnotes.apis.domain.MnLog;
import com.musicnotes.apis.domain.MnMailNotificationList;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.util.JavaMessages;

public abstract class BaseDaoImpl
{
	private Logger logger = Logger.getLogger(BaseDaoImpl.class);
	MongoOperations mongoOperations;

	public MongoOperations getMongoOperations()
	{
		return mongoOperations;
	}

	public void setMongoOperations(MongoOperations mongoOperations)
	{
		this.mongoOperations = mongoOperations;
	}
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	
	public void writeLog(Integer userId, String logType, String logDesc, String pageFrom, String listId, String noteId,  Set<Integer> notUserIdList ,String pageName){
		Query query = null;
		int logId = 0 ;
		try{
			Date curresntDate = new Date();
			if(logType.equals("N")){
				for(Integer notUserId: notUserIdList){
					MnLog log = createMnLogClass(userId, logType, logDesc, pageFrom, listId, noteId, notUserId, curresntDate,pageName);
					insertMnLog(log);
				}
			}else{
				MnLog log = createMnLogClass(userId, logType, logDesc, pageFrom, listId, noteId, 0, curresntDate,pageName);
				insertMnLog(log);
			}

		}catch(Exception e){
			logger.error("Exception while write Log in Base impl!"+e);
		}
	}
	
	public int getLogId(){
		int logId = 0;
		try{
			List<MnLog> mnLogList = mongoOperations.findAll(MnLog.class,JavaMessages.Mongo.MNLOG);
			if (mnLogList != null && mnLogList.size() != 0)
			{
				MnLog lastLog = mnLogList.get(mnLogList.size() - 1);
				logId = lastLog.getLogId() + 1;
			}
			else
			{
				logId = 1000;
			}
		}catch (Exception e) {
			logger.error("Exception while get Log id in Base impl!"+e);
		}
		return logId;
	}
	
	public MnLog createMnLogClass(Integer userId, String logType, String logDesc, String pageFrom, String listId, String noteId, Integer notUserId, Date date,String pageName){
		MnLog log = new MnLog();
		try{
			
			log.setLogId(getLogId());
			log.setLogType(logType);
			log.setLogDescription(logDesc);
			log.setUserId(userId);
			log.setPageName(pageName);
			if(listId != null){
				log.setListId(listId);
			}else{
				log.setListId("");
			}
			if(noteId != null ){
				log.setNoteId(noteId);
			}else{
				log.setNoteId("");
			}
			log.setTime(timeFormat.format(date));
			log.setDate(dateFormat.format(date));
			log.setPageType(pageFrom);
			log.setStatus("A");
			if(notUserId != 0 ){
				log.setNotUserId(notUserId);
			}
		}
		catch (Exception e) {
			logger.error("Exception while convert Log object in Base impl!"+e);
		}
		return log;
	}
	public void insertMnLog(MnLog log){
		try{
			mongoOperations.insert(log,JavaMessages.Mongo.MNLOG);
		}catch (Exception e) {
			
		}
	}
	public MnUsers getUserDetailsObject (Integer userId){
		if(logger.isDebugEnabled())
		logger.debug("getUserDetailsObject method called "+userId);
		MnUsers mnUser = null;
		Query query = null;
		try{
			query = new Query(Criteria.where("userId").is(userId));
			mnUser = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			
		}catch (Exception e) {
			logger.error("Exception in getUserDetailsObject impl!"+e.getMessage());
		}
		if(logger.isDebugEnabled())
		logger.debug("getUserDetailsObject method successfully returned");
		return mnUser;
	}
	
	public Boolean getUserMailNotification(Integer userId, String type) {
		MnMailNotificationList  mailNotify=null;
		boolean  mailNotification = false;
		try{
				Query query2=new Query(Criteria.where("userId").is(userId));
				mailNotify = mongoOperations.findOne(query2, MnMailNotificationList.class,JavaMessages.Mongo.MNMAILNOTIFICATIONLIST);
				if(mailNotify!= null && !mailNotify.equals("")){
				if(type!=null && type.matches("music")){	
					mailNotification=mailNotify.isNoteBasisMail();
				}
				else if(type!=null && type.matches("schedule")){
					mailNotification=mailNotify.isEventBasisMail();
				}
				else if(type!=null && type.matches("bill")){
					mailNotification=mailNotify.isMemoBasisMail();
				}
				else if(type!=null && type.matches("contact")){
					mailNotification=mailNotify.isContactBasisMail();
				}
				else if(type!=null && type.matches("crowd")){
					mailNotification=mailNotify.isCrowdBasisMail();
				}
				else if(type!=null && type.matches("duedate")){
					mailNotification=mailNotify.isDueDateMail();
				}
				}
			
		}catch(Exception e){
			logger.error("Exception while inserting MnMailNotifications details :" + e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for MnMailNotifications method : ");
		return mailNotification;
		
	}
	
}
