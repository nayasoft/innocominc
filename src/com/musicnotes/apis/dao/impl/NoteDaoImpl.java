package com.musicnotes.apis.dao.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Order;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.musicnotes.apis.dao.interfaces.INoteDao;
import com.musicnotes.apis.domain.MnAdminVotes;
import com.musicnotes.apis.domain.MnAttachmentDetails;
import com.musicnotes.apis.domain.MnBlockedUsers;
import com.musicnotes.apis.domain.MnComments;
import com.musicnotes.apis.domain.MnComplaints;
import com.musicnotes.apis.domain.MnEventDetails;
import com.musicnotes.apis.domain.MnEventsSharingDetails;
import com.musicnotes.apis.domain.MnGroupDetailsDomain;
import com.musicnotes.apis.domain.MnGroupDomain;
import com.musicnotes.apis.domain.MnList;
import com.musicnotes.apis.domain.MnLog;
import com.musicnotes.apis.domain.MnMailNotificationList;
import com.musicnotes.apis.domain.MnNonUserSharingDetails;
import com.musicnotes.apis.domain.MnNoteDetails;
import com.musicnotes.apis.domain.MnNoteDueDateDetails;
import com.musicnotes.apis.domain.MnNotesDetails;
import com.musicnotes.apis.domain.MnNotesSharingDetails;
import com.musicnotes.apis.domain.MnPublicSharedNoteCopyDetails;
import com.musicnotes.apis.domain.MnRemainders;
import com.musicnotes.apis.domain.MnSubEventDetails;
import com.musicnotes.apis.domain.MnTag;
import com.musicnotes.apis.domain.MnTagDetails;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.domain.MnVideoFile;
import com.musicnotes.apis.domain.MnVoteViewCount;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.MailContent;
import com.musicnotes.apis.util.SendMail;

public class NoteDaoImpl extends BaseDaoImpl implements INoteDao {

	Logger logger = Logger.getLogger(NoteDaoImpl.class);
	DateFormat formatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss a");
	private List<MnList> details;
	
	/* Create List */
	@Override
	public MnList createList(MnList mnList,String listIdIdFromList,String noteIdFromList) {
		Integer listId = 0;
		Query query = null;
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		try {
			if(logger.isDebugEnabled())
			logger.debug("createList method called in Impl listIdIdFromList: "+listIdIdFromList+"  noteIdFromList: "+noteIdFromList);
			
			query = new Query();
			query.sort().on("listId", Order.ASCENDING);
			List<MnList> mnLists = mongoOperations.find(query, MnList.class,
					JavaMessages.Mongo.MNLIST);

			if (mnLists != null && mnLists.size() != 0) {
				MnList lastList = mnLists.get(mnLists.size() - 1);
				listId = lastList.getListId() + 1;

			} else {
				listId = 1;

			}
			mnList.setListId(listId);
			mongoOperations.insert(mnList, JavaMessages.Mongo.MNLIST);
			
			if(listIdIdFromList!=null && !listIdIdFromList.isEmpty() && !listIdIdFromList.equals("0") && noteIdFromList!=null && !noteIdFromList.isEmpty() && !noteIdFromList.equals("0")){
				
				MnNotesSharingDetails sharingDetails = new MnNotesSharingDetails();
				sharingDetails.setUserId(mnList.getUserId());
				sharingDetails.setSharingUserId(mnList.getUserId());
				sharingDetails.setSharingGroupId(0);
				sharingDetails.setListId(Integer.parseInt(listIdIdFromList));
				sharingDetails.setListType(mnList.getListType());
				sharingDetails.setNoteId(Integer.parseInt(noteIdFromList));
				sharingDetails.setSharingLevel("individual");
				sharingDetails.setNoteLevel("edit");
				sharingDetails.setShareAllContactFlag(false);
				sharingDetails.setSharedDate(formatter.format(date));
				sharingDetails.setStatus("A");
				sharingDetails.setSharedBookId(listId);
			
				mongoOperations.insert(sharingDetails,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			}
			MnUsers mnUsers = getUserDetailsObject(mnList.getUserId());
			// log --- activity
			writeLog(mnList.getUserId(), "A", "Added new notebook: <B>" + mnList.getListName()+ "</B> to " + getListTypePage(mnList.getListType()), mnList.getListType(), mnList.getListId().toString(), "", null,  mnList.getListType());
			
			if(logger.isDebugEnabled())
			logger.debug(" List successfully inserted in Impl :" +listId);

			return mnList;
		} catch (Exception e) {
			logger.error("Exception in createList method :"+e.getMessage());
			return null;
		}
	}
	public void createGreetNote(MnNoteDetails mndetail){
		try{
			if(logger.isDebugEnabled())
				logger.debug("createList method called in Impl :");
			
		mongoOperations.insert(mndetail, JavaMessages.Mongo.MNNOTEDETAILS);
		}catch(Exception e){
			logger.error("Exception in createList method "+e.getMessage());
		}
	}
	
	
	
	/* Update List */
	@Override
	public String updateList(MnList mnList) {
		// TODO Auto-generated method stub
		return null;
	}

	/* Create Note */
	@Override
	public String createNote(String mnNote, String listId, String userId,String access) 
	{
		if(logger.isDebugEnabled())
			logger.debug("createNote method called in Impl :"+userId);
		
		try {
			JSONObject jsonObject = new JSONObject();
			Integer size = 0;
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);

			if (mnList.getMnNotesDetails() != null	&& !mnList.getMnNotesDetails().isEmpty()) {
				size = mnList.getMnNotesDetails().size() + 1;
				if (mnNote.contains("\"eventId\":\"\"")) {
					mnNote = mnNote.replace("\"eventId\":\"\"",	"\"eventId\":\"" + size + "\"");
				} else {
					mnNote = mnNote.replace("\"noteId\":\"\"", "\"noteId\":\""+ size + "\"");
				}

				mnList.getMnNotesDetails().add(mnNote);
			} else {
				Set<String> tempNotes = new HashSet<String>();
				size = 1;
				if (mnNote.contains("\"eventId\":\"\"")) {
					mnNote = mnNote.replace("\"eventId\":\"\"",	"\"eventId\":\"" + size + "\"");
				} else {
					mnNote = mnNote.replace("\"noteId\":\"\"", "\"noteId\":\""+ size + "\"");
				}
				tempNotes.add(mnNote);
				mnList.setMnNotesDetails(tempNotes);
			}

			Update update = new Update();
			if(access!=null && !access.isEmpty() && access.equals("public"))
				update.set("noteAccess", access);
			
			update.set("mnNotesDetails", mnList.getMnNotesDetails());
			mongoOperations.updateFirst(query, update,	JavaMessages.Mongo.MNLIST);

			
			// log --- activity
			jsonObject = new JSONObject(mnNote);
			MnUsers mnUsers = getUserDetailsObject(Integer.parseInt(userId));
			if (!jsonObject.has("noteName")) {
				writeLog(Integer.parseInt(userId), "A", "Added new event: <B>"+  changedNoteNameWithDouble((String)jsonObject.get("eventName")) + "</B> to " + getListTypePage(mnList.getListType()), mnList.getListType(), mnList.getListId().toString(),(String) jsonObject.get("eventId"), null, mnList.getListType());
			} else {
				writeLog(Integer.parseInt(userId), "A", "Added new note: <B>"+  changedNoteNameWithDouble((String)jsonObject.get("noteName")) +  "</B> to " + getListTypePage(mnList.getListType()), mnList.getListType(), mnList.getListId().toString(),(String) jsonObject.get("noteId"), null, mnList.getListType());
			}

			if(logger.isDebugEnabled())
				logger.debug(" createNote successfully inserted in Impl noteId :" +size);
			
			if (jsonObject.has("eventId")) {
				return "" + size + "," + mnList.getListName();
			} else {
				return "" + size;
			}
			
		} catch (Exception e) {
			logger.error("Exception while creating Note in createNote:" + e);
		}
		
		

		return "0";
	}

	/*  */
	@Override
	public String updatenoteSchedule(String mnNote, String listId, String userId)throws JSONException {
		Set<String> updateNoteDetails=new TreeSet<String>();
		Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

		String eventId = "";
		String eventName = "";
		String description = "";
		String startDate = "";
		String endDate = "";
		String location="";
		String repeatEvent="";
		String repeatEventStart="";
		String repeatEventEnd="";
		

		String oldeventName = "";
		String olddescription = "";
		String oldstartDate = "";
		String oldendDate = "";
		String oldLocation="";
		String oldRepeatEvent="";
		String oldRepeatEventStart="";
		String oldRepeatEventEnd="";
		MnList mnList=null;
		
		if(logger.isDebugEnabled())
			logger.debug("updatenoteSchedule method called in Impl :"+userId);
		try
		{
		if (mnNote != null && !mnNote.equals("")) {
			JSONObject jsonObject;
			jsonObject = new JSONObject(mnNote);

			eventName = (String) jsonObject.get("eventName");
			if(eventName.indexOf("`*`") != -1){
				eventName = eventName.replace("`*`", "\\\"");
			}
			description = (String) jsonObject.get("description");
			if(description.indexOf("`*`") != -1){
				description = description.replace("`*`", "\\\"");
			}
			startDate = (String) jsonObject.get("startDate");
			endDate = (String) jsonObject.get("endDate");
			eventId = (String) jsonObject.get("eventId");
			location = (String) jsonObject.get("location");
			if(location.indexOf("`*`") != -1){
				location = location.replace("`*`", "\\\"");
			}
			repeatEvent = (String) jsonObject.get("repeatEvent");
			repeatEventStart = (String) jsonObject.get("eventStartDate");
			repeatEventEnd = (String) jsonObject.get("eventEndDate");
		}
		mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
		String tempNoteDetails = null;
		String removeNoteDeatils = null;
		for (String note : mnList.getMnNotesDetails()) {
			if (note.contains("\"eventId\":\"" + eventId + "\"")) {

				JSONObject jsonObject;
				jsonObject = new JSONObject(note);

				oldeventName = (String) jsonObject.get("eventName");
				if(oldeventName.indexOf("\"") != -1){
					oldeventName = oldeventName.replace("\"", "\\\"");
				}
				olddescription = (String) jsonObject.get("description");
				if(olddescription.indexOf("\"") != -1){
					olddescription = olddescription.replace("\"", "\\\"");
				}
				oldstartDate = (String) jsonObject.get("startDate");
				oldendDate = (String) jsonObject.get("endDate");
				oldLocation = (String) jsonObject.get("location");
				if(oldLocation.indexOf("\"") != -1){
					oldLocation = oldLocation.replace("\"", "\\\"");
				}
				oldRepeatEvent = (String) jsonObject.get("repeatEvent");
				oldRepeatEventStart = (String) jsonObject.get("eventStartDate");
				oldRepeatEventEnd = (String) jsonObject.get("eventEndDate");

				removeNoteDeatils = note;

				if (eventName != null) {
					if (note.contains("\"eventName\":\"\""))
						note = note.replace("\"eventName\":\"\"","\"eventName\":\"" + eventName + "\"");
					else
						note = note.replace("\"eventName\":\"" + oldeventName+ "\"", "\"eventName\":\"" + eventName + "\"");

				}

				if (description != null) {
					if (note.contains("\"description\":\"\""))
						note = note.replace("\"description\":\"\"","\"description\":\"" + description + "\"");
					else
						note = note.replace("\"description\":\""+ olddescription + "\"", "\"description\":\""+ description + "\"");

				}
				if (startDate != null) {
					if (note.contains("\"startDate\":\"\""))
						note = note.replace("\"startDate\":\"\"","\"startDate\":\"" + startDate + "\"");
					else
						note = note.replace("\"startDate\":\"" + oldstartDate+ "\"", "\"startDate\":\"" + startDate + "\"");

				}
				if (endDate != null) {
					if (note.contains("\"endDate\":\"\""))
						note = note.replace("\"endDate\":\"\"",	"\"endDate\":\"" + endDate + "\"");
					else
						note = note.replace("\"endDate\":\"" + oldendDate+ "\"", "\"endDate\":\"" + endDate + "\"");

				}
				if (location != null) {
					if (note.contains("\"location\":\"\""))
						note = note.replace("\"location\":\"\"",	"\"location\":\"" + location + "\"");
					else
						note = note.replace("\"location\":\"" + oldLocation+ "\"", "\"location\":\"" + location + "\"");

				}
				if (repeatEvent != null) {
					if (note.contains("\"repeatEvent\":\"\""))
						note = note.replace("\"repeatEvent\":\"\"",	"\"repeatEvent\":\"" + repeatEvent + "\"");
					else
						note = note.replace("\"repeatEvent\":\"" + oldRepeatEvent+ "\"", "\"repeatEvent\":\"" + repeatEvent + "\"");
				}
				
					if (note.contains("\"eventStartDate\":\"\""))
						note = note.replace("\"eventStartDate\":\"\"",	"\"eventStartDate\":\"" + repeatEventStart + "\"");
					else
						note = note.replace("\"eventStartDate\":\"" + oldRepeatEventStart+ "\"", "\"eventStartDate\":\"" + repeatEventStart + "\"");
				
					if (note.contains("\"eventEndDate\":\"\""))
						note = note.replace("\"eventEndDate\":\"\"",	"\"eventEndDate\":\"" + repeatEventEnd + "\"");
					else
						note = note.replace("\"eventEndDate\":\"" + oldRepeatEventEnd+ "\"", "\"eventEndDate\":\"" + repeatEventEnd + "\"");

			
				tempNoteDetails = note;
			}
			updateNoteDetails.add(note);
		}

		if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
			mnList.getMnNotesDetails().remove(removeNoteDeatils);

		if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
			mnList.getMnNotesDetails().add(tempNoteDetails);

		Update update = new Update();
		update.set("mnNotesDetails", updateNoteDetails);
		mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNLIST);
		
		}catch (Exception e) {
			logger.error(" Error updatenoteSchedule "+e.getMessage());
		}
		
		if(logger.isDebugEnabled())
			logger.debug(" updatenoteSchedule method returened successfully " );
		return mnList.getListName();

	}

	/* Update Due Date For Particular Note */
	@Override
	public String updateDuedateForNote(String dueDate, String dueTime,String listId, String noteId, String userId,String ownerName) 
	{
		if(logger.isDebugEnabled())
			logger.debug("updateDuedateForNote method called in Impl :"+userId+" dueDate: "+dueDate);
		try {
			Integer ownerId=null;
			MnNoteDueDateDetails details = new MnNoteDueDateDetails();
			String size = "";
			String duedateAction = "";
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			Set<String> updateNoteDetails=new TreeSet<String>();
			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			JSONObject jsonObject;
			ownerId=mnList.getUserId();
			
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					removeNoteDeatils = note;
					if (dueDate != null && !dueDate.equals("")) {
						//duedateAction = "A";
						if (note.contains("\"dueDate\":\"\"")) {
							duedateAction = "A";
							size = "added~" + mnList.getListName() + "~"+ mnList.getListType() + "~";
							note = note.replace("\"dueDate\":\"\"","\"dueDate\":\"" + dueDate + "\"");
						} else {
							duedateAction = "U";
							size = "updated~" + mnList.getListName() + "~"+ mnList.getListType() + "~";
							note = note.replace(noteDueDateConvertToJsonObject(note),"\"dueDate\":\"" + dueDate + "\"");
						}
					} else {
						duedateAction = "R";
						note = note.replace(noteDueDateConvertToJsonObject(note),"\"dueDate\":\"\"");
					}

					
					if (dueTime != "") {
						if (note.contains("\"dueTime\":\"\"")) {
							note = note.replace("\"dueTime\":\"\"","\"dueTime\":\"" + dueTime + "\"");
						} else {
							note = note.replace(noteDueTimeConvertToJsonObject(note),"\"dueTime\":\"" + dueTime + "\"");
						}
					} else {
						note = note.replace(noteDueTimeConvertToJsonObject(note),"\"dueTime\":\"\"");
					}
					tempNoteDetails = note;
				}
				updateNoteDetails.add(note);
			}
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);
			Date date1 = null;
			if(!dueDate.equals("")){
				date1 = format.parse(dueDate);
			}
			
			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			
			// Update Due date Details In Note Details Table
			query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)));
			update = new Update();
			update.set("dueDate", dueDate);
			update.set("dueTime", dueTime);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNNOTEDETAILS);
			
			if (!dueDate.equals("")){
				commomLog(tempNoteDetails, mnList, "Updated due date for", true,userId,false,false,true,": <B>"+dateFormat.format(date1)+" "+dueTime+"</B>",false,"");
			}else
				commomLog(tempNoteDetails, mnList, "Removed due date for", true,userId,false,false,true,"",false,"");

			if (duedateAction.equals("A")) 
			{
				query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("listType").is(	mnList.getListType()).and("noteId").is(Integer.parseInt(noteId)).and("status").is("I"));
				List<MnNoteDueDateDetails> mnNoteDueDateDetails=mongoOperations.find(query, MnNoteDueDateDetails.class, JavaMessages.Mongo.MNNOTEDUEDATEDETAILS);
				
				if(mnNoteDueDateDetails!=null && !mnNoteDueDateDetails.isEmpty())
				{
					Update updates = new Update();
					updates.set("dueDate",dueDate);
					updates.set("status","A");
					mongoOperations.updateFirst(query, updates,	JavaMessages.Mongo.MNNOTEDUEDATEDETAILS);
				}
				else
				{
				details.setUserId(Integer.parseInt(userId));
				details.setListId(Integer.parseInt(listId));
				details.setNoteId(Integer.parseInt(noteId));
				details.setListType(mnList.getListType());
				details.setDueDate(dueDate);
				details.setStatus("A");
				mongoOperations.insert(details,	JavaMessages.Mongo.MNNOTEDUEDATEDETAILS);
				}
			}
			else if(duedateAction.equals("U"))
			{
				
				query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("listType").is(	mnList.getListType()).and("noteId").is(Integer.parseInt(noteId)));
				MnNoteDueDateDetails mnNoteDueDateDetails=mongoOperations.findOne(query, MnNoteDueDateDetails.class, JavaMessages.Mongo.MNNOTEDUEDATEDETAILS);
				
				if(mnNoteDueDateDetails!=null)
				{
					Update updates = new Update();
					updates.set("dueDate",dueDate);
					mongoOperations.updateFirst(query, updates,	JavaMessages.Mongo.MNNOTEDUEDATEDETAILS);
				}
				
			} else {
				query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("listType").is(	mnList.getListType()).and("noteId").is(Integer.parseInt(noteId)));		
				Update updates = new Update();
				updates.set("status", "I");
				mongoOperations.updateFirst(query, updates,	JavaMessages.Mongo.MNNOTEDUEDATEDETAILS);
			}


			jsonObject = new JSONObject(tempNoteDetails);
			String notesMembers = (String) jsonObject.get("notesMembers");
			String notegroups = (String) jsonObject.get("noteGroups");
			Integer noteAllcontact = (Integer) jsonObject.get("noteSharedAllContact");
			
			List<Integer> groupUser = new ArrayList<Integer>();
			Set<Integer> groupUserList=new HashSet<Integer>();
			
			if(notesMembers !=null && !notesMembers.equals(""))
			{
				notesMembers= notesMembers.replace("[","");
				notesMembers= notesMembers.replace("]","");
				
				String[] notesMembersArray =notesMembers.split(",");
				for(int i =0;i<notesMembersArray.length; i++){
					groupUserList.add(Integer.parseInt(notesMembersArray[i]));
				}
			}
			
			
			if(notegroups !=null && !notegroups.equals(""))
			{
				notegroups= notegroups.replace("[","");
				notegroups= notegroups.replace("]","");
				
				String[] groupIds = notegroups.split(",");
				if (groupIds.length != 0) {
					for (int i = 0; i < groupIds.length; i++) {
						groupUser = getUserIdBasedOnGroupId(Integer.parseInt(groupIds[i]));
						if(groupUser!=null && !groupUser.isEmpty())
							groupUserList.addAll(groupUser);
					}
			}
			}
			
			
			if(noteAllcontact !=null && noteAllcontact.equals("1"))
			{
			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
			MnUsers mnUsers = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			if (mnUsers.getFriends() != null && !mnUsers.getFriends().isEmpty()) 
			{
				for (Integer id : mnUsers.getFriends()) {
					try {
						groupUserList.add(id);
					} catch (Exception e) {

					}
				}
			}
			
			}
			
			if(logger.isDebugEnabled())
				logger.debug(" updateDuedateForNote method returened successfully " );
			String changes="" + size +groupUserList+"~"+tempNoteDetails;
			

			String[] splitStatus= changes.split("~"); 	
			
			String notesMemberss=splitStatus[3];
			
			jsonObject = new JSONObject(splitStatus[4]);
			
			String duedate=(String) jsonObject.get("dueDate");
			String duetime=(String) jsonObject.get("dueTime");
			String noteName=(String) jsonObject.get("noteName");
			
			SimpleDateFormat sdf= new SimpleDateFormat("MM/dd/yyyy");
			notesMemberss= notesMemberss.replace("[","");
			notesMemberss= notesMemberss.replace("]","");
			
			if(notesMemberss!=null && notesMemberss.trim()!=" " && !notesMemberss.isEmpty())
			{
				String[] notesMembersArray =notesMemberss.split(","); 
				
				if(notesMembersArray.length>0 && notesMembersArray[0]!= null){

					String label = "";
					if(duedate == ""){
						label = "removed duedate for the note '"+noteName+"' in "+splitStatus[1]+" on "+splitStatus[2] +" book"; 
					}else{
						Date date = (Date)sdf.parse(duedate);
						label = ownerName+" was "+splitStatus[0]+" due date on your note,"+noteName+": \""+sdf.format(date)+" "+duetime +"\"";
					}
					MnUsers mnUsers=getUserDetails(Integer.parseInt(userId));
					
					if(mnUsers!=null ){
						String userName = mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName();
						
						if(duedate == ""){
							label = "removed duedate for the note '"+noteName+"' in "+splitStatus[1]+" on "+splitStatus[2] +" book"; 
						}else{
							Date date = (Date)sdf.parse(duedate);
							label = userName+" was "+splitStatus[0]+" due date on your note,"+noteName+": \""+sdf.format(date)+" "+duetime +"\"";
						}
						
						List<Integer> usersIds = new ArrayList<Integer>();
						
						for(int i =0;i<notesMembersArray.length; i++){
							usersIds.add(Integer.parseInt(notesMembersArray[i].trim()));
						}
						 
						usersIds.add(ownerId);
						
						List<MnUsers> list = getUsersDetails(usersIds);
						if (list != null && !list.isEmpty())
						{
							for (MnUsers users : list)
							{
								try{
									if(!users.getUserId().equals(userId))
									{
									if(users.getNotificationFlag().equalsIgnoreCase("yes"))
									{
										boolean mailAlertEnable=getUserMailNotification(users.getUserId(),"duedate");
										if(mailAlertEnable){
									SendMail sendMail=new SendMail(); 	
									String recipients[]={users.getEmailId()};
									
									String message=MailContent.sharingNotification+users.getUserFirstName()+" "+users.getUserLastName()+MailContent.sharingNotification1+label+" "+MailContent.sharingNotification2;
									if(splitStatus[0].equalsIgnoreCase("added"))
										sendMail.postEmail(recipients, "Due date added to your note",message);
									else
										sendMail.postEmail(recipients, "Due date edited on your note",message);
									}
									}
									}
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}
					}
				}
			}
		
			
			return "" + size +groupUserList+"~"+tempNoteDetails;
		} catch (Exception e) {
			logger.error("Exception while Updating Due date For Note :" + e);
		}
		return "Exception while Updating Due date For Note";
	}

	/* Note DueDate Convert To Json Object */
	public String noteDueDateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("noteDueDateConvertToJsonObject method called in Impl :");
		String dueDate = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String dueDates = (String) jsonObject.get("dueDate");

				dueDate = "\"dueDate\":\"" + dueDates + "\"";
			} catch (Exception e) {
				logger.error("Exception while converting json to Note Duedate object In Impl !",e);

			}
		}
		if(logger.isDebugEnabled())
			logger.debug(" noteDueDateConvertToJsonObject method returened successfully " );
		return dueDate;
	}

	/* Note Access Convert To Json Object */
	public String noteAccessConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("noteAccessConvertToJsonObject method called in Impl :");
		String noteAccess = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String noteAccesss = (String) jsonObject.get("access");

				noteAccess = "\"access\":\"" + noteAccesss + "\"";
			} catch (Exception e) {
				logger.error("Exception while noteAccessConvertToJsonObject In Impl !",e);

			}
		}
		if(logger.isDebugEnabled())
			logger.debug(" noteAccessConvertToJsonObject method returened successfully " );
		return noteAccess;
	}
	
	/* Note Access Convert To Json Object */
	public String notePublicAccessConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("notePublicAccessConvertToJsonObject method called in Impl :");
		String noteAccess = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String publicUser = (String) jsonObject.get("publicUser");

				noteAccess = "\"publicUser\":\"" + publicUser + "\"";
			} catch (Exception e) {
				logger.error("Exception while notePublicAccessConvertToJsonObject In Impl !",e);

			}
		}
		if(logger.isDebugEnabled())
			logger.debug(" notePublicAccessConvertToJsonObject method returened successfully " );
		return noteAccess;
	}
	public String notePublicDateAccessConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("notePublicDateAccessConvertToJsonObject method called in Impl :");
		String noteAccess = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String publicDate = (String) jsonObject.get("publicDate");

				noteAccess = "\"publicDate\":\"" + publicDate + "\"";
			} catch (Exception e) {
				logger.error("Exception while notePublicDateAccessConvertToJsonObject In Impl !",e);

			}
		}
		if(logger.isDebugEnabled())
			logger.debug(" notePublicDateAccessConvertToJsonObject method returened successfully " );
		return noteAccess;
	}

	/* Note DueTime Convert To Json Object */
	public String noteDueTimeConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("noteDueTimeConvertToJsonObject method called in Impl :");
		String dueTime = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String dueTimes = (String) jsonObject.get("dueTime");

				dueTime = "\"dueTime\":\"" + dueTimes + "\"";
			} catch (Exception e) {
				logger.error("Exception while noteDueTimeConvertToJsonObject In Impl !",e);

			}
		}
		return dueTime;
	}

	/* Sharing Members For Note */
	@Override
	public String updateMembersForNote(String members, String listId,String noteId, String fromUserId, String noteType, String noteLevel) {
		if(logger.isDebugEnabled())
			logger.debug("updateMembersForNote method called in Impl fromUserId: "+fromUserId+" noteLevel: "+noteLevel);
		
		List<MnNotesSharingDetails> details = new ArrayList<MnNotesSharingDetails>();
		List<MnEventsSharingDetails> eventSharing = new ArrayList<MnEventsSharingDetails>();
		Integer userId;
		Date date = new Date();
		String newMembers = "";
		String removeMembersBasedOnOldMembers = "";
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		Set<Integer> notUserIdSet = new HashSet<Integer>();
		Set<String> updateNoteDetails=new TreeSet<String>();
		try {
			Integer size = 0;
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			userId = Integer.parseInt(fromUserId);
			newMembers = members;

			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			for (String note : mnList.getMnNotesDetails()) {

				if (noteType.equalsIgnoreCase("Schedule")) {
					if (note.contains("\"eventId\":\"" + noteId + "\"")) {
						removeNoteDeatils = note;

						if (note.contains("\"eventMembers\":\"\"")) {
							note = note.replace("\"eventMembers\":\"\"","\"eventMembers\":\"" + members + "\"");
						} else {
							removeMembersBasedOnOldMembers = note;
							String oldMemberIds = notesSharingDetailsConvertToJsonObject(removeMembersBasedOnOldMembers); 
							if(oldMemberIds!=null && !oldMemberIds.isEmpty()){
								oldMemberIds=oldMemberIds+","+members;
								oldMemberIds = "[" + oldMemberIds + "]";
								note = note.replace(eventMembersUpdateConvertToJsonObject(note),"\"eventMembers\":\"" + oldMemberIds+ "\"");
							}else{
								note = note.replace(eventMembersUpdateConvertToJsonObject(note),"\"eventMembers\":\"" + members+ "\"");
							}
							
						}
						tempNoteDetails = note;
					}

				} else {
					if (note.contains("\"noteId\":\"" + noteId + "\"")) {
						removeNoteDeatils = note;

						if (note.contains("\"notesMembers\":\"\"")) {
							note = note.replace("\"notesMembers\":\"\"","\"notesMembers\":\"" + members + "\"");
						} else {
							removeMembersBasedOnOldMembers = note;
							String oldMemberIds = notesSharingDetailsConvertToJsonObject(removeMembersBasedOnOldMembers); 
							if(oldMemberIds!=null && !oldMemberIds.isEmpty()){
								oldMemberIds=oldMemberIds+","+members;
								oldMemberIds = "[" + oldMemberIds + "]";
								note = note.replace(notesMembersUpdateConvertToJsonObject(note),"\"notesMembers\":\"" + oldMemberIds+ "\"");
							}else{
								note = note.replace(notesMembersUpdateConvertToJsonObject(note),"\"notesMembers\":\"" + members+ "\"");
							}
						}
						tempNoteDetails = note;
					}
				}
				updateNoteDetails.add(note);
			}
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			
			String oldMember = "";
			if (!removeMembersBasedOnOldMembers.isEmpty()) {
				oldMember = notesSharingDetailsConvertToJsonObject(removeMembersBasedOnOldMembers);
			}
			String[] memberIds = newMembers.split(",");
			if (memberIds.length != 0) {
				for (int i = 0; i < memberIds.length; i++) {
					if (!oldMember.contains(memberIds[i])) {
						try {
							notUserIdSet.add(Integer.parseInt(memberIds[i]));
						} catch (Exception e) {

						}
						
						if(mnList.getUserId()!=Integer.parseInt(memberIds[i]))
						{
						MnNotesSharingDetails sharingDetails = new MnNotesSharingDetails();
						sharingDetails.setUserId(userId);
						sharingDetails.setSharingUserId(Integer.parseInt(memberIds[i]));
						sharingDetails.setSharingGroupId(0);
						sharingDetails.setListId(Integer.parseInt(listId));
						sharingDetails.setListType(mnList.getListType());
						sharingDetails.setNoteId(Integer.parseInt(noteId));
						sharingDetails.setSharingLevel("note");
						sharingDetails.setNoteLevel(noteLevel);
						sharingDetails.setShareAllContactFlag(false);
						sharingDetails.setSharedDate(formatter.format(date));
						sharingDetails.setStatus("A");
						details.add(sharingDetails);
						}
					}else{
						try {
							notUserIdSet.add(Integer.parseInt(memberIds[i]));
						} catch (Exception e) {

						}
					}
				}
			} else if (newMembers != null && !newMembers.isEmpty()) {
				if (!oldMember.contains(newMembers)) {
					MnNotesSharingDetails sharingDetails = new MnNotesSharingDetails();
					try {
						notUserIdSet.add(Integer.parseInt(newMembers));
					} catch (Exception e) {

					}
					if(mnList.getUserId()!=Integer.parseInt(newMembers))
					{
					sharingDetails.setUserId(userId);
					sharingDetails.setSharingUserId(Integer.parseInt(newMembers));
					sharingDetails.setSharingGroupId(0);
					sharingDetails.setListId(Integer.parseInt(listId));
					sharingDetails.setListType(mnList.getListType());
					sharingDetails.setNoteId(Integer.parseInt(noteId));
					sharingDetails.setSharingLevel("note");
					sharingDetails.setNoteLevel(noteLevel);
					sharingDetails.setShareAllContactFlag(false);
					sharingDetails.setSharedDate(formatter.format(date));
					sharingDetails.setStatus("A");
					details.add(sharingDetails);
					}
				}
			}

			if (details != null && !details.isEmpty()) {
				for (MnNotesSharingDetails det : details) {
					if (det.getListType().equalsIgnoreCase("schedule")) {
						MnEventsSharingDetails share = new MnEventsSharingDetails();
						share.setUserId(det.getUserId());
						share.setSharingUserId(det.getSharingUserId());
						share.setListId(det.getListId());
						share.setNoteId(det.getNoteId());
						share.setListType(det.getListType());
						share.setSharingGroupId(det.getSharingGroupId());
						share.setSharingLevel("note");
						share.setNoteLevel(noteLevel);
						share.setShareAllContactFlag(false);
						share.setStatus(det.getStatus());
						share.setSharingStatus("P");
						if (eventSharing != null)
							eventSharing.add(share);
					}

				}
				insertNotesSharingDetails(details, eventSharing);

			}
			
			//Sharing Members Details Entry Added In Mn_NoteDetails Table Also - Venu
			if (details != null && !details.isEmpty() && !noteType.equalsIgnoreCase("Schedule")) {
				List<Integer> memIds=new ArrayList<Integer>();
				for (MnNotesSharingDetails det : details) {
					if(!memIds.contains(det.getSharingUserId()))
							memIds.add(det.getSharingUserId());
				}
				if(memIds!=null && !memIds.isEmpty()){
					query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
					MnNoteDetails noteDetails= mongoOperations.findOne(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
					if(noteDetails.getNotesMembers()!=null)
						noteDetails.getNotesMembers().addAll(memIds);
					else
						noteDetails.setNotesMembers(memIds);
					update = new Update();
					update.set("notesMembers", noteDetails.getNotesMembers());
					mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNNOTEDETAILS);
				}
			}
			//End - Venu

			// add activity
			String addUser = "", addUserName = "";
			if (notUserIdSet != null) {
				for (Integer integer : notUserIdSet) {
					MnUsers mnUsers = getUserDetailsObject(integer);
					if (mnUsers != null) {
						addUser = addUser + " <B>" + mnUsers.getUserFirstName()+" " + mnUsers.getUserLastName()+ "</B>,";
						addUserName = addUserName + " <B>"+ mnUsers.getUserFirstName()+" " + mnUsers.getUserLastName()+ "</B>,";
					}
				}
			}
			addUser = addUser.substring(0, addUser.lastIndexOf(","));
			addUserName = addUserName.substring(0, addUserName.lastIndexOf(","));
			// notification activity
			Set<Integer> addedNotiUserIdSet = new HashSet<Integer>();
			String[] oldMemberIds = oldMember.split(",");
			for (int i = 0; i < oldMemberIds.length; i++) {
				if (!newMembers.contains(oldMemberIds[i])) {
					try {
						addedNotiUserIdSet.add(Integer.parseInt(oldMemberIds[i]));
					} catch (Exception e) {

					}
				}
			}
			if(addUserName.length() > 36){
				addUserName = addUserName.substring(0,35)+"...";
			}
			if(addUser.length() > 36){
				addUser = addUser.substring(0,35)+"...";
			}
			// log --- activity
			JSONObject jsonObject = new JSONObject(tempNoteDetails);
			MnUsers mnUsers = getUserDetailsObject(userId);
			
			if (addedNotiUserIdSet != null && !addedNotiUserIdSet.isEmpty()) {
				addedNotiUserIdSet.remove(userId);
				if(!userId.equals(mnList.getUserId())){
					addedNotiUserIdSet.add(mnList.getUserId());
				}
			}else{
				if(!userId.equals(mnList.getUserId())){
					addedNotiUserIdSet.add(mnList.getUserId());
				}
			}
			
			if (noteType.equalsIgnoreCase("Schedule")) {
				
				if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
					writeLog(userId, "A", "Shared a "+ getListType(mnList.getListType())+": <B>"+  changedNoteNameWithDouble((String)jsonObject.get("eventName")) + "</B> with " + addUser,mnList.getListType(), mnList.getListId().toString(),(String) jsonObject.get("eventId"), null, mnList.getListType());
				}

				if (addedNotiUserIdSet != null && !addedNotiUserIdSet.isEmpty()) {
					writeLog(userId, "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> Shared a "+ getListType(mnList.getListType())+": <B>"+  changedNoteNameWithDouble((String)jsonObject.get("eventName")) + "</B> with <B>"+ addUserName + "</B>", mnList.getListType(),mnList.getListId().toString(), (String) jsonObject	.get("eventId"), addedNotiUserIdSet, mnList.getListType());
				}
				if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
					writeLog(userId, "N", "<B>"+ mnUsers.getUserFirstName()+" "+  mnUsers.getUserLastName() + "</B> Shared a "+ getListType(mnList.getListType())+" with you: <B>"+ changedNoteNameWithDouble((String)jsonObject.get("eventName"))+"</B>" , mnList.getListType(), mnList.getListId().toString(),(String) jsonObject.get("eventId"), notUserIdSet, mnList.getListType());
				}
			} else {
				if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
					writeLog(userId, "A", "Shared a note: <B>"+  changedNoteNameWithDouble((String)jsonObject.get("noteName")) + "</B> with " + addUser,mnList.getListType(), mnList.getListId().toString(),(String) jsonObject.get("noteId"), null, mnList.getListType());
				}

				if (addedNotiUserIdSet != null && !addedNotiUserIdSet.isEmpty()) {
					writeLog(userId, "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> Shared a "+ getListType(mnList.getListType())+": <B>"+ changedNoteNameWithDouble((String)jsonObject.get("noteName")) + "</B> with <B>"+ addUserName + "</B>", mnList.getListType(),mnList.getListId().toString(), (String) jsonObject.get("noteId"), addedNotiUserIdSet, mnList.getListType());
				}
				if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
					writeLog(userId, "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> Shared a "+ getListType(mnList.getListType())+" with you: <B>"+  changedNoteNameWithDouble((String)jsonObject.get("noteName"))+"</B>" , mnList.getListType(), mnList.getListId().toString(),(String) jsonObject.get("noteId"), notUserIdSet, mnList.getListType());
				}
			}

			if(logger.isDebugEnabled())
				logger.debug(" updateMembersForNote method returened successfully "+size );
			
			return "" + size;
		} catch (Exception e) {
			logger.error("Exception while Updating Members For Note :" + e);
		}
		return "Exception while Updating Members For Note";
	}
	public String changedNoteNameWithDouble(String noteName){
		if(noteName.indexOf("\"")!= -1){
			noteName = noteName.replace("\"", "\\\"");
		}
		return noteName;
	}
	public void dueDateSharing(String userId, String listId, String noteId, String dueDate) 
	{
		if(logger.isDebugEnabled())
			logger.debug("dueDateSharing method called in Impl :"+userId+" dueDate: "+dueDate);
		try
		{
		MnNoteDueDateDetails details = new MnNoteDueDateDetails();
		details.setUserId(Integer.parseInt(userId));
		details.setListId(Integer.parseInt(listId));
		details.setNoteId(Integer.parseInt(noteId));
		details.setListType("music");
		details.setDueDate(dueDate);
		details.setStatus("A");
		mongoOperations.insert(details,	JavaMessages.Mongo.MNNOTEDUEDATEDETAILS);
		}catch (Exception e) {
			logger.error("Error in dueDateSharing:"+e.getMessage());
		}
	}

	public String notesMembersCopyUpdateConvertToJsonObject(String jsonStr) {
		String members = "";
		if(logger.isDebugEnabled())
			logger.debug("notesMembersCopyUpdateConvertToJsonObject method called in Impl :");
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldMembers = (String) jsonObject.get("copyToMember");
				members = "\"copyToMember\":\"" + oldMembers + "\"";
			} catch (Exception e) {
				logger.error("Exception while converting json to Notes Copy Details update object In Impl !",e);
			}
		}
		return members;
	}
	
	public String notesMembersUpdateConvertToJsonObject(String jsonStr) {
		String members = "";
		if(logger.isDebugEnabled())
			logger.debug("notesMembersUpdateConvertToJsonObject method called in Impl :");
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				
				String oldMembers = (String) jsonObject.get("notesMembers");
				members = "\"notesMembers\":\"" + oldMembers + "\"";	
			} catch (Exception e) {
				logger.error("Exception while converting json to Notes Sharing Details update object In Impl !",e);
			}
		}
		return members;
	}
	

	public String eventMembersUpdateConvertToJsonObject(String jsonStr) {
		String members = "";
		if(logger.isDebugEnabled())
			logger.debug("eventMembersUpdateConvertToJsonObject method called in Impl :");
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldMembers = (String) jsonObject.get("eventMembers");
				members = "\"eventMembers\":\"" + oldMembers + "\"";
			} catch (Exception e) {
				logger.error("Exception while eventMembersUpdateConvertToJsonObject In Impl !",e);
			}
		}
		return members;
	}

	public List<Integer> notesTagRemoveUnWantedConvertToJsonObject(
			String jsonStr, String flag, Integer tagId) {
		if(logger.isDebugEnabled())
			logger.debug("notesTagRemoveUnWantedConvertToJsonObject method called in Impl tagId: "+tagId);
		List<Integer> tagList = new ArrayList<Integer>();
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String tag = (String) jsonObject.get("tag");
				tag = tag.replace("[", "");
				tag = tag.replace("]", "");

				if (tag.contains(",")) {
					String[] str = tag.split(",");
					for (int i = 0; i < str.length; i++) {
						tagList.add(Integer.parseInt(str[i].trim()));
					}
				} else {
					tagList.add(Integer.parseInt(tag));
				}

				if (flag.equals("R"))
					tagList.remove(tagId);
				else if (flag.equals("A"))
					tagList.add(tagId);

			}

			catch (Exception e) {
				logger.error("Exception while notesTagRemoveUnWantedConvertToJsonObject In Impl !",e);
			}
		}
		if(logger.isDebugEnabled())
			logger.debug(" notesTagRemoveUnWantedConvertToJsonObject method returened successfully " );
		return tagList;
	}

	public List<Integer> notesTagUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("notesTagUpdateConvertToJsonObject method called in Impl :");
		List<Integer> tags = new ArrayList<Integer>();
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String tag = (String) jsonObject.get("tag");
				tag = tag.replace("[", "");
				tag = tag.replace("]", "");
				if (tag.contains(",")) {
					String[] str = tag.split(",");
					for (int i = 0; i < str.length; i++) {
						tags.add(Integer.parseInt(str[i].trim()));
					}
				} else {
					tags.add(Integer.parseInt(tag));
				}
			} catch (Exception e) {
				logger.error("Exception while notesTagUpdateConvertToJsonObject In Impl !",e);
			}
		}
		if(logger.isDebugEnabled())
			logger.debug(" notesTagUpdateConvertToJsonObject method returened successfully " );
		return tags;
	}

	public String noteGroupsCopyUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("noteGroupsCopyUpdateConvertToJsonObject method called in Impl :");
		String groups = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldGroups = (String) jsonObject.get("noteGroups");
				groups = "\"noteGroups\":\"" + oldGroups + "\"";
				
			} catch (Exception e) {
				logger.error("Exception while noteGroupsCopyUpdateConvertToJsonObject In Impl !",e);
			}
		}
		return groups;
	}
	
	
	public String noteFromCopyUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("noteFromCopyUpdateConvertToJsonObject method called in Impl :");
		String groups = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldMember = (String) jsonObject.get("copyFromMember");
				groups = "\"copyFromMember\":\"" + oldMember + "\"";
				
			} catch (Exception e) {
				logger.error("Exception while noteFromCopyUpdateConvertToJsonObject In Impl !",e);
			}
		}
		return groups;
	}
	
	public String noteGroupsUpdateConvertToJsonObject(String jsonStr,boolean flag) {
		if(logger.isDebugEnabled())
			logger.debug("noteGroupsUpdateConvertToJsonObject method called in Impl :");
		String groups = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldGroups = (String) jsonObject.get("noteGroups");
				if(flag){
					groups = "\"noteGroups\":\"" + oldGroups + "\"";
				}else{
					groups = oldGroups;
				}
			} catch (Exception e) {
				logger.error("Exception while noteGroupsUpdateConvertToJsonObject In Impl !",e);
			}
		}
		return groups;
	}

	public String eventGroupsUpdateConvertToJsonObject(String jsonStr,boolean flag) {
		if(logger.isDebugEnabled())
			logger.debug("eventGroupsUpdateConvertToJsonObject method called in Impl :");
		String groups = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldGroups = (String) jsonObject.get("eventGroups");
				if(flag){
					groups = "\"eventGroups\":\"" + oldGroups + "\"";
				}else{
					groups = oldGroups;
				}
			} catch (Exception e) {
				logger.error("Exception while eventGroupsUpdateConvertToJsonObject In Impl !",e);
			}
		}
		if(logger.isDebugEnabled())
			logger.debug(" eventGroupsUpdateConvertToJsonObject method returened successfully " );
		return groups;
	}

	public String noteCommentsUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("noteCommentsUpdateConvertToJsonObject method called in Impl :");
		String comments = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldComments = (String) jsonObject.get("comments");

				comments = "\"comments\":\"" + oldComments + "\"";
			} catch (Exception e) {
				logger.error("Exception while noteCommentsUpdateConvertToJsonObject In Impl !",e);
			}
		}
		return comments;
	}

	public String noteDueTimeUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("noteDueTimeUpdateConvertToJsonObject method called in Impl :");
		String dueTime = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldtime = (String) jsonObject.get("dueTime");

				dueTime = "\"dueTime\":\"" + oldtime + "\"";
			} catch (Exception e) {
				logger.error("Exception while noteDueTimeUpdateConvertToJsonObject In Impl !",e);
			}
		}
		return dueTime;
	}
	
	public String notePrivateTagUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("notePrivateTagUpdateConvertToJsonObject method called in Impl :");
		String tags = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldtags = (String) jsonObject.get("privateTags");

				tags = "\"privateTags\":\"" + oldtags + "\"";
			} catch (Exception e) {
				logger.error("Exception while notePrivateTagUpdateConvertToJsonObject In Impl !",e);
			}
		}
		return tags;
	}
	
	public String noteTagUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("noteTagUpdateConvertToJsonObject method called in Impl :");
		String tags = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldtags = (String) jsonObject.get("tag");

				tags = "\"tag\":\"" + oldtags + "\"";
			} catch (Exception e) {
				logger.error("Exception while noteTagUpdateConvertToJsonObject In Impl !",e);
			}
		}
		return tags;
	}
	
	public String noteReminderUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("noteReminderUpdateConvertToJsonObject method called in Impl :");
		String remainders = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldreminder = (String) jsonObject.get("remainders");

				remainders = "\"remainders\":\"" + oldreminder + "\"";
			} catch (Exception e) {
				logger.error("Exception while noteReminderUpdateConvertToJsonObject In Impl !",e);
			}
		}
		return remainders;
	}
	public String noteDueDateUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("noteDueDateUpdateConvertToJsonObject method called in Impl :");
		String dueDate = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldDate = (String) jsonObject.get("dueDate");

				dueDate = "\"dueDate\":\"" + oldDate + "\"";
			} catch (Exception e) {
				logger.error("Exception while noteDueDateUpdateConvertToJsonObject In Impl !",e);
			}
		}
		return dueDate;
	}
	public String noteFilePathUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("noteFilePathUpdateConvertToJsonObject method called in Impl :");
		String attachFilePath = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldfile = (String) jsonObject.get("attachFilePath");

				attachFilePath = "\"attachFilePath\":\"" + oldfile + "\"";
			} catch (Exception e) {
				logger.error("Exception while noteFilePathUpdateConvertToJsonObject In Impl !",e);
			}
		}
		return attachFilePath;
	}
	public String noteVotesUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("noteVotesUpdateConvertToJsonObject method called in Impl :");
		String votes = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldvotes = (String) jsonObject.get("vote");

				votes = "\"vote\":\"" + oldvotes + "\"";
			} catch (Exception e) {
				logger.error("Exception while noteVotesUpdateConvertToJsonObject In Impl !",e);
			}
		}
		return votes;
	}

	public String notesSharingDetailsConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("notesSharingDetailsConvertToJsonObject method called in Impl :");
		String members = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				if (jsonObject.has("notesMembers"))
					members = (String) jsonObject.get("notesMembers");
				else
					members = (String) jsonObject.get("eventMembers");
				if (!members.isEmpty() && members.contains("[")) {
					members = members.substring(members.indexOf("[") + 1,
							members.length());
				}
				if (!members.isEmpty() && members.contains("]")) {
					members = members.substring(0, members.indexOf("]"));
				}
			} catch (Exception e) {
				logger.error("Exception while notesSharingDetailsConvertToJsonObject In Impl !",e);
			}
		}
		if(logger.isDebugEnabled())
			logger.debug(" notesSharingDetailsConvertToJsonObject method returened successfully " );
		return members.trim();
	}
	
	
	public String notesSharingCopyDetailsConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("notesSharingCopyDetailsConvertToJsonObject method called in Impl :");
		String members = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				
					members = (String) jsonObject.get("copyToMember");
				
				if (!members.isEmpty() && members.contains("[")) {
					members = members.substring(members.indexOf("[") + 1,
							members.length());
				}
				if (!members.isEmpty() && members.contains("]")) {
					members = members.substring(0, members.indexOf("]"));
				}
			} catch (Exception e) {
				logger.error("Exception while notesSharingCopyDetailsConvertToJsonObject In Impl !",e);
			}
		}
		if(logger.isDebugEnabled())
			logger.debug(" notesSharingCopyDetailsConvertToJsonObject method returened successfully " );
		return members.trim();
	}
	
	
	public String notesSharingGroupCopyDetailsConvertToJsonObject(String jsonStr) {
		
		if(logger.isDebugEnabled())
			logger.debug("notesSharingGroupCopyDetailsConvertToJsonObject method called in Impl :");
		String members = "";
		
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				
					members = (String) jsonObject.get("copyToGroup");
				
				if (!members.isEmpty() && members.contains("[")) {
					members = members.substring(members.indexOf("[") + 1,
							members.length());
				}
				if (!members.isEmpty() && members.contains("]")) {
					members = members.substring(0, members.indexOf("]"));
				}
			} catch (Exception e) {
				logger.error("Exception while notesSharingGroupCopyDetailsConvertToJsonObject In Impl !",e);
			}
		}
		if(logger.isDebugEnabled())
			logger.debug(" notesSharingGroupCopyDetailsConvertToJsonObject method returened successfully " );
		return members.trim();
	}
	
	public String notesSharingAllContactMembersDetailsConvertToJsonObject(String jsonStr) {
		String members = "";
		if(logger.isDebugEnabled())
			logger.debug("notesSharingAllContactMembersDetailsConvertToJsonObject method called in Impl :");
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				if (jsonObject.has("noteSharedAllContactMembers"))
					members = (String) jsonObject.get("noteSharedAllContactMembers");
				else
					members = (String) jsonObject.get("eventSharedAllContactMembers");
				if (!members.isEmpty() && members.contains("[")) {
					members = members.substring(members.indexOf("[") + 1,
							members.length());
				}
				if (!members.isEmpty() && members.contains("]")) {
					members = members.substring(0, members.indexOf("]"));
				}
			} catch (Exception e) {
				logger.error("Exception while notesSharingAllContactMembersDetailsConvertToJsonObject In Impl !",e);
			}
		}
		if(logger.isDebugEnabled())
			logger.debug(" notesSharingAllContactMembersDetailsConvertToJsonObject method returened successfully " );
		return members.trim();
	}

	public String notesSharingGroupDetailsConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("notesSharingGroupDetailsConvertToJsonObject method called in Impl :");
	String groups = "";
	if (jsonStr != null && !jsonStr.equals("")) {
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(jsonStr);
			if (jsonObject.has("noteGroups"))
				groups = (String) jsonObject.get("noteGroups");
			else
				groups = (String) jsonObject.get("eventGroups");
			
			if (groups.contains("[")) {
				groups = groups.substring(groups.indexOf("[") + 1,
						groups.length());
			}
			if (groups.contains("]")) {
				groups = groups.substring(0, groups.indexOf("]"));
			}
		} catch (Exception e) {
			logger.error("Exception while notesSharingGroupDetailsConvertToJsonObject In Impl !",e);
		}
	}
	if(logger.isDebugEnabled())
		logger.debug(" notesSharingGroupDetailsConvertToJsonObject method returened successfully " );
	return groups;
}


	
	
	private String insertNotesSharingDetails(List<MnNotesSharingDetails> details,List<MnEventsSharingDetails> sharingDetails) {
		if(logger.isDebugEnabled())
			logger.debug("insertNotesSharingDetails method called in Impl :");
		try {
			mongoOperations.insert(details,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			if (sharingDetails != null && !sharingDetails.isEmpty()) {
				Query query = null;
				Integer cid = 0;
				query = new Query();
				query.sort().on("_id", Order.ASCENDING);
				List<MnEventsSharingDetails> shares = mongoOperations.find(query, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
				if (shares != null && shares.size() != 0) {
					MnEventsSharingDetails event = shares.get(shares.size() - 1);
					cid = event.getEventSharedId() + 1;

				} else {
					cid = 1;
				}
				for (MnEventsSharingDetails det : sharingDetails) {
					det.setEventSharedId(cid);
					cid++;
				}
				mongoOperations.insert(sharingDetails,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);

			}
			if(logger.isDebugEnabled())
				logger.debug(" insertNotesSharingDetails method returened successfully " );
			
		} catch (Exception e) {
			logger.error("Exception while insertNotesSharingDetails Details :"
					+ e);
		}
		return "Exception while Inserting Notes Sharing Details";
	}

	private String updateNotesSharingDetails(String sharingUserId,
			Integer userId, String listId, String noteId) {
		if(logger.isDebugEnabled())
			logger.debug("updateNotesSharingDetails method called in Impl userId: "+userId+" sharingUserId: "+sharingUserId);
		try {
			Query query = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(sharingUserId)).and("userId").is(userId).and("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
			Update update = new Update();
			update.set("status", "I");
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			
			Query query2=new Query(Criteria.where("sharingUserId").is(Integer.parseInt(sharingUserId)).and("userId").is(Integer.parseInt(sharingUserId)).and("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("sharingLevel").is("individual").and("status").is("A"));
			Update update1 = new Update();
			update1.set("status", "I");
			mongoOperations.updateFirst(query2, update1,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			
			
			Query query1 = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(sharingUserId)).and("userId").is(userId).and("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A").and("sharingGroupId").is(0).and("listType").is("schedule"));
			mongoOperations.updateFirst(query1, update,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
			
		} catch (Exception e) {
			logger.error("Exception while Updating Notes Sharing Details :"+ e);
		}
		return "Exception while Updating Notes Sharing Details";
	}

	private String updateNotesgroupSharingDetails(String sharingGroupId,
			Integer userId, String listId, String noteId) {
		if(logger.isDebugEnabled())
			logger.debug("updateNotesgroupSharingDetails method called in Impl userId: "+userId+" sharingGroupId: "+sharingGroupId);
		try {
			Query query = new Query(Criteria.where("sharingGroupId").is(Integer.parseInt(sharingGroupId)).and("sharingUserId").is(userId).and("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
			List<MnNotesSharingDetails> details=mongoOperations.find(query,MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			
			for(MnNotesSharingDetails test:details)
			{
			Query query2 = new Query(Criteria.where("sharingUserId").is(test.getSharingUserId()).and("userId").is(test.getSharingUserId()).and("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("sharingLevel").is("individual").and("status").is("A"));
			Update update1 = new Update();
			update1.set("status", "I");
			mongoOperations.updateFirst(query2, update1,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			}
			
			Update update = new Update();
			update.set("status", "I");
			mongoOperations.updateMulti(query, update,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);

			Query query1 = new Query(Criteria.where("userId").is(userId).and("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A").and("sharingGroupId").is(Integer.parseInt(sharingGroupId)).and("listType").is("schedule"));
			mongoOperations.updateMulti(query1, update,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
			
		} catch (Exception e) {
			logger.error("Exception while Updating Notes Sharing Details :"	+ e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" updateNotesgroupSharingDetails method returened successfully " );
		
		return "Exception while Updating Notes Sharing Details";
	}

	private List<Integer> getUserIdBasedOnGroupId(Integer groupId) {
		if(logger.isDebugEnabled())
			logger.debug("getUserIdBasedOnGroupId method called in Impl groupId: "+groupId);
		List<Integer> userIds = new ArrayList<Integer>();
		List<MnGroupDetailsDomain> groupDetails = new ArrayList<MnGroupDetailsDomain>();
		try {
			Query query = new Query(Criteria.where("groupId").is(groupId).and("status").is("A"));
			groupDetails = mongoOperations.find(query,MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);
			if (groupDetails != null && !groupDetails.isEmpty()) {
				for (MnGroupDetailsDomain group : groupDetails) {
					if (userIds != null)
						userIds.add(group.getUserId());
				}
			}

		} catch (Exception e) {
			logger.error("Error in getUserIdBasedOnGroupId:" + e);
			userIds = null;
		}
		if(logger.isDebugEnabled())
			logger.debug(" getUserIdBasedOnGroupId method returened successfully " );
		return userIds;
	}

	@Override
	public MnList archiveList(String listId, String userId,String sharedType,String listType) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		
		if(logger.isDebugEnabled())
			logger.debug("archiveList method called in Impl :"+userId);
		
		try {
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
			MnList mnList = mongoOperations.findOne(query, MnList.class,	JavaMessages.Mongo.MNLIST);
			
			Update update = new Update();
			update.set("status", "I");
			if(sharedType.trim().equals("js-close-list")){
				Query queryAssocite = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("sharedBookId").is(Integer.parseInt(listId))
						.and("listType").is(listType).and("status").is("A").and("sharingLevel").is("individual").and("sharingUserId").is(Integer.parseInt(userId)));
				mongoOperations.updateFirst(queryAssocite,update,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				
				update.set("listOwnerStatus", "I");
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
				
				Query queryMnNotes =  new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("listId").is(Integer.parseInt(listId))
						.and("listType").is(listType).and("listOwnerStatus").is("A"));
				Update updateMnNote = new Update();
				updateMnNote.set("listOwnerStatus", "I");
				updateMnNote.set("ownerNoteStatus", "I");
				mongoOperations.updateMulti(queryMnNotes, updateMnNote,JavaMessages.Mongo.MNNOTEDETAILS);
				
			}else{
				Query query1 =  null;
				List<Integer> sharedIndividuals = null;
				String share[] = sharedType.split(" ");
				update.set("endDate", sdf.format(date));
				if(share[1].trim().equals("individual" )){
					if(mnList.getSharedIndividuals().indexOf(Integer.parseInt(userId)) != -1 ){
						sharedIndividuals = mnList.getSharedIndividuals();
						sharedIndividuals.remove(sharedIndividuals.indexOf(Integer.parseInt(userId)));  	
					}
					query1 = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("listId").is(Integer.parseInt(listId)).and("listType").is(listType).and("status").is("A").
					and("shareAllContactFlag").is(false).and("sharingGroupId").is(0).and("noteId").is(0).and("sharingLevel").is("book"));

				}else if (share[1].trim().equals("allcon")){
						query1 = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("listId").is(Integer.parseInt(listId)).and("listType").is(listType).and("status").is("A").
								and("shareAllContactFlag").is(true).and("sharingGroupId").is(0).and("noteId").is(0).and("sharingLevel").is("book"));
					
				}else if (share[1].trim().equals("groups")){
					query1 = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("listId").is(Integer.parseInt(listId)).and("listType").is(listType).and("status").is("A").
							and("shareAllContactFlag").is(false).and("sharingGroupId").ne(0).and("noteId").is(0).and("sharingLevel").is("book"));
				}
								
				mongoOperations.updateFirst(query1,update,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				
				if(sharedIndividuals!= null){
					Update update1 = new Update();
					update1.set("sharedIndividuals", sharedIndividuals);
					mongoOperations.updateFirst(query, update1,JavaMessages.Mongo.MNLIST);
				}
			}
			Set<String> tempNoteDetails = new HashSet<String>();
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"status\":\"A\"")) {
					tempNoteDetails.add(note);
				}
			}

			moveAllNoteLog(tempNoteDetails, mnList, "Deleted notebook:", "on "+getListTypePage(mnList.getListType()), userId);
			
			if(logger.isDebugEnabled())
				logger.debug(" archiveList method returened successfully " );
			return mnList;
		} catch (Exception e) {
			logger.error("Exception while Archive The List :" + e);
			return null;
		}
	}

	@Override
	public String archiveNote(String listId, String noteId, String userId,String eventsOrNotes,String shareType,String groupId,String shareUserId) {
		Date date = new Date();
		if(logger.isDebugEnabled())
			logger.debug("archiveNote method called in Impl :"+userId+" groupId: "+groupId);
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Set<String> updateNoteDetails=new TreeSet<String>();
		boolean updateNeedOrNotFlag=false;
		boolean deleteNoteCompletedFlag=false;
		try {
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			String tempNoteDetails = null;
			String removeNoteDeatils = null;

			if (eventsOrNotes.equalsIgnoreCase("notes")) {
				for (String note : mnList.getMnNotesDetails()) {

					if (note.contains("\"noteId\":\"" + noteId + "\"")) {
						removeNoteDeatils = note;
						
						if(mnList.getUserId() == Integer.parseInt(userId)){
							
						
							
							if (note.contains("\"ownerNoteStatus\":\"A\"")) {
								note = note.replace("\"ownerNoteStatus\":\"A\"","\"ownerNoteStatus\":\"I\"");
							}
							if(note.contains("\"notesMembers\":\"\"") && note.contains("\"noteGroups\":\"\"") && note.contains("\"noteSharedAllContact\":0") && note.contains("\"access\":\"private\"")){
								if (note.contains("\"status\":\"A\"")) {
									deleteNoteCompletedFlag=true;
									note = note.replace("\"status\":\"A\"","\"status\":\"I\"");
								}
							}
						}else{
							updateNeedOrNotFlag=true;
							
							if(shareType!=null && !shareType.equals("") && shareType.equalsIgnoreCase("single")){
								
								deleteMembersForNote(userId,listId,noteId, shareUserId);
								
							}else if(shareType!=null && !shareType.equals("") && shareType.equalsIgnoreCase("group")){
								
								deletegroupForNote(groupId,listId,noteId,userId);
								
							}else if(shareType!=null && !shareType.equals("") && shareType.equalsIgnoreCase("allCon")){
								
								deleteAllContactMembersSharingForSingleUser(userId, listId, noteId, mnList.getListType(), shareUserId);
								
							}
						}
							
						tempNoteDetails = note;
					}
					updateNoteDetails.add(note);
				}
			} else {
				for (String note : mnList.getMnNotesDetails()) {

					if (note.contains("\"eventId\":\"" + noteId + "\"")) {

						removeNoteDeatils = note;
						
						if(mnList.getUserId() == Integer.parseInt(userId)){
							if (note.contains("\"ownerEventStatus\":\"A\"")) {
								note = note.replace("\"ownerEventStatus\":\"A\"","\"ownerEventStatus\":\"I\"");
							}
							if(note.contains("\"eventMembers\":\"\"") && note.contains("\"eventGroups\":\"\"") && note.contains("\"eventSharedAllContact\":0") && note.contains("\"access\":\"private\"")){
								if (note.contains("\"status\":\"A\"")) {
									deleteNoteCompletedFlag=true;
									note = note.replace("\"status\":\"A\"","\"status\":\"I\"");
								}
							}
						}else{
							updateNeedOrNotFlag=true;
							
							try {
								Query query2 = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A").and("listType").is("schedule"));
								Update update = new Update();
								update.set("status", "I");
								mongoOperations.updateFirst(query2, update,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
								
								Query query1 = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A").and("listType").is("schedule"));
								mongoOperations.updateFirst(query1, update,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
							} catch (Exception e) {
								logger.error("Exception while Updating Notes Sharing Details :"+ e);
							}
							
						}
						tempNoteDetails = note;
					}
					updateNoteDetails.add(note);
				}
				
				Query query1 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("eventId").is(Integer.parseInt(noteId)));
				List<MnSubEventDetails> mnSubEventDetails = mongoOperations.find(query1, MnSubEventDetails.class,JavaMessages.Mongo.MNESUBVENTS);
				if(mnSubEventDetails!=null && !mnSubEventDetails.isEmpty())
				{
				if(deleteNoteCompletedFlag)
				{
					
						Query query2 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("eventId").is(Integer.parseInt(noteId)));
						Update update = new Update();
						update.set("status", 'I');
						mongoOperations.updateMulti(query2, update,JavaMessages.Mongo.MNESUBVENTS);
					
				}
				}
			}
			
			if(!updateNeedOrNotFlag && !deleteNoteCompletedFlag){
				if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
					mnList.getMnNotesDetails().remove(removeNoteDeatils);

				if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
					mnList.getMnNotesDetails().add(tempNoteDetails);

				Update update = new Update();
				update.set("mnNotesDetails", updateNoteDetails);
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
				
			}
			
			if(!updateNeedOrNotFlag && deleteNoteCompletedFlag){
				
				if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
					mnList.getMnNotesDetails().remove(removeNoteDeatils);

				if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
					mnList.getMnNotesDetails().add(tempNoteDetails);

				Update update = new Update();
				update.set("mnNotesDetails", updateNoteDetails);
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);

				// This below query part is used to change the Status of the Notes
				// when archive is clicked for particular note in the musicnote
				// page.

				Query query2 = new Query(Criteria.where("listId").is(	Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("userId").is(Integer.parseInt(userId)));

				Update updateNotes = new Update();
				updateNotes.set("endDate", sdf.format(date));
				updateNotes.set("status", "I");
				mongoOperations.updateMulti(query2, updateNotes,	JavaMessages.Mongo.MNNOTESSHARINGDETAILS);

				// This below query part is used to change the Status of the Events
				// when archive is clicked for particular event in the Schedule
				// page.

				Query query1 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("eventId").is(Integer.parseInt(noteId)));
				MnEventDetails mnEvents = mongoOperations.findOne(query1,MnEventDetails.class, JavaMessages.Mongo.MNEVENTS);
				if (mnEvents != null) {
					Update updateEvents = new Update();
					updateEvents.set("status", "I");
					mongoOperations.updateFirst(query1, updateEvents,JavaMessages.Mongo.MNEVENTS);
				}
			}
			
			// Update Delete Details In Note Details Table
			if(eventsOrNotes.equalsIgnoreCase("notes")){
				if(mnList.getUserId() == Integer.parseInt(userId)){
					query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)));
					Update update = new Update();
					update.set("ownerNoteStatus", "I");
					if(removeNoteDeatils.contains("\"notesMembers\":\"\"") && removeNoteDeatils.contains("\"noteGroups\":\"\"") && removeNoteDeatils.contains("\"noteSharedAllContact\":0") && removeNoteDeatils.contains("\"access\":\"private\"")){
						if (removeNoteDeatils.contains("\"status\":\"A\"")) {
							update.set("status", "I");
						}
					}
					mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNNOTEDETAILS);
				}
			}
			
			commomLog(tempNoteDetails, mnList, "Deleted note:", false,userId,false,false,true,"",true,"");
			if(logger.isDebugEnabled())
				logger.debug(" archiveNote method returened successfully " );
			
			return "1";
		} catch (Exception e) {
			logger.error("Exception while Archive The Note :" + e);
			return "0";
		}
	}

	@Override
	public String archiveAllNoteInList(String listId,String userId,String listType) {
		
		if(logger.isDebugEnabled())
			logger.debug("archiveAllNoteInList method called in Impl :"+userId);
		
		try {
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);

			Set<String> tempNoteDetails = new HashSet<String>();
		
			for(String note: mnList.getMnNotesDetails()){
				if(listType!="" && listType.equals("schedule")){
					if (note.contains("\"ownerEventStatus\":\"A\"")) {
						note = note.replace("\"ownerEventStatus\":\"A\"","\"ownerEventStatus\":\"I\"");
					}
					if(note.contains("\"eventMembers\":\"\"") && note.contains("\"eventGroups\":\"\"") && note.contains("\"eventSharedAllContact\":0") && note.contains("\"access\":\"private\"")){
						if (note.contains("\"status\":\"A\"")) {
							note = note.replace("\"status\":\"A\"","\"status\":\"I\"");
						}
					}
				}else{
				if (note.contains("\"ownerNoteStatus\":\"A\"")) {
					note = note.replace("\"ownerNoteStatus\":\"A\"","\"ownerNoteStatus\":\"I\"");
				}
				if(note.contains("\"notesMembers\":\"\"") && note.contains("\"noteGroups\":\"\"") && note.contains("\"noteSharedAllContact\":0") && note.contains("\"access\":\"private\"")){
					if (note.contains("\"status\":\"A\"")) {
						note = note.replace("\"status\":\"A\"","\"status\":\"I\"");
					}
				}
				}
				tempNoteDetails.add(note);
				
				if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
					mnList.setMnNotesDetails(tempNoteDetails);
				
				Update update=new Update();
				update.set("mnNotesDetails", mnList.getMnNotesDetails());
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
				
			}
			
			// mn note details 
			Query queryMnNotes =  new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("listId").is(Integer.parseInt(listId))
					.and("listType").is(listType).and("listOwnerStatus").is("A"));
			List<MnNoteDetails> mnNotesDetails = mongoOperations.find(queryMnNotes, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS );
			if(mnNotesDetails!= null && !mnNotesDetails.isEmpty()){
				for(MnNoteDetails details: mnNotesDetails){
					Update updateNote=new Update();
					if(details.getNoteGroups()!= null && !details.getNoteGroups().isEmpty() && details.getNotesMembers()!= null 
						&& !details.getNotesMembers().isEmpty() &&  details.isNoteSharedAllContact()){
						// DOES NOTHG
					}else{
						updateNote.set("status", "I");	
					}
					queryMnNotes  =  new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("listId").is(Integer.parseInt(listId))
							.and("noteId").is(details.getNoteId()).and("listType").is(listType).and("listOwnerStatus").is("A"));
					updateNote.set("ownerNoteStatus", "I");
					mongoOperations.updateFirst(queryMnNotes, updateNote,JavaMessages.Mongo.MNNOTEDETAILS);
				}
			}
			
			// mn note sharing details 
			
			Query queryAssocite = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("sharedBookId").is(Integer.parseInt(listId))
					.and("listType").is(listType).and("status").is("A").and("sharingLevel").is("individual").and("sharingUserId").is(Integer.parseInt(userId)));
			List<MnNotesSharingDetails> details = mongoOperations.find(queryAssocite, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			Update updateMnNote = new Update();
			updateMnNote.set("status", "I");
			mongoOperations.updateMulti(queryAssocite,updateMnNote,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			
			moveAllNoteLog(tempNoteDetails, mnList, "Deleted all notes from","", userId);
			if(logger.isDebugEnabled())
				logger.debug(" archiveAllNoteInList method returened successfully " );
			return "1";
		} catch (Exception e) {
			logger.error("Exception while Archive All Note In This List :" + e);
			return "0";
		}
	}

	@Override
	public List<MnNoteDetails> fetchList(String userId, String listType) {
		List<MnNoteDetails> mnNoteDetailsList = null;
		Query query = null;
		
		if(logger.isDebugEnabled())
			logger.debug("fetchList method called in Impl :"+userId);
		try {
			
			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("listType").is(listType).and("status").is("A").and("ownerNoteStatus").is("A"));

			mnNoteDetailsList = mongoOperations.find(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
			
		} catch (Exception e) {
			logger.error("Exception while fetching List :" + e);

		}
		return mnNoteDetailsList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MnNoteDetails> fetchSharedNotesBasedOnList(String userId,String listType) {
		if(logger.isDebugEnabled())
			logger.debug("fetchSharedNotesBasedOnList method called in Impl :"+userId);
		List<MnNotesSharingDetails> details = null;
		List<MnNoteDetails> sharedNotesDetailsBasedOnListIds = new ArrayList<MnNoteDetails>();
		Map<String,Integer> asociateBook=new HashMap<String,Integer>();
		Query query = null;
		try {
			query = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("listType").is(listType).and("status").is("A").and("sharingGroupId").is(0).and("sharingLevel").ne("individual"));
			query.sort().on("listId", Order.ASCENDING);
			details = mongoOperations.find(query, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);

			List<MnNotesSharingDetails> associateNoteForBook = new ArrayList<MnNotesSharingDetails>();
			if (details != null && !details.isEmpty()) {
				List<Integer> listIds = new ArrayList<Integer>();
				List<Integer> bookLevelSharedlistIds = new ArrayList<Integer>();

				for (MnNotesSharingDetails sharingDetails : details) {

					if (!listIds.contains(sharingDetails.getListId())){
						if(sharingDetails.getNoteId().equals(0)){
							if (!bookLevelSharedlistIds.contains(sharingDetails.getListId())){
								bookLevelSharedlistIds.add(sharingDetails.getListId());
								listIds.add(sharingDetails.getListId());
								//fetch the book level sharing , associate note for the particular book
								Query query12 = new Query(Criteria.where("sharingLevel").is("individual").and("sharedBookId").is(sharingDetails.getListId()).and("status").is("A"));
								List<MnNotesSharingDetails> mnSharedList = mongoOperations.find(query12, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
								if(mnSharedList!= null && !mnSharedList.isEmpty()){
									for(MnNotesSharingDetails associateSharing :mnSharedList){
										if(!listIds.contains(associateSharing.getListId())){
											listIds.add(associateSharing.getListId());
										}
										query12 = new Query(Criteria.where("listType").is(listType).and("status").is("A").and("sharingGroupId").is(0)
												.and("sharingLevel").is("individual").and("sharedBookId").is(sharingDetails.getListId()).and("listId").is(associateSharing.getListId()).and("noteId").is(associateSharing.getNoteId()));
										MnNotesSharingDetails associateNoteObj = mongoOperations.findOne(query12, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
										
										asociateBook.put(associateNoteObj.getListId()+"~"+associateNoteObj.getNoteId(), associateNoteObj.getSharedBookId());
										
										if(associateNoteObj!= null ){
											associateNoteForBook.add(associateNoteObj);
										}
									}
								}
							}
						}else{
							listIds.add(sharingDetails.getListId());
						}
					}

				}
				if(!associateNoteForBook.isEmpty()){
					details.addAll(associateNoteForBook);
				}
				
				//Using New Table To Fetch mnNotes
				for (Integer ids : listIds) {
					List<String> noteIds = new ArrayList<String>();
					Map sharedDates=new HashMap();
					if(!bookLevelSharedlistIds.contains(ids)){
						for (MnNotesSharingDetails sharingDetails : details) {
							if (sharingDetails.getListId().equals(ids)) {
								if(!noteIds.contains(sharingDetails.getNoteId().toString())){
									noteIds.add(sharingDetails.getNoteId().toString());
									sharedDates.put(sharingDetails.getNoteId().toString(), sharingDetails.getSharedDate());
								}
							}
						}
					}else{
						noteIds.add("0");
						for (MnNotesSharingDetails sharingDetails : details) {
							if (sharingDetails.getListId() == ids) {
								sharedDates.put("0", sharingDetails.getSharedDate());
							}
						}
					}
					if (listType.equalsIgnoreCase("schedule")) {
						
						//No Need To Write Coding Here
						
					}else{
						List<MnNoteDetails> sharedNotesDetailsBasedOnListNoteIds=new ArrayList<MnNoteDetails>();
						for(String noteId:noteIds){
							if(noteId.equals("0")){
								query = new Query(Criteria.where("listId").is(ids).and("listType").is(listType).and("status").is("A"));
								List<MnNoteDetails> tempNoteDetails = mongoOperations.find(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
								if(tempNoteDetails!=null){
									for(MnNoteDetails temp:tempNoteDetails){
											
										if(temp.isNoteSharedAllContact())
											temp.setShareType("allCon");
										else
											temp.setShareType("single");
										temp.setTempSharedDate(formatter.parse(sharedDates.get(noteId).toString()));
										sharedNotesDetailsBasedOnListNoteIds.add(temp);
									}
									
								}
							}else{
								query = new Query(Criteria.where("listId").is(ids).and("noteId").is(Integer.parseInt(noteId)).and("listType").is(listType).and("status").is("A"));
								MnNoteDetails tempNoteDetail = mongoOperations.findOne(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
								if(tempNoteDetail!=null){
									if(tempNoteDetail.isNoteSharedAllContact())
										tempNoteDetail.setShareType("allCon");
									else
										tempNoteDetail.setShareType("single");
									tempNoteDetail.setTempSharedDate(formatter.parse(sharedDates.get(noteId).toString()));
									
									
									// **** change listId **** ///
									if(asociateBook !=null && !asociateBook.isEmpty())
									{
									Integer listID=asociateBook.get(tempNoteDetail.getListId()+"~"+tempNoteDetail.getNoteId());
									if(listID!=null && !listID.equals(""))
									{
									query = new Query(Criteria.where("listId").is(listID).and("listType").is(listType).and("status").is("A"));
									MnNoteDetails tempNoteDetail1 = mongoOperations.findOne(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
									if(tempNoteDetail1!=null)
									{
										tempNoteDetail.setListId(tempNoteDetail1.getListId());
										tempNoteDetail.setListName(tempNoteDetail1.getListName());
										
									}
									else
									{
										MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
										tempNoteDetail.setOriginalListId(tempNoteDetail.getListId());
										tempNoteDetail.setListId(mnList.getListId());
										tempNoteDetail.setListName(mnList.getListName());
										tempNoteDetail.setSharedIndividuals(mnList.getSharedIndividuals());
										tempNoteDetail.setSharedGroups(mnList.getSharedGroups());
										tempNoteDetail.setShareAllContactFlag(mnList.isShareAllContactFlag());
									}
									}
									}
									//******************************************//
									
									sharedNotesDetailsBasedOnListNoteIds.add(tempNoteDetail);
								}
							}
						}
						if(sharedNotesDetailsBasedOnListNoteIds!=null && !sharedNotesDetailsBasedOnListNoteIds.isEmpty())
							sharedNotesDetailsBasedOnListIds.addAll(sharedNotesDetailsBasedOnListNoteIds);
						
					}
				}
			}
		} catch (Exception e) {
			logger.error("Exception while Fetching Shared Notes Based On List :"+ e);

		}
		
		if(logger.isDebugEnabled())
			logger.debug(" fetchSharedNotesBasedOnList method returened successfully " );
		return sharedNotesDetailsBasedOnListIds;
	}

	@Override
	public String moveNote(String listId, String noteId, String selectedListId,	String userId) {
		String noteData = "";
		String dueDate ="";
		Integer SelectedListAddedNoteId;
		Set<String> updateNoteDetails=null;
		if(logger.isDebugEnabled())
			logger.debug("moveNote method called in Impl :"+userId+" noteId: "+noteId);
		try {
			// Remove Note From List
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);

			String removeNoteDeatils = null;
			updateNoteDetails=new TreeSet<String>();
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					removeNoteDeatils = note;
				}else{
					updateNoteDetails.add(note);
				}
			}
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			
			
				if (!(removeNoteDeatils.contains("\"dueDate\":\"\""))) {
					JSONObject jsonObject = new JSONObject(removeNoteDeatils);
					dueDate = (String) jsonObject.get("dueDate");
					int note1=Integer.parseInt(noteId);
					dueDateUpdateBasedOnListMoveCopy(listId,note1,userId,dueDate,"I");
				}
			
			

			// Adding Selected List to Note
			Query addQuery = new Query(Criteria.where("listId").is(Integer.parseInt(selectedListId)));

			MnList addNote = mongoOperations.findOne(addQuery, MnList.class,JavaMessages.Mongo.MNLIST);
			
			List<Integer> noteIdList = new ArrayList<Integer>();
			try{
				updateNoteDetails=new TreeSet<String>();
				for (String note : addNote.getMnNotesDetails()) {
					JSONObject jsonObject = new JSONObject(note);
					String tempNoteId = (String) jsonObject.get("noteId");
					noteIdList.add(Integer.parseInt(tempNoteId));
					updateNoteDetails.add(note);
				}
			}catch (Exception e) {
				logger.error(e);
			}
			
			int size = addNote.getMnNotesDetails().size() + 1;
			SelectedListAddedNoteId=size;
			while(noteIdList.contains(size)){
				size++;
			}
			removeNoteDeatils = removeNoteDeatils.replace("\"noteId\":\""+ noteId + "\"", "\"noteId\":\"" + size + "\"");

			if (addNote.getMnNotesDetails() != null	&& addNote.getMnNotesDetails().isEmpty()) {
				addNote.setMnNotesDetails(new HashSet<String>());
				addNote.getMnNotesDetails().add(removeNoteDeatils);
				updateNoteDetails.add(removeNoteDeatils);
			} else {
				addNote.getMnNotesDetails().add(removeNoteDeatils);
				updateNoteDetails.add(removeNoteDeatils);
			}

			Update addUpdate = new Update();
			addUpdate.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(addQuery, addUpdate,JavaMessages.Mongo.MNLIST);
			
			if(!dueDate.equals(""))
			dueDateUpdateBasedOnListMoveCopy(selectedListId,size,userId,dueDate,"A");
			
			//Move Based Changing Associated Details In MnNotesSharingDetails Table and shared details also changing.
			
			query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("userId").is(Integer.parseInt(userId)).and("sharedBookId").is(Integer.parseInt(selectedListId)).and("status").is("A"));
			update = new Update();
			update.set("status", "I");
			mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			
			query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("userId").is(Integer.parseInt(userId)).and("status").is("A"));
			update = new Update();
			update.set("listId", Integer.parseInt(selectedListId));
			mongoOperations.updateMulti(query, update, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			
			mongoOperations.updateMulti(query, update, JavaMessages.Mongo.MNNONUSERSHARINGDETAILS);
			
			// Update Move Note Changes In Note Details Table
			if(selectedListId!=null && !selectedListId.isEmpty()){
				query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)));
				update = new Update();
				update.set("listId", Integer.parseInt(selectedListId));
				update.set("noteId", SelectedListAddedNoteId);
				mongoOperations.updateFirst(query, update,	JavaMessages.Mongo.MNNOTEDETAILS);
			}
			
			moveNoteLog(removeNoteDeatils, mnList, addNote.getListName(),"Moved from", userId,false);

			noteData = removeNoteDeatils;
		} catch (Exception e) {
			logger.error("Exception while Moveing Note One List To Another List :"	+ e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" moveNote method returened successfully " );
		return noteData;
	}

	
	
	@Override
	public String moveAllNote(String listId, String selectedListId,
			String userId) {
		String noteData = "";
		JSONObject jsonObject;
		String dueDate="";
		if(logger.isDebugEnabled())
			logger.debug("moveAllNote method called in Impl :"+userId+" listId: "+listId);
		try {
			// Remove All Note From List
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,	JavaMessages.Mongo.MNLIST);

			Set<String> removeNoteDeatils = new HashSet<String>();

			removeNoteDeatils.addAll(mnList.getMnNotesDetails());

			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().removeAll(removeNoteDeatils);

			Update update = new Update();
			update.set("mnNotesDetails", mnList.getMnNotesDetails());
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);

			// Adding Selected List to All Note
			Query addQuery = new Query(Criteria.where("listId").is(Integer.parseInt(selectedListId)));

			MnList addNote = mongoOperations.findOne(addQuery, MnList.class,JavaMessages.Mongo.MNLIST);

			for (String str : removeNoteDeatils) {

				int size = addNote.getMnNotesDetails().size() + 1;

				str = str.replace("\"noteId\":\"" + getNoteIdInJson(str) + "\"","\"noteId\":\"" + size + "\"");
				if (addNote.getMnNotesDetails() != null&& addNote.getMnNotesDetails().isEmpty()) {
					addNote.setMnNotesDetails(new HashSet<String>());
					addNote.getMnNotesDetails().add(str);
				} else {
					addNote.getMnNotesDetails().add(str);
				}
				
				jsonObject = new JSONObject(str);
				dueDate = (String) jsonObject.get("dueDate");
				String noteId = (String) jsonObject.get("noteId");
				if(!dueDate.equals(""))
				{
					int note1=Integer.parseInt(noteId);
					dueDateUpdateBasedOnListMoveCopy(listId,note1,userId,dueDate,"I");
					dueDateUpdateBasedOnListMoveCopy(selectedListId,size,userId,dueDate,"A");
				}
				
			}

			Update addUpdate = new Update();
			addUpdate.set("mnNotesDetails", addNote.getMnNotesDetails());
			mongoOperations.updateFirst(addQuery, addUpdate,JavaMessages.Mongo.MNLIST);

			moveAllNoteLog(removeNoteDeatils, mnList, "Moved all notes from"," to " + addNote.getListName(), userId);

			noteData = addNote.getMnNotesDetails().toString();
		} catch (Exception e) {
			logger.error("Exception while Moveing Note One List To Another List :"+ e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" moveAllNote method returened successfully " );
		return noteData;
	}

	public String getNoteIdInJson(String jsonStr) {
		String noteId = "";
		if(logger.isDebugEnabled())
			logger.debug("getNoteIdInJson method called in Impl :");
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				noteId = (String) jsonObject.get("noteId");
			} catch (Exception e) {
				logger.error("Exception while getNoteIdInJson In Impl !",e);

			}
		}
		return noteId;
	}
	@Override
	public MnList copyList(String listId, String newListName, String userId) {
		MnList newMnList = new MnList();
		Query query = null;
		String noteid="";
		String dueDate="";
		Integer newListId = 0;
		if(logger.isDebugEnabled())
			logger.debug("copyList method called in Impl :"+userId);
		try {
			// Copying Selected Note
			
			query = new Query();
			query.sort().on("listId", Order.ASCENDING);
			List<MnList> mnLists = mongoOperations.find(query, MnList.class,JavaMessages.Mongo.MNLIST);
			String oldListName = "";
			
			MnList newList =null;
			if (mnLists != null && mnLists.size() != 0) {
				MnList lastList = mnLists.get(mnLists.size() - 1);
				newListId = lastList.getListId() + 1;
				for (MnList mnList : mnLists) {
					
					if (mnList.getListId() != null && mnList.getListId().equals(Integer.parseInt(listId))) {
						
						oldListName=mnList.getListName();
						//newList=mnList;
						newList=new MnList();
						newList.setListId(newListId);
						newList.setUserId(Integer.parseInt(userId));
						newList.setListName(newListName);
						newList.setDefaultNote(false);
						newList.setListType(mnList.getListType());
						newList.setStatus("A");
						newList.setDefaultNote(mnList.isDefaultNote());
						newList.setNoteAccess(mnList.getNoteAccess());
						newList.setEnableDisableFlag(false);
						newList.setMnNotesDetails(new HashSet<String>());
						newList.setSharedIndividuals(new ArrayList<Integer>());
						newList.setSharedGroups(new ArrayList<Integer>());
						newList.setShareAllContactFlag(false);
						newList.setMnNotesDetails(new HashSet<String>());
						for(String str:mnList.getMnNotesDetails())
						{
							JSONObject jsonObject;
							jsonObject = new JSONObject(str);
							dueDate=(String) jsonObject.get("dueDate");
							noteid=(String) jsonObject.get("noteId");
							int note=Integer.parseInt(noteid);
							if(!dueDate.equals(""))
								dueDateUpdateBasedOnListMoveCopy(newListId.toString(),note,userId,dueDate,"A");
						}
						// entry for associated book in note sharing table
						JSONObject jsonObject = null;
						String noteId=null;
						Date date=new Date();
						for (String note : mnList.getMnNotesDetails()) {
						
								jsonObject = new JSONObject(note);
								noteId = (String) jsonObject.get("noteId");
								MnNotesSharingDetails sharingDetails = new MnNotesSharingDetails();
								sharingDetails.setUserId(Integer.parseInt(userId));
								sharingDetails.setSharingUserId(Integer.parseInt(userId));
								sharingDetails.setSharingGroupId(0);
								sharingDetails.setListId(Integer.parseInt(listId));
								sharingDetails.setListType(mnList.getListType());
								sharingDetails.setNoteId(Integer.parseInt(noteId));
								sharingDetails.setSharingLevel("individual");
								sharingDetails.setNoteLevel("edit");
								sharingDetails.setShareAllContactFlag(false);
								sharingDetails.setSharedDate(formatter.format(date));
								sharingDetails.setStatus("A");
								sharingDetails.setSharedBookId(newListId);
								mongoOperations.insert(sharingDetails,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
								
							}
					}
				}
			}
			
			
			
			
			
			// Adding New List
			mongoOperations.insert(newList, JavaMessages.Mongo.MNLIST);
//kishore new 
			Query queryAssocite = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("sharedBookId").is(Integer.parseInt(listId))
					.and("listType").is(newList.getListType()).and("status").is("A").and("sharingLevel").is("individual").and("sharingUserId").is(Integer.parseInt(userId)));
			List<MnNotesSharingDetails> details = mongoOperations.find(queryAssocite, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			if(details!= null && !details.isEmpty()){
				Date date=new Date();
				for(MnNotesSharingDetails details2 :details){
					MnNotesSharingDetails sharingDetails = new MnNotesSharingDetails();
					sharingDetails.setUserId(Integer.parseInt(userId));
					sharingDetails.setSharingUserId(Integer.parseInt(userId));
					sharingDetails.setSharingGroupId(0);
					sharingDetails.setListId(details2.getListId());
					sharingDetails.setListType(details2.getListType());
					sharingDetails.setNoteId(details2.getNoteId());
					sharingDetails.setSharingLevel("individual");
					sharingDetails.setNoteLevel("edit");
					sharingDetails.setShareAllContactFlag(false);
					sharingDetails.setSharedDate(formatter.format(date));
					sharingDetails.setStatus("A");
					sharingDetails.setSharedBookId(newListId);
					mongoOperations.insert(sharingDetails,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				}
			}
//end kishore
			moveNoteLog(newList, newList.getListName(),oldListName, " Copied from", userId);
		} catch (Exception e) {
			logger.error("Exception while Copying Note One List To Another List :"+ e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" copyList method returened successfully " );
		return newMnList;
	}

	@Override
	public String copyNote(String listId, String noteId, String selectedListId,	String userId,boolean flagForLog) {
		String noteData = "";
		String dueDate = "";
		Date date = new Date();
		String oldnoteName=null;
		String newnoteName=null;
		boolean copyExist=false;
		String tagIds=null;
		String reminderId=null;
		if(logger.isDebugEnabled())
			logger.debug("copyNote method called in Impl :"+userId+" noteId: "+noteId);
		try {
			// Fetch Selected Note From List
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);

			String selectedNoteDeatils = null;
			String tempNoteDetails = null;
			JSONObject jsonObject = null;
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					selectedNoteDeatils = note;
					tempNoteDetails = note;
					jsonObject = new JSONObject(note);
					dueDate = (String) jsonObject.get("dueDate");
					oldnoteName=changedNoteNameWithDouble((String) jsonObject.get("noteName"));
					tagIds=(String) jsonObject.get("tag");
					reminderId=(String) jsonObject.get("remainders");
					
				}
			}
			// Adding Selected List to Selected Note
			
			Query addQuery=new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("listType").is(mnList.getListType()).and("defaultNote").is(true));
			MnList addNote = mongoOperations.findOne(addQuery, MnList.class,JavaMessages.Mongo.MNLIST);
			
			//copy number
			int i=1;
			for (String note : addNote.getMnNotesDetails()) {
				JSONObject existNote = new JSONObject(note);
				if (note.contains("\"copyFromMember\":\"" + userId + "\"")&& note.contains("\"status\":\"A\"")) {
					newnoteName=changedNoteNameWithDouble((String) existNote.get("noteName"));
					if(newnoteName.equals(oldnoteName))
					{
						
					}
					else
					{
					int position=newnoteName.lastIndexOf("-copy");
					String noteNameArr=newnoteName;
						if(position!=-1)
						{
							noteNameArr=newnoteName.substring(0,position);
						}
					if(noteNameArr.equals(oldnoteName))
					{
						copyExist=true;
						i=i+1;
					}
					}
				}
			}
			
			if(copyExist)
				newnoteName=oldnoteName+"-copy("+i+")";
			else
				newnoteName=oldnoteName+"-copy";
			
			// entry in comments table
			List<MnComments> mnCommentsList = null;
			JSONObject jsonObject2 = null;
			List<Integer> cmtList = new ArrayList<Integer>();
			int cId = 0;
			
				jsonObject2 = new JSONObject(tempNoteDetails);
				String commenntString =(String) jsonObject2.get("comments");
				if (commenntString.lastIndexOf(']') != -1) {
					commenntString = commenntString.substring(0, commenntString.lastIndexOf(']'));
				}
				if (commenntString.lastIndexOf('[') != -1) {
					commenntString = commenntString.substring( commenntString.lastIndexOf('[')+1, commenntString.length());
				}
				String[] comts = commenntString.split(",");
				
				for(String str:comts){
					if(str!=null && !str.isEmpty())
						cmtList.add(Integer.parseInt(str.trim()));
				}
				Query query2 = new Query(Criteria.where("cId").in(cmtList));
				mnCommentsList = mongoOperations.find(query2,MnComments.class, JavaMessages.Mongo.MNCOMMETS);
				
				
				List<MnComments> mnCommentsListTot = mongoOperations.findAll(MnComments.class, JavaMessages.Mongo.MNCOMMETS);
				if (mnCommentsListTot != null && mnCommentsListTot.size() != 0) {
					cId = mnCommentsListTot.size() + 1;
				} else {
					cId = 1;
				}
			
			int size = addNote.getMnNotesDetails().size() + 1;
			selectedNoteDeatils = selectedNoteDeatils.replace("\"noteId\":\""+ noteId + "\"", "\"noteId\":\"" + size + "\"");
			selectedNoteDeatils = selectedNoteDeatils.replace("\"noteName\":\""+ oldnoteName + "\"", "\"noteName\":\"" + newnoteName + "\"");
			selectedNoteDeatils = selectedNoteDeatils.replace("\"notesMembers\":\""+ (String) jsonObject.get("notesMembers") + "\"", "\"notesMembers\":\"\"");
			selectedNoteDeatils = selectedNoteDeatils.replace("\"noteGroups\":\""+ (String) jsonObject.get("noteGroups") + "\"", "\"noteGroups\":\"\"");
			selectedNoteDeatils = selectedNoteDeatils.replace("\"noteSharedAllContact\":"+ (Integer) jsonObject.get("noteSharedAllContact") , "\"noteSharedAllContact\":0");
			selectedNoteDeatils = selectedNoteDeatils.replace("\"copyToMember\":\""+ (String) jsonObject.get("copyToMember") + "\"", "\"copyToMember\":\"\"");
			selectedNoteDeatils = selectedNoteDeatils.replace("\"copyToGroup\":\""+ (String) jsonObject.get("copyToGroup") + "\"", "\"copyToGroup\":\"\"");
			selectedNoteDeatils = selectedNoteDeatils.replace("\"copyToAllContact\":"+ (Integer) jsonObject.get("copyToAllContact") , "\"copyToAllContact\":0");
			selectedNoteDeatils = selectedNoteDeatils.replace("\"publicUser\":\""+ (String) jsonObject.get("publicUser") + "\"", "\"publicUser\":\"\"");
			selectedNoteDeatils = selectedNoteDeatils.replace("\"publicDate\":\""+ (String) jsonObject.get("publicDate") + "\"", "\"publicDate\":\"\"");
			selectedNoteDeatils = selectedNoteDeatils.replace("\"access\":\""+ (String) jsonObject.get("access") + "\"", "\"access\":\"private\"");
			
			//insert into comments in cmd table
			try{
				if(cmtList!= null && !cmtList.isEmpty()){
					cmtList.clear();
				}
				for(MnComments comments : mnCommentsList){
					comments.setcId(cId);
					cmtList.add(cId);
					cId++;
					comments.setListId(addNote.getListId().toString());
					comments.setNoteId(Integer.valueOf(size).toString());
					comments.setCommLevel("I");
				}
				mongoOperations.insert(mnCommentsList, JavaMessages.Mongo.MNCOMMETS);
				
			}catch (Exception e) {
				logger.error(e);
			}
			
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"comments\":\""+(String) jsonObject.getString("comments")+"\"", "\"comments\":\""+cmtList.toString()+"\"");
			selectedNoteDeatils = selectedNoteDeatils.replace("\"pcomments\":\""+(String) jsonObject.getString("pcomments")+"\"", "\"pcomments\":\""+cmtList.toString()+"\"");
			if (selectedNoteDeatils.contains("\"copyFromMember\":\"\""))
				selectedNoteDeatils = selectedNoteDeatils.replace("\"copyFromMember\":\"\"","\"copyFromMember\":\"" + userId + "\"");
			else
				selectedNoteDeatils = selectedNoteDeatils.replace(noteFromCopyUpdateConvertToJsonObject(selectedNoteDeatils),"\"copyFromMember\":\"" + userId + "\"");
			
			// entry in reminder table
			if(reminderId!=null && !reminderId.equals(""))
			{
			if (reminderId.lastIndexOf(']') != -1) {
				reminderId = reminderId.substring(0, reminderId.lastIndexOf(']'));
			}
			if (reminderId.lastIndexOf('[') != -1) {
				reminderId = reminderId.substring( reminderId.lastIndexOf('[')+1, reminderId.length());
			}
			
			
			Query query3 = new Query(Criteria.where("rId").is(Integer.parseInt(reminderId)));
			MnRemainders mnRemin = mongoOperations.findOne(query3,MnRemainders.class, JavaMessages.Mongo.MNREMAINDERS);
			int rId=0;
			
			List<MnRemainders> mnReminListTot = mongoOperations.findAll(MnRemainders.class, JavaMessages.Mongo.MNREMAINDERS);
			if (mnReminListTot != null && mnReminListTot.size() != 0) {
				rId = mnReminListTot.size() + 1;
			} else {
				rId = 1;
			}
			
			MnRemainders mnRemainders =new MnRemainders();
			mnRemainders.setrId(rId);               
			mnRemainders.setListId(addNote.getListId().toString());
			mnRemainders.setNoteId(Integer.valueOf(size).toString());
			mnRemainders.setUserId(Integer.parseInt(userId));
			mnRemainders.setrName(mnRemin.getrName());
			mnRemainders.setStatus(mnRemin.getStatus());
			mnRemainders.setcDate(mnRemin.getcDate());
			mnRemainders.setEventDate(mnRemin.getEventDate());
			mnRemainders.setEventTime(mnRemin.getEventTime());
			mnRemainders.setSendMail(mnRemin.isSendMail());
			mnRemainders.setEdited(mnRemin.getEdited());
			mnRemainders.setEditDate(mnRemin.getEditDate());
			mnRemainders.setInactiveRemainderUserId(new ArrayList<Integer>());
			mongoOperations.insert(mnRemainders,JavaMessages.Mongo.MNREMAINDERS);
			}

			if (addNote.getMnNotesDetails() != null	&& addNote.getMnNotesDetails().isEmpty()) {
				addNote.setMnNotesDetails(new HashSet<String>());
				addNote.getMnNotesDetails().add(selectedNoteDeatils);
			} else {
				addNote.getMnNotesDetails().add(selectedNoteDeatils);
			}
			Update addUpdate = new Update();
			addUpdate.set("mnNotesDetails", addNote.getMnNotesDetails());
			mongoOperations.updateFirst(addQuery, addUpdate,JavaMessages.Mongo.MNLIST);
			
			// associate note entry in noteSharingdetail table
			
			if(!(selectedListId.equals(addNote.getListId().toString())))
			{
				MnNotesSharingDetails sharingDetails = new MnNotesSharingDetails();
				sharingDetails.setUserId(Integer.parseInt(userId));
				sharingDetails.setSharingUserId(Integer.parseInt(userId));
				sharingDetails.setSharingGroupId(0);
				sharingDetails.setListId(Integer.parseInt(listId));
				sharingDetails.setListType(mnList.getListType());
				sharingDetails.setNoteId(size);
				sharingDetails.setSharingLevel("individual");
				sharingDetails.setNoteLevel("edit");
				sharingDetails.setShareAllContactFlag(false);
				sharingDetails.setSharedDate(formatter.format(date));
				sharingDetails.setStatus("A");
				sharingDetails.setSharedBookId(Integer.parseInt(selectedListId));
			
				mongoOperations.insert(sharingDetails,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				
			}
			
			// tag entry for tagDetails table.
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			
			if (tagIds.lastIndexOf(']') != -1) {
				tagIds = tagIds.substring(0, tagIds.lastIndexOf(']'));
			}
			if (tagIds.lastIndexOf('[') != -1) {
				tagIds = tagIds.substring( tagIds.lastIndexOf('[')+1, tagIds.length());
			}
			String[] tag = tagIds.split(",");
			
			List<MnTagDetails> listTag=new ArrayList<MnTagDetails>();
			
			for(String str:tag){
				if(str!=null && !str.equals(""))
				{
			MnTagDetails mnTagDetails=new MnTagDetails();
			mnTagDetails.setTagId(Integer.parseInt(str.trim()));
			mnTagDetails.setUserId(Integer.parseInt(userId));
			mnTagDetails.setListId(Integer.parseInt(addNote.getListId().toString()));
			mnTagDetails.setNoteId(size);
			mnTagDetails.setTagedDate(sdf.format(date));
			mnTagDetails.setListType(addNote.getListType());
			mnTagDetails.setStatus("A");
			listTag.add(mnTagDetails);
				}
			}
			mongoOperations.insert(listTag,JavaMessages.Mongo.MNTAGDETAILS);
			
			
			//Make Copy Base Add Note Details In Mn_NoteDetails Table Also - Venu
			if(!addNote.getListType().equals("schedule")){
				query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
				MnNoteDetails details = mongoOperations.findOne(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
				if(details!=null){
					MnNoteDetails newDetails=new MnNoteDetails();
					newDetails.setSharedIndividuals(details.getSharedIndividuals());
					newDetails.setSharedGroups(details.getSharedGroups());
					newDetails.setShareAllContactFlag(details.isShareAllContactFlag());
					newDetails.setStatus(details.getStatus());
					newDetails.setOwnerNoteStatus(details.getOwnerNoteStatus());
					newDetails.setTag(details.getTag());
					newDetails.setRemainders(details.getRemainders());
					newDetails.setVote(details.getVote());
					newDetails.setLinks(details.getLinks());
					newDetails.setEndDate(details.getEndDate());
					newDetails.setDueDate(details.getDueDate());
					newDetails.setDueTime(details.getDueTime());
					newDetails.setAccess(details.getAccess());
					newDetails.setAttachFilePath(details.getAttachFilePath());
					newDetails.setDescription(details.getDescription());
					newDetails.setComments(details.getComments());
					newDetails.setPcomments(details.getPcomments());
					newDetails.setPublicUser(details.getPublicUser());
					newDetails.setPublicDate(details.getPublicDate());
					newDetails.setShareType(details.getShareType());
					newDetails.setPrivateTags(details.getPrivateTags());
					newDetails.setNoteName(newnoteName);
					newDetails.setUserId(Integer.parseInt(userId));
					newDetails.setStartDate(date);
					newDetails.setNoteSharedAllContact(false);
					newDetails.setStatus("A");
					newDetails.setOwnerNoteStatus("A");
					newDetails.setAccess("private");
					newDetails.setCopyFromMember(userId);
					newDetails.setListId(addNote.getListId());
					newDetails.setListName(addNote.getListName());
					newDetails.setListType(addNote.getListType());
					newDetails.setNoteAccess(addNote.getNoteAccess());
					newDetails.setDefaultNote(addNote.isDefaultNote());
					newDetails.setEnableDisableFlag(addNote.isEnableDisableFlag());
					newDetails.setNoteId(size);
				
					mongoOperations.insert(newDetails,JavaMessages.Mongo.MNNOTEDETAILS);
				}
			}
			//End -Venu
			
			if(!dueDate.equals(""))
			dueDateUpdateBasedOnListMoveCopy(selectedListId,size,userId,dueDate,"A");

			if(flagForLog){
				moveNoteLog(tempNoteDetails, mnList, addNote.getListName(),"Copied <B>"+changedNoteNameWithDouble((String) jsonObject.get("noteName"))+"</B> from", userId,false);
			}
			noteData = selectedNoteDeatils;
		} catch (Exception e) {
			logger.error("Exception while copyNote method :"+ e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" copyNote method returened successfully " );
		return noteData;
	}

	
	public void dueDateUpdateBasedOnListMoveCopy(String listId, int noteId ,String userId, String dueDate,String status)
	{
		if(logger.isDebugEnabled())
			logger.debug("dueDateUpdateBasedOnListMoveCopy method called in Impl :"+userId+" dueDate: "+dueDate);
		try
		{
		if(status.contains("A"))
		{
			MnNoteDueDateDetails details = new MnNoteDueDateDetails();
			details.setUserId(Integer.parseInt(userId));
			details.setListId(Integer.parseInt(listId));
			details.setNoteId(noteId);
			details.setListType("music");
			details.setDueDate(dueDate);
			details.setStatus(status);
			mongoOperations.insert(details,	JavaMessages.Mongo.MNNOTEDUEDATEDETAILS);
		}
		else
		{
				Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("userId").is(Integer.parseInt(userId)).and("noteId").is(noteId).and("dueDate").is(dueDate));
				Update update = new Update();
				update.set("status", "I");
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNNOTEDUEDATEDETAILS);
		}
		if(logger.isDebugEnabled())
			logger.debug(" dueDateUpdateBasedOnListMoveCopy method returened successfully " );
		}catch (Exception e) {
			logger.error(" error in dueDateUpdateBasedOnListMoveCopy :"+e.getMessage());
		}
	}
	
	@Override
	public String updateAttachedFileForNote(String fileName, String listId,String noteId, String userId) {
		Set<String> updateNoteDetails=new TreeSet<String>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		Date todayDate = new Date(); 
		Date todatTime = new Date();
		if(logger.isDebugEnabled())
			
			logger.debug("updateAttachedFileForNote method called in Impl :"+userId+" fileName: "+fileName);
		try {
			Integer size = 0;
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,
					JavaMessages.Mongo.MNLIST);
			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			List<Integer> oldFileIdList = null;
			List<Integer> newFileIdList = null;
			boolean fromLast=false;
			int conut=0;
			String newfile = "";
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					removeNoteDeatils = note;
					JSONObject json= new JSONObject(note);
					String attString = json.getString("attachFilePath");
					if(attString!= null && !attString.equals("")){
						attString = attString.replace("[", "");
						attString = attString.replace("]", "");
						String[] oldAttaches= attString.split(",");
						conut =  oldAttaches.length;
						if(oldAttaches.length > 0 && !oldAttaches[0].trim().equals("")){
							oldFileIdList = new ArrayList<Integer>();
							for(String strId :oldAttaches ){
								oldFileIdList.add(Integer.parseInt(strId.trim()));
							}
						}
					}
					List<MnAttachmentDetails> mnAttachmentDetailsList = null;
					if (!fileName.equals("")) {
						int attachId = 0;
						Query query1=new Query();
						query1.sort().on("attachId", Order.ASCENDING);
						List<MnAttachmentDetails> attachmentDetailsList = mongoOperations.find(query1,MnAttachmentDetails.class,JavaMessages.Mongo.MNATTACHMENTDETAILS);
						if (attachmentDetailsList != null && attachmentDetailsList.size() != 0)
						{
							attachId = attachmentDetailsList.size() + 1;
						}
						else
						{
							attachId= 1;
						}
						String[] fileAttchaed = fileName.split(",");
						mnAttachmentDetailsList = new ArrayList<MnAttachmentDetails>(); 
						if (fileAttchaed.length > 0) {
							newFileIdList = new ArrayList<Integer>(); 
							for (int i = 0; i < fileAttchaed.length; i++) {
								
								Query query2 = new Query(Criteria.where("fileName").is(fileAttchaed[i].trim()).and("status").is("A").
										and("listId").is(Integer.parseInt(listId)).and("noteId").is(noteId));
								
								MnAttachmentDetails attachmentDetails2 = mongoOperations.findOne(query2,MnAttachmentDetails.class,JavaMessages.Mongo.MNATTACHMENTDETAILS);
								if(attachmentDetails2 == null){
									newfile = newfile + fileAttchaed[i]+",";
									
									//Query query21=new Query(Criteria.where("fileName").is(fileAttchaed[i].trim()));
									
									Query query21 = new Query(Criteria.where("status").is("A").orOperator(
											Criteria.where("fileName").is(fileAttchaed[i].trim()),
											Criteria.where("fullPath").is(fileAttchaed[i].trim())));
									
									MnVideoFile mnVideoFile= mongoOperations.findOne(query21,MnVideoFile.class,JavaMessages.Mongo.MnFiles);
									
									if(mnVideoFile==null)
									{
										mnVideoFile= mongoOperations.findOne(query21,MnVideoFile.class,JavaMessages.Mongo.MnVideoFile);	
									}
									
									attachmentDetails2 = new MnAttachmentDetails();
									attachmentDetails2.setFullPath(mnVideoFile.getFullPath());
									attachmentDetails2.setAttachId(attachId);
									attachmentDetails2.setFileId(mnVideoFile.getId());
									attachmentDetails2.setFileName(fileAttchaed[i].trim()) ;
									attachmentDetails2.setaDate(dateFormat.format(todayDate));
									attachmentDetails2.setaTime(timeFormat.format(todatTime));
									attachmentDetails2.setListId(Integer.parseInt(listId));
									attachmentDetails2.setNoteId(noteId);
									attachmentDetails2.setStatus("A");
									attachmentDetails2.setUploadDate(mnVideoFile.getUploadDate());

									String fullName=fileAttchaed[i].trim();
									
									
									String splitName[]=mnVideoFile.getFileName().split("_");
									attachmentDetails2.setFileUploadName(splitName[0]+"."+mnVideoFile.getFileType());
									Date date=new Date();
									SimpleDateFormat simDateFormat=new SimpleDateFormat("M/dd/yyyy h:m:s a");
									attachmentDetails2.setCurrentTime(simDateFormat.format(date));
									
									attachmentDetails2.setUserId(Integer.parseInt(userId));
									
									newFileIdList.add(attachmentDetails2.getAttachId());
									
									attachId++;
									mnAttachmentDetailsList.add(attachmentDetails2);
								}else{
									newFileIdList.add(attachmentDetails2.getAttachId());
								}
							}
						}
					}
					
					if(mnAttachmentDetailsList!= null && !mnAttachmentDetailsList.isEmpty()){
						mongoOperations.insert(mnAttachmentDetailsList,JavaMessages.Mongo.MNATTACHMENTDETAILS);
					}
					
					if(newFileIdList!= null && !newFileIdList.isEmpty()){
						attString = newFileIdList.toString();
					}else{
						attString ="";
						fromLast=true;
					}
					if (note.contains("\"attachFilePath\":\"\"")) {
						note = note.replace("\"attachFilePath\":\"\"","\"attachFilePath\":\"" + attString + "\"");
					} else {
						note = note.replace(noteAttachedFileConvertToJsonObject(note),"\"attachFilePath\":\"" + attString + "\"");
					}

					tempNoteDetails = note;
				}
				updateNoteDetails.add(note);
			}
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			
			Query detailsQuery = new Query(Criteria.where("listId").is(mnList.getListId()).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
			MnNoteDetails mnNoteDetails =mongoOperations.findOne(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
			if(mnNoteDetails!= null ){
				Update update1 = new Update();
				if(newFileIdList!= null && !newFileIdList.isEmpty() ){
					mnNoteDetails.setAttachFilePath(newFileIdList);
				}else{
					mnNoteDetails.setAttachFilePath(new ArrayList<Integer>());
				}
				update1.set("attachFilePath",mnNoteDetails.getAttachFilePath());
				
				mongoOperations.updateFirst(detailsQuery,update1,JavaMessages.Mongo.MNNOTEDETAILS);
			}
			String removeFile="";
			if(oldFileIdList!= null && !oldFileIdList.isEmpty()){
				for(Integer it :oldFileIdList ){
					if(fromLast)
					{
						Query query2 = new Query(Criteria.where("attachId").is(it).and("status").is("A"));
						MnAttachmentDetails attachmentDetails2 = mongoOperations.findOne(query2,MnAttachmentDetails.class,JavaMessages.Mongo.MNATTACHMENTDETAILS);
						removeFile = removeFile + attachmentDetails2.getFileName()+",";
						Update update1 = new Update();
						update1.set("status", "I");
						update1.set("endDate",dateFormat.format(todayDate));
						update1.set("endTime",timeFormat.format(todatTime));
						mongoOperations.updateFirst(query2, update1,JavaMessages.Mongo.MNATTACHMENTDETAILS);
					}else
					{
					if(!newFileIdList.contains(it)){
						Query query2 = new Query(Criteria.where("attachId").is(it).and("status").is("A"));
						MnAttachmentDetails attachmentDetails2 = mongoOperations.findOne(query2,MnAttachmentDetails.class,JavaMessages.Mongo.MNATTACHMENTDETAILS);
						removeFile = removeFile + attachmentDetails2.getFileName()+",";
						Update update1 = new Update();
						update1.set("status", "I");
						update1.set("endDate",dateFormat.format(todayDate));
						update1.set("endTime",timeFormat.format(todatTime));
						mongoOperations.updateFirst(query2, update1,JavaMessages.Mongo.MNATTACHMENTDETAILS);
					}
					}
				}
			}
			if(!removeFile.equals("")){
				if(removeFile.lastIndexOf(",") != -1)
					removeFile = removeFile.substring(0, removeFile.lastIndexOf(","));
				
				if(removeFile.length() > 36){
					removeFile = "<B>"+ removeFile.substring(0,35)+"</B>...";
				}else{
					removeFile = "<B>"+ removeFile+"</B>";
				}
				commomLog(tempNoteDetails, mnList, "Detached a file: ", true, userId,false,false,true,": "+removeFile,false,"");
			}
			
			if (!newfile.equals("")) {
				if(newfile.lastIndexOf(",") != -1){
					newfile = newfile.substring(0,newfile.lastIndexOf(","));
				}
				if(newfile.length() > 36){
					newfile = "<B>"+ newfile.substring(0,35)+"</B>...";
				}else{
					newfile = "<B>"+ newfile+"</B>";
				}
				
				commomLog(tempNoteDetails, mnList, "Attached a file: ", true, userId,false,false,true,": "+newfile,false,"");
			}
			
			if(logger.isDebugEnabled())
				logger.debug(" updateAttachedFileForNote method returened successfully " );
			
			return "" + size;
		} catch (Exception e) {
			logger.error("Exception while updateAttachedFileForNote :" + e);
		}
		return "Exception while Updating Due date For Note";
	}

	public String noteAttachedFileConvertToJsonObject(String jsonStr) {
		
		if(logger.isDebugEnabled())
			logger.debug("noteAttachedFileConvertToJsonObject method called in Impl :");
		
		String attach = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String attachFilePath = (String) jsonObject
						.get("attachFilePath");
				attach = "\"attachFilePath\":\"" + attachFilePath + "\"";
			} catch (Exception e) {
				logger.error("Exception while converting json to Note Duedate object In Impl !",e);
			}
		}
		
		return attach;
	}
	public List<MnAttachmentDetails> getAttachedFileInNote(String listId,String noteId,String userId){
		List<MnAttachmentDetails> attachmentDetails = null;
		List<MnAttachmentDetails> attachmentDetailsReturn = new ArrayList<MnAttachmentDetails>();
		String loginUserZone=null;
		if(logger.isDebugEnabled())
			logger.debug("getAttachedFileInNote method called in Impl :"+userId+" noteId: "+noteId);
		try {
			String attachFilePath = "";
			
			Query query3 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
   			MnUsers mnUser = mongoOperations.findOne(query3, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			loginUserZone=mnUser.getTimeZone();
   			
   			
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					JSONObject jsonObject;
					try {
						jsonObject = new JSONObject(note);
						attachFilePath = (String) jsonObject.get("attachFilePath");
						
						if(attachFilePath!= null && !attachFilePath.equals("")){
							if(attachFilePath.indexOf("[")!= -1){
								attachFilePath = attachFilePath.replace("[", "");
							}
							if(attachFilePath.indexOf("]")!= -1){
								attachFilePath = attachFilePath.replace("]", "");
							}
							String[] attachFileArray = attachFilePath.split(",");
							List<Integer> attachedId = new ArrayList<Integer>();
							if(attachFileArray.length > 0 && attachFileArray[0] != null && !attachFileArray[0].equals("") ){
								for(String str : attachFileArray){
									attachedId.add(Integer.parseInt(str.trim()));
								}
							}
							
							
							Query arg1 = new Query(Criteria.where("attachId").in(attachedId));
							attachmentDetails = mongoOperations.find(arg1,  MnAttachmentDetails.class,JavaMessages.Mongo.MNATTACHMENTDETAILS);
							
						}
					} catch (Exception e) {
						logger.error("Exception while converting json to Note Attached file In Impl !",e);

					}
				}
			}
			
			if(attachmentDetails!=null && !attachmentDetails.isEmpty())
			{
			for(MnAttachmentDetails mnAttach:attachmentDetails)
			{
				// *************** change time zone ************* ///
				DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
					Date date=null;
					String d=mnAttach.getUploadDate();
					date=formatter.parse(d);
					
					Calendar c = Calendar.getInstance();
					c.setTime(date);
					TimeZone obj = TimeZone.getTimeZone("GMT"+loginUserZone);
				formatter.setTimeZone(obj);
				String convertedDate=formatter.format(c.getTime());
				date=formatter.parse(convertedDate);
				mnAttach.setUploadDate(convertedDate);
				attachmentDetailsReturn.add(mnAttach);
				//****************************//
			}
			}
			
			
		}catch (Exception e) {
			logger.error("Exception while get Attached File In Note & List In Impl !",e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" getAttachedFileInNote method returened successfully " );
		
		return attachmentDetailsReturn;
	}
	@Override
	public String getAttachedFileForNote(String listId, String noteId) {
		if(logger.isDebugEnabled())
			logger.debug("getAttachedFileForNote method called in Impl noteId: "+noteId);
		try {
			String attachFilePath = "";
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					JSONObject jsonObject;
					try {
						jsonObject = new JSONObject(note);
						attachFilePath = (String) jsonObject.get("attachFilePath");
						
						if(attachFilePath!= null && !attachFilePath.equals("")){
							if(attachFilePath.indexOf("[")!= -1){
								attachFilePath = attachFilePath.replace("[", "");
							}
							if(attachFilePath.indexOf("]")!= -1){
								attachFilePath = attachFilePath.replace("]", "");
							}
							String[] attachFileArray = attachFilePath.split(",");
							List<Integer> attachedId = new ArrayList<Integer>();
							if(attachFileArray.length > 0 && attachFileArray[0] != null && !attachFileArray[0].equals("") ){
								for(String str : attachFileArray){
									attachedId.add(Integer.parseInt(str.trim()));
								}
							}
							
							Query arg1 = new Query(Criteria.where("attachId").in(attachedId));
							List<MnAttachmentDetails> attachmentDetails = mongoOperations.find(arg1,  MnAttachmentDetails.class,JavaMessages.Mongo.MNATTACHMENTDETAILS);
							attachFilePath = "";
							if(attachmentDetails!= null && !attachmentDetails.isEmpty()){
								for(MnAttachmentDetails details : attachmentDetails){
									attachFilePath = attachFilePath + details.getFileName()+","; 
								}
								attachFilePath = attachFilePath.substring(0,attachFilePath.lastIndexOf(","));
							}
						}else{
							attachFilePath="";
						}
					} catch (Exception e) {
						logger.error("Exception while converting json to Note Attached file In Impl !",e);

					}
				}
			}
			
			if(logger.isDebugEnabled())
				logger.debug(" getAttachedFileForNote method returened successfully " );
			
			return attachFilePath;
		} catch (Exception e) {
			logger.error("Exception while Updating Due date For Note :" + e);
		}
		return "Exception while get Attached File For Note";
	}

	public String noteDetails(String listId, String noteId, String type, String userId) {
		String notes = "";
		if(logger.isDebugEnabled())
			logger.debug("noteDetails method called in Impl :"+userId+" noteId: "+noteId);
		try {
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			Integer sharingUserId=mnList.getUserId();
			
			for (String note : mnList.getMnNotesDetails()) {
				if (!note.isEmpty()){
				if(sharingUserId!=Integer.parseInt(userId)){
				JSONObject jsonObject;
				jsonObject = new JSONObject(note);
				String dueDate=(String) jsonObject.get("dueDate");
				String dueTime=(String) jsonObject.get("dueTime");
				 SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
			       SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
			       if((dueTime!=null)&&(!dueTime.isEmpty())){
			       Date date1 = parseFormat.parse(dueTime);
			       dueTime=displayFormat.format(date1);
			       
			       String date=dueDate+" "+dueTime;
					String stDate = convertedZoneEvents(Integer.parseInt(userId),sharingUserId, date);
					String arr[]=stDate.split(" ");
					
					SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
				    Date dateObj = sdf.parse(arr[1]);
				    String time=new SimpleDateFormat("hh:mm a").format(dateObj);
				    note=note.replace("\"dueTime\":\"" + (String) jsonObject.get("dueTime")+ "\"","\"dueTime\":\"" + time+ "\"");
				    note=note.replace("\"dueDate\":\"" + dueDate+ "\"","\"dueDate\":\"" + arr[0]+ "\"");
				}
				}
				
				if (type.equalsIgnoreCase("schedule")) {
					if (note.contains("\"eventId\":\"" + noteId + "\"")) {
						notes = note;
						break;
					}
				} else {
					if (note.contains("\"noteId\":\"" + noteId + "\"")) {
						notes = note;
						break;
					}
				}
			}
		}
			if(notes!=null && !notes.isEmpty()){
				//Share Individual
				String[] memberIds= notesSharingDetailsConvertToJsonObject(notes).split(","); 
				String membersList="";
				query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("userId").is(Integer.parseInt(userId)).and("sharingGroupId").is(0).and("shareAllContactFlag").is(false).and("status").is("A"));
				List<MnNotesSharingDetails> details = mongoOperations.find(query, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				
				
				if (details!=null && !details.isEmpty())
				{
					for(int i=0;i<memberIds.length;i++)
					{
						if(!memberIds[i].isEmpty()){
							for(MnNotesSharingDetails sharingDetails:details){
								if(sharingDetails.getSharingUserId() == Integer.parseInt(memberIds[i])){
									
									if(membersList.isEmpty())
										membersList=membersList+memberIds[i];
									else
										membersList=membersList+","+memberIds[i];
								}
							}
						}
					}
					membersList="["+membersList+"]";
					if (type.equalsIgnoreCase("schedule")) {
						notes = notes.replace(eventMembersUpdateConvertToJsonObject(notes),"\"eventMembers\":\"" + membersList+ "\"");
					}else{
						notes = notes.replace(notesMembersUpdateConvertToJsonObject(notes),"\"notesMembers\":\"" + membersList+ "\"");
					
					}
				}else{
					if (type.equalsIgnoreCase("schedule")) {
						notes = notes.replace(eventMembersUpdateConvertToJsonObject(notes),"\"eventMembers\":\"\"");
					}else{
						notes = notes.replace(notesMembersUpdateConvertToJsonObject(notes),"\"notesMembers\":\"\"");
					}
				}
				//Share Group
				String[] groupIds= notesSharingGroupDetailsConvertToJsonObject(notes).split(","); 
				String groupsList="";
				query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("userId").is(Integer.parseInt(userId)).and("sharingGroupId").ne(0).and("shareAllContactFlag").is(false).and("status").is("A"));
				List<MnNotesSharingDetails> groupDetails = mongoOperations.find(query, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				if (groupDetails!=null && !groupDetails.isEmpty())
				{
					for(int i=0;i<groupIds.length;i++)
					{
						if(!groupIds[i].isEmpty()){
							for(MnNotesSharingDetails sharingDetails:groupDetails){
								if(sharingDetails.getSharingGroupId() == Integer.parseInt(groupIds[i])){
									if(groupsList.isEmpty())
										groupsList=groupsList+groupIds[i];
									else
										groupsList=groupsList+","+groupIds[i];
								}
							}
						}
					}
					groupsList="["+groupsList+"]";
					if (type.equalsIgnoreCase("schedule")) {
						notes = notes.replace(eventGroupsUpdateConvertToJsonObject(notes,true),"\"eventGroups\":\"" + groupsList+ "\"");
					}else{
						notes = notes.replace(noteGroupsUpdateConvertToJsonObject(notes,true),"\"noteGroups\":\"" + groupsList+ "\"");
					}
				}else{
					if (type.equalsIgnoreCase("schedule")) {
						notes = notes.replace(eventGroupsUpdateConvertToJsonObject(notes,true),"\"eventGroups\":\"\"");
					}else{
						notes = notes.replace(noteGroupsUpdateConvertToJsonObject(notes,true),"\"noteGroups\":\"\"");
					}
				}
			}
		} catch (Exception e) {
			logger.error("Exception while note Details In Impl !", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" noteDetails method returened successfully " );
		return notes;
	}
	
	
	@Override
	public String listDetails(String listId,String type)
	{
		if(logger.isDebugEnabled())
			logger.debug("listDetails method called in Impl and listId: "+listId);
		String status="";
		MnList mnList=null;
		List member=null;
		try
		{
			Query query=new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
			 mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			 
			 if(mnList!=null)
			 {
				 if(type.equalsIgnoreCase("member"))
					 member= mnList.getSharedIndividuals();
				 else
					member= mnList.getSharedGroups();
				 
				 status= member.toString();
			 }
		}catch (Exception e) {
			logger.error("Exception while listDetails In Impl !", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" listDetails method returened successfully " );
		return status;
	}

	public String updateNoteName(String listId, String noteId, String noteName,String userId,String type) 
	{
		if(logger.isDebugEnabled())
			logger.debug("updateNoteName method called in Impl :"+userId+" noteId: "+noteId+" noteName: "+noteName);
		Integer size = 0;
		String oldNoteName = "";
		Set<String> updateNoteDetails = new TreeSet<String>();
		try {
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			
			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			for (String note : mnList.getMnNotesDetails()) 
			{
				JSONObject jsonObject;
				if(type.equalsIgnoreCase("schedule"))
				{
					if (note.contains("\"eventId\":\"" + noteId + "\"")) 
					{
						removeNoteDeatils = note;
						try {
							jsonObject = new JSONObject(note);
							oldNoteName = (String) jsonObject.get("eventName");
							if (note.contains("\"eventName\":\"\"")) {
								note = note.replace("\"eventName\":\"\"","\"eventName\":\"" + noteName + "\"");
							} else {
								if(oldNoteName.indexOf("\"") != -1){
									oldNoteName = oldNoteName.replace("\"", "\\\"");
								}
								note = note.replace("\"eventName\":\"" + oldNoteName+ "\"", "\"eventName\":\""+ noteName + "\"");
							}
							tempNoteDetails = note;
						} catch (Exception e) {
							logger.error("Exception while converting json to Note Attached file In Impl !",e);
						}
						
					}
					
					/*update in mn_events*/
					Query query3=new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("eventId").is(Integer.parseInt(noteId)).and("listType").is("schedule").and("status").is("A"));
					Update update=new Update();
					update.set("eventName", noteName);
					mongoOperations.updateFirst(query3, update, JavaMessages.Mongo.MNEVENTS);
	       
					/*Update Mn_subEvents*/
					Query query4=new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("eventId").is(Integer.parseInt(noteId)).and("status").is("A"));
					Update update4=new Update();
					update4.set("eventName", noteName);
					mongoOperations.updateMulti(query4, update4, JavaMessages.Mongo.MNESUBVENTS);
				}
				else
				{
					if (note.contains("\"noteId\":\"" + noteId + "\"")) 
					{
						removeNoteDeatils = note;
						try {
							jsonObject = new JSONObject(note);
							oldNoteName = (String) jsonObject.get("noteName");
							if (note.contains("\"noteName\":\"\"")) {
								note = note.replace("\"noteName\":\"\"","\"noteName\":\"" + noteName + "\"");
							} else {
								if(oldNoteName.indexOf("\"") != -1){
									oldNoteName = oldNoteName.replace("\"", "\\\"");
								}
								note = note.replace("\"noteName\":\"" + oldNoteName+ "\"", "\"noteName\":\""+ noteName + "\"");
							}
							tempNoteDetails = note;
						} catch (Exception e) {
							logger.error("Exception while converting json to Note Attached file In Impl !",e);
						}
						
					}
				}
				updateNoteDetails.add(note);
			}
			
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,	JavaMessages.Mongo.MNLIST);
			
			// Update Note Name Changes In Note Details Table
			if(noteName!=null && !noteName.isEmpty() && !type.equalsIgnoreCase("schedule")){
				query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)));
				update = new Update();
				update.set("noteName", noteName);
				mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNNOTEDETAILS);
			}
			
		
		commomLog(removeNoteDeatils, mnList, "Updated note title:<B>"+ noteName + "</B> from", true, userId,false,false,true,"",true,"");

		} catch (Exception e) {
			logger.error("Exception while update note name In Impl !", e);
		}
		
		if(logger.isDebugEnabled())
			logger.debug(" updateNoteName method returened successfully " );
		return "" + size;
	}

	@Override
	public String addNoteDescription(String listId, String noteId,String description, String userId) {
		String desc = "";
		Set<String> updateNoteDetails=new TreeSet<String>();
		if(logger.isDebugEnabled())
			logger.debug("addNoteDescription method called in Impl :"+userId+" noteId: "+noteId);
		try {
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					removeNoteDeatils = note;
					JSONObject jsonObject;
					try {
						jsonObject = new JSONObject(note);
						desc = (String) jsonObject.get("description");
						if (note.contains("\"description\":\"\"")) {
							note = note.replace("\"description\":\"\"","\"description\":\"" + description.trim()+ "\"");
						} else {
							note = note.replace("\"description\":\"" + desc+ "\"", "\"description\":\""+ description.trim() + "\"");
						}
						tempNoteDetails = note;
					} catch (Exception e) {
						logger.error("Exception while converting json to Note Attached file In Impl !",e);
					}
				}
				updateNoteDetails.add(note);
			}
			
			if (removeNoteDeatils != null&& !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);
			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			
			Query detailsQuery = new Query(Criteria.where("listId").is(mnList.getListId()).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
			MnNoteDetails mnNoteDetails =mongoOperations.findOne(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
			if(mnNoteDetails!= null ){
				Update update1 = new Update();
				update1.set("description", description.trim());
				mongoOperations.updateFirst(detailsQuery,update1,JavaMessages.Mongo.MNNOTEDETAILS);
			}

			if(!description.contains("<a") ){
				if(description.length() > 36){
					desc = description.trim().substring(0,35)+"...";
					while((desc.charAt(desc.length()) <= 65 && desc.charAt(desc.length()) >= 90) ||  
							(desc.charAt(desc.length()) <= 97 && desc.charAt(desc.length()) >= 120)){
						desc = desc.substring(0, desc.length()-1);
					}
				}else{
					desc = description.trim();
				}
			}else{
				desc = description.trim();
			}
			if(!desc.equals("")){
				desc = ": <B>["+desc+"]</B>";
				commomLog(tempNoteDetails, mnList, "Updated note description:",true, userId,false,false,true,desc,false,"");
			}else{
				desc="";
				commomLog(tempNoteDetails, mnList, "Removed note description:",true, userId,false,false,true,desc,false,"");
			}

		} catch (Exception e) {
			logger.error("Exception while addNoteDescription In Impl !", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" addNoteDescription method returened successfully " );
		
		return "" + description;
	}

	@Override
	public String getNote(String listId, String noteId, String userId) {
		String notes = "";
		String noteLevel ="";
		Query query=null;
		MnList mnList=null;
		String sharedStatus="";
		if(logger.isDebugEnabled())
			logger.debug("getNote method called in Impl :"+userId+" noteId: "+noteId);
		try {
			query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
			mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			
			
			if((mnList.isShareAllContactFlag() ||
					(mnList.getSharedIndividuals()!= null && !mnList.getSharedIndividuals().isEmpty() && mnList.getSharedIndividuals().indexOf(Integer.parseInt(userId.trim()))!= -1) ||
					mnList.getSharedGroups()!= null && !mnList.getSharedGroups().isEmpty() )){
				sharedStatus = sharedStatus + ",\"shared\":\"shared\"";
			}else{
				sharedStatus = sharedStatus + ",\"shared\":\"notshared\"";
			}
			
			query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("sharingUserId").is(Integer.parseInt(userId)).and("status").is("A"));

			MnNotesSharingDetails details = mongoOperations.findOne(query, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			if(details != null)
				noteLevel = details.getNoteLevel();
			else{
				//book level sharing
				//note view
					query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(0).and("sharingLevel").is("book").and("sharingUserId").is(Integer.parseInt(userId)).and("status").is("A"));
					details = mongoOperations.findOne(query, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
					if(details != null)
					{
						noteLevel = details.getNoteLevel();
						query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
						mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
						
						if((mnList.isShareAllContactFlag() ||
								(mnList.getSharedIndividuals()!= null && !mnList.getSharedIndividuals().isEmpty() && mnList.getSharedIndividuals().indexOf(Integer.parseInt(userId.trim()))!= -1) ||
								mnList.getSharedGroups()!= null && !mnList.getSharedGroups().isEmpty() )){
							sharedStatus = sharedStatus + ",\"shared\":\"shared\"";
						}else{
							sharedStatus = sharedStatus + ",\"shared\":\"notshared\"";
						}
						
						if(!(mnList.getMnNotesDetails() == null && !mnList.getMnNotesDetails().isEmpty()))
						{
							query = new Query(Criteria.where("userId").is(details.getUserId()).and("sharingUserId").is(details.getUserId()).and("listType").is(details.getListType()).and("sharingLevel").is("individual").and("sharedBookId").is(details.getListId()).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
							MnNotesSharingDetails detailsTemp= mongoOperations.findOne(query, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
							if(detailsTemp!=null)
							{
								query = new Query(Criteria.where("listId").is(detailsTemp.getListId()));
								mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
							}
						}
					}
					else
						// book view
					{
				query = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("listType").ne("schedule").and("sharingLevel").is("book").and("status").is("A"));
				List<MnNotesSharingDetails> shareList=mongoOperations.find(query, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				if(shareList!=null)
				{
					boolean findFlag=false;
					for(MnNotesSharingDetails listDetail:shareList)
					{
						noteLevel=listDetail.getNoteLevel();
						query = new Query(Criteria.where("userId").is(listDetail.getUserId()).and("sharingUserId").is(listDetail.getUserId()).and("listType").is(listDetail.getListType()).and("sharingLevel").is("individual").and("sharedBookId").is(listDetail.getListId()).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
						MnNotesSharingDetails ownSharedList=mongoOperations.findOne(query, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
						if(ownSharedList!=null)
						{
							// selected book id
							query = new Query(Criteria.where("listId").is(listDetail.getListId()));
							MnList mnList1 = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
							
							if((mnList1.isShareAllContactFlag() ||
									(mnList1.getSharedIndividuals()!= null && !mnList1.getSharedIndividuals().isEmpty() && mnList1.getSharedIndividuals().indexOf(Integer.parseInt(userId.trim()))!= -1) ||
									mnList1.getSharedGroups()!= null && !mnList1.getSharedGroups().isEmpty() )){
								sharedStatus = sharedStatus + ",\"shared\":\"shared\"";
							}else{
								sharedStatus = sharedStatus + ",\"shared\":\"notshared\"";
							}
							
							findFlag=true;
							break;
						}
						if(findFlag)
							break;
					}
				}
				}
				
			}
			
			
			if (mnList.getMnNotesDetails() != null && !mnList.getMnNotesDetails().isEmpty()) {
				for (String noteString : mnList.getMnNotesDetails()) {
					if (!mnList.getListType().equals("schedule")) {
						if (noteString.contains("\"noteId\":\"" + noteId + "\"")) {
							if (noteString.contains("\"status\":\"A\"")) {
								noteString=noteString.substring(0,noteString.length() - 1);
								noteString=noteString+sharedStatus+",\"noteLevel\":\""+noteLevel+ "\",\"userId\":\""+mnList.getUserId() + "\"}";
								notes = noteString;
							}
						}
					} else {

						if (noteString.contains("\"eventId\":\"" + noteId+ "\"")) {
							if (noteString.contains("\"status\":\"A\"")) {
								notes = noteString;
								Query query1 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A").and("sharingUserId").is(Integer.parseInt(userId)));
								MnEventsSharingDetails shared = mongoOperations.findOne(query1,MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
								if (shared != null) {
									if (notes != null && notes.length() != 0) {
										String subnote = notes.substring(0,notes.length() - 1);
										subnote = subnote+ ",\"noteLevel\":\""+noteLevel+ "\",\"userId\":\""+mnList.getUserId()+"\",\"sharingdetails\":\""+ shared.getSharingStatus()+ "\"}";
										notes = subnote;
									}

								}else{
									Query query2 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(0).and("status").is("A").and("sharingUserId").is(Integer.parseInt(userId)));
									MnEventsSharingDetails shareds = mongoOperations.findOne(query2,MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
									if (shareds != null) {
										if (notes != null && notes.length() != 0) {
											String subnote = notes.substring(0,notes.length() - 1);
											subnote = subnote+ ",\"noteLevel\":\""+noteLevel+ "\",\"userId\":\""+mnList.getUserId()+"\",\"sharingdetails\":\""+ shareds.getSharingStatus()+ "\"}";
											notes = subnote;
										}

									}
								}

							}
						}

					}
				}
				
			}
			
			JSONObject jsonObject=new JSONObject(notes);
			if(!jsonObject.has("selectedBookId")){
				String listIds=getSharedListIds( Integer.parseInt(listId), userId,noteId);
				jsonObject.append("selectedBookId", listIds.toString());
				notes=jsonObject.toString();
			}
			
			if(!jsonObject.has("userId")){
				jsonObject.append("userId", mnList.getUserId());
				notes=jsonObject.toString();
			}
			
		} catch (Exception e) {
			logger.error("Exception while get note details In Impl !", e);
		}
		
		if(logger.isDebugEnabled())
			logger.debug(" getNote method returened successfully " );
		return notes;

	}

	@Override
	public String addComments(MnComments mnComments, String listId,String noteId,String listType) {
		int cId=0;
		if(logger.isDebugEnabled())
			logger.debug("addComments method called in Impl :"+noteId);
		Set<String> updateNoteDetails=new TreeSet<String>();
		String loginUserZone=null;
		try {
			
			Query query3 = new Query(Criteria.where("userId").is(mnComments.getUserId()));
   			MnUsers mnUser = mongoOperations.findOne(query3, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			loginUserZone=mnUser.getTimeZone();
			
			List<MnComments> mnCommentsList = mongoOperations.findAll(MnComments.class, JavaMessages.Mongo.MNCOMMETS);
			if (mnCommentsList != null && mnCommentsList.size() != 0) {
				cId = mnCommentsList.size() + 1;
			} else {
				cId = 1;
			}
			mnComments.setcId(cId);
			mongoOperations.insert(mnComments, JavaMessages.Mongo.MNCOMMETS);

			// adding comment id in notes
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			String oldComments = "";
			String newComment = "";
			for (String note : mnList.getMnNotesDetails()) {
				if(!listType.equals("crowd"))
				{
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
						removeNoteDeatils = note;
						JSONObject jsonObject;
						try {
							jsonObject = new JSONObject(note);
							oldComments = (String) jsonObject.get("comments");

							if (oldComments != null && !oldComments.isEmpty()) {
								newComment = oldComments;

								if (newComment.lastIndexOf(']') != -1) {
									newComment = newComment.substring(0, newComment.lastIndexOf(']'));
								}

								newComment = newComment + "," + cId + "]";
							} else {
								newComment = "[" + cId + "]";
							}

							if (note.contains("\"comments\":\"\"")) {
								note = note.replace("\"comments\":\"\"","\"comments\":\"" + newComment + "\"");
							} else {
								note = note.replace("\"comments\":\"" + oldComments+ "\"", "\"comments\":\"" + newComment+ "\"");
							}
							tempNoteDetails = note;
						} catch (Exception e) {
							logger.error("Exception while converting json to Note Attached file In Impl !",e);
						}
				}
				}else
				{
					if (note.contains("\"noteId\":\"" + noteId + "\"")) {
						removeNoteDeatils = note;
						JSONObject jsonObject;
						try {
							jsonObject = new JSONObject(note);
							oldComments = (String) jsonObject.get("pcomments");

							if (oldComments != null && !oldComments.isEmpty()) {
								newComment = oldComments;

								if (newComment.lastIndexOf(']') != -1) {
									newComment = newComment.substring(0, newComment.lastIndexOf(']'));
								}

								newComment = newComment + "," + cId + "]";
							} else {
								newComment = "[" + cId + "]";
							}

							if (note.contains("\"pcomments\":\"\"")) {
								note = note.replace("\"pcomments\":\"\"","\"pcomments\":\"" + newComment + "\"");
							} else {
								note = note.replace("\"pcomments\":\"" + oldComments+ "\"", "\"pcomments\":\"" + newComment+ "\"");
							}
							tempNoteDetails = note;
						} catch (Exception e) {
							logger.error("Exception while converting json to Note Attached file In Impl !",e);
						}
				}	
				}
				updateNoteDetails.add(note);
			}
			
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			
			Query detailsQuery = new Query(Criteria.where("listId").is(mnList.getListId()).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
			MnNoteDetails mnNoteDetails =mongoOperations.findOne(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
			if(mnNoteDetails!= null ){
				Update update1 = new Update();
				if(mnNoteDetails.getComments()!= null && !mnNoteDetails.getComments().isEmpty()){
					mnNoteDetails.getComments().add(cId);
				}else{
					List<Integer> comtList= new ArrayList<Integer>();
					comtList.add(cId);
					mnNoteDetails.setComments(comtList);
				}
				if(!listType.equals("crowd"))
					update1.set("comments",mnNoteDetails.getComments());
				else
					update1.set("pcomments",mnNoteDetails.getComments());
				
				mongoOperations.updateFirst(detailsQuery,update1,JavaMessages.Mongo.MNNOTEDETAILS);
			}
			
			if(!mnComments.getComments().contains("<a") ){
				if(mnComments.getComments().length() > 91){
					newComment= mnComments.getComments().substring(0,90)+"...";
				}else{
					newComment= mnComments.getComments();
				}
			}else{
				newComment= mnComments.getComments();
			}
			commomLog(tempNoteDetails, mnList, "Added comments to", true, mnComments.getUserId().toString(),false,false,true,": <B>"+newComment+"</B>",false,"");
			
			// *************** change time zone ************* ///
			Date date=null;
			DateFormat formatter = new SimpleDateFormat("MM/d/yyyy HH:mm:ss");
				String d=mnComments.getcDate()+" "+mnComments.getcTime();
				date=formatter.parse(d);
				Calendar c = Calendar.getInstance();
				c.setTime(date);
				TimeZone obj = TimeZone.getTimeZone("GMT"+loginUserZone);
			formatter.setTimeZone(obj);
			String convertedDate=formatter.format(c.getTime());
			String arr[]=convertedDate.split(" ");
			mnComments.setcDate(arr[0].toString());
			mnComments.setcTime(arr[1].toString());
			//****************************//
			
		} catch (Exception e) {
			logger.error("Exception while add note commnets In Impl !", e);
		}
		
		if(logger.isDebugEnabled())
			logger.debug(" addComments method returened successfully " );
		
		return "{\"cId\":\"" + cId+"\",\"comment\":\""+mnComments.getComments()+"\",\"date\":\""+mnComments.getcDate()+"\",\"time\":\""+mnComments.getcTime()+"\"}";
	}

	@Override
	public List<MnComments> getCommentsList(List<Integer> paramsList,String cmtsLevel,String userid) {
		List<MnComments> commentsList = null;
		Query query = null;
		String loginUserZone=null,otherUserZone=null;
		List<MnComments> commentsListNew = new ArrayList<MnComments>();
		if(logger.isDebugEnabled())
			logger.debug("getCommentsList method called in Impl :"+userid);
		try {
			
			Query query3 = new Query(Criteria.where("userId").is(Integer.parseInt(userid)));
   			MnUsers mnUser = mongoOperations.findOne(query3, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			loginUserZone=mnUser.getTimeZone();
			
			query = new Query(
					Criteria.where("cId").in(paramsList).and("status").is("A").orOperator(
						Criteria.where("commLevel").is(cmtsLevel),
						Criteria.where("commLevel").is("P/I")));
			
			query.sort().on("_id", Order.DESCENDING);
			commentsList = mongoOperations.find(query, MnComments.class,JavaMessages.Mongo.MNCOMMETS);
			
			for(MnComments mnCmnt:commentsList)
			{
				Date date=null;
				DateFormat formatter = new SimpleDateFormat("MM/d/yyyy HH:mm:ss");
	   			String d=mnCmnt.getcDate()+" "+mnCmnt.getcTime();
	   			date=formatter.parse(d);
	   			
	   			Calendar c = Calendar.getInstance();
	   			c.setTime(date);
	   			TimeZone obj = TimeZone.getTimeZone("GMT"+loginUserZone);
				formatter.setTimeZone(obj);
				
				String convertedDate=formatter.format(c.getTime());
				String arr[]=convertedDate.split(" ");
				mnCmnt.setcDate(arr[0].toString());
				mnCmnt.setcTime(arr[1].toString());
				
				if(mnCmnt.getEditDate()!=null && !mnCmnt.getEditDate().equals(""))
				{
					Date Edate=null;
					DateFormat Eformatter = new SimpleDateFormat("MM/d/yyyy HH:mm:ss");
		   			String Ed=mnCmnt.getEditDate()+" "+mnCmnt.getEditTime();
		   			Edate=Eformatter.parse(Ed);
		   			
		   			Calendar cc = Calendar.getInstance();
		   			cc.setTime(Edate);
		   			TimeZone Eobj = TimeZone.getTimeZone("GMT"+loginUserZone);
		   			Eformatter.setTimeZone(Eobj);
					
					String converted=Eformatter.format(cc.getTime());
					String arrE[]=converted.split(" ");
					mnCmnt.setEditDate(arrE[0].toString());
					mnCmnt.setEditTime(arrE[1].toString());
				}
				
				commentsListNew.add(mnCmnt);
			}
			

		} catch (Exception e) {
			logger.error("Exception while get note commnets List In Impl !", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" getCommentsList method returened successfully " );
		return commentsListNew;
	}

	static final Comparator<MnComments> SENIORITY_ORDER = new Comparator<MnComments>() {
		public int compare(MnComments e1, MnComments e2) {
			try {
				Integer i1 = e1.getcId();
				Integer i2 = e2.getcId();
				return i2.compareTo(i1);

			} catch (Exception e) {
				return e2.getcId().compareTo(e1.getcId());
			}
		}
	};

	@Override
	public String updateComments(String cId, String cmts, String editDate,String editTime, String listId, String noteId) {
		String eDate=null,eTime=null;
		if(logger.isDebugEnabled())
			logger.debug("updateComments method called in Impl :"+noteId);
		try {
			String loginUserZone=null;
			Query query = new Query(Criteria.where("cId").is(Integer.parseInt(cId)));
			Date d=new Date();
			DateFormat formatter = new SimpleDateFormat("MM/d/yyyy HH:mm:ss");
			String ss=formatter.format(d);
			String arr[]=ss.split(" ");
			
			Update update = new Update();
			update.set("comments", cmts);
			update.set("editDate", arr[0]);
			update.set("editTime", arr[1]);
			update.set("edited", 1);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNCOMMETS);

			MnComments comments = mongoOperations.findOne(query,MnComments.class, JavaMessages.Mongo.MNCOMMETS);
			
			Query query3 = new Query(Criteria.where("userId").is(comments.getUserId()));
   			MnUsers mnUser = mongoOperations.findOne(query3, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			loginUserZone=mnUser.getTimeZone();

			Query query2 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
			MnList mnList = mongoOperations.findOne(query2, MnList.class,JavaMessages.Mongo.MNLIST);
				
			//***************** time zone *******************///////
			
   			Calendar cc = Calendar.getInstance();
   			cc.setTime(d);
   			TimeZone Eobj = TimeZone.getTimeZone("GMT"+loginUserZone);
			formatter.setTimeZone(Eobj);
			
			String converted=formatter.format(cc.getTime());
			String arrE[]=converted.split(" ");
			eDate=arrE[0].toString();
			eTime=arrE[1].toString();
		    ////////////////////////////////////////////////////////
			
			String tempNoteDetails = "";
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					tempNoteDetails = note;
				}
			}
			String cmt="";
			if(!cmts.contains("<a")){
				if(cmts.length() > 91){
					cmt= cmts.substring(0,90)+"...";
					while((cmt.charAt(cmt.length()-1) <= 65 && cmt.charAt(cmt.length()-1) >= 90) ||  
							(cmt.charAt(cmt.length()-1) <= 97 && cmt.charAt(cmt.length()-1) >= 120)){
						cmt = cmt.substring(0, cmt.length()-1);
					}
				}else{
					cmt = cmts;
				}
			}else{
				cmt = cmts;
			}
			
			commomLog(tempNoteDetails, mnList, "Edited comments in", false, comments.getUserId().toString(),false,false,true,": <B>"+cmt+"</B>",false,"");

		} catch (Exception e) {
			logger.error("Exception while uppdate commnets List In Impl !", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" updateComments method returened successfully " );
		
		return "{\"cId\":\"" + cId+"\",\"comment\":\""+cmts+"\",\"eDate\":\""+eDate+"\",\"eTime\":\""+eTime+"\"}";
	}

	@Override
	public String deleteComments(String cId, String listId, String noteId,String type) {
		Set<String> updateNoteDetails=new TreeSet<String>();
		if(logger.isDebugEnabled())
			logger.debug("deleteComments method called in Impl :"+noteId);
		MnComments comments;
		try {

			Query query = new Query(Criteria.where("cId").is(Integer.parseInt(cId)));
			 comments = mongoOperations.findOne(query,MnComments.class, JavaMessages.Mongo.MNCOMMETS);
			 Update update = new Update();
			 if(type.equals("crowd"))
			 {
			 if(comments.getCommLevel().equals("P/I"))
			 {
				 update.set("commLevel", "I"); 
			 }
			 else
			 {
				 update.set("status", "I");
			 }
			 }else
			 {
				 if(comments.getCommLevel().equals("P/I"))
				 {
					 update.set("commLevel", "P"); 
				 }
				 else
				 {
					 update.set("status", "I");
				 }
			 }
			//update.set("status", "I");
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNCOMMETS);
			 comments = mongoOperations.findOne(query,MnComments.class, JavaMessages.Mongo.MNCOMMETS);
			Query query2 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query2, MnList.class,JavaMessages.Mongo.MNLIST);
			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			String oldComments = "";
			String newComment = "";
			for (String note : mnList.getMnNotesDetails()) {
				if(! type.equals("crowd"))
				{
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
						removeNoteDeatils = note;
						JSONObject jsonObject;
						try {
							jsonObject = new JSONObject(note);
							oldComments = (String) jsonObject.get("comments");

							if (oldComments != null && !oldComments.isEmpty()) {
								newComment = oldComments;
								newComment = newComment.replace(cId + ",", "");
								newComment = newComment.replace("," + cId, "");
								newComment = newComment.replace(cId, "");
								if (newComment.length() <= 2) {
									newComment = "";
								}
							} else {
								newComment = "";
							}

							if (note.contains("\"comments\":\"\"")) {
								note = note.replace("\"comments\":\"\"","\"comments\":\"" + newComment + "\"");
							} else {
								note = note.replace("\"comments\":\"" + oldComments+ "\"", "\"comments\":\"" + newComment+ "\"");
							}
							tempNoteDetails = note;
						} catch (Exception e) {
							logger.error("Exception while converting json to Note Attached file In Impl !",e);
						}
				}
				}
				else
				{
					if (note.contains("\"noteId\":\"" + noteId + "\"")) {
						removeNoteDeatils = note;
						JSONObject jsonObject;
						try {
							jsonObject = new JSONObject(note);
							oldComments = (String) jsonObject.get("pcomments");

							if (oldComments != null && !oldComments.isEmpty()) {
								newComment = oldComments;
								newComment = newComment.replace(cId + ",", "");
								newComment = newComment.replace("," + cId, "");
								newComment = newComment.replace(cId, "");
								if (newComment.length() <= 2) {
									newComment = "";
								}
							} else {
								newComment = "";
							}

							if (note.contains("\"pcomments\":\"\"")) {
								note = note.replace("\"pcomments\":\"\"","\"pcomments\":\"" + newComment + "\"");
							} else {
								note = note.replace("\"pcomments\":\"" + oldComments+ "\"", "\"pcomments\":\"" + newComment+ "\"");
							}
							tempNoteDetails = note;
						} catch (Exception e) {
							logger.error("Exception while converting json to Note Attached file In Impl !",e);
						}
				}
				
				}
				updateNoteDetails.add(note);
			}
			
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update2 = new Update();
			update2.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query2, update2,JavaMessages.Mongo.MNLIST);
			
			
			Query detailsQuery = new Query(Criteria.where("listId").is(mnList.getListId()).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
			MnNoteDetails mnNoteDetails =mongoOperations.findOne(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
			if(mnNoteDetails!= null ){
				Update update1 = new Update();
				List<Integer> cmtIdList = new ArrayList<Integer>();
				if(!type.equals("crowd")){
					if(newComment.equals("")){
						mnNoteDetails.setComments(new ArrayList<Integer>());
					}else{
						if(newComment.indexOf("[") != -1){
							newComment = newComment.substring(newComment.indexOf("[")+1,newComment.length());
						}
						if(newComment.indexOf("]") != -1){
							newComment = newComment.substring(0,newComment.indexOf("]"));
						}
						String[] splitCmtIds = newComment.split(",");
						if(splitCmtIds.length > 0 && !splitCmtIds[0].equals("")){
							for(String str : splitCmtIds){
								cmtIdList.add(Integer.parseInt(str.trim()));
							}
						}
						if(!cmtIdList.isEmpty()){
							mnNoteDetails.setComments(cmtIdList);
						}
 					}
					update1.set("comments",mnNoteDetails.getComments());
				}else{
					if(newComment.equals("")){
						mnNoteDetails.setPcomments(new ArrayList<Integer>());
					}else{
						if(newComment.indexOf("[") != -1){
							newComment = newComment.substring(newComment.indexOf("[")+1,newComment.length());
						}
						if(newComment.indexOf("]") != -1){
							newComment = newComment.substring(0,newComment.indexOf("]"));
						}
						String[] splitCmtIds = newComment.split(",");
						if(splitCmtIds.length > 0 && !splitCmtIds[0].equals("")){
							for(String str : splitCmtIds){
								cmtIdList.add(Integer.parseInt(str.trim()));
							}
						}
						if(!cmtIdList.isEmpty()){
							mnNoteDetails.setPcomments(cmtIdList);
						}
					}
					
					update1.set("pcomments",mnNoteDetails.getPcomments());
				}
					
				mongoOperations.updateFirst(detailsQuery,update1,JavaMessages.Mongo.MNNOTEDETAILS);
			}
			commomLog(tempNoteDetails, mnList, "Deleted comments from",false, comments.getUserId().toString(),false,false,true,"",false,"");
			
		} catch (Exception e) {
			logger.error("Exception while delete commnets List In Impl !", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" deleteComments method returened successfully " );
		return cId;
	}

	public String deleteMembersForNote(String userIds, String listId,String noteId, String fromUserId) {
		Integer userId;
		String newMembers = "";
		String oldMembers = "";
		String removeMembersBasedOnOldMembers = "";
		Set<String> updateNoteDetails=new TreeSet<String>();
		if(logger.isDebugEnabled())
			logger.debug("deleteMembersForNote method called in Impl fromUserId: "+fromUserId+"userIds: "+userIds);
		try {
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			userId = mnList.getUserId();
			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			JSONObject jsonObject;
			for (String note : mnList.getMnNotesDetails()) {
				if(note.contains("noteName")){
					if (note.contains("\"noteId\":\"" + noteId + "\"")) {
						removeNoteDeatils = note;
						jsonObject = new JSONObject(note);
						oldMembers = (String) jsonObject.get("notesMembers");
	
						removeMembersBasedOnOldMembers = note;
						newMembers = noteDeletMembersUpdateConvertToJsonObject(note, userIds);
						note = removeMembersBasedOnOldMembers.replace("\"notesMembers\":\"" + oldMembers + "\"","\"notesMembers\":\"" + newMembers + "\"");
						tempNoteDetails = note;
						
					}
				}else{
					if (note.contains("\"eventId\":\"" + noteId + "\"")) {
						removeNoteDeatils = note;
						jsonObject = new JSONObject(note);
						oldMembers = (String) jsonObject.get("eventMembers");
	
						removeMembersBasedOnOldMembers = note;
						newMembers = noteDeletMembersUpdateConvertToJsonObject(note, userIds);
						note = removeMembersBasedOnOldMembers.replace("\"eventMembers\":\"" + oldMembers + "\"","\"eventMembers\":\"" + newMembers + "\"");
						tempNoteDetails = note;
					}
				}
				updateNoteDetails.add(note);
			}

			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			
			updateNotesSharingDetails(userIds, Integer.parseInt(fromUserId), listId, noteId);
			
			//Sharing Members Details Delete Entry Reflect  In Mn_NoteDetails Table Also - Venu
			query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
			MnNoteDetails noteDetails= mongoOperations.findOne(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
			if(noteDetails.getNotesMembers()!=null && !noteDetails.getNotesMembers().isEmpty()){
				List<Integer> noteDet=new ArrayList<Integer>();
				for(Integer in:noteDetails.getNotesMembers()){
					if(in!=Integer.parseInt(userIds))
						noteDet.add(in);
				}
					
				update = new Update();
				update.set("notesMembers", noteDet);
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNNOTEDETAILS);
			}
			//End - Venu
					
			// add activity
			Set<Integer> notUserIdSet = new HashSet<Integer>();
			String addUser = "", addUserName = "";
			newMembers=newMembers.replace("[", "");
			newMembers=newMembers.replace("]", "");
			String[] newMembersIds = newMembers.split(",");
			if (newMembersIds.length != 0) {
				for (int i = 0; i < newMembersIds.length; i++) {
					if (!oldMembers.contains(newMembersIds[i])) {
						notUserIdSet.add(Integer.parseInt(newMembersIds[i].trim()));
						MnUsers mnUsers = getUserDetailsObject(Integer.parseInt(newMembersIds[i]));
						if (mnUsers != null) {
							addUser = addUser + "<br> <p> removed "+ mnUsers.getUserName() + "</p>";
							addUserName = addUserName + "<B>"+ mnUsers.getUserName() + "</B>";
						}
					}
				}
			}
			// notification activity
			Set<Integer> addedNotiUserIdSet = new HashSet<Integer>();
			if(oldMembers.indexOf("[") != -1){
				oldMembers = oldMembers.substring(oldMembers.indexOf("[")+1,oldMembers.length());
			}
			if(oldMembers.indexOf("]") != -1){
				oldMembers = oldMembers.substring(0,oldMembers.indexOf("]"));
			}
			String[] oldMemberIds = oldMembers.split(",");
			for (int i = 0; i < oldMemberIds.length; i++) {
				if (!newMembers.contains(oldMemberIds[i])) {
					try {
						addedNotiUserIdSet.add(Integer.parseInt(oldMemberIds[i]));
					} catch (Exception e) {
						logger.error("reacent activities problem ",e);
					}
				}
			}
			if(addUser.length() > 36){
				addUser = addUser.substring(0,35)+"...";
			}
			if(addUserName.length() > 36){
				addUserName = addUserName.substring(0,35)+"...";
			}
			
			// log --- activity
			JSONObject jsonObject1 = new JSONObject(tempNoteDetails);
			MnUsers mnUsers = getUserDetailsObject(Integer.parseInt(userIds));
			MnUsers fromUser=getUserDetailsObject(Integer.parseInt(fromUserId));
			if (jsonObject1.has("noteName")){
				if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
					writeLog(Integer.parseInt(userIds), "A", "Removed "	+ addUser + " from " +  changedNoteNameWithDouble((String)jsonObject1.get("noteName"))+ " in " + mnList.getListName(), mnList.getListType(),mnList.getListId().toString(), (String) jsonObject1.get("noteId"),null,  mnList.getListType());
				}
	
				if (addedNotiUserIdSet != null && !addedNotiUserIdSet.isEmpty()) {
					writeLog(Integer.parseInt(userIds), "N", "<B>"+ fromUser.getUserFirstName()+" "+ fromUser.getUserLastName() + "</B> Removed "+ addUserName + "</B> from the note "+  changedNoteNameWithDouble((String)jsonObject1.get("noteName")) + " in "+ mnList.getListName() , mnList.getListType(), mnList.getListId().toString(),(String) jsonObject1.get("noteId"), addedNotiUserIdSet,  mnList.getListType());
				}
				if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
					writeLog(Integer.parseInt(userIds), "N", "<B>"+ mnUsers.getUserFirstName()+ " "+mnUsers.getUserLastName()+ "</B> Removed you from the note "+  changedNoteNameWithDouble((String)jsonObject1.get("noteName")) + " in "+ mnList.getListName() , mnList.getListType(), mnList.getListId().toString(),(String) jsonObject1.get("noteId"), notUserIdSet,  mnList.getListType());
				}
			}else{
				if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
					writeLog(Integer.parseInt(userIds), "A", "Removed "	+ addUser + " from the note " + changedNoteNameWithDouble((String) jsonObject1.get("eventName"))+ " in " + mnList.getListName() , mnList.getListType(),mnList.getListId().toString(), (String) jsonObject1.get("eventId"),null,  mnList.getListType());
				}
	
				if (addedNotiUserIdSet != null && !addedNotiUserIdSet.isEmpty()) {
					writeLog(Integer.parseInt(userIds), "N", "<B>"+ fromUser.getUserFirstName()+" "+ fromUser.getUserLastName() + "</B> Removed "+ addUserName + "</B> from the note "+  changedNoteNameWithDouble((String)jsonObject1.get("eventName")) + " in "+ mnList.getListName() , mnList.getListType(), mnList.getListId().toString(),(String) jsonObject1.get("eventId"), addedNotiUserIdSet,  mnList.getListType());
				}
				if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
					writeLog(Integer.parseInt(userIds), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName()+ "</B> Removed you from the note "+  changedNoteNameWithDouble((String)jsonObject1.get("eventName")) + " in "+ mnList.getListName()  , mnList.getListType(), mnList.getListId().toString(),(String) jsonObject1.get("eventId"), notUserIdSet,  mnList.getListType());
				}
			}
		} catch (Exception e) {
			noteId = "0";
			logger.error("Exception while delete Members Note In Impl !", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" deleteMembersForNote method returened successfully " );
		return noteId;

	}

	public String noteDeletMembersUpdateConvertToJsonObject(String jsonStr,	String userId) {
		String existuser = "";
		if(logger.isDebugEnabled())
			logger.debug("noteDeletMembersUpdateConvertToJsonObject method called in Impl :"+userId);
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				if(jsonObject.has("notesMembers")){
					existuser = (String) jsonObject.get("notesMembers");
				}else{
					existuser = (String) jsonObject.get("eventMembers");
				}
				if (existuser.contains(userId)) {
					if (existuser.contains(",")) {
						if (existuser.contains("," + userId)) {
							existuser = existuser.replace("," + userId, "");
						} else {
							existuser = existuser.replace(userId + ",", "");
						}
					} else {
						existuser = "";
					}
				}
			} catch (Exception e) {
				logger.error("Exception while converting json to shared members object In Impl !",e);
			}
		}
		if(logger.isDebugEnabled())
			logger.debug(" noteDeletMembersUpdateConvertToJsonObject method returened successfully " );
		return existuser;
	}

	public String deletegroupForNote(String groupid, String listId,	String noteId, String fromUserId) {
		if(logger.isDebugEnabled())
			logger.debug("deletegroupForNote method called in Impl fromUserId: "+fromUserId+" noteId: "+noteId);
		Integer userId;
		String newgroups = "";
		String oldgroups = "";
		String removegroupsBasedOnOldgroups = "";
		Set<String> updateNoteDetails=new TreeSet<String>();
		try {
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			userId = mnList.getUserId();
			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			JSONObject jsonObject;
			for (String note : mnList.getMnNotesDetails()) {
				if(note.contains("noteName")){
					if (note.contains("\"noteId\":\"" + noteId + "\"")) {
						removeNoteDeatils = note;
						jsonObject = new JSONObject(note);
						oldgroups = (String) jsonObject.get("noteGroups");
						
						removegroupsBasedOnOldgroups = note;
						newgroups = noteDeletgroupUpdateConvertToJsonObject(note,groupid);
						note = removegroupsBasedOnOldgroups.replace("\"noteGroups\":\"" + oldgroups + "\"","\"noteGroups\":\"" + newgroups + "\"");
						tempNoteDetails = note;
					}
				}else{
					if (note.contains("\"eventId\":\"" + noteId + "\"")) {
						removeNoteDeatils = note;
						jsonObject = new JSONObject(note);
						oldgroups = (String) jsonObject.get("eventGroups");
						
						removegroupsBasedOnOldgroups = note;
						newgroups = noteDeletgroupUpdateConvertToJsonObject(note,groupid);
						note = removegroupsBasedOnOldgroups.replace("\"eventGroups\":\"" + oldgroups + "\"","\"eventGroups\":\"" + newgroups + "\"");
						tempNoteDetails = note;
					}
				}
				updateNoteDetails.add(note);
			}

			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			
			updateNotesgroupSharingDetails(groupid, Integer.parseInt(fromUserId), listId, noteId);
			
			//Sharing Group Details Delete Entry Reflect  In Mn_NoteDetails Table Also - Venu
			query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
			MnNoteDetails noteDetails= mongoOperations.findOne(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
			if(noteDetails.getNoteGroups()!=null && !noteDetails.getNoteGroups().isEmpty()){
				List<Integer> noteDet=new ArrayList<Integer>();
				for(Integer in:noteDetails.getNoteGroups()){
					if(in!=Integer.parseInt(groupid))
						noteDet.add(in);
				}
					
				update = new Update();
				update.set("noteGroups", noteDet);
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNNOTEDETAILS);
			}
			//End - Venu
			
			
			Query query2 = new Query(Criteria.where("groupId").is(Integer.parseInt(groupid)));
			MnGroupDomain mnGroupDomain = mongoOperations.findOne(query2,MnGroupDomain.class, JavaMessages.Mongo.MNGROUP);
						
			commomLog(tempNoteDetails, mnList, "Removed "+ mnGroupDomain.getGroupName() + " group from ", true,fromUserId,false,false,true,"",true,"");
		} catch (Exception e) {
			noteId = "0";
			logger.error("Exception while delete group For Note In Impl !", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" deletegroupForNote method returened successfully " );
		return noteId;
	}

	public String noteDeletgroupUpdateConvertToJsonObject(String jsonStr,
			String userId) {
		if(logger.isDebugEnabled())
			logger.debug("noteDeletgroupUpdateConvertToJsonObject method called in Impl :"+userId);
		String existgroup = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				if(jsonObject.has("noteGroups")){
					existgroup = (String) jsonObject.get("noteGroups");
				}else{
					existgroup = (String) jsonObject.get("eventGroups");
				}
				
				if (existgroup.contains(userId)) {
					if (existgroup.contains(",")) {
						if (existgroup.contains("," + userId)) {
							existgroup = existgroup.replace("," + userId, "");
						} else {
							existgroup = existgroup.replace(userId + ",", "");
						}
					} else {
						existgroup = "";
					}
				}
			} catch (Exception e) {
				logger.error("Exception while noteDeletgroupUpdateConvertToJsonObject In Impl !",e);
			}
		}
		if(logger.isDebugEnabled())
			logger.debug(" noteDeletgroupUpdateConvertToJsonObject method returened successfully " );
		return existgroup;
	}

	@Override
	public String checkListMenuAccess(String noteId, String listId,
			String userId, String noteOrEvent) {
		if(logger.isDebugEnabled())
			logger.debug("checkListMenuAccess method called in Impl :"+userId);
		String access = "";
		try {
			// Check who create List
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and(
					"userId").is(Integer.parseInt(userId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,
					JavaMessages.Mongo.MNLIST);

			if (mnList != null) {
				String enabledisableflag = "false";
				if (mnList.isEnableDisableFlag())
					enabledisableflag = "true";

				JSONObject jsonObject;
				if (noteId != null && !noteId.equals("0")) {
					if (noteOrEvent != null && noteOrEvent != ""&& noteOrEvent.equalsIgnoreCase("notes")) {
						for (String note : mnList.getMnNotesDetails()) {
							if (note.contains("\"noteId\":\"" + noteId + "\"")) {
								jsonObject = new JSONObject(note);
								Integer allContacts = (Integer) jsonObject.get("noteSharedAllContact");
								access = "{\"noteAccess\":\""+ mnList.getNoteAccess()+ "\",\"noteSharedAllContact\":"+ allContacts + ",\"defaultNote\":"+ mnList.isDefaultNote() + "}";
							}
						}
					} else {
						for (String note : mnList.getMnNotesDetails()) {
							if (note.contains("\"eventId\":\"" + noteId + "\"")) {
								jsonObject = new JSONObject(note);
								Integer allContacts = (Integer) jsonObject.get("eventSharedAllContact");
								access = "{\"noteAccess\":\""+ mnList.getNoteAccess()+ "\",\"eventSharedAllContact\":"+ allContacts+ ",\"enabledisableflag\":\""+ enabledisableflag+ "\",\"sentUserId\":\"0\",\"defaultNote\":"+ mnList.isDefaultNote() + "}";
							}
						}
					}
				} else {
					if (noteOrEvent != null && noteOrEvent != ""
							&& noteOrEvent.equalsIgnoreCase("notes"))
						access = "{\"noteAccess\":\"" + mnList.getNoteAccess()+ "\",\"noteSharedAllContact\":"+ mnList.isShareAllContactFlag()+ ",\"defaultNote\":" + mnList.isDefaultNote()+ "}";
					else if (noteOrEvent != null && noteOrEvent != ""&& noteOrEvent.equalsIgnoreCase("events"))
						access = "{\"noteAccess\":\"" + mnList.getNoteAccess()+ "\",\"eventSharedAllContact\":"+ mnList.isShareAllContactFlag()+ ",\"enabledisableflag\":\""+ enabledisableflag+ "\",\"sentUserId\":\"0\",\"defaultNote\":"+ mnList.isDefaultNote() + "}";
				}
			} else {
				if (noteOrEvent != null && noteOrEvent != ""&& noteOrEvent.equalsIgnoreCase("notes")){
					query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
					mnList = mongoOperations.findOne(query, MnList.class,	JavaMessages.Mongo.MNLIST);
					if(mnList.getSharedIndividuals()!= null && !mnList.getSharedIndividuals().isEmpty() && mnList.getSharedIndividuals().contains(Integer.parseInt(userId))){
						access = "{\"noteAccess\":\"0\",\"noteSharedAllContact\":0,\"noteSharedAllContact\":\"0\",\"shared\":\"individual\"}";
					}else if(mnList.isShareAllContactFlag()){
						
						if(mnList.getSharedGroups()!= null && !mnList.getSharedGroups().isEmpty()){
							Query queryAll = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("status").is("A").and("sharingLevel").is("book").and("listId").is(Integer.parseInt(listId)).and("noteId").is(0).and("userId").is(mnList.getUserId()).and("shareAllContactFlag").is(true));
							MnNotesSharingDetails details = mongoOperations.findOne(queryAll, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
							if(details != null){
								access = "{\"noteAccess\":\"0\",\"noteSharedAllContact\":0,\"noteSharedAllContact\":\"0\",\"shared\":\"allcon\"}";
							}else{
								access = "{\"noteAccess\":\"0\",\"noteSharedAllContact\":0,\"noteSharedAllContact\":\"0\",\"shared\":\"groups\"}";
							}
						}else{
							access = "{\"noteAccess\":\"0\",\"noteSharedAllContact\":0,\"noteSharedAllContact\":\"0\",\"shared\":\"allcon\"}";
						}
					}else if (mnList.getSharedGroups()!= null && !mnList.getSharedGroups().isEmpty()){
						access = "{\"noteAccess\":\"0\",\"noteSharedAllContact\":0,\"noteSharedAllContact\":\"0\",\"shared\":\"groups\"}";
					}else {
						access = "{\"noteAccess\":\"0\",\"noteSharedAllContact\":0,\"noteSharedAllContact\":\"0\",\"shared\":\"notshared\"}";
					}
				}	
				else if (noteOrEvent != null && noteOrEvent != "" && noteOrEvent.equalsIgnoreCase("events")) {
					Query query1 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("status").is("A"));
					MnList mnList1 = mongoOperations.findOne(query1,MnList.class, JavaMessages.Mongo.MNLIST);
					if (mnList1 != null) {
						String enabledisableflag = "false";
						if (mnList1.isEnableDisableFlag())
							enabledisableflag = "true";
						access = "{\"noteAccess\":\"0\",\"eventSharedAllContact\":0,\"enabledisableflag\":\""+ enabledisableflag+ "\",\"sentUserId\":\""	+ mnList1.getUserId()+ "\",\"noteSharedAllContact\":\"0\"}";
					} else {
						access = "{\"noteAccess\":\"0\",\"eventSharedAllContact\":0,\"enabledisableflag\":\"false\",\"sentUserId\":\"0\",\"noteSharedAllContact\":\"0\"}";
					}
				}
			}
		} catch (Exception e) {
			logger.error("Exception while checking List Menu Access :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" checkListMenuAccess method returened successfully " );
		return access;
	}

	@Override
	public String castUncastVotes(String userListArray, String listId,String noteId, String userId) {
		String votes = "";
		if(logger.isDebugEnabled())
			logger.debug("castUncastVotes method called in Impl :"+userId+" noteId: "+noteId);
		Set<String> updateNoteDetails=new TreeSet<String>();
		try {
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			String accessType = null;
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					removeNoteDeatils = note;
					JSONObject jsonObject;
					try {
						jsonObject = new JSONObject(note);
						votes = (String) jsonObject.get("vote");
						accessType = (String) jsonObject.get("access");

						if (note.contains("\"vote\":\"\"")) {
							note = note.replace("\"vote\":\"\"", "\"vote\":\""+ userListArray + "\"");
						} else {
							note = note.replace("\"vote\":\"" + votes + "\"","\"vote\":\"" + userListArray + "\"");
						}

						tempNoteDetails = note;
					} catch (Exception e) {
						logger.error(e);
					}
				}
				updateNoteDetails.add(note);
			}
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);

			Query detailsQuery = new Query(Criteria.where("listId").is(mnList.getListId()).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
			MnNoteDetails mnNoteDetails =mongoOperations.findOne(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
			if(mnNoteDetails!= null ){
				Update update1 = new Update();
				String tempUserArray = userListArray;
				if(tempUserArray.trim().equals("")){
					mnNoteDetails.setVote(new ArrayList<Integer>());
				}else{
					if(tempUserArray.indexOf("[") != -1){
						tempUserArray = tempUserArray.substring(tempUserArray.indexOf("[")+1,tempUserArray.length());
					}
					if(tempUserArray.indexOf("]") != -1){
						tempUserArray = tempUserArray.substring(0,tempUserArray.indexOf("]"));
					}
					String arrayVote[] = null; 
					if(tempUserArray.indexOf(",")!= -1){
						arrayVote = tempUserArray.split(",");
					}else{
						if(!tempUserArray.trim().equals("")){
							arrayVote = new String[]{tempUserArray.trim()};
						}
					}
					if(arrayVote.length > 0 && !arrayVote[0].equals("")){
						List<Integer> list = new ArrayList<Integer>();
						for(String str: arrayVote){
							list.add(Integer.parseInt(str.trim()));
						}
						mnNoteDetails.setVote(list);
					}else{
						mnNoteDetails.setVote(new ArrayList<Integer>());
					}
				}
				update1.set("vote",mnNoteDetails.getVote());
				
				mongoOperations.updateFirst(detailsQuery,update1,JavaMessages.Mongo.MNNOTEDETAILS);
			}
			
			if (accessType != null && accessType.equals("public")) {

				Query query2 = new Query(Criteria.where("listId").is(listId).and("noteId").is(noteId).and("status").is("A"));
				MnVoteViewCount viewCounts = mongoOperations.findOne(query2,MnVoteViewCount.class,JavaMessages.Mongo.MNVOTEVIEWCOUNT);

				Integer voteCount = 0;
				if (userListArray.contains(userId)) {

					if (viewCounts == null) {

						viewCounts = new MnVoteViewCount();

						viewCounts.setViewed(0);
						viewCounts.setListId(listId);
						viewCounts.setNoteId(noteId);
						viewCounts.setVote(1);

						insertMostViewed(viewCounts, userId);

					} else {
						voteCount = viewCounts.getVote() + 1;
						viewCounts.setVote(voteCount);

						updateMostViewed(viewCounts, userId);
					}

				} else {

					voteCount = viewCounts.getVote() - 1;
					viewCounts.setVote(voteCount);

					updateMostViewed(viewCounts, userId);

				}
			}
			if (userListArray.contains(userId)){
				commomLog(tempNoteDetails, mnList, "Voted for", true, userId,false,false,true,"",true,"");
			} else{
				commomLog(tempNoteDetails, mnList, "Unvoted for", false, userId,false,false,true,"",false,"");
			}
		} catch (Exception e) {
			logger.error("Exception while casting & uncasting votes to Note In Impl !",e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" castUncastVotes method returened successfully " );
		return "success";
	}

	@Override
	public String changeNoteAccess(String listId, String noteId, String access,String userId,String userName,String date) {
		String tempNoteDetails = null;
		String removeNoteDeatils = null;
		String oldComments=null;
		String oldTags=null;
		String  oldAttaches = null;
		Set<String> updateMnNoteDetails=new TreeSet<String>();
		String oldDescription = null;
		if(logger.isDebugEnabled())
			logger.debug("changeNoteAccess method called in Impl :"+userId+" userName: "+userName);
		try {
			// Check who create List
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);

			boolean accessPrivatePublic = false;
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					removeNoteDeatils = note;
					JSONObject jsonObject;
					jsonObject = new JSONObject(note);
					oldComments = (String) jsonObject.get("comments");
					oldTags=(String)jsonObject.get("tag");
					oldAttaches = (String)jsonObject.get("attachFilePath");
					oldDescription = (String)jsonObject.get("description");
					List<Integer> tagDetails=getOwnerTagDetails(Integer.parseInt(userId),listId,noteId);
					if (note.contains("\"privateAttachFilePath\":\"\"")) 
						note = note.replace("\"privateAttachFilePath\":\"\"","\"privateAttachFilePath\":\"" + oldAttaches + "\"");
					if (note.contains("\"privateTags\":\"\"")) 
						note = note.replace("\"privateTags\":\"\"","\"privateTags\":\"" + tagDetails + "\"");
					if (note.contains("\"pcomments\":\"\"")) 
						note = note.replace("\"pcomments\":\"\"","\"pcomments\":\"" + oldComments + "\"");
					
					if(oldDescription.indexOf("\"") != -1){
						oldDescription = oldDescription.replace("\"", "\\\"");
					}
					
					if (note.contains("\"publicDescription\":\"\"")){ 
						note = note.replace("\"publicDescription\":\"\"","\"publicDescription\":\"" + oldDescription + "\"");
					}
					note = note.replace(noteAccessConvertToJsonObject(note),"\"access\":\"" + access + "\"");
					tempNoteDetails = note;
					note=note.replace(notePublicAccessConvertToJsonObject(note),"\"publicUser\":\"" + userId + "\"");
					note=note.replace(notePublicDateAccessConvertToJsonObject(note),"\"publicDate\":\"" + date + "\"");
					updateMnNoteDetails.add(note);
					
				}
				else{
					updateMnNoteDetails.add(note);
				}
				
				if (note.contains("\"access\":\"public\"")) {
					accessPrivatePublic = true;
				}
				
			}
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);
			    mnList.getMnNotesDetails().remove(userName);
			

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);
			    mnList.getMnNotesDetails().add(userName);

			Update update = new Update();

			if (accessPrivatePublic)
			{
				update.set("noteAccess", "public");
			   
			}
			else
			{
				update.set("noteAccess", "private");
			}
			   
			update.set("mnNotesDetails", updateMnNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			
			Query detailsQuery = new Query(Criteria.where("listId").is(mnList.getListId()).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
			MnNoteDetails mnNoteDetails =mongoOperations.findOne(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
			if(mnNoteDetails!= null ){
				Update update1 = new Update();
				if(accessPrivatePublic){
					update1.set("noteAccess", "public");
					if(mnNoteDetails.getComments()!= null && !mnNoteDetails.getComments().isEmpty()){
						mnNoteDetails.setPcomments(mnNoteDetails.getComments());
					}
					if(mnNoteDetails.getTag()!= null && !mnNoteDetails.getTag().isEmpty()){
						mnNoteDetails.setPrivateTags(mnNoteDetails.getTag());
					}
					update1.set("privateAttachFilePath",mnNoteDetails.getAttachFilePath());
					update1.set("pcomments",mnNoteDetails.getPcomments());
					update1.set("privateTags",mnNoteDetails.getPrivateTags());
					update1.set("publicUser",userId);
					update1.set("publicDate",date);
				}else{
					update1.set("noteAccess", "private");
				}
				mongoOperations.updateFirst(detailsQuery,update1,JavaMessages.Mongo.MNNOTEDETAILS);
			}
			
			JSONObject jsonObject = new JSONObject(tempNoteDetails);
			String accessType = (String) jsonObject.get("access");
			String votes = (String) jsonObject.get("vote");

			if (votes.indexOf("[") != -1) {
				votes = votes.substring(votes.indexOf("[") + 1, votes.length());
			}
			if (votes.indexOf("]") != -1) {
				votes = votes.substring(0, votes.indexOf("]"));
			}
			String vote[] = null;
			Integer noOfVote = 0;
			if (!votes.trim().equals("")) {
				vote = votes.split(",");
				if (vote.length > 0 && vote[0] != null && vote[0] != "") {
					noOfVote = vote.length;
				}
			}
			Query query2 = new Query(Criteria.where("listId").is(listId).and("noteId").is(noteId));
			MnVoteViewCount viewCounts = mongoOperations.findOne(query2,MnVoteViewCount.class, JavaMessages.Mongo.MNVOTEVIEWCOUNT);
			if (accessType != null && accessType.equals("public")) {
				if (viewCounts == null) {
					viewCounts = new MnVoteViewCount();

					viewCounts.setViewed(0);
					viewCounts.setListId(listId);
					viewCounts.setNoteId(noteId);
					viewCounts.setVote(noOfVote);
					viewCounts.setStatus("A");
					insertMostViewed(viewCounts, userId);

				} else {
					viewCounts.setVote(noOfVote);
					viewCounts.setViewed(0);
					viewCounts.setStatus("A");
					updateMostViewed(viewCounts, userId);
				}

			} else {
				if (viewCounts != null) {
					viewCounts.setStatus("I");
					updateMostViewed(viewCounts, userId);
				}
			}
			if (vote!= null && vote.length > 0 && vote[0] != null) {

				for (int i = 0; i < vote.length; i++) {
					MnUsers mnUsers = getUserDetails(Integer.parseInt(vote[i].trim()));

					if (mnUsers != null && mnUsers.getUserRole().equals("Administrator")) {
						Query updQuery = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("listId").is(listId).and("noteId").is(noteId).and("status").is("A"));
						MnAdminVotes mnAdminVotes = mongoOperations.findOne(updQuery, MnAdminVotes.class,JavaMessages.Mongo.MNADMINVOTES);

						if (accessType != null && accessType.equals("public")) {
							if (mnAdminVotes == null) {
								insertAdminVotes(userId, listId, noteId);
							}
						} else {

							if (mnAdminVotes != null) {
								updateAdminVoites(updQuery, userId, listId,noteId);
							}
						}

					}
				}
			}
			commomLog(tempNoteDetails, mnList, "Shared a note:", true,	userId,true,true,true," with the Crowd",false,"crowd");
			
		} catch (Exception e) {
			logger.error("Exception while checking note Menu Access :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" changeNoteAccess method returened successfully " );
		return tempNoteDetails;
	}

	@Override
	public String updateGroupsForNote(String groups, String listId,String noteId, String fromUserId, String noteType,String fullName, String firstName, String noteName, String noteLevel) {
		List<MnNotesSharingDetails> details = new ArrayList<MnNotesSharingDetails>();
		List<MnEventsSharingDetails> eventSharing = new ArrayList<MnEventsSharingDetails>();
		Integer userId;
		Date date = new Date();
		String newGroups = "";
		String removeMembersBasedOnOldMembers = "";
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		Set<Integer> notUserIdSet = new HashSet<Integer>();
		List<Integer> newGroupId = new ArrayList<Integer>();
		List<Integer> groupUser = new ArrayList<Integer>();
		Set<String> updateNoteDetails=new TreeSet<String>();
		String message=null;
		String subject=null;
		Boolean mailNotify=false;
		
		if(logger.isDebugEnabled())
			logger.debug("updateGroupsForNote method called in Impl fromUserId: "+fromUserId+" fullName: "+fullName);
		try {
			Integer size = 0;
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			userId = Integer.parseInt(fromUserId);
			newGroups = groups;

			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			for (String note : mnList.getMnNotesDetails()) {

				if (noteType.equalsIgnoreCase("Schedule")) {
					if (note.contains("\"eventId\":\"" + noteId + "\"")) {
						removeNoteDeatils = note;
						if (note.contains("\"eventGroups\":\"\"")) {
							note = note.replace("\"eventGroups\":\"\"","\"eventGroups\":\"" + groups + "\"");
						} else {
							removeMembersBasedOnOldMembers = note;
							String oldGroup = notesSharingGroupDetailsConvertToJsonObject(removeMembersBasedOnOldMembers); 
							if(oldGroup!=null && !oldGroup.isEmpty()){
								oldGroup=oldGroup+","+groups;
								oldGroup = "[" + oldGroup + "]";
								note = note.replace(eventGroupsUpdateConvertToJsonObject(note,true),"\"eventGroups\":\"" + oldGroup + "\"");
							}else{
								note = note.replace(eventGroupsUpdateConvertToJsonObject(note,true),"\"eventGroups\":\"" + groups + "\"");
							}
						}
						tempNoteDetails = note;
					}
				} else {
					if (note.contains("\"noteId\":\"" + noteId + "\"")) {
						removeNoteDeatils = note;
						if (note.contains("\"noteGroups\":\"\"")) {
							note = note.replace("\"noteGroups\":\"\"","\"noteGroups\":\"" + groups + "\"");
						} else {
							removeMembersBasedOnOldMembers = note;
							String oldGroup = notesSharingGroupDetailsConvertToJsonObject(removeMembersBasedOnOldMembers); 
							if(oldGroup!=null && !oldGroup.isEmpty()){
								oldGroup=oldGroup+","+groups;
								oldGroup = "[" + oldGroup + "]";
								note = note.replace(noteGroupsUpdateConvertToJsonObject(note,true),"\"noteGroups\":\"" + oldGroup + "\"");
							}else{
								note = note.replace(noteGroupsUpdateConvertToJsonObject(note,true),"\"noteGroups\":\"" + groups + "\"");
							}
						}
						tempNoteDetails = note;
					}
				}
				updateNoteDetails.add(note);
			}
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);

			String oldGroup = "";
			if (!removeMembersBasedOnOldMembers.isEmpty()) {
				oldGroup = notesSharingGroupDetailsConvertToJsonObject(removeMembersBasedOnOldMembers);
			}
			
			String[] groupIds = newGroups.split(",");
			if (groupIds.length != 0) {
				for (int i = 0; i < groupIds.length; i++) {
					if (!oldGroup.contains(groupIds[i])) {
						
						try {
							newGroupId.add(Integer.parseInt(groupIds[i]));
						} catch (Exception e) {}
					}
				}
				
				
				for (int i = 0; i < newGroupId.size(); i++) 
					{
					groupUser = getUserIdBasedOnGroupId(newGroupId.get(0));
					if (groupUser != null && !groupUser.isEmpty()) {
						for (Integer user:groupUser) 
						{
							MnNotesSharingDetails sharingDetails = new MnNotesSharingDetails();
							sharingDetails.setUserId(Integer.parseInt(fromUserId));
							sharingDetails.setSharingUserId(user);
							sharingDetails.setSharingGroupId(newGroupId.get(i));
							sharingDetails.setListId(Integer.parseInt(listId));
							sharingDetails.setListType(mnList.getListType());
							sharingDetails.setNoteId(Integer.parseInt(noteId));
							sharingDetails.setSharedDate(formatter.format(date));
							sharingDetails.setSharingLevel("note");
							sharingDetails.setNoteLevel(noteLevel);
							sharingDetails.setShareAllContactFlag(false);
							sharingDetails.setStatus("A");
							details.add(sharingDetails);
							
							
							if(mnList.getListType().equalsIgnoreCase("schedule"))
							{
								MnEventsSharingDetails share = new MnEventsSharingDetails();
								share.setUserId(Integer.parseInt(fromUserId));
								share.setSharingUserId(user);
								share.setListId(Integer.parseInt(listId));
								share.setNoteId(Integer.parseInt(noteId));
								share.setListType(mnList.getListType());
								share.setSharingGroupId(newGroupId.get(i));
								share.setSharingLevel("note");
								share.setNoteLevel(noteLevel);
								share.setShareAllContactFlag(false);
								share.setStatus("A");
								share.setSharingStatus("P");
								if (eventSharing != null)
									eventSharing.add(share);
							}
							
						}
					}
					////////////// mail content /////////////////
					List<MnUsers> list = getUsersDetails(groupUser);
					if (list != null && !list.isEmpty())
					{
						for (MnUsers users : list)
						{
							try{
								mailNotify=getUserMailNotification(users.getUserId(),mnList.getListType());
							if(users.getNotificationFlag().equalsIgnoreCase("yes"))	
							{
							if(mailNotify){	
							SendMail sendMail=new SendMail(); 	
							String recipients[]={users.getEmailId()};
								if(mnList.getListType().equalsIgnoreCase("schedule"))
								{
									MnEventDetails event=null;
										
										Query query1 = new Query(Criteria.where("eventId").is(Integer.parseInt(noteId)).and("listId").is(Integer.parseInt(listId)).and("status").is("A"));
										event = mongoOperations.findOne(query1, MnEventDetails.class, JavaMessages.Mongo.MNEVENTS);
										
										 if(!event.getAlldayevent())
											{
												
												String stDate =convertedZoneEvents(users.getUserId(), event.getUserId(), event.getStartDate());
												event.setStartDate(stDate);
												String endDate =convertedZoneEvents(users.getUserId(), event.getUserId(), event.getEndDate());
												event.setEndDate(endDate);
											}
										
										SimpleDateFormat dateFormat  = new SimpleDateFormat("MMM/dd/yyyy");
										Date start=new Date(event.getStartDate());
										Date end=new Date(event.getEndDate());
										
									message="<div style=\"margin:0;padding:0\" dir=\"ltr\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:98%\" border=\"0\"><tbody><tr><td style=\"font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;font-size:12px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"font-size:16px;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;background:#3b5998;color:#ffffff;font-weight:bold;vertical-align:baseline;letter-spacing:-0.03em;text-align:left;padding:10px 38px 4px\"><img src=https://www.musicnoteapp.com/musicnote/pics/logo.png>&nbsp;&nbsp;<a style=\"text-decoration:none\" href=\"https://www.musicnoteapp.com/musicnote/index.html\" target=\"_blank\"><span style=\"background:#3b5998;color:#ffffff;font-weight:bold;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;vertical-align:middle;font-size:16px;letter-spacing:-0.03em;text-align:left;vertical-align:baseline\">Musicnote</span></a></td></tr></tbody></table><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"border-right:1px solid #ccc;line-height:16px;font-size:11px;border-bottom:1px solid #ccc;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;border-top:1px solid #ccc;padding:10px 20px;border-left:1px solid #ccc\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 20px;line-height:16px;width:620px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\">Hi "+users.getUserFirstName()+",</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">"+fullName+" shared a new event with you on Musicnote.</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\"> Event Name : "+event.getEventName()+"</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">Start Date : "+dateFormat.format(start)+"</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">End date : "+dateFormat.format(end)+"</td></tr><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\"></td></tr><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\"><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse:collapse;width:100%\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px;background-color:#f2f2f2;border-left:none;border-right:none;border-top:1px solid #ccc;border-bottom:1px solid #ccc\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding-right:10px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"border-width:1px;border-style:solid;border-color:#29447e #29447e #1a356e;background-color:#5b74a8\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:2px 6px 4px;border-top:1px solid #8a9cc2\"><a href=\"https://www.musicnoteapp.com/musicnote/index.html\" style=\"color:#3b5998;text-decoration:none\" target=\"_blank\"><span style=\"font-weight:bold;white-space:nowrap;color:#fff;font-size:13px\">Go To Musicnote</span></a></td></tr></tbody></table></td></tr></tbody></table></td><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\"></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"border-right:none;color:#999999;font-size:11px;border-bottom:none;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;border:none;border-top:none;border-left:none\">This message was sent to you from Musicnote .If you don\'t want to receive these emails from Musicnote in the future, please <a href=\"https://www.musicnoteapp.com/musicnote/unsubscribe.html\" style=\"color:#3b5998;text-decoration:none\" target=\"_blank\">unsubscribe.</a><br>Musicnote, Inc., 3240 E. State St. Ext., Hamilton NJ, 08619.</td></tr></tbody></table></div>";
									subject=firstName+" shared a new event with you";
								}
								else
								{
									message=MailContent.sharingNotification+firstName+MailContent.sharingNotification1+users.getUserFirstName()+"  shared a new note with you on Musicnote :" + noteName+" "+MailContent.sharingNotification2;;
									subject=firstName+" shared a new note with you";
								}
							
							sendMail.postEmail(recipients, subject,message);
							}
							}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					/////////////////////////
				}
					
			} else if (newGroups != null && !newGroups.isEmpty()) {
				if (!oldGroup.contains(newGroups)) {
					MnNotesSharingDetails sharingDetails = new MnNotesSharingDetails();
					sharingDetails.setUserId(userId);
					try {
						newGroupId.add(Integer.parseInt(newGroups));
					} catch (Exception e) {

					}
					sharingDetails.setSharingUserId(0);
					sharingDetails.setSharingGroupId(Integer.parseInt(newGroups));
					sharingDetails.setListId(Integer.parseInt(listId));
					sharingDetails.setListType(mnList.getListType());
					sharingDetails.setNoteId(Integer.parseInt(noteId));
					sharingDetails.setSharingLevel("note");
					sharingDetails.setNoteLevel(noteLevel);
					sharingDetails.setShareAllContactFlag(false);
					sharingDetails.setSharedDate(formatter.format(date));
					sharingDetails.setStatus("A");
					details.add(sharingDetails);
				}
			}
			insertNotesSharingDetails(details, eventSharing);
			
			//Sharing Group Details Base Update Note Details In Mn_NoteDetails Table Also - Venu
			if (details != null && !details.isEmpty() && !noteType.equalsIgnoreCase("Schedule")) {
				List<Integer> groIds=new ArrayList<Integer>();
				for (MnNotesSharingDetails det : details) {
					if(!groIds.contains(det.getSharingGroupId()))
						groIds.add(det.getSharingGroupId());
				}
				if(groIds!=null && !groIds.isEmpty()){
					query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
					MnNoteDetails noteDetails= mongoOperations.findOne(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
					if(noteDetails.getNoteGroups()!=null)
						noteDetails.getNoteGroups().addAll(groIds);
					else
						noteDetails.setNoteGroups(groIds);
					
					update = new Update();
					update.set("noteGroups", noteDetails.getNoteGroups());
					mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNNOTEDETAILS);
				}
			}
			//End -Venu
			
			// notification activity for old group members
			Set<Integer> addedNotiUserIdSet = new HashSet<Integer>();
			String[] oldMemberIds = oldGroup.split(",");
			for (int i = 0; i < oldMemberIds.length; i++) {
				if (!newGroups.contains(oldMemberIds[i])) {
					try {

						Query query2 = new Query(Criteria.where("groupId").is(Integer.parseInt(oldMemberIds[i])));
						List<MnGroupDetailsDomain> detailsDomain = mongoOperations.find(query2, MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);
						if (detailsDomain != null && !detailsDomain.isEmpty()) {
							for (MnGroupDetailsDomain domain : detailsDomain) {
								notUserIdSet.add(domain.getUserId());
							}
						}
					} catch (Exception e) {

					}
				}
			}

			// new group members
			String newGroupName = "";
			if (newGroupId != null && !newGroupId.isEmpty()) {

				for (Integer groupId : newGroupId) {
					Query query2 = new Query(Criteria.where("groupId").is(groupId));
					MnGroupDomain group = mongoOperations.findOne(query2,MnGroupDomain.class, JavaMessages.Mongo.MNGROUP);
					if (group != null) {
						newGroupName = newGroupName + " <B>"+group.getGroupName() + "</B>, ";
					}
					newGroupName = newGroupName.substring(0, newGroupName.lastIndexOf(","));
					
					List<MnGroupDetailsDomain> detailsDomain = mongoOperations.find(query2, MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);
					if (detailsDomain != null && !detailsDomain.isEmpty()) {
						for (MnGroupDetailsDomain domain : detailsDomain) {
							addedNotiUserIdSet.add(domain.getUserId());
						}
					}
				}
			}
			if (addedNotiUserIdSet != null && !addedNotiUserIdSet.isEmpty()) {
				addedNotiUserIdSet.remove(Integer.parseInt(fromUserId));
				if(!mnList.getUserId().equals(Integer.parseInt(fromUserId))){
					addedNotiUserIdSet.add(mnList.getUserId());
				}
			}else{
				if(!userId.equals(mnList.getUserId())){
					addedNotiUserIdSet.add(mnList.getUserId());
				}
			}
			// log --- activity
			JSONObject jsonObject = new JSONObject(tempNoteDetails);
			MnUsers mnUsers = getUserDetailsObject(Integer.parseInt(fromUserId));
			if (noteType.equalsIgnoreCase("Schedule")) {

				writeLog(Integer.parseInt(fromUserId), "A", "Shared a "+getListType(mnList.getListType())+": "+  changedNoteNameWithDouble((String)jsonObject.get("eventName")) + " in "+ mnList.getListName() + " with Group <B>"+ newGroupName + "</B>", mnList.getListType(),mnList.getListId().toString(), (String) jsonObject.get("eventId"),null, mnList.getListType());

				if (addedNotiUserIdSet != null && !addedNotiUserIdSet.isEmpty()) {
					writeLog(Integer.parseInt(fromUserId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> Shared a "+getListType(mnList.getListType())+": "+  changedNoteNameWithDouble((String)jsonObject.get("eventName")) + " with Group <B>"+ newGroupName + "</B>", mnList.getListType(),mnList.getListId().toString(), (String) jsonObject.get("eventId"), addedNotiUserIdSet, mnList.getListType());
				}
				if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
					writeLog(Integer.parseInt(fromUserId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> Shared a "+getListType(mnList.getListType())+": "+  changedNoteNameWithDouble((String)jsonObject.get("eventName")) + " with Group <B>"+ newGroupName + "</B>", mnList.getListType(),mnList.getListId().toString(), (String) jsonObject.get("eventId"), notUserIdSet, mnList.getListType());
				}
			} else {

				writeLog(Integer.parseInt(fromUserId), "A", "Shared a "+getListType(mnList.getListType())+": "+  changedNoteNameWithDouble((String)jsonObject.get("noteName")) + " in "+ mnList.getListName() + " with Group <B>"+ newGroupName + "</B>", mnList.getListType(),mnList.getListId().toString(), (String) jsonObject.get("noteId"),null, mnList.getListType());

				if (addedNotiUserIdSet != null && !addedNotiUserIdSet.isEmpty()) {
					writeLog(Integer.parseInt(fromUserId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B>  Shared a "+getListType(mnList.getListType())+": "+  changedNoteNameWithDouble((String)jsonObject.get("noteName")) + " with Group <B>"+ newGroupName + "</B>", mnList.getListType(),mnList.getListId().toString(), (String) jsonObject	.get("noteId"), addedNotiUserIdSet, mnList.getListType());
				}
				if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
					writeLog(Integer.parseInt(fromUserId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B>  Shared a "+getListType(mnList.getListType())+": "+  changedNoteNameWithDouble((String)jsonObject.get("noteName")) + " with Group <B>"+ newGroupName + "</B>", mnList.getListType(),mnList.getListId().toString(), (String) jsonObject.get("noteId"), notUserIdSet, mnList.getListType());
				}
			}

			if(logger.isDebugEnabled())
				logger.debug(" updateGroupsForNote method returened successfully " );
			
			return "" + size;
		} catch (Exception e) {
			logger.error("Exception while Updating shared Group For Note :"+ e);
		}
		return "Exception while Updating shared Group For Note";
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MnNoteDetails> fetchGroupSharedNotesBasedOnList(String userId,String listType) {
		List<MnNotesSharingDetails> details = null;
		List<MnGroupDetailsDomain> mnGroupDetails = null;
		List<Integer> sharingGroupIds = null;
		List<MnNoteDetails> sharedNotesDetailsBasedOnListIds= new ArrayList<MnNoteDetails>(); 
		Map<String,Integer> asociateBook=new HashMap<String,Integer>();
		Query query = null;
		if(logger.isDebugEnabled())
			logger.debug("fetchGroupSharedNotesBasedOnList method called in Impl :"+userId+" listType"+listType);
		
		try {
			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("status").is("A"));
			mnGroupDetails = mongoOperations.find(query,MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);
			if (mnGroupDetails != null && !mnGroupDetails.isEmpty()) {
				sharingGroupIds = new ArrayList<Integer>();
				for (MnGroupDetailsDomain domain : mnGroupDetails) {
					sharingGroupIds.add(domain.getGroupId());
				}
			}
			if (sharingGroupIds != null && !sharingGroupIds.isEmpty()) {
				query = new Query(Criteria.where("sharingGroupId").in(sharingGroupIds).and("sharingUserId").is(Integer.parseInt(userId)).and("listType").is(listType).and("status").is("A"));
				query.sort().on("listId", Order.ASCENDING);
				details = mongoOperations.find(query,MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);

				
				List<MnNotesSharingDetails> associateNoteForBook = new ArrayList<MnNotesSharingDetails>();
				if (details != null && !details.isEmpty()) {
					List<Integer> listIds = new ArrayList<Integer>();
					List<Integer> bookLevelSharedlistIds = new ArrayList<Integer>();

					for (MnNotesSharingDetails sharingDetails : details) {
						if (!listIds.contains(sharingDetails.getListId())){
							if(sharingDetails.getNoteId().equals(0)){
								if (!bookLevelSharedlistIds.contains(sharingDetails.getListId())){
									bookLevelSharedlistIds.add(sharingDetails.getListId());
									listIds.add(sharingDetails.getListId());
									
									//fetch the book level sharing , associate note for the particular book
									Query query12 = new Query(Criteria.where("sharingLevel").is("individual").and("sharedBookId").is(sharingDetails.getListId()).and("status").is("A"));
									List<MnNotesSharingDetails> mnSharedList = mongoOperations.find(query12, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
									if(mnSharedList!= null && !mnSharedList.isEmpty()){
										for(MnNotesSharingDetails associateSharing :mnSharedList){
											if(!listIds.contains(associateSharing.getListId())){
												listIds.add(associateSharing.getListId());
											}
											query12 = new Query(Criteria.where("listType").is(listType).and("status").is("A").and("sharingGroupId").is(0)
													.and("sharingLevel").is("individual").and("sharedBookId").is(sharingDetails.getListId()).and("listId").is(associateSharing.getListId()).and("noteId").is(associateSharing.getNoteId()));
											MnNotesSharingDetails associateNoteObj = mongoOperations.findOne(query12, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
											
											asociateBook.put(associateNoteObj.getListId()+"~"+associateNoteObj.getNoteId(), associateNoteObj.getSharedBookId());
											
											if(associateNoteObj!= null ){
												associateNoteForBook.add(associateNoteObj);
											}
										}
									}
									//***************//
									
								}
							}else{
								listIds.add(sharingDetails.getListId());
							}
						}
						
					}
					if(!associateNoteForBook.isEmpty()){
						details.addAll(associateNoteForBook);
					}
					
					
					//Using New Table Fetch List
					for (Integer ids : listIds) {
						List<String> noteIds = new ArrayList<String>();
						Map groupIds=new HashMap();
						Map sharedDates=new HashMap();
						if(!bookLevelSharedlistIds.contains(ids)){
							for (MnNotesSharingDetails sharingDetails : details) {
								if (sharingDetails.getListId() == ids) {
									if(!noteIds.contains(sharingDetails.getNoteId().toString())){
										noteIds.add(sharingDetails.getNoteId().toString());
										groupIds.put(sharingDetails.getNoteId().toString(), sharingDetails.getSharingGroupId());
										sharedDates.put(sharingDetails.getNoteId().toString(), sharingDetails.getSharedDate());
									}
								}
							}
						}else{
							noteIds.add("0");
							for (MnNotesSharingDetails sharingDetails : details) {
								if (sharingDetails.getListId() == ids) {
									sharedDates.put("0", sharingDetails.getSharedDate());
								}
							}
						}
						if(listType.equalsIgnoreCase("schedule")){
							
							//No Need To Write Coding Here 
							
						}else{
							List<MnNoteDetails> sharedNotesDetailsBasedOnListNoteIds=new ArrayList<MnNoteDetails>();
							for(String noteId:noteIds){
								if(noteId.equals("0")){
									query = new Query(Criteria.where("listId").is(ids).and("listType").is(listType).and("status").is("A"));
									List<MnNoteDetails> tempNoteDetails = mongoOperations.find(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
									if(tempNoteDetails!=null){
										for(MnNoteDetails temp:tempNoteDetails){
											temp.setShareType("group"+groupIds.get(noteId));
											
											temp.setTempSharedDate(formatter.parse(sharedDates.get(noteId).toString()));
											sharedNotesDetailsBasedOnListNoteIds.add(temp);
										}
										
									}
								}else{
									query = new Query(Criteria.where("listId").is(ids).and("noteId").is(Integer.parseInt(noteId)).and("listType").is(listType).and("status").is("A"));
									MnNoteDetails tempNoteDetail = mongoOperations.findOne(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
									if(tempNoteDetail!=null){
										tempNoteDetail.setShareType("group"+groupIds.get(noteId));
										
										tempNoteDetail.setTempSharedDate(formatter.parse(sharedDates.get(noteId).toString()));
										
										// **** change listId **** ///
										if(asociateBook !=null && !asociateBook.isEmpty())
										{
										Integer listID=asociateBook.get(tempNoteDetail.getListId()+"~"+tempNoteDetail.getNoteId());
										if(listID!=null && !listID.equals(""))
										{
										query = new Query(Criteria.where("listId").is(listID).and("listType").is(listType).and("status").is("A"));
										MnNoteDetails tempNoteDetail1 = mongoOperations.findOne(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
										if(tempNoteDetail1!=null)
										{
											tempNoteDetail.setListId(tempNoteDetail1.getListId());
											tempNoteDetail.setListName(tempNoteDetail1.getListName());
											
										}
										else
										{
											MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
											tempNoteDetail.setListId(mnList.getListId());
											tempNoteDetail.setOriginalListId(tempNoteDetail.getListId());
											tempNoteDetail.setListName(mnList.getListName());
											tempNoteDetail.setSharedIndividuals(mnList.getSharedIndividuals());
											tempNoteDetail.setSharedGroups(mnList.getSharedGroups());
											tempNoteDetail.setShareAllContactFlag(mnList.isShareAllContactFlag());
										}
										}
										}
										//***** end ************//
										
										sharedNotesDetailsBasedOnListNoteIds.add(tempNoteDetail);
									}
								}
							}
							if(sharedNotesDetailsBasedOnListNoteIds!=null && !sharedNotesDetailsBasedOnListNoteIds.isEmpty())
								sharedNotesDetailsBasedOnListIds.addAll(sharedNotesDetailsBasedOnListNoteIds);
						}
					}
				}
			}

		} catch (Exception e) {
			logger.error("Exception while Fetching Shared Notes Based On List :"+ e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" fetchGroupSharedNotesBasedOnList method returened successfully " );
		return sharedNotesDetailsBasedOnListIds;
	}

	@Override
	public String copyNoteToAllContact(String userId, String listId,String noteId, boolean withComments, String fullName, String firstName, String noteName) {
		MnUsers mnUsers = new MnUsers();
		Query query = null;
		Set<Integer> notUserIdSet = new HashSet<Integer>();
		List<Integer> groupUser = new ArrayList<Integer>();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		String oldnoteName=null;
		String newnoteName=null;
		boolean copyExist=false;
		boolean mailNotify=false;
		if(logger.isDebugEnabled())
			logger.debug("copyNoteToAllContact method called in Impl :"+userId+" noteId: "+noteId);
		try {
			JSONObject jsonObject1 = null;
			query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);

			String tempNoteDetails = null;
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					tempNoteDetails = note;
				}
			}

			jsonObject1 = new JSONObject(tempNoteDetails);
			oldnoteName=(String) jsonObject1.get("noteName");
			
			// Find user based on Friends List
			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("status").is("A"));

			mnUsers = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			
			//add the comment  in cmt table
			List<MnComments> mnCommentsList = null;
			List<Integer> cmtList = new ArrayList<Integer>();
			int cId = 0;
			
			if (withComments) {
				jsonObject1 = new JSONObject(tempNoteDetails);
				String commenntString =(String) jsonObject1.get("comments");
				
				if (commenntString.lastIndexOf(']') != -1) {
					commenntString = commenntString.substring(0, commenntString.lastIndexOf(']'));
				}
				if (commenntString.lastIndexOf('[') != -1) {
					commenntString = commenntString.substring( commenntString.lastIndexOf('[')+1, commenntString.length());
				}
				String[] comts = commenntString.split(",");
				if(comts.length>0 && !comts[0].equals("") ){
					for(String str:comts){
						cmtList.add(Integer.parseInt(str.trim()));
					}
				}
				Query query2 = new Query(Criteria.where("cId").in(cmtList));
				mnCommentsList = mongoOperations.find(query2,MnComments.class, JavaMessages.Mongo.MNCOMMETS);
				
				
				List<MnComments> mnCommentsListTot = mongoOperations.findAll(MnComments.class, JavaMessages.Mongo.MNCOMMETS);
				if (mnCommentsListTot != null && mnCommentsListTot.size() != 0) {
					cId = mnCommentsListTot.size() + 1;
				} else {
					cId = 1;
				}
			}
			
			// copy attach file changes
			List<Integer> attachList = new ArrayList<Integer>();
			List<MnAttachmentDetails> attachmentDetails = null;
			int aId=0;
			
			jsonObject1 = new JSONObject(tempNoteDetails);
			if (!tempNoteDetails.contains("\"attachFilePath\":\"\"")) {
			String attachFilePathString =(String) jsonObject1.get("attachFilePath");
			if (attachFilePathString.lastIndexOf(']') != -1) {
				attachFilePathString = attachFilePathString.substring(0, attachFilePathString.lastIndexOf(']'));
			}
			if (attachFilePathString.lastIndexOf('[') != -1) {
				attachFilePathString = attachFilePathString.substring( attachFilePathString.lastIndexOf('[')+1, attachFilePathString.length());
			}
			String[] attachFile = attachFilePathString.split(",");
			
			for(String str:attachFile){
				if(str!=null && !str.isEmpty())
					attachList.add(Integer.parseInt(str.trim()));
			}
			Query query2 = new Query(Criteria.where("attachId").in(attachList));
			attachmentDetails = mongoOperations.find(query2,MnAttachmentDetails.class, JavaMessages.Mongo.MNATTACHMENTDETAILS);
			
			List<MnAttachmentDetails> mnAttachmentListTot = mongoOperations.findAll(MnAttachmentDetails.class, JavaMessages.Mongo.MNATTACHMENTDETAILS);
			if (mnAttachmentListTot != null && mnAttachmentListTot.size() != 0) {
				aId = mnAttachmentListTot.size() + 1;
			} else {
				aId = 1;
			}
			}
			
			
			for (Integer in : mnUsers.getFriends()) {
				// Adding Selected List to Selected Note
				try {
					notUserIdSet.add(in);
					groupUser.add(in);
				} catch (Exception e) {

				}
				Query addQuery = new Query(Criteria.where("userId").is(in).and("listType").is("music").and("defaultNote").is(true));

				MnList addNote = mongoOperations.findOne(addQuery,MnList.class, JavaMessages.Mongo.MNLIST);
					
				// calculate copy number
				int i=0;
				for (String note : addNote.getMnNotesDetails()) {
					JSONObject existNote = new JSONObject(note);
					if (note.contains("\"copyFromMember\":\"" + userId + "\"")&& note.contains("\"status\":\"A\"")) {
						newnoteName=(String) existNote.get("noteName");
						if(newnoteName.equals(oldnoteName))
						{
							copyExist=true;
							i=i+1;
						}
						else
						{
							int position=newnoteName.lastIndexOf("-copy");
							String noteNameArr=newnoteName;
								if(position!=-1)
								{
									noteNameArr=newnoteName.substring(0,position);
								}
						if(noteNameArr.equals(oldnoteName))
						{
							copyExist=true;
							i=i+1;
						}
						}

					}
				}
				
				if(copyExist)
				{
					if(i==1)
						newnoteName=oldnoteName+"-copy";
					else
						newnoteName=oldnoteName+"-copy("+i+")";
				}
				else
					newnoteName=oldnoteName;
				////////////
				
				Integer size = addNote.getMnNotesDetails().size() + 1;
				tempNoteDetails = tempNoteDetails.replace("\"noteId\":\""+ noteId + "\"", "\"noteId\":\"" + size + "\"");
				tempNoteDetails=tempNoteDetails.replace("\"noteName\":\""+ oldnoteName +"\"","\"noteName\":\""+newnoteName+"\"");
				
				if (!tempNoteDetails.contains("\"notesMembers\":\"\"")) {
					tempNoteDetails = tempNoteDetails.replace(notesMembersUpdateConvertToJsonObject(tempNoteDetails),"\"notesMembers\":\"\"");
				}

				if (!tempNoteDetails.contains("\"noteGroups\":\"\""))
					tempNoteDetails = tempNoteDetails.replace(noteGroupsUpdateConvertToJsonObject(tempNoteDetails,true),"\"noteGroups\":\"\"");

				if (tempNoteDetails.contains("\"noteSharedAllContact\":1"))
					tempNoteDetails = tempNoteDetails.replace("\"noteSharedAllContact\":1","\"noteSharedAllContact\":0");
				
				if (!tempNoteDetails.contains("\"copyToMember\":\"\""))
					tempNoteDetails = tempNoteDetails.replace(notesMembersCopyUpdateConvertToJsonObject(tempNoteDetails),"\"copyToMember\":\"\"");

				if (!tempNoteDetails.contains("\"copyToGroup\":\"\""))
					tempNoteDetails = tempNoteDetails.replace(noteGroupsCopyUpdateConvertToJsonObject(tempNoteDetails),"\"copyToGroup\":\"\"");

				if (tempNoteDetails.contains("\"copyToAllContact\":1"))
					tempNoteDetails = tempNoteDetails.replace("\"copyToAllContact\":1","\"copyToAllContact\":0");
				
				if (tempNoteDetails.contains("\"copyFromMember\":\"\""))
					tempNoteDetails = tempNoteDetails.replace("\"copyFromMember\":\"\"","\"copyFromMember\":\"" + userId + "\"");
				else
					tempNoteDetails = tempNoteDetails.replace(noteFromCopyUpdateConvertToJsonObject(tempNoteDetails),"\"copyFromMember\":\"" + userId + "\"");
				
				tempNoteDetails = tempNoteDetails.replace("\"publicUser\":\""+ (String) jsonObject1.get("publicUser") + "\"", "\"publicUser\":\"\"");
				
				tempNoteDetails = tempNoteDetails.replace("\"publicDate\":\""+ (String) jsonObject1.get("publicDate") + "\"", "\"publicDate\":\"\"");
				
				tempNoteDetails = tempNoteDetails.replace("\"access\":\""+ (String) jsonObject1.get("access") + "\"", "\"access\":\"private\"");
				
				if (!withComments) {
					if (!tempNoteDetails.contains("\"comments\":\"\""))
						tempNoteDetails = tempNoteDetails.replace(noteCommentsUpdateConvertToJsonObject(tempNoteDetails),"\"comments\":\"\"");
				}else{
					try{

						if(cmtList!= null && !cmtList.isEmpty()){
							cmtList.clear();
						}
						for(MnComments comments : mnCommentsList){
							comments.setcId(cId);
							cmtList.add(cId);
							cId++;
							comments.setListId(addNote.getListId().toString());
							comments.setNoteId(size.toString());
						}
						
						mongoOperations.insert(mnCommentsList, JavaMessages.Mongo.MNCOMMETS);
						if(cmtList!= null && !cmtList.isEmpty()){
							tempNoteDetails = tempNoteDetails.replace(noteCommentsUpdateConvertToJsonObject(tempNoteDetails),"\"comments\":\""+cmtList.toString()+"\"");
						}else{
							tempNoteDetails = tempNoteDetails.replace(noteCommentsUpdateConvertToJsonObject(tempNoteDetails),"\"comments\":\"\"");
						}
					}catch (Exception e) {
						logger.error(e);
					}
				}

				if (!tempNoteDetails.contains("\"vote\":\"\""))
					tempNoteDetails = tempNoteDetails.replace(noteVotesUpdateConvertToJsonObject(tempNoteDetails),"\"vote\":\"\"");
				
				if (!tempNoteDetails.contains("\"tag\":\"\"")) {
					tempNoteDetails = tempNoteDetails.replace(noteTagUpdateConvertToJsonObject(tempNoteDetails),"\"tag\":\"\"");
				}
				
				if (!tempNoteDetails.contains("\"remainders\":\"\"")) {
					tempNoteDetails = tempNoteDetails.replace(noteReminderUpdateConvertToJsonObject(tempNoteDetails),"\"remainders\":\"\"");
				}
				
				if (!tempNoteDetails.contains("\"dueDate\":\"\"")) {
					tempNoteDetails = tempNoteDetails.replace(noteDueDateUpdateConvertToJsonObject(tempNoteDetails),"\"dueDate\":\"\"");
				}
				
				if (!tempNoteDetails.contains("\"dueTime\":\"\"")) {
					tempNoteDetails = tempNoteDetails.replace(noteDueTimeUpdateConvertToJsonObject(tempNoteDetails),"\"dueTime\":\"\"");
				}
				
				// entery in attaachement table
				if (!tempNoteDetails.contains("\"attachFilePath\":\"\"")) 
				{
					try{
						if(attachList!= null && !attachList.isEmpty()){
							attachList.clear();
						}
						for(MnAttachmentDetails achmentDetails : attachmentDetails){
							achmentDetails.setAttachId(aId);
							attachList.add(aId);
							aId++;
							achmentDetails.setListId(addNote.getListId());
							achmentDetails.setNoteId(size.toString());
						}
						logger.info("attachement details ::::"+attachmentDetails.toString());
						mongoOperations.insert(attachmentDetails, JavaMessages.Mongo.MNATTACHMENTDETAILS);
						tempNoteDetails = tempNoteDetails.replace(noteFilePathUpdateConvertToJsonObject(tempNoteDetails),"\"attachFilePath\":\""+attachList.toString()+"\"");
					}catch (Exception e) {
						logger.error(e);
					}
				}
				
				
				if (!tempNoteDetails.contains("\"privateTags\":\"\"")) {
					tempNoteDetails = tempNoteDetails.replace(notePrivateTagUpdateConvertToJsonObject(tempNoteDetails),"\"privateTags\":\"\"");
				}

				if (addNote.getMnNotesDetails() != null && addNote.getMnNotesDetails().isEmpty()) {
					addNote.setMnNotesDetails(new HashSet<String>());
					addNote.getMnNotesDetails().add(tempNoteDetails);
				} else {
					addNote.getMnNotesDetails().add(tempNoteDetails);
				}

				Update addUpdate = new Update();
				addUpdate.set("mnNotesDetails", addNote.getMnNotesDetails());
				mongoOperations.updateFirst(addQuery, addUpdate,JavaMessages.Mongo.MNLIST);
				
				//Sharing Copy To All Contact Base Added New Note Details In Mn_NoteDetails Table Also - Venu
				if(!addNote.getListType().equals("schedule")){
					MnNoteDetails details=new MnNoteDetails();
					JSONObject json =new JSONObject(tempNoteDetails);
					details.setNoteName(newnoteName);
					details.setStartDate(date);
					details.setNoteSharedAllContact(false);
					details.setStatus("A");
					details.setOwnerNoteStatus("A");
					details.setAccess("private");
					details.setDueDate("");
					details.setDueTime("");
					
					String description = (String) json.get("description");
					if(description.indexOf("\"") != -1){
						description = description.replace("\"", "\\\"");
					}
					details.setDescription(description);
					
					details.setPublicUser("");
					details.setCopyFromMember(userId);
					details.setUserId(in);
					details.setListId(addNote.getListId());
					details.setListName(addNote.getListName());
					details.setListType(addNote.getListType());
					details.setNoteAccess(addNote.getNoteAccess());
					details.setDefaultNote(addNote.isDefaultNote());
					details.setEnableDisableFlag(addNote.isEnableDisableFlag());
					details.setNoteId(size);
					if (withComments && cmtList!=null)
						details.setComments(cmtList);
					if(attachList!=null && !attachList.isEmpty())
						details.setAttachFilePath(attachList);
					
					mongoOperations.insert(details,JavaMessages.Mongo.MNNOTEDETAILS);
				}
				//End -Venu
			}
			updateAllContactMembersForCopyNote(userId, listId, noteId);

			/////////////mail content /////////////////
			List<MnUsers> list = getUsersDetails(groupUser);
			if (list != null && !list.isEmpty())
			{
				for (MnUsers users : list)
				{
					try{
						mailNotify=getUserMailNotification(users.getUserId(), mnList.getListType());
					if(users.getNotificationFlag().equalsIgnoreCase("yes"))
					{
						if(mailNotify){
							SendMail sendMail=new SendMail(); 	
							String recipients[]={users.getEmailId()};
					
							String message=MailContent.sharingNotification+users.getUserFirstName()+MailContent.sharingNotification1+fullName+"  shared a new note with you on Musicnote : "+ noteName+" "+MailContent.sharingNotification2;;
							String subject=firstName+" shared a copy of a note with you";
							sendMail.postEmail(recipients, subject,message);
						}
					}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			/////////////////////////
			
			
			String addUser = "", addUserName = "";
			if (notUserIdSet != null) {
				for (Integer integer : notUserIdSet) {
					MnUsers mnUsers2 = getUserDetailsObject(integer);
					if (mnUsers2 != null) {
						addUser = addUser + "<br> <p>" + mnUsers2.getUserName()+ "</p>";
						addUserName = addUserName + "<B>"+ mnUsers2.getUserName() + "</B>";
					}
				}
			}

			JSONObject jsonObject = new JSONObject(tempNoteDetails);

			writeLog(Integer.parseInt(userId), "A", " Shared a copy of note :"+  changedNoteNameWithDouble((String)jsonObject.get("noteName")) + " in "+ mnList.getListName() +" with all Contacts", mnList.getListType(), mnList.getListId().toString(), (String) jsonObject.get("noteId"), null, mnList.getListType());

			if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
				writeLog(Integer.parseInt(userId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> Shared a copy of note you :"+  changedNoteNameWithDouble((String)jsonObject.get("noteName")) , mnList.getListType(),mnList.getListId().toString(), (String) jsonObject.get("noteId"),notUserIdSet, mnList.getListType());
			}

			if(logger.isDebugEnabled())
				logger.debug(" copyNoteToAllContact method returened successfully " );
			
			return "1";

		} catch (Exception e) {
			logger.error("Exception while Copy Note To All Contact :" + e);
		}
		return "Exception while Copy Note To All Contact";
	}

	public String updateAllContactMembersForCopyNote(String userId,
			String listId, String noteId) {
		
		if(logger.isDebugEnabled())
			logger.debug("updateAllContactMembersForCopyNote method called in Impl :"+userId+" noteId: "+noteId);
		Query query = null;
		try {
			query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			Set<String> updateNoteDetails=new TreeSet<String>();
			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					removeNoteDeatils = note;
					if (note.contains("\"copyToAllContact\":0")) {
						note = note.replace("\"copyToAllContact\":0","\"copyToAllContact\":1");
					}
					tempNoteDetails = note;
				}
				updateNoteDetails.add(note);
			}
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			
			//Sharing All Contact Details Base Update Note Details In Mn_NoteDetails Table Also - Venu
			if(!mnList.getListType().equalsIgnoreCase("Schedule")){
				query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
				MnNoteDetails noteDetails= mongoOperations.findOne(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
				if(noteDetails!=null){		
					update = new Update();
					update.set("copyToAllContact", true);
					mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNNOTEDETAILS);
				}
			}
			//End -Venu
		} catch (Exception e) {
			logger.error("Exception while Updating All Contact Members For Note :"+ e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" updateAllContactMembersForCopyNote method returened successfully " );
		return "Exception while All Contact Updating Members For Note";
	}

	@Override
	public String copyNoteToGroups(List<Integer> groups, String listId,String noteId, boolean withComments, String userId,String fullName, String firstName, String noteName) {
		List<MnGroupDetailsDomain> domains = new ArrayList<MnGroupDetailsDomain>();
		Query query = null;
		String qroupIds = groups.toString();
		Set<Integer> notUserIdSet = new HashSet<Integer>();
		List<Integer> groupUser = new ArrayList<Integer>();
		String oldnoteName=null;
		String newnoteName=null;
		boolean copyExist=false;
		boolean mailNotify=false;
		Date date=new Date();
		if(logger.isDebugEnabled())
			logger.debug("copyNoteToGroups method called in Impl :"+userId+" fullName: "+fullName);
		try {
			query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			String tempNoteDetails = null;
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					tempNoteDetails = note;
				}
			}
			JSONObject jsonObject = new JSONObject(tempNoteDetails);
			oldnoteName=changedNoteNameWithDouble((String) jsonObject.get("noteName"));
			// Find user based on Groups
			query = new Query(Criteria.where("groupId").in(groups).and("status").is("A"));

			domains = mongoOperations.find(query, MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);
			//add the comment  in cmt table
			List<MnComments> mnCommentsList = null;
			JSONObject jsonObject1 = null;
			List<Integer> cmtList = new ArrayList<Integer>();
			int cId = 0;
			
			if (withComments) {
				jsonObject1 = new JSONObject(tempNoteDetails);
				String commenntString =(String) jsonObject1.get("comments");
				if (commenntString.lastIndexOf(']') != -1) {
					commenntString = commenntString.substring(0, commenntString.lastIndexOf(']'));
				}
				if (commenntString.lastIndexOf('[') != -1) {
					commenntString = commenntString.substring( commenntString.lastIndexOf('[')+1, commenntString.length());
				}
				String[] comts = commenntString.split(",");
				if(comts.length>0 && !comts[0].equals("") ){
					for(String str:comts){
						cmtList.add(Integer.parseInt(str.trim()));
					}
				}
				
				
				Query query2 = new Query(Criteria.where("cId").in(cmtList));
				mnCommentsList = mongoOperations.find(query2,MnComments.class, JavaMessages.Mongo.MNCOMMETS);
				
				
				List<MnComments> mnCommentsListTot = mongoOperations.findAll(MnComments.class, JavaMessages.Mongo.MNCOMMETS);
				if (mnCommentsListTot != null && mnCommentsListTot.size() != 0) {
					cId = mnCommentsListTot.size() + 1;
				} else {
					cId = 1;
				}
			}
			
			// copy attach file changes
						List<Integer> attachList = new ArrayList<Integer>();
						List<MnAttachmentDetails> attachmentDetails = null;
						int aId=0;
						
						jsonObject1 = new JSONObject(tempNoteDetails);
						if (!tempNoteDetails.contains("\"attachFilePath\":\"\"")) {
						String attachFilePathString =(String) jsonObject1.get("attachFilePath");
						if (attachFilePathString.lastIndexOf(']') != -1) {
							attachFilePathString = attachFilePathString.substring(0, attachFilePathString.lastIndexOf(']'));
						}
						if (attachFilePathString.lastIndexOf('[') != -1) {
							attachFilePathString = attachFilePathString.substring( attachFilePathString.lastIndexOf('[')+1, attachFilePathString.length());
						}
						String[] attachFile = attachFilePathString.split(",");
						
						for(String str:attachFile){
							if(str!=null && !str.isEmpty())
								attachList.add(Integer.parseInt(str.trim()));
						}
						Query query2 = new Query(Criteria.where("attachId").in(attachList));
						attachmentDetails = mongoOperations.find(query2,MnAttachmentDetails.class, JavaMessages.Mongo.MNATTACHMENTDETAILS);
						
						List<MnAttachmentDetails> mnAttachmentListTot = mongoOperations.findAll(MnAttachmentDetails.class, JavaMessages.Mongo.MNATTACHMENTDETAILS);
						if (mnAttachmentListTot != null && mnAttachmentListTot.size() != 0) {
							aId = mnAttachmentListTot.size() + 1;
						} else {
							aId = 1;
						}
						}
						
			
			
			for (MnGroupDetailsDomain in : domains) {
				// Adding Selected List to Selected Note
				try {
					notUserIdSet.add(in.getUserId());
					groupUser.add(in.getUserId());
				} catch (Exception e) {
					logger.error(" Exception while Copy To Groups inside " + e);
				}
				Query addQuery = new Query(Criteria.where("userId").is(in.getUserId()).and("listType").is(mnList.getListType()).and("defaultNote").is(true));

				MnList addNote = mongoOperations.findOne(addQuery,MnList.class, JavaMessages.Mongo.MNLIST);
				
				// calculate copy number
				int i=0;
				for (String note : addNote.getMnNotesDetails()) {
					JSONObject existNote = new JSONObject(note);										
					if (note.contains("\"copyFromMember\":\"" + userId + "\"") && note.contains("\"status\":\"A\"")) {
						newnoteName=(String) existNote.get("noteName");
						if(newnoteName.equals(oldnoteName))
						{
							copyExist=true;
							i=i+1;
						}
						else
						{
							int position=newnoteName.lastIndexOf("-copy");
							String noteNameArr=newnoteName;
								if(position!=-1)
								{
									noteNameArr=newnoteName.substring(0,position);
								}
						if(noteNameArr.equals(oldnoteName))
						{
							copyExist=true;
							i=i+1;
						}
						}

					}
				}
				
				if(copyExist)
				{
					if(i==1)
						newnoteName=oldnoteName+"-copy";
					else
						newnoteName=oldnoteName+"-copy("+i+")";
				}
				else
					newnoteName=oldnoteName;
				
				///////
				
				Integer size = addNote.getMnNotesDetails().size() + 1;
				tempNoteDetails = tempNoteDetails.replace("\"noteId\":\""+ noteId + "\"", "\"noteId\":\"" + size + "\"");
				tempNoteDetails=tempNoteDetails.replace("\"noteName\":\""+ oldnoteName +"\"","\"noteName\":\""+newnoteName+"\"");

				if (!tempNoteDetails.contains("\"notesMembers\":\"\""))
					tempNoteDetails = tempNoteDetails.replace(notesMembersUpdateConvertToJsonObject(tempNoteDetails),"\"notesMembers\":\"\"");

				if (!tempNoteDetails.contains("\"noteGroups\":\"\""))
					tempNoteDetails = tempNoteDetails.replace(noteGroupsUpdateConvertToJsonObject(tempNoteDetails,true),"\"noteGroups\":\"\"");

				if (tempNoteDetails.contains("\"noteSharedAllContact\":1"))
					tempNoteDetails = tempNoteDetails.replace("\"noteSharedAllContact\":1","\"noteSharedAllContact\":0");
				
				if (!tempNoteDetails.contains("\"copyToMember\":\"\""))
					tempNoteDetails = tempNoteDetails.replace(notesMembersCopyUpdateConvertToJsonObject(tempNoteDetails),"\"copyToMember\":\"\"");

				if (!tempNoteDetails.contains("\"copyToGroup\":\"\""))
					tempNoteDetails = tempNoteDetails.replace(noteGroupsCopyUpdateConvertToJsonObject(tempNoteDetails),"\"copyToGroup\":\"\"");

				if (tempNoteDetails.contains("\"copyToAllContact\":1"))
					tempNoteDetails = tempNoteDetails.replace("\"copyToAllContact\":1","\"copyToAllContact\":0");
				
				if (tempNoteDetails.contains("\"copyFromMember\":\"\""))
					tempNoteDetails = tempNoteDetails.replace("\"copyFromMember\":\"\"","\"copyFromMember\":\"" + userId + "\"");
				else
					tempNoteDetails = tempNoteDetails.replace(noteFromCopyUpdateConvertToJsonObject(tempNoteDetails),"\"copyFromMember\":\"" + userId + "\"");
				
				tempNoteDetails = tempNoteDetails.replace("\"publicUser\":\""+ (String) jsonObject1.get("publicUser") + "\"", "\"publicUser\":\"\"");
				
				tempNoteDetails = tempNoteDetails.replace("\"publicDate\":\""+ (String) jsonObject1.get("publicDate") + "\"", "\"publicDate\":\"\"");
				
				tempNoteDetails = tempNoteDetails.replace("\"access\":\""+ (String) jsonObject1.get("access") + "\"", "\"access\":\"private\"");
				
				if (!withComments) {
					if (!tempNoteDetails.contains("\"comments\":\"\""))
						tempNoteDetails = tempNoteDetails.replace(noteCommentsUpdateConvertToJsonObject(tempNoteDetails),"\"comments\":\"\"");
				}else{
					try{
						if(cmtList!= null && !cmtList.isEmpty()){
							cmtList.clear();
						}
						
						for(MnComments comments : mnCommentsList){
							comments.setcId(cId);
							cmtList.add(cId);
							cId++;
							comments.setListId(addNote.getListId().toString());
							comments.setNoteId(size.toString());
						}				
						mongoOperations.insert(mnCommentsList, JavaMessages.Mongo.MNCOMMETS);
						
						tempNoteDetails = tempNoteDetails.replace(noteCommentsUpdateConvertToJsonObject(tempNoteDetails),"\"comments\":\""+cmtList.toString()+"\"");
						
					}catch (Exception e) {
						logger.error(e);
					}
				}

				if (!tempNoteDetails.contains("\"vote\":\"\""))
					tempNoteDetails = tempNoteDetails.replace(noteVotesUpdateConvertToJsonObject(tempNoteDetails),"\"vote\":\"\"");
				
				if (!tempNoteDetails.contains("\"tag\":\"\"")) {
					tempNoteDetails = tempNoteDetails.replace(noteTagUpdateConvertToJsonObject(tempNoteDetails),"\"tag\":\"\"");
				}
				
				if (!tempNoteDetails.contains("\"remainders\":\"\"")) {
					tempNoteDetails = tempNoteDetails.replace(noteReminderUpdateConvertToJsonObject(tempNoteDetails),"\"remainders\":\"\"");
				}
				
				if (!tempNoteDetails.contains("\"dueDate\":\"\"")) {
					tempNoteDetails = tempNoteDetails.replace(noteDueDateUpdateConvertToJsonObject(tempNoteDetails),"\"dueDate\":\"\"");
				}
				
				if (!tempNoteDetails.contains("\"dueTime\":\"\"")) {
					tempNoteDetails = tempNoteDetails.replace(noteDueTimeUpdateConvertToJsonObject(tempNoteDetails),"\"dueTime\":\"\"");
				}
				
					// entery in attaachement table
					if (!tempNoteDetails.contains("\"attachFilePath\":\"\"")) 
					{
						try{
							if(attachList!= null && !attachList.isEmpty()){
								attachList.clear();
							}
							for(MnAttachmentDetails achmentDetails : attachmentDetails){
								achmentDetails.setAttachId(aId);
								attachList.add(aId);
								aId++;
								achmentDetails.setListId(addNote.getListId());
								achmentDetails.setNoteId(size.toString());
							}
							mongoOperations.insert(attachmentDetails, JavaMessages.Mongo.MNATTACHMENTDETAILS);
							tempNoteDetails = tempNoteDetails.replace(noteFilePathUpdateConvertToJsonObject(tempNoteDetails),"\"attachFilePath\":\""+attachList.toString()+"\"");
						}catch (Exception e) {
							logger.error(e);
						}
					}
				
				if (!tempNoteDetails.contains("\"privateTags\":\"\"")) {
					tempNoteDetails = tempNoteDetails.replace(notePrivateTagUpdateConvertToJsonObject(tempNoteDetails),"\"privateTags\":\"\"");
				}
				
				if (addNote.getMnNotesDetails() != null	&& addNote.getMnNotesDetails().isEmpty()) {
					addNote.setMnNotesDetails(new HashSet<String>());
					addNote.getMnNotesDetails().add(tempNoteDetails);
				} else {
					addNote.getMnNotesDetails().add(tempNoteDetails);
				}
				Update addUpdate = new Update();
				addUpdate.set("mnNotesDetails", addNote.getMnNotesDetails());
				mongoOperations.updateFirst(addQuery, addUpdate,JavaMessages.Mongo.MNLIST);
				
				//Sharing Copy Group Details Base Added New Note Details In Mn_NoteDetails Table Also - Venu
				if(!addNote.getListType().equals("schedule")){
					MnNoteDetails details=new MnNoteDetails();
					JSONObject json =new JSONObject(tempNoteDetails);
					details.setNoteName(newnoteName);
					details.setStartDate(date);
					details.setNoteSharedAllContact(false);
					details.setStatus("A");
					details.setOwnerNoteStatus("A");
					details.setAccess("private");
					details.setDueDate("");
					details.setDueTime("");
					
					String description = (String) json.get("description");
					if(description.indexOf("\"") != -1){
						description = description.replace("\"", "\\\"");
					}
					details.setDescription(description);
					
					details.setPublicUser("");
					details.setCopyFromMember(userId);
					details.setUserId(addNote.getUserId());
					details.setListId(addNote.getListId());
					details.setListName(addNote.getListName());
					details.setListType(addNote.getListType());
					details.setNoteAccess(addNote.getNoteAccess());
					details.setDefaultNote(addNote.isDefaultNote());
					details.setEnableDisableFlag(addNote.isEnableDisableFlag());
					details.setNoteId(size);
					if (withComments && cmtList!=null)
						details.setComments(cmtList);
					if(attachList!=null && !attachList.isEmpty())
						details.setAttachFilePath(attachList);
					mongoOperations.insert(details,JavaMessages.Mongo.MNNOTEDETAILS);
				}
				//End -Venu
			}
			updateGroupsForNoteCopy(qroupIds, listId, noteId);
			
			
////	//////////mail content /////////////////
			List<MnUsers> list = getUsersDetails(groupUser);
			if (list != null && !list.isEmpty())
			{
				for (MnUsers users : list)
				{
					try{
						mailNotify=getUserMailNotification(users.getUserId(), mnList.getListType());
						if(users.getNotificationFlag().equalsIgnoreCase("yes"))
						{
							if(mailNotify){
							SendMail sendMail=new SendMail(); 	
							String recipients[]={users.getEmailId()};
						
							String message=MailContent.sharingNotification+users.getUserFirstName()+MailContent.sharingNotification1+fullName+"  shared a new note with you on Musicnote : "+ noteName+" "+MailContent.sharingNotification2;
							String subject=firstName+" shared a copy of a note with you";
									
							sendMail.postEmail(recipients, subject,message);
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			/////////////////////////

			String group = "";
			for (Integer groupId : groups) {
				Query query2 = new Query(Criteria.where("groupId").is(groupId));
				MnGroupDomain mnGroupDomain = mongoOperations.findOne(query2,MnGroupDomain.class, JavaMessages.Mongo.MNGROUP);
				if (mnGroupDomain != null) {
					group = group + "<br> <p> " + mnGroupDomain.getGroupName()	+ "</p>";
				}
			}
			MnUsers mnUsers = getUserDetailsObject(Integer.parseInt(userId));
			writeLog(Integer.parseInt(userId), "A", "Shared a copy of a note:"+  changedNoteNameWithDouble((String)jsonObject.get("noteName")) + " in "+ mnList.getListName() +" with Group <B>" + group + "</B>", mnList.getListType(), listId, noteId, null, mnList.getListType());

			if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
				writeLog(Integer.parseInt(userId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> Shared a copy of a note:"+  changedNoteNameWithDouble((String)jsonObject.get("noteName")) + " with Group <B>" + group + "</B>", mnList.getListType(), listId, noteId, notUserIdSet,  mnList.getListType());
			}
			if(logger.isDebugEnabled())
				logger.debug(" copyNoteToGroups method returened successfully " );
			
			return "1";

		} catch (Exception e) {
			logger.error("Exception while Copy To Groups For Note :" + e);
		}
		return "Exception while Copy To Groups For Note";
	}

	// copy to groups
	public String updateGroupsForNoteCopy(String groups, String listId,	String noteId) {
		
		if(logger.isDebugEnabled())
			logger.debug("updateGroupsForNoteCopy method called in Impl noteId: "+noteId);
		try {
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			Set<String> updateNoteDetails=new TreeSet<String>();
			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					removeNoteDeatils = note;
					if (note.contains("\"copyToGroup\":\"\"")) {
						note = note.replace("\"copyToGroup\":\"\"","\"copyToGroup\":\"" + groups + "\"");
					} else {
						removeNoteDeatils = note;
						String oldMemberIds = notesSharingGroupCopyDetailsConvertToJsonObject(removeNoteDeatils); 
						if(oldMemberIds!=null && !oldMemberIds.isEmpty()){
							if(groups!=null && !groups.equals("")){
								groups=groups.replace("[", "");
								groups=groups.replace("]", "");
								groups=groups.trim();
							}
							
							oldMemberIds=oldMemberIds+","+groups;
							oldMemberIds = "[" + oldMemberIds + "]";
							note = note.replace(noteCopyGroupsUpdateConvertToJsonObject(note),"\"copyToGroup\":\"" + oldMemberIds + "\"");
						}
						
					}
					tempNoteDetails = note;
				}
				updateNoteDetails.add(note);
			}
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			
			//Sharing Copy Groups Details Base Update Note Details In Mn_NoteDetails Table Also - Venu
			if(mnList!=null && !mnList.getListType().equals("schedule")){
				if(groups!=null && !groups.equals("")){
					groups=groups.replace("[", "");
					groups=groups.replace("]", "");
					groups=groups.trim();
					String[] groupIds=groups.split(",");
					List<Integer> addingGroupIds=new ArrayList<Integer>();
					for(int i=0;i<groupIds.length;i++){
						addingGroupIds.add(Integer.parseInt(groupIds[i]));
					}
					query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
					MnNoteDetails noteDetails= mongoOperations.findOne(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
					if(noteDetails.getCopyToGroup()!=null && !noteDetails.getCopyToGroup().isEmpty())
						noteDetails.getCopyToGroup().addAll(addingGroupIds);
					else
						noteDetails.setCopyToGroup(addingGroupIds);

					update = new Update();
					update.set("copyToGroup", noteDetails.getCopyToGroup());
					mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNNOTEDETAILS);
				}
			}
			//End - Venu

		} catch (Exception e) {
			logger.error("Exception while Updating copy Group For Note :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" updateGroupsForNoteCopy method returened successfully " );
		
		return "Exception while Updating Copy Note Group For Note";
	}

	public String noteCopyGroupsUpdateConvertToJsonObject(String jsonStr) {
		String groups = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			if(logger.isDebugEnabled())
				logger.debug("noteCopyGroupsUpdateConvertToJsonObject method called in Impl :");
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldGroups = (String) jsonObject.get("copyToGroup");

				groups = "\"copyToGroup\":\"" + oldGroups + "\"";
			} catch (Exception e) {
				logger.error("Exception while converting json to Notes Sharing Details Group object In Impl !",e);
			}
		}
		return groups;
	}

	@Override
	public String copyNoteToIndividual(List<Integer> members, String listId, String noteId, boolean withComments,String fromUserId) {
		Query query=null;
		String membersId=members.toString();
		String noteAccess="";
		List<Integer> oldListInt=new ArrayList<Integer>();
		String oldnoteName=null;
		String newnoteName=null;
		boolean copyExist=false;
		Date date = new Date();
		
		if(logger.isDebugEnabled())
			logger.debug("copyNoteToIndividual method called in Impl fromUserId: "+fromUserId+" noteId: "+noteId);
		try
		{
			query=new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
			
			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			String tempNoteDetails = null;
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					tempNoteDetails = note;

				}
			}
			Integer size;
			
			JSONObject jsonObject1 = null;
			try {
				jsonObject1 = new JSONObject(tempNoteDetails);
				String oldMembers=(String) jsonObject1.get("copyToMember");
				oldnoteName=(String) jsonObject1.get("noteName");
				noteAccess=(String) jsonObject1.get("access");
				if(oldMembers!=null && !oldMembers.equals("")){
					oldMembers=oldMembers.replace("[", "");
					oldMembers=oldMembers.replace("]", "");
					oldMembers=oldMembers.trim();
					String[] str=oldMembers.split(",");
					if(str!=null && str.length !=-1){
						for(int i=0;i<str.length;i++){
							oldListInt.add(Integer.parseInt(str[i].trim()));
						}
					}else{
						oldListInt.add(Integer.parseInt(oldMembers.trim()));
					}
						
				}
				
			}
			catch (Exception e) {
				logger.error("Exception while converting json to Notes Sharing Details object notes Copy In Impl !", e);
			}
			
			//add the comment  in cmt table
			List<MnComments> mnCommentsList = null;
			JSONObject jsonObject2 = null;
			List<Integer> cmtList = new ArrayList<Integer>();
			int cId = 0;
			
			// copy comments
			if (withComments) {
				jsonObject2 = new JSONObject(tempNoteDetails);
				String commenntString =(String) jsonObject2.get("comments");
				if (commenntString.lastIndexOf(']') != -1) {
					commenntString = commenntString.substring(0, commenntString.lastIndexOf(']'));
				}
				if (commenntString.lastIndexOf('[') != -1) {
					commenntString = commenntString.substring( commenntString.lastIndexOf('[')+1, commenntString.length());
				}
				String[] comts = commenntString.split(",");
				
				for(String str:comts){
					if(str!=null && !str.isEmpty())
						cmtList.add(Integer.parseInt(str.trim()));
				}
				Query query2 = new Query(Criteria.where("cId").in(cmtList));
				mnCommentsList = mongoOperations.find(query2,MnComments.class, JavaMessages.Mongo.MNCOMMETS);
				
				
				List<MnComments> mnCommentsListTot = mongoOperations.findAll(MnComments.class, JavaMessages.Mongo.MNCOMMETS);
				if (mnCommentsListTot != null && mnCommentsListTot.size() != 0) {
					cId = mnCommentsListTot.size() + 1;
				} else {
					cId = 1;
				}
			}
			
			// copy attach file changes
			List<Integer> attachList = new ArrayList<Integer>();
			List<MnAttachmentDetails> attachmentDetails = null;
			int aId=0;
			
			
			jsonObject2 = new JSONObject(tempNoteDetails);
			if (!tempNoteDetails.contains("\"attachFilePath\":\"\"")) {
			String attachFilePathString =(String) jsonObject2.get("attachFilePath");
			if (attachFilePathString.lastIndexOf(']') != -1) {
				attachFilePathString = attachFilePathString.substring(0, attachFilePathString.lastIndexOf(']'));
			}
			if (attachFilePathString.lastIndexOf('[') != -1) {
				attachFilePathString = attachFilePathString.substring( attachFilePathString.lastIndexOf('[')+1, attachFilePathString.length());
			}
			String[] attachFile = attachFilePathString.split(",");
			
			for(String str:attachFile){
				if(str!=null && !str.isEmpty())
					attachList.add(Integer.parseInt(str.trim()));
			}
			Query query2 = new Query(Criteria.where("attachId").in(attachList));
			attachmentDetails = mongoOperations.find(query2,MnAttachmentDetails.class, JavaMessages.Mongo.MNATTACHMENTDETAILS);
			
			
			List<MnAttachmentDetails> mnAttachmentListTot = mongoOperations.findAll(MnAttachmentDetails.class, JavaMessages.Mongo.MNATTACHMENTDETAILS);
			if (mnAttachmentListTot != null && mnAttachmentListTot.size() != 0) {
				aId = mnAttachmentListTot.size() + 1;
			} else {
				aId = 1;
			}
			}
			
			
			for(Integer in:members){
					//Adding Selected List to Selected Note
					Query addQuery=new Query(Criteria.where("userId").is(in).and("listType").is(mnList.getListType()).and("defaultNote").is(true));
	
					MnList addNote = mongoOperations.findOne(addQuery,MnList.class, JavaMessages.Mongo.MNLIST);
					
					// calculate copy number
					int i=0;
					for (String note : addNote.getMnNotesDetails()) {
						JSONObject existNote = new JSONObject(note);
						if (note.contains("\"copyFromMember\":\"" + fromUserId + "\"")&& note.contains("\"status\":\"A\"")) {
							newnoteName=(String) existNote.get("noteName");
							if(newnoteName.equals(oldnoteName))
							{
								copyExist=true;
								i=i+1;
							}
							else
							{
								int position=newnoteName.lastIndexOf("-copy");
								String noteNameArr=newnoteName;
									if(position!=-1)
									{
										noteNameArr=newnoteName.substring(0,position);
									}
							if(noteNameArr.equals(oldnoteName))
							{
								copyExist=true;
								i=i+1;
							}
							}

						}
					}
					
					if(copyExist)
					{
						if(i==1)
							newnoteName=oldnoteName+"-copy";
						else
							newnoteName=oldnoteName+"-copy("+i+")";
					}
					else
						newnoteName=oldnoteName;

					
					if (addNote.getMnNotesDetails().size() != 0)
						size = addNote.getMnNotesDetails().size() + 1;
					else
						size = 1;
					
					tempNoteDetails = tempNoteDetails.replace("\"noteId\":\""+ noteId + "\"", "\"noteId\":\"" + size + "\"");
					tempNoteDetails=tempNoteDetails.replace("\"noteName\":\""+ oldnoteName +"\"","\"noteName\":\""+newnoteName+"\"");
					
					tempNoteDetails = tempNoteDetails.replace("\"access\":\""+ noteAccess + "\"", "\"access\":\"private\"");
					
					if (!tempNoteDetails.contains("\"notesMembers\":\"\"")) {
						tempNoteDetails = tempNoteDetails.replace(notesMembersUpdateConvertToJsonObject(tempNoteDetails),"\"notesMembers\":\"\"");
					}
	
					if (!tempNoteDetails.contains("\"noteGroups\":\"\""))
						tempNoteDetails = tempNoteDetails.replace(noteGroupsUpdateConvertToJsonObject(tempNoteDetails,true),"\"noteGroups\":\"\"");
	
					if (tempNoteDetails.contains("\"noteSharedAllContact\":1"))
						tempNoteDetails = tempNoteDetails.replace("\"noteSharedAllContact\":1","\"noteSharedAllContact\":0");
					
					if (!tempNoteDetails.contains("\"copyToMember\":\"\""))
						tempNoteDetails = tempNoteDetails.replace(notesMembersCopyUpdateConvertToJsonObject(tempNoteDetails),"\"copyToMember\":\"\"");

					if (!tempNoteDetails.contains("\"copyToGroup\":\"\""))
						tempNoteDetails = tempNoteDetails.replace(noteGroupsCopyUpdateConvertToJsonObject(tempNoteDetails),"\"copyToGroup\":\"\"");

					if (tempNoteDetails.contains("\"copyToAllContact\":1"))
						tempNoteDetails = tempNoteDetails.replace("\"copyToAllContact\":1","\"copyToAllContact\":0");
					
					if (tempNoteDetails.contains("\"copyFromMember\":\"\""))
						tempNoteDetails = tempNoteDetails.replace("\"copyFromMember\":\"\"","\"copyFromMember\":\"" + fromUserId + "\"");
					else
						tempNoteDetails = tempNoteDetails.replace(noteFromCopyUpdateConvertToJsonObject(tempNoteDetails),"\"copyFromMember\":\"" + fromUserId + "\"");
					
					tempNoteDetails = tempNoteDetails.replace("\"publicUser\":\""+ (String) jsonObject1.get("publicUser") + "\"", "\"publicUser\":\"\"");
					
					tempNoteDetails = tempNoteDetails.replace("\"publicDate\":\""+ (String) jsonObject1.get("publicDate") + "\"", "\"publicDate\":\"\"");
					
	
					if (!withComments) {
						if (!tempNoteDetails.contains("\"comments\":\"\""))
							tempNoteDetails = tempNoteDetails.replace(noteCommentsUpdateConvertToJsonObject(tempNoteDetails),"\"comments\":\"\"");
					}else{
						try{
							if(cmtList!= null && !cmtList.isEmpty()){
								cmtList.clear();
							}
							for(MnComments comments : mnCommentsList){
								comments.setcId(cId);
								cmtList.add(cId);
								cId++;
								comments.setListId(addNote.getListId().toString());
								comments.setNoteId(size.toString());
							}
							mongoOperations.insert(mnCommentsList, JavaMessages.Mongo.MNCOMMETS);
							
							tempNoteDetails = tempNoteDetails.replace(noteCommentsUpdateConvertToJsonObject(tempNoteDetails),"\"comments\":\""+cmtList.toString()+"\"");
						}catch (Exception e) {
							logger.error(e);
						}
					}
	
					if (!tempNoteDetails.contains("\"vote\":\"\""))
						tempNoteDetails = tempNoteDetails.replace(noteVotesUpdateConvertToJsonObject(tempNoteDetails),"\"vote\":\"\"");
					
					if (!tempNoteDetails.contains("\"tag\":\"\"")) {
						tempNoteDetails = tempNoteDetails.replace(noteTagUpdateConvertToJsonObject(tempNoteDetails),"\"tag\":\"\"");
					}
					
					if (!tempNoteDetails.contains("\"remainders\":\"\"")) {
						tempNoteDetails = tempNoteDetails.replace(noteReminderUpdateConvertToJsonObject(tempNoteDetails),"\"remainders\":\"\"");
					}
					
					if (!tempNoteDetails.contains("\"dueDate\":\"\"")) {
						tempNoteDetails = tempNoteDetails.replace(noteDueDateUpdateConvertToJsonObject(tempNoteDetails),"\"dueDate\":\"\"");
					}
					
					if (!tempNoteDetails.contains("\"dueTime\":\"\"")) {
						tempNoteDetails = tempNoteDetails.replace(noteDueTimeUpdateConvertToJsonObject(tempNoteDetails),"\"dueTime\":\"\"");
					}
					
					// entery in attaachement table
					if (!tempNoteDetails.contains("\"attachFilePath\":\"\"")) 
					{
						try{
							if(attachList!= null && !attachList.isEmpty()){
								attachList.clear();
							}
							for(MnAttachmentDetails achmentDetails : attachmentDetails){
								achmentDetails.setAttachId(aId);
								attachList.add(aId);
								aId++;
								achmentDetails.setListId(addNote.getListId());
								achmentDetails.setNoteId(size.toString());
							}
							mongoOperations.insert(attachmentDetails, JavaMessages.Mongo.MNATTACHMENTDETAILS);
							tempNoteDetails = tempNoteDetails.replace(noteFilePathUpdateConvertToJsonObject(tempNoteDetails),"\"attachFilePath\":\""+attachList.toString()+"\"");
						}catch (Exception e) {
							logger.error(e);
						}
					}
					
					
					if (!tempNoteDetails.contains("\"privateTags\":\"\"")) {
						tempNoteDetails = tempNoteDetails.replace(notePrivateTagUpdateConvertToJsonObject(tempNoteDetails),"\"privateTags\":\"\"");
					}
	
					if (addNote.getMnNotesDetails() != null&& addNote.getMnNotesDetails().isEmpty()) {
						addNote.setMnNotesDetails(new HashSet<String>());
						addNote.getMnNotesDetails().add(tempNoteDetails);
					} else {
						addNote.getMnNotesDetails().add(tempNoteDetails);
					}
	
					Update addUpdate = new Update();
					addUpdate.set("mnNotesDetails", addNote.getMnNotesDetails());
					mongoOperations.updateFirst(addQuery, addUpdate,JavaMessages.Mongo.MNLIST);
					
					
					//Sharing Copy To Individual Base Add New Note Details In Mn_NoteDetails Table Also - Venu
					if(!addNote.getListType().equals("schedule")){
						MnNoteDetails details=new MnNoteDetails();
						JSONObject json =new JSONObject(tempNoteDetails);
						details.setNoteName(newnoteName);
						details.setStartDate(date);
						details.setNoteSharedAllContact(false);
						details.setStatus("A");
						details.setOwnerNoteStatus("A");
						details.setAccess("private");
						details.setDueDate("");
						details.setDueTime("");
						
						String description = (String) json.get("description");
						if(description.indexOf("\"") != -1){
							description = description.replace("\"", "\\\"");
						}
						
						details.setDescription(description);
						details.setPublicUser("");
						details.setCopyFromMember(fromUserId);
						details.setUserId(in);
						details.setListId(addNote.getListId());
						details.setListName(addNote.getListName());
						details.setListType(addNote.getListType());
						details.setNoteAccess(addNote.getNoteAccess());
						details.setDefaultNote(addNote.isDefaultNote());
						details.setEnableDisableFlag(addNote.isEnableDisableFlag());
						details.setNoteId(size);
						if (withComments && cmtList!=null)
							details.setComments(cmtList);
						if(attachList!=null && !attachList.isEmpty())
							details.setAttachFilePath(attachList);
						
						mongoOperations.insert(details,JavaMessages.Mongo.MNNOTEDETAILS);
					}
					//End -Venu
			}
			updateMembersForNoteCopy(members,listId,noteId,oldListInt);
			
			
			
			Set<Integer> notUserIdSet = new HashSet<Integer>();
			notUserIdSet.addAll(members);
			String addUser = "", addUserName = "";
			if (notUserIdSet != null) {
				for (Integer integer : notUserIdSet) {
					MnUsers mnUsers2 = getUserDetailsObject(integer);
					if (mnUsers2 != null) {
						addUser = addUser + "<br> <p>" + mnUsers2.getUserName()+ "</p>";
						addUserName = addUserName + "<B>"+ mnUsers2.getUserName() + "</B>, ";
					}
				}
			}
			addUserName = addUserName.substring(0,addUserName.lastIndexOf(","));
			if(addUser.length() > 36){
				addUser =addUser.substring(0,35)+"...";
			}
			if(addUserName.length() > 36){
				addUserName =addUserName.substring(0,35)+"...";
			}
			JSONObject jsonObject = new JSONObject(tempNoteDetails);

			MnUsers mnUsers = getUserDetailsObject(Integer.parseInt(fromUserId));

			writeLog(mnList.getUserId(), "A", "Shared a copy of a note:"+  changedNoteNameWithDouble((String)jsonObject.get("noteName")) + " in "+ mnList.getListName() +" with " + addUserName, "music", listId, noteId, null, "music");

			if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
				writeLog(mnList.getUserId(), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName()	+ "</B> Shared a copy of a note with you:"+  changedNoteNameWithDouble((String)jsonObject.get("noteName")) , "music", listId, noteId, notUserIdSet, "music");
			}
			
			if(logger.isDebugEnabled())
				logger.debug(" copyNoteToIndividual method returened successfully " );
			return "1";

		} catch (Exception e) {
			logger.error("Exception while Copy To Individual For Note :" + e);
		}
		return "Exception while Copy To Individual For Note";
	}

	public String updateMembersForNoteCopy(List<Integer> members, String listId, String noteId, List<Integer> oldListInt) {
		if(logger.isDebugEnabled())
			logger.debug("updateMembersForNoteCopy method called in Impl noteId: "+noteId);
		try {
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			Set<String> updateNoteDetails=new TreeSet<String>();
			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					removeNoteDeatils = note;
					if (note.contains("\"copyToMember\":\"\"")) {
						note = note.replace("\"copyToMember\":\"\"","\"copyToMember\":\"" + members + "\"");
					} else {
						
							removeNoteDeatils = note;
							String oldMemberIds = notesSharingCopyDetailsConvertToJsonObject(removeNoteDeatils); 
							if(oldMemberIds!=null && !oldMemberIds.isEmpty()){
								if(members!=null && !members.equals("")){
									for(Integer in:members)
									{
										if(!oldListInt.contains(in))
											oldListInt.add(in);
									}
								}
								
								note = note.replace(notesCopyMembersUpdateConvertToJsonObject(note),"\"copyToMember\":\"" + oldListInt.toString() + "\"");
							}
						
					}
					tempNoteDetails = note;
				}
				updateNoteDetails.add(note);
			}
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			
			//Sharing Copy To Individual Base Update Note Deatils In Mn_NoteDetails Table Also - Venu
			if(mnList!=null && !mnList.getListType().equals("schedule")){
				query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
				MnNoteDetails noteDetails= mongoOperations.findOne(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
				if(noteDetails.getCopyToMember()!=null && !noteDetails.getCopyToMember().isEmpty())
					noteDetails.setCopyToMember(oldListInt);
				else
					noteDetails.setCopyToMember(members);
				
				update = new Update();
				update.set("copyToMember", noteDetails.getCopyToMember());
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNNOTEDETAILS);
			}
			//End - Venu
			if(logger.isDebugEnabled())
				logger.debug(" updateMembersForNoteCopy method returened successfully " );
			
		} catch (Exception e) {
			logger.error("Exception while Updating Members For Note :" + e);
		}
		return "Exception while Updating Members For Note";
	}

	public String notesCopyMembersUpdateConvertToJsonObject(String jsonStr) {
		String members = "";
		if(logger.isDebugEnabled())
			logger.debug("notesCopyMembersUpdateConvertToJsonObject method called in Impl :");
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldMembers = (String) jsonObject.get("copyToMember");

				members = "\"copyToMember\":\"" + oldMembers + "\"";
			} catch (Exception e) {
				logger.error("Exception while converting json to Notes Sharing Details object notes Copy In Impl !",e);
			}
		}
		return members;
	}

	public String getTimeZone(String timeZoneDate,String userId){
		String convertedDate="";
		
		if(logger.isDebugEnabled())
			logger.debug("getTimeZone method called in Impl :"+userId);
		try
		{
		if(timeZoneDate!=null && !timeZoneDate.isEmpty())
		{
		String loginUserZone=null;
		Query query3 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
			MnUsers mnUser = mongoOperations.findOne(query3, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			loginUserZone=mnUser.getTimeZone();
			
			Date tDate=new Date(timeZoneDate);
			DateFormat formatter = new SimpleDateFormat("MMM/d/yyyy hh:mm a");
			TimeZone obj = TimeZone.getTimeZone("GMT"+loginUserZone);
			Calendar c = Calendar.getInstance();
			c.setTime(tDate);
			formatter.setTimeZone(obj);
			convertedDate=formatter.format(tDate);
		}else
		{
			convertedDate="";
		}
		}catch (Exception e) {
			logger.error(" Exception getTimeZone :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug(" getTimeZone method returened successfully " );
		return convertedDate;
		
	}
	
	@Override
	public List<MnList> getList(String userId, String noteAccess) {
		List<MnList> mnListList = null;
		Query query = null;
		
		if(logger.isDebugEnabled())
			logger.debug("getList method called in Impl :"+userId);
		
		try {
			query = new Query(Criteria.where("noteAccess").is(noteAccess).and("status").is("A"));
			mnListList = mongoOperations.find(query, MnList.class,JavaMessages.Mongo.MNLIST);
		} catch (Exception e) {
			logger.error("Exception while fetching List :" + e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" getList method returened successfully " );
		return mnListList;
		
	}

	// Log writing methods
	public void commomLog(String tempNoteDetails, MnList mnList, String label,
			boolean stopNotiflag, String userId,boolean stopFollowerNotifi,boolean stopFrdFlag,boolean noteNameFlag,String endLabel,boolean listNameFlag,String pageName) {
		
		if(logger.isDebugEnabled())
			logger.debug("commomLog method called in Impl :"+userId);
		
		try {
			JSONObject jsonObject = new JSONObject(tempNoteDetails);
			MnUsers mnUsers = getUserDetailsObject(Integer.parseInt(userId));
			
			Set<Integer> notUserIdSet = new HashSet<Integer>();
			if (stopNotiflag) {
				String oldMember = notesSharingDetailsConvertToJsonObject(tempNoteDetails);
				if (oldMember != null && !oldMember.trim().equals("")) {
					String[] memberIds = oldMember.split(",");
					if (memberIds.length != 0) {
						for (int i = 0; i < memberIds.length; i++) {
							if (!memberIds[i].equals(userId)) {
								notUserIdSet.add(Integer.parseInt(memberIds[i]));
							}
						}
					}
				}
				if (!mnList.getUserId().equals(Integer.parseInt(userId))) {
					notUserIdSet.add(mnList.getUserId());
				}
				String groupId = null;
				if (tempNoteDetails.contains("\"noteGroups\"")) {
					if(!tempNoteDetails.contains("\"noteGroups\":\"\""))
						groupId = noteGroupsUpdateConvertToJsonObject(tempNoteDetails,false);
				} else if (tempNoteDetails.contains("\"eventGroups\"")){
					if(!tempNoteDetails.contains("\"eventGroups\":\"\""))
						groupId = eventGroupsUpdateConvertToJsonObject(tempNoteDetails,false);
				}
				if(groupId!= null && !groupId.equals("")){
					groupId = groupId.replace("[","");
					groupId = groupId.replace("]","");
					String[] grpIds = null;
					if(groupId.trim()!= ""){
						grpIds = groupId.split(",");
					}
					List<Integer> grpIdList = new ArrayList<Integer>();
					if(grpIds.length >0 && !grpIds[0].equals("")){
						for(String grpId:grpIds){
							grpIdList.add(Integer.parseInt(grpId.trim()));
						}
						Query query2 = new Query(Criteria.where("groupId").in(grpIdList).and("status").is("A"));
						List<MnGroupDetailsDomain> detailsDomain = mongoOperations.find(query2, MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);
						if (detailsDomain != null && !detailsDomain.isEmpty()) {
							for (MnGroupDetailsDomain domain : detailsDomain) {
								if(!notUserIdSet.contains(domain.getUserId())){
									notUserIdSet.add(domain.getUserId());
								}
							}
						}
					}
				}
				if (tempNoteDetails.contains("\"noteSharedAllContact\":1")) {
					stopFrdFlag = true;
				}else if (tempNoteDetails.contains("\"eventSharedAllContact\":1")){
					stopFrdFlag = true;
				}
			}
			if(stopFrdFlag){
				if(mnUsers.getFriends()!= null && !mnUsers.getFriends().isEmpty()){
					for(Integer userIds :mnUsers.getFriends()){
						if(!notUserIdSet.contains(userIds)){
							notUserIdSet.add(userIds);
						}
					}
				}
			}
			if(stopFollowerNotifi){
				if(mnUsers.getFollowers()!= null && !mnUsers.getFollowers().isEmpty()){
					for(Integer userIds :mnUsers.getFollowers()){
						if(!notUserIdSet.contains(userIds)){
							notUserIdSet.add(userIds);
						}
					}
				}
			}
			String noteNameLabel ="";
			if(noteNameFlag){
				if (jsonObject.has("eventName")){
					noteNameLabel = " " + (String)jsonObject.get("eventName");
				}else{
					noteNameLabel = " " + (String)jsonObject.get("noteName");
				}
				if(noteNameLabel.indexOf("\"")!= -1){
					noteNameLabel = noteNameLabel.replace("\"", "\\\""); 
				}
				if(listNameFlag){
					noteNameLabel = noteNameLabel + " in "+ mnList.getListName();
				}
			}
			
			if (jsonObject.has("noteName"))
				writeLog(Integer.parseInt(userId), "A", "" + label+noteNameLabel+endLabel , mnList.getListType(), mnList.getListId().toString(),(String) jsonObject.get("noteId"), null, pageName);
			else
				writeLog(Integer.parseInt(userId), "A", "" + label+noteNameLabel+endLabel, mnList.getListType(), mnList.getListId().toString(),(String) jsonObject.get("eventId"), null, pageName);
			
			
			if (stopNotiflag) {
				if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
					notUserIdSet.remove(Integer.parseInt(userId));
					if (jsonObject.has("noteName"))
						writeLog(Integer.parseInt(userId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> " + label+noteNameLabel+endLabel , mnList.getListType(), mnList.getListId().toString(),(String) jsonObject.get("noteId"), notUserIdSet, pageName);
					else
						writeLog(Integer.parseInt(userId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> " + label+noteNameLabel+endLabel, mnList.getListType(), mnList.getListId().toString(),(String) jsonObject.get("eventId"),notUserIdSet, pageName);
				}
			}
		} catch (Exception e) {
			logger.error("Exception while commomLog In Impl !", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" commomLog method returened successfully " );
	}

	public void moveAllNoteLog(Set<String> removeNoteDeatils, MnList mnList,
			String label, String newListName, String userId) {
		
		if(logger.isDebugEnabled())
			logger.debug("moveAllNoteLog method called in Impl :"+userId);
		try {
			MnUsers mnUsers = getUserDetailsObject(Integer.parseInt(userId));
			Set<Integer> notUserIdSet = new HashSet<Integer>();
			for (String tempNoteDetails : removeNoteDeatils) {

				String oldMember = notesSharingDetailsConvertToJsonObject(tempNoteDetails);
				if (oldMember != null && !oldMember.trim().equals("")) {
					String[] memberIds = oldMember.split(",");
					if (memberIds.length != 0) {
						for (int i = 0; i < memberIds.length; i++) {
							if (!memberIds[i].equals(userId)) {
								notUserIdSet.add(Integer.parseInt(memberIds[i]));
							}
						}
					}
				}
			}
			if (!mnList.getUserId().equals(Integer.parseInt(userId))) {
				notUserIdSet.add(mnList.getUserId());
			}
			
			writeLog(Integer.parseInt(userId), "A", "" + label + " "+  changedNoteNameWithDouble(mnList.getListName()) + " " + newListName , mnList.getListType(),mnList.getListId().toString(), "", null, mnList.getListType());

			if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
				notUserIdSet.remove(Integer.parseInt(userId));
				writeLog(Integer.parseInt(userId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> " + label + " "+  changedNoteNameWithDouble(mnList.getListName()) + " " + newListName , mnList.getListType(),mnList.getListId().toString(), "", notUserIdSet, mnList.getListType());
			}

		} catch (Exception e) {
			logger.error("Exception while moveAllNoteLog In Impl !", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" moveAllNoteLog method returened successfully " );
	}

	public void moveNoteLog(String tempNoteDetails, MnList mnList,
			String newListName, String label, String userId,boolean stopNotification) {
		
		if(logger.isDebugEnabled())
			logger.debug("moveNoteLog method called in Impl :"+userId);
		
		try {
			MnUsers mnUsers = getUserDetailsObject(Integer.parseInt(userId));
			Set<Integer> notUserIdSet = new HashSet<Integer>();
			if(stopNotification){
				String oldMember = notesSharingDetailsConvertToJsonObject(tempNoteDetails);
				if (oldMember != null && !oldMember.trim().equals("")) {
					String[] memberIds = oldMember.split(",");
					if (memberIds.length != 0) {
						for (int i = 0; i < memberIds.length; i++) {
							if (!memberIds[i].equals(userId)) {
								notUserIdSet.add(Integer.parseInt(memberIds[i]));
							}
						}
					}
				}
				if (!mnList.getUserId().equals(Integer.parseInt(userId))) {
					notUserIdSet.add(mnList.getUserId());
				}
			}
			
			writeLog(Integer.parseInt(userId), "A", "" + label + " "+ changedNoteNameWithDouble(mnList.getListName()) + " to " + newListName , mnList.getListType(),mnList.getListId().toString(), "", null, mnList.getListType());

			if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
				notUserIdSet.remove(Integer.parseInt(userId));
				writeLog(Integer.parseInt(userId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> " + label + " "+  changedNoteNameWithDouble(mnList.getListName()) + " to " + newListName , mnList.getListType(),mnList.getListId().toString(), "", notUserIdSet, mnList.getListType());
			}
		} catch (Exception e) {
			logger.error("Exception while moveNoteLog In Impl !", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" moveNoteLog method returened successfully " );
	}

	@Override
	public String insertNewNoteLog(MnLog log) {
		if(logger.isDebugEnabled())
			logger.debug("insertNewNoteLog method called in Impl :");
		Query query = null;
		Integer userId = 0;
		try {
			query = new Query();
			query.sort().on("_id", Order.ASCENDING);
			List<MnLog> logs = mongoOperations.find(query, MnLog.class,	JavaMessages.Mongo.MNLOG);
			if (logs != null && logs.size() != 0) {
				for (MnLog mnLog : logs) {
					if (userId <= mnLog.getLogId())
						userId = mnLog.getLogId() + 1;
				}
			} else {
				userId = 1000;
			}
			log.setLogId(userId);

			if(logger.isDebugEnabled())
				logger.debug(" insertNewNoteLog method returened successfully " );
			
			return "" + userId;
		} catch (Exception e) {
			logger.error(e);
			return "0";
		}
		

	}

	@Override
	public String insertMostViewed(MnVoteViewCount mnVoteViewCount,String userId) {
		Query query = null;
		Integer countId = 0;
		if(logger.isDebugEnabled())
			logger.debug("insertMostViewed method called in Impl :"+userId);
		try {
			query = new Query();
			query.sort().on("_id", Order.ASCENDING);
			List<MnVoteViewCount> viewCounts = mongoOperations.find(query,MnVoteViewCount.class, JavaMessages.Mongo.MNVOTEVIEWCOUNT);
			if (viewCounts != null && viewCounts.size() != 0) {
				for (MnVoteViewCount viewCount : viewCounts) {
					if (countId <= viewCount.getCountId())
						countId = viewCount.getCountId() + 1;
				}
			} else {
				countId = 1000;
			}
			mnVoteViewCount.setCountId(countId);

			mongoOperations.insert(mnVoteViewCount,JavaMessages.Mongo.MNVOTEVIEWCOUNT);

			MnUsers mnUsers = getUserDetails(Integer.parseInt(userId));

			if (mnUsers != null&& mnUsers.getUserRole().equals("Administrator")) {
				Query query2 = new Query(Criteria.where("listId").is(Integer.parseInt(mnVoteViewCount.getListId())));
				MnList mnList = mongoOperations.findOne(query2, MnList.class,JavaMessages.Mongo.MNLIST);
				String votes = "";
				for (String note : mnList.getMnNotesDetails()) {
					if (note.contains("\"noteId\":\""+ mnVoteViewCount.getNoteId() + "\"")) {
						JSONObject jsonObject;
						try {
							jsonObject = new JSONObject(note);
							votes = (String) jsonObject.get("vote");
						} catch (Exception e) {
							logger.error("Exception while updating most view checking votes detail :"	+ e);
						}
					}
				}
				
				if (votes.contains(userId))
				{
					Query query1 = new Query();
					query1.addCriteria(Criteria.where("status").is("A").and("listId").is(mnVoteViewCount.getListId()).and("noteId").is(mnVoteViewCount.getNoteId()));
					String adminVote = null;
					MnVoteViewCount viewCount = mongoOperations.findOne(query1, MnVoteViewCount.class, JavaMessages.Mongo.MNVOTEVIEWCOUNT);
					if (votes.contains(userId))
					{
						if (viewCounts != null) adminVote = viewCount.getAdminVote();
						if (adminVote != null && !adminVote.equals(""))
						{
							adminVote = "" + Integer.parseInt(adminVote) + 1;
						}
						else
						{
							adminVote = "1";
						}
						Update update2 = new Update();
						update2.set("adminVote", adminVote);
						mongoOperations.updateFirst(query1, update2, JavaMessages.Mongo.MNVOTEVIEWCOUNT);
						insertAdminVotes(userId, mnVoteViewCount.getListId(), mnVoteViewCount.getNoteId());
					}
				}
			
			}
			
			if(logger.isDebugEnabled())
				logger.debug(" insertMostViewed method returened successfully " );
			
			return ""+countId;
		}
		catch (Exception e)
		{
			logger.error("Error in insertMostViewedmethod :"+e.getMessage());
			return "0";
		}

	}

	@Override
	public List<MnNotesDetails> getMnMostViewed(String userId, String noteAccess) {
		List<MnVoteViewCount> viewCounts = null;
		List<MnList> mnListList = null;
		List<MnNotesDetails> details = new ArrayList<MnNotesDetails>();
		JSONObject jsonObject;
		String access;
		String status = "";
		Query query = null;
		String publicUserName;
		if(logger.isDebugEnabled())
			logger.debug("getMnMostViewed method called in Impl :"+userId);
		try {
			query = new Query();
			query.addCriteria(Criteria.where("status").is("A"));
			query.sort().on("viewed", Order.DESCENDING);
			viewCounts = mongoOperations.find(query, MnVoteViewCount.class,JavaMessages.Mongo.MNVOTEVIEWCOUNT);
			for (MnVoteViewCount mnVoteViewCount : viewCounts) {
				Query query1 = new Query(Criteria.where("listId").is(Integer.parseInt(mnVoteViewCount.getListId())).and("noteAccess").is(noteAccess).and("status").is("A"));
				mnListList = mongoOperations.find(query1, MnList.class,JavaMessages.Mongo.MNLIST);
				if (mnListList != null && !mnListList.isEmpty()) {
					for (MnList list : mnListList) {
						for (String str : list.getMnNotesDetails()) {
							jsonObject = new JSONObject(str);
							access = (String) jsonObject.get("access");
							status = (String) jsonObject.get("status");
							if (status.equals("A") && access != null && !access.isEmpty() && access.contains("public")) {
								if (mnVoteViewCount.getNoteId().equals((String) jsonObject.get("noteId"))) {
									MnNotesDetails mnNotesDetails = new MnNotesDetails();
									mnNotesDetails.setNoteName(changedNoteNameWithDouble((String) jsonObject.get("noteName")));
									mnNotesDetails.setStatus((String) jsonObject.get("status"));
									mnNotesDetails.setNoteId(Integer.parseInt((String) jsonObject.get("noteId")));
									mnNotesDetails.setAccess((String) jsonObject.get("access"));
									/// for public user change
									String publicUserId=(String) jsonObject.get("publicUser");
									boolean isUserId = publicUserId.matches("[0-9]*");
									if(isUserId){
									MnUsers mnUsers=getUserDetails(Integer.parseInt(publicUserId));
									publicUserName=mnUsers.getUserFirstName()+" "+mnUsers.getUserLastName();
									}else{
										publicUserName=(String) jsonObject.get("publicUser");
									}
									mnNotesDetails.setPublicUser(publicUserName);
									String jdate=(String)jsonObject.get("publicDate");
									String tdate=getTimeZone(jdate,userId);
									mnNotesDetails.setPublicDate(tdate);
									mnNotesDetails.setUserId(list.getUserId());
									mnNotesDetails.setListId(list.getListId().toString());
									mnNotesDetails.setViewed(mnVoteViewCount.getViewed());
									mnNotesDetails.setVote(mnVoteViewCount.getVote());
									mnNotesDetails.setCountId(mnVoteViewCount.getCountId());
									mnNotesDetails.setTagIds((String)jsonObject.get("privateTags"));
									details.add(mnNotesDetails);
								}
							}

						}
					}
				}
			}
			for (MnNotesDetails mnNotesDetails : details) {
				//System.out.println("viewed list=====>"+mnNotesDetails.getViewed());
			}
			
			if(logger.isDebugEnabled())
				logger.debug(" getMnMostViewed method returened successfully " );

		} catch (Exception e) {
			logger.error("Exception while fetching MostViewed List :" + e);

		}
		return details;
	}

	
	@Override
	public List<MnNotesDetails> getMnMostVoted(String userId, String noteAccess) {
		if(logger.isDebugEnabled())
		logger.debug("getMnMostVoted method called "+userId);
		List<MnVoteViewCount> viewCounts = null;
		List<MnList> mnListList = null;
		List<MnNotesDetails> details = new ArrayList<MnNotesDetails>();
		JSONObject jsonObject;
		String access;
		String publicUserName;
		String status = "";
		Query query = null;
		try {
			query = new Query();
			query.addCriteria(Criteria.where("status").is("A"));
			query.sort().on("vote", Order.DESCENDING);
			viewCounts = mongoOperations.find(query, MnVoteViewCount.class,JavaMessages.Mongo.MNVOTEVIEWCOUNT);
			for (MnVoteViewCount mnVoteViewCount : viewCounts) {
				Query query1 = new Query(Criteria.where("listId").is(Integer.parseInt(mnVoteViewCount.getListId())).and("noteAccess").is(noteAccess).and("status").is("A"));
				mnListList = mongoOperations.find(query1, MnList.class,JavaMessages.Mongo.MNLIST);
				if (mnListList != null && !mnListList.isEmpty()) {
					for (MnList list : mnListList) {
						for (String str : list.getMnNotesDetails()) {
							jsonObject = new JSONObject(str);
							access = (String) jsonObject.get("access");
							status = (String) jsonObject.get("status");
							
							
							if (status.equals("A") && access != null && !access.isEmpty() && access.contains("public")) {
								if (mnVoteViewCount.getNoteId().equals((String) jsonObject.get("noteId"))) {
									MnNotesDetails mnNotesDetails = new MnNotesDetails();
									mnNotesDetails.setNoteName(changedNoteNameWithDouble((String) jsonObject.get("noteName")));
									mnNotesDetails.setStatus((String) jsonObject.get("status"));
									mnNotesDetails.setNoteId(Integer.parseInt((String) jsonObject.get("noteId")));
									mnNotesDetails.setAccess((String) jsonObject.get("access"));
									/// for public user change
									String publicUserId=(String) jsonObject.get("publicUser");
									boolean isUserId = publicUserId.matches("[0-9]*");
									if(isUserId){
									MnUsers mnUsers=getUserDetails(Integer.parseInt(publicUserId));
									publicUserName=mnUsers.getUserFirstName()+" "+mnUsers.getUserLastName();
									}else{
										publicUserName=(String) jsonObject.get("publicUser");
									}
									mnNotesDetails.setPublicUser(publicUserName);
									String jdate=(String)jsonObject.get("publicDate");
									String tdate=getTimeZone(jdate,userId);
									mnNotesDetails.setPublicDate(tdate);
									//mnNotesDetails.setPublicDate((String) jsonObject.get("publicDate"));
									mnNotesDetails.setUserId(list.getUserId());
									mnNotesDetails.setListId(list.getListId().toString());
									mnNotesDetails.setViewed(mnVoteViewCount.getViewed());
									mnNotesDetails.setVote(mnVoteViewCount.getVote());
									mnNotesDetails.setTagIds((String)jsonObject.get("privateTags"));
									mnNotesDetails.setCountId(mnVoteViewCount.getCountId());

									details.add(mnNotesDetails);
								}
							}

						}
					}
				}
			}
			for (MnNotesDetails mnNotesDetails : details) {
				//System.out.println("vote list=====>"+mnNotesDetails.getVote());
			}

		} catch (Exception e) {
			logger.error("Exception in getMnMostVoted method:"+e);

		}
		if(logger.isDebugEnabled())
		logger.debug("getMnMostVoted method successfully returned");
		return details;
	}
	
	
	
	@Override
	public String updateMostViewed(MnVoteViewCount mnVoteViewCount,String userId) {
		
		if(logger.isDebugEnabled())
			logger.debug("updateMostViewed method called in Impl :"+userId);
		
		try {
			Query query = new Query(Criteria.where("noteId").is(mnVoteViewCount.getNoteId()).and("listId").is(mnVoteViewCount.getListId()));
			Update update = new Update();
			update.set("viewed", mnVoteViewCount.getViewed());
			update.set("vote", mnVoteViewCount.getVote());
			if(mnVoteViewCount.getStatus() != null){
				update.set("status", mnVoteViewCount.getStatus());
			}


			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNVOTEVIEWCOUNT);

			// for admin count of public votes
			MnUsers mnUsers = getUserDetails(Integer.parseInt(userId));
			if (mnUsers != null&& mnUsers.getUserRole().equals("Administrator")) {
				Query query2 = new Query(Criteria.where("listId").is(Integer.parseInt(mnVoteViewCount.getListId())));
				MnList mnList = mongoOperations.findOne(query2, MnList.class,JavaMessages.Mongo.MNLIST);
				String votes = "";
				for (String note : mnList.getMnNotesDetails()) {
					if (note.contains("\"noteId\":\""+ mnVoteViewCount.getNoteId() + "\"")) {
						JSONObject jsonObject;
						try {
							jsonObject = new JSONObject(note);
							votes = (String) jsonObject.get("vote");
						} catch (Exception e) {
							logger.error("Exception while updating most view checking votes detail :"+ e);
						}
					}
				}
				
				Query updQuery= new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("listId").is(mnVoteViewCount.getListId()).and("noteId").is(mnVoteViewCount.getNoteId()).and("status").is("A"));
				MnAdminVotes mnAdminVotes = mongoOperations.findOne(updQuery,MnAdminVotes.class,JavaMessages.Mongo.MNADMINVOTES);
				
				Query query1 = new Query();
				query1.addCriteria(Criteria.where("status").is("A").and("listId").is(mnVoteViewCount.getListId()).and("noteId").is(mnVoteViewCount.getNoteId()));
				query1.sort().on("viewed", Order.DESCENDING);
				String adminVote=null;
				MnVoteViewCount viewCounts = mongoOperations.findOne(query1, MnVoteViewCount.class, JavaMessages.Mongo.MNVOTEVIEWCOUNT);
				if(votes.contains(userId) && mnAdminVotes == null ){
					if(viewCounts!=null)
						adminVote=viewCounts.getAdminVote();
					if(adminVote!=null && !adminVote.equals("")){
						adminVote=""+Integer.parseInt(adminVote)+1;
					}else{
						adminVote="1";
					}
				
					insertAdminVotes(userId,mnVoteViewCount.getListId() , mnVoteViewCount.getNoteId());
				}else if(!votes.contains(userId) && mnAdminVotes != null){
					if(viewCounts!=null)
						adminVote=viewCounts.getAdminVote();
					if(adminVote!=null && !adminVote.equals("")){
						adminVote=""+(Integer.parseInt(adminVote)-1);
					}else{
						adminVote="";
					}
					updateAdminVoites(updQuery,userId, mnVoteViewCount.getListId(), mnVoteViewCount.getNoteId());
				}
				Update update2=new Update();
				update2.set("adminVote", adminVote);
				mongoOperations.updateFirst(query1, update2, JavaMessages.Mongo.MNVOTEVIEWCOUNT);
			}
			
			if(logger.isDebugEnabled())
				logger.debug(" updateMostViewed method returened successfully " );
			
			return "success";
		} catch (Exception e) {
			logger.error("Exception while updating viewed detail :" + e);
		}
		return "Exception while updating  viewed detail";
	}

	@Override
	public List<MnVoteViewCount> maxMnMostViewed(String noteId, String ListId) {
		Query query = null;
		List<MnVoteViewCount> viewCounts = null;
		if(logger.isDebugEnabled())
			logger.debug("maxMnMostViewed method called in Impl noteId:"+noteId);
		try {
			query = new Query(Criteria.where("listId").is(ListId).and("noteId").is(noteId).and("status").is("A"));
			query.sort().on("viewed", Order.DESCENDING);
			viewCounts = mongoOperations.find(query, MnVoteViewCount.class,JavaMessages.Mongo.MNVOTEVIEWCOUNT);

		} catch (Exception e) {
			logger.error(" Error in maxMnMostViewedmethods ::"+e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" maxMnMostViewed method returened successfully " );
		return viewCounts;

	}

	public void moveNoteLog(MnList mnList, String newListName,String oldListName, String label,String userId) {
		
		if(logger.isDebugEnabled())
			logger.debug("moveNoteLog method called in Impl :"+userId);
		
		try {
			MnUsers mnUsers = getUserDetailsObject(Integer.parseInt(userId));
			Set<Integer> notUserIdSet = new HashSet<Integer>();
			
			if(oldListName.equals( mnList.getListName())){
				oldListName = mnList.getListName();
			}
			writeLog(Integer.parseInt(userId), "A", "" + label + " "+ oldListName + " to " + newListName , mnList.getListType(),mnList.getListId().toString(), "", null, mnList.getListType());

			if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
				writeLog(Integer.parseInt(userId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B>" + label + " "+ oldListName + " to " + newListName , mnList.getListType(),mnList.getListId().toString(), "", notUserIdSet, mnList.getListType());
			}
			
			if(logger.isDebugEnabled())
				logger.debug(" moveNoteLog method returened successfully " );

		} catch (Exception e) {
			logger.error("Exception while move note log In Impl !", e);
		}
	}

	@Override
	public List<MnTag> getTags(String userId) {
		List<MnTag> tags = null;
		
		if(logger.isDebugEnabled())
			logger.debug("getTags method called in Impl :"+userId);
		
		try {
			Query query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("status").is("A"));
			tags = mongoOperations.find(query, MnTag.class,JavaMessages.Mongo.MNTAGS);

			if (tags != null && !tags.isEmpty()) {

			}

		} catch (Exception e) {
			logger.error("Exception while get tag list In Impl !", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" getTags method returened successfully " );
		return tags;
	}
    
	
	@Override
	public String createTag(MnTag mnTag) {
		if(logger.isDebugEnabled())
		logger.debug("createTag method called");
		Query query = null;
		Integer tagId = 0;
		boolean tagFlag = false;
		try {
			query=new Query();
			query.sort().on("_id", Order.ASCENDING);
			List<MnTag> listTag = mongoOperations.find(query,MnTag.class,JavaMessages.Mongo.MNTAGS);

			if (listTag != null && listTag.size() != 0) {
		
				MnTag tag = listTag.get(listTag.size() - 1);
				tagId = tag.getTagId() + 1;
			} else {
				tagId = 1;
		
			}

			query = new Query(Criteria.where("userId").is(
					(mnTag.getUserId())).and("status").is("A"));
			query.sort().on("_id", Order.ASCENDING);
			List<MnTag> listTags = mongoOperations.find(query, MnTag.class,JavaMessages.Mongo.MNTAGS);

			for (MnTag tag : listTags) {
				if (tag.getTagName().equalsIgnoreCase(mnTag.getTagName())) {
					tagFlag = true;
					break;
				}
			}

			if (!tagFlag) {
				mnTag.setTagId(tagId);
				mongoOperations.insert(mnTag, JavaMessages.Mongo.MNTAGS);

				if(logger.isDebugEnabled())
					logger.debug("createTag method successfully returned");
				return "" + tagId;
			} else {
				return "0";
			}

		} catch (Exception e) {
			logger.error("Exception in createTag method:"+e);
			return "0";
		}
		
	}
	
	@Override
	public String deleteTag(String userId, Integer tagId) {
		if(logger.isDebugEnabled())
		logger.debug("deleteTag method called"+userId+" tagId: "+tagId);
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		try {
			Query query = new Query(Criteria.where("tagId").is(tagId));
			MnTag Tags = mongoOperations.findOne(query, MnTag.class,JavaMessages.Mongo.MNTAGS);
			
			query = new Query(Criteria.where("tagId").is(tagId));
			
			Update tagUpdate = new Update();

			tagUpdate.set("status", "I");

			mongoOperations.updateFirst(query, tagUpdate,JavaMessages.Mongo.MNTAGS);
			
			query = new Query(Criteria.where("tagId").is(tagId).and("status").is("A"));
			
			List<MnTagDetails> mnTagDetails= mongoOperations.find(query, MnTagDetails.class,JavaMessages.Mongo.MNTAGDETAILS);
			if(mnTagDetails!=null){
				for(MnTagDetails tagDetails:mnTagDetails){
					Set<String> newMnNotesDetails=new TreeSet<String>();
					query = new Query(Criteria.where("listId").is(tagDetails.getListId()).and("status").is("A"));
				
					MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
					
					for(String str:mnList.getMnNotesDetails()){
						if (str.contains("\"noteId\":\""+ tagDetails.getNoteId() + "\"")&& str.contains("\"status\":\"A\"")){
							JSONObject jsonObject=new JSONObject(str);
							String tag=(String)jsonObject.get("tag");
							if(!tag.contains(",")){
								String temp=tag;
								temp=temp.replace("[", "");
								temp=temp.replace("]", "");
								temp=temp.trim();
								if(temp.equals(tagDetails.getTagId().toString()))
									str=str.replace("\"tag\":\""+tag+"\"", "\"tag\":\"\"");
							}else if(tag.trim().startsWith("["+tagDetails.getTagId().toString()+",")){
								String temp=tag.replace("["+tagDetails.getTagId().toString()+",", "[");
								str=str.replace("\"tag\":\""+tag+"\"", "\"tag\":\""+temp+"\"");
							}else if(tag.trim().endsWith(", "+tagDetails.getTagId().toString()+"]")){
								String oldTags=tag;
								String temp=oldTags.replace(", "+tagDetails.getTagId().toString()+"]", "]");
								str=str.replace("\"tag\":\""+tag+"\"", "\"tag\":\""+temp+"\"");
							}else{
								String temp=tag.replace("[", "");
								temp=temp.replace("]", "");
								String[] tagIds=temp.split(",");
								String newTagIds="[";
								for(int i=0;i<tagIds.length;i++){
									if(!tagIds[i].trim().equals(tagDetails.getTagId().toString()))
										newTagIds=newTagIds+tagIds[i]+",";	
								}
								newTagIds=newTagIds.substring(0, newTagIds.length()-1);
								newTagIds=newTagIds+"]";
								str=str.replace("\"tag\":\""+tag+"\"", "\"tag\":\""+newTagIds+"\"");
							}
							newMnNotesDetails.add(str);
						}else{
							newMnNotesDetails.add(str);
						}
					}
					Update noteTagUpdate = new Update();

					noteTagUpdate.set("mnNotesDetails", newMnNotesDetails);

					mongoOperations.updateFirst(query, noteTagUpdate,JavaMessages.Mongo.MNLIST);
				}
			}
			
			query = new Query(Criteria.where("tagId").is(tagId));

			Update tagDetailsUpdate = new Update();

			tagDetailsUpdate.set("status", "I");
			tagDetailsUpdate.set("endDate", sdf.format(date));

			mongoOperations.updateMulti(query, tagDetailsUpdate,JavaMessages.Mongo.MNTAGDETAILS);

			writeLog(Integer.parseInt(userId), "A", "Deleted tag:"+ Tags.getTagName(), "music", null, null, null, "music");
			return "" + tagId;
		} catch (Exception e) {
			logger.error("Exception in deleteTag method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("deleteTag method successfully returned");
		return "Exception while Deleting  Tag Detail";
	}

	@Override
	public String updateTag(String userId, String tagName, Integer tagId,String listId, String noteId) {
		if(logger.isDebugEnabled())
		logger.debug("updateTag method called "+userId+" tagId: "+tagId);
		String status="";
		try {
			boolean tagExist=false;
			Query querys=new  Query(Criteria.where("userId").is(
					Integer.parseInt(userId)).and("status").is("A"));
			List<MnTag> mnTag= mongoOperations.find(querys,MnTag.class,JavaMessages.Mongo.MNTAGS);
			for(MnTag tag:mnTag){
				if(tag.getTagName().equalsIgnoreCase(tagName.trim()) && !tag.getTagId().equals(tagId)){
					tagExist=true;
				}
			}
				if(!tagExist)
				{
					Query query = new Query(Criteria.where("tagId").is(tagId).and("userId").is(Integer.parseInt(userId)));

					Update tagUpdate = new Update();

					tagUpdate.set("tagName", tagName);

					mongoOperations.updateFirst(query, tagUpdate,JavaMessages.Mongo.MNTAGS);

					MnTag Tags = mongoOperations.findOne(query, MnTag.class,JavaMessages.Mongo.MNTAGS);

					Query query2 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
					MnList mnList = mongoOperations.findOne(query2, MnList.class,JavaMessages.Mongo.MNLIST);

					String tempNoteDetails = "";
					for (String note : mnList.getMnNotesDetails()) {
						if (note.contains("\"noteId\":\"" + noteId + "\"")) {
							tempNoteDetails = note;
						}
					}

					commomLog(tempNoteDetails, mnList, "Tag updated as <B>"+ Tags.getTagName() + "</B> on ", true, userId,false,false,true,"",false,"");
					status="200";
				}
				else
				{
					status="404";
				}
			

			return status;
		} catch (Exception e) {
			logger.error("Exception in updateTag method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("updateTag method successfully returned");
		return "Exception while Updating  Tag Detail";
	}

	@Override
	public String updateTagForNote(String tagId, String listId, String noteId,String userId, String listType) {
		if(logger.isDebugEnabled())
		logger.debug("updateTagForNote method called "+userId+" tagId: "+tagId);
		MnTagDetails details = new MnTagDetails();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Set<String> updateNoteDetails=new TreeSet<String>();
		List<Integer> str2 = null;
		String tempNoteDetails = null;
		String removeNoteDeatils = null;
		try {
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			
			String str = "";
			for (String note : mnList.getMnNotesDetails()) {
				
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					removeNoteDeatils = note;
					if (note.contains("\"tag\":\"\"")) {
						note = note.replace("\"tag\":\"\"", "\"tag\":\""
								+ tagId + "\"");
						str = "A";
					} else {
						List<Integer> tagList = notesTagUpdateConvertToJsonObject(note);
						if (tagList.contains(Integer.parseInt(tagId)))
							str = "R";
						else
							str = "A";

						str2 = notesTagRemoveUnWantedConvertToJsonObject(
								note, str, Integer.parseInt(tagId));
						if (str2 != null && !str2.isEmpty())
							note = note.replace(notesTagConvertToJsonObject(note),"\"tag\":\"" + str2.toString() + "\"");
						else
							note = note.replace(notesTagConvertToJsonObject(note),"\"tag\":\"\"");
					}
					tempNoteDetails = note;

				}
				updateNoteDetails.add(note);
			}
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			if (str.equals("A")) {
				details.setTagId(Integer.parseInt(tagId));
				details.setUserId(Integer.parseInt(userId));
				details.setListId(Integer.parseInt(listId));
				details.setNoteId(Integer.parseInt(noteId));
				details.setListType(listType);
				details.setTagedDate(sdf.format(date));
				details.setStatus("A");
				mongoOperations	.insert(details, JavaMessages.Mongo.MNTAGDETAILS);
			} else {
				query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("tagId").is(Integer.parseInt(tagId)).and("noteId").is(Integer.parseInt(noteId)).and("userId").is(Integer.parseInt(userId)));
				Update updates = new Update();
				updates.set("endDate", sdf.format(date));
				updates.set("status", "I");
				mongoOperations.updateFirst(query, updates,JavaMessages.Mongo.MNTAGDETAILS);
			}
			
			Query detailsQuery = new Query(Criteria.where("listId").is(mnList.getListId()).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
			MnNoteDetails mnNoteDetails =mongoOperations.findOne(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
			if(mnNoteDetails!= null ){
				Update update1 = new Update();
				if (removeNoteDeatils.contains("\"tag\":\"\"")) {
					mnNoteDetails.setTag(new ArrayList<Integer>());
					mnNoteDetails.getTag().add(Integer.parseInt(tagId));
				}else{
					if (str2 != null && !str2.isEmpty()){
						mnNoteDetails.setTag(str2);
					}else{
						mnNoteDetails.setTag(new ArrayList<Integer>());
					}
				}
				update1.set("tag", mnNoteDetails.getTag());
				mongoOperations.updateFirst(detailsQuery,update1,JavaMessages.Mongo.MNNOTEDETAILS);
			}
			
			Query query3 = new Query(Criteria.where("tagId").is(Integer.parseInt(tagId)));
			MnTag Tags = mongoOperations.findOne(query3, MnTag.class,JavaMessages.Mongo.MNTAGS);
			String tag ="";
			if(Tags.getTagName().length()>36){
				tag = Tags.getTagName().substring(0,35)+"...";
			}else{
				tag = Tags.getTagName();
			}
			
			JSONObject jsonObject = new JSONObject(tempNoteDetails);
			if (str.equals("A")) {
				commomLog(tempNoteDetails, mnList, "Tagged ", false, userId,false ,false,true,": <B>"+tag+"</B>",false ,"");
			}else{
				commomLog(tempNoteDetails, mnList, "Tag removed "+tag, false, userId,false ,false,false,": "+changedNoteNameWithDouble(jsonObject.getString("noteName")),false ,"");
			}
			return "{\"status\":\"" + str + "\"}";
		} catch (Exception e) {
			logger.error("Exception in updateTagForNote method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("updateTagForNote method successfully returned");
		return "Exception while Updating Tag For Note";
	}

	@Override
	public List<MnNotesDetails> getMostViewedDateSearch(String userId,String noteAccess, String date1) {
		if(logger.isDebugEnabled())
		logger.debug("getMostViewedDateSearch method called "+userId);
		List<MnVoteViewCount> viewCounts = null;
		List<MnList> mnListList = null;
		List<MnNotesDetails> details = new ArrayList<MnNotesDetails>();
		JSONObject jsonObject;
		String access;
		String startDate;
		String publicUserName;
		DateFormat df = new SimpleDateFormat("M/dd/yyyy");
		Query query = null;
		try {
			query = new Query();
			query.sort().on("viewed", Order.DESCENDING);
			viewCounts = mongoOperations.find(query, MnVoteViewCount.class,JavaMessages.Mongo.MNVOTEVIEWCOUNT);
			for (MnVoteViewCount mnVoteViewCount : viewCounts) {
				Query query1 = new Query(Criteria.where("listId").is(Integer.parseInt(mnVoteViewCount.getListId())).and("noteAccess").is(noteAccess).and("status").is("A"));
				mnListList = mongoOperations.find(query1, MnList.class,JavaMessages.Mongo.MNLIST);
				if (mnListList != null && !mnListList.isEmpty()) {
					for (MnList list : mnListList) {
						for (String str : list.getMnNotesDetails()) {
							jsonObject = new JSONObject(str);
							access = (String) jsonObject.get("access");
							startDate = (String) jsonObject.get("startDate");
							
							Date recentDate=new Date(startDate);
							String startDates=df.format(recentDate);
							if (access != null && !access.isEmpty()
									&& access.contains("public")) {
								if (mnVoteViewCount.getNoteId().equals(
										(String) jsonObject.get("noteId"))) {
									if (date1.equals(startDates)) {
										MnNotesDetails mnNotesDetails = new MnNotesDetails();
										mnNotesDetails.setNoteName(changedNoteNameWithDouble((String) jsonObject.get("noteName")));
										mnNotesDetails.setStatus((String) jsonObject.get("status"));
										mnNotesDetails.setNoteId(Integer.parseInt((String) jsonObject.get("noteId")));
										mnNotesDetails.setAccess((String) jsonObject.get("access"));
										/// for public user change
										String publicUserId=(String) jsonObject.get("publicUser");
										boolean isUserId = publicUserId.matches("[0-9]*");
										if(isUserId){
										MnUsers mnUsers=getUserDetails(Integer.parseInt(publicUserId));
										publicUserName=mnUsers.getUserFirstName()+" "+mnUsers.getUserLastName();
										}else{
											publicUserName=(String) jsonObject.get("publicUser");
										}
										
										mnNotesDetails.setPublicUser(publicUserName);
										mnNotesDetails.setPublicDate((String) jsonObject.get("publicDate"));
										mnNotesDetails.setUserId(list.getUserId());
										mnNotesDetails.setListId(list.getListId().toString());
										mnNotesDetails.setViewed(mnVoteViewCount.getViewed());
										mnNotesDetails.setVote(mnVoteViewCount.getVote());
										mnNotesDetails.setCountId(mnVoteViewCount.getCountId());

										details.add(mnNotesDetails);
									}
								}
							}

						}
					}
				}
			}

		} catch (Exception e) {
			logger.error("Exception in getMostViewedDateSearch method:"+e);

		}
		if(logger.isDebugEnabled())
		logger.debug("getMostViewedDateSearch method successfully returned");
		return details;
	}

	@Override
	public MnUsers getUserDetails(Integer userId) {
		if(logger.isDebugEnabled())
			logger.debug("getUserDetails method called "+userId);
		MnUsers memberList = null;
		Query query = null;
		try {
			query = new Query(Criteria.where("userId").is(userId));
			memberList = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			
		} catch (Exception e) {
			logger.error("Exception in getUserDetails method:"+e);

		}
		if(logger.isDebugEnabled())
		logger.debug("getUserDetails method successfully returned");
		return memberList;
	}
	
	@Override
	public void sendingMailToFollower(MnUsers mnUsers, String noteName) 
	{
		try
		{
			if(mnUsers!=null ){
			String userName = mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName();
			List<Integer> usersIds = new ArrayList<Integer>(mnUsers.getFollowers());
			List<MnUsers> list = getUsersDetails(usersIds);
			if (list != null && !list.isEmpty())
			{
				for (MnUsers users : list)
				{
					try{
					if(users.getNotificationFlag().equalsIgnoreCase("yes"))
					{
					if(getUserMailNotification(users.getUserId(),"crowd"))
					{
					SendMail sendMail=new SendMail(); 	
					String recipients[]={users.getEmailId()};
					String message=MailContent.sharingNotification+users.getUserFirstName()+" "+users.getUserLastName()+MailContent.sharingNotification1+userName+" published a note to the Crowd : "+noteName+ ""+MailContent.sharingNotification2;
					sendMail.postEmail(recipients, "New content in the Musicnote Crowd",message);
					}
					}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
		}

		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	@Override
	public List<MnUsers> getUsersDetails(List<Integer> userIds) {
		if(logger.isDebugEnabled())
		logger.debug("getUsersDetails method called");
		List<MnUsers> memberList = null;
		Query query = null;
		try {
			query = new Query(Criteria.where("userId").in(userIds).and("notificationFlag").is("yes"));
			memberList = mongoOperations.find(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);

		} catch (Exception e) {
			logger.error("Exception in getUsersDetails method:"+e);

		}
		if(logger.isDebugEnabled())
		logger.debug("getUsersDetails method successfully returned");
		return memberList;
	}

	public String notesTagConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("notesTagConvertToJsonObject method called");
		String tags = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String tag = (String) jsonObject.get("tag");
				tags = "\"tag\":\"" + tag + "\"";
			} catch (Exception e) {
				logger.error("Exception in notesTagConvertToJsonObject method:"+e);
			}
		}
		if(logger.isDebugEnabled())
		logger.debug("notesTagConvertToJsonObject method successfully returned");
		return tags;
	}

	@Override
	public String updateAllContactMembersForNote(String userId, String listId,String noteId, String noteType,String fullName, String firstName, String noteName, String noteLevel) {
		if(logger.isDebugEnabled())
		logger.debug("updateAllContactMembersForNote method called "+userId+" fullName: "+fullName);
		List<MnNotesSharingDetails> details = new ArrayList<MnNotesSharingDetails>();
		List<MnEventsSharingDetails> eventSharing = new ArrayList<MnEventsSharingDetails>();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		Query query = null;
		Set<Integer> notUserIdSet = new HashSet<Integer>();
		List<Integer> userIdList = new ArrayList<Integer>();
		Set<String> updateNoteDetails=new TreeSet<String>();
		String removeMembersBasedOnOldMembers="";
		Boolean mailNotify=false;
		try {
			Integer size = 0;
			query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);

			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			for (String note : mnList.getMnNotesDetails()) {
				if(mnList.getUserId() == Integer.parseInt(userId)){
					if (noteType.equalsIgnoreCase("Schedule")) {
						if (note.contains("\"eventId\":\"" + noteId + "\"")) {
							removeNoteDeatils = note;
							if (note.contains("\"eventSharedAllContact\":0")) {
								note = note.replace("\"eventSharedAllContact\":0","\"eventSharedAllContact\":1");
							}
							tempNoteDetails = note;
						}
					} else {
						if (note.contains("\"noteId\":\"" + noteId + "\"")) {
							removeNoteDeatils = note;
							if (note.contains("\"noteSharedAllContact\":0")) {
								note = note.replace("\"noteSharedAllContact\":0","\"noteSharedAllContact\":1");
							}
							tempNoteDetails = note;
						}
					}
				}else{
					if (noteType.equalsIgnoreCase("Schedule")) {
						if (note.contains("\"eventId\":\"" + noteId + "\"")) {
							removeNoteDeatils = note;
							if (note.contains("\"eventSharedAllContactMembers\":\"\"")) {
								note = note.replace("\"eventSharedAllContactMembers\":\"\"","\"eventSharedAllContactMembers\":\"" +"["+userId+"]"+ "\"");
							} else {
								removeMembersBasedOnOldMembers = note;
								String oldMemberIds = notesSharingAllContactMembersDetailsConvertToJsonObject(removeMembersBasedOnOldMembers); 
								if(oldMemberIds!=null && !oldMemberIds.isEmpty()){
									oldMemberIds=oldMemberIds+","+userId;
									oldMemberIds = "[" + oldMemberIds + "]";
									note = note.replace(eventMembersUpdateConvertToJsonObject(note),"\"eventSharedAllContactMembers\":\"" + oldMemberIds+ "\"");
								}else{
									note = note.replace(eventMembersUpdateConvertToJsonObject(note),"\"eventSharedAllContactMembers\":\"" +"["+userId+"]"+ "\"");
								}
								
							}
							tempNoteDetails = note;
						}
					} else {
						if (note.contains("\"noteId\":\"" + noteId + "\"")) {
							removeNoteDeatils = note;
							if (note.contains("\"noteSharedAllContactMembers\":\"\"")) {
								note = note.replace("\"noteSharedAllContactMembers\":\"\"","\"noteSharedAllContactMembers\":\"" +"["+userId+"]"+ "\"");
							} else {
								removeMembersBasedOnOldMembers = note;
								String oldMemberIds = notesSharingAllContactMembersDetailsConvertToJsonObject(removeMembersBasedOnOldMembers); 
								if(oldMemberIds!=null && !oldMemberIds.isEmpty()){
									oldMemberIds=oldMemberIds+","+userId;
									oldMemberIds = "[" + oldMemberIds + "]";
									note = note.replace(eventMembersUpdateConvertToJsonObject(note),"\"noteSharedAllContactMembers\":\"" + oldMemberIds+ "\"");
								}else{
									note = note.replace(eventMembersUpdateConvertToJsonObject(note),"\"noteSharedAllContactMembers\":\"" +"["+userId+"]"+ "\"");
								}
								
							}
							tempNoteDetails = note;
						}
					}
				}
				updateNoteDetails.add(note);
			}
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);

			//Sharing All Contact Details Base Update Note Deatils In Mn_NoteDetails Table Also - Venu
			if(!noteType.equalsIgnoreCase("Schedule")){
				query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
				MnNoteDetails noteDetails= mongoOperations.findOne(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
				if(noteDetails!=null){		
					update = new Update();
					update.set("noteSharedAllContact", true);
					mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNNOTEDETAILS);
				}
			}
			//End -Venu
			
			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
			
			MnUsers mnUsers = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);

			if (mnUsers.getFriends() != null && !mnUsers.getFriends().isEmpty()) {

				for (Integer id : mnUsers.getFriends()) {
					MnNotesSharingDetails sharingDetails = new MnNotesSharingDetails();
					sharingDetails.setUserId(Integer.parseInt(userId));
					try {
						notUserIdSet.add(id);
						userIdList.add(id);
					} catch (Exception e) {

					}
					sharingDetails.setSharingUserId(id);
					sharingDetails.setSharingGroupId(0);
					sharingDetails.setListId(Integer.parseInt(listId));
					sharingDetails.setListType(mnList.getListType());
					sharingDetails.setNoteId(Integer.parseInt(noteId));
					sharingDetails.setSharedDate(formatter.format(date));
					sharingDetails.setSharingLevel("note");
					sharingDetails.setNoteLevel(noteLevel);
					sharingDetails.setShareAllContactFlag(true);
					sharingDetails.setStatus("A");
					details.add(sharingDetails);
				
				}
				
				//////////////// mail send /////////////
				String message=null;
				String subject=null;
				List<MnUsers> list = getUsersDetails(userIdList);
				if (list != null && !list.isEmpty())
				{
					for (MnUsers users : list)
					{
						try{
							mailNotify=getUserMailNotification(users.getUserId(), mnList.getListType());
						if(users.getNotificationFlag().equalsIgnoreCase("yes"))	
						{
						if(mailNotify){	
						SendMail sendMail=new SendMail(); 	
						String recipients[]={users.getEmailId()};
						
						if(mnList.getListType().equalsIgnoreCase("schedule"))
						{
							MnEventDetails event=null;
								
								Query query1 = new Query(Criteria.where("eventId").is(Integer.parseInt(noteId)).and("listId").is(Integer.parseInt(listId)).and("status").is("A"));
								event = mongoOperations.findOne(query1, MnEventDetails.class, JavaMessages.Mongo.MNEVENTS);
								
								 if(!event.getAlldayevent())
									{
										
										String stDate =convertedZoneEvents(users.getUserId(), event.getUserId(), event.getStartDate());
										event.setStartDate(stDate);
										String endDate =convertedZoneEvents(users.getUserId(), event.getUserId(), event.getEndDate());
										event.setEndDate(endDate);
									}
								
								
								SimpleDateFormat dateFormat  = new SimpleDateFormat("MMM/dd/yyyy");
								Date start=new Date(event.getStartDate());
								Date end=new Date(event.getEndDate());
								
							message="<div style=\"margin:0;padding:0\" dir=\"ltr\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:98%\" border=\"0\"><tbody><tr><td style=\"font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;font-size:12px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"font-size:16px;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;background:#3b5998;color:#ffffff;font-weight:bold;vertical-align:baseline;letter-spacing:-0.03em;text-align:left;padding:10px 38px 4px\"><img src=https://www.musicnoteapp.com/musicnote/pics/logo.png>&nbsp;&nbsp;<a style=\"text-decoration:none\" href=\"https://www.musicnoteapp.com/musicnote/index.html\" target=\"_blank\"><span style=\"background:#3b5998;color:#ffffff;font-weight:bold;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;vertical-align:middle;font-size:16px;letter-spacing:-0.03em;text-align:left;vertical-align:baseline\">Musicnote</span></a></td></tr></tbody></table><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"border-right:1px solid #ccc;line-height:16px;font-size:11px;border-bottom:1px solid #ccc;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;border-top:1px solid #ccc;padding:10px 20px;border-left:1px solid #ccc\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 20px;line-height:16px;width:620px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\">Hi "+users.getUserFirstName()+",</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">"+fullName+" shared a new event with you on Musicnote</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\"> Event Name : "+event.getEventName()+"</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">Start Date : "+dateFormat.format(start)+"</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">End date : "+dateFormat.format(end)+"</td></tr><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\"></td></tr><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\"><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse:collapse;width:100%\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px;background-color:#f2f2f2;border-left:none;border-right:none;border-top:1px solid #ccc;border-bottom:1px solid #ccc\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding-right:10px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"border-width:1px;border-style:solid;border-color:#29447e #29447e #1a356e;background-color:#5b74a8\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:2px 6px 4px;border-top:1px solid #8a9cc2\"><a href=\"https://www.musicnoteapp.com/musicnote/index.html\" style=\"color:#3b5998;text-decoration:none\" target=\"_blank\"><span style=\"font-weight:bold;white-space:nowrap;color:#fff;font-size:13px\">Go To Musicnote</span></a></td></tr></tbody></table></td></tr></tbody></table></td><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\"></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"border-right:none;color:#999999;font-size:11px;border-bottom:none;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;border:none;border-top:none;border-left:none\">This message was sent to you from Musicnote .If you don\'t want to receive these emails from Musicnote in the future, please <a href=\"https://www.musicnoteapp.com/musicnote/unsubscribe.html\" style=\"color:#3b5998;text-decoration:none\" target=\"_blank\">unsubscribe.</a><br>Musicnote, Inc., 3240 E. State St. Ext., Hamilton NJ, 08619.</td></tr></tbody></table></div>";
							subject=firstName+" shared a new event with you";
						}
						else
						{
							message=MailContent.sharingNotification+users.getUserFirstName()+MailContent.sharingNotification1+fullName+"  shared a new note with you on Musicnote : "+ noteName+" "+MailContent.sharingNotification2;;
							subject=firstName+" shared a new note with you";
						}
						sendMail.postEmail(recipients, subject,message);
						}
						}
						}catch(Exception e){
							logger.error("Exception in updateAllContactMembersForNote method:"+e);
						}
					}
				}
				
				////////////////////////////////////////
				
			}

			if (!details.isEmpty()) {

				for (MnNotesSharingDetails det : details) {
					if (det.getListType().equalsIgnoreCase("schedule")) {
						MnEventsSharingDetails share = new MnEventsSharingDetails();
						share.setUserId(det.getUserId());
						share.setSharingUserId(det.getSharingUserId());
						share.setListId(det.getListId());
						share.setNoteId(det.getNoteId());
						share.setListType(det.getListType());
						share.setSharingGroupId(det.getSharingGroupId());
						share.setStatus(det.getStatus());
						share.setSharingLevel("note");
						share.setNoteLevel(noteLevel);
						share.setShareAllContactFlag(true);
						share.setSharingStatus("P");
						if (eventSharing != null)
							eventSharing.add(share);
					}

				}
				insertNotesSharingDetails(details, eventSharing);
				size = 1;
			}
			String addUser = "", addUserName = "";
			if (notUserIdSet != null) {
				for (Integer integer : notUserIdSet) {
					MnUsers mnUsers2 = getUserDetailsObject(integer);
					if (mnUsers2 != null) {
						addUser = addUser +  mnUsers2.getUserFirstName() +" "+mnUsers2.getUserLastName()
								+ ", ";
						addUserName = addUserName
						+ mnUsers2.getUserFirstName() +" "+mnUsers2.getUserLastName() + ",";
					}
				}
			}
			addUser = addUser.substring(0,addUser.lastIndexOf(","));
			if(addUser.length() > 36 ){
				addUser = "<B>"+addUser.substring(0,35)+"...</B>";
			}
			addUserName = addUserName.substring(0,addUserName.lastIndexOf(","));
			if(addUserName.length() > 36 ){
				addUserName = "<B>"+addUserName.substring(0,35)+"...</B>";
			}
			
			JSONObject jsonObject = new JSONObject(tempNoteDetails);

			if (noteType.equalsIgnoreCase("Schedule")) {
				writeLog(Integer.parseInt(userId), "A", "Shared a "+getListType(mnList.getListType())+":"+  changedNoteNameWithDouble((String)jsonObject.get("eventName")) + " with All Contacts(" + addUser+")", mnList.getListType(),listId, noteId, null, mnList.getListType());
				if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
					writeLog(Integer.parseInt(userId), "N", "<B>"	+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B>  Shared a "+getListType(mnList.getListType())+" with you:"+  changedNoteNameWithDouble((String)jsonObject.get("eventName")) ,mnList.getListType(), listId, noteId, notUserIdSet, mnList.getListType());
				}
			} else {
				writeLog(Integer.parseInt(userId), "A", "Shared a "+getListType(mnList.getListType())+":"+  changedNoteNameWithDouble((String)jsonObject.get("noteName")) +" with All Contacts(" + addUser+")", mnList.getListType(),listId, noteId, null, mnList.getListType());
				if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
					writeLog(Integer.parseInt(userId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> Shared a "+getListType(mnList.getListType())+" with you:"+  changedNoteNameWithDouble((String)jsonObject.get("noteName")),mnList.getListType(), listId, noteId, notUserIdSet, mnList.getListType());
				}
			}
			return "" + size;

		} catch (Exception e) {
			logger.error("Exception in updateAllContactMembersForNote method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("updateAllContactMembersForNote method successfully returned");
		return "Exception while All Contact Updating Members For Note";
	}

	@Override
	public String checkOwnerOfList(String listId, String noteId, String userId) {
		if(logger.isDebugEnabled())
		logger.debug("checkOwnerOfList method called "+userId+" noteId: "+noteId);
		String status = "{";
		Query query = null;
		Query query2 = null;
		MnList mnLists = null;
		MnUsers mnUsers = null;
		MnBlockedUsers mnBlockingUsers=null;
		String crowdFlaf="";
		
		MnPublicSharedNoteCopyDetails mnPublicShared=null;
		try {
			
			
			
			query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
			mnLists = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);

			if (mnLists != null) {
				if (mnLists.getUserId() == Integer.parseInt(userId.trim())) {
					status = status + "\"status\":\"success\",";
				} else {
					status = status + "\"status\":\"fail\",";
					if((mnLists.isShareAllContactFlag() ||
							(mnLists.getSharedIndividuals()!= null && !mnLists.getSharedIndividuals().isEmpty() && mnLists.getSharedIndividuals().indexOf(Integer.parseInt(userId.trim()))!= -1) ||
							mnLists.getSharedGroups()!= null && !mnLists.getSharedGroups().isEmpty() )){
						status = status + "\"shared\":\"shared\",";
					}else{
						status = status + "\"shared\":\"notshared\",";
					}
				}
				mnUsers = getUserDetailsObject(mnLists.getUserId());
				
				
				
				status = status + "\"userId\":\"" + mnLists.getUserId() + "\",\"userFirstName\":\"" + mnUsers.getUserFirstName()+ "\",\"userName\":\"" + mnUsers.getUserFirstName()+" "+mnUsers.getUserLastName() + "\"";
			}
			
			query = new Query(Criteria.where("listId").is(listId).and("noteId").is(noteId).and("userId").is(Integer.parseInt(userId)).and("status").is("A"));

			mnPublicShared = mongoOperations.findOne(query, MnPublicSharedNoteCopyDetails.class,JavaMessages.Mongo.MNPUBLICSHAREDNOTECOPY);
			
			if(mnPublicShared!=null){
				if(status.contains(","))
					status = status + ",\"publiCopy\":\"true\"";
				else
					status = status + "\"publiCopy\":\"true\"";
			}else{
				if(status.contains(","))
					status = status + ",\"publiCopy\":\"false\"";
				else
					status = status + "\"publiCopy\":\"false\"";
			}
			if(mnUsers!=null && !mnUsers.isPublicShareWarnMsgFlag()){
				if(status.contains(","))
					status = status + ",\"publiShareWarnMsg\":true";
				else
					status = status + "\"publiShareWarnMsg\":true";
			}else if(mnUsers!=null && mnUsers.isPublicShareWarnMsgFlag()){
				if(status.contains(","))
					status = status + ",\"publiShareWarnMsg\":false";
				else
					status = status + "\"publiShareWarnMsg\":false";
			}
			
			//crowd share
			query2 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("status").is("A"));
			mnBlockingUsers=mongoOperations.findOne(query2, MnBlockedUsers.class, JavaMessages.Mongo.MNBLOCKEDUSERS);
			if(mnBlockingUsers!=null)
			{
				crowdFlaf=mnBlockingUsers.getCrowdShareFlag();
			}
			if(mnBlockingUsers!=null&&crowdFlaf.equalsIgnoreCase("True"))
			{
				if(status.contains(","))
					status = status + ",\"crowdShareMsg\":true";
				else
					status = status + "\"crowdShareMsg\":true";
			}
			else 
			{
				if(status.contains(","))
					status = status + ",\"crowdShareMsg\":false";
				else
					status = status + "\"crowdShareMsg\":false";
			}
			
			status=status+" }";
		} catch (Exception e) {
			status = "{\"status\":\"fail\",\"userId\":\"" + mnLists.getUserId()+ "\",\"userName\":\"" + mnUsers.getUserFirstName()+mnUsers.getUserLastName() + "\"}";
			logger.error("Exception in checkOwnerOfList method:"+e);
		}
		if(logger.isDebugEnabled())
			logger.debug("checkOwnerOfList method successfully returned");
		return status;
	}



	
	@Override
	public List<MnNotesDetails> getMostVotedDateSearch(String userId, String noteAccess, String date1) {
		if(logger.isDebugEnabled())
		logger.debug("getMostVotedDateSearch method called "+userId);
		List<MnVoteViewCount> viewCounts = null;
		List<MnList> mnListList = null;
		List<MnNotesDetails> details = new ArrayList<MnNotesDetails>();
		JSONObject jsonObject;
		String access;
		String startDate;
		String publicUserName;
		DateFormat df = new SimpleDateFormat("M/dd/yyyy");
		Query query = null;
		try {
			query = new Query();
			query.sort().on("vote", Order.DESCENDING);
			viewCounts = mongoOperations.find(query, MnVoteViewCount.class,JavaMessages.Mongo.MNVOTEVIEWCOUNT);
			for (MnVoteViewCount mnVoteViewCount : viewCounts) {

				Query query1 = new Query(Criteria.where("listId").is(Integer.parseInt(mnVoteViewCount.getListId())).and("noteAccess").is(noteAccess).and("status").is("A"));
				mnListList = mongoOperations.find(query1, MnList.class,JavaMessages.Mongo.MNLIST);
				if (mnListList != null && !mnListList.isEmpty()) {
					for (MnList list : mnListList) {
						for (String str : list.getMnNotesDetails()) {
							jsonObject = new JSONObject(str);
							access = (String) jsonObject.get("access");
							 startDate = (String) jsonObject.get("startDate");
							Date recentDate = new Date(startDate);
							String startDates=df.format(recentDate);

							if (access != null && !access.isEmpty() && access.contains("public")) {
								if (mnVoteViewCount.getNoteId().equals((String) jsonObject.get("noteId"))) {
									if (date1.equals(startDates)) {
										MnNotesDetails mnNotesDetails = new MnNotesDetails();
										mnNotesDetails.setNoteName(changedNoteNameWithDouble((String) jsonObject.get("noteName")));
										mnNotesDetails.setStatus((String) jsonObject.get("status"));
										mnNotesDetails.setNoteId(Integer.parseInt((String) jsonObject.get("noteId")));
										mnNotesDetails.setAccess((String) jsonObject.get("access"));
										/// for public user change
										String publicUserId=(String) jsonObject.get("publicUser");
										boolean isUserId = publicUserId.matches("[0-9]*");
										if(isUserId){
										MnUsers mnUsers=getUserDetails(Integer.parseInt(publicUserId));
										publicUserName=mnUsers.getUserFirstName()+" "+mnUsers.getUserLastName();
										}else{
											publicUserName=(String) jsonObject.get("publicUser");
										}
										mnNotesDetails.setPublicUser(publicUserName);
										mnNotesDetails.setPublicDate((String) jsonObject.get("publicDate"));
										mnNotesDetails.setUserId(list.getUserId());
										mnNotesDetails.setListId(list.getListId().toString());
										mnNotesDetails.setViewed(mnVoteViewCount.getViewed());
										mnNotesDetails.setVote(mnVoteViewCount.getVote());
										mnNotesDetails.setCountId(mnVoteViewCount.getCountId());

										details.add(mnNotesDetails);
									}
								}
							}

						}
					}
				}
			}

		} catch (Exception e) {
			logger.error("Exception in getMostVotedDateSearch method:"+e);

		}
		if(logger.isDebugEnabled())
		logger.debug("getMostVotedDateSearch method successfully returned");
		return details;
	}
	@Override
	public List<MnNoteDetails> getListBasedOnListId(String userId, String listId,String listType) {
		if(logger.isDebugEnabled())
		logger.debug("getListBasedOnListId method called "+userId);
		List<MnNoteDetails> mnNoteDetails = null;
		Query query = null;
		List<MnNotesSharingDetails> mnSharedList = null;
		List<MnNoteDetails> mnNoteDetailsObjAssociated = new ArrayList<MnNoteDetails>();
		List<Integer> sharedListIds=new ArrayList<Integer>();
		try {
			
			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("listId").is(Integer.parseInt(listId)));
			mnNoteDetails = mongoOperations.find(query, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
			
			

			mnSharedList=getIndividualCopyToOwnBook(userId,listType ,listId);
			
			// get shared from associate note
			
			List<MnNotesSharingDetails> mnSharedList1=getSharedAssociateNote(userId,listType ,listId);
			
			if (mnSharedList != null && !mnSharedList.isEmpty())
			{
				for (MnNotesSharingDetails list : mnSharedList)
				{
					if(!sharedListIds.contains(list.getListId()))
						sharedListIds.add(list.getListId());
				}
			}
			if (sharedListIds != null && !sharedListIds.isEmpty())
			{
				List<MnList> listss = getLists(sharedListIds);
				if (listss != null && !listss.isEmpty())
				{
					for (MnList mnList : listss)
					{
						List<Integer> noteIds = getNoteIds(mnList.getListId(), userId, listType,listId);
						if(noteIds!= null && !noteIds.isEmpty()){
							Query detailsQuery = new Query(Criteria.where("listId").is(mnList.getListId()).and("noteId").in(noteIds));
							List<MnNoteDetails> mnNoteDetailsObj =mongoOperations.find(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
							if (mnNoteDetailsObj != null && !mnNoteDetailsObj.isEmpty()){
								mnNoteDetailsObjAssociated.addAll(mnNoteDetailsObj);
							}
						}
							
					}
					if (mnNoteDetailsObjAssociated != null && !mnNoteDetailsObjAssociated.isEmpty())
					{
						if(mnNoteDetails!=null && !mnNoteDetails.isEmpty() ){
							mnNoteDetails.addAll(mnNoteDetailsObjAssociated);
						}
						else{
							mnNoteDetails = mnNoteDetailsObjAssociated;
						}
					}
				}
			}
			// ram for associate book.
			if(mnSharedList1!=null)
			{
				for(MnNotesSharingDetails details:mnSharedList1)
				{
					MnNoteDetails mnNoteDetailsObj1=null; 
					Query detailsQuery1 = new Query(Criteria.where("listId").is(details.getListId()).and("noteId").in(details.getNoteId()));
					mnNoteDetailsObj1=mongoOperations.findOne(detailsQuery1, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
					if(mnNoteDetailsObj1!=null)
					mnNoteDetails.add(mnNoteDetailsObj1);
				}
			}
			

		} catch (Exception e) {
			logger.error("Exception in getListBasedOnListId method:"+e);

		}
		if(logger.isDebugEnabled())
		logger.debug("getListBasedOnListId method successfully returned");
		return mnNoteDetails;
	}

	@Override
	public List<MnNoteDetails> fetchSharedNotesBasedTag(String userId,String listType, String tagId, String multiFilterBaseNoteName, String multiFilterBaseListId) {
		//List<MnList> mnListList = null;
		List<MnNoteDetails> mnNoteDetails  = null;
		List<MnNotesSharingDetails> mnSharedList = null;
		List<MnTagDetails> details = new ArrayList<MnTagDetails>();
		Query query = null;
		try {
			List<Integer> listIds = new ArrayList<Integer>();
			if(multiFilterBaseListId!=null && !multiFilterBaseListId.isEmpty()){
				Query query1 = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("sharingLevel").is("individual").and("sharedBookId").is(Integer.parseInt(multiFilterBaseListId)).and("status").is("A"));
				mnSharedList = mongoOperations.find(query1, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				listIds.add(Integer.parseInt(multiFilterBaseListId));
				
				if (mnSharedList != null && !mnSharedList.isEmpty())
				{
					for (MnNotesSharingDetails list : mnSharedList)
					{
						query = new Query(Criteria.where("tagId").is(Integer.parseInt(tagId)).and("userId").is(Integer.parseInt(userId)).and("listId").is(list.getListId()).and("noteId").is(list.getNoteId()).and("listType").is(listType).and("status").is("A"));
						details.addAll(mongoOperations.find(query, MnTagDetails.class,JavaMessages.Mongo.MNTAGDETAILS));
						if(details!=null && !details.isEmpty() && !listIds.contains(list.getListId()))
							listIds.add(list.getListId());
					}
				}
				query = new Query(Criteria.where("tagId").is(Integer.parseInt(tagId)).and("userId").is(Integer.parseInt(userId)).and("listId").in(listIds).and("listType").is(listType).and("status").is("A"));
			}else
				query = new Query(Criteria.where("tagId").is(Integer.parseInt(tagId)).and("userId").is(Integer.parseInt(userId)).and("listType").is(listType).and("status").is("A"));
			
			query.sort().on("listId", Order.ASCENDING);
			details.addAll(mongoOperations.find(query, MnTagDetails.class,JavaMessages.Mongo.MNTAGDETAILS));
			
			
			if (details != null && !details.isEmpty()) {

				listIds = new ArrayList<Integer>();
				for (MnTagDetails tagDetails : details) {

					if (!listIds.contains(tagDetails.getListId()))
						listIds.add(tagDetails.getListId());

				}
				
				List<MnNoteDetails> removedUnwantedNotesList = new ArrayList<MnNoteDetails>();
				if (listIds != null && !listIds.isEmpty()) {
					for (Integer ids : listIds) {
						List<Integer> noteIds = new ArrayList<Integer>();
						for (MnTagDetails tagDetails : details) {
							if (tagDetails.getListId()== ids) {
								if (!noteIds.contains(tagDetails.getNoteId()))
									noteIds.add(tagDetails.getNoteId());
							}
						}
						
						List<MnNoteDetails> mnNoteDetList = getListAndNotes(ids,noteIds);
						if(mnNoteDetList!= null && !mnNoteDetList.isEmpty()){
							removedUnwantedNotesList.addAll(mnNoteDetList);
						}
					}
					return removedUnwantedNotesList;
				}

			}

		} catch (Exception e) {

		}
		return mnNoteDetails;
	}
	public List<MnNoteDetails> getListAndNotes(Integer listId, List<Integer> noteIds){
		List<MnNoteDetails> list = null;
		try{
			Query query2 = new Query(Criteria.where("listId").is(listId).and("noteId").in(noteIds));
			list = mongoOperations.find(query2, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
		}catch (Exception e) {
		}
		return list; 
	}
	public String addRemainder(MnRemainders mnRemainders, String listId,String noteId, Integer userId) {
		int rId = 0;
		String status=""; 
		Set<String> updateNoteDetails=new TreeSet<String>();
		MnList list = null;
		try {
			List<MnRemainders> mnRemaindersList = mongoOperations.findAll(	MnRemainders.class, JavaMessages.Mongo.MNREMAINDERS);
			if (mnRemaindersList != null && mnRemaindersList.size() != 0) {
				rId = mnRemaindersList.size() + 1;
			} else {
				rId = 1;
			}
			mnRemainders.setrId(rId);
			mongoOperations.insert(mnRemainders,JavaMessages.Mongo.MNREMAINDERS);

			// adding comment id in notes
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			String oldRemainders = "";
			String newRemainders = "";
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					list = mnList;
					removeNoteDeatils = note;
					JSONObject jsonObject;
					try {
						jsonObject = new JSONObject(note);
						oldRemainders = (String) jsonObject.get("remainders");

						if (oldRemainders != null && !oldRemainders.isEmpty()) {
							newRemainders = oldRemainders;

							if (newRemainders.lastIndexOf(']') != -1) {
								newRemainders = newRemainders.substring(0,newRemainders.lastIndexOf(']'));
							}

							newRemainders = newRemainders + "," + rId + "]";
						} else {
							newRemainders = "[" + rId + "]";
						}

						if (note.contains("\"remainders\":\"\"")) {
							note = note.replace("\"remainders\":\"\"","\"remainders\":\"" + newRemainders + "\"");
						} else {
							note = note.replace("\"remainders\":\""	+ oldRemainders + "\"", "\"remainders\":\""+ newRemainders + "\"");
						}
						tempNoteDetails = note;
					} catch (Exception e) {
					}
				}
				
				updateNoteDetails.add(note);
			}
			
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				list.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				list.getMnNotesDetails().add(tempNoteDetails);

			
			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			
			Query detailsQuery = new Query(Criteria.where("listId").is(mnList.getListId()).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
			MnNoteDetails mnNoteDetails =mongoOperations.findOne(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
			if(mnNoteDetails!= null ){
				Update update1 = new Update();
				if(mnNoteDetails.getRemainders()!= null && !mnNoteDetails.getRemainders().isEmpty()){
					mnNoteDetails.getRemainders().add(rId);
				}else{
					List<Integer> rmdrList = new ArrayList<Integer>();
					rmdrList.add(rId);
					mnNoteDetails.setRemainders(rmdrList);
				}
				update1.set("remainders",mnNoteDetails.getRemainders());
				
				mongoOperations.updateFirst(detailsQuery,update1,JavaMessages.Mongo.MNNOTEDETAILS);
			}
			
			Date date1 = format.parse(mnRemainders.getEventDate());
			
			List<Integer> sharedUserListForMail = getNoteSharedMemberListByIndGrpAll(tempNoteDetails,mnRemainders.getUserId().toString(),mnList);
			if(sharedUserListForMail!= null && !sharedUserListForMail.isEmpty()){
				status = ",\"sharedUserListForMail\":\""+sharedUserListForMail.toString()+"\"";
			}else{
				status = ",\"sharedUserListForMail\":\"\"";
			}
			status = "{\"rId\":\""+rId+"\""+status+"}";
			
			SimpleDateFormat sdf1 = new SimpleDateFormat("H:mm");
		        String time =mnRemainders.getEventTime();
				Date dateObj = sdf1.parse(time);
		        String time1=new SimpleDateFormat("h:mm a").format(dateObj);
			
			
			commomLog(tempNoteDetails, mnList, "Set reminder on", true,mnRemainders.getUserId().toString(),false,false,true," for <B>"+dateFormat.format(date1)+" at "+time1+"</B>  :<B>"+mnRemainders.getrName()+"</B>",false,"");

		} catch (Exception e) {
		}
		return "" + status;
	}
	SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
	SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy");
	
	public String deleteRemainder(String rId, String listId, String noteId) {
		Set<String> updateNoteDetails=new TreeSet<String>();
		try {

			Query query = new Query(Criteria.where("rId").is(Integer.parseInt(rId)));

			Update update = new Update();
			update.set("status", "I");
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNREMAINDERS);
			MnRemainders mnRemainders = mongoOperations.findOne(query,MnRemainders.class, JavaMessages.Mongo.MNREMAINDERS);

			Query query2 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query2, MnList.class,JavaMessages.Mongo.MNLIST);
			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			String oldRemainders = "";
			String newRemainders = "";
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					removeNoteDeatils = note;
					JSONObject jsonObject;
					try {
						jsonObject = new JSONObject(note);
						oldRemainders = (String) jsonObject.get("remainders");

						if (oldRemainders != null && !oldRemainders.isEmpty()) {
							newRemainders = oldRemainders;
							newRemainders = newRemainders.replace(rId + ",", "");
							newRemainders = newRemainders.replace("," + rId, "");
							newRemainders = newRemainders.replace(rId, "");
							if (newRemainders.length() <= 2) {
								newRemainders = "";
							}
						} else {
							newRemainders = "";
						}

						if (note.contains("\"remainders\":\"\"")) {
							note = note.replace("\"remainders\":\"\"","\"remainders\":\"" + newRemainders + "\"");
						} else {
							note = note.replace("\"remainders\":\""+ oldRemainders + "\"", "\"remainders\":\""+ newRemainders + "\"");
						}
						tempNoteDetails = note;
					} catch (Exception e) {
						logger.error("Exception while delete Reminders file In Impl !",e);
					}
				}
				updateNoteDetails.add(note);
			}
			
			if (removeNoteDeatils != null&& !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update2 = new Update();
			update2.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query2, update2,JavaMessages.Mongo.MNLIST);
			
			Query detailsQuery = new Query(Criteria.where("listId").is(mnList.getListId()).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
			MnNoteDetails mnNoteDetails =mongoOperations.findOne(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
			if(mnNoteDetails!= null ){
				Update update1 = new Update();
				if(mnNoteDetails.getRemainders()!= null && !mnNoteDetails.getRemainders().isEmpty()){
					mnNoteDetails.setRemainders(null);
				}
				update1.set("remainders",mnNoteDetails.getRemainders());
				
				mongoOperations.updateFirst(detailsQuery,update1,JavaMessages.Mongo.MNNOTEDETAILS);
			}
			commomLog(tempNoteDetails, mnList, "Delete reminders on ",false, mnRemainders.getUserId().toString(),false,false,true,": <B>"+mnRemainders.getrName()+"</B>",true,"");
			
		} catch (Exception e) {
			logger.error("Exception while delete remainder In Impl !", e);
		}
		return rId;
	}

	public List<MnRemainders> getRemaindersList(List<Integer> paramsList) {
		List<MnRemainders> mnRemaindersList = null;
		Query query = null;
		try {
			query = new Query(Criteria.where("rId").in(paramsList).and("status").in("A","I"));

			mnRemaindersList = mongoOperations.find(query, MnRemainders.class,JavaMessages.Mongo.MNREMAINDERS);
			if (mnRemaindersList != null && !mnRemaindersList.isEmpty()) {
				Collections.sort(mnRemaindersList, SENIORITY_ORDER_REMAIND);
			}

		} catch (Exception e) {
			logger.error("Exception while get note commnets List In Impl !", e);
		}
		return mnRemaindersList;
	}

	static final Comparator<MnRemainders> SENIORITY_ORDER_REMAIND = new Comparator<MnRemainders>() {
		public int compare(MnRemainders e1, MnRemainders e2) {
			try {
				Integer i1 = e1.getrId();
				Integer i2 = e2.getrId();
				return i1.compareTo(i2);
			} catch (Exception e) {
				return e1.getrId().compareTo(e2.getrId());
			}
		}
	};

	public String updateRemainders(String rId, String rNmae, String eventDate,String eventTime, boolean sendMail,String editDate, String listId, String noteId,String userId) {
		String status="";
		try {
			Query query = new Query(Criteria.where("rId").is(Integer.parseInt(rId)));
		
			Update update = new Update();
			update.set("rName", rNmae);
			update.set("editDate", editDate);
			update.set("eventDate", eventDate);
			update.set("eventTime", eventTime);
			update.set("sendMail", sendMail);
			update.set("edited", 1);
			update.set("status", "A");
			update.set("inactiveRemainderUserId", new ArrayList<Integer>());
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNREMAINDERS);
	
			MnRemainders remainders = mongoOperations.findOne(query,MnRemainders.class, JavaMessages.Mongo.MNREMAINDERS);

			Query query2 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
			MnList mnList = mongoOperations.findOne(query2, MnList.class,JavaMessages.Mongo.MNLIST);

			String tempNoteDetails = "";
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					tempNoteDetails = note;
				}
			}
			List<Integer> sharedUserListForMail = getNoteSharedMemberListByIndGrpAll(tempNoteDetails,userId,mnList);
			if(sharedUserListForMail!= null && !sharedUserListForMail.isEmpty()){
				status = ",\"sharedUserListForMail\":\""+sharedUserListForMail.toString()+"\"";
			}else{
				status = ",\"sharedUserListForMail\":\"\"";
			}
			status = "{\"rId\":\""+rId+"\""+status+"}";
			
			commomLog(tempNoteDetails, mnList, "Updated reminder: <B>" + rNmae+ "</B> details on ", true, userId,false,false,true,"",true,"");

		} catch (Exception e) {
			logger.error("Exception while uppdate commnets List In Impl !", e);
		}
		return status;
	}

	@Override
	public List<MnList> fetchNotesBasedDueDate(String userId, String listType,
			String dueDate) {
		List<MnList> mnListList = null;
		List<MnList> mnListList1 = null;
		List<MnList> mnListList2 = null;
		List<MnNoteDueDateDetails> details = null;
		MnNoteDueDateDetails dateDetails=null;
		List<MnList> removedUnwantedNotesList1 = new ArrayList<MnList>();
		List<MnList> removedUnwantedNotesList2 = new ArrayList<MnList>();
		
		Query query = null;
		try {
			if(logger.isDebugEnabled()){logger.debug("fetchNotesBasedDueDate method called "+userId+" dueDate: "+dueDate);}
			query = new Query(Criteria.where("dueDate").is(dueDate).and("userId").is(Integer.parseInt(userId)).and("listType").is(listType).and("status").is("A"));
			query.sort().on("listId", Order.ASCENDING);
			details = mongoOperations.find(query, MnNoteDueDateDetails.class,JavaMessages.Mongo.MNNOTEDUEDATEDETAILS);
			
			if (details != null && !details.isEmpty()) {

				List<Integer> listIds = new ArrayList<Integer>();

				for (MnNoteDueDateDetails dueDetails : details) {

					if (!listIds.contains(dueDetails.getListId()))
						listIds.add(dueDetails.getListId());

				}
				query = new Query(Criteria.where("listId").in(listIds));

				mnListList = mongoOperations.find(query, MnList.class,JavaMessages.Mongo.MNLIST);
				List<MnList> removedUnwantedNotesList = new ArrayList<MnList>();
				if (mnListList != null && !mnListList.isEmpty()) {
					for (Integer ids : listIds) {
						List<String> noteIds = new ArrayList<String>();
						for (MnNoteDueDateDetails dueDetails : details) {
							if (dueDetails.getListId()== ids) {

								if (!noteIds.contains(dueDetails.getNoteId().toString()))
									noteIds.add(dueDetails.getNoteId().toString());
							}
						}
						for (MnList list : mnListList) {
							if (list.getListId()== ids) {
								Set<String> orginalNotes = list.getMnNotesDetails();
								Set<String> sharedNotes = new HashSet<String>();
								for (String str : orginalNotes) {
									for (String noteStr : noteIds) {
										if (str.contains("\"noteId\":\""+ noteStr + "\"")&& str.contains("\"status\":\"A\""))
											sharedNotes.add(str);
									}
								}
								if (!sharedNotes.isEmpty()) {
									list.setMnNotesDetails(sharedNotes);
									removedUnwantedNotesList.add(list);
								}
							}
						}
					}
				}
			}
			
			
			//// note level
			Query query1=new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("sharingLevel").is("note").and("status").is("A"));
			List<MnNotesSharingDetails> mnNotesSharingDetails=mongoOperations.find(query1,MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			List<Integer> newlistIds = new ArrayList<Integer>();
			logger.info("mnNotesSharingDetails " + mnNotesSharingDetails.toString());
			for(MnNotesSharingDetails noteshare:mnNotesSharingDetails)
			{
				Query query2 = new Query(Criteria.where("dueDate").is(dueDate).and("listId").is(noteshare.getListId()).and("noteId").is(noteshare.getNoteId()).and("listType").is(noteshare.getListType()).and("status").is("A"));
				dateDetails = mongoOperations.findOne(query2, MnNoteDueDateDetails.class,JavaMessages.Mongo.MNNOTEDUEDATEDETAILS);
				if(dateDetails!=null)
				newlistIds.add(dateDetails.getListId());
				
			}
			logger.info("newlistIds " + newlistIds.toString());
			
			query = new Query(Criteria.where("listId").in(newlistIds));
			
			mnListList1 = mongoOperations.find(query, MnList.class,JavaMessages.Mongo.MNLIST);
			//List<MnList> removedUnwantedNotesList = new ArrayList<MnList>();
			if (mnListList1 != null && !mnListList1.isEmpty()) {
				for (Integer ids : newlistIds) {
					List<String> noteIds = new ArrayList<String>();
					for (MnNotesSharingDetails noteshare : mnNotesSharingDetails) {
						if (noteshare.getListId()== ids) {
								noteIds.add(noteshare.getNoteId().toString());
								logger.info("noteIds " + noteIds.toString());
						}
					}
					for (MnList list : mnListList1) {
						if (list.getListId()== ids) {
							Set<String> orginalNotes = list.getMnNotesDetails();
							Set<String> sharedNotes = new HashSet<String>();
							for (String str : orginalNotes) {
								for (String noteStr : noteIds) {
									if (str.contains("\"noteId\":\""+ noteStr + "\"") && str.contains("\"dueDate\":\""+dueDate+"\"")&& str.contains("\"status\":\"A\""))
										sharedNotes.add(str);
									logger.info("sharedNotes " + sharedNotes.toString());
								}
							}
							if (!sharedNotes.isEmpty()) {
								list.setMnNotesDetails(sharedNotes);
								removedUnwantedNotesList2.add(list);
							}
						}
					}
				}
			}
			
			////// book level//////
			Query query2=new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("sharingLevel").is("book").and("status").is("A"));
			List<MnNotesSharingDetails> mnNotesSharingDetails1=mongoOperations.find(query2,MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			List<Integer> newlistIds1 = new ArrayList<Integer>();
			for(MnNotesSharingDetails noteshare:mnNotesSharingDetails1)
			{
				newlistIds1.add(noteshare.getListId());
			}
			
			query = new Query(Criteria.where("listId").in(newlistIds1));
			
			mnListList2 = mongoOperations.find(query, MnList.class,JavaMessages.Mongo.MNLIST);
			
			for (MnList list : mnListList2) 
					{
						Set<String> orginalNotes = list.getMnNotesDetails();
						Set<String> sharedNotes = new HashSet<String>();
						for (String str : orginalNotes) 
						{
							if (str.contains("\"dueDate\":\""+dueDate+"\"") && str.contains("\"status\":\"A\""))
								sharedNotes.add(str);
							}
						
							if (!sharedNotes.isEmpty()) {
								list.setMnNotesDetails(sharedNotes);
								removedUnwantedNotesList1.add(list);
							}
						}
			////////book level/////
			if(logger.isDebugEnabled()){logger.debug("Fetch Notes based on due date"+userId);}

		} catch (Exception e) {
			logger.error("Exception while Fetching Notes DueDate Based On List :"+ e);

		}
		if(mnListList!=null)
		{
			mnListList.addAll(removedUnwantedNotesList2);
			mnListList.addAll(removedUnwantedNotesList1);
		}
		else
		{
			mnListList=new ArrayList<MnList>();
			mnListList.addAll(removedUnwantedNotesList2);
			mnListList.addAll(removedUnwantedNotesList1);
		}
		
		logger.info("mnListList final " + mnListList.toString());
		return mnListList;
	}

	

	@Override
	public String updateAllContactMembersForBook(String userId, String listId,String noteType,String fullName, String firstName, String bookName, String noteLevel) {
		List<MnNotesSharingDetails> details = new ArrayList<MnNotesSharingDetails>();
		List<MnEventsSharingDetails> eventSharing = new ArrayList<MnEventsSharingDetails>();
		List<Integer> userIds=new ArrayList<Integer>();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Query query = null;
		Boolean mailNotify=false;
		try {
			if(logger.isDebugEnabled()){logger.debug("updateAllContactMembersForBook method called "+userId +" fullName: "+fullName);}
			Integer size = 0;
			query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);

			if (!mnList.isShareAllContactFlag()) {
				Update update = new Update();
				update.set("shareAllContactFlag", true);
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			}

			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));

			MnUsers mnUsers = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);

			if (mnUsers.getFriends() != null && !mnUsers.getFriends().isEmpty()) {

				for (Integer id : mnUsers.getFriends()) {
					MnNotesSharingDetails sharingDetails = new MnNotesSharingDetails();
					sharingDetails.setUserId(Integer.parseInt(userId));
					sharingDetails.setSharingUserId(id);
					userIds.add(id);
					sharingDetails.setSharingGroupId(0);
					sharingDetails.setListId(Integer.parseInt(listId));
					sharingDetails.setListType(mnList.getListType());
					sharingDetails.setNoteId(0);
					sharingDetails.setSharedDate(formatter.format(date));
					sharingDetails.setSharingLevel("book");
					sharingDetails.setNoteLevel(noteLevel);
					sharingDetails.setShareAllContactFlag(true);
					sharingDetails.setStatus("A");
					details.add(sharingDetails);
				}
				
				//////////////mail content /////////////////
				List<MnUsers> list = getUsersDetails(userIds);
				if (list != null && !list.isEmpty())
				{
					for (MnUsers users : list)
					{
						try{
							mailNotify=getUserMailNotification(users.getUserId(), mnList.getListType());
						if(users.getNotificationFlag().equalsIgnoreCase("yes"))	
						{
						if(mailNotify){	
						SendMail sendMail=new SendMail(); 	
						String recipients[]={users.getEmailId()};
							
						String message=MailContent.sharingNotification+users.getUserFirstName()+MailContent.sharingNotification1+fullName+"  shared a new Notebook with you on Musicnote : "+ bookName+".."+MailContent.sharingNotification2;;
						String subject=firstName+" shared a new Notebook with you";
						
						sendMail.postEmail(recipients, subject,message);
						}
						}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				/////////////////////////
			}

			if (!details.isEmpty()) {

				for (MnNotesSharingDetails det : details) {
					if (det.getListType().equalsIgnoreCase("schedule")) {
						
						JSONObject jsonObject;
						String noteid=null;
						for(String str:mnList.getMnNotesDetails())
						{
							jsonObject = new JSONObject(str);
							noteid = (String) jsonObject.get("eventId");
							
						MnEventsSharingDetails share = new MnEventsSharingDetails();
						share.setUserId(det.getUserId());
						share.setSharingUserId(det.getSharingUserId());
						share.setListId(det.getListId());
						share.setNoteId(Integer.parseInt(noteid));
						share.setListType(det.getListType());
						share.setSharingGroupId(det.getSharingGroupId());
						share.setStatus(det.getStatus());
						share.setSharingLevel("book");
						share.setNoteLevel(noteLevel);
						share.setShareAllContactFlag(true);
						share.setSharingStatus("P");
						if (eventSharing != null)
							eventSharing.add(share);
					}
					}

				}
				insertNotesSharingDetails(details, eventSharing);
				size = 1;
			}
			String addUser = "", addUserName = "";
			if (mnUsers.getFriends() != null) {
				for (Integer integer : mnUsers.getFriends()) {
					MnUsers mnUsers2 = getUserDetailsObject(integer);
					if (mnUsers2 != null) {
						addUser = addUser + " <B>" + mnUsers2.getUserFirstName()+ " "+mnUsers2.getUserLastName()+ "</B>,";
						addUserName = addUserName + " <B>"+ mnUsers2.getUserFirstName()+ " "+mnUsers2.getUserLastName()+ "</B>,";
					}
				}
			}
			addUser = addUser.substring(0, addUser.lastIndexOf(","));
			if(addUser.length() > 36){
				addUser = addUser.substring(0,35)+"...";
			}
			addUserName = addUserName.substring(0, addUserName.lastIndexOf(","));
			if(addUserName.length() > 36){
				addUserName = addUserName.substring(0,35)+"...";
			}
			
			if (noteType.equalsIgnoreCase("Schedule")) {
				writeLog(mnList.getUserId(), "A", "Shared a "+getListType(mnList.getListType())+" book : <B>"+ mnList.getListName() + "</B> with All Contacts" + addUser+"", mnList.getListType(),listId, null, null, mnList.getListType());
				if (mnUsers.getFriends() != null && !mnUsers.getFriends().isEmpty()) {
					writeLog(mnList.getUserId(), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> Shared a "+getListType(mnList.getListType())+" book with you :<B>" + mnList.getListName()+"</B>",mnList.getListType(), listId, null, mnUsers.getFriends(), mnList.getListType());
				}
			} else {
				writeLog(mnList.getUserId(), "A", "Shared a "+getListType(mnList.getListType())+": <B>"+ mnList.getListName() +"</B> book with All Contacts" + addUser+"", mnList.getListType(),listId, null, null, mnList.getListType());
				if (mnUsers.getFriends() != null && !mnUsers.getFriends().isEmpty()) {
					writeLog(mnList.getUserId(), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> Shared a "+getListType(mnList.getListType())+" book with you :<B>" + mnList.getListName()+"</B>",mnList.getListType(), listId, null, mnUsers.getFriends(), mnList.getListType());
				}
			}
			if(logger.isDebugEnabled()){logger.debug("Updated all contact members for the book");}
			return "" + size;

		} catch (Exception e) {
			logger.error("Exception while Updating All Contact Members For Book :"+ e);
		}
		return "Exception while All Contact Updating Members For Book";
	}

	@Override
	public String updateGroupsForBook(String groups, String listId,String userId, String noteType,String fullName, String firstName, String bookName, String noteLevel) {
		List<MnNotesSharingDetails> details = new ArrayList<MnNotesSharingDetails>();
		List<MnEventsSharingDetails> eventSharing = new ArrayList<MnEventsSharingDetails>();
		Date date = new Date();
		List<Integer> newGroupIds = new ArrayList<Integer>();
		List<Integer> oldGroupIds = null;
		List<Integer> groupUser = new ArrayList<Integer>();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Boolean mailNotify=false;
		try {
			if(logger.isDebugEnabled()){logger.debug("updateGroupsForBook method called "+userId+" bookName: "+bookName);}
			Integer size = 0;
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			oldGroupIds = mnList.getSharedGroups();
			if (groups != null && !groups.equals("")) {
				if (groups.contains(",")) {
					String str[] = groups.split(",");
					for (int i = 0; i < str.length; i++) {
						newGroupIds.add(Integer.parseInt(str[i]));
					}
				} else {
					newGroupIds.add(Integer.parseInt(groups));
				}
			}

			if (oldGroupIds != null && !oldGroupIds.isEmpty()) {
				oldGroupIds.addAll(newGroupIds);
				Update update = new Update();
				update.set("sharedGroups", oldGroupIds);
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);

			} else {
				Update update = new Update();
				update.set("sharedGroups", newGroupIds);
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			}
			///////////////////////////
			
			if(newGroupIds!=null)
			{
				
			for (int i = 0; i < newGroupIds.size(); i++) 
			{
			groupUser = getUserIdBasedOnGroupId(newGroupIds.get(i));
			if (groupUser != null && !groupUser.isEmpty()) {
				for (Integer user:groupUser) 
				{
					MnNotesSharingDetails sharingDetails = new MnNotesSharingDetails();
					sharingDetails.setUserId(Integer.parseInt(userId));
					sharingDetails.setSharingUserId(user);
					sharingDetails.setSharingGroupId(newGroupIds.get(i));
					sharingDetails.setListId(Integer.parseInt(listId));
					sharingDetails.setListType(mnList.getListType());
					sharingDetails.setNoteId(0);
					sharingDetails.setSharedDate(formatter.format(date));
					sharingDetails.setSharingLevel("book");
					sharingDetails.setNoteLevel(noteLevel);
					sharingDetails.setShareAllContactFlag(false);
					sharingDetails.setStatus("A");
					details.add(sharingDetails);
					
					if(mnList.getListType().equalsIgnoreCase("schedule"))
					{
						MnEventsSharingDetails share = new MnEventsSharingDetails();
						share.setUserId(Integer.parseInt(userId));
						share.setSharingUserId(user);
						share.setListId(Integer.parseInt(listId));
						share.setNoteId(0);
						share.setListType(mnList.getListType());
						share.setSharingGroupId(newGroupIds.get(i));
						share.setSharingLevel("book");
						share.setNoteLevel(noteLevel);
						share.setShareAllContactFlag(false);
						share.setStatus("A");
						share.setSharingStatus("P");
						if (eventSharing != null)
							eventSharing.add(share);
					}
					
				}
			}
			
			//////////////mail content /////////////////
			List<MnUsers> list = getUsersDetails(groupUser);
			if (list != null && !list.isEmpty())
			{
				for (MnUsers users : list)
				{
					try{
						mailNotify=getUserMailNotification(users.getUserId(), mnList.getListType());
						if(users.getNotificationFlag().equalsIgnoreCase("yes"))	
						{
							if(mailNotify){
								SendMail sendMail=new SendMail(); 	
								String recipients[]={users.getEmailId()};
								String message=MailContent.sharingNotification+users.getUserFirstName()+MailContent.sharingNotification1+fullName+"  shared a new Notebook with you on Musicnote : "+ bookName+".."+MailContent.sharingNotification2;;
								String subject=firstName+" shared a new Notebook with you";
								sendMail.postEmail(recipients, subject,message);
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
		}
			insertNotesSharingDetails(details, eventSharing);

				// notification activity for old group members
				Set<Integer> addedNotiUserIdSet = new HashSet<Integer>();
				Set<Integer> notUserIdSet = new HashSet<Integer>();
				for (Integer oldMemberIds1 : newGroupIds) {
					try {

						Query query2 = new Query(Criteria.where("groupId").is(oldMemberIds1));

						List<MnGroupDetailsDomain> detailsDomain = mongoOperations.find(query2, MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);
						if (detailsDomain != null && !detailsDomain.isEmpty()) {
							for (MnGroupDetailsDomain domain : detailsDomain) {
								notUserIdSet.add(domain.getUserId());
							}
						}
					} catch (Exception e) {

					}
				}

				// new group members
				String newGroupName = "";
				if (newGroupIds != null && !newGroupIds.isEmpty()) {

					for (Integer groupId : newGroupIds) {

						Query query2 = new Query(Criteria.where("groupId").is(groupId));
						MnGroupDomain group = mongoOperations.findOne(query2, MnGroupDomain.class,JavaMessages.Mongo.MNGROUP);
						if (group != null) {
							newGroupName = newGroupName + group.getGroupName() + ", ";
						}
						
						if(newGroupName.lastIndexOf(",")!= -1){
							newGroupName = newGroupName.substring(0,newGroupName.lastIndexOf(","));
						}
							
						List<MnGroupDetailsDomain> detailsDomain = mongoOperations.find(query2, MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);
						if (detailsDomain != null && !detailsDomain.isEmpty()) {
							for (MnGroupDetailsDomain domain : detailsDomain) {
								addedNotiUserIdSet.add(domain.getUserId());
							}
						}
					}
				}
				
				
				// log --- activity

				MnUsers mnUsers = getUserDetailsObject(Integer.parseInt(userId));
				
				if (noteType.equalsIgnoreCase("Schedule")) {

					writeLog(Integer.parseInt(userId), "A", "Shared a "+getListType(mnList.getListType())+" book with Group <B>" + newGroupName+ "</B> : <B>"+mnList.getListName()+"</B>", mnList.getListType(), mnList.getListId().toString(), null, null, mnList.getListType());

					if (addedNotiUserIdSet != null && !addedNotiUserIdSet.isEmpty()) {
						writeLog(Integer.parseInt(userId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> Shared a "+getListType(mnList.getListType())+" book with Group <B>" + newGroupName+ "</B> : <B>"+mnList.getListName()+"</B>", mnList.getListType(), mnList.getListId().toString(), null, addedNotiUserIdSet, mnList.getListType());
					}
					if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
						notUserIdSet.removeAll(addedNotiUserIdSet);
						writeLog(Integer.parseInt(userId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> Shared a "+getListType(mnList.getListType())+" book with Group <B>" + newGroupName+ "</B> : <B>"+mnList.getListName()+"</B>", mnList.getListType(), mnList.getListId().toString(), null, notUserIdSet, mnList.getListType());
					}
				} else {

					writeLog(Integer.parseInt(userId), "A", "Shared "+ mnList.getListName() + " on "+ getListType(mnList.getListType()) + " book with Group <B>"+ newGroupName+ "</B> : <B>"+mnList.getListName()+"</B>", mnList.getListType(), mnList.getListId().toString(), null, null, mnList.getListType());

					if (addedNotiUserIdSet != null	&& !addedNotiUserIdSet.isEmpty()) {
						writeLog(Integer.parseInt(userId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> Shared a "+getListType(mnList.getListType())+" book with Group <B>" + newGroupName+ "</B> : <B>"+mnList.getListName()+"</B>", mnList.getListType(), mnList.getListId().toString(), null, addedNotiUserIdSet, mnList.getListType());
					}
					if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
						notUserIdSet.removeAll(addedNotiUserIdSet);
						writeLog(Integer.parseInt(userId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> Shared a "+getListType(mnList.getListType())+" book with Group <B>" + newGroupName+ "</B> : <B>"+mnList.getListName()+"</B>", mnList.getListType(), mnList.getListId().toString(), null, notUserIdSet, mnList.getListType());
					}
				}
			}
			if(logger.isDebugEnabled()){logger.debug("Updated groups for book");}
			return "" + size;
		} catch (Exception e) {
			logger.error("Exception while Updating Groups For Book :" + e);
		}
		return "Exception while Updating Groups For Book";
	}

	@Override
	public String updateMembersForBook(String members, String listId,String userId, String noteType, String noteLevel) {
		List<MnNotesSharingDetails> details = new ArrayList<MnNotesSharingDetails>();
		List<MnEventsSharingDetails> eventSharing = new ArrayList<MnEventsSharingDetails>();
		Date date = new Date();
		List<Integer> newUserIds = new ArrayList<Integer>();
		List<Integer> oldUserIds = null;
		List<Integer> oldUserIdTemp = new ArrayList<Integer>();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Set<Integer> notUserIdSet = new HashSet<Integer>();
		try {
			if(logger.isDebugEnabled()){logger.debug("updateMembersForBook method called "+userId+" noteLevel: "+noteLevel);}
			Integer size = 0;
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			oldUserIds = mnList.getSharedIndividuals();
			if (members != null && !members.equals("")) {
				if (members.contains(",")) {
					String str[] = members.split(",");
					for (int i = 0; i < str.length; i++) {
						if(!oldUserIds.contains(Integer.parseInt(str[i]))){
							newUserIds.add(Integer.parseInt(str[i]));
						}
					}
				} else {
					if(!oldUserIds.contains(Integer.parseInt(members))){
						newUserIds.add(Integer.parseInt(members));
					}
				}
			}

			if (oldUserIds != null && !oldUserIds.isEmpty()) {
				oldUserIds.addAll(newUserIds);
				Update update = new Update();
				update.set("sharedIndividuals", oldUserIds);
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);

			} else {
				Update update = new Update();
				update.set("sharedIndividuals", newUserIds);
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			}

			if (newUserIds != null && !newUserIds.isEmpty()) {
				for (Integer in : newUserIds) {
					
						try {
							notUserIdSet.add(in);
						} catch (Exception e) {

						}
						
					MnNotesSharingDetails sharingDetails = new MnNotesSharingDetails();
					sharingDetails.setUserId(Integer.parseInt(userId));
					sharingDetails.setSharingUserId(in);
					sharingDetails.setSharingGroupId(0);
					sharingDetails.setListId(Integer.parseInt(listId));
					sharingDetails.setListType(mnList.getListType());
					sharingDetails.setNoteId(0);
					sharingDetails.setSharingLevel("book");
					sharingDetails.setNoteLevel(noteLevel);
					sharingDetails.setShareAllContactFlag(false);
					sharingDetails.setSharedDate(formatter.format(date));
					sharingDetails.setStatus("A");
					details.add(sharingDetails);
						
				}
			}
			
				
			if (details != null && !details.isEmpty()) {
				for (MnNotesSharingDetails det : details) {
					try {
						notUserIdSet.add(det.getSharingUserId());
					} catch (Exception e) {

					}
					
					JSONObject jsonObject;
					String noteid=null;
					if (det.getListType().equalsIgnoreCase("schedule")) {
						
						for(String str:mnList.getMnNotesDetails())
						{
							MnEventsSharingDetails share = new MnEventsSharingDetails();
							try {
								
								jsonObject = new JSONObject(str);
								noteid = (String) jsonObject.get("eventId");
								share.setUserId(det.getUserId());
								share.setSharingUserId(det.getSharingUserId());
								share.setListId(det.getListId());
								share.setNoteId(Integer.parseInt(noteid));
								share.setListType(det.getListType());
								share.setSharingGroupId(det.getSharingGroupId());
								share.setSharingLevel("book");
								share.setNoteLevel(noteLevel);
								share.setShareAllContactFlag(false);
								share.setStatus(det.getStatus());
								share.setSharingStatus("P");
						if (eventSharing != null)
							eventSharing.add(share);
							}catch (Exception e) {}
						}
					}

				}
				insertNotesSharingDetails(details, eventSharing);

				// notification activity
				Set<Integer> addedNotiUserIdSet = new HashSet<Integer>();
				addedNotiUserIdSet.addAll(newUserIds);
				String addUser = "", addUserName = "";
				if (notUserIdSet != null) {
					for (Integer integer : addedNotiUserIdSet) {
						MnUsers mnUsers = getUserDetailsObject(integer);
						if (mnUsers != null) {
							addUser = addUser + "<B> "+ mnUsers.getUserFirstName()+" "+mnUsers.getUserLastName() + "</B>,";
						}
					}
				}
				if(addUser.lastIndexOf(",") != -1)
					addUser = addUser.substring(0,addUser.lastIndexOf(","));
				
				if(addUser.length() > 36  ){
					addUser = addUser.substring(0,35)+"...";
				}
				notUserIdSet.addAll(oldUserIds);
				// log --- activity
				if(notUserIdSet!= null && addedNotiUserIdSet!= null  ){
					notUserIdSet.removeAll(addedNotiUserIdSet);
				}
				
				MnUsers mnUsers = getUserDetailsObject(Integer.parseInt(userId));
				
				if (noteType.equalsIgnoreCase("Schedule")) {

					writeLog(Integer.parseInt(userId), "A", "Shared a "+getListType(mnList.getListType())+ " book: <B>"+ mnList.getListName() +"</B> with " + addUser,mnList.getListType(), mnList.getListId().toString(), null,null, mnList.getListType());

					if (addedNotiUserIdSet != null && !addedNotiUserIdSet.isEmpty()) {
						writeLog(Integer.parseInt(userId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> Shared a "+getListType(mnList.getListType())+ " book: <B>"+ mnList.getListName() + "</B> with " + addUser, mnList.getListType(),mnList.getListId().toString(), null, addedNotiUserIdSet, mnList.getListType());
					}
					if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
						writeLog(Integer.parseInt(userId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> Shared a "+getListType(mnList.getListType())+" book with you: <B>" + mnList.getListName()+"</B>",mnList.getListType(), mnList.getListId().toString(), null,notUserIdSet, mnList.getListType());
					}
				} else {
					writeLog(Integer.parseInt(userId), "A", "Shared a "+getListType(mnList.getListType())+ " book: <B>"+ mnList.getListName() + "</B> with " + addUser,mnList.getListType(), mnList.getListId().toString(), null,null, mnList.getListType());

					if (addedNotiUserIdSet != null && !addedNotiUserIdSet.isEmpty()) {
						writeLog(Integer.parseInt(userId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + "</B> Shared a "+getListType(mnList.getListType())+ " book: <B>"+ mnList.getListName() + "</B> with " + addUser,mnList.getListType(), mnList.getListId().toString(), null,addedNotiUserIdSet, mnList.getListType());
					}
					if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
						writeLog(Integer.parseInt(userId), "N", "<B>"+ mnUsers.getUserFirstName()+" "+ mnUsers.getUserLastName() + " Shared a "+getListType(mnList.getListType())+" book with you: <B>" + mnList.getListName()+"</B>",mnList.getListType(), mnList.getListId().toString(), null,notUserIdSet, mnList.getListType());
					}
				}
			}
			if(logger.isDebugEnabled()){logger.debug("Updated members for book");}
			return "" + size;
		} catch (Exception e) {
			logger.error("Exception while Updating Members For Book :" + e);
		}
		return "Exception while Updating Members For Book";
	}

	@Override
	public String deleteMembersForBook(String userIds, String listId,String userId) {
		List<Integer> deleteUserIds = new ArrayList<Integer>();
		List<Integer> oldUserIds = null;
		try {
			if(logger.isDebugEnabled()){logger.debug("deleteMembersForBook method called "+userId);}
			Integer size = 0;
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			oldUserIds = mnList.getSharedIndividuals();
			if (userIds != null && !userIds.equals("")) {
				if (userIds.contains(",")) {
					String str[] = userIds.split(",");
					for (int i = 0; i < str.length; i++) {
						deleteUserIds.add(Integer.parseInt(str[i]));
					}
				} else {
					deleteUserIds.add(Integer.parseInt(userIds));
				}
			}

			if (oldUserIds != null && !oldUserIds.isEmpty()) {
				oldUserIds.removeAll(deleteUserIds);
				Update update = new Update();
				update.set("sharedIndividuals", oldUserIds);
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);

			}

			if (deleteUserIds != null && !deleteUserIds.isEmpty()) {
				for (Integer in : deleteUserIds) {

					updateNotesSharingDetails(in.toString(), Integer.parseInt(userId), listId, "0");
				}
			}
			if(logger.isDebugEnabled()){logger.debug("Deleted members for the book");}
			return "" + size;
		} catch (Exception e) {
			logger.error("Exception while Deleting Members Book In Impl :" + e);
		}
		return "Exception while Deleting Members Book In Impl";
	}

	@Override
	public String deletegroupForBook(String groupIds, String listId,
			String userId) {
		List<Integer> deleteGroupIds = new ArrayList<Integer>();
		List<Integer> oldGroupIds = null;
		try {
			if(logger.isDebugEnabled()){logger.debug("deletegroupForBook method called "+userId);}
			Integer size = 0;
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			oldGroupIds = mnList.getSharedGroups();
			if (groupIds != null && !groupIds.equals("")) {
				if (groupIds.contains(",")) {
					String str[] = groupIds.split(",");
					for (int i = 0; i < str.length; i++) {
						deleteGroupIds.add(Integer.parseInt(str[i]));
					}
				} else {
					deleteGroupIds.add(Integer.parseInt(groupIds));
				}
			}

			if (oldGroupIds != null && !oldGroupIds.isEmpty()) {
				oldGroupIds.removeAll(deleteGroupIds);
				Update update = new Update();
				update.set("sharedGroups", oldGroupIds);
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);

			}

			if (deleteGroupIds != null && !deleteGroupIds.isEmpty()) {
				for (Integer in : deleteGroupIds) {

					updateNotesgroupSharingDetails(in.toString(), Integer.parseInt(userId), listId, "0");
				}
			}
			if(logger.isDebugEnabled()){logger.debug("Deleted group for the corresponding group");}
			return "" + size;
		} catch (Exception e) {
			logger.error("Exception while Deleting Groups Book In Impl :" + e);
		}
		return "Exception while Deleting Groups Book In Impl";
	}

	public void insertAdminVotes(String userId, String listId, String noteId) {
		try {
			if(logger.isDebugEnabled()){logger.debug("insertAdminVotes method called "+userId);}
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

			MnAdminVotes mnAdminVotes = new MnAdminVotes();

			mnAdminVotes.setUserId(Integer.parseInt(userId));
			mnAdminVotes.setListId(listId);
			mnAdminVotes.setNoteId(noteId);
			mnAdminVotes.setStatus("A");
			mnAdminVotes.setcDate(sdf.format(date));
			mnAdminVotes.setEditDate("");

			mongoOperations.insert(mnAdminVotes,
					JavaMessages.Mongo.MNADMINVOTES);
			if(logger.isDebugEnabled()){logger.debug("Inserted admin votes");}
		} catch (Exception e) {
			logger.error("Exception while insert admin votes in Impl!", e);
		}
	}

	public void updateAdminVoites(Query query, String userId, String listId,
			String noteId) {
		try {
			if(logger.isDebugEnabled()){logger.debug("updateAdminVoites method called "+userId);}
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

			Update updateQuery = new Update();
			updateQuery.set("status", "I");
			updateQuery.set("editDate", sdf.format(date));
			mongoOperations.updateFirst(query, updateQuery,JavaMessages.Mongo.MNADMINVOTES);
			if(logger.isDebugEnabled()){logger.debug("Updated admin votes");}
		} catch (Exception e) {
			logger.error("Exception while update admin votes in Impl!", e);
		}
	}

	public String getVotesBasedNote(String listId, String noteId) {
		String votes = "";
		try {
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
			if(logger.isDebugEnabled()){logger.debug("getVotesBasedNote method called noteId:"+noteId);}
			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			for(String note: mnList.getMnNotesDetails()){
				if(note.contains("\"noteId\":\""+noteId+"\"")){
					JSONObject jsonObject;
					try {
						jsonObject = new JSONObject(note);
						votes = (String) jsonObject.get("vote");
						if (votes.indexOf("[") != -1) {
							votes = votes.substring(votes.indexOf("[") + 1,votes.length());
						}
						if (votes.indexOf("]") != -1) {
							votes = votes.substring(0, votes.indexOf("]"));
						}
						if(logger.isDebugEnabled()){logger.debug("Got votes based on note");}
					} catch (Exception e) {
						logger.error(e);
					}
				}
			}
		} catch (Exception e) {
			logger.error("Exception while getVotesBasedNote in Impl!", e);
		}
		return votes;
	}

	@Override
	public String deleteAllContactMembersForBook(String userId, String listId,String noteType) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Query query = null;
		try {
			if(logger.isDebugEnabled()){logger.debug("deleteAllContactMembersForBook method called:"+userId);}
			Integer size = 0;
			query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);

			if (mnList.isShareAllContactFlag()) {
				Update update = new Update();
				update.set("shareAllContactFlag", false);
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			}

			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));

			MnUsers mnUsers = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);

			if (mnUsers.getFriends() != null && !mnUsers.getFriends().isEmpty()) {

				/* Note Sharing Details */
				query = new Query(Criteria.where("sharingUserId").in(mnUsers.getFriends()).and("listId").is(Integer.parseInt(listId)).and("noteId").is(0).and("userId").is(Integer.parseInt(userId)).and("listType").is(noteType).and("sharingLevel").is("book").and("shareAllContactFlag").is(true));

				Update updateMulti = new Update();
				updateMulti.set("endDate", sdf.format(date));
				updateMulti.set("status", "I");

				mongoOperations.updateMulti(query, updateMulti,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);

				/* Event Sharing Deatils */
				if (noteType.equalsIgnoreCase("Schedule")) {
					query = new Query(Criteria.where("sharingUserId").in(mnUsers.getFriends()).and("listId").is(Integer.parseInt(listId)).and("noteId").is(0).and("userId").is(Integer.parseInt(userId)).and("listType").is(noteType).and("sharingLevel").is("book").and("shareAllContactFlag").is(true));

					Update updateEvent = new Update();
					updateEvent.set("status", "I");

					mongoOperations.updateMulti(query, updateEvent,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
				}
				size = 1;
			}
			if(logger.isDebugEnabled()){logger.debug("Deleted all contact members for book");}
			return "" + size;

		} catch (Exception e) {
			logger.error("Exception while Deleting All Contact Members For Book :"+ e);
		}
		return "Exception while All Contact Deleting Members For Book";
	}

	@Override
	public String deleteAllContactMembersForNote(String userId, String listId,String noteId, String noteType) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Query query = null;
		Set<String> updateNoteDetails=new TreeSet<String>();
		try {
			if(logger.isDebugEnabled()){logger.debug("deleteAllContactMembersForNote method called:"+userId+" noteId: "+noteId);}
			Integer size = 0;
			query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);

			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			for (String note : mnList.getMnNotesDetails()) {

				if (noteType.equalsIgnoreCase("Schedule")) {
					if (note.contains("\"eventId\":\"" + noteId + "\"")) {
						removeNoteDeatils = note;
						if (note.contains("\"eventSharedAllContact\":1")) {
							note = note.replace("\"eventSharedAllContact\":1","\"eventSharedAllContact\":0");
						}
						tempNoteDetails = note;
					}
				} else {
					if (note.contains("\"noteId\":\"" + noteId + "\"")) {
						removeNoteDeatils = note;
						if (note.contains("\"noteSharedAllContact\":1")) {
							note = note.replace("\"noteSharedAllContact\":1","\"noteSharedAllContact\":0");
						}
						tempNoteDetails = note;
					}
				}
				updateNoteDetails.add(note);
			}
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			
			//Sharing All Contact Details Delete Base Update Note Details In Mn_NoteDetails Table Also - Venu
			query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
			MnNoteDetails noteDetails= mongoOperations.findOne(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
			if(noteDetails!=null){		
				update = new Update();
				update.set("noteSharedAllContact", false);
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNNOTEDETAILS);
			}
			//End -Venu

			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));

			MnUsers mnUsers = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			if (mnUsers.getFriends() != null && !mnUsers.getFriends().isEmpty()) {
				/* Note Sharing Details */
				query = new Query(Criteria.where("sharingUserId").in(mnUsers.getFriends()).and("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("userId").is(Integer.parseInt(userId)).and("listType").is(mnList.getListType()).and("sharingLevel").is("note").and("shareAllContactFlag").is(true));
				List<MnNotesSharingDetails> details=mongoOperations.find(query,MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				
				for(MnNotesSharingDetails test:details)
				{
				Query query2 = new Query(Criteria.where("sharingUserId").is(test.getSharingUserId()).and("userId").is(test.getSharingUserId()).and("listId").is(test.getListId()).and("noteId").is(test.getNoteId()).and("sharingLevel").is("individual").and("status").is("A"));
				Update update1 = new Update();
				update1.set("status", "I");
				mongoOperations.updateFirst(query2, update1,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				
				}
				
				Update updateMulti = new Update();
				updateMulti.set("endDate", sdf.format(date));
				updateMulti.set("status", "I");
				mongoOperations.updateMulti(query, updateMulti,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				
				/* Event Sharing Deatils */
				if (noteType.equalsIgnoreCase("Schedule")) {
					query = new Query(Criteria.where("sharingUserId").in(mnUsers.getFriends()).and("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("userId").is(Integer.parseInt(userId)).and("listType").is(mnList.getListType()).and("sharingLevel").is("note").and("shareAllContactFlag").is(true));

					Update updateEvent = new Update();
					updateEvent.set("status", "I");

					mongoOperations.updateMulti(query, updateEvent,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
				}

				size = 1;
			}
			if(logger.isDebugEnabled()){logger.debug("Deleted all contact members");}
			return "" + size;

		} catch (Exception e) {
			logger.error("Exception while Deleting All Contact Members For Note :"+ e);
		}
		return "Exception while All Contact Deleting Members For Note";
	}

	public List<MnNoteDetails> fetchSharingNotesBasedOnListId(String userId,String listType, String listId) {

		List<MnNoteDetails> mnNoteDetails = null;
		List<MnNotesSharingDetails> details = null;
		Query query = null;
		try {
			if(logger.isDebugEnabled()){logger.debug("fetchSharingNotesBasedOnListId method called "+userId);}	
			query = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("listType").is(listType).and("status").is("A").and("listId").is(Integer.parseInt(listId)).and("sharedBookId").is(0));
			query.sort().on("listId", Order.ASCENDING);
			details = mongoOperations.find(query, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			
			if (details != null && !details.isEmpty()) {


				for (MnNotesSharingDetails sharingDetails : details) {
					Query detailsQuery = null;
					if(sharingDetails.getNoteId().equals(0)){
						detailsQuery = new Query(Criteria.where("listId").is(sharingDetails.getListId()));
					}else{
						detailsQuery = new Query(Criteria.where("listId").is(sharingDetails.getListId()).and("noteId").is(sharingDetails.getNoteId()));
					}
					
					List<MnNoteDetails> mnNoteDetailsObj =mongoOperations.find(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
					if(mnNoteDetailsObj!= null && !mnNoteDetailsObj.isEmpty()){
						for(MnNoteDetails temp:mnNoteDetailsObj){
							
							if(sharingDetails.isShareAllContactFlag())
								temp.setShareType("allCon");
							else if(sharingDetails.getSharingGroupId()!=0)
								temp.setShareType("group"+sharingDetails.getSharingGroupId());
							else
								temp.setShareType("single");
						}
						
						
						if(mnNoteDetails!=null && !mnNoteDetails.isEmpty()){
							mnNoteDetails.addAll(mnNoteDetailsObj);
						}else{
							mnNoteDetails = mnNoteDetailsObj;
						}
					}
					
				}

			}
			if(logger.isDebugEnabled()){logger.debug("Sharing notes fetched");}
		} catch (Exception e) {
			logger.error("Exception while Fetching Shared Notes Based On List :"+ e);

		}

		return mnNoteDetails;

	}

	public List<MnNoteDetails> fetchGroupSharedNotesBasedOnListId(String userId,String listType,String listId) {
		List<MnList> mnListList = null;
		List<MnNoteDetails> mnNoteDetails = null;
		List<MnNotesSharingDetails> details = null;
		List<MnGroupDetailsDomain> mnGroupDetails = null;
		List<Integer> sharingGroupIds = null;
		Query query = null;
		try {
			if(logger.isDebugEnabled()){logger.debug("fetchGroupSharedNotesBasedOnListId method called "+userId);}
			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("status").is("A"));
			mnGroupDetails = mongoOperations.find(query,MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);
			if (mnGroupDetails != null && !mnGroupDetails.isEmpty()) {
				sharingGroupIds = new ArrayList<Integer>();
				for (MnGroupDetailsDomain domain : mnGroupDetails) {
					sharingGroupIds.add(domain.getGroupId());
				}
			}
			if (sharingGroupIds != null && !sharingGroupIds.isEmpty()) {
				query = new Query(Criteria.where("sharingGroupId").in(sharingGroupIds).and("listType").is(listType).and("status").is("A").and("listId").is(Integer.parseInt(listId)));
				query.sort().on("listId", Order.ASCENDING);
				details = mongoOperations.find(query,MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				
				if (details != null && !details.isEmpty()) {

					

					for (MnNotesSharingDetails sharingDetails : details) {
						Query detailsQuery = null;
						if(sharingDetails.getNoteId().equals(0)){
							detailsQuery = new Query(Criteria.where("listId").is(sharingDetails.getListId()));
						}else{
							detailsQuery = new Query(Criteria.where("listId").is(sharingDetails.getListId()).and("noteId").is(sharingDetails.getNoteId()));
						}
						List<MnNoteDetails> mnNoteDetailsObj =mongoOperations.find(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
						if(mnNoteDetailsObj!= null && !mnNoteDetailsObj.isEmpty()){
							if(mnNoteDetails!=null && !mnNoteDetails.isEmpty()){
								mnNoteDetails.addAll(mnNoteDetailsObj);
							}else{
								mnNoteDetails = mnNoteDetailsObj;
							}
						}
						
					}
				}
			}
			if(logger.isDebugEnabled()){logger.debug("Fetched group shared notes");}
		} catch (Exception e) {
			logger.error("Exception while Fetching Shared Notes Based On List :"+ e);

		}
		return mnNoteDetails;
	}

	
	
	@Override
	public String bookenableDisable(String userId, String listId) {
		String status = null;
		boolean flag;
		try {
			if(logger.isDebugEnabled()){logger.debug("bookenableDisable method called "+userId);}
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);

			if (mnList != null) {

				if (mnList.isEnableDisableFlag())
					flag = false;
				else
					flag = true;

				Update update = new Update();
				update.set("enableDisableFlag", flag);
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);

				status = "success";
				if(logger.isDebugEnabled()){logger.debug("Successfully book enabled/disabled");}
			}
			
		} catch (Exception e) {
			logger.error("Exception bookenableDisable method :" + e);
			status = "0";
		}
		return status;
	}

	@Override
	public List<MnNotesDetails> getFeatureNotes(String userId)
	{
		if(logger.isDebugEnabled()){logger.debug("getFeatureNotes method called "+userId);}
		List<MnVoteViewCount> viewCounts = null;
		List<MnList> mnListList=null;
		List<MnNotesDetails> details=new ArrayList<MnNotesDetails>();
		JSONObject jsonObject;
		String access;
		Query query = null;
		try
		{
			query = new Query();
			query.addCriteria(Criteria.where("status").is("A"));
			query.sort().on("adminVote", Order.DESCENDING);
			viewCounts = mongoOperations.find(query, MnVoteViewCount.class, JavaMessages.Mongo.MNVOTEVIEWCOUNT);
			for(MnVoteViewCount mnVoteViewCount:viewCounts)
			{
				Query query1 = new Query(Criteria.where("listId").is(Integer.parseInt(mnVoteViewCount.getListId())).and("noteAccess").is("public").and("status").is("A"));
				mnListList = mongoOperations.find(query1, MnList.class, JavaMessages.Mongo.MNLIST);
				if(mnListList!=null && !mnListList.isEmpty())
				{
				for(MnList list:mnListList)
				{
					for(String str:list.getMnNotesDetails())
					{
						jsonObject = new JSONObject(str);
						access = (String) jsonObject.get("access");
						if(access!=null && !access.isEmpty() &&  access.contains("public"))
						{
							if(mnVoteViewCount.getNoteId().equals((String) jsonObject.get("noteId")))
							{
						    MnNotesDetails mnNotesDetails=new MnNotesDetails();
						    mnNotesDetails.setNoteName(changedNoteNameWithDouble((String) jsonObject.get("noteName")));
							mnNotesDetails.setStatus((String) jsonObject.get("status"));
							mnNotesDetails.setNoteId(Integer.parseInt((String) jsonObject.get("noteId")));
							mnNotesDetails.setAccess((String) jsonObject.get("access"));
							mnNotesDetails.setUserId(list.getUserId());
							mnNotesDetails.setListId(list.getListId().toString());
							mnNotesDetails.setViewed(mnVoteViewCount.getViewed());
							mnNotesDetails.setVote(mnVoteViewCount.getVote());
							mnNotesDetails.setCountId(mnVoteViewCount.getCountId());
					
							details.add(mnNotesDetails);
							}
						}
						
					}
				}
				}
     		}
			
			if(logger.isDebugEnabled()){logger.debug("Got featured notes for "+userId);}
		}
		catch (Exception e)
		{
			logger.error("Exception while getting featured notes :" + e);

		}
		return details;
	}
	
	public String getListNoteNamebyId(String listId,String noteId){
		String name= "";
		try{
			if(logger.isDebugEnabled()){logger.debug("getListNoteNamebyId method called noteId: "+noteId+" listId: "+listId);}
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
				
			if (mnList.getMnNotesDetails() != null && !mnList.getMnNotesDetails().isEmpty()) {
				for (String noteString : mnList.getMnNotesDetails()) {
					if (noteString.contains("\"noteId\":\"" + noteId + "\"")) {
						JSONObject jsonObject = new JSONObject(noteString);
						name = "{\"noteName\":\""+changedNoteNameWithDouble((String)jsonObject.get("noteName"))+"\",\"listName\":\""+mnList.getListName()+"\",\"listType\":\""+mnList.getListType()+"\"}";
					}
				}
			}
			if(logger.isDebugEnabled()){logger.debug("Got List Note name");}
		}catch(Exception ex){
			logger.error("Exception in Get note & List Name in Impl!",ex);
		}
		return name;
	}
	public String getListType(String listTypeFrom){
		String listType ="";
		try{
			if(logger.isDebugEnabled()){logger.debug("getListType method called");}
			if(listTypeFrom.equals("bill")){
				listType = "Memo";
			}else if(listTypeFrom.equals("music")){
				listType = "Music";
			}else if(listTypeFrom.equals("schedule")){
				listType = "Schedule";
			}
			if(logger.isDebugEnabled()){logger.debug("Got listType"+listType);}
		}catch (Exception e) {
			logger.error(e);
		}
		return listType;
	}
	public String getListTypePage(String listTypeFrom){
		String listType ="";
		try{
			if(logger.isDebugEnabled()){logger.debug("getListTypePage method called");}
			if(listTypeFrom.equals("bill")){
				listType = "Memos page";
			}else if(listTypeFrom.equals("schedule")){
				listType = "Scheduling page";
			}else if(listTypeFrom.equals("music")){
				listType = "Musicnote page";
			}
			if(logger.isDebugEnabled()){logger.debug("Got listType"+listType);}
		}catch (Exception e) {
			logger.error(e);
		}
		return listType;
	}
	@Override
	public String getCommentsCountBasedOnNote(String listId, String noteId, String userId)
	{
		List<MnComments> commentsList = null;
		Query query = null;
		try {
			if(logger.isDebugEnabled()){logger.debug("getCommentsCountBasedOnNote method called "+userId);}
			query = new Query(Criteria.where("listId").is(listId).and("noteId").is(noteId).and("status").is("A"));
			
			query.sort().on("_id", Order.DESCENDING);
			commentsList = mongoOperations.find(query, MnComments.class,JavaMessages.Mongo.MNCOMMETS);
			if(logger.isDebugEnabled()){logger.debug("Got comments count based on note");}
		} catch (Exception e) {
			logger.error("Exception while get note commnets Count In Impl !", e);
		}
		if(commentsList!=null)
			return "{\"len\":"+commentsList.size()+"}";
		else
			return "{\"len\":\"0\"}";
	}

	@Override
	public String copyPublicNote(String listId, String noteId, String userId)
	{
		if(logger.isDebugEnabled()){logger.debug("copyPublicNote method called "+userId +" noteId:"+noteId);}
		String noteData = "";
		String dueDate = "";
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		try {
			// Fetch Selected Note From List
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);

			String selectedNoteDeatils = null;
			String tempNoteDetails = null;
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					selectedNoteDeatils = note;
					tempNoteDetails = note;
					JSONObject jsonObject = new JSONObject(note);
					dueDate = (String) jsonObject.get("dueDate");
					
				}
			}
			// added comments in cmd table
			
			List<MnComments> mnCommentsList = null;
			List<MnAttachmentDetails> mnAttachList = null;
			JSONObject jsonObject2 = null;
			List<Integer> cmtList = new ArrayList<Integer>();
			List<Integer> attachList = new ArrayList<Integer>();
			int cId = 0;
			int attchId = 0;
				jsonObject2 = new JSONObject(tempNoteDetails);
				String commenntString =(String) jsonObject2.get("pcomments");
				if (commenntString.lastIndexOf(']') != -1) {
					commenntString = commenntString.substring(0, commenntString.lastIndexOf(']'));
				}
				if (commenntString.lastIndexOf('[') != -1) {
					commenntString = commenntString.substring( commenntString.lastIndexOf('[')+1, commenntString.length());
				}
				String[] comts = commenntString.split(",");
				
				for(String str:comts){
					if(str!=null && !str.isEmpty())
						cmtList.add(Integer.parseInt(str.trim()));
				}
				
				String privateAttach = (String) jsonObject2.get("privateAttachFilePath");
				if (privateAttach.lastIndexOf(']') != -1) {
					privateAttach = privateAttach.substring(0, privateAttach.lastIndexOf(']'));
				}
				if (privateAttach.lastIndexOf('[') != -1) {
					privateAttach = privateAttach.substring( privateAttach.lastIndexOf('[')+1, privateAttach.length());
				}
				String[] privateAttachArray = privateAttach.split(",");
				
				for(String str:privateAttachArray){
					if(str!=null && !str.isEmpty())
						attachList.add(Integer.parseInt(str.trim()));
				}
				
				Query query2 = new Query(Criteria.where("cId").in(cmtList));
				mnCommentsList = mongoOperations.find(query2,MnComments.class, JavaMessages.Mongo.MNCOMMETS);
				
				
				List<MnComments> mnCommentsListTot = mongoOperations.findAll(MnComments.class, JavaMessages.Mongo.MNCOMMETS);
				if (mnCommentsListTot != null && mnCommentsListTot.size() != 0) {
					cId = mnCommentsListTot.size() + 1;
				} else {
					cId = 1;
				}
				
				Query query3 = new  Query(Criteria.where("attachId").in(attachList));
				mnAttachList = mongoOperations.find(query3,MnAttachmentDetails.class, JavaMessages.Mongo.MNATTACHMENTDETAILS);
			
				List<MnAttachmentDetails> mnAttachmentDetails = mongoOperations.findAll(MnAttachmentDetails.class, JavaMessages.Mongo.MNATTACHMENTDETAILS);
				if (mnAttachmentDetails != null && mnAttachmentDetails.size() != 0) {
					attchId = mnAttachmentDetails.size() + 1;
				} else {
					attchId = 1;
				}
			
			// Adding Selected List to Selected Note
			Query addQuery = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("listType").is("music").and("defaultNote").is(true));

			MnList addNote = mongoOperations.findOne(addQuery, MnList.class,JavaMessages.Mongo.MNLIST);

			Integer size = addNote.getMnNotesDetails().size() + 1;
			selectedNoteDeatils = selectedNoteDeatils.replace("\"noteId\":\""+ noteId + "\"", "\"noteId\":\"" + size + "\"");
			
			//insert into comments in cmd table
			try{
				if(cmtList!= null && !cmtList.isEmpty()){
					cmtList.clear();
				}
				for(MnComments comments : mnCommentsList){
					comments.setcId(cId);
					cmtList.add(cId);
					cId++;
					comments.setListId(addNote.getListId().toString());
					comments.setNoteId(size.toString());
					
					if(comments.getCommLevel().equalsIgnoreCase("P"))
					comments.setCommLevel("P/I");
					
						
				}
				mongoOperations.insert(mnCommentsList, JavaMessages.Mongo.MNCOMMETS);
			}catch (Exception e) {
			}
			try{
				mnAttachmentDetails = new ArrayList<MnAttachmentDetails>();
				if(attachList!= null && !attachList.isEmpty()){
					attachList.clear();
				}
				for(MnAttachmentDetails attach : mnAttachList){
					attach.setAttachId(attchId);
					attachList.add(attchId);
					attchId++;
					attach.setaDate(dateFormat.format(date));
					attach.setaTime(timeFormat.format(date));
					attach.setListId(addNote.getListId());
					attach.setNoteId(size.toString());
					attach.setStatus("A");
					attach.setUserId(Integer.parseInt(userId));
					attach.setEndDate("");
					attach.setEndTime("");
					mnAttachmentDetails.add(attach);
				}
				if(mnAttachList!= null && !mnAttachList.isEmpty()){
					mongoOperations.insert(mnAttachList,JavaMessages.Mongo.MNATTACHMENTDETAILS);
				}
			}catch (Exception e) {
			}
			
			
			
			JSONObject jsonObject=new JSONObject(selectedNoteDeatils);
			
			if(jsonObject.has("fromCrowd"))
			{
			selectedNoteDeatils = selectedNoteDeatils.replace("\"fromCrowd\":\""+ (String) jsonObject.get("fromCrowd") + "\"", "\"fromCrowd\":\"True\"");
			}
			else
			{
				jsonObject.append("fromCrowd", "True");
				selectedNoteDeatils=jsonObject.toString();
			}
			
			if(jsonObject.has("publicDescription"))
			{
				selectedNoteDeatils = selectedNoteDeatils.replace("\"publicDescription\":\""+ (String) jsonObject.get("publicDescription") + "\"", "\"publicDescription\":\"\"");
			}
			else
			{
				jsonObject.append("publicDescription", "\"\"");
				selectedNoteDeatils=jsonObject.toString();
			}
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"notesMembers\":\""+ (String) jsonObject.get("notesMembers") + "\"", "\"notesMembers\":\"\"");
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"noteGroups\":\""+ (String) jsonObject.get("noteGroups") + "\"", "\"noteGroups\":\"\"");
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"noteSharedAllContact\":"+ (Integer) jsonObject.get("noteSharedAllContact") , "\"noteSharedAllContact\":0");
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"tag\":\""+ (String) jsonObject.get("tag") + "\"", "\"tag\":\"\"");
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"remainders\":\""+ (String) jsonObject.get("remainders") + "\"", "\"remainders\":\"\"");
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"vote\":\""+ (String) jsonObject.get("vote") + "\"", "\"vote\":\"\"");
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"access\":\""+ (String) jsonObject.get("access") + "\"", "\"access\":\"private\"");

			selectedNoteDeatils = selectedNoteDeatils.replace("\"attachFilePath\":\""+ (String) jsonObject.get("attachFilePath") + "\"", "\"attachFilePath\":\""+attachList.toString()+"\"");

			selectedNoteDeatils = selectedNoteDeatils.replace("\"copyToMember\":\""+ (String) jsonObject.get("copyToMember") + "\"", "\"copyToMember\":\"\"");
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"copyToGroup\":\""+ (String) jsonObject.get("copyToGroup") + "\"", "\"copyToGroup\":\"\"");
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"copyToAllContact\":"+ (Integer) jsonObject.get("copyToAllContact") , "\"copyToAllContact\":0");
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"copyFromMember\":\""+ (String) jsonObject.get("copyFromMember")+"\"" , "\"copyFromMember\":\"\"");
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"dueDate\":\""+ (String) jsonObject.get("dueDate")+"\"" , "\"dueDate\":\"\"");
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"dueTime\":\""+ (String) jsonObject.get("dueTime")+"\"" , "\"dueTime\":\"\"");

			selectedNoteDeatils = selectedNoteDeatils.replace("\"comments\":\""+(String) jsonObject.getString("comments")+"\"", "\"comments\":\""+cmtList.toString()+"\"");
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"pcomments\":\""+(String) jsonObject.getString("pcomments")+"\"", "\"pcomments\":\"\"");
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"privateTags\":\""+ (String) jsonObject.get("privateTags") + "\"", "\"privateTags\":\"\"");
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"publicUser\":\""+ (String) jsonObject.get("publicUser") + "\"", "\"publicUser\":\"\"");
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"publicDate\":\""+ (String) jsonObject.get("publicDate") + "\"", "\"publicDate\":\"\"");
			
			selectedNoteDeatils = selectedNoteDeatils.replace("\"privateAttachFilePath\":\""+ (String) jsonObject.get("privateAttachFilePath") + "\"", "\"privateAttachFilePath\":\"\"");
			
			
			
			if (addNote.getMnNotesDetails() != null	&& addNote.getMnNotesDetails().isEmpty()) {
				addNote.setMnNotesDetails(new HashSet<String>());
				addNote.getMnNotesDetails().add(selectedNoteDeatils);
			} else {
				addNote.getMnNotesDetails().add(selectedNoteDeatils);
			}

			Update addUpdate = new Update();
			addUpdate.set("mnNotesDetails", addNote.getMnNotesDetails());
			mongoOperations.updateFirst(addQuery, addUpdate,JavaMessages.Mongo.MNLIST);
			
			if(!dueDate.equals(""))
			dueDateUpdateBasedOnListMoveCopy(addNote.getListId().toString(),size,userId,dueDate,"A");

			MnPublicSharedNoteCopyDetails publicSharedNoteCopyDetails=new MnPublicSharedNoteCopyDetails();
			publicSharedNoteCopyDetails.setUserId(Integer.parseInt(userId));
			publicSharedNoteCopyDetails.setListId(listId);
			publicSharedNoteCopyDetails.setNoteId(noteId);
			publicSharedNoteCopyDetails.setStartDate(sdf.format(date));
			publicSharedNoteCopyDetails.setStatus("A");
			mongoOperations.insert(publicSharedNoteCopyDetails, JavaMessages.Mongo.MNPUBLICSHAREDNOTECOPY);
			
			if(selectedNoteDeatils!=null && !selectedNoteDeatils.isEmpty()){
				MnNoteDetails details=new MnNoteDetails();
				details.setNoteName(changedNoteNameWithDouble((String) jsonObject.get("noteName")));
				details.setStartDate(date);
				details.setNoteSharedAllContact(false);
				details.setStatus("A");
				details.setOwnerNoteStatus("A");
				details.setAccess("private");
				details.setUserId(Integer.parseInt(userId));
				details.setListId(addNote.getListId());
				details.setListName(addNote.getListName());
				details.setListType(addNote.getListType());
				details.setNoteAccess(addNote.getNoteAccess());
				details.setDefaultNote(addNote.isDefaultNote());
				details.setEnableDisableFlag(addNote.isEnableDisableFlag());
				details.setSharedIndividuals(addNote.getSharedIndividuals());
				details.setSharedGroups(addNote.getSharedGroups());
				details.setShareAllContactFlag(addNote.isShareAllContactFlag());
				details.setNoteId(size);
				details.setAttachFilePath(attachList);
				details.setComments(cmtList);
				details.setDescription(changedNoteNameWithDouble((String) jsonObject.get("description")));
				
				mongoOperations.insert(details,JavaMessages.Mongo.MNNOTEDETAILS);
			}
			moveNoteLog(tempNoteDetails, mnList, addNote.getListName(),"Copied from", userId,false);

			noteData = selectedNoteDeatils;
			if(logger.isDebugEnabled()){logger.debug("Copied note to public");}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while Copying Public Note To Our Own List :"+ e);
		}

		return noteData;
	}

	@Override
	public String changePublicShareWarnMsgFlag(String userId)
	{
		String status = "{";
		Query query = null;
		MnUsers mnUsers = null;
		try {
			if(logger.isDebugEnabled()){logger.debug("changePublicShareWarnMsgFlag method called "+userId);}
			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));

			mnUsers = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);

			if (mnUsers != null) {
							
				Update update = new Update();
				update.set("publicShareWarnMsgFlag", true);
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNUSERS);
				
				status = status + "\"status\":\"success\"";
			}else{
				status = status + "\"status\":\"fail\"";
			}
			status=status+" }";
			if(logger.isDebugEnabled()){logger.debug("changed public share warn message flag");}
		} catch (Exception e) {
			logger.error("Exception while change Public Share Warn Msg Flag in impl!:" + e);
		}
		return status;
	}

	@Override
	public String deleteAllContactMembersSharingForSingleUser(String userId, String listId, String noteId, String noteType, String fromUserId)
	{
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Query query = null;
		try {
			if(logger.isDebugEnabled()){logger.debug("deleteAllContactMembersSharingForSingleUser method called "+userId +"noteId: "+noteId);}
			if(!noteType.equals("schedule")){
				/* Note Sharing Details */
				query = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("userId").is(Integer.parseInt(fromUserId)).and("listType").is(noteType).and("sharingLevel").is("note").and("shareAllContactFlag").is(true).and("status").is("A"));

				Update updateMulti = new Update();
				updateMulti.set("endDate", sdf.format(date));
				updateMulti.set("status", "I");

				mongoOperations.updateFirst(query, updateMulti,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			}else{
				/* Event Sharing Deatils */
					query = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("userId").is(Integer.parseInt(fromUserId)).and("listType").is(noteType).and("sharingLevel").is("note").and("shareAllContactFlag").is(true).and("status").is("A"));

					Update updateEvent = new Update();
					updateEvent.set("status", "I");

					mongoOperations.updateFirst(query, updateEvent,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
			}
			if(logger.isDebugEnabled()){logger.debug("deleted all contact members that shares for single user");}

		} catch (Exception e) {
			logger.error("Exception while Deleting All Contact Sharing For Single User :"+ e);
		}
		return "Exception while Deleting All Contact Sharing For Single User";
	}

	@Override
	public String updateCommentLevel(String listId, String noteId, String commentsId) {
		List<MnComments> mnComments;
		String string = null;
		try
		{
			if(logger.isDebugEnabled()){logger.debug("updateCommentLevel method called commentsId: "+commentsId);}
			if (commentsId.contains("[")) {
				commentsId = commentsId.substring(commentsId.indexOf("[") + 1,
						commentsId.length());
			}
			if (commentsId.contains("]")) {
				commentsId = commentsId.substring(0, commentsId.indexOf("]"));
			}
			
			String[] paramArray=commentsId.split(",");
			List<Integer> paramsList = new ArrayList<Integer>();
			if(paramArray.length >0 && paramArray[0]!=null && !paramArray.equals("")){
				for(String par:paramArray){
					if(par!=null && !par.equals(""))
					paramsList.add(Integer.parseInt(par.trim()));
				}
			}
			
			Query query=new Query(Criteria.where("cId").in(paramsList));
			mnComments=mongoOperations.find(query,MnComments.class,JavaMessages.Mongo.MNCOMMETS);
			if(mnComments!=null)
			{
					Update update=new Update();
					update.set("commLevel", "P/I");
					mongoOperations.updateMulti(query,update,JavaMessages.Mongo.MNCOMMETS);
					string="success";
			}
			if(logger.isDebugEnabled()){logger.debug("Updated comment level");}
		}catch (Exception e) {
			logger.error("Exception while update Comment Level Note", e);
		}
		return string;
	}
	public MnVideoFile getMnVideoFileDetails(Integer fileId){
		MnVideoFile mnVideoFile = null;
		try{
			if(logger.isDebugEnabled()){logger.debug("getMnVideoFileDetails method called "+fileId);}
			Query query=new Query(Criteria.where("Id").is(fileId));
			mnVideoFile= mongoOperations.findOne(query,MnVideoFile.class,JavaMessages.Mongo.MnFiles);
			if(logger.isDebugEnabled()){logger.debug("Got video file details fileid="+fileId);}
		}catch (Exception e) {
			logger.error("Exception while fetching  Mn Video File Details"+e);	
		}
		return mnVideoFile;
	}
	@Override
	public List<MnAttachmentDetails> getAttachedFileForNote(List<Integer> paramsList, String userId) {
		List<MnAttachmentDetails> mnAttachmentDetailsList = null;
		List<MnAttachmentDetails> mnAttachmentDetailsReturn = new ArrayList<MnAttachmentDetails>();
		String loginUserZone=null;
		Query query = null;
		try {
			if(logger.isDebugEnabled()){logger.debug("getAttachedFileForNote method called "+userId);}
			Query query3 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
   			MnUsers mnUser = mongoOperations.findOne(query3, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			loginUserZone=mnUser.getTimeZone();
			
			query = new Query(Criteria.where("attachId").in(paramsList));
			query.sort().on("attachId", Order.DESCENDING);
			mnAttachmentDetailsList = mongoOperations.find(query,MnAttachmentDetails.class,JavaMessages.Mongo.MNATTACHMENTDETAILS);

			
			for(MnAttachmentDetails mnAttachment:mnAttachmentDetailsList)
			{
				// *************** change time zone ************* ///
				Date date=null;
				DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
				String d=mnAttachment.getCurrentTime();
					date=formatter.parse(d);
					Calendar c = Calendar.getInstance();
					c.setTime(date);
					TimeZone obj = TimeZone.getTimeZone("GMT"+loginUserZone);
				formatter.setTimeZone(obj);
				String convertedDate=formatter.format(c.getTime());
				mnAttachment.setCurrentTime(convertedDate);
				mnAttachmentDetailsReturn.add(mnAttachment);
				//****************************//
			}
			if(logger.isDebugEnabled()){logger.debug("Got attached file for note for "+userId);}
			
		} catch (Exception e) {
			logger.error("Exception while get AttachedFile For Note In Impl !", e);
		}
		return mnAttachmentDetailsReturn;
	}

	@Override
	public String searchEngine(String searchingNote) {
		List<MnList> mnListList = null;
		List<MnComments> comments=null;
		List<MnNotesDetails> details=new ArrayList<MnNotesDetails>();
		Query query = null;
		String noteName=null;
		String description=null;
		JSONObject jsonObject;
		String noteId=null;
		String noteAccess=null;
		boolean searchingFalg=false;
		JSONArray array=new JSONArray();
		
		try {
			if(logger.isDebugEnabled()){logger.debug("searchEngine method called searchingNote: "+searchingNote);}
			query = new Query(Criteria.where("noteAccess").is("public").and("status").is("A"));
			mnListList = mongoOperations.find(query, MnList.class,JavaMessages.Mongo.MNLIST);
			
			if(mnListList!=null && !mnListList.isEmpty())
			{

				for(MnList list:mnListList)
				{
					for(String str:list.getMnNotesDetails())
					{
						searchingFalg=false;
						jsonObject = new JSONObject(str);
						noteName=(String) jsonObject.get("noteName");
						noteAccess=(String) jsonObject.get("access");
						noteId=(String) jsonObject.get("noteId");
						description=(String) jsonObject.get("description");
						
						if(searchingNote!=null && (noteName.contains(searchingNote) || description.contains(searchingNote))&& noteAccess.contains("public"))
						{
							searchingFalg=true;  
						}
						if(noteAccess.contains("public")){
							Query query2=new  Query(Criteria.where("listId").is(list.getListId().toString()).and("noteId").is(noteId).and("commLevel").ne("I"));
							comments=mongoOperations.find(query2, MnComments.class,JavaMessages.Mongo.MNCOMMETS);
							if(comments!=null && !comments.isEmpty())
							{
							
								for(MnComments comments2:comments)
								{
									if(comments2.getComments().contains(searchingNote))
									{
									searchingFalg=true;
									}
								}
							}
						}
						if(searchingFalg)
						{
							    MnNotesDetails mnNotesDetails=new MnNotesDetails();
							    mnNotesDetails.setNoteName(changedNoteNameWithDouble((String) jsonObject.get("noteName")));
								mnNotesDetails.setStatus((String) jsonObject.get("status"));
								mnNotesDetails.setNoteId(Integer.parseInt((String) jsonObject.get("noteId")));
								mnNotesDetails.setAccess((String) jsonObject.get("access"));
								mnNotesDetails.setPublicUser((String)jsonObject.get("publicUser"));
								mnNotesDetails.setPublicDate((String)jsonObject.get("publicDate"));
								mnNotesDetails.setUserId(list.getUserId());
								mnNotesDetails.setListId(list.getListId().toString());
								details.add(mnNotesDetails);
						}
						}
					
					
						
					}
				}
			if(logger.isDebugEnabled()){logger.debug("SearchEngine method executed successfully");}
		
		} catch (Exception e) {
			logger.error("Exception while searching error List :" + e);

		}
		return array.toString();
	}


	public List<MnNoteDetails> byKewordConvertMnListTo(List<MnNoteDetails> mnNoteDetails,String keyWord,String listType){
		List<MnComments> comments=null;
		List<MnNoteDetails> newMnNoteDetailsList = new ArrayList<MnNoteDetails>();
		String noteName=null;
		String description=null;
		String noteId=null;
		String noteAccess=null;
		boolean searchingFalg=false;
		try{
			if(logger.isDebugEnabled()){logger.debug("byKewordConvertMnListTo method called "+keyWord);}
			for(MnNoteDetails list:mnNoteDetails)
			{
				searchingFalg=false;
				if(!listType.equals("schedule")){	
					noteName= list.getNoteName();
					noteId=list.getNoteId().toString();
				}
				noteAccess=list.getAccess();
				description=list.getDescription();
				if(keyWord!=null && (noteName.toLowerCase().contains(keyWord.toLowerCase()) || description.toLowerCase().contains(keyWord.toLowerCase())))
				{
					searchingFalg=true;
				}
				if(!listType.equals("schedule")){	
					Query query2=new  Query(Criteria.where("listId").is(list.getListId().toString()).and("noteId").is(noteId));
					comments=mongoOperations.find(query2, MnComments.class,JavaMessages.Mongo.MNCOMMETS);
					if(comments!=null && !comments.isEmpty())
					{
						for(MnComments comments2:comments)
						{
							if(comments2.getComments().toLowerCase().contains(keyWord.toLowerCase()))
							{
								searchingFalg=true;
							}
						}
					}
				}
				if(searchingFalg)
				{
					newMnNoteDetailsList.add(list);
				}
				if(logger.isDebugEnabled()){logger.debug("byKewordConvertMnListTo method processed successfully");}
			}
		}catch (Exception e) {
			logger.error("Exception while by Keword Convert MnList To List :" + e);
		}
		return newMnNoteDetailsList;
		
	}
	
	@Override
	public List<MnNotesDetails> getSearch(String searchingNote,String userId) {
	if(logger.isDebugEnabled()){logger.debug("getSearch method called "+userId+" searchingNote:"+searchingNote);}	
	List<MnList> mnListList = null;
	List<MnComments> comments=null;
	List<MnNotesDetails> details=new ArrayList<MnNotesDetails>();
	Query query = null;
	String noteName=null;
	String description=null;
	JSONObject jsonObject;
	String publicUserName;
	String noteId=null;
	String noteAccess=null;
	boolean searchingFalg=false;
	JSONArray array=new JSONArray();
	
	try {
		
		query = new Query(Criteria.where("noteAccess").is("public").and("status").is("A"));
		mnListList = mongoOperations.find(query, MnList.class,JavaMessages.Mongo.MNLIST);
		
		if(mnListList!=null && !mnListList.isEmpty())
		{

			for(MnList list:mnListList)
			{
				for(String str:list.getMnNotesDetails())
				{
					searchingFalg=false;
					jsonObject = new JSONObject(str);
					noteName=(String) jsonObject.get("noteName");
					noteAccess=(String) jsonObject.get("access");
					noteId=(String) jsonObject.get("noteId");
					description=(String) jsonObject.get("description");
					
					if(searchingNote!=null && (noteName.toLowerCase().contains(searchingNote.toLowerCase()) || description.toLowerCase().contains(searchingNote.toLowerCase()))&& noteAccess.contains("public"))
					{
						searchingFalg=true;  
					}
					if(noteAccess.contains("public")){
						Query query2=new  Query(Criteria.where("listId").is(list.getListId().toString()).and("noteId").is(noteId).and("commLevel").ne("I"));
						comments=mongoOperations.find(query2, MnComments.class,JavaMessages.Mongo.MNCOMMETS);
						if(comments!=null && !comments.isEmpty())
						{
						
							for(MnComments comments2:comments)
							{
								if(comments2.getComments().toLowerCase().contains(searchingNote.toLowerCase()))
								{
								searchingFalg=true;
								}
							}
						}
					}
					if(searchingFalg)
					{
						    MnNotesDetails mnNotesDetails=new MnNotesDetails();
						    mnNotesDetails.setNoteName(changedNoteNameWithDouble((String) jsonObject.get("noteName")));
							mnNotesDetails.setStatus((String) jsonObject.get("status"));
							mnNotesDetails.setNoteId(Integer.parseInt((String) jsonObject.get("noteId")));
							mnNotesDetails.setAccess((String) jsonObject.get("access"));
							/// for public user change
							String publicUserId=(String) jsonObject.get("publicUser");
							boolean isUserId = publicUserId.matches("[0-9]*");
							if(isUserId){
							MnUsers mnUsers=getUserDetails(Integer.parseInt(publicUserId));
							publicUserName=mnUsers.getUserFirstName()+" "+mnUsers.getUserLastName();
							}else{
								publicUserName=(String) jsonObject.get("publicUser");
							}
							mnNotesDetails.setPublicUser(publicUserName);
							String jdate=(String)jsonObject.get("publicDate");
							String tDate=getTimeZone(jdate,userId);
							mnNotesDetails.setPublicDate(tDate);
							mnNotesDetails.setUserId(list.getUserId());
							mnNotesDetails.setListId(list.getListId().toString());
							mnNotesDetails.setTagIds((String)jsonObject.get("privateTags"));
							details.add(mnNotesDetails);
					}
					}
				}
			}
	
		
		} catch (Exception e) {
			logger.error("Exception while seaching get recent note ereror List :" + e);

		}
		if(logger.isDebugEnabled()){logger.debug("Searching executed successfully for "+userId);}
		return details;
		}

	@Override
	public List<MnNotesDetails> getSearchMnMostViewed(String searchText,
			String noteAccess,String userId) {
		if(logger.isDebugEnabled()){logger.debug("getSearchMnMostViewed method called "+userId+" searchText: "+searchText);}
		List<MnVoteViewCount> viewCounts = null;
		List<MnList> mnListList = null;
		List<MnNotesDetails> details = new ArrayList<MnNotesDetails>();
		JSONObject jsonObject;
		String access;
		String status = "";
		String noteName=null;
		boolean searchingFalg=false;
		String description=null;
		String noteId=null;
		String publicUserName;
		List<MnComments> comments=null;
		List<MnNotesDetails> tempDetails=new ArrayList<MnNotesDetails>();
		Query query = null;
		try {
			query = new Query();
			query.addCriteria(Criteria.where("status").is("A"));
			query.sort().on("viewed", Order.DESCENDING);
			viewCounts = mongoOperations.find(query, MnVoteViewCount.class,JavaMessages.Mongo.MNVOTEVIEWCOUNT);
			
			
			Query query1 = new Query(Criteria.where("noteAccess").is("public").and("status").is("A"));
			mnListList = mongoOperations.find(query1, MnList.class,JavaMessages.Mongo.MNLIST);
			
			if(mnListList!=null && !mnListList.isEmpty())
			{
			

				for(MnList list:mnListList)
				{
					for(String str:list.getMnNotesDetails())
					{
						searchingFalg=false;
						jsonObject = new JSONObject(str);
						noteName=(String) jsonObject.get("noteName");
						noteAccess=(String) jsonObject.get("access");
						noteId=(String) jsonObject.get("noteId");
						description=(String) jsonObject.get("description");
						
						if(searchText!=null && (noteName.toLowerCase().contains(searchText.toLowerCase()) || description.toLowerCase().contains(searchText.toLowerCase()))&& noteAccess.contains("public"))
						{
							searchingFalg=true;  
						}
						if(noteAccess.contains("public")){
							Query query2=new  Query(Criteria.where("listId").is(list.getListId().toString()).and("noteId").is(noteId).and("commLevel").ne("I"));
							comments=mongoOperations.find(query2, MnComments.class,JavaMessages.Mongo.MNCOMMETS);
						
							if(comments!=null && !comments.isEmpty())
							{
							
								for(MnComments comments2:comments)
								{
								
									if(comments2.getComments().toLowerCase().contains(searchText.toLowerCase()))
									{
									searchingFalg=true;
									}
								}
							}
						}
						if(searchingFalg)
						{
							    MnNotesDetails mnNotesDetails=new MnNotesDetails();
							    mnNotesDetails.setNoteName(changedNoteNameWithDouble((String) jsonObject.get("noteName")));
								mnNotesDetails.setStatus((String) jsonObject.get("status"));
								mnNotesDetails.setNoteId(Integer.parseInt((String) jsonObject.get("noteId")));
								mnNotesDetails.setAccess((String) jsonObject.get("access"));
								/// for public user change
								String publicUserId=(String) jsonObject.get("publicUser");
								boolean isUserId = publicUserId.matches("[0-9]*");
								if(isUserId){
								MnUsers mnUsers=getUserDetails(Integer.parseInt(publicUserId));
								publicUserName=mnUsers.getUserFirstName()+" "+mnUsers.getUserLastName();
								}else{
									publicUserName=(String) jsonObject.get("publicUser");
								}
							
								mnNotesDetails.setPublicUser(publicUserName);
								String jdate=(String)jsonObject.get("publicDate");
								String tdate=getTimeZone(jdate,userId);
								mnNotesDetails.setPublicDate(tdate);
								mnNotesDetails.setUserId(list.getUserId());
								mnNotesDetails.setListId(list.getListId().toString());
								mnNotesDetails.setTagIds((String)jsonObject.get("privateTags"));
								details.add(mnNotesDetails);
						}
						}
					}
				}
			
			for(MnNotesDetails details2:details)
			{
			for(MnVoteViewCount mnVoteViewCount : viewCounts)
			{
				if(details2.getListId().contains(mnVoteViewCount.getListId()) && details2.getNoteId().toString().contains(mnVoteViewCount.getNoteId()))
				{
					
					details2.setNoteName(details2.getNoteName());
					details2.setStatus(details2.getStatus());
					details2.setNoteId(details2.getNoteId());
					details2.setAccess(details2.getAccess());
					details2.setPublicUser(details2.getPublicUser());
					details2.setPublicDate(details2.getPublicDate());
					details2.setUserId(details2.getUserId());
					details2.setListId(details2.getListId());
					details2.setViewed(mnVoteViewCount.getViewed());
					tempDetails.add(details2);
				}
				
			}
			}
			
			

		} catch (Exception e) {
			logger.error("Exception while get Searching  MostViewed List :" + e);

		}
		if(logger.isDebugEnabled()){logger.debug("Searched mostvoted list for "+userId);}
		return tempDetails;
	}

	@Override
	public List<MnNotesDetails> getSearchMnMostVoted(String searchText,
		String noteAccess,String userId) {
		if(logger.isDebugEnabled()){logger.debug("getSearchMnmostVoted method called"+userId+" searchText: "+searchText);}
		if(logger.isDebugEnabled())
			logger.debug("getSearchMnMostVoted method called: ");	
		List<MnVoteViewCount> viewCounts = null;
		List<MnList> mnListList = null;
		List<MnNotesDetails> details = new ArrayList<MnNotesDetails>();
		JSONObject jsonObject;
		String access;
		String status = "";
		String noteName=null;
		boolean searchingFalg=false;
		String description=null;
		String noteId=null;
		String publicUserName;
		List<MnComments> comments=null;
		List<MnNotesDetails> tempDetails=new ArrayList<MnNotesDetails>();
		Query query = null;
		try {
			query = new Query();
			query.addCriteria(Criteria.where("status").is("A"));
			query.sort().on("vote", Order.DESCENDING);
			viewCounts = mongoOperations.find(query, MnVoteViewCount.class,JavaMessages.Mongo.MNVOTEVIEWCOUNT);
			
			
			Query query1 = new Query(Criteria.where("noteAccess").is("public").and("status").is("A"));
			mnListList = mongoOperations.find(query1, MnList.class,JavaMessages.Mongo.MNLIST);
			
			if(mnListList!=null && !mnListList.isEmpty())
			{

				for(MnList list:mnListList)
				{
					for(String str:list.getMnNotesDetails())
					{
						searchingFalg=false;
						jsonObject = new JSONObject(str);
						noteName=(String) jsonObject.get("noteName");
						noteAccess=(String) jsonObject.get("access");
						noteId=(String) jsonObject.get("noteId");
						description=(String) jsonObject.get("description");
						
						if(searchText!=null && (noteName.toLowerCase().contains(searchText.toLowerCase()) || description.toLowerCase().contains(searchText.toLowerCase()))&& noteAccess.contains("public"))
						{
							searchingFalg=true;  
						}
						if(noteAccess.contains("public")){
							Query query2=new  Query(Criteria.where("listId").is(list.getListId().toString()).and("noteId").is(noteId).and("commLevel").ne("I"));
							comments=mongoOperations.find(query2, MnComments.class,JavaMessages.Mongo.MNCOMMETS);
							if(comments!=null && !comments.isEmpty())
							{
							
								for(MnComments comments2:comments)
								{
									if(comments2.getComments().toLowerCase().contains(searchText.toLowerCase()))
									{
									searchingFalg=true;
									}
								}
							}
						}
						if(searchingFalg)
						{
							    MnNotesDetails mnNotesDetails=new MnNotesDetails();
							    mnNotesDetails.setNoteName(changedNoteNameWithDouble((String) jsonObject.get("noteName")));
								mnNotesDetails.setStatus((String) jsonObject.get("status"));
								mnNotesDetails.setNoteId(Integer.parseInt((String) jsonObject.get("noteId")));
								mnNotesDetails.setAccess((String) jsonObject.get("access"));
								mnNotesDetails.setPublicUser((String) jsonObject.get("publicUser"));
								String jdate=(String)jsonObject.get("publicDate");
								String tdate=getTimeZone(jdate,userId);
								mnNotesDetails.setPublicDate(tdate
										);
								mnNotesDetails.setUserId(list.getUserId());
								mnNotesDetails.setListId(list.getListId().toString());
								mnNotesDetails.setTagIds((String)jsonObject.get("privateTags"));
								details.add(mnNotesDetails);
						}
						}
					
					
						
					}
				}
			
			for(MnNotesDetails details2:details)
			{
			for(MnVoteViewCount mnVoteViewCount : viewCounts)
			{
				if(details2.getListId().contains(mnVoteViewCount.getListId()) && details2.getNoteId().toString().contains(mnVoteViewCount.getNoteId()))
				{
					
					details2.setNoteName(details2.getNoteName());
					details2.setStatus(details2.getStatus());
					details2.setNoteId(details2.getNoteId());
					details2.setAccess(details2.getAccess());
					details2.setPublicUser(details2.getPublicUser());
					details2.setPublicDate(details2.getPublicDate());
					details2.setUserId(details2.getUserId());
					details2.setListId(details2.getListId());
					details2.setVote(mnVoteViewCount.getVote());
					tempDetails.add(details2);
				}
				
			}
			}
			
			

		} catch (Exception e) {
			logger.error("Exception while get Searching  Motevoted List :" + e);

		}
		if(logger.isDebugEnabled()){logger.debug("Got Most Voted List for "+userId);}
		return tempDetails;
	}

	@Override
	public String getBookNames(String userId,String listType) {
		if(logger.isDebugEnabled())
			logger.debug("getBookNames method called: "+userId);	
		List<MnList> mnListList = null;
		StringBuffer jsonStr=null;
		Query query = null;
		
		try {
			
			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("listType").is(listType).and("status").is("A"));
			mnListList = mongoOperations.find(query, MnList.class,JavaMessages.Mongo.MNLIST);
			
			if(mnListList !=null )
			{
				 jsonStr = new StringBuffer("[");
				for(MnList list:mnListList)
				{
					jsonStr.append("{\"bookName\" : \"" + list.getListName() + "\",");
					jsonStr.append("\"listId\" : \"" + list.getListId() + "\"},");
				}
				jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
				jsonStr.append("]");
			}

		} catch (Exception e) {
			logger.error("Exception while getBookNames  :" + e);

		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for getBookNames method : ");
		return jsonStr.toString();
	
	}
	@Override
	public String createNoteInDefualtList(String mnNote, String listType, String userId,String access,String selectedBookId) {
		try {
			if(logger.isDebugEnabled())
				logger.debug("createNoteInDefualtList method called: "+userId+" selectedBookId: "+selectedBookId);	
			Integer size = 0;
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Query query =null;
			String publicUserName;
			if(listType.equals("schedule") && selectedBookId.equals("0"))
				query = new Query(Criteria.where("defaultNote").is(true).and("userId").is(Integer.parseInt(userId)).and("listType").is(listType));
			else if(listType.equals("schedule") && !selectedBookId.equals("0"))
				query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("listType").is(listType).and("listId").is(Integer.parseInt(selectedBookId)));
			else
				query = new Query(Criteria.where("defaultNote").is(true).and("userId").is(Integer.parseInt(userId)).and("listType").is(listType));
			
			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);

			if (mnList.getMnNotesDetails() != null	&& !mnList.getMnNotesDetails().isEmpty()) {
				size = mnList.getMnNotesDetails().size() + 1;
				if(listType.equals("schedule")){
					mnNote = mnNote.replace("\"eventId\":\"\"", "\"eventId\":\""+ size + "\"");
				}else{
					mnNote = mnNote.replace("\"noteId\":\"\"", "\"noteId\":\""+ size + "\"");
				}
				mnList.getMnNotesDetails().add(mnNote);
			} else {
				Set<String> tempNotes = new HashSet<String>();
				size = 1;
				if(listType.equals("schedule")){
					mnNote = mnNote.replace("\"eventId\":\"\"", "\"eventId\":\""+ size + "\"");
				}else{
					mnNote = mnNote.replace("\"noteId\":\"\"", "\"noteId\":\""+ size + "\"");
				}
				tempNotes.add(mnNote);
				mnList.setMnNotesDetails(tempNotes);
			}

			Update update = new Update();
			if(access!=null && !access.isEmpty() && access.equals("public"))
				update.set("noteAccess", access);
			
			update.set("mnNotesDetails", mnList.getMnNotesDetails());
			mongoOperations.updateFirst(query, update,	JavaMessages.Mongo.MNLIST);

			// log --- activity
			JSONObject jsonObject = new JSONObject(mnNote);
			
			if(!selectedBookId.equals("0") && !listType.equals("schedule") && mnList.getListId()!= Integer.parseInt(selectedBookId)){
				MnNotesSharingDetails sharingDetails = new MnNotesSharingDetails();
				sharingDetails.setUserId(Integer.parseInt(userId));
				sharingDetails.setSharingUserId(Integer.parseInt(userId));
				sharingDetails.setSharingGroupId(0);
				sharingDetails.setListId(mnList.getListId());
				sharingDetails.setListType(listType);
				sharingDetails.setNoteId(size);
				sharingDetails.setSharingLevel("individual");
				sharingDetails.setNoteLevel("edit");
				sharingDetails.setShareAllContactFlag(false);
				sharingDetails.setSharedDate(formatter.format(date));
				sharingDetails.setStatus("A");
				sharingDetails.setSharedBookId(Integer.parseInt(selectedBookId));
			
				mongoOperations.insert(sharingDetails,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			}
			
			
			// Create Note Details Added To Note deatils table also
			if(mnNote!=null && !mnNote.isEmpty() && !listType.equals("schedule")){
				MnNoteDetails details=new MnNoteDetails();
				details.setNoteName(changedNoteNameWithDouble((String) jsonObject.get("noteName")));
				details.setStartDate(date);
				details.setNoteSharedAllContact(false);
				details.setStatus("A");
				details.setOwnerNoteStatus("A");
				details.setAccess((String) jsonObject.get("access"));
				details.setPublicUser((String) jsonObject.get("publicUser"));
				details.setDueDate((String) jsonObject.get("dueDate"));
				details.setDueTime((String) jsonObject.get("dueTime"));
				details.setDescription((String) jsonObject.get("description"));
				details.setCopyFromMember((String) jsonObject.get("copyFromMember"));
				String publicDate=(String) jsonObject.get("publicDate");
				if(publicDate !=null && !publicDate.equals(""))
					details.setPublicDate(date);
					
				details.setUserId(Integer.parseInt(userId));
				details.setListId(mnList.getListId());
				details.setListName(mnList.getListName());
				details.setListType(mnList.getListType());
				details.setNoteAccess(mnList.getNoteAccess());
				details.setDefaultNote(mnList.isDefaultNote());
				details.setEnableDisableFlag(mnList.isEnableDisableFlag());
				details.setNoteId(size);
				
				mongoOperations.insert(details,JavaMessages.Mongo.MNNOTEDETAILS);
			}
			String noteName="";
			if(listType.equals("schedule")){
				noteName = (String) jsonObject.get("eventName");
			}else{
				noteName = (String) jsonObject.get("noteName");
			}
			if(noteName.indexOf("\"")!= -1){
				noteName = noteName.replace("\"", "\\\"");
			}
			if(listType.equals("schedule")){
				writeLog(Integer.parseInt(userId), "A", "Added new event: <B>"+ noteName +  "</B> to " + getListTypePage(mnList.getListType()), mnList.getListType(), mnList.getListId().toString(),(String) jsonObject.get("eventId"), null, mnList.getListType());
				return "" + size+"\",\"listId\":\""+mnList.getListId()+"\",\"listName\":\""+mnList.getListName();
			}else{
				writeLog(Integer.parseInt(userId), "A", "Added new note: <B>"+ noteName +  "</B> to " + getListTypePage(mnList.getListType()), mnList.getListType(), mnList.getListId().toString(),(String) jsonObject.get("noteId"), null, mnList.getListType());
				return "" + size+"\",\"listId\":\""+mnList.getListId();
			}

		} catch (Exception e) {
			logger.error("Exception while creating Note in createNoteInDefualtList :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for createNoteInDefualtList method : ");
		return "0";
	}
	public void sahreBasedOnEmailId(String emailIds, String  listId, Integer noteId,String userId,String level, String fullName, String noteName, String noteType){
		try{
			if(logger.isDebugEnabled())
				logger.debug("sahreBasedOnEmailId method called: "+userId+" noteName: "+noteName);	
			int nsId;
			List<MnNonUserSharingDetails> mnNonUserSharingDetailsList = mongoOperations.findAll(MnNonUserSharingDetails.class, JavaMessages.Mongo.MNNONUSERSHARINGDETAILS);
			if (mnNonUserSharingDetailsList != null && mnNonUserSharingDetailsList.size() != 0) {
				nsId = mnNonUserSharingDetailsList.size() + 1;
			} else {
				nsId = 1;
			}
			List<MnNonUserSharingDetails> mnNonUserSharingList = checkAllEmailForNmUsers( nsId, emailIds, Integer.parseInt(listId.trim()), noteId,Integer.parseInt(userId),level,fullName,noteName) ;
			
			
			if(mnNonUserSharingList!= null && !mnNonUserSharingList.isEmpty()){
				
				for( MnNonUserSharingDetails list:mnNonUserSharingList)
				{
					Query query = null;
					query = new Query(Criteria.where("listId").is(list.getListId()).and("noteId").is(list.getNoteId()).and("userId").is(list.getUserId()).and("emailId").is(list.getEmailId()));
					MnNonUserSharingDetails mnNonUserSharingDetails=mongoOperations.findOne(query,MnNonUserSharingDetails.class,JavaMessages.Mongo.MNNONUSERSHARINGDETAILS);
				
					if(mnNonUserSharingDetails==null){
						mongoOperations.insert(list,JavaMessages.Mongo.MNNONUSERSHARINGDETAILS);
					}
				}
			}
		} catch (Exception e) {
			logger.error("Exception while creating Note in sahreBasedOnEmailId :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for sahreBasedOnEmailId method : ");
	}
	public List<MnNonUserSharingDetails> checkAllEmailForNmUsers(int nsId, String emailIdString, Integer  listId, Integer noteId,Integer userId,String level, String fullName, String noteName){
		List<MnNonUserSharingDetails> mnNonUserSharingDetailsList = new ArrayList<MnNonUserSharingDetails>();
		if(logger.isDebugEnabled())
			logger.debug("checkAllEmailForNmUsers method called: "+userId+" noteName: "+noteName);	
		Boolean mailNotify=false;
		try{
			Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			List<MnNotesSharingDetails> mnNotesSharingDetailsList =  new ArrayList<MnNotesSharingDetails>();
			List<MnEventsSharingDetails> mnEventsSharingDetailsList = new ArrayList<MnEventsSharingDetails>();
			List<String> nonMnEmailIdList = new ArrayList<String>();
			String[] emailArray = emailIdString.split(",");
			Query query = null;
			Update update = new Update();
			update.set("requestPending", true);
			
			query = new Query(Criteria.where("listId").is(listId));
			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			String listType = "";
			if(mnList!= null ){
				listType= mnList.getListType();
			}
			int scharedEventId = 0;
			if(listType.equals("schedule")){
				List<MnEventsSharingDetails> sharingDetailsList = mongoOperations.findAll(MnEventsSharingDetails.class, JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
				if(sharingDetailsList!= null &&  mnNonUserSharingDetailsList.size() != 0){
					scharedEventId = sharingDetailsList.size()+1;
				}else{
					scharedEventId = 1;
				}
			}
			String message="";
			String subject="";
			String[] userName=null;
			
				for(String emailId : emailArray)
				{
				query = new Query(Criteria.where("emailId").in(emailId.trim()));
				MnUsers mnUsers = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS); 
				
				if(mnUsers != null )
				{
					Set<Integer> friends =null;
					friends=mnUsers.getFriends();
					
					if(friends!=null && friends.contains(userId))
					{
						updateMembersForNote(mnUsers.getUserId().toString(), listId.toString(), noteId.toString(), userId.toString(), listType, level);
					}
					else
					{
					MnNotesSharingDetails notesSharingDetails = new MnNotesSharingDetails();
					notesSharingDetails.setListId(listId);
					notesSharingDetails.setListType(listType);
					notesSharingDetails.setNoteId(noteId);
					notesSharingDetails.setNoteLevel(level);
					if(noteId == 0){
						notesSharingDetails.setSharingLevel("book");
					}else{
						notesSharingDetails.setSharingLevel("note");
					}
					notesSharingDetails.setStatus("E");
					notesSharingDetails.setShareAllContactFlag(false);
					notesSharingDetails.setSharedDate(formatter.format(date));
					notesSharingDetails.setSharingGroupId(0);
					notesSharingDetails.setSharingUserId(mnUsers.getUserId());
					notesSharingDetails.setUserId(userId);
					
					mnNotesSharingDetailsList.add(notesSharingDetails);
					
					if(listType.equals("schedule")){
												
						MnEventsSharingDetails eventsSharingDetails = new MnEventsSharingDetails();
						eventsSharingDetails.setUserId(userId);
						eventsSharingDetails.setSharingUserId(mnUsers.getUserId());
						eventsSharingDetails.setListId(listId);
						eventsSharingDetails.setNoteId(noteId);
						eventsSharingDetails.setNoteLevel(level);
						eventsSharingDetails.setListType(listType);
						eventsSharingDetails.setEventSharedId(scharedEventId);
						eventsSharingDetails.setShareAllContactFlag(false);
						eventsSharingDetails.setSharingGroupId(0);
						eventsSharingDetails.setStatus("E");
						if(noteId == 0){
							eventsSharingDetails.setSharingLevel("book");
						}else{
							eventsSharingDetails.setSharingLevel("note");
						}
						eventsSharingDetails.setSharingStatus("P");
						mnEventsSharingDetailsList.add(eventsSharingDetails);
						scharedEventId++;
					}
					
					mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNUSERS);
					
				}
					userName=emailId.split("@");
					if(noteId == 0)
						{
							message=MailContent.sharingNotification+userName[0]+MailContent.sharingNotification1+fullName+"  shared a new Notebook with you on Musicnote : "+ noteName+" "+ 
							MailContent.sharingNotification2;
							subject=fullName+" shared a new Notebook with you";
						}
						else
						{
							message=MailContent.sharingNotification+userName[0]+MailContent.sharingNotification1+fullName+"  shared a new note with you on Musicnote : "+ noteName+" "+ 
							MailContent.sharingNotification2;
							subject=fullName+" shared a new note with you";
						}
					
				}else{
					nonMnEmailIdList.add(emailId);
					userName=emailId.split("@");
					if(noteId == 0)
					{
						message=MailContent.sharingNotification+userName[0]+MailContent.sharingNotification1+fullName+"  shared a new Notebook with you on Musicnote : "+ noteName+" "+ 
						"<br><br>In order to view this Notebook you must be logged into your Musicnote profile. If you are not signed up for Musicnote yet, sign up and get a free 30-day trial today! Musicnote helps musicians, music students and music teachers to play, learn and teach music more easily."+ MailContent.sharingNotification2;
						subject=fullName+" shared a new Notebook with you";
					}
					else
					{
						message=MailContent.sharingNotification+userName[0]+MailContent.sharingNotification1+fullName+"  shared a new note with you on Musicnote : "+ noteName+" "+ 
						"<br><br>In order to view this note you must be logged into your Musicnote profile. If you are not signed up for Musicnote yet, sign up and get a free 30-day trial today! Musicnote helps musicians, music students and music teachers to play, learn and teach music more easily."+ MailContent.sharingNotification2;
						subject=fullName+" shared a new note with you";
					}
					
				}
				
				// ************************* sending mail ******************** //
				
				SendMail sendMail=new SendMail(); 	
				String recipients[]={emailId.toLowerCase()};
				try{
					mailNotify=getUserMailNotification(mnUsers.getUserId(), mnList.getListType());
					if(mailNotify){
					logger.info("message"+message);
					sendMail.postEmail(recipients,subject,message);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				// **********************************************************//
				
				
			}
			if(mnNotesSharingDetailsList!= null && !mnNotesSharingDetailsList.isEmpty()){
				mongoOperations.insert(mnNotesSharingDetailsList, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			
			}
			
			if(mnEventsSharingDetailsList!= null && !mnEventsSharingDetailsList.isEmpty()){
				mongoOperations.insert(mnEventsSharingDetailsList, JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
			}
			if(nonMnEmailIdList!= null && !nonMnEmailIdList.isEmpty()){
				mnNonUserSharingDetailsList = convertJsonToNonUserObj(nsId, nonMnEmailIdList, listId, noteId, userId, level, listType,true);
			}
			
			
		}catch (Exception e) {
			logger.error("Exception while check All Email For Nm Users :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for checkAllEmailForNmUsers method : ");
		return mnNonUserSharingDetailsList;
	}
	
	 // copy mailsharing //
	
	public void copyBasedOnEmailId(String emailIds, String listId, int noteId,String userId, String noteLevel, String fullName, String noteName,boolean withComments)
	{
		if(logger.isDebugEnabled())
			logger.debug("copyBasedOnEmailId method called: ");	
		try{
			int nsId;
			List<MnNonUserSharingDetails> mnNonUserSharingDetailsList = mongoOperations.findAll(MnNonUserSharingDetails.class, JavaMessages.Mongo.MNNONUSERSHARINGDETAILS);
			if (mnNonUserSharingDetailsList != null && mnNonUserSharingDetailsList.size() != 0) {
				nsId = mnNonUserSharingDetailsList.size() + 1;
			} else {
				nsId = 1;
			}
			 List<MnNonUserSharingDetails> mnNonUserSharingList = checkAllEmailForNmUsersCopy( nsId, emailIds, Integer.parseInt(listId.trim()), noteId,Integer.parseInt(userId),noteLevel,fullName,noteName,withComments) ;
			 
			if(mnNonUserSharingList!= null && !mnNonUserSharingList.isEmpty()){
				
				for( MnNonUserSharingDetails list:mnNonUserSharingList)
				{
				Query query = null;
				query = new Query(Criteria.where("listId").is(list.getListId()).and("noteId").is(list.getNoteId()).and("userId").is(list.getUserId()).and("emailId").is(list.getEmailId()));
				MnNonUserSharingDetails mnNonUserSharingDetails=mongoOperations.findOne(query,MnNonUserSharingDetails.class,JavaMessages.Mongo.MNNONUSERSHARINGDETAILS);
				
				if(mnNonUserSharingDetails==null)
				{
					mongoOperations.insert(list,JavaMessages.Mongo.MNNONUSERSHARINGDETAILS);
				}
				}
			}
		} catch (Exception e) {
			logger.error("Exception while creating Note in copyBasedOnEmailId :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for copyBasedOnEmailId method : ");
	}
	
	
	public List<MnNonUserSharingDetails> checkAllEmailForNmUsersCopy(int nsId, String emailIdString, Integer  listId, Integer noteId,Integer userId,String level, String fullName, String noteName, boolean withComments){
		List<MnNonUserSharingDetails> mnNonUserSharingDetailsList = new ArrayList<MnNonUserSharingDetails>();
		Boolean mailNotify=false;
		try{
			if(logger.isDebugEnabled())
				logger.debug("checkAllEmailForNmUsersCopy method called: "+userId+" noteName: "+noteName);	
			List<String> nonMnEmailIdList = new ArrayList<String>();
			String[] emailArray = emailIdString.split(",");
			Query query = null;
			Update update = new Update();
			update.set("requestPending", true);
			
			query = new Query(Criteria.where("listId").is(listId));
			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			String listType = "";
			if(mnList!= null ){
				listType= mnList.getListType();
			}
			
			String message="";
			String subject="";
			String[] userName=null;
			List<Integer> member=null;
			
				for(String emailId : emailArray)
				{
				query = new Query(Criteria.where("emailId").in(emailId.trim()));
				MnUsers mnUsers = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS); 
				if(mnUsers != null ){
					
					userName=emailId.split("@");
					
							message=MailContent.sharingNotification+userName[0]+MailContent.sharingNotification1+fullName+"  shared a new note with you on Musicnote : "+ noteName+" "+ 
							MailContent.sharingNotification2;
							subject=fullName+" shared a copy of a note with you";
							
							mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNUSERS);
							member=new ArrayList<Integer>();
							member.add(mnUsers.getUserId());
							
							Set<Integer> friends =null;
							friends=mnUsers.getFriends();
							
							if(friends!=null && friends.contains(userId))
							{
								copyNoteToIndividual(member, listId.toString(), noteId.toString(), withComments, userId.toString());
							}
							else
							{
								mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNUSERS);
								nonMnEmailIdList.add(emailId);
							}
							
					}else{
					nonMnEmailIdList.add(emailId);
					userName=emailId.split("@");
					
						message=MailContent.sharingNotification+userName[0]+MailContent.sharingNotification1+fullName+"  shared a new note with you on Musicnote : "+ noteName+" "+ 
						"<br><br>In order to view this note you must be logged into your Musicnote profile. If you are not signed up for Musicnote yet, sign up and get a free 30-day trial today! Musicnote helps musicians, music students and music teachers to play, learn and teach music more easily."+ MailContent.sharingNotification2;
						subject=fullName+" shared a copy of a note with you";
				}
				
				// ************************* sending mail ******************** //
				
				SendMail sendMail=new SendMail(); 	
				String recipients[]={emailId.toLowerCase()};
				try{
					mailNotify=getUserMailNotification(mnUsers.getUserId(), mnList.getListType());
					if(mailNotify){
					logger.info("message"+message);
					sendMail.postEmail(recipients,subject,message);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				// **********************************************************//
				
				
			}
				
			
			if(nonMnEmailIdList!= null && !nonMnEmailIdList.isEmpty()){
				mnNonUserSharingDetailsList = convertJsonToNonUserObj(nsId, nonMnEmailIdList, listId, noteId, userId, level, listType,withComments);
			}
			
		}catch (Exception e) {
			logger.error("Exception while check All Email For Nm Users :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for checkAllEmailForNmUsersCopy method : ");
		return mnNonUserSharingDetailsList;
	}
	
	
	public List<MnNonUserSharingDetails> convertJsonToNonUserObj(int nsId, List<String> emailIdString, Integer  listId, Integer noteId,Integer userId,
				String level, String listType,boolean withComments){
		if(logger.isDebugEnabled())
			logger.debug("convertJsonToNonUserObj method called: "+userId);	
		List<MnNonUserSharingDetails> mnNonUserSharingDetailsList = new ArrayList<MnNonUserSharingDetails>();
		try{

			for(String emailId : emailIdString){
				MnNonUserSharingDetails details = new MnNonUserSharingDetails();
				details.setListId(listId);
				details.setNoteId(noteId);
				details.setUserId(userId);
				details.setEmailId(emailId.toLowerCase());
				details.setStatus("P");
				details.setNsId(nsId);
				
				details.setListType(listType);
				if(noteId == 0){
					details.setSharingLevel("book");
					details.setNoteLevel(level);
					details.setStatus("A");
				}else{
					if(level.equals("copy"))
					{
						details.setStatus("P");
						details.setSharingLevel(level);
						if(withComments)
							details.setNoteLevel("comments");
						else
							details.setNoteLevel("withOutComments");
					}
					else
					{
						details.setStatus("A");
						details.setSharingLevel("note");
						details.setNoteLevel(level);
					}
				}
				
				nsId++;
				mnNonUserSharingDetailsList.add(details);
			}
			
		}catch (Exception e) {
			logger.error("Exception in convert Json To NonUserObj method  : ", e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for convertJsonToNonUserObj method : ");
		return mnNonUserSharingDetailsList;
	}

	
	
	
	@Override
	public String updateBookForNote(String bookId, String listId, String noteId, String userId, String listType)
	{
		if(logger.isDebugEnabled())
			logger.debug("updateBookForNote method called: "+userId+" noteId: "+noteId);	
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		try {
			Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A").and("listType").is(listType).and("sharingLevel").is("individual").and("sharedBookId").is(Integer.parseInt(bookId)));

			MnNotesSharingDetails notesSharingDetails = mongoOperations.findOne(query, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			
			if(notesSharingDetails!=null){
				Update update = new Update();
				update.set("status", "I");
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				return "{\"status\":\"I\"}";
			}else{
				
				MnNotesSharingDetails sharingDetails = new MnNotesSharingDetails();
				sharingDetails.setUserId(Integer.parseInt(userId));
				sharingDetails.setSharingUserId(Integer.parseInt(userId));
				sharingDetails.setSharingGroupId(0);
				sharingDetails.setListId(Integer.parseInt(listId));
				sharingDetails.setListType(listType);
				sharingDetails.setNoteId(Integer.parseInt(noteId));
				sharingDetails.setSharingLevel("individual");
				sharingDetails.setNoteLevel("edit");
				sharingDetails.setShareAllContactFlag(false);
				sharingDetails.setSharedDate(formatter.format(date));
				sharingDetails.setStatus("A");
				sharingDetails.setSharedBookId(Integer.parseInt(bookId));
			
				mongoOperations.insert(sharingDetails,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				
				return "{\"status\":\"A\"}";
			}
			
			
		} catch (Exception e) {
			logger.error("Exception while Updating Book For Note :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for updateBookForNote method : ");
		return "Exception while Updating Book For Note";
	}
	
	public List<MnNotesSharingDetails> getSharedAssociateNote(String userId,String listType, String listId)
	{
		if(logger.isDebugEnabled())
			logger.debug("getSharedAssociateNote method called: "+userId);	
		List<MnNotesSharingDetails> mnNotesSharingDetails = null;
		try
		{
			// fetch note from shared associate book notes
			Query query = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("sharingLevel").is("book").and("listId").is(Integer.parseInt(listId)).and("status").is("A"));
			MnNotesSharingDetails mnNotesSharingDetails1 = mongoOperations.findOne(query, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			
			if(mnNotesSharingDetails1!=null)
			{
			query = new Query(Criteria.where("sharingUserId").is(mnNotesSharingDetails1.getUserId()).and("sharingLevel").is("individual").and("sharedBookId").is(Integer.parseInt(listId)).and("status").is("A"));
			mnNotesSharingDetails = mongoOperations.find(query, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			}
			
		}
		catch (Exception e)
		{
			logger.error("Exception while getSharedAssociateNote Note 1  :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for getSharedAssociateNote method : ");
		return mnNotesSharingDetails;
	}

	@Override
	public List<MnNotesSharingDetails> getIndividualCopyToOwnBook(String userId,String listType, String listId)
	{
		if(logger.isDebugEnabled())
			logger.debug("getIndividualCopyToOwnBook method called: "+userId);	
		List<MnNotesSharingDetails> mnNotesSharingDetails = null;
		try
		{
			Query query = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("sharingLevel").is("individual").and("sharedBookId").is(Integer.parseInt(listId)).and("status").is("A"));
			mnNotesSharingDetails = mongoOperations.find(query, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			
		}
		catch (Exception e)
		{
			logger.error("Exception while getIndividualCopyToOwnBook Note 1  :" + e);
		}
		
		if(logger.isDebugEnabled())
			logger.debug("Return response for getIndividualCopyToOwnBook method : ");
		return mnNotesSharingDetails;
	}
	
	
	public List<MnNotesSharingDetails> getIndividualShareBookNoteWithAssociateNote(String listType,String listId , String noteId)
	{
		if(logger.isDebugEnabled())
			logger.debug("getIndividualShareBookNoteWithAssociateNote method called: "+noteId);	
		List<MnNotesSharingDetails> mnNotesSharingDetails = null;
		try
		{
			Query query = new Query(Criteria.where("sharingLevel").is("individual").and("sharedBookId").is(Integer.parseInt(listId)).and("status").is("A"));
			mnNotesSharingDetails = mongoOperations.find(query, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			
		}
		catch (Exception e)
		{
			logger.error("Exception while getIndividualShareBookNoteWithAssociateNote Note :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for getIndividualShareBookNoteWithAssociateNote method : ");
		return mnNotesSharingDetails;
	}
	@Override
	public List<MnList> getLists(List<Integer> listIds)
	{
		if(logger.isDebugEnabled())
			logger.debug("getLists method called: ");	
		List<MnList> lists = null;
		try
		{
			Query query = new Query(Criteria.where("listId").in(listIds));
			lists = mongoOperations.find(query, MnList.class, JavaMessages.Mongo.MNLIST);
		}
		catch (Exception e)
		{
			logger.error("Exception while creating Note in getLists:" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for getLists method : ");
		return lists;
	}

	@Override
	public List<Integer> getNoteIds(Integer listId, String userId, String listType,String searchingListId)
	{
		if(logger.isDebugEnabled())
			logger.debug("getNoteIds method called: "+userId);	
		List<Integer> noteIds = new ArrayList<Integer>();

		List<MnNotesSharingDetails> mnNotesSharingDetails = null;
		try
		{
			Query query = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("sharingLevel").is("individual").and("listId").is(listId).and("sharedBookId").is(Integer.parseInt(searchingListId)).and("status").is("A"));
			mnNotesSharingDetails = mongoOperations.find(query, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			if (mnNotesSharingDetails != null && !mnNotesSharingDetails.isEmpty())
			{
				for (MnNotesSharingDetails details : mnNotesSharingDetails)
				{
					noteIds.add(details.getNoteId());
				}
			}
		}
		catch (Exception e)
		{
			logger.error("Exception while creating Note in getNoteIds :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for getNoteIds method : ");
		return noteIds;
	}
	public List<Integer> getNoteIdsForBooksAssociateNotes(Integer listId, String listType,String searchingListId)
	{
		if(logger.isDebugEnabled())
			logger.debug("getNoteIdsForBooksAssociateNotes method called: searchingListId: "+searchingListId);	
		
		List<Integer> noteIds = new ArrayList<Integer>();

		List<MnNotesSharingDetails> mnNotesSharingDetails = null;
		try
		{
			Query query = new Query(Criteria.where("sharingLevel").is("individual").and("listId").is(listId).and("sharedBookId").is(Integer.parseInt(searchingListId)).and("status").is("A"));
			mnNotesSharingDetails = mongoOperations.find(query, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			if (mnNotesSharingDetails != null && !mnNotesSharingDetails.isEmpty())
			{
				for (MnNotesSharingDetails details : mnNotesSharingDetails)
				{
					noteIds.add(details.getNoteId());
				}
			}
		}
		catch (Exception e)
		{
			logger.error("Exception while getNoteIdsForBooksAssociateNotes Note :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for getNoteIdsForBooksAssociateNotes method : ");
		return noteIds;
	}
	@Override
	public String getSharedListIds(Integer listId, String userId,String noteId)
	{
		if(logger.isDebugEnabled())
			logger.debug("getSharedListIds method called: "+userId+" noteId:"+noteId);	
		List<MnNotesSharingDetails> mnNotesSharingDetails = null;
		List<Integer> listIds = new ArrayList<Integer>();
		StringBuffer buffer=new StringBuffer();
		try
		{
			Query query = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("sharingLevel").is("individual").and("listId").is(listId).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
			mnNotesSharingDetails = mongoOperations.find(query, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			if (mnNotesSharingDetails != null && !mnNotesSharingDetails.isEmpty())
			{
				for (MnNotesSharingDetails details : mnNotesSharingDetails)
				{
					buffer.append(""+details.getSharedBookId()+",");
					listIds.add(details.getSharedBookId());
				}
				buffer.deleteCharAt(buffer.lastIndexOf(","));
				
			}
		}
		catch (Exception e)
		{
			logger.error("Exception while creating Note in getSharedListIds:" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for getSharedListIds method : ");
		return buffer.toString();
	}

	@Override
	public List<MnTag> getAllTags()
	{
		if(logger.isDebugEnabled())
			logger.debug("getAllTags method called: ");	
		List<MnTag> tags = null;
		try {
			Query query = new Query();
			tags = mongoOperations.find(query, MnTag.class,JavaMessages.Mongo.MNTAGS);
			if (tags != null && !tags.isEmpty()) {

			}

		} catch (Exception e) {
			logger.error("Exception while get tag list In Impl !", e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for getAllTags method : ");
		return tags;
	}

	public List<MnNotesSharingDetails> fetchSharedNotes(String userId, String listType)
	{
		if(logger.isDebugEnabled())
			logger.debug("fetchSharedNotes method called: "+userId);	
		Query query;
		List<MnNotesSharingDetails> details=null;
		try
		{
		query = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("listType").is(listType).and("status").is("A").and("sharingLevel").ne("individual"));
		details = mongoOperations.find(query, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
		}catch(Exception e){
			logger.error("Exception occured in fetchSharedNotes "+e);	
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for fetchSharedNotes method : ");
		return details;
	}
	
	public List<Integer> getOwnerTagDetails(Integer userId, String listId,String noteId)
	{
		if(logger.isDebugEnabled())
			logger.debug("getOwnerTagDetails method called: "+userId+"noteId: "+noteId);	
		Query query;
		List<MnTagDetails> details=null;
		List<Integer> tagIds=new ArrayList<Integer>();
		
		try
		{
		query = new Query(Criteria.where("userId").is(userId).and("noteId").is(Integer.parseInt(noteId)).and("listId").is(Integer.parseInt(listId)).and("status").is("A"));
		details= mongoOperations.find(query, MnTagDetails.class,JavaMessages.Mongo.MNTAGDETAILS);
		if(details!=null && !details.isEmpty()){
			for(MnTagDetails tagDetails:details){
				tagIds.add(tagDetails.getTagId());
			}
		}
		}catch(Exception e){
			
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for getOwnerTagDetails method : ");
		return tagIds;
	}
	public List<Integer> getNoteSharedMemberListByIndGrpAll(String tempNoteDetails,String userId,MnList mnList){
		if(logger.isDebugEnabled())
			logger.debug("getNoteSharedMemberListByIndGrpAll method called: "+userId);	
		List<Integer> sharedUserIdSet = new ArrayList<Integer>();
		boolean stopFrdFlag = false;
		try{
			MnUsers mnUsers = getUserDetailsObject(Integer.parseInt(userId));
			sharedUserIdSet.add(Integer.parseInt(userId));
			String oldMember = notesSharingDetailsConvertToJsonObject(tempNoteDetails);
			if (oldMember != null && !oldMember.trim().equals("")) {
				String[] memberIds = oldMember.split(",");
				if (memberIds.length != 0) {
					for (int i = 0; i < memberIds.length; i++) {
						if (!memberIds[i].equals(userId)) {
							sharedUserIdSet.add(Integer.parseInt(memberIds[i]));
						}
					}
				}
			}
			if (!mnList.getUserId().equals(Integer.parseInt(userId))) {
				sharedUserIdSet.add(mnList.getUserId());
			}
			String groupId = null;
			if (tempNoteDetails.contains("\"noteGroups\"")) {
				if(!tempNoteDetails.contains("\"noteGroups\":\"\""))
					groupId = noteGroupsUpdateConvertToJsonObject(tempNoteDetails,false);
			} else if (tempNoteDetails.contains("\"eventGroups\"")){
				if(!tempNoteDetails.contains("\"eventGroups\":\"\""))
					groupId = eventGroupsUpdateConvertToJsonObject(tempNoteDetails,false);
			}
			if(groupId!= null && !groupId.equals("")){
				groupId = groupId.replace("[","");
				groupId = groupId.replace("]","");
				String[] grpIds = null;
				if(groupId.trim()!= ""){
					grpIds = groupId.split(",");
				}
				List<Integer> grpIdList = new ArrayList<Integer>();
				if(grpIds.length >0 && !grpIds[0].equals("")){
					for(String grpId:grpIds){
						grpIdList.add(Integer.parseInt(grpId.trim()));
					}
					Query query2 = new Query(Criteria.where("groupId").in(grpIdList).and("status").is("A"));
					List<MnGroupDetailsDomain> detailsDomain = mongoOperations.find(query2, MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);
					if (detailsDomain != null && !detailsDomain.isEmpty()) {
						for (MnGroupDetailsDomain domain : detailsDomain) {
							if(!sharedUserIdSet.contains(domain.getUserId())){
								sharedUserIdSet.add(domain.getUserId());
							}
						}
					}
				}
			}
			if (tempNoteDetails.contains("\"noteSharedAllContact\":1")) {
				stopFrdFlag = true;
			}else if (tempNoteDetails.contains("\"eventSharedAllContact\":1")){
				stopFrdFlag = true;
			}
			if(stopFrdFlag){
				if(mnUsers.getFriends()!= null && !mnUsers.getFriends().isEmpty()){
					for(Integer userIds :mnUsers.getFriends()){
						if(!sharedUserIdSet.contains(userIds)){
							sharedUserIdSet.add(userIds);
						}
					}
				}
			}
		}catch (Exception e) {
			logger.error("Exception while get Note Shared Member List By Ind Grp AllCon",e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for getNoteSharedMemberListByIndGrpAll method : ");
		return sharedUserIdSet;
	}

	@Override
	public MnList createBook(MnList mnList) {
	Integer bookId = 0;
	Query query = null;
	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	try {
		if(logger.isDebugEnabled())
			logger.debug("createBook method called: ");	
		query = new Query();
		query.sort().on("listId", Order.ASCENDING);
		List<MnList> mnLists = mongoOperations.find(query, MnList.class,
				JavaMessages.Mongo.MNLIST);

		if (mnLists != null && mnLists.size() != 0) {
			MnList lastList = mnLists.get(mnLists.size() - 1);
			bookId = lastList.getListId() + 1;

		} else {
			bookId = 1;

		}
		mnList.setListId(bookId);
		mongoOperations.insert(mnList, JavaMessages.Mongo.MNLIST);
	
		if(logger.isDebugEnabled())
			logger.debug("Return response for createBook method : ");
	return mnList;
} catch (Exception e) {
	logger.error("Exception in createBook method in Impl :"+e.getMessage());
	return null;
}
	}

	@Override
	public List<MnList> fetchEmptyList(String userId, String listType)
	{ 
		if(logger.isDebugEnabled())
			logger.debug("fetchEmptyList method called: "+userId);	
		List<MnList> mnListList = null;
		Query query = null;
		
		try {
			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("listType").is(listType).and("status").is("A").and("mnNotesDetails").size(0));

			mnListList = mongoOperations.find(query, MnList.class,JavaMessages.Mongo.MNLIST);
			
		} catch (Exception e) {
			logger.error("Exception while fetching List :" + e);

		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for fetchEmptyList method : ");
		return mnListList;
	}
	@Override
	public List<MnList> fetchScheduleList(String userId, String listType) {
		if(logger.isDebugEnabled())
			logger.debug("fetchScheduleList method called: "+userId);	
		List<MnList> mnListList = null;
		Query query = null;
		
		try {
			
			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("listType").is(listType).and("status").is("A"));

			mnListList = mongoOperations.find(query, MnList.class,JavaMessages.Mongo.MNLIST);

		} catch (Exception e) {
			logger.error("Exception while fetching List :" + e);

		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for fetchScheduleList method : ");
		return mnListList;
	}
	@Override
	public List<MnList> fetchSharedNotesBasedOnScheduleList(String userId,String listType) {
		if(logger.isDebugEnabled())
			logger.debug("fetchSharedNotesBasedOnScheduleList method called: "+userId);	
		List<MnList> mnListList = null;
		List<MnNotesSharingDetails> details = null;
		Query query = null;
		try {
			query = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("listType").is(listType).and("status").is("A").and("sharingGroupId").is(0).and("sharingLevel").ne("individual"));
			query.sort().on("listId", Order.ASCENDING);
			details = mongoOperations.find(query, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);

			List<MnNotesSharingDetails> associateNoteForBook = new ArrayList<MnNotesSharingDetails>();
			if (details != null && !details.isEmpty()) {
				List<Integer> listIds = new ArrayList<Integer>();
				List<Integer> bookLevelSharedlistIds = new ArrayList<Integer>();

				for (MnNotesSharingDetails sharingDetails : details) {

					if (!listIds.contains(sharingDetails.getListId())){
						if(sharingDetails.getNoteId().equals(0)){
							if (!bookLevelSharedlistIds.contains(sharingDetails.getListId())){
								bookLevelSharedlistIds.add(sharingDetails.getListId());
								listIds.add(sharingDetails.getListId());
								
								//fetch the book level sharing , associate note for the perticular book
								Query query12 = new Query(Criteria.where("sharingLevel").is("individual").and("sharedBookId").is(sharingDetails.getListId()).and("status").is("A"));
								List<MnNotesSharingDetails> mnSharedList = mongoOperations.find(query12, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
								if(mnSharedList!= null && !mnSharedList.isEmpty()){
									for(MnNotesSharingDetails associateSharing :mnSharedList){
										if(!listIds.contains(associateSharing.getListId())){
											listIds.add(associateSharing.getListId());
										}
										query12 = new Query(Criteria.where("listType").is(listType).and("status").is("A").and("sharingGroupId").is(0)
												.and("sharingLevel").is("individual").and("sharedBookId").is(sharingDetails.getListId()).and("listId").is(associateSharing.getListId()).and("noteId").is(associateSharing.getNoteId()));
										MnNotesSharingDetails associateNoteObj = mongoOperations.findOne(query12, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
										if(associateNoteObj!= null ){
											associateNoteForBook.add(associateNoteObj);
										}
									}
								}
							}
						}else{
							listIds.add(sharingDetails.getListId());
						}
					}

				}
				//
				if(!associateNoteForBook.isEmpty()){
					details.addAll(associateNoteForBook);
				}
				
				query = new Query(Criteria.where("listId").in(listIds));

				mnListList = mongoOperations.find(query, MnList.class,JavaMessages.Mongo.MNLIST);
				List<MnList> removedUnwantedNotesList = new ArrayList<MnList>();
				if (mnListList != null && !mnListList.isEmpty()) {
					for (Integer ids : listIds) {
						List<String> noteIds = new ArrayList<String>();
						if(!bookLevelSharedlistIds.contains(ids)){
							for (MnNotesSharingDetails sharingDetails : details) {
								if (sharingDetails.getListId().equals(ids)) {
									if(!noteIds.contains(sharingDetails.getNoteId().toString()))
										noteIds.add(sharingDetails.getNoteId().toString());
								}
							}
						}else{
							noteIds.add("0");
						}
						for (MnList list : mnListList) {
							if (list.getListId().equals(ids)) {
								Set<String> orginalNotes = list.getMnNotesDetails();
								Set<String> sharedNotes = new HashSet<String>();
								for (String str : orginalNotes) {
									for (String noteStr : noteIds) {
										if (listType.equalsIgnoreCase("schedule")) {
											Query query1;
											if(noteStr.equals("0"))
												query1= new Query(Criteria.where("listId").is(ids).and("status").is("A").and("sharingStatus").in("A", "P").and("sharingUserId").is(Integer.parseInt(userId)));
											else
												query1 = new Query(Criteria.where("listId").is(ids).and("noteId").is(Integer.parseInt(noteStr)).and("status").is("A").and("sharingStatus").in("A", "P").and("sharingUserId").is(Integer.parseInt(userId)));
											
											MnEventsSharingDetails shareddet = mongoOperations.findOne(query1,MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
											if (shareddet != null) {
												if (str.contains("\"eventId\":\""+ noteStr+ "\"")&& str.contains("\"status\":\"A\"")){
													
													if(str.contains("\"noteSharedAllContact\":0"))
														str=str.replace("\"shareType\":\"\"", "\"shareType\":\"single\"");
													else if(str.contains("\"noteSharedAllContact\":1"))
														str=str.replace("\"shareType\":\"\"", "\"shareType\":\"allCon\"");
													
													sharedNotes.add(str);
												}else if(noteStr.equals("0")){
													
													if(str.contains("\"noteSharedAllContact\":0"))
														str=str.replace("\"shareType\":\"\"", "\"shareType\":\"single\"");
													else if(str.contains("\"noteSharedAllContact\":1"))
														str=str.replace("\"shareType\":\"\"", "\"shareType\":\"allCon\"");
													
													sharedNotes.add(str);
												}
											}
										} else {
											if (str.contains("\"noteId\":\""+ noteStr + "\"")&& str.contains("\"status\":\"A\"")){
												
												if(str.contains("\"noteSharedAllContact\":0"))
													str=str.replace("\"shareType\":\"\"", "\"shareType\":\"single\"");
												else if(str.contains("\"noteSharedAllContact\":1"))
													str=str.replace("\"shareType\":\"\"", "\"shareType\":\"allCon\"");
												
												sharedNotes.add(str);
											}else if(noteStr.equals("0")){
												
												if(str.contains("\"noteSharedAllContact\":0"))
													str=str.replace("\"shareType\":\"\"", "\"shareType\":\"single\"");
												else if(str.contains("\"noteSharedAllContact\":1"))
													str=str.replace("\"shareType\":\"\"", "\"shareType\":\"allCon\"");
												
												sharedNotes.add(str);
											}
										}
									}
								}
								if (!sharedNotes.isEmpty()) {
									list.setMnNotesDetails(sharedNotes);
									removedUnwantedNotesList.add(list);
								}
							}
						}
					}
					return removedUnwantedNotesList;
				}

			}
		} catch (Exception e) {
			logger.error("Exception while Fetching Shared Notes Based On List :"+ e);

		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for fetchSharedNotesBasedOnScheduleList method : ");
		return mnListList;
	}
	@Override
	public List<MnList> fetchGroupSharedNotesBasedOnScheduleList(String userId,String listType) {
		if(logger.isDebugEnabled())
			logger.debug("fetchGroupSharedNotesBasedOnScheduleList method called: "+userId);	
		List<MnList> mnListList = null;
		List<MnNotesSharingDetails> details = null;
		Query query = null;
		try {
			query = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("listType").is(listType).and("status").is("A").and("sharingGroupId").ne(0).and("sharingLevel").ne("individual"));
			query.sort().on("listId", Order.ASCENDING);
			details = mongoOperations.find(query, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);

			List<MnNotesSharingDetails> associateNoteForBook = new ArrayList<MnNotesSharingDetails>();
			if (details != null && !details.isEmpty()) {
				List<Integer> listIds = new ArrayList<Integer>();
				List<Integer> bookLevelSharedlistIds = new ArrayList<Integer>();

				for (MnNotesSharingDetails sharingDetails : details) {

					if (!listIds.contains(sharingDetails.getListId())){
						if(sharingDetails.getNoteId().equals(0)){
							if (!bookLevelSharedlistIds.contains(sharingDetails.getListId())){
								bookLevelSharedlistIds.add(sharingDetails.getListId());
								listIds.add(sharingDetails.getListId());
								
								//fetch the book level sharing , associate note for the perticular book
								Query query12 = new Query(Criteria.where("sharingLevel").is("individual").and("sharedBookId").is(sharingDetails.getListId()).and("status").is("A"));
								List<MnNotesSharingDetails> mnSharedList = mongoOperations.find(query12, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
								if(mnSharedList!= null && !mnSharedList.isEmpty()){
									for(MnNotesSharingDetails associateSharing :mnSharedList){
										if(!listIds.contains(associateSharing.getListId())){
											listIds.add(associateSharing.getListId());
										}
										query12 = new Query(Criteria.where("listType").is(listType).and("status").is("A").and("sharingGroupId").is(0)
												.and("sharingLevel").is("individual").and("sharedBookId").is(sharingDetails.getListId()).and("listId").is(associateSharing.getListId()).and("noteId").is(associateSharing.getNoteId()));
										MnNotesSharingDetails associateNoteObj = mongoOperations.findOne(query12, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
										if(associateNoteObj!= null ){
											associateNoteForBook.add(associateNoteObj);
										}
									}
								}
							}
						}else{
							listIds.add(sharingDetails.getListId());
						}
					}

				}
				//
				if(!associateNoteForBook.isEmpty()){
					details.addAll(associateNoteForBook);
				}
				query = new Query(Criteria.where("listId").in(listIds));

				mnListList = mongoOperations.find(query, MnList.class,JavaMessages.Mongo.MNLIST);
				List<MnList> removedUnwantedNotesList = new ArrayList<MnList>();
				if (mnListList != null && !mnListList.isEmpty()) {
					for (Integer ids : listIds) {
						List<String> noteIds = new ArrayList<String>();
						if(!bookLevelSharedlistIds.contains(ids)){
							for (MnNotesSharingDetails sharingDetails : details) {
								if (sharingDetails.getListId() == ids) {
									if(!noteIds.contains(sharingDetails.getNoteId().toString()))
										noteIds.add(sharingDetails.getNoteId().toString());
								}
							}
						}else{
							noteIds.add("0");
						}
						for (MnList list : mnListList) {
							if (list.getListId() == ids) {
								Set<String> orginalNotes = list.getMnNotesDetails();
								Set<String> sharedNotes = new HashSet<String>();
								for (String str : orginalNotes) {
									for (String noteStr : noteIds) {
										if (listType.equalsIgnoreCase("schedule")) {
											Query query1;
											if(noteStr.equals("0"))
												query1= new Query(Criteria.where("listId").is(ids).and("status").is("A").and("sharingStatus").in("A", "P").and("sharingUserId").is(Integer.parseInt(userId)));
											else
												query1 = new Query(Criteria.where("listId").is(ids).and("noteId").is(Integer.parseInt(noteStr)).and("status").is("A").and("sharingStatus").in("A", "P").and("sharingUserId").is(Integer.parseInt(userId)));
											
											MnEventsSharingDetails shareddet = mongoOperations.findOne(query1,MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
											if (shareddet != null) {
												if (str.contains("\"eventId\":\""+ noteStr+ "\"")&& str.contains("\"status\":\"A\"")){
													
													if(str.contains("\"noteSharedAllContact\":0"))
														str=str.replace("\"shareType\":\"\"", "\"shareType\":\"single\"");
													else if(str.contains("\"noteSharedAllContact\":1"))
														str=str.replace("\"shareType\":\"\"", "\"shareType\":\"allCon\"");
													
													sharedNotes.add(str);
												}else if(noteStr.equals("0")){
													
													if(str.contains("\"noteSharedAllContact\":0"))
														str=str.replace("\"shareType\":\"\"", "\"shareType\":\"single\"");
													else if(str.contains("\"noteSharedAllContact\":1"))
														str=str.replace("\"shareType\":\"\"", "\"shareType\":\"allCon\"");
													
													sharedNotes.add(str);
												}
											}
										} else {
											if (str.contains("\"noteId\":\""+ noteStr + "\"")&& str.contains("\"status\":\"A\"")){
												
												if(str.contains("\"noteSharedAllContact\":0"))
													str=str.replace("\"shareType\":\"\"", "\"shareType\":\"single\"");
												else if(str.contains("\"noteSharedAllContact\":1"))
													str=str.replace("\"shareType\":\"\"", "\"shareType\":\"allCon\"");
												
												sharedNotes.add(str);
											}else if(noteStr.equals("0")){
												
												if(str.contains("\"noteSharedAllContact\":0"))
													str=str.replace("\"shareType\":\"\"", "\"shareType\":\"single\"");
												else if(str.contains("\"noteSharedAllContact\":1"))
													str=str.replace("\"shareType\":\"\"", "\"shareType\":\"allCon\"");
												
												sharedNotes.add(str);
											}
										}
									}
								}
								if (!sharedNotes.isEmpty()) {
									list.setMnNotesDetails(sharedNotes);
									removedUnwantedNotesList.add(list);
								}
							}
						}
					}
					return removedUnwantedNotesList;
				}

			}
		} catch (Exception e) {
			logger.error("Exception while Fetching Shared Notes Based On List :"+ e);

		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for fetchGroupSharedNotesBasedOnScheduleList method : ");
		return mnListList;
	}
	
	public String convertedZoneEvents(Integer userId, Integer ownerId,String date) {
		if(logger.isDebugEnabled())
			logger.debug("convertedZoneEvents method called: ");	
		String convertedDate=null;
		SimpleDateFormat formatter = new SimpleDateFormat("MM/d/yyyy HH:mm");
		try
		{
			Query query2 = new Query(Criteria.where("userId").is(userId));
   			MnUsers mnUser = mongoOperations.findOne(query2, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			String toZone=mnUser.getTimeZone();
			
			Query query3 = new Query(Criteria.where("userId").is(ownerId));
   			MnUsers mnUser1 = mongoOperations.findOne(query3, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			String fromZone=mnUser1.getTimeZone();
   			
			Date d = formatter.parse(date);
		
		long lngDate=d.getTime(); 
		String fromTimeZone="GMT"+fromZone;
        String toTimeZone="GMT"+toZone;
        
	    TimeZone toTZ = TimeZone.getTimeZone(toTimeZone);
	    Calendar toCal = Calendar.getInstance(toTZ);        

	    TimeZone fromTZ = TimeZone.getTimeZone(fromTimeZone);
	    Calendar fromCal = Calendar.getInstance(fromTZ);
	    fromCal.setTimeInMillis(lngDate);
	    
	    toCal.setTimeInMillis(fromCal.getTimeInMillis()
	            + toTZ.getOffset(fromCal.getTimeInMillis())
	            - TimeZone.getTimeZone(fromTimeZone).getOffset(fromCal.getTimeInMillis())); 
	    
	    d=toCal.getTime();
	    convertedDate=formatter.format(d);
		}catch (Exception e) {
			logger.error("Exception occured in convertedZoneEvents "+e);
			
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for convertedZoneEvents method : ");
		return convertedDate;
	}
	
	public String complaintReportMail(MnComplaints report){
		MnUsers userDetail;
		String status="";
		Integer compliantId=1;
		if(logger.isDebugEnabled())
			logger.debug("while user complaint for complaintReportMail method:");
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
			
			try{
				if(report!=null){
					Query query1 = new Query(Criteria.where("userId").is(report.getComplaintUserId()).and("status").is("A"));
					userDetail = mongoOperations.findOne(query1, MnUsers.class, JavaMessages.Mongo.MNUSERS);
					
					Query query=new Query();
					query.sort().on("_id", Order.ASCENDING);
					List<MnComplaints> listCompliant = mongoOperations.find(query,MnComplaints.class,JavaMessages.Mongo.MNCOMPLAINTS);
					if (listCompliant != null && listCompliant.size() != 0)
					{
						for (MnComplaints lastUser : listCompliant)
						{
							if (compliantId <= lastUser.getCompliantId()) 
								compliantId = lastUser.getCompliantId() + 1;
						}
					}
					else
					{
						compliantId = 1;
					}
					
					report.setCompliantId(compliantId);
					report.setUserName(userDetail.getUserName());
					report.setUserFirstName(userDetail.getUserFirstName());
					report.setUserLastName(userDetail.getUserLastName());
					report.setStatus(userDetail.getStatus());
					report.setUserMailId(userDetail.getEmailId());
					report.setNoteId(report.getNoteId().trim());
					report.setDate(format.format(date));
					mongoOperations.insert(report,JavaMessages.Mongo.MNCOMPLAINTS);
					status= "success";
				}
			}catch(Exception e){
				status= "error";
				logger.error("Exception while inserting complaints details :" + e);
			}
			if(logger.isDebugEnabled())
			logger.debug("Return response for complaintReportMail method : ");
			return status;
		
		
		
		
		
	}
	@Override
	public String getUserName(String userId) {
		MnUsers userDetail;
		String status="";
		if(logger.isDebugEnabled())
			logger.debug("Get userNAme method called:");
			Date date = new Date();
			try{
					Query query1 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
					userDetail = mongoOperations.findOne(query1, MnUsers.class, JavaMessages.Mongo.MNUSERS);
					if(userDetail!=null){
						status=userDetail.getUserName();
					}
				
			}catch(Exception e){
				status= "error";
				logger.error("Exception while getting userName :" + e);
			}
			if(logger.isDebugEnabled())
			logger.debug("Return response for getting userName method : ");
			return status;
		
		
		
		
	}
	@Override
	public String getUserMailId(String userId) {
		MnUsers userDetail;
		String status="";
		if(logger.isDebugEnabled())
			logger.debug("Get userNAme method called:");
			Date date = new Date();
			try{
					Query query1 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
					userDetail = mongoOperations.findOne(query1, MnUsers.class, JavaMessages.Mongo.MNUSERS);
					if(userDetail!=null){
						status=userDetail.getEmailId();
					}
				
			}catch(Exception e){
				status= "error";
				logger.error("Exception while getting userName :" + e);
			}
			if(logger.isDebugEnabled())
			logger.debug("Return response for getting userName method : ");
			return status;
		
		
		
		
	}
	@Override
	public List<String> getCommentsListForMail(String listId, String noteId,
			String userId,String reportLevel,String commentId) {
		List <MnComments> comment=null;
		MnComments mCommnent;
		List<String>commentList=new ArrayList<String>();
		if(logger.isDebugEnabled())
			logger.debug("getCommentsListForMail method called:");
			try{
				if(reportLevel.equals("note")){
					Query query1 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("listId").is(listId).and("noteId").is(noteId.trim()));
					comment = mongoOperations.find(query1, MnComments.class, JavaMessages.Mongo.MNCOMMETS);
				}else{
					Query query2 = new Query(Criteria.where("cId").is(Integer.parseInt(commentId)));
					comment = mongoOperations.find(query2, MnComments.class, JavaMessages.Mongo.MNCOMMETS);
					
				}
					if(comment!=null && !comment.isEmpty()){
					for(MnComments comments1:comment){
						commentList.add(comments1.getComments());
					}
					}
				
			}catch(Exception e){
				logger.error("Exception while getCommentsListForMail :" + e);
			}
			if(logger.isDebugEnabled())
			logger.debug("Return response for getCommentsListForMail method : ");
			return commentList;
		
	}
	@Override
	public String getSharedByUserName(String ownerUserId,String listId,String noteId) {
		MnUsers userDetail;
		String publicUser="";
		String sharedByUserName="";
		if(logger.isDebugEnabled())
			logger.debug("Get SharedByUserName method called");
			Date date = new Date();
			try{
				Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("noteId").is((Integer.parseInt(noteId.trim()))).and("userId").is(Integer.parseInt(ownerUserId)).and("status").is("A"));
				MnNoteDetails noteDetails= mongoOperations.findOne(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
					if(noteDetails!=null){
						publicUser=noteDetails.getPublicUser();
					}
					if(publicUser!=null){
						Query query1 = new Query(Criteria.where("userId").is(Integer.parseInt(publicUser)));
						userDetail = mongoOperations.findOne(query1, MnUsers.class, JavaMessages.Mongo.MNUSERS);
						if(userDetail!=null){
							sharedByUserName=userDetail.getUserName();
						}
					}
				
			}catch(Exception e){
				sharedByUserName= "error";
				logger.error("Exception while getting SharedByUserName :" + e);
			}
			return sharedByUserName;
	}
}
