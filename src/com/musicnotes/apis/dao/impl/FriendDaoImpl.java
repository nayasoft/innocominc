package com.musicnotes.apis.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.musicnotes.apis.dao.interfaces.IFriendDao;
import com.musicnotes.apis.domain.MnAttachmentDetails;
import com.musicnotes.apis.domain.MnComments;
import com.musicnotes.apis.domain.MnEventDetails;
import com.musicnotes.apis.domain.MnEventsSharingDetails;
import com.musicnotes.apis.domain.MnFriends;
import com.musicnotes.apis.domain.MnInvitedUsers;
import com.musicnotes.apis.domain.MnList;
import com.musicnotes.apis.domain.MnMailNotificationList;
import com.musicnotes.apis.domain.MnNonUserSharingDetails;
import com.musicnotes.apis.domain.MnNoteDetails;
import com.musicnotes.apis.domain.MnNotesSharingDetails;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.MailContent;
import com.musicnotes.apis.util.SendMail;

public class FriendDaoImpl extends BaseDaoImpl implements IFriendDao
{
	Logger logger = Logger.getLogger(FriendDaoImpl.class);

	@Override
	public String addFriendRequest(MnFriends mnFriends)
	{
		Boolean mailNotify=false;
		if(logger.isDebugEnabled())
		logger.debug("addFriendRequest method called");
		mongoOperations.insert(mnFriends, JavaMessages.Mongo.MNFRIEND);
		
		MnUsers mnUser = getUserDetailsObject(mnFriends.getUserId());
		MnUsers users = getUserDetailsObject(mnFriends.getRequestedUserId());
		writeLog(mnFriends.getRequestedUserId(), "A", "Friend request sent to "+mnUser.getUserFirstName()+" "+mnUser.getUserLastName(), "contact", "","", null, "contact");
		
				try{
					mailNotify=getUserMailNotification(mnUser.getUserId(),"contact");
					if(mnUser.getNotificationFlag().equalsIgnoreCase("yes"))
					{
						if(mailNotify){
						SendMail sendMail=new SendMail(); 	
						String recipients[]={mnUser.getEmailId()};
						String message=MailContent.sharingNotification+mnUser.getUserFirstName()+MailContent.sharingNotification1+users.getUserFirstName()+" "+users.getUserLastName()+" has sent a request to add you as a contact in Musicnote. To accept their request and add them as one of your contacts please log in to your profile.."+MailContent.sharingNotification2;;
						String subject=users.getUserFirstName()+" wants to add you as a contact in Musicnote";
						sendMail.postEmail(recipients, subject,message);
					
						}
					}
				}catch(Exception e){
					logger.error("Exception in addFriendRequest method:"+e);
				}
				if(logger.isDebugEnabled())
				logger.debug("addFriendRequest method successfully returned");
		return "success";
	}

	@Override
	public List<MnFriends> checkFriendRequest(MnFriends mnFriends)
	{
		if(logger.isDebugEnabled())
		logger.debug("checkFriendRequest method called");
		List<MnFriends> friends = null;
		Query query = null;
		try
		{
			query = new Query(Criteria.where("userId").is(mnFriends.getUserId()).and("status").is(JavaMessages.requestStatus));
			friends = mongoOperations.find(query, MnFriends.class, JavaMessages.Mongo.MNFRIEND);
		}
		catch (Exception e)
		{
			logger.error("Exception in checkFriendRequest method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("checkFriendRequest method successfully returned");
		return friends;
	}

	@Override
	public String acceptFriendRequest(MnFriends mnFriends)
	{
		if(logger.isDebugEnabled())
		logger.debug("acceptFriendRequest method called");
		try
		{

			Query query = new Query(Criteria.where("userId").is((mnFriends.getUserId())).and("requestedUserId").is(mnFriends.getRequestedUserId()).and("status").is(JavaMessages.requestStatus));

			Update update = new Update();

			update.set("status", JavaMessages.acceptStatus);
			update.set("acceptedDate", new Date());

			mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNFRIEND);
			
			MnUsers mnUser = getUserDetailsObject(mnFriends.getUserId());
			MnUsers users = getUserDetailsObject(mnFriends.getRequestedUserId());
			if(users.getUserFirstName()!=null && !users.getUserFirstName().isEmpty() ){
				writeLog(mnFriends.getUserId(), "A", "Added new contact: <B>"+users.getUserFirstName()+" "+users.getUserLastName()+"</B>", "contact", "","", null, "contact");
			}else{
				writeLog(mnFriends.getUserId(), "A", "Added new contact: <B>"+users.getUserName()+"</B>", "contact", "","", null, "contact");
			}
			if(mnUser.getFollowers()!=null && !mnUser.getFollowers().isEmpty() ){
				if(mnUser.getFollowers().contains(mnFriends.getRequestedUserId()) ){
					mnUser.getFollowers().remove(mnFriends.getRequestedUserId());
				}
				if(mnUser.getFollowers().contains(mnFriends.getUserId()) ){
					mnUser.getFollowers().remove(mnFriends.getUserId());
				}
			}
			
			Set<Integer> notUserIdSet= new HashSet<Integer>();
			notUserIdSet.add(mnFriends.getRequestedUserId());
			
			if(notUserIdSet!=null && !notUserIdSet.isEmpty() ){
				if(mnUser.getUserFirstName()!=null && !mnUser.getUserFirstName().isEmpty() ){
					writeLog(mnFriends.getUserId(), "N", "<B>"+mnUser.getUserFirstName()+" "+mnUser.getUserLastName()+"</B> added you back ", "contact", "","",notUserIdSet , "contact");
				}else{
					writeLog(mnFriends.getUserId(), "N", "<B>"+mnUser.getUserName()+"</B> added you back ", "contact", "","",notUserIdSet , "contact");
				}
			}
			
		}
		catch (Exception e)
		{
			logger.error("Exception in acceptFriendRequest method:"+e);
			return "false";
		}
		if(logger.isDebugEnabled())
		logger.debug("acceptFriendRequest method successfully returned");
		return "success";
	}

	@Override
	public String declineFriendRequest(MnFriends mnFriends)
	{
		if(logger.isDebugEnabled())
		logger.debug("declineFriendRequest method called");
		try
		{

			Query query = new Query(Criteria.where("userId").is((mnFriends.getUserId())).and("requestedUserId").is(mnFriends.getRequestedUserId()).and("status").is(JavaMessages.requestStatus));

			Update update = new Update();

			update.set("status", JavaMessages.declineStatus);
			update.set("declineDate", new Date());

			mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNFRIEND);
			
			MnUsers users = getUserDetailsObject(mnFriends.getRequestedUserId());
			if(users.getUserFirstName()!=null && !users.getUserFirstName().isEmpty() ){
				writeLog(mnFriends.getUserId(), "A", "Declined friend request from "+users.getUserFirstName()+" "+users.getUserLastName()+"", "contact", "","", null, "contact");
			}else{
				writeLog(mnFriends.getUserId(), "A", "Declined friend request from "+users.getUserName()+"", "contact", "","", null, "contact");
			}
		}
		catch (Exception e)
		{
			logger.error("Exception in declineFriendRequest method:"+e);
			return "false";
		}
		if(logger.isDebugEnabled())
		logger.debug("declineFriendRequest method successfully returned");
		return "success";
	}

	@Override
	public String addToFriendsList(Integer userId, Integer requestedId)
	{
		if(logger.isDebugEnabled())
		logger.debug("addToFriendsList method called "+userId+" requestedId: "+requestedId);
		try
		{
			Query query = new Query(Criteria.where("userId").is((userId)));

			Update update = new Update();

			update.addToSet("friends", requestedId);

			mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNUSERS);
		}
		catch (Exception e)
		{
			logger.error("Exception in addToFriendsList method:"+e);
			return "false";
		}
		if(logger.isDebugEnabled())
		logger.debug("addToFriendsList method successfully returned");
		return "success";
	}
	
	@Override
	public MnUsers checkAlreadyFriends(MnFriends mnFriends){
		if(logger.isDebugEnabled())
		logger.debug("checkAlreadyFriends method called");
		MnUsers users = null;
		Query query = null;
		try
		{
			query = new Query(Criteria.where("userId").is(mnFriends.getUserId()));
			users = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
		}
		catch (Exception e)
		{
			logger.error("Exception in checkAlreadyFriends method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("checkAlreadyFriends method successfully returned");
		return users;
	
	}
	
	@Override
	public List<MnFriends> checkAlreadyFriendRequested(Integer userId,Integer requestedId){
		if(logger.isDebugEnabled())
		logger.debug("checkAlreadyFriendRequested method called"+userId+"requestedId: "+requestedId);
		List<MnFriends> friends = null;
		Query query = null;
		try
		{
			query = new Query(Criteria.where("userId").is(userId).and("status").is(JavaMessages.requestStatus).and("requestedUserId").is(requestedId));
			friends = mongoOperations.find(query, MnFriends.class, JavaMessages.Mongo.MNFRIEND);
		}
		catch (Exception e)
		{
			logger.error("Exception in checkAlreadyFriendRequested method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("checkAlreadyFriendRequested method successfully returned");
		return friends;
	
	}

	public String addFollowers(MnFriends friends){
		if(logger.isDebugEnabled())
		logger.debug("addFollowers method called");
		try
		{
			Query query = new Query(Criteria.where("userId").is((friends.getUserId())));

			Update update = new Update();

			update.addToSet("followers", friends.getRequestedUserId());

			mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNUSERS);
			
			MnUsers mnUser = getUserDetailsObject(friends.getUserId());
			MnUsers users = getUserDetailsObject(friends.getRequestedUserId());
			writeLog(friends.getRequestedUserId(), "A", "Subscribed with "+mnUser.getUserFirstName()+" "+mnUser.getUserLastName()+"", "contact", "","", null, "contact");
			
			if(users.getFollowers()!=null && !users.getFollowers().isEmpty() ){
				if(users.getFollowers().contains(friends.getUserId())){
					users.getFollowers().remove(friends.getUserId());
				}
			}
			Set<Integer> followUserSet= new HashSet<Integer>();
			followUserSet.add(friends.getUserId()); 
			
			writeLog(friends.getRequestedUserId(), "N", "<B>"+users.getUserFirstName()+" "+users.getUserLastName()+"</B> started following you", "contact", "","",followUserSet, "contact");
		}
		catch (Exception e)
		{
			logger.error("Exception in addFollowers method:"+e);
			return "false";
		}
		if(logger.isDebugEnabled())
		logger.debug("addFollowers method successfully returned");
		return "success";
	}
	
	public String unFollowers(MnUsers mnUsers,MnFriends friends){
		if(logger.isDebugEnabled())
		logger.debug("unFollowers method called");
		try
		{
			Query query = new Query(Criteria.where("userId").is((mnUsers.getUserId())));

			Update update = new Update();

			update.set("followers", mnUsers.getFollowers());

			mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNUSERS);
			
			MnUsers users = getUserDetailsObject(friends.getRequestedUserId());
			writeLog(friends.getUserId(), "A", "Unsubscribed "+users.getUserFirstName()+" "+users.getUserLastName()+"", "contact", "","", null, "contact");
			
		}
		catch (Exception e)
		{
			logger.error("Exception in unFollowers method:"+e);
			return "false";
		}
		if(logger.isDebugEnabled())
		logger.debug("unFollowers method successfully returned");
		return "success";
	}

	@Override
	public List<MnUsers> getFriendsList(MnUsers mnUsers)
	{
		if(logger.isDebugEnabled())
		logger.debug("getFriendsList method called");
		List<MnUsers> users = null;
		Query query = null;
		try
		{
			query = new Query(Criteria.where("userId").in(mnUsers.getFriends()));
			users = mongoOperations.find(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
		}
		catch (Exception e)
		{
			logger.error("Exception in getFriendsList method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("getFriendsList method successfully returned");
		return users;
	
	}

	@Override
	public String getUserDetails(Integer userId)
	{
		if(logger.isDebugEnabled())
		logger.debug("getUserDetails method called "+userId);
		String userName=null;
		MnUsers users = null;
		Query query = null;
		try
		{
			query = new Query(Criteria.where("userId").is(userId));
			users = mongoOperations.findOne(query, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			if(users!=null){
				userName=users.getUserFirstName()+" "+users.getUserLastName();	
			}
		}
		catch (Exception e)
		{
			logger.error("Exception in getUserDetails method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("getUserDetails method successfully returned");
		return userName;
	
	}

	public String acceptDeclineSharedNoteByMail(Integer userId,String acceptDecline,Integer friendUserId,boolean finalflag,String level,String noteId,String listIdd){
		if(logger.isDebugEnabled())
		logger.debug("acceptDeclineSharedNoteByMail method called "+userId+" friendUserId:"+friendUserId);
		try{
			MnUsers mnUsers=null;
			Set<Integer> notUserIdSet= new HashSet<Integer>();
			Query query = new Query(Criteria.where("sharingUserId").is(userId).and("userId").is(friendUserId).and("status").is("E"));
			Update update=new Update();	
			if(acceptDecline.equals("accept")){
				MnFriends mnFriends = new MnFriends();
				mnFriends.setRequestedDate(new Date());
				mnFriends.setAcceptedDate(new Date());
				mnFriends.setUserId(friendUserId);
				mnFriends.setRequestedUserId(userId);
				mnFriends.setStatus("A");
				
				mongoOperations.insert(mnFriends, JavaMessages.Mongo.MNFRIEND);
				
				mnUsers = getUserDetailsObject(userId);
				if(mnUsers!= null){
					Set<Integer> friends =null;
					if(mnUsers.getFriends()!= null && !mnUsers.getFriends().isEmpty()){
						friends = mnUsers.getFriends();
					}else{
						friends = new HashSet<Integer>();
					}
					if(!friends.contains(friendUserId)){
						friends.add(friendUserId);
						Update update2 = new Update(); 
						update2.set("friends",friends);
						
						Query query2 = new Query(Criteria.where("userId").is(userId)); 
						mongoOperations.updateFirst(query2, update2, JavaMessages.Mongo.MNUSERS);
					}
				}
				
				MnUsers users = getUserDetailsObject(friendUserId);
				if(users!= null ){
					Set<Integer> friends = null;
					if(users.getFriends()!= null && !users.getFriends().isEmpty()){
						friends = users.getFriends();
					}else{
						friends = new HashSet<Integer>();
					}
					if(!friends.contains(userId)){
						
						friends.add(userId);
						Update update2 = new Update(); 
						update2.set("friends",friends);
						Query query2 = new Query(Criteria.where("userId").is(friendUserId)); 
						mongoOperations.updateFirst(query2, update2, JavaMessages.Mongo.MNUSERS);
					}
				}
				writeLog(mnUsers.getUserId(), "A", "Added new contact: <B>"+users.getUserFirstName()+" "+users.getUserLastName()+"</B>", "contact", "","", null, "contact");
							
				notUserIdSet.add(users.getUserId());
				Set<Integer> notUserIdSet1=new HashSet<Integer>();
				notUserIdSet1.add(mnUsers.getUserId());
				
				if(notUserIdSet!=null && !notUserIdSet.isEmpty() ){
					
					if(!noteId.equals("0")){
						Query query1 = new Query(Criteria.where("listId").is(Integer.parseInt(listIdd)).and("eventId").is(Integer.parseInt(noteId)));
						MnEventDetails mnEventss = mongoOperations.findOne(query1,MnEventDetails.class, JavaMessages.Mongo.MNEVENTS);
						
						writeLog(users.getUserId(), "N", "<B>"+ users.getUserFirstName()+" "+  users.getUserLastName() + "</B> Shared a Schedule with you: <B>"+mnEventss.getEventName()+"</B>" , "schedule",listIdd,noteId,notUserIdSet1 ,"schedule");
					}
					if(mnUsers.getUserFirstName()!=null && !mnUsers.getUserFirstName().isEmpty() ){
						writeLog(users.getUserId(), "N", "<B>"+mnUsers.getUserFirstName()+" "+mnUsers.getUserLastName()+"</B> added you back ", "contact", "","",notUserIdSet ,"contact");
					}else{
						writeLog(users.getUserId(), "N", "<B>"+mnUsers.getUserName()+"</B> added you back ", "contact", "","",notUserIdSet ,"contact");
					}
				}
				
			}else{
				
			}
			
			if(finalflag)
			{	
				update=new Update();
				update.set("requestPending",false);
				Query query2 = new Query(Criteria.where("userId").is(userId)); 
				mongoOperations.updateFirst(query2, update, JavaMessages.Mongo.MNUSERS);
			}
			
			if(!(level.equals("copy")))
			{
			List<MnNotesSharingDetails> mnNotesSharingDetailsList = mongoOperations.find(query, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			if(mnNotesSharingDetailsList!= null && !mnNotesSharingDetailsList.isEmpty()){
				for(MnNotesSharingDetails details : mnNotesSharingDetailsList ){
					if(details.getSharingLevel().equals("note")){
						
						Query query1= new Query(Criteria.where("sharingUserId").is(userId).and("userId").is(friendUserId).and("status").is("E").and("listId").is(details.getListId()).and("noteId").is(details.getNoteId()));
						if(acceptDecline.equals("accept"))
						{
							update.set("status", "A");
							mongoOperations.updateFirst(query1, update,JavaMessages.Mongo.MNNOTESSHARINGDETAILS );
							updateSharedMembersinNote(details.getListId(),details.getNoteId(),userId.toString());
							
							//update in event table
							List<MnEventsSharingDetails> mnEventsSharingDetailsList = mongoOperations.find(query, MnEventsSharingDetails.class, JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
							if(mnEventsSharingDetailsList!= null && !mnEventsSharingDetailsList.isEmpty()){
								
									update.set("status", "A");
								
								mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
							}
							
							//Sharing Members Based On Email Accept/Decline Based Add Entry In Mn_NoteDetails Table Also - Venu
							Query query3 = new Query(Criteria.where("listId").is(details.getListId()).and("noteId").is(details.getNoteId()).and("status").is("A"));
									
							MnNoteDetails noteDetails= mongoOperations.findOne(query3, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
							if(noteDetails!=null)
							{
							if(noteDetails.getNotesMembers()!=null)
								noteDetails.getNotesMembers().add(userId);
							else{
								List<Integer> userIds=new ArrayList<Integer>();
								userIds.add(userId);
								noteDetails.setNotesMembers(userIds);
							}		
							update = new Update();
							update.set("notesMembers", noteDetails.getNotesMembers());
							mongoOperations.updateFirst(query3, update,JavaMessages.Mongo.MNNOTEDETAILS);
							}
							//End - Venu
							
						}
						else
						{
						update.set("status", "I");
						mongoOperations.updateFirst(query1, update,JavaMessages.Mongo.MNNOTESSHARINGDETAILS );
						
						// event table
						List<MnEventsSharingDetails> mnEventsSharingDetailsList = mongoOperations.find(query, MnEventsSharingDetails.class, JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
						if(mnEventsSharingDetailsList!= null && !mnEventsSharingDetailsList.isEmpty()){
							
								update.set("status", "I");
							
							mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
						}
						}
						
					}
					else
					{
						Query query2= new Query(Criteria.where("sharingUserId").is(userId).and("userId").is(friendUserId).and("status").is("E").and("listId").is(details.getListId()).and("noteId").is(0));
						if(acceptDecline.equals("accept"))
						{
							update.set("status", "A");
							mongoOperations.updateFirst(query2, update,JavaMessages.Mongo.MNNOTESSHARINGDETAILS );
							updateSharedMembersinBook(details.getListId(),userId.toString());
						
						}
						else
						{
							update.set("status", "I");
							mongoOperations.updateFirst(query2, update,JavaMessages.Mongo.MNNOTESSHARINGDETAILS );
						}
					}
				}
			}
			
			}
			else
			{
				updateCopyMailSharing(mnUsers.getEmailId(),mnUsers.getUserId(),friendUserId,acceptDecline);
			}
			if(logger.isDebugEnabled())
			logger.debug("acceptDeclineSharedNoteByMail method successfully returned");
			return acceptDecline;	
			
		}catch (Exception e) {
			logger.error("Exception in acceptDeclineSharedNoteByMail method:"+e);
			return "error";
		}
	}
	public void updateSharedMembersinNote(Integer listId, Integer noteId,String newMember  ){
		if(logger.isDebugEnabled())
		logger.debug("updateSharedMembersinNote method called noteId: "+noteId+" newMember: "+newMember);
		try {
			Query query = new Query(Criteria.where("listId").is(listId));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			
			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			
			
			for (String note : mnList.getMnNotesDetails()) {

				if (mnList.getListType().equalsIgnoreCase("Schedule")) {
					if (note.contains("\"eventId\":\"" + noteId.toString() + "\"")) {
						removeNoteDeatils = note;

						if (note.contains("\"eventMembers\":\"\"")) {
							note = note.replace("\"eventMembers\":\"\"","\"eventMembers\":\"[" + newMember + "]\"");
						} else {
							removeNoteDeatils = note;
							String oldMemberIds = notesSharingDetailsConvertToJsonObject(removeNoteDeatils); 
							if(oldMemberIds!=null && !oldMemberIds.isEmpty()){
								oldMemberIds=oldMemberIds+","+newMember;
								oldMemberIds = "[" + oldMemberIds + "]";
								note = note.replace(membersUpdateConvertToJsonObject(note,mnList.getListType()),"\"eventMembers\":\"" + oldMemberIds+ "\"");
							}else{
								note = note.replace(membersUpdateConvertToJsonObject(note,mnList.getListType()),"\"eventMembers\":\"[" + newMember+ "]\"");
							}
							
						}
						tempNoteDetails = note;
					}

				} else {
					if (note.contains("\"noteId\":\"" + noteId.toString() + "\"")) {
						removeNoteDeatils = note;

						if (note.contains("\"notesMembers\":\"\"")) {
							note = note.replace("\"notesMembers\":\"\"","\"notesMembers\":\"[" + newMember + "]\"");
						} else {
							removeNoteDeatils = note;
							String oldMemberIds = notesSharingDetailsConvertToJsonObject(removeNoteDeatils); 
							if(oldMemberIds!=null && !oldMemberIds.isEmpty()){
								oldMemberIds=oldMemberIds+","+newMember;
								oldMemberIds = "[" + oldMemberIds + "]";
								note = note.replace(membersUpdateConvertToJsonObject(note,mnList.getListType()),"\"notesMembers\":\"" + oldMemberIds+ "\"");
							}else{
								note = note.replace(membersUpdateConvertToJsonObject(note,mnList.getListType()),"\"notesMembers\":\"[" + newMember+ "]\"");
							}
						}
						tempNoteDetails = note;
					}
				}
			}
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);
			
			
			Update update = new Update();
			update.set("mnNotesDetails", mnList.getMnNotesDetails());
			
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			
		}catch (Exception e) {
			logger.error("Exception in updateSharedMembersinNote method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("updateSharedMembersinNote method successfully returned");
	}
	
	
	public void updateCopyMailSharing(String mailId,Integer userId,Integer friendUserId,String acceptDecline)
	{
		if(logger.isDebugEnabled())
		logger.debug("updateCopyMailSharing method called "+userId+" friendUserId: "+friendUserId);
		try {
			Date date = new Date();
			Integer size = 0;
			Query query = new Query(Criteria.where("emailId").in(mailId).and("sharingLevel").is("copy").and("status").is("P"));
			MnNonUserSharingDetails mnNonUserSharingDetailsList = mongoOperations.findOne(query, MnNonUserSharingDetails.class, JavaMessages.Mongo.MNNONUSERSHARINGDETAILS);
			Update update=new Update();
			if(acceptDecline.equals("accept"))
			{
			if(mnNonUserSharingDetailsList!=null && !mnNonUserSharingDetailsList.equals(""))
			{
				update.set("status", "A");
				mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNNONUSERSHARINGDETAILS);
				
				String noteAccess="";
				List<Integer> oldListInt=new ArrayList<Integer>();
				try
				{
					query=new Query(Criteria.where("listId").is(mnNonUserSharingDetailsList.getListId()));
					
					MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
					String tempNoteDetails = null;
					for (String note : mnList.getMnNotesDetails()) {
						if (note.contains("\"noteId\":\"" + mnNonUserSharingDetailsList.getNoteId() + "\"")) {
							tempNoteDetails = note;

						}
					}
					
					JSONObject jsonObject1 = null;
					try {
						jsonObject1 = new JSONObject(tempNoteDetails);
						String oldMembers=(String) jsonObject1.get("copyToMember");
						noteAccess=(String) jsonObject1.get("access");
						if(oldMembers!=null && !oldMembers.equals("")){
							oldMembers=oldMembers.replace("[", "");
							oldMembers=oldMembers.replace("]", "");
							oldMembers=oldMembers.trim();
							String[] str=oldMembers.split(",");
							if(str!=null && str.length !=-1){
								for(int i=0;i<str.length;i++){
									oldListInt.add(Integer.parseInt(str[i].trim()));
								}
							}else{
								oldListInt.add(Integer.parseInt(oldMembers.trim()));
							}
								
						}
						
					}
					catch (Exception e) {
						logger.error("Exception while converting json to NotesSharingDetails object notes Copy in updateCopyMailSharing method !", e);
					}
					//add the comment  in cmt table
					List<MnComments> mnCommentsList = null;
					JSONObject jsonObject2 = null;
					List<Integer> cmtList = new ArrayList<Integer>();
					int cId = 0;
					
					if (mnNonUserSharingDetailsList.getNoteLevel().equals("comments")) {
						jsonObject2 = new JSONObject(tempNoteDetails);
						String commenntString =(String) jsonObject2.get("comments");
						if (commenntString.lastIndexOf(']') != -1) {
							commenntString = commenntString.substring(0, commenntString.lastIndexOf(']'));
						}
						if (commenntString.lastIndexOf('[') != -1) {
							commenntString = commenntString.substring( commenntString.lastIndexOf('[')+1, commenntString.length());
						}
						String[] comts = commenntString.split(",");
						
						for(String str:comts){
							if(str!=null && !str.isEmpty())
								cmtList.add(Integer.parseInt(str.trim()));
						}
						Query query2 = new Query(Criteria.where("cId").in(cmtList));
						mnCommentsList = mongoOperations.find(query2,MnComments.class, JavaMessages.Mongo.MNCOMMETS);
						
						
						List<MnComments> mnCommentsListTot = mongoOperations.findAll(MnComments.class, JavaMessages.Mongo.MNCOMMETS);
						if (mnCommentsListTot != null && mnCommentsListTot.size() != 0) {
							cId = mnCommentsListTot.size() + 1;
						} else {
							cId = 1;
						}
					}
					
					// copy attach file changes
					List<Integer> attachList = new ArrayList<Integer>();
					List<MnAttachmentDetails> attachmentDetails = null;
					int aId=0;
					
					
					jsonObject2 = new JSONObject(tempNoteDetails);
					if (!tempNoteDetails.contains("\"attachFilePath\":\"\"")) {
					String attachFilePathString =(String) jsonObject2.get("attachFilePath");
					if (attachFilePathString.lastIndexOf(']') != -1) {
						attachFilePathString = attachFilePathString.substring(0, attachFilePathString.lastIndexOf(']'));
					}
					if (attachFilePathString.lastIndexOf('[') != -1) {
						attachFilePathString = attachFilePathString.substring( attachFilePathString.lastIndexOf('[')+1, attachFilePathString.length());
					}
					String[] attachFile = attachFilePathString.split(",");
					
					for(String str:attachFile){
						if(str!=null && !str.isEmpty())
							attachList.add(Integer.parseInt(str.trim()));
					}
					Query query2 = new Query(Criteria.where("attachId").in(attachList));
					attachmentDetails = mongoOperations.find(query2,MnAttachmentDetails.class, JavaMessages.Mongo.MNATTACHMENTDETAILS);
					
					
					List<MnAttachmentDetails> mnAttachmentListTot = mongoOperations.findAll(MnAttachmentDetails.class, JavaMessages.Mongo.MNATTACHMENTDETAILS);
					if (mnAttachmentListTot != null && mnAttachmentListTot.size() != 0) {
						aId = mnAttachmentListTot.size() + 1;
					} else {
						aId = 1;
					}
					}
					
						if(!(oldListInt.contains(userId)))
						{
							//Adding Selected List to Selected Note
							Query addQuery=new Query(Criteria.where("userId").is(userId).and("listType").is(mnList.getListType()).and("defaultNote").is(true));
			
							MnList addNote = mongoOperations.findOne(addQuery,MnList.class, JavaMessages.Mongo.MNLIST);
							if (addNote.getMnNotesDetails().size() != 0)
								size = addNote.getMnNotesDetails().size() + 1;
							else
								size = 1;
							
							tempNoteDetails = tempNoteDetails.replace("\"noteId\":\""+ mnNonUserSharingDetailsList.getNoteId() + "\"", "\"noteId\":\"" + size + "\"");
			
							
							tempNoteDetails = tempNoteDetails.replace("\"access\":\""+ noteAccess + "\"", "\"access\":\"private\"");
							
							if (!tempNoteDetails.contains("\"notesMembers\":\"\"")) {
								tempNoteDetails = tempNoteDetails.replace(notesMembersUpdateConvertToJsonObject(tempNoteDetails),"\"notesMembers\":\"\"");
							}
			
							if (!tempNoteDetails.contains("\"noteGroups\":\"\""))
								tempNoteDetails = tempNoteDetails.replace(noteGroupsUpdateConvertToJsonObject(tempNoteDetails,true),"\"noteGroups\":\"\"");
			
							if (tempNoteDetails.contains("\"noteSharedAllContact\":1"))
								tempNoteDetails = tempNoteDetails.replace("\"noteSharedAllContact\":1","\"noteSharedAllContact\":0");
							
							if (!tempNoteDetails.contains("\"copyToMember\":\"\""))
								tempNoteDetails = tempNoteDetails.replace(notesMembersCopyUpdateConvertToJsonObject(tempNoteDetails),"\"copyToMember\":\"\"");

							if (!tempNoteDetails.contains("\"copyToGroup\":\"\""))
								tempNoteDetails = tempNoteDetails.replace(noteGroupsCopyUpdateConvertToJsonObject(tempNoteDetails),"\"copyToGroup\":\"\"");

							if (tempNoteDetails.contains("\"copyToAllContact\":1"))
								tempNoteDetails = tempNoteDetails.replace("\"copyToAllContact\":1","\"copyToAllContact\":0");
							
							if (tempNoteDetails.contains("\"copyFromMember\":\"\""))
								tempNoteDetails = tempNoteDetails.replace("\"copyFromMember\":\"\"","\"copyFromMember\":\"" + friendUserId + "\"");
							else
								tempNoteDetails = tempNoteDetails.replace(noteFromCopyUpdateConvertToJsonObject(tempNoteDetails),"\"copyFromMember\":\"" + friendUserId + "\"");
			
							tempNoteDetails = tempNoteDetails.replace("\"publicUser\":\""+ (String) jsonObject1.get("publicUser") + "\"", "\"publicUser\":\"\"");
							
							tempNoteDetails = tempNoteDetails.replace("\"publicDate\":\""+ (String) jsonObject1.get("publicDate") + "\"", "\"publicDate\":\"\"");
			
							if (mnNonUserSharingDetailsList.getNoteLevel().equals("withOutComments")) {
								if (!tempNoteDetails.contains("\"comments\":\"\""))
									tempNoteDetails = tempNoteDetails.replace(noteCommentsUpdateConvertToJsonObject(tempNoteDetails),"\"comments\":\"\"");
							}else{
								try{
									if(cmtList!= null && !cmtList.isEmpty()){
										cmtList.clear();
									}
									for(MnComments comments : mnCommentsList){
										comments.setcId(cId);
										cmtList.add(cId);
										cId++;
										comments.setListId(addNote.getListId().toString());
										comments.setNoteId(size.toString());
									}
									mongoOperations.insert(mnCommentsList, JavaMessages.Mongo.MNCOMMETS);
									
									tempNoteDetails = tempNoteDetails.replace(noteCommentsUpdateConvertToJsonObject(tempNoteDetails),"\"comments\":\""+cmtList.toString()+"\"");
								}catch (Exception e) {
									logger.error("Exception in updateCopyMailSharing method:"+e);
								}
							}
			
							if (!tempNoteDetails.contains("\"vote\":\"\""))
								tempNoteDetails = tempNoteDetails.replace(noteVotesUpdateConvertToJsonObject(tempNoteDetails),"\"vote\":\"\"");
							
							if (!tempNoteDetails.contains("\"tag\":\"\"")) {
								tempNoteDetails = tempNoteDetails.replace(noteTagUpdateConvertToJsonObject(tempNoteDetails),"\"tag\":\"\"");
							}
							
							if (!tempNoteDetails.contains("\"privateTags\":\"\"")) {
								tempNoteDetails = tempNoteDetails.replace(notePrivateTagUpdateConvertToJsonObject(tempNoteDetails),"\"privateTags\":\"\"");
							}
							
							if (!tempNoteDetails.contains("\"remainders\":\"\"")) {
								tempNoteDetails = tempNoteDetails.replace(noteReminderUpdateConvertToJsonObject(tempNoteDetails),"\"remainders\":\"\"");
							}
							
							if (!tempNoteDetails.contains("\"dueDate\":\"\"")) {
								tempNoteDetails = tempNoteDetails.replace(noteDueDateUpdateConvertToJsonObject(tempNoteDetails),"\"dueDate\":\"\"");
							}
							
							if (!tempNoteDetails.contains("\"dueTime\":\"\"")) {
								tempNoteDetails = tempNoteDetails.replace(noteDueTimeUpdateConvertToJsonObject(tempNoteDetails),"\"dueTime\":\"\"");
							}
							
							// entery in attaachement table
							if (!tempNoteDetails.contains("\"attachFilePath\":\"\"")) 
							{
								try{
									if(attachList!= null && !attachList.isEmpty()){
										attachList.clear();
									}
									for(MnAttachmentDetails achmentDetails : attachmentDetails){
										achmentDetails.setAttachId(aId);
										attachList.add(aId);
										aId++;
										achmentDetails.setListId(addNote.getListId());
										achmentDetails.setNoteId(size.toString());
									}
									mongoOperations.insert(attachmentDetails, JavaMessages.Mongo.MNATTACHMENTDETAILS);
									tempNoteDetails = tempNoteDetails.replace(noteFilePathUpdateConvertToJsonObject(tempNoteDetails),"\"attachFilePath\":\""+attachList.toString()+"\"");
								}catch (Exception e) {
									logger.error(e);
								}
							}
			
							if (addNote.getMnNotesDetails() != null&& addNote.getMnNotesDetails().isEmpty()) {
								addNote.setMnNotesDetails(new HashSet<String>());
								addNote.getMnNotesDetails().add(tempNoteDetails);
							} else {
								addNote.getMnNotesDetails().add(tempNoteDetails);
							}
			
							Update addUpdate = new Update();
							addUpdate.set("mnNotesDetails", addNote.getMnNotesDetails());
							mongoOperations.updateFirst(addQuery, addUpdate,JavaMessages.Mongo.MNLIST);
							
							//Sharing Copy Members Based On Email Id And Accept/Decline Base Add Entry In Mn_NoteDetails Table For New Note Added To select User - Venu
							if(!addNote.getListType().equals("schedule")){
								JSONObject jsonObject=new JSONObject(tempNoteDetails);
								
								MnNoteDetails details=new MnNoteDetails();
								details.setNoteName((String)jsonObject.get("noteName"));
								
								String description = (String) jsonObject.get("description");
								if(description.indexOf("\"") != -1){
									description = description.replace("\"", "\\\"");
								}
								details.setDescription(description);
								
								details.setStartDate(date);
								details.setNoteSharedAllContact(false);
								details.setStatus("A");
								details.setOwnerNoteStatus("A");
								details.setAccess("private");
								
								query=new Query(Criteria.where("userId").is(userId).and("status").is("A"));
								
								details.setCopyFromMember(friendUserId.toString());
								details.setUserId(userId);
								details.setPublicUser("");
								details.setDueDate("");
								details.setDueTime("");
								details.setListId(addNote.getListId());
								details.setListName(addNote.getListName());
								details.setListType(addNote.getListType());
								details.setNoteAccess(addNote.getNoteAccess());
								details.setDefaultNote(addNote.isDefaultNote());
								details.setEnableDisableFlag(addNote.isEnableDisableFlag());
								details.setNoteId(size);
								if (!mnNonUserSharingDetailsList.getNoteLevel().equals("withOutComments") && cmtList!=null) 
									details.setComments(cmtList);
								if(attachList!=null && !attachList.isEmpty())
									details.setAttachFilePath(attachList);
								
								mongoOperations.insert(details,JavaMessages.Mongo.MNNOTEDETAILS);
							}
							//End -Venu
							
						}
					updateMembersForNoteCopy(userId,mnNonUserSharingDetailsList.getListId(),mnNonUserSharingDetailsList.getNoteId());
					Set<Integer> notUserIdSet = new HashSet<Integer>();
					notUserIdSet.add(userId);
					String addUser = "", addUserName = "";
					if (notUserIdSet != null) {
						for (Integer integer : notUserIdSet) {
							MnUsers mnUsers2 = getUserDetailsObject(integer);
							if (mnUsers2 != null) {
								addUser = addUser + "<br> <p>" + mnUsers2.getUserName()+ "</p>";
								addUserName = addUserName + "<B>"+ mnUsers2.getUserName() + "</B>, ";
							}
						}
					}
					addUserName = addUserName.substring(0,addUserName.lastIndexOf(","));
					if(addUser.length() > 36){
						addUser =addUser.substring(0,35)+"...";
					}
					if(addUserName.length() > 36){
						addUserName =addUserName.substring(0,35)+"...";
					}
					JSONObject jsonObject = new JSONObject(tempNoteDetails);

					MnUsers mnUsers = getUserDetailsObject(friendUserId);

					writeLog(mnList.getUserId(), "A", "Shared a copy of a note:"+ jsonObject.get("noteName") + " in "+ mnList.getListName() +" with " + addUserName, "music", mnNonUserSharingDetailsList.getListId().toString(),mnNonUserSharingDetailsList.getNoteId().toString(), null, null);

					if (notUserIdSet != null && !notUserIdSet.isEmpty()) {
						writeLog(mnList.getUserId(), "N", "<B>"+ mnUsers.getUserFirstName()+ mnUsers.getUserLastName()	+ "</B> Shared a copy of a note with you:"+ jsonObject.get("noteName") , "music", mnNonUserSharingDetailsList.getListId().toString(),mnNonUserSharingDetailsList.getNoteId().toString(), notUserIdSet, null);
					}

				} catch (Exception e) {
					logger.error("Exception while Copy To Individual For Note in updateCopyMailSharing method:" + e);
				}
			
				
			}
			}
			else
			{
				update.set("status", "A");
				mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNNONUSERSHARINGDETAILS);
			}
			
			
		}catch (Exception e) {
			logger.error("Exception in updateCopyMailSharing method:"+e);
		}
		if(logger.isDebugEnabled())
			logger.debug("updateCopyMailSharing method successfully returned");
	
	}
	
	public String notesMembersCopyUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
		logger.debug("notesMembersCopyUpdateConvertToJsonObject method called");
		String members = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldMembers = (String) jsonObject.get("copyToMember");
				members = "\"copyToMember\":\"" + oldMembers + "\"";
			} catch (Exception e) {
				logger.error("Exception in notesMembersCopyUpdateConvertToJsonObject method:"+e);
			}
		}
		if(logger.isDebugEnabled())
		logger.debug("notesMembersCopyUpdateConvertToJsonObject method successfully returned");
		return members;
	}
	
	public String noteGroupsCopyUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
		logger.debug("noteGroupsCopyUpdateConvertToJsonObject method called");
		String groups = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldGroups = (String) jsonObject.get("noteGroups");
				groups = "\"noteGroups\":\"" + oldGroups + "\"";
			} catch (Exception e) {
				logger.error("Exception in noteGroupsCopyUpdateConvertToJsonObject method:"+e);
			}
		}
		if(logger.isDebugEnabled())
		logger.debug("noteGroupsCopyUpdateConvertToJsonObject method successfully returned");
		return groups;
	}
	
	public String noteFromCopyUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
		logger.debug("noteFromCopyUpdateConvertToJsonObject method called");
		String groups = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldMember = (String) jsonObject.get("copyFromMember");
				groups = "\"copyFromMember\":\"" + oldMember + "\"";
			} catch (Exception e) {
				logger.error("Exception in noteFromCopyUpdateConvertToJsonObject method:"+e);
			}
		}
		if(logger.isDebugEnabled())
		logger.debug("noteFromCopyUpdateConvertToJsonObject method successfully returned");
		return groups;
	}
	
	
	public String notePrivateTagUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
		logger.debug("notePrivateTagUpdateConvertToJsonObject method called");
		String tags = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldtags = (String) jsonObject.get("privateTags");

				tags = "\"privateTags\":\"" + oldtags + "\"";
			} catch (Exception e) {
				logger.error("Exception in notePrivateTagUpdateConvertToJsonObject method:"+e);
			}
		}
		if(logger.isDebugEnabled())
		logger.debug("notePrivateTagUpdateConvertToJsonObject method successfully returned");
		return tags;
	}
	/////////////////////
	
	public String updateMembersForNoteCopy(Integer members, Integer listId,Integer noteId) {
		if(logger.isDebugEnabled())
		logger.debug("updateMembersForNoteCopy method called noteId: "+noteId+" listId "+listId);
		try {
			Query query = new Query(Criteria.where("listId").is(listId));

			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			Set<String> updateNoteDetails=new TreeSet<String>();
			String tempNoteDetails = null;
			String removeNoteDeatils = null;
			for (String note : mnList.getMnNotesDetails()) {
				if (note.contains("\"noteId\":\"" + noteId + "\"")) {
					removeNoteDeatils = note;
					if (note.contains("\"copyToMember\":\"\"")) {
						note = note.replace("\"copyToMember\":\"\"","\"copyToMember\":\"" + members + "\"");
					} else {
						note = note.replace(notesCopyMembersUpdateConvertToJsonObject(note),"\"copyToMember\":\"" + members + "\"");
					}
					tempNoteDetails = note;
				}
				updateNoteDetails.add(note);
			}
			if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
				mnList.getMnNotesDetails().remove(removeNoteDeatils);

			if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
				mnList.getMnNotesDetails().add(tempNoteDetails);

			Update update = new Update();
			update.set("mnNotesDetails", updateNoteDetails);
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			
			//Sharing Copy Members Based On Email Id And Accept/Decline Base Entry Update Copyied Details In Mn_NoteDetails Table Also - Venu
			query = new Query(Criteria.where("listId").is(listId).and("noteId").is(noteId).and("status").is("A"));
					
			MnNoteDetails noteDetails= mongoOperations.findOne(query, MnNoteDetails.class,JavaMessages.Mongo.MNNOTEDETAILS);
			if(noteDetails.getCopyToMember()!=null)
				noteDetails.getCopyToMember().add(members);
			else{
				List<Integer> userIds=new ArrayList<Integer>();
				userIds.add(members);
				noteDetails.setCopyToMember(userIds);
			}		
			update = new Update();
			update.set("copyToMember", noteDetails.getCopyToMember());
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNNOTEDETAILS);
			//End - Venu

		} catch (Exception e) {
			logger.error("Exception in updateMembersForNoteCopy method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("updateMembersForNoteCopy method successfully returned");
		return "Exception while Updating Members For Note";
	}

	public String notesCopyMembersUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
		logger.debug("notesCopyMembersUpdateConvertToJsonObject method called");
		String members = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldMembers = (String) jsonObject.get("copyToMember");

				members = "\"copyToMember\":\"" + oldMembers + "\"";
			} catch (Exception e) {
				logger.error("Exception in notesCopyMembersUpdateConvertToJsonObject method:"+e);
			}
		}
		if(logger.isDebugEnabled())
		logger.debug("notesCopyMembersUpdateConvertToJsonObject method successfully returned");
		return members;
	}
	
	public String notesMembersUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
		logger.debug("notesMembersUpdateConvertToJsonObject method called");
		String members = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldMembers = (String) jsonObject.get("notesMembers");
				members = "\"notesMembers\":\"" + oldMembers + "\"";
			} catch (Exception e) {
				logger.error("Exception in notesMembersUpdateConvertToJsonObject method:"+e);
			}
		}
		if(logger.isDebugEnabled())
		logger.debug("notesMembersUpdateConvertToJsonObject method successfully returned");
		return members;
	}
	
	public String noteGroupsUpdateConvertToJsonObject(String jsonStr,boolean flag) {
		if(logger.isDebugEnabled())
		logger.debug("noteGroupsUpdateConvertToJsonObject method called");
		String groups = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldGroups = (String) jsonObject.get("noteGroups");
				if(flag){
					groups = "\"noteGroups\":\"" + oldGroups + "\"";
				}else{
					groups = oldGroups;
				}
			} catch (Exception e) {
				logger.error("Exception in noteGroupsUpdateConvertToJsonObject method:"+e);
			}
		}
		if(logger.isDebugEnabled())
		logger.debug("noteGroupsUpdateConvertToJsonObject method successfully returned");
		return groups;
	}
	
	
	public String noteCommentsUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
		logger.debug("noteCommentsUpdateConvertToJsonObject method called");
		String comments = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldComments = (String) jsonObject.get("comments");

				comments = "\"comments\":\"" + oldComments + "\"";
			} catch (Exception e) {
				logger.error("Exception in noteCommentsUpdateConvertToJsonObject method:"+e);
			}
		}
		if(logger.isDebugEnabled())
		logger.debug("noteCommentsUpdateConvertToJsonObject method successfully returned");
		return comments;
	}

	public String noteDueTimeUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
		logger.debug("noteDueTimeUpdateConvertToJsonObject method called");
		String dueTime = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldtime = (String) jsonObject.get("dueTime");

				dueTime = "\"dueTime\":\"" + oldtime + "\"";
			} catch (Exception e) {
				logger.error("Exception in noteDueTimeUpdateConvertToJsonObject method:"+e);
			}
		}
		if(logger.isDebugEnabled())
		logger.debug("noteDueTimeUpdateConvertToJsonObject method successfully returned");
		return dueTime;
	}
	
	public String noteTagUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
		logger.debug("noteTagUpdateConvertToJsonObject method called");
		String tags = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldtags = (String) jsonObject.get("tag");

				tags = "\"tag\":\"" + oldtags + "\"";
			} catch (Exception e) {
				logger.error("Exception in noteTagUpdateConvertToJsonObject method:"+e);
			}
		}
		if(logger.isDebugEnabled())
		logger.debug("noteTagUpdateConvertToJsonObject method successfully returned");
		return tags;
	}
	public String noteReminderUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
		logger.debug("noteReminderUpdateConvertToJsonObject method called");
		String remainders = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldreminder = (String) jsonObject.get("remainders");

				remainders = "\"remainders\":\"" + oldreminder + "\"";
			} catch (Exception e) {
				logger.error("Exception in noteReminderUpdateConvertToJsonObject method:"+e);
			}
		}
		if(logger.isDebugEnabled())
		logger.debug("noteReminderUpdateConvertToJsonObject method successfully returned");
		return remainders;
	}
	public String noteDueDateUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
		logger.debug("noteDueDateUpdateConvertToJsonObject method called");
		String dueDate = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldDate = (String) jsonObject.get("dueDate");

				dueDate = "\"dueDate\":\"" + oldDate + "\"";
			} catch (Exception e) {
				logger.error("Exception in noteDueDateUpdateConvertToJsonObject method:"+e);
			}
		}
		if(logger.isDebugEnabled())
		logger.debug("noteDueDateUpdateConvertToJsonObject method successfully returned");
		return dueDate;
	}
	public String noteFilePathUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
		logger.debug("noteFilePathUpdateConvertToJsonObject method called");
		String attachFilePath = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldfile = (String) jsonObject.get("attachFilePath");

				attachFilePath = "\"attachFilePath\":\"" + oldfile + "\"";
			} catch (Exception e) {
				logger.error("Exception in noteFilePathUpdateConvertToJsonObject method:"+e);
			}
		}
		if(logger.isDebugEnabled())
		logger.debug("noteFilePathUpdateConvertToJsonObject method successfully returned");
		return attachFilePath;
	}
	public String noteVotesUpdateConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
		logger.debug("noteVotesUpdateConvertToJsonObject method called");
		String votes = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				String oldvotes = (String) jsonObject.get("vote");

				votes = "\"vote\":\"" + oldvotes + "\"";
			} catch (Exception e) {
				logger.error("Exception in noteVotesUpdateConvertToJsonObject method:"+e);
			}
		}
		if(logger.isDebugEnabled())
		logger.debug("noteVotesUpdateConvertToJsonObject method successfully returned");
		return votes;
	}
	
	public void updateSharedMembersinBook(Integer listId,String newMember)
	{
		if(logger.isDebugEnabled())
		logger.debug("updateSharedMembersinBook method called listId: "+listId);
		try {
			Query query = new Query(Criteria.where("listId").is(listId));
			MnList mnList = mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
			
			
			List<Integer> oldUserIds = null;
			List<Integer> newUserIds = new ArrayList<Integer>();
			Update update = new Update();
			
				oldUserIds = mnList.getSharedIndividuals();
				if (newMember != null && !newMember.equals("")) {
					if (newMember.contains(",")) {
						String str[] = newMember.split(",");
						for (int i = 0; i < str.length; i++) {
							if(!oldUserIds.contains(Integer.parseInt(str[i]))){
								newUserIds.add(Integer.parseInt(str[i]));
							}
						}
					} else {
						if(!oldUserIds.contains(Integer.parseInt(newMember))){
							newUserIds.add(Integer.parseInt(newMember));
						}
					}
				}
				

				if (oldUserIds != null && !oldUserIds.isEmpty()) 
				{
					oldUserIds.addAll(newUserIds);
					update.set("sharedIndividuals", oldUserIds);
					mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);

				} else {
					
					update.set("sharedIndividuals", newUserIds);
					mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
				}
			
			
			
			mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNLIST);
			
		}catch (Exception e) {
			logger.error("Exception in updateSharedMembersinBook method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("updateSharedMembersinBook method successfully returned");
	}
	////////////////////////////////
	
		public String notesSharingDetailsConvertToJsonObject(String jsonStr) {
			if(logger.isDebugEnabled())
			logger.debug("notesSharingDetailsConvertToJsonObject method called");
			String members = "";
			if (jsonStr != null && !jsonStr.equals("")) {
				JSONObject jsonObject;
				try {
					jsonObject = new JSONObject(jsonStr);
					if (jsonObject.has("notesMembers"))
						members = (String) jsonObject.get("notesMembers");
					else
						members = (String) jsonObject.get("eventMembers");
					if (!members.isEmpty() && members.contains("[")) {
						members = members.substring(members.indexOf("[") + 1,
								members.length());
					}
					if (!members.isEmpty() && members.contains("]")) {
						members = members.substring(0, members.indexOf("]"));
					}
				} catch (Exception e) {
					logger.error("Exception in notesSharingDetailsConvertToJsonObject method:"+e);
				}
			}
			if(logger.isDebugEnabled())
			logger.debug("notesSharingDetailsConvertToJsonObject method successfully returned");
			return members.trim();
		}
		public String membersUpdateConvertToJsonObject(String jsonStr,String listType) {
			if(logger.isDebugEnabled())
			logger.debug("membersUpdateConvertToJsonObject method called");
			String members = "";
			if (jsonStr != null && !jsonStr.equals("")) {
				JSONObject jsonObject;
				try {
					jsonObject = new JSONObject(jsonStr);
					String oldMembers ="";
					if(listType.equals("schedule")){
						oldMembers = (String) jsonObject.get("eventMembers");
						members = "\"eventMembers\":\"" + oldMembers + "\"";
					}else{
						oldMembers = (String) jsonObject.get("notesMembers");
						members = "\"notesMembers\":\"" + oldMembers + "\"";
					}
					
				} catch (Exception e) {
					logger.error("Exception in membersUpdateConvertToJsonObject method:"+e);
				}
			}
			if(logger.isDebugEnabled())
			logger.debug("membersUpdateConvertToJsonObject method successfully returned");
			return members;
		}
		
		@Override
		public String addFriendInvitedUsers(String userMail,String userId,
				String requestedUserId) {
			String status="";
			try{
				if(logger.isDebugEnabled())
					logger.debug("addFriendInvitedUsers method called");
				boolean userExistsCheck=false;
				
				Query query5 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
				MnUsers mnUser1 = mongoOperations.findOne(query5, MnUsers.class, JavaMessages.Mongo.MNUSERS);
				
				Query query6 = new Query(Criteria.where("emailId").is(userMail));
				MnUsers mnUser2 = mongoOperations.findOne(query6, MnUsers.class, JavaMessages.Mongo.MNUSERS);
				if(mnUser2==null){
					
					userExistsCheck=false;
					
				}else {
					
				if(mnUser1!=null){
					
				Set<Integer> friendsSet=mnUser1.getFriends();
				if(friendsSet.contains(mnUser2.getUserId())){
				userExistsCheck=true;
				
				}else{
					userExistsCheck=false;
				}
				}
				}
				
				if(!userExistsCheck){
				Query query1 = new Query(Criteria.where("userId").is(Integer.parseInt(requestedUserId)).and("status").is(JavaMessages.requestStatus).and("requestedUserId").is(Integer.parseInt(userId)));
				MnFriends mnFriend = mongoOperations.findOne(query1, MnFriends.class, JavaMessages.Mongo.MNFRIEND);
					if(mnFriend!=null && !mnFriend.equals("")){
						status="success";
					}
					else{
						MnFriends friend = new MnFriends();
						friend.setUserId(Integer.parseInt(requestedUserId));
						friend.setRequestedUserId(Integer.parseInt(userId));
						friend.setStatus("R");
						friend.setRequestedDate(new Date());
						status = addFriendRequest(friend);
					}
				}
				if(logger.isDebugEnabled())
					logger.debug("addFriendInvitedUsers method executed successfully");
			}catch(Exception e){
			logger.error("Exception in addFriendiInvitedUsers method");	
			}
			return status;
		}
		
		
}
