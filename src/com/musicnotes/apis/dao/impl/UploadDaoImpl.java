package com.musicnotes.apis.dao.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Order;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import com.musicnotes.apis.dao.interfaces.IUploadDao;
import com.musicnotes.apis.domain.MnAttachmentDetails;
import com.musicnotes.apis.domain.MnList;
import com.musicnotes.apis.domain.MnNoteDetails;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.domain.MnVideoFile;
import com.musicnotes.apis.util.JavaMessages;

public class UploadDaoImpl extends BaseDaoImpl implements IUploadDao
{
	Logger logger = Logger.getLogger(UploadDaoImpl.class);

	@Override
	public String saveVideoFileName(String userId, String fileName, String fileType, String status,String fullPath,double fileSize)
	{
		if(logger.isDebugEnabled())
			logger.debug("saveVideoFileName method Called: "+userId+" fileName: "+fileName);
		Query query=null;
		String result = "";
		Integer Id=0;
		try
		{
			
			
			query=new Query();
			query.sort().on("_id", Order.ASCENDING);
			List<MnVideoFile> listFile = mongoOperations.find(query,MnVideoFile.class,JavaMessages.Mongo.MnVideoFile);
			if (listFile != null && listFile.size() != 0)
			{
				MnVideoFile lastFile = listFile.get(listFile.size() - 1);
				Id = lastFile.getId() + 1;
			}
			else
			{
				Id= 1000;
			}
			String fileDummyName[]=fileName.split("_");
			String fileDummyNames=fileDummyName[0]+"."+fileType;
			Date date1=new Date();
			SimpleDateFormat simDateFormat1=new SimpleDateFormat("M/dd/yyyy h:m:s a");
			String dd=simDateFormat1.format(date1);
			mongoOperations.insert(new MnVideoFile(userId, fileName, fileType, status, Id,fileDummyNames,dd,fullPath,fileSize), JavaMessages.Mongo.MnVideoFile);
			Query query2 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
   			MnUsers mnUser = mongoOperations.findOne(query2, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			Update update=new Update();
   			update.set("uploadedSize",mnUser.getUploadedSize()+fileSize);
   			mongoOperations.updateFirst(query2, update, JavaMessages.Mongo.MNUSERS);
			result = "success";
		}
		catch (Exception e)
		{
			logger.error("Create VcUsers Info   :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for saveVideoFileName method Called: ");
		return result;
	}

	@Override
	public List<MnVideoFile> getVideoFileNames(String userId)
	{
		if(logger.isDebugEnabled())
			logger.debug("getVideoFileNames method Called: "+userId);
		List<MnVideoFile> files = new ArrayList<MnVideoFile>();
		List<MnVideoFile> filesReturn = new ArrayList<MnVideoFile>();
		String loginUserZone=null;
		try
		{
			Query query3 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
   			MnUsers mnUser = mongoOperations.findOne(query3, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			loginUserZone=mnUser.getTimeZone();
   			
			Query query = new Query();
			query.addCriteria(Criteria.where("userId").is(userId));
			query.sort().on("_id", Order.DESCENDING);
			List<MnVideoFile> listAudioFiles = mongoOperations.find(query, MnVideoFile.class, JavaMessages.Mongo.MnVideoFile);
			files.addAll(listAudioFiles);
			
			for(MnVideoFile mnVideoFile:files)
			{
				// *************** change time zone ************* ///
				DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
					Date date=null;
					String d=mnVideoFile.getUploadDate();
					date=formatter.parse(d);
					Calendar c = Calendar.getInstance();
					c.setTime(date);
					TimeZone obj = TimeZone.getTimeZone("GMT"+loginUserZone);
				formatter.setTimeZone(obj);
				String convertedDate=formatter.format(c.getTime());
				mnVideoFile.setUploadDate(convertedDate);
				filesReturn.add(mnVideoFile);
				//****************************//
			}
			
		}
		catch (Exception e)
		{
			logger.error("Error in Get video File Names :", e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for getVideoFileNames method Called: ");
		return filesReturn;
	}
	
	@Override
	public List<MnVideoFile> getVideoFileNamesForNote(String userId, String listId, String noteId)
	{
		if(logger.isDebugEnabled())
			logger.debug("getVideoFileNamesForNote method Called: "+userId+" noteId: "+noteId);
		List<MnVideoFile> files = new ArrayList<MnVideoFile>();
		List<MnVideoFile> filesReturn = new ArrayList<MnVideoFile>();
		String loginUserZone=null;
		try
		{
			Query query3 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
   			MnUsers mnUser = mongoOperations.findOne(query3, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			loginUserZone=mnUser.getTimeZone();
			
			Query query = new Query();
			query.addCriteria(Criteria.where("userId").is(userId));
			query.sort().on("_id", Order.DESCENDING);
			List<MnVideoFile> listAudioFiles = mongoOperations.find(query, MnVideoFile.class, JavaMessages.Mongo.MnVideoFile);
			
			if(listAudioFiles!=null && !listAudioFiles.isEmpty())
			{
			for(MnVideoFile mnVideoFile:listAudioFiles)	
			{
				Query query2=new Query();
				query2.addCriteria(Criteria.where("userId").is(Integer.parseInt(userId)).and("fileName").is(mnVideoFile.getFileName()).and("listId").is(Integer.parseInt(listId)).and("noteId").is(noteId).and("status").is("A"));
				MnAttachmentDetails listAttachedFiles = mongoOperations.findOne(query2, MnAttachmentDetails.class, JavaMessages.Mongo.MNATTACHMENTDETAILS);
				if(listAttachedFiles!=null && !listAttachedFiles.equals(""))
				{
					
				}else
				{
					files.add(mnVideoFile);
				}
			}
			
			for(MnVideoFile mnVideoFile:files)
			{
				// *************** change time zone ************* ///
				DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
					Date date=null;
					String d=mnVideoFile.getUploadDate();
					date=formatter.parse(d);
					Calendar c = Calendar.getInstance();
					c.setTime(date);
					TimeZone obj = TimeZone.getTimeZone("GMT"+loginUserZone);
				formatter.setTimeZone(obj);
				String convertedDate=formatter.format(c.getTime());
				mnVideoFile.setUploadDate(convertedDate);
				filesReturn.add(mnVideoFile);
				//****************************//
			}
			
			}
			
		}
		catch (Exception e)
		{
			logger.error("Error in Get video File Names :", e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for getVideoFileNamesForNote method Called: ");
		return filesReturn;
	}
	

	@Override
	public boolean deleteVideoFileNames(String fileName, String userId)
	{
		if(logger.isDebugEnabled())
			logger.debug("saveVideoFileName method Called: "+userId+" fileName: "+fileName);
		boolean flag = false;
		boolean deleteFlag=true;
		try
		{
			 Query query3 = new  Query(Criteria.where("fileName").is(fileName.trim()).and("status").is("A"));
			 List<MnAttachmentDetails> details = mongoOperations.find(query3,MnAttachmentDetails.class,JavaMessages.Mongo.MNATTACHMENTDETAILS);
			 if(details!= null && !details.isEmpty()){
				 deleteFlag=false;
			 }
			
			 if(deleteFlag)
			 {
			Query query = new Query(Criteria.where("userId").is(userId).and("fileName").is(fileName));
			MnVideoFile vcAudioFile = mongoOperations.findAndRemove(query, MnVideoFile.class, JavaMessages.Mongo.MnVideoFile);
			if (vcAudioFile != null && !vcAudioFile.getFileName().isEmpty())
			{
				flag = true;
			}
			MnUsers mnUser = getUserDetailsObject(Integer.parseInt(userId));
			
			writeLog(Integer.parseInt(userId), "A",  "Deleted lesson file :"+removeUnwantedSlash(fileName), "record", "","", null, "record");
			 }
			
		}
		catch (Exception e)
		{
			logger.error("Error delete video File Names :", e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for saveVideoFileName method Called: ");
		return flag;
	}

	@Override
	public String saveOtherFileName(String userId, String fileName, String fileType, String status,String fullPath,double fileSize)
	{
		if(logger.isDebugEnabled())
			logger.debug("saveOtherFileName method Called: "+userId+" fileName: "+fileName);
		Query query=null;
		String result = "";
		Integer Id = 0;
		try
		{
				query=new Query();
				query.sort().on("_id", Order.ASCENDING);
				List<MnVideoFile> listFile = mongoOperations.find(query,MnVideoFile.class,JavaMessages.Mongo.MnFiles);
				if (listFile != null && listFile.size() != 0)
				{
					MnVideoFile lastFile = listFile.get(listFile.size() - 1);
					Id = lastFile.getId() + 1;
				}
				else
				{
					Id= 1000;
				}
				String fileDummyName[]=fileName.split("_");
				String fileDummyNames=fileDummyName[0]+"."+fileType;
				Date date=new Date();
				SimpleDateFormat simDateFormat1=new SimpleDateFormat("M/dd/yyyy h:m:s a");
				String dd=simDateFormat1.format(date);
			    mongoOperations.insert(new MnVideoFile(userId, fileName, fileType, status, Id,fileDummyNames,dd,fullPath,fileSize), JavaMessages.Mongo.MnFiles);
			
			    MnUsers mnUser = getUserDetailsObject(Integer.parseInt(userId));
			    Query query2 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
	   			MnUsers mnUsers = mongoOperations.findOne(query2, MnUsers.class,JavaMessages.Mongo.MNUSERS);
	   			Update update=new Update();
	   			update.set("uploadedSize",mnUsers.getUploadedSize()+fileSize);
	   			mongoOperations.updateFirst(query2, update, JavaMessages.Mongo.MNUSERS);
			    writeLog(Integer.parseInt(userId), "A",  "Added new file :"+removeUnwantedSlash(fileName), "record", "","", null, "record");
			
			result = "success";
		}
		catch (Exception e)
		{
			logger.error("Error inserting files to DB  :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for saveOtherFileName method Called: ");
		return result;
	}
	@Override
	public String saveOtherFileNameAttachWithNote(String userId,String fileName ,String fileType, String status,String listId,String noteId,String fullPath,double fileSize)
	{
		if(logger.isDebugEnabled())
			logger.debug("saveOtherFileNameAttachWithNote method Called: "+userId+" fileName: "+fileName);
		Query query=null;
		String result = "";
		Integer Id = 0;
		try
		{
			query=new Query();
			query.sort().on("_id", Order.ASCENDING);
			List<MnVideoFile> listFile = mongoOperations.find(query,MnVideoFile.class,JavaMessages.Mongo.MnFiles);
			if (listFile != null && listFile.size() != 0)
			{
				MnVideoFile lastFile = listFile.get(listFile.size() - 1);
				Id = lastFile.getId() + 1;
			}
			else
			{
				Id= 1000;
			}
			String fileDummyName[]=fileName.split("_");
			String fileDummyNames=fileDummyName[0]+"."+fileType;
			Date date1=new Date();
			SimpleDateFormat simDateFormat1=new SimpleDateFormat("M/dd/yyyy h:m:s a");
			String dd=simDateFormat1.format(date1);
			
			mongoOperations.insert(new MnVideoFile(userId, fileName, fileType, status, Id,fileDummyNames,dd,fullPath,fileSize), JavaMessages.Mongo.MnFiles);
			//start of attachment insertion
			
			
			
			    Query query4 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
	   			MnUsers users = mongoOperations.findOne(query4, MnUsers.class,JavaMessages.Mongo.MNUSERS);
	   			Update update2=new Update();
	   			update2.set("uploadedSize",users.getUploadedSize()+fileSize);
	   			mongoOperations.updateFirst(query4, update2, JavaMessages.Mongo.MNUSERS);
			
			int attachId = 0;
			Query query1=new Query();
			query1.sort().on("attachId", Order.ASCENDING);
			List<MnAttachmentDetails> attachmentDetails = mongoOperations.find(query1,MnAttachmentDetails.class,JavaMessages.Mongo.MNATTACHMENTDETAILS);
			if (attachmentDetails != null && attachmentDetails.size() != 0)
			{
				MnAttachmentDetails lastFile = attachmentDetails.get(attachmentDetails.size() - 1);
				attachId = lastFile.getAttachId() + 1;
			}
			else
			{
				attachId= 1;
			}
			MnAttachmentDetails attachmentDetails2 = new MnAttachmentDetails();
			attachmentDetails2.setFullPath(fullPath);
			attachmentDetails2.setAttachId(attachId);
			attachmentDetails2.setFileId(Id);
			attachmentDetails2.setFileName(fileName);
			String fileUploadName[]=fileName.split("_");
			attachmentDetails2.setFileUploadName(fileUploadName[0]+"."+fileType);
			attachmentDetails2.getaDate();
			attachmentDetails2.getaTime();
			attachmentDetails2.setListId(Integer.parseInt(listId));
			attachmentDetails2.setNoteId(noteId);
			attachmentDetails2.setStatus("A");
			
			Query query21=new Query(Criteria.where("fileName").is(fileName));
			MnVideoFile mnVideoFile= mongoOperations.findOne(query21,MnVideoFile.class,JavaMessages.Mongo.MnFiles);
			if(mnVideoFile!=null)
			{
			attachmentDetails2.setUploadDate(mnVideoFile.getUploadDate());
			}
			else
			{
				
			mnVideoFile= mongoOperations.findOne(query21,MnVideoFile.class,JavaMessages.Mongo.MnVideoFile);
			attachmentDetails2.setUploadDate(mnVideoFile.getUploadDate());

				
			}
			Date date=new Date();
			SimpleDateFormat simDateFormat=new SimpleDateFormat("M/dd/yyyy h:m:s a");
			
			attachmentDetails2.setCurrentTime(simDateFormat.format(date));
			attachmentDetails2.setUserId(Integer.parseInt(userId));
			mongoOperations.insert(attachmentDetails2,JavaMessages.Mongo.MNATTACHMENTDETAILS);
			
			//end of attachment insertion
			//adding with list and note 
			Set<String> updateNoteDetails=new TreeSet<String>();
			MnList mnList = null;
			String tempNoteDetails = null;
			JSONObject jsonObject = null;
			String attachFilePath = null;
			try {
				Integer size = 0;
				Query query2 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

				mnList = mongoOperations.findOne(query2, MnList.class,
						JavaMessages.Mongo.MNLIST);
				
				String removeNoteDeatils = null;
				for (String note : mnList.getMnNotesDetails()) {
					if (note.contains("\"noteId\":\"" + noteId + "\"")) {
						removeNoteDeatils = note;
						jsonObject = new JSONObject(note);
						if (note.contains("\"attachFilePath\":\"\"")) {
							fileName="["+attachId+"]";
							note = note.replace("\"attachFilePath\":\"\"","\"attachFilePath\":\"" + fileName + "\"");
						} else {
							
							attachFilePath = (String) jsonObject.get("attachFilePath");
							String tempAttach = attachFilePath;
							
							if(attachFilePath.contains("["))
								attachFilePath = attachFilePath.replace("[", "");
							if(attachFilePath.contains("]"))
								attachFilePath = attachFilePath.replace("]", "");
							
							attachFilePath = "["+attachId+","+attachFilePath+"]" ;
							note = note.replace("\"attachFilePath\":\""+tempAttach+"\"","\"attachFilePath\":\"" + attachFilePath + "\"");
						}

						tempNoteDetails = note;
					}
					updateNoteDetails.add(note);
				}
				if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
					mnList.getMnNotesDetails().remove(removeNoteDeatils);

				if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
					mnList.getMnNotesDetails().add(tempNoteDetails);

				Update update = new Update();
				update.set("mnNotesDetails", updateNoteDetails);
				mongoOperations.updateFirst(query2, update,JavaMessages.Mongo.MNLIST);
				
				Query detailsQuery = new Query(Criteria.where("listId").is(mnList.getListId()).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
				MnNoteDetails mnNoteDetails =mongoOperations.findOne(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
				if(mnNoteDetails!= null ){
					Update update1 = new Update();
					if(mnNoteDetails.getAttachFilePath()!= null && !mnNoteDetails.getAttachFilePath().isEmpty()){
						mnNoteDetails.getAttachFilePath().add(attachId);
					}else{
						mnNoteDetails.setAttachFilePath(new ArrayList<Integer>());
						mnNoteDetails.getAttachFilePath().add(attachId);
					}
					update1.set("attachFilePath",mnNoteDetails.getAttachFilePath());
					mongoOperations.updateFirst(detailsQuery,update1,JavaMessages.Mongo.MNNOTEDETAILS);
				}
			} catch (Exception e) {
				logger.error("Exception while Updating Due date For Note :" + e);
			}
						
			String listType ="";
			
			if(mnList.getListType() == "bill"){
				listType = "Memo";
			}else{
				listType = mnList.getListType();
			} 
			
			writeLog(Integer.parseInt(userId), "A",  "Added new file :"+fileDummyNames +", Attached file to :"+jsonObject.get("noteName"),listType , listId, noteId, null, listType);
			
			MnUsers mnUsers = getUserDetailsObject(Integer.parseInt(userId));
			Set<Integer> notUserIdSet = new HashSet<Integer>();
			
				String oldMember = notesSharingDetailsConvertToJsonObject(tempNoteDetails);
				if (oldMember != null && !oldMember.trim().equals("")) {
					String[] memberIds = oldMember.split(",");
					if (memberIds.length != 0) {
						for (int i = 0; i < memberIds.length; i++) {
							if (!memberIds[i].equals(userId)) {
								notUserIdSet.add(Integer.parseInt(memberIds[i]));
							}
						}
					}
				}
				if (!mnList.getUserId().equals(Integer.parseInt(userId))) {
					notUserIdSet.add(mnList.getUserId());
				}
			
				if (notUserIdSet!= null && !notUserIdSet.isEmpty())
						writeLog(Integer.parseInt(userId), "N",  "<B>"+ users.getUserFirstName()+" "+  users.getUserLastName() + "</B> Attached a file to "+ jsonObject.get("noteName") +":<B>"+fileDummyNames+"</B>", mnList.getListType(), mnList.getListId().toString(),(String) jsonObject.get("noteId"),notUserIdSet , mnList.getListType());
				
			result = "success";
		}
		catch (Exception e)
		{
			logger.error("Error inserting files to DB with note :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for saveOtherFileNameAttachWithNote method Called: ");
		return result;
	}
	
	@Override
	public List<MnVideoFile> getOtherFileNames(String userId)
	{
		if(logger.isDebugEnabled())
			logger.debug("getOtherFileNames method Called: "+userId);
		List<MnVideoFile> files = new ArrayList<MnVideoFile>();
		List<MnVideoFile> filesReturn = new ArrayList<MnVideoFile>();
		String loginUserZone=null;
		try
		{
			Query query3 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
   			MnUsers mnUser = mongoOperations.findOne(query3, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			loginUserZone=mnUser.getTimeZone();
   			
			Query query = new Query();
			query.addCriteria(Criteria.where("userId").is(userId).and("status").is("A"));
			query.sort().on("_id", Order.DESCENDING);
			List<MnVideoFile> listFiles = mongoOperations.find(query, MnVideoFile.class, JavaMessages.Mongo.MnFiles);
			files.addAll(listFiles);

			for(MnVideoFile mnVideoFile:files)
			{
				// *************** change time zone ************* ///
				DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
					Date date=null;
					String d=mnVideoFile.getUploadDate();
					date=formatter.parse(d);
					Calendar c = Calendar.getInstance();
					c.setTime(date);
					TimeZone obj = TimeZone.getTimeZone("GMT"+loginUserZone);
				formatter.setTimeZone(obj);
				String convertedDate=formatter.format(c.getTime());
				mnVideoFile.setUploadDate(convertedDate);
				filesReturn.add(mnVideoFile);
				//****************************//
			}
			
		}
		
		catch (Exception e)
		{
			logger.error("Error in getting File Names :", e);
		}
		
		if(logger.isDebugEnabled())
			logger.debug("Return response for getOtherFileNames method Called: ");
		return filesReturn;
	}
	
	@Override
	public boolean deleteOtherFileNames(String fileName, String userId)
	{
		if(logger.isDebugEnabled())
			logger.debug("deleteOtherFileNames method Called: "+userId+" fileName: "+fileName);
		boolean flag = true;
		boolean deleteFlag = false;
		 try{
			 Query query3 = new  Query(Criteria.where("fileName").is(fileName.trim()).and("status").is("A"));
			 List<MnAttachmentDetails> details = mongoOperations.find(query3,MnAttachmentDetails.class,JavaMessages.Mongo.MNATTACHMENTDETAILS);
			 if(details!= null && !details.isEmpty()){
				 for( MnAttachmentDetails mnAttach:details){
					 Query query2 = new Query(Criteria.where("listId").is(mnAttach.getListId()).and("noteId").is(Integer.parseInt(mnAttach.getNoteId())).and("ownerNoteStatus").is("I").and("status").is("I"));
					 MnNoteDetails mnNoteDetails = mongoOperations.findOne(query2, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
					 if(mnNoteDetails!= null){
							deleteFlag=false;
					 }else{
							 deleteFlag=true;
						}
					}
					 
				 }
				if (!deleteFlag) {
				Query query = new Query(Criteria.where("userId").is(userId).and("fileName").is(fileName));
				Update update = new Update();

				update.set("status", "I");
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MnFiles);

				MnUsers mnUser = getUserDetailsObject(Integer.parseInt(userId));

				writeLog(Integer.parseInt(userId), "A",  "Deleted file:"+removeUnwantedSlash(fileName)+" from others ", "record", "","", null, "record");
			} else {
				flag = false;
			}
			
		}
		catch (Exception e)
		{
			flag = false;
			logger.error("Error in Delete File Names :", e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for deleteOtherFileNames method Called: ");
		return flag;
	}
	public void commonLog(String fileName, String userId,String label){
		try{
			if(logger.isDebugEnabled())
				logger.debug("commonLog method Called: "+userId+" fileName: "+fileName);
			MnUsers mnUser = getUserDetailsObject(Integer.parseInt(userId));
			writeLog(Integer.parseInt(userId), "A",  ""+label+" file "+removeUnwantedSlash(fileName)+"", "record", "","", null, "record");
			
		}catch (Exception e) {
			logger.error("Error in download Log File :", e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for commonLog method Called: ");
	}
	public String removeUnwantedSlash(String fileNames){
		if(logger.isDebugEnabled())
			logger.debug("removeUnwantedSlash method Called fileNames: "+fileNames);
		String file="";
		try{
			if(fileNames.lastIndexOf("\\") != -1){
				file = fileNames.substring(fileNames.lastIndexOf("\\")+1,fileNames.length());
			}else{
				file = fileNames;
			}
			
		}catch (Exception e) {
			logger.error(e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for removeUnwantedSlash method Called: ");
		return file;
	}
	public String deleteUploadFileNamesForNote (String listId,String noteId,String fileName, String userId)
	{
		if(logger.isDebugEnabled())
			logger.debug("deleteUploadFileNamesForNote method Called: "+userId+" fileName: "+fileName);
		Date todayDate = new Date();
		Date todayTime = new Date();
		String flag = "";
		Query query1 = null;
		JSONObject jsonObject;
		String attachFile;
		boolean deleteFlag=false;
		MnList mnList = null;
		String tempNoteDetails = null;
		String removeNoteDeatils = null;
		List<Integer> stringList = null;
		try{
			Query query3 = new  Query(Criteria.where("fileName").is(fileName).and("status").is("A"));
			List<MnAttachmentDetails> details  = mongoOperations.find(query3,MnAttachmentDetails.class,JavaMessages.Mongo.MNATTACHMENTDETAILS);
			if(details!= null && !details.isEmpty()){
				if( details.size() == 1){
					 query1 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
					 mnList = mongoOperations.findOne(query1, MnList.class, JavaMessages.Mongo.MNLIST);
					 if(mnList!= null){
						 for (String str : mnList.getMnNotesDetails()) {
							 jsonObject = new JSONObject(str);
							 attachFile = (String) jsonObject.get("attachFilePath");
							 if(str.contains("\"noteId\":\"" + noteId + "\"")){
								 if (attachFile != null && !attachFile.trim().equals("")) {
									 removeNoteDeatils = str;
									 String oldAttaches = attachFile;
										
									 if(attachFile.contains("["))
									 	attachFile = attachFile.replace("[", "");
									 if(attachFile.contains("]"))
									 	attachFile = attachFile.replace("]", "");
									 
									 String[] array = attachFile.split(",");
									 stringList = new ArrayList<Integer>();
									 for(String s: array){
									 	stringList.add(Integer.parseInt(s.trim()));
									 }
									 if(stringList.indexOf(details.get(0).getAttachId()) != -1){
										 stringList.remove(stringList.indexOf(details.get(0).getAttachId()));
									 }
									 
									 String newAttached = "";
									 if(stringList!= null && !stringList.isEmpty()){
									 	newAttached = stringList.toString();
									 }
									 
									 str = str.replace("\"attachFilePath\":\""+oldAttaches+"\"","\"attachFilePath\":\"" +newAttached.trim()+ "\"");
									 tempNoteDetails = str;
								 }
							 }else{
								 if (attachFile != null && !attachFile.trim().equals("")) {
									 if(attachFile.contains("["))
										attachFile = attachFile.replace("[", "");
									 if(attachFile.contains("]"))
										attachFile = attachFile.replace("]", "");
										 
									 String[] array = attachFile.split(",");
									 stringList = new ArrayList<Integer>();
									 for(String s: array){
									 	stringList.add(Integer.parseInt(s.trim()));
									 }
									 if(stringList.indexOf(details.get(0).getAttachId()) != -1){
										 deleteFlag=true;
										 flag = "otherNote";
										 break;
									 }
								 }
							 }
						 }
					 }
				}else if ( details.size() > 1){
					 flag = "otherList";
					 deleteFlag=true;
				}
			}else{
				deleteFlag=true;
			}
			if (!deleteFlag && !flag.equals("otherList") && !flag.equals("otherNote")) {
				if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
					mnList.getMnNotesDetails().remove(removeNoteDeatils);

				if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
					mnList.getMnNotesDetails().add(tempNoteDetails);

				Update update2 = new Update();
				update2.set("mnNotesDetails", mnList.getMnNotesDetails());
				mongoOperations.updateFirst(query1, update2,JavaMessages.Mongo.MNLIST);

				Query query = new Query(Criteria.where("userId").is(userId).and("fileName").is(fileName));
				Update update = new Update();

				update.set("status", "I");
				mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MnFiles);
				
				
				Query detailsQuery = new Query(Criteria.where("listId").is(mnList.getListId()).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
				MnNoteDetails mnNoteDetails =mongoOperations.findOne(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
				if(mnNoteDetails!= null ){
					Update update1 = new Update();
					if(stringList!= null && !stringList.isEmpty() ){
						mnNoteDetails.setAttachFilePath(stringList);
					}else{
						mnNoteDetails.setAttachFilePath(new ArrayList<Integer>());
					}
					update1.set("attachFilePath",mnNoteDetails.getAttachFilePath());
					
					mongoOperations.updateFirst(detailsQuery,update1,JavaMessages.Mongo.MNNOTEDETAILS);
				}
				
				

				Query query2 = new Query(Criteria.where("attachId").is(details.get(0).getAttachId()).and("status").is("A"));
				update.set("endDate",dateFormat.format(todayDate));
				update.set("endTime",timeFormat.format(todayTime));
				mongoOperations.updateFirst(query2, update,JavaMessages.Mongo.MNATTACHMENTDETAILS);
				
				flag = "success";
				
				MnUsers mnUser = getUserDetailsObject(Integer.parseInt(userId));
				writeLog(Integer.parseInt(userId), "A", "Deleted file :"+ removeUnwantedSlash(fileName) ,"record", "", "", null, "record");
			}
			
		}
		catch (Exception e)
		{
			flag = "error";
			logger.error("Error in Delete File Names :", e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for deleteUploadFileNamesForNote method Called: ");
		return ""+flag;
	}
	public String notesSharingDetailsConvertToJsonObject(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("notesSharingDetailsConvertToJsonObject method Called: ");
		String members = "";
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				if (jsonObject.has("notesMembers"))
					members = (String) jsonObject.get("notesMembers");
				else
					members = (String) jsonObject.get("eventMembers");
				if (members.contains("[")) {
					members = members.substring(members.indexOf("[") + 1,
							members.length());
				}
				if (members.contains("]")) {
					members = members.substring(0, members.indexOf("]"));
				}
			} catch (Exception e) {
				logger.error("Exception while converting json to Notes Sharing Details object In Impl !",e);
			}
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for notesSharingDetailsConvertToJsonObject method Called: ");
		return members.trim();
	}

	@Override
	public String saveOtherFileNameAttachWithNoteAudio(String userId,
			String fileName, String fileType, String status, String listId,
			String noteId,String fullPath,double fileSize) {
		if(logger.isDebugEnabled())
			logger.debug("saveOtherFileNameAttachWithNoteAudio method Called: "+userId+" fileName: "+fileName);
		Query query=null;
		String result = "";
		Integer Id = 0;
		try
		{
			query=new Query();
			query.sort().on("_id", Order.ASCENDING);
			List<MnVideoFile> listFile = mongoOperations.find(query,MnVideoFile.class,JavaMessages.Mongo.MnVideoFile);
			if (listFile != null && listFile.size() != 0)
			{
				MnVideoFile lastFile = listFile.get(listFile.size() - 1);
				Id = lastFile.getId() + 1;
			}
			else
			{
				Id= 1000;
			}
			String fileDummyName[]=fileName.split("_");
			String fileDummyNames=fileDummyName[0]+"."+fileType;
			Date date1=new Date();
			SimpleDateFormat simDateFormat1=new SimpleDateFormat("M/dd/yyyy h:m:s a");
			String dd=simDateFormat1.format(date1);
			mongoOperations.insert(new MnVideoFile(userId, fileName, fileType, status, Id,fileDummyNames,dd,fullPath,fileSize), JavaMessages.Mongo.MnVideoFile);
			//start of attachment insertion
			
			    Query query4 = new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
	   			MnUsers users = mongoOperations.findOne(query4, MnUsers.class,JavaMessages.Mongo.MNUSERS);
	   			Update update2=new Update();
	   			update2.set("uploadedSize",users.getUploadedSize()+fileSize);
	   			mongoOperations.updateFirst(query4, update2, JavaMessages.Mongo.MNUSERS);
			
			
			int attachId = 0;
			Query query1=new Query();
			query1.sort().on("attachId", Order.ASCENDING);
			List<MnAttachmentDetails> attachmentDetails = mongoOperations.find(query1,MnAttachmentDetails.class,JavaMessages.Mongo.MNATTACHMENTDETAILS);
			if (attachmentDetails != null && attachmentDetails.size() != 0)
			{
				MnAttachmentDetails lastFile = attachmentDetails.get(attachmentDetails.size() - 1);
				attachId = lastFile.getAttachId() + 1;
			}
			else
			{
				attachId= 1;
			}
			MnAttachmentDetails attachmentDetails2 = new MnAttachmentDetails();
			attachmentDetails2.setAttachId(attachId);
			attachmentDetails2.setFileId(Id);
			attachmentDetails2.setFileName(fileName);
			attachmentDetails2.setFullPath(fullPath);
			String fileUploadName[]=fileName.split("_");
			attachmentDetails2.setFileUploadName(fileUploadName[0]+"."+fileType);
			attachmentDetails2.getaDate();
			attachmentDetails2.getaTime();
			attachmentDetails2.setListId(Integer.parseInt(listId));
			attachmentDetails2.setNoteId(noteId);
			attachmentDetails2.setStatus("A");
			Date date=new Date();
			SimpleDateFormat simDateFormat=new SimpleDateFormat("M/dd/yyyy h:m:s a");
			attachmentDetails2.setCurrentTime(simDateFormat.format(date));
			Query query21=new Query(Criteria.where("fileName").is(fileName));
			MnVideoFile mnVideoFile= mongoOperations.findOne(query21,MnVideoFile.class,JavaMessages.Mongo.MnFiles);
			if(mnVideoFile!=null)
			{
			attachmentDetails2.setUploadDate(mnVideoFile.getUploadDate());
			}
			else
			{
				
			mnVideoFile= mongoOperations.findOne(query21,MnVideoFile.class,JavaMessages.Mongo.MnVideoFile);
			attachmentDetails2.setUploadDate(mnVideoFile.getUploadDate());

				
			}
			attachmentDetails2.setUserId(Integer.parseInt(userId));
			mongoOperations.insert(attachmentDetails2,JavaMessages.Mongo.MNATTACHMENTDETAILS);
			
			//end of attachment insertion
			//adding with list and note 
			Set<String> updateNoteDetails=new TreeSet<String>();
			MnList mnList = null;
			String tempNoteDetails = null;
			JSONObject jsonObject = null;
			String attachFilePath = null;
			try {
				Integer size = 0;
				Query query2 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)));

				mnList = mongoOperations.findOne(query2, MnList.class,
						JavaMessages.Mongo.MNLIST);
				
				String removeNoteDeatils = null;
				for (String note : mnList.getMnNotesDetails()) {
					if (note.contains("\"noteId\":\"" + noteId + "\"")) {
						removeNoteDeatils = note;

						if (note.contains("\"attachFilePath\":\"\"")) {
							fileName="["+attachId+"]";
							note = note.replace("\"attachFilePath\":\"\"","\"attachFilePath\":\"" + fileName + "\"");
						} else {
							jsonObject = new JSONObject(note);
							attachFilePath = (String) jsonObject.get("attachFilePath");
							String tempAttach = attachFilePath;
							
							if(attachFilePath.contains("["))
								attachFilePath = attachFilePath.replace("[", "");
							if(attachFilePath.contains("]"))
								attachFilePath = attachFilePath.replace("]", "");
							
							attachFilePath = "["+attachId+","+attachFilePath+"]" ;
							note = note.replace("\"attachFilePath\":\""+tempAttach+"\"","\"attachFilePath\":\"" + attachFilePath + "\"");
						}

						tempNoteDetails = note;
					}
					updateNoteDetails.add(note);
				}
				if (removeNoteDeatils != null && !removeNoteDeatils.isEmpty())
					mnList.getMnNotesDetails().remove(removeNoteDeatils);

				if (tempNoteDetails != null && !tempNoteDetails.isEmpty())
					mnList.getMnNotesDetails().add(tempNoteDetails);

				Update update = new Update();
				update.set("mnNotesDetails", updateNoteDetails);
				mongoOperations.updateFirst(query2, update,JavaMessages.Mongo.MNLIST);
				
				Query detailsQuery = new Query(Criteria.where("listId").is(mnList.getListId()).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
				MnNoteDetails mnNoteDetails =mongoOperations.findOne(detailsQuery, MnNoteDetails.class, JavaMessages.Mongo.MNNOTEDETAILS);
				if(mnNoteDetails!= null ){
					Update update1 = new Update();
					if(mnNoteDetails.getAttachFilePath()!= null && !mnNoteDetails.getAttachFilePath().isEmpty()){
						mnNoteDetails.getAttachFilePath().add(attachId);
					}else{
						mnNoteDetails.setAttachFilePath(new ArrayList<Integer>());
						mnNoteDetails.getAttachFilePath().add(attachId);
					}
					update1.set("attachFilePath",mnNoteDetails.getAttachFilePath());
					mongoOperations.updateFirst(detailsQuery,update1,JavaMessages.Mongo.MNNOTEDETAILS);
				}
			} catch (Exception e) {
				logger.error("Exception while Updating Due date For Note :" + e);
			}
						
			String listType ="";
			
			if(mnList.getListType() == "bill"){
				listType = "Memo";
			}else{
				listType = mnList.getListType();
			} 
			writeLog(Integer.parseInt(userId), "A",  "Added new file :"+fileName +", Attached file to :"+jsonObject.get("noteName"),listType , listId, noteId, null, listType);
			
			MnUsers mnUsers = getUserDetailsObject(Integer.parseInt(userId));
			Set<Integer> notUserIdSet = new HashSet<Integer>();
			
				String oldMember = notesSharingDetailsConvertToJsonObject(tempNoteDetails);
				if (oldMember != null && !oldMember.trim().equals("")) {
					String[] memberIds = oldMember.split(",");
					if (memberIds.length != 0) {
						for (int i = 0; i < memberIds.length; i++) {
							if (!memberIds[i].equals(userId)) {
								notUserIdSet.add(Integer.parseInt(memberIds[i]));
							}
						}
					}
				}
				if (!mnList.getUserId().equals(Integer.parseInt(userId))) {
					notUserIdSet.add(mnList.getUserId());
				}
			
				if (notUserIdSet!= null && !notUserIdSet.isEmpty())
						writeLog(Integer.parseInt(userId), "A", "Attached a file to "+ jsonObject.get("noteName") +":<B>"+fileName+"</B>", mnList.getListType(), mnList.getListId().toString(),(String) jsonObject.get("noteId"),notUserIdSet , null);
				
			result = "success";
		}
		catch (Exception e)
		{
			logger.error("Error inserting files to DB with note :" + e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for saveOtherFileNameAttachWithNoteAudio method Called: ");;
		return result;
	}

	@Override
	public String getAllFiles(String fileName, Integer userId) {
		if(logger.isDebugEnabled())
			logger.debug("getAllFiles method Called: "+userId+" fileName: "+fileName);
		String fullPath=null;
		try
	   {
			
		Query query = new Query(Criteria.where("userId").is(userId.toString()).and("fileName").is(fileName));
		MnVideoFile vcAudioFile = mongoOperations.findOne(query, MnVideoFile.class, JavaMessages.Mongo.MnVideoFile);
		if (vcAudioFile != null)
		{
			fullPath=vcAudioFile.getFullPath();
		}
		else
		{
			Query query1 = new Query(Criteria.where("userId").is(userId.toString()).and("fileName").is(fileName));
			MnVideoFile mnVideoFile = mongoOperations.findOne(query1, MnVideoFile.class, JavaMessages.Mongo.MnFiles);
			if(mnVideoFile!=null)
			{
				fullPath=mnVideoFile.getFullPath();
			}
		}
	
	}
	catch (Exception e)
	{
		logger.error("Error delete video File Names :", e);
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for getAllFiles method Called: ");
		return fullPath;
	
	}

	@Override
	public MnUsers limitedUpload(String userId) {
		if(logger.isDebugEnabled())
			logger.debug("Limited Size upload "+userId);
		MnUsers mnUsers=null;
		try
	   {
			
		
			 Query limitedQuery=new Query(Criteria.where("userId").is(Integer.parseInt(userId)));
			 mnUsers=mongoOperations.findOne(limitedQuery,MnUsers.class,JavaMessages.Mongo.MNUSERS);
		}
	catch (Exception e)
	{
		logger.error("Error Limited Size upload :", e);
	}
	if(logger.isDebugEnabled())
		logger.debug("Return response for Limited Size upload: ");
		return mnUsers;
	
	}
	}
