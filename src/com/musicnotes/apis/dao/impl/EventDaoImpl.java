package com.musicnotes.apis.dao.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Order;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.musicnotes.apis.dao.interfaces.IEventDao;
import com.musicnotes.apis.domain.MnEventDetails;
import com.musicnotes.apis.domain.MnEventsSharingDetails;
import com.musicnotes.apis.domain.MnFriends;
import com.musicnotes.apis.domain.MnGroupDetailsDomain;
import com.musicnotes.apis.domain.MnList;
import com.musicnotes.apis.domain.MnLog;
import com.musicnotes.apis.domain.MnNotesSharingDetails;
import com.musicnotes.apis.domain.MnSubEventDetails;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.resources.MnEvents;
import com.musicnotes.apis.resources.MnUser;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.MailContent;
import com.musicnotes.apis.util.SendMail;

public class EventDaoImpl extends BaseDaoImpl implements IEventDao
{
	Logger logger = Logger.getLogger(EventDaoImpl.class);

	@Override
	public String createEvent(MnEventDetails mnEventDetails,String emailFlag)
	{
		if(logger.isDebugEnabled())
		logger.debug("createEvent method called emailFlag: "+emailFlag);
		Integer calendarId=0;
		Integer id=0;
		Query query=null;
		Query query2=null;
		boolean mailFlag=false;
		String offSet=null;
		try
		{
			query=new Query();
			query.sort().on("_id",Order.ASCENDING);
			List<MnEventDetails> eventList=mongoOperations.find(query, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS);
			
			if(eventList!=null && !eventList.isEmpty())
			{
				MnEventDetails eventdetail=eventList.get(eventList.size()-1);
				calendarId=eventdetail.getcId()+1;
			}
			else
			{
				calendarId=1;
			}
			mnEventDetails.setcId(calendarId);
			mongoOperations.insert(mnEventDetails, JavaMessages.Mongo.MNEVENTS);
			query2=new Query();
			query2.sort().on("_id",Order.ASCENDING);
			
			if(emailFlag.equalsIgnoreCase("True"))
				mailFlag=true;
			
			Query query3 = new Query(Criteria.where("userId").is(mnEventDetails.getUserId()));
   			MnUsers mnUser = mongoOperations.findOne(query3, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			offSet=mnUser.getTimeZone();
			
			 List<MnSubEventDetails> subEventList=mongoOperations.find(query2, MnSubEventDetails.class,JavaMessages.Mongo.MNESUBVENTS);
		        if(subEventList!=null && !subEventList.isEmpty())
				{
					MnSubEventDetails subEventdetail=subEventList.get(subEventList.size()-1);
					id=subEventdetail.getId()+0;
				}
				else
				{
					id=0;
				}
					
			List<Date> dates = new ArrayList<Date>();
			
			SimpleDateFormat formatter = new SimpleDateFormat("MM/d/yyyy");
			if (mnEventDetails.getRepeatEvent() != null && mnEventDetails.getRepeatEvent().equals("Daily"))
			{
				try
				{
					Date startDate = (Date) formatter.parse(mnEventDetails.getEventStartDate());
					Date endDate = (Date) formatter.parse(mnEventDetails.getEventEndDate());
					long interval = 24 * 1000 * 60 * 60; 
					long endTime = endDate.getTime(); 
					long curTime = startDate.getTime();
					while (curTime <= endTime)
					{
						dates.add(new Date(curTime));
						curTime += interval;
					}
					String startTime[] = mnEventDetails.getStartDate().split(" ");
					String endTimes[] = mnEventDetails.getEndDate().split(" ");
				
			       
					for (int i = 0; i < dates.size(); i++)
					{
						
						MnSubEventDetails details2 = new MnSubEventDetails();
						details2.setEventId(mnEventDetails.getEventId());
						details2.setAlldayevent(mnEventDetails.getAlldayevent());
						details2.setEventName(mnEventDetails.getEventName());
						details2.setDescription(mnEventDetails.getDescription());
						details2.setListId(mnEventDetails.getListId());
						details2.setLocation(mnEventDetails.getLocation());
						details2.setUserId(mnEventDetails.getUserId());
						details2.setC_id(mnEventDetails.getcId());
						details2.setRepeatEvent(mnEventDetails.getRepeatEvent());
						details2.setEmailFlag(mailFlag);
						details2.setTimeZone(offSet);
						
						Date lDate = (Date) dates.get(i);
						String ds = formatter.format(lDate);
						if (mnEventDetails.getAlldayevent())
						{
							details2.setStartDate(ds);
							details2.setEndDate(ds);
						}
						else
						{
							details2.setStartDate(ds + " " + startTime[1]);
							details2.setEndDate(ds + " " + endTimes[1]);
						}
						details2.setStatus("A");
						
						
						details2.setId(++id);
						mongoOperations.insert(details2, JavaMessages.Mongo.MNESUBVENTS);
						
						
						
					}
				}
				catch (Exception e)
				{
					logger.error("Exception in createEvent method:"+e);
				}
			}
			else if (mnEventDetails.getRepeatEvent() != null && mnEventDetails.getRepeatEvent().equals("Weekly"))
			{
				try
				{
					Date startDate = (Date) formatter.parse(mnEventDetails.getEventStartDate());
					Date endDate = (Date) formatter.parse(mnEventDetails.getEventEndDate());
					long interval = 24 * 1000 * 60 * 60 * 7; 
					long endTime = endDate.getTime(); 
					long curTime = startDate.getTime();
					while (curTime <= endTime)
					{
						dates.add(new Date(curTime));
						curTime += interval;
					}
					String startTime[] = mnEventDetails.getStartDate().split(" ");
					String endTimes[] = mnEventDetails.getEndDate().split(" ");
					for (int i = 0; i < dates.size(); i++)
					{
						MnSubEventDetails details2 = new MnSubEventDetails();
						details2.setEventId(mnEventDetails.getEventId());
						details2.setAlldayevent(mnEventDetails.getAlldayevent());
						details2.setEventName(mnEventDetails.getEventName());
						details2.setDescription(mnEventDetails.getDescription());
						details2.setListId(mnEventDetails.getListId());
						details2.setLocation(mnEventDetails.getLocation());
						details2.setUserId(mnEventDetails.getUserId());
						details2.setC_id(mnEventDetails.getcId());
						details2.setRepeatEvent(mnEventDetails.getRepeatEvent());
						details2.setEmailFlag(mailFlag);
						details2.setTimeZone(offSet);
						Date lDate = (Date) dates.get(i);
						String ds = formatter.format(lDate);
						if (mnEventDetails.getAlldayevent())
						{
							details2.setStartDate(ds);
							details2.setEndDate(ds);
						}
						else
						{
							details2.setStartDate(ds + " " + startTime[1]);
							details2.setEndDate(ds + " " + endTimes[1]);
						}
						details2.setStatus("A");
						
						
						details2.setId(++id);
						mongoOperations.insert(details2, JavaMessages.Mongo.MNESUBVENTS);
					}
				}
				catch (Exception e)
				{
					logger.error("Exception in createEvent method:"+e);
				}
			}
			else if (mnEventDetails.getRepeatEvent() != null && mnEventDetails.getRepeatEvent().equals("Monthly"))
			{
				try
				{
					Date startDate = (Date) formatter.parse(mnEventDetails.getEventStartDate());
					Date endDate = (Date) formatter.parse(mnEventDetails.getEventEndDate());
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(startDate);
					Calendar cal3 = Calendar.getInstance();
					cal3.setTime(endDate);
					int difInYear = cal3.get(Calendar.YEAR)- cal2.get(Calendar.YEAR);
					int difInMonths = cal3.get(Calendar.MONTH)-cal2.get(Calendar.MONTH);
					difInMonths+=difInYear*12;
					String startTime[] = mnEventDetails.getStartDate().split(" ");
					String endTimes[] = mnEventDetails.getEndDate().split(" ");
					
					for (int i = 0; i <= difInMonths; i++)
					{
						Calendar calStart = Calendar.getInstance();
						calStart.setTime(startDate);
						calStart.add(Calendar.MONTH, i);
						Calendar calEnd = Calendar.getInstance();
						calEnd.setTime((Date)formatter.parse(mnEventDetails.getEndDate()));
						calEnd.add(Calendar.MONTH, i);
						MnSubEventDetails details2 = new MnSubEventDetails();
						details2.setEventId(mnEventDetails.getEventId());
						details2.setAlldayevent(mnEventDetails.getAlldayevent());
						details2.setEventName(mnEventDetails.getEventName());
						details2.setDescription(mnEventDetails.getDescription());
						details2.setListId(mnEventDetails.getListId());
						details2.setLocation(mnEventDetails.getLocation());
						details2.setUserId(mnEventDetails.getUserId());
						details2.setC_id(mnEventDetails.getcId());
						details2.setRepeatEvent(mnEventDetails.getRepeatEvent());
						details2.setEmailFlag(mailFlag);
						details2.setTimeZone(offSet);
						Date sDate = (Date) calStart.getTime();
						Date eDate = (Date) calEnd.getTime();
						String ds = formatter.format(sDate);
						String es=formatter.format(eDate);
						
						if (mnEventDetails.getAlldayevent())
						{
							details2.setStartDate(ds);
							details2.setEndDate(es);
						}
						else
						{
							details2.setStartDate(ds + " " + startTime[1]);
							details2.setEndDate(es + " " + endTimes[1]);
						}
						
						details2.setStatus("A");
						
						
						details2.setId(++id);
						mongoOperations.insert(details2, JavaMessages.Mongo.MNESUBVENTS);
						
					}
				}
				catch (Exception e)
				{
					logger.error("Exception in createEvent method:"+e);
				}
			}
			else
			{
				
				MnSubEventDetails details2 = new MnSubEventDetails();
				details2.setEventId(mnEventDetails.getEventId());
				details2.setAlldayevent(mnEventDetails.getAlldayevent());
				details2.setEventName(mnEventDetails.getEventName());
				details2.setDescription(mnEventDetails.getDescription());
				details2.setListId(mnEventDetails.getListId());
				details2.setLocation(mnEventDetails.getLocation());
				details2.setUserId(mnEventDetails.getUserId());
				details2.setC_id(mnEventDetails.getcId());
				details2.setRepeatEvent(mnEventDetails.getRepeatEvent());
				details2.setEmailFlag(mailFlag);
				details2.setTimeZone(offSet);
				
				String startD=mnEventDetails.getStartDate();
				String endD=mnEventDetails.getEndDate();
				
				if (mnEventDetails.getAlldayevent())
				{
					Date startDate = (Date) formatter.parse(startD);
					Date endDate = (Date) formatter.parse(endD);
					
					details2.setStartDate(formatter.format(startDate));
					details2.setEndDate(formatter.format(endDate));
				}
				else
				{
					SimpleDateFormat sdf = new SimpleDateFormat("MM/d/yyyy HH:mm");
					Date startDate = (Date) sdf.parse(startD);
					Date endDate = (Date) sdf.parse(endD);
					
					details2.setStartDate(sdf.format(startDate));
					details2.setEndDate(sdf.format(endDate));
				}
				details2.setStatus("A");
				
				
				details2.setId(++id);
				mongoOperations.insert(details2, JavaMessages.Mongo.MNESUBVENTS);
			}
			
			MnUsers mnUsers = getUserDetailsObject(mnEventDetails.getUserId());
			if(logger.isDebugEnabled())
				logger.debug("createEvent method successfully returned");
			return ""+calendarId;
		}
		catch (Exception e) {
			logger.error("Exception in createEvent method:"+e);
			return "0";
		}
		
	}

	@Override
	public MnList getMnList(String listId)
	{
		if(logger.isDebugEnabled())
		logger.debug("getMnList method called listId: "+listId);
		MnList mnList=new MnList();
		try
		{
			Query query=new Query(Criteria.where("listId").is(Integer.parseInt(listId)));
			mnList=mongoOperations.findOne(query, MnList.class, JavaMessages.Mongo.MNLIST);
		}
		catch (Exception e) {
			logger.error("Exception in getMnList method:"+e);
		}
		if(logger.isDebugEnabled())
			logger.debug("getMnList method successfully returned");
		return mnList;
	}

	
	public List<MnSubEventDetails> fetchTodayEventDetail(Integer userId)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchTodayEvents method called "+userId);
		List<MnList> lists=new ArrayList<MnList>();
		List<MnEventDetails> events=new ArrayList<MnEventDetails>();
		List<MnSubEventDetails> subEvents=new ArrayList<MnSubEventDetails>();
		String status="";
		String ownerStatus="";
		MnEventDetails event=null;
		try
		{
			Query query2=new Query(Criteria.where("userId").is(userId).and("listType").is("schedule").and("status").is("A").and("enableDisableFlag").is(true));
			lists=mongoOperations.find(query2,MnList.class,JavaMessages.Mongo.MNLIST);
			
			for(MnList mnList:lists)
			{
				if(mnList.isEnableDisableFlag())
				{
					if(mnList.getMnNotesDetails()!=null && !mnList.getMnNotesDetails().isEmpty())
	                {
						
	                	for(String detail:mnList.getMnNotesDetails())
	                	{
	                		JSONObject jsonObject=null;
	                		jsonObject=new JSONObject(detail);
	                		status=jsonObject.getString("status");
	                		ownerStatus=jsonObject.getString("ownerEventStatus");
	                		
	                		if(status!=null && status.equalsIgnoreCase("A") && ownerStatus!=null && ownerStatus.equalsIgnoreCase("A"))
	                		{
	                		
	                			MnEventDetails eventDetails=new MnEventDetails();
	                			eventDetails.setListId(mnList.getListId());
	                			eventDetails.setEventId(Integer.parseInt(jsonObject.getString("eventId")));
	                			eventDetails.setUserId(userId);
	                			eventDetails.setListType(mnList.getListType());
	                			if(events!=null)
	                			{
	                				events.add(eventDetails);
	                			}
	                		}
	                			
	                	}
	                }
				}
				
			}
			if(events!=null && !events.isEmpty())
			{
			       for(MnEventDetails det:events)
			       {
			    	   event=new MnEventDetails();
			    	   Query query3=new Query(Criteria.where("listId").is(det.getListId()).and("eventId").is(det.getEventId()).and("listType").is("schedule").and("status").is("A").and("userId").is(userId));
			    	   event=mongoOperations.findOne(query3, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS);
			    	   if(event!=null && !event.equals(""))
			    	   {
			    	   Query query4=new Query(Criteria.where("cId").is(event.getcId()).and("status").is("A"));
			    	   subEvents.addAll(mongoOperations.find(query4, MnSubEventDetails.class,JavaMessages.Mongo.MNESUBVENTS));
			    	   }
			       }
			}
		}
		catch (Exception e) {
			logger.error("Exception in fetchEvents method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchEvents method successfully returned");
		return subEvents;
		  
	}
	
	
	@Override
	public List<MnEventDetails> fetchEvents(Integer userId)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchEvents method called "+userId);
		List<MnEventDetails> eventList=new ArrayList<MnEventDetails>();
		List<MnList> lists=new ArrayList<MnList>();
		List<MnEventDetails> events=new ArrayList<MnEventDetails>();
		String status="";
		String ownerStatus="";
		try
		{
			Query query2=new Query(Criteria.where("userId").is(userId).and("listType").is("schedule").and("status").is("A").and("enableDisableFlag").is(true));
			lists=mongoOperations.find(query2,MnList.class,JavaMessages.Mongo.MNLIST);
			
			for(MnList mnList:lists)
			{
				if(mnList.isEnableDisableFlag())
				{
					if(mnList.getMnNotesDetails()!=null && !mnList.getMnNotesDetails().isEmpty())
	                {
						
	                	for(String detail:mnList.getMnNotesDetails())
	                	{
	                		JSONObject jsonObject=null;
	                		jsonObject=new JSONObject(detail);
	                		status=jsonObject.getString("status");
	                		ownerStatus=jsonObject.getString("ownerEventStatus");
	                		
	                		if(status!=null && status.equalsIgnoreCase("A") && ownerStatus!=null && ownerStatus.equalsIgnoreCase("A"))
	                		{
	                		
	                			MnEventDetails eventDetails=new MnEventDetails();
	                			eventDetails.setListId(mnList.getListId());
	                			eventDetails.setEventId(Integer.parseInt(jsonObject.getString("eventId")));
	                			eventDetails.setUserId(userId);
	                			eventDetails.setListType(mnList.getListType());
	                			if(events!=null)
	                			{
	                				events.add(eventDetails);
	                			}
	                		}
	                			
	                	}
	                }
				}
				
			}
			if(events!=null && !events.isEmpty())
			{
			       for(MnEventDetails det:events)
			       {
			    	   Query query3=new Query(Criteria.where("listId").is(det.getListId()).and("eventId").is(det.getEventId()).and("listType").is("schedule").and("status").is("A").and("userId").is(userId));
			    	   		if(eventList!=null)
			    	   			eventList.addAll(mongoOperations.find(query3, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS));
			    	   		
			       }
			}
		}
		catch (Exception e) {
			logger.error("Exception in fetchEvents method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchEvents method successfully returned");
		return eventList;
		  
	}

	@Override
	public List<MnEventDetails> fetchTodayEvents(String startdate, Integer userId,String enddate)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchTodayEvents method called "+userId);
		List<MnEventDetails> eventList=new ArrayList<MnEventDetails>();
		List<MnEventDetails> eventConvertedList=new ArrayList<MnEventDetails>();
		List<MnSubEventDetails> subEvent=new ArrayList<MnSubEventDetails>();
		
		try
		{
			
			String fsdate[]=startdate.split(" ");
			String fedate[]=enddate.split(" ");
			String fullDayStartDate=fsdate[0];
			String fullDayEndDate=fedate[0];
			
			
						/* Block 1: This block is used for Getting Own today events list from the DB ...*/
							List<MnEventDetails> ownEvents=new ArrayList<MnEventDetails>();
							subEvent=fetchTodayEventDetail(userId);
							for(MnSubEventDetails details:subEvent)
							{
								MnEventDetails eventDetails=new MnEventDetails();
								eventDetails.setcId(details.getC_id());
								eventDetails.setUserId(details.getUserId());
								eventDetails.setEventName(details.getEventName());
								eventDetails.setStartDate(details.getStartDate());
								eventDetails.setEndDate(details.getEndDate());
								eventDetails.setEventStartDate(details.getStartDate());
								eventDetails.setEventEndDate(details.getEndDate());
								eventDetails.setEventId(details.getEventId());
								eventDetails.setAlldayevent(details.getAlldayevent());
								eventDetails.setListType(details.getListType());
								eventDetails.setListId(details.getListId());
								eventDetails.setStatus(details.getStatus());
								ownEvents.add(eventDetails);
							}
												
							//List<MnEventDetails> ownEvents=fetchTodayEvents(userId);
							if(ownEvents!=null && !ownEvents.isEmpty())
							{
								for(MnEventDetails deet:ownEvents)
								{
									if(deet.getAlldayevent())
									{
										String sdate="";
										String edate="";
										if (deet.getRepeatEvent() != null && deet.getRepeatEvent().equals("Daily"))
										{
											 sdate=deet.getEventStartDate();
											 edate=deet.getEventEndDate();
										}
										else if (deet.getRepeatEvent() != null && deet.getRepeatEvent().equals("Weekly"))
										{
											 sdate=deet.getEventStartDate();
											 edate=deet.getEventEndDate();
										}
										else if (deet.getRepeatEvent() != null && deet.getRepeatEvent().equals("Monthly"))
										{
											 sdate=deet.getEventStartDate();
											 edate=deet.getEventEndDate();
										}
										
										else
										{
											 sdate=deet.getStartDate();
											 edate=deet.getEndDate();	
										}
										
										SimpleDateFormat format=new SimpleDateFormat("MM/dd/yyyy");
										Date todayDate=format.parse(format.format(new Date()));
										Date d1=format.parse(sdate);
										Date d2=format.parse(edate);
										if((todayDate.after(d1) && todayDate.before(d2)) || todayDate.compareTo(d1)==0 || todayDate.compareTo(d2)==0)
										{
											deet.setDateRange("alldayevent");
											eventList.add(deet);
										}
									}
									else
									{
										String sdatetime="";
										String edatetime="";
										
										if (deet.getRepeatEvent() != null && deet.getRepeatEvent().equals("Daily"))
										{
											 sdatetime=deet.getEventStartDate();
											 edatetime=deet.getEventEndDate();
											 SimpleDateFormat format=new SimpleDateFormat("MM/dd/yyyy");
												Date todayDate=format.parse(format.format(new Date()));
												Date d1=format.parse(sdatetime);
												Date d2=format.parse(edatetime);
												
												if(todayDate.after(d1) && todayDate.before(d2))
												{
													deet.setDateRange("between");
													eventList.add(deet);
												}
												else if(todayDate.compareTo(d1)==0 && todayDate.compareTo(d2)==0)
												{
													deet.setDateRange("startend");
													eventList.add(deet);
												}
												else if(todayDate.compareTo(d1)==0)
												{
													deet.setDateRange("start");
													eventList.add(deet);	

												}
												else if(todayDate.compareTo(d2)==0)
												{
													deet.setDateRange("end");
													eventList.add(deet);
												}
										}
										else if (deet.getRepeatEvent() != null && deet.getRepeatEvent().equals("Weekly"))
										{
											
											 sdatetime=deet.getEventStartDate();
											 edatetime=deet.getEventEndDate();
											 SimpleDateFormat format=new SimpleDateFormat("MM/dd/yyyy");
												Date todayDate=format.parse(format.format(new Date()));
												Date d1=format.parse(sdatetime);
												Date d2=format.parse(edatetime);
												
												
												if(todayDate.compareTo(d1)==0 && todayDate.compareTo(d2)==0)
												{
													
													deet.setDateRange("startend");
													eventList.add(deet);
												}
												else if(todayDate.compareTo(d1)==0)
												{
													
													deet.setDateRange("start");
													eventList.add(deet);	

												}
												else if(todayDate.compareTo(d2)==0)
												{
													deet.setDateRange("end");
													eventList.add(deet);
												}
										}
										else if (deet.getRepeatEvent() != null && deet.getRepeatEvent().equals("Monthly"))
										{

											
											 sdatetime=deet.getEventStartDate();
											 edatetime=deet.getEventEndDate();
											 SimpleDateFormat format=new SimpleDateFormat("MM/dd/yyyy");
												Date todayDate=format.parse(format.format(new Date()));
												Date d1=format.parse(sdatetime);
												Date d2=format.parse(edatetime);
												
												
												if(todayDate.compareTo(d1)==0 && todayDate.compareTo(d2)==0)
												{
													
													deet.setDateRange("startend");
													eventList.add(deet);
												}
												else if(todayDate.compareTo(d1)==0)
												{
													
													deet.setDateRange("start");
													eventList.add(deet);	

												}
												else if(todayDate.compareTo(d2)==0)
												{
													deet.setDateRange("end");
													eventList.add(deet);
												}
										
										}
										else
											{
											
											 sdatetime=deet.getStartDate();
											 edatetime=deet.getEndDate();
											String[] sdate=sdatetime.split(" ");
											String[] edate=edatetime.split(" ");
											SimpleDateFormat format=new SimpleDateFormat("MM/dd/yyyy");
											Date todayDate=format.parse(format.format(new Date()));
											Date d1=format.parse(sdate[0]);
											Date d2=format.parse(edate[0]);
											
											if(todayDate.after(d1) && todayDate.before(d2))
											{
												deet.setDateRange("between");
												eventList.add(deet);
											}
											else if(todayDate.compareTo(d1)==0 && todayDate.compareTo(d2)==0)
											{
												deet.setDateRange("startend");
												eventList.add(deet);
											}
											else if(todayDate.compareTo(d1)==0)
											{
												deet.setDateRange("start");
												eventList.add(deet);	

											}
											else if(todayDate.compareTo(d2)==0)
											{
												deet.setDateRange("end");
												eventList.add(deet);
											}
											}
									}
								}

							}
							
						/* End of the Block 1 */
						
						/* Block 2: This block is used for Getting shared Individual or Contact events on today list from the DB ...*/
							
							List<MnEventDetails> sharedContactIndividualEvents=new ArrayList<MnEventDetails>();
							//logger.info("sharedContactIndividualEvents :"+sharedContactIndividualEvents.toString());
							
							List<MnSubEventDetails> sharedIndividualEvents=fetchSharedEventsForTodaySchedule(userId ,"loading");
							for(MnSubEventDetails details:sharedIndividualEvents)
							{
								MnEventDetails eventDetails=new MnEventDetails();
								eventDetails.setcId(details.getC_id());
								eventDetails.setUserId(details.getUserId());
								eventDetails.setEventName(details.getEventName());
								eventDetails.setStartDate(details.getStartDate());
								eventDetails.setEndDate(details.getEndDate());
								eventDetails.setEventStartDate(details.getStartDate());
								eventDetails.setEventEndDate(details.getEndDate());
								eventDetails.setEventId(details.getEventId());
								eventDetails.setAlldayevent(details.getAlldayevent());
								eventDetails.setListType(details.getListType());
								eventDetails.setListId(details.getListId());
								eventDetails.setStatus(details.getStatus());
								sharedContactIndividualEvents.add(eventDetails);
							}
							
							if(sharedContactIndividualEvents!=null && !sharedContactIndividualEvents.isEmpty())
							{
								for(MnEventDetails deet:sharedContactIndividualEvents)
								{
									if(deet.getAlldayevent())
									{
										String sdate=deet.getStartDate();
										String edate=deet.getEndDate();
										SimpleDateFormat format=new SimpleDateFormat("MM/dd/yyyy");
										Date todayDate=format.parse(format.format(new Date()));
										Date d1=format.parse(sdate);
										Date d2=format.parse(edate);
										if((todayDate.after(d1) && todayDate.before(d2)) || todayDate.compareTo(d1)==0 || todayDate.compareTo(d2)==0)
										{
											deet.setDateRange("alldayevent");
											eventList.add(deet);
										}
									}
									else
									{
										String sdatetime=deet.getStartDate();
										String edatetime=deet.getEndDate();
										String[] sdate=sdatetime.split(" ");
										String[] edate=edatetime.split(" ");
										SimpleDateFormat format=new SimpleDateFormat("MM/dd/yyyy");
										Date todayDate=format.parse(format.format(new Date()));
										Date d1=format.parse(sdate[0]);
										Date d2=format.parse(edate[0]);
										if(todayDate.after(d1) && todayDate.before(d2))
										{
											deet.setDateRange("between");
											eventList.add(deet);
										}
										else if(todayDate.compareTo(d1)==0 && todayDate.compareTo(d2)==0)
										{
											deet.setDateRange("startend");
											eventList.add(deet);
										}
										else if(todayDate.compareTo(d1)==0)
										{
											deet.setDateRange("start");
											eventList.add(deet);	
										}
										else if(todayDate.compareTo(d2)==0)
										{
											deet.setDateRange("end");
											eventList.add(deet);
										}
									}
								}
								 
							}
						/* End of the Block 2 */
						
						/* Block 3: This block is used for Getting shared group events on today list from the DB ...*/
							//List<MnEventDetails> sharedGroupEvents=fetchGroupSharedEventBasedOnList(userId,"loading");
							List<MnEventDetails> sharedGroupEvents=new ArrayList<MnEventDetails>();
							List<MnSubEventDetails> sharedGroupSubEvents=fetchGroupSharedEventForTodaySchedule(userId,"loading");
							
							for(MnSubEventDetails details:sharedGroupSubEvents)
							{
								MnEventDetails eventDetails=new MnEventDetails();
								eventDetails.setcId(details.getC_id());
								eventDetails.setUserId(details.getUserId());
								eventDetails.setEventName(details.getEventName());
								eventDetails.setStartDate(details.getStartDate());
								eventDetails.setEndDate(details.getEndDate());
								eventDetails.setEventStartDate(details.getStartDate());
								eventDetails.setEventEndDate(details.getEndDate());
								eventDetails.setEventId(details.getEventId());
								eventDetails.setAlldayevent(details.getAlldayevent());
								eventDetails.setListType(details.getListType());
								eventDetails.setListId(details.getListId());
								eventDetails.setStatus(details.getStatus());
								sharedGroupEvents.add(eventDetails);
							}
							
							if(sharedGroupEvents!=null && !sharedGroupEvents.isEmpty())
							{
								for(MnEventDetails deet:sharedGroupEvents)
								{
									if(deet.getAlldayevent())
									{
										String sdate=deet.getStartDate();
										String edate=deet.getEndDate();
										SimpleDateFormat format=new SimpleDateFormat("MM/dd/yyyy");
										Date todayDate=format.parse(format.format(new Date()));
										Date d1=format.parse(sdate);
										Date d2=format.parse(edate);
										if((todayDate.after(d1) && todayDate.before(d2)) || todayDate.compareTo(d1)==0 || todayDate.compareTo(d2)==0)
										{
											deet.setDateRange("alldayevent");
											eventList.add(deet);
										}
									}
									else
									{
										String sdatetime=deet.getStartDate();
										String edatetime=deet.getEndDate();
										String[] sdate=sdatetime.split(" ");
										String[] edate=edatetime.split(" ");
										SimpleDateFormat format=new SimpleDateFormat("MM/dd/yyyy");
										Date todayDate=format.parse(format.format(new Date()));
										Date d1=format.parse(sdate[0]);
										Date d2=format.parse(edate[0]);
										if(todayDate.after(d1) && todayDate.before(d2))
										{
											deet.setDateRange("between");
											eventList.add(deet);
										}
										else if(todayDate.compareTo(d1)==0 && todayDate.compareTo(d2)==0)
										{
											deet.setDateRange("startend");
											eventList.add(deet);
										}
										else if(todayDate.compareTo(d1)==0)
										{
											deet.setDateRange("start");
											eventList.add(deet);	
										}
										else if(todayDate.compareTo(d2)==0)
										{
											deet.setDateRange("end");
											eventList.add(deet);
										}
									}
								}
							}
						
						/* End of the Block 3 */
			
		   
		// code for change time Zone
			
			for(MnEventDetails details:eventList)
			{
				if(!(details.getUserId().equals(userId) || details.getAlldayevent()))
				{
					String stDate = convertedZoneEvents(userId, details.getUserId(), details.getStartDate());
					details.setStartDate(stDate);
					String endDate = convertedZoneEvents(userId, details.getUserId(), details.getEndDate());
					details.setEndDate(endDate);
				}
				
				eventConvertedList.add(details);
			}
			logger.info("eventConvertedList ----------------- :"+eventConvertedList.toString());
		}
		catch (Exception e) {
			logger.error("Exception in fetchTodayEvents method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchTodayEvents method successfully returned");
		return eventConvertedList;
	}
	
	
	private List<MnSubEventDetails> fetchSharedEventsForTodaySchedule(Integer userId, String type) 
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchSharedEventsBasedOnList method called "+userId);
		List<MnEventDetails> eventList=new ArrayList<MnEventDetails>();
		List<MnNotesSharingDetails> details=null;
		List<MnEventsSharingDetails> sharedDetails=new ArrayList<MnEventsSharingDetails>();
		List<MnSubEventDetails> subEvents=new ArrayList<MnSubEventDetails>();
		Query query = null;
		Query query1=null;
		try
		{
			query = new Query(Criteria.where("sharingUserId").is(userId).and("listType").is("schedule").and("status").is("A").and("sharingGroupId").is(0));
			query.sort().on("noteId", Order.ASCENDING);
			details = mongoOperations.find(query, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			
			if(details!=null && !details.isEmpty()){
				for(MnNotesSharingDetails sharingDetails: details){
					if(sharingDetails.getNoteId().toString().equals("0"))
						query1=new Query(Criteria.where("sharingUserId").is(userId).and("listType").is("schedule").and("status").is("A").and("sharingStatus").is("A").and("sharingGroupId").is(0).and("listId").is(sharingDetails.getListId()));
					else
						if(type.equals("InitialLoad"))
							query1=new Query(Criteria.where("sharingUserId").is(userId).and("listType").is("schedule").and("status").is("A").and("sharingStatus").ne("D").and("sharingGroupId").is(0).and("listId").is(sharingDetails.getListId()).and("noteId").is(sharingDetails.getNoteId()));
						else
							query1=new Query(Criteria.where("sharingUserId").is(userId).and("listType").is("schedule").and("status").is("A").and("sharingStatus").is("A").and("sharingGroupId").is(0).and("listId").is(sharingDetails.getListId()).and("noteId").is(sharingDetails.getNoteId()));
					
					MnEventsSharingDetails eventsSharingDetails=mongoOperations.findOne(query1, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
					if(eventsSharingDetails!=null && !eventsSharingDetails.equals(""))
					{
					sharedDetails.add(mongoOperations.findOne(query1, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS));
					}
				}
				
				if(sharedDetails!=null && !sharedDetails.isEmpty())
				{
					for (MnEventsSharingDetails sharingD : sharedDetails)
					{
						Query query2=new Query(Criteria.where("listId").is(sharingD.getListId()).and("listType").is("schedule").and("status").is("A").and("enableDisableFlag").is(true));
						MnList mnList=mongoOperations.findOne(query2, MnList.class,JavaMessages.Mongo.MNLIST);
						if(mnList!=null)
						{
								query = new Query(Criteria.where("eventId").is(sharingD.getNoteId()).and("status").is("A").and("listId").is(mnList.getListId()));	
								eventList.addAll(mongoOperations.find(query, MnEventDetails.class, JavaMessages.Mongo.MNEVENTS));
						}
					}
				}
				
				if(eventList!=null && !eventList.isEmpty())
				{
				       for(MnEventDetails det:eventList)
				       {
				    	   Query query4=new Query(Criteria.where("cId").is(det.getcId()).and("status").is("A"));
				    	   subEvents.addAll(mongoOperations.find(query4, MnSubEventDetails.class,JavaMessages.Mongo.MNESUBVENTS));
				       }
				}
				
			}
			
		}
		catch (Exception e)
		{
			logger.error("Exception in fetchSharedEventsBasedOnList method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchSharedEventsBasedOnList method successfully returned");
		return subEvents;
	
	}

	public List<MnEventDetails> fetchSharedEventsBasedOnList(Integer userId ,String type)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchSharedEventsBasedOnList method called "+userId);
		List<MnEventDetails> eventList=new ArrayList<MnEventDetails>();
		List<MnNotesSharingDetails> details=null;
		List<MnEventsSharingDetails> sharedDetails=new ArrayList<MnEventsSharingDetails>();
		Query query = null;
		Query query1=null;
		try
		{
			query = new Query(Criteria.where("sharingUserId").is(userId).and("listType").is("schedule").and("status").is("A").and("sharingGroupId").is(0));
			query.sort().on("noteId", Order.ASCENDING);
			details = mongoOperations.find(query, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			
			if(details!=null && !details.isEmpty()){
				for(MnNotesSharingDetails sharingDetails: details){
					if(sharingDetails.getNoteId().toString().equals("0"))
						query1=new Query(Criteria.where("sharingUserId").is(userId).and("listType").is("schedule").and("status").is("A").and("sharingStatus").is("A").and("sharingGroupId").is(0).and("listId").is(sharingDetails.getListId()));
					else
						if(type.equals("InitialLoad"))
							query1=new Query(Criteria.where("sharingUserId").is(userId).and("listType").is("schedule").and("status").is("A").and("sharingStatus").ne("D").and("sharingGroupId").is(0).and("listId").is(sharingDetails.getListId()).and("noteId").is(sharingDetails.getNoteId()));
						else
							query1=new Query(Criteria.where("sharingUserId").is(userId).and("listType").is("schedule").and("status").is("A").and("sharingStatus").is("A").and("sharingGroupId").is(0).and("listId").is(sharingDetails.getListId()).and("noteId").is(sharingDetails.getNoteId()));
					
					MnEventsSharingDetails eventsSharingDetails=mongoOperations.findOne(query1, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
					if(eventsSharingDetails!=null && !eventsSharingDetails.equals(""))
					{
					sharedDetails.add(mongoOperations.findOne(query1, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS));
					}
				}
				
				if(sharedDetails!=null && !sharedDetails.isEmpty())
				{
					for (MnEventsSharingDetails sharingD : sharedDetails)
					{
						Query query2=new Query(Criteria.where("listId").is(sharingD.getListId()).and("listType").is("schedule").and("status").is("A").and("enableDisableFlag").is(true));
						MnList mnList=mongoOperations.findOne(query2, MnList.class,JavaMessages.Mongo.MNLIST);
						if(mnList!=null)
						{
								query = new Query(Criteria.where("eventId").is(sharingD.getNoteId()).and("status").is("A").and("listId").is(mnList.getListId()));	
								eventList.addAll(mongoOperations.find(query, MnEventDetails.class, JavaMessages.Mongo.MNEVENTS));
						}
					}
				}
				
			}
		}
		catch (Exception e)
		{
			logger.error("Exception in fetchSharedEventsBasedOnList method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchSharedEventsBasedOnList method successfully returned");
		return eventList;
	}

	public List<MnSubEventDetails> fetchGroupSharedEventForTodaySchedule(Integer userId , String type) {
		if(logger.isDebugEnabled())
			logger.debug("fetchGroupSharedEventBasedOnList method called "+userId);
		List<MnEventDetails> eventList = new ArrayList<MnEventDetails>();
		List<MnNotesSharingDetails> details=null;
		List<MnGroupDetailsDomain> mnGroupDetails=null;
		List<Integer> sharingGroupIds=null;
		List<MnSubEventDetails> subEvents=new ArrayList<MnSubEventDetails>();
		List<MnEventsSharingDetails> sharedDetails=new ArrayList<MnEventsSharingDetails>();
		Query query1=null;
		Query query = null;
		try
		{
			query = new Query(Criteria.where("userId").is(userId).and("status").is("A"));
			mnGroupDetails = mongoOperations.find(query, MnGroupDetailsDomain.class, JavaMessages.Mongo.MNGROUPDETAILS);
			if(mnGroupDetails!=null && !mnGroupDetails.isEmpty()){
				sharingGroupIds=new ArrayList<Integer>();
				for(MnGroupDetailsDomain domain: mnGroupDetails){
					sharingGroupIds.add(domain.getGroupId());
				}
			}
			if(sharingGroupIds!=null && !sharingGroupIds.isEmpty()){
				query = new Query(Criteria.where("sharingGroupId").in(sharingGroupIds).and("sharingUserId").is(userId).and("listType").is("schedule").and("status").is("A"));
				query.sort().on("listId", Order.ASCENDING);
				details = mongoOperations.find(query, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			
				if(details!=null && !details.isEmpty()){
					for(MnNotesSharingDetails sharingDetails: details){
						
						if(sharingDetails.getNoteId().toString().equals("0"))
							query1=new Query(Criteria.where("sharingUserId").is(userId).and("listType").is("schedule").and("status").is("A").and("sharingStatus").is("A").and("sharingGroupId").is(sharingDetails.getSharingGroupId()).and("listId").is(sharingDetails.getListId()));
						else
							if(type.equals("InitialLoad"))
								query1=new Query(Criteria.where("sharingUserId").is(userId).and("listType").is("schedule").and("status").is("A").and("sharingStatus").ne("D").and("sharingGroupId").is(sharingDetails.getSharingGroupId()).and("listId").is(sharingDetails.getListId()).and("noteId").is(sharingDetails.getNoteId()));
							else
								query1=new Query(Criteria.where("sharingUserId").is(userId).and("listType").is("schedule").and("status").is("A").and("sharingStatus").is("A").and("sharingGroupId").is(sharingDetails.getSharingGroupId()).and("listId").is(sharingDetails.getListId()).and("noteId").is(sharingDetails.getNoteId()));	
								
						sharedDetails.addAll(mongoOperations.find(query1, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS));
					}
				
					List<Integer> eventIds=new ArrayList<Integer>();
				
					if(sharedDetails!=null && !sharedDetails.isEmpty())
					{
						for(MnEventsSharingDetails sharingDet: sharedDetails){
							
							Query query2=new Query(Criteria.where("listId").is(sharingDet.getListId()).and("listType").is("schedule").and("status").is("A").and("enableDisableFlag").is(true));
							MnList mnList=mongoOperations.findOne(query2, MnList.class,JavaMessages.Mongo.MNLIST);
							if(mnList!=null){
								
									query = new Query(Criteria.where("eventId").is(sharingDet.getNoteId()).and("status").is("A").and("listId").is(mnList.getListId()));
									
									eventList.addAll(mongoOperations.find(query, MnEventDetails.class, JavaMessages.Mongo.MNEVENTS));
							}
						}
					}
					
					if(eventList!=null && !eventList.isEmpty())
					{
					       for(MnEventDetails det:eventList)
					       {
					    	   Query query4=new Query(Criteria.where("cId").is(det.getcId()).and("status").is("A"));
					    	   subEvents.addAll(mongoOperations.find(query4, MnSubEventDetails.class,JavaMessages.Mongo.MNESUBVENTS));
					       }
					}
				}
			}
		}
		catch (Exception e)
		{
			logger.error("Exception in fetchGroupSharedEventBasedOnList method:"+e);

		}
		if(logger.isDebugEnabled())
		logger.debug("fetchGroupSharedEventBasedOnList method successfully returned");
		return subEvents;
	
	}
	
	@Override
	public List<MnEventDetails> fetchGroupSharedEventBasedOnList(Integer userId , String type) {
		if(logger.isDebugEnabled())
			logger.debug("fetchGroupSharedEventBasedOnList method called "+userId);
		List<MnEventDetails> eventList = new ArrayList<MnEventDetails>();
		List<MnNotesSharingDetails> details=null;
		List<MnGroupDetailsDomain> mnGroupDetails=null;
		List<Integer> sharingGroupIds=null;
		List<MnEventsSharingDetails> sharedDetails=new ArrayList<MnEventsSharingDetails>();
		Query query1=null;
		Query query = null;
		try
		{
			query = new Query(Criteria.where("userId").is(userId).and("status").is("A"));
			mnGroupDetails = mongoOperations.find(query, MnGroupDetailsDomain.class, JavaMessages.Mongo.MNGROUPDETAILS);
			if(mnGroupDetails!=null && !mnGroupDetails.isEmpty()){
				sharingGroupIds=new ArrayList<Integer>();
				for(MnGroupDetailsDomain domain: mnGroupDetails){
					sharingGroupIds.add(domain.getGroupId());
				}
			}
			if(sharingGroupIds!=null && !sharingGroupIds.isEmpty()){
				query = new Query(Criteria.where("sharingGroupId").in(sharingGroupIds).and("sharingUserId").is(userId).and("listType").is("schedule").and("status").is("A"));
				query.sort().on("listId", Order.ASCENDING);
				details = mongoOperations.find(query, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			
				if(details!=null && !details.isEmpty()){
					for(MnNotesSharingDetails sharingDetails: details){
						
						if(sharingDetails.getNoteId().toString().equals("0"))
							query1=new Query(Criteria.where("sharingUserId").is(userId).and("listType").is("schedule").and("status").is("A").and("sharingStatus").is("A").and("sharingGroupId").is(sharingDetails.getSharingGroupId()).and("listId").is(sharingDetails.getListId()));
						else
							if(type.equals("InitialLoad"))
								query1=new Query(Criteria.where("sharingUserId").is(userId).and("listType").is("schedule").and("status").is("A").and("sharingStatus").ne("D").and("sharingGroupId").is(sharingDetails.getSharingGroupId()).and("listId").is(sharingDetails.getListId()).and("noteId").is(sharingDetails.getNoteId()));
							else
								query1=new Query(Criteria.where("sharingUserId").is(userId).and("listType").is("schedule").and("status").is("A").and("sharingStatus").is("A").and("sharingGroupId").is(sharingDetails.getSharingGroupId()).and("listId").is(sharingDetails.getListId()).and("noteId").is(sharingDetails.getNoteId()));	
								
						sharedDetails.addAll(mongoOperations.find(query1, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS));
					}
				
					List<Integer> eventIds=new ArrayList<Integer>();
				
					if(sharedDetails!=null && !sharedDetails.isEmpty())
					{
						for(MnEventsSharingDetails sharingDet: sharedDetails){
							
							Query query2=new Query(Criteria.where("listId").is(sharingDet.getListId()).and("listType").is("schedule").and("status").is("A").and("enableDisableFlag").is(true));
							MnList mnList=mongoOperations.findOne(query2, MnList.class,JavaMessages.Mongo.MNLIST);
							if(mnList!=null){
								
									query = new Query(Criteria.where("eventId").is(sharingDet.getNoteId()).and("status").is("A").and("listId").is(mnList.getListId()));
									
									eventList.addAll(mongoOperations.find(query, MnEventDetails.class, JavaMessages.Mongo.MNEVENTS));
							}
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			logger.error("Exception in fetchGroupSharedEventBasedOnList method:"+e);

		}
		if(logger.isDebugEnabled())
		logger.debug("fetchGroupSharedEventBasedOnList method successfully returned");
		return eventList;
	
	}

	@Override
	public String deleteEvents(Integer listId, Integer userId)
	{
		if(logger.isDebugEnabled())
		logger.debug("deleteEvents method called "+userId);
		try
		{
			Query query=new Query(Criteria.where("listId").is(listId).and("userId").is(userId));
			Update update=new Update();
			update.set("status", "I");
			mongoOperations.updateMulti(query, update,JavaMessages.Mongo.MNEVENTS);
			
			List<MnEventsSharingDetails> sharingDetails=mongoOperations.find(query, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
			if(sharingDetails!=null && !sharingDetails.isEmpty())
			mongoOperations.updateMulti(query, update, JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
			
			MnEventDetails mnEventDetails=mongoOperations.findOne(query, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS);
			
			MnUsers mnUsers = getUserDetailsObject(userId);
			writeLog(userId,  "A", "Deleted event :"+changedNoteNameWithDouble(mnEventDetails.getEventName())+" in "+ mnEventDetails.getListName()  ,  mnEventDetails.getListType(),  mnEventDetails.getListId().toString() , mnEventDetails.getcId().toString() , null,  mnEventDetails.getListType());
			if(logger.isDebugEnabled())
			logger.debug("deleteEvents method successfully returned");			
			return "1";
		}
		catch (Exception e) {
			logger.error("Exception in deleteEvents method:"+e);
			return "0";
		}
		
	}

	@Override
	public List<MnList> fetchCalendarSharingMembers(Integer userId, String listType)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchCalendarSharingMembers method called "+userId);
		List<MnList> lists=new ArrayList<MnList>();
		List<MnList> ltt=new ArrayList<MnList>();
		List<MnNotesSharingDetails> sharingDetails=new ArrayList<MnNotesSharingDetails>();
		List<MnGroupDetailsDomain> groupDetails=new ArrayList<MnGroupDetailsDomain>();
		List<MnEventsSharingDetails> sharedDetails=new ArrayList<MnEventsSharingDetails>();
		
		try
		{
			/* This block is used to getting information of Own Event lists */
			Query query = new Query(Criteria.where("userId").is(userId).and("status").is("A").and("enableDisableFlag").is(true));
			ltt.addAll(mongoOperations.find(query, MnList.class,JavaMessages.Mongo.MNLIST));
			
			if(ltt!=null && !ltt.isEmpty())
			{
				for(MnList lss:ltt)
				{
					boolean flag=false;
					if(lss.getMnNotesDetails()!=null && !lss.getMnNotesDetails().isEmpty())
					{
						for(String mnNotes:lss.getMnNotesDetails())
						{
							JSONObject obj=new JSONObject(mnNotes);
							if(obj.getString("status").equalsIgnoreCase("A"))
							{
								flag=true;
							}
						}
						if(flag)
						{
						if(lists!=null)
							lists.add(lss);
						}
					}
				}
			}
			/*End of the block*/
			
			/* This block is used to getting information of Individual sharing and Sharing via contact list */
			Query query1  = new Query(Criteria.where("sharingUserId").is(userId).and("listType").is("schedule").and("status").is("A"));
			sharingDetails=mongoOperations.find(query1, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			Query query2=null;
			if(sharingDetails!=null && !sharingDetails.isEmpty())
			{
				for(MnNotesSharingDetails share:sharingDetails)
				{
					if(share.getNoteId().toString().equals("0"))
						query2=new Query(Criteria.where("sharingUserId").is(share.getSharingUserId()).and("listType").is("schedule").and("status").is("A").and("sharingStatus").is("A").and("sharingGroupId").is(0).and("listId").is(share.getListId()));
					else
						query2=new Query(Criteria.where("sharingUserId").is(share.getSharingUserId()).and("listType").is("schedule").and("status").is("A").and("sharingStatus").is("A").and("sharingGroupId").is(0).and("listId").is(share.getListId()).and("noteId").is(share.getNoteId()));	
					
					sharedDetails.addAll(mongoOperations.find(query2, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS));
				}
			}
			
			
			if(sharedDetails!=null && !sharedDetails.isEmpty())
			{
				for(MnEventsSharingDetails share:sharedDetails)
				{
					Query query3 = new Query(Criteria.where("listId").is(share.getListId()).and("status").is("A").and("enableDisableFlag").is(true));
					if(lists!=null)
					lists.addAll(mongoOperations.find(query3, MnList.class,JavaMessages.Mongo.MNLIST));		
				}
			}
			
			/*End of the block*/
			
			
			/* This block is used to getting information of Sharing Group wise lists */
			if(sharedDetails!=null && !sharedDetails.isEmpty())
			sharedDetails.clear();
			if(sharingDetails!=null && !sharingDetails.isEmpty())
				sharingDetails.clear();
			Query query4=new Query(Criteria.where("userId").is(userId).and("status").is("A"));
			groupDetails=mongoOperations.find(query4, MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);
			
			if(groupDetails!=null && !groupDetails.isEmpty())
			{
				List<Integer> groupIds=new ArrayList<Integer>();
				for(MnGroupDetailsDomain details:groupDetails)
				{
				       if(groupIds!=null)
				    	   groupIds.add(details.getGroupId());
				}
				Query grpquery=new Query(Criteria.where("sharingGroupId").in(groupIds).and("status").is("A"));
				sharingDetails=mongoOperations.find(grpquery, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				Query query5=null;
				if(sharingDetails!=null && !sharingDetails.isEmpty())
				{
					for(MnNotesSharingDetails share:sharingDetails)
					{
						if(share.getNoteId().toString().equals("0"))
							query5=new Query(Criteria.where("listType").is("schedule").and("status").is("A").and("sharingStatus").is("A").and("sharingGroupId").is(share.getSharingGroupId()).and("listId").is(share.getListId()));
						else
							query5=new Query(Criteria.where("listType").is("schedule").and("status").is("A").and("sharingStatus").is("A").and("sharingGroupId").is(share.getSharingGroupId()).and("listId").is(share.getListId()).and("noteId").is(share.getNoteId()));
						sharedDetails.addAll(mongoOperations.find(query5, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS));
					}
					
				}
				if(sharedDetails!=null && !sharedDetails.isEmpty())
				{
					for(MnEventsSharingDetails share:sharedDetails)
					{
						Query query6 = new Query(Criteria.where("listId").is(share.getListId()).and("status").is("A").and("enableDisableFlag").is(true));
						if(lists!=null)
						lists.addAll(mongoOperations.find(query6, MnList.class,JavaMessages.Mongo.MNLIST));
					}
				}
				
			}
			
		
		}
		catch (Exception e) {
			logger.error("Exception in fetchCalendarSharingMembers method:"+e);
		}
		if(logger.isDebugEnabled())
			logger.debug("fetchCalendarSharingMembers method successfully returned");
		return lists;
	}

	@Override
	public List<MnUsers> fetchCalendarSharingContacts(Integer userId, String listType)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchCalendarSharingContacts method called "+userId);
		List<String> status=null;
		List<MnUsers> users=null;
		List<MnNotesSharingDetails> sharingDetails=null;
		List<MnFriends> friends=null;
		List<MnList> lists=new ArrayList<MnList>();
		List<MnUsers> selectedUsers=new ArrayList<MnUsers>();
		try
		{
			Query query=new Query(Criteria.where("userId").is(userId).and("status").is("A"));
			friends=mongoOperations.find(query, MnFriends.class,JavaMessages.Mongo.MNFRIEND);
			if(friends!=null && !friends.isEmpty())
			{
				List<Integer> userIds=new ArrayList<Integer>();
				for(MnFriends details:friends)
				{
				       if(userIds!=null)
				    	   userIds.add(details.getRequestedUserId());
				}
				Query usrquery=new Query(Criteria.where("userId").in(userIds).and("status").is("A"));
				users=mongoOperations.find(usrquery, MnUsers.class, JavaMessages.Mongo.MNUSERS);
			}
			if(lists!=null && !lists.isEmpty())
			{
				lists.clear();
			}
			if(users!=null && !users.isEmpty())
			{
				for(MnUsers user:users)
				{
					Query query2=new Query(Criteria.where("userId").is(user.getUserId()).and("listType").is("schedule").and("status").is("A"));
					lists=mongoOperations.find(query2,MnList.class,JavaMessages.Mongo.MNLIST);
					if(lists!=null && !lists.isEmpty())
					{
						    for(MnList li:lists)
						    {
				                if(li.getMnNotesDetails()!=null && !li.getMnNotesDetails().isEmpty())
				                {
				                	for(String detail:li.getMnNotesDetails())
				                	{
				                		JSONObject jsonObject=null;
				                		jsonObject=new JSONObject(detail);
				                		if(jsonObject.getString("eventSharedAllContact").equals("1"))
				                		{
				                			selectedUsers.add(user);
				                			break;
				                		}
				                	}
				                }
						    }
					}
				}
			}
			
			
			
			
			
		}
		catch (Exception e) {
			logger.error("Exception in fetchCalendarSharingContacts method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchCalendarSharingContacts method successfully returned");
		return selectedUsers;
	}

	@Override
	public List<MnUsers> fetchCalendarSharingGroups(Integer userId, String listType)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchCalendarSharingGroups method called "+userId);
		List<String> status=null;
		List<MnUsers> users=null;
		List<MnNotesSharingDetails> sharingDetails=null;
		List<MnGroupDetailsDomain> groupDetails=null;
		try
		{
			Query query=new Query(Criteria.where("userId").is(userId).and("status").is("A"));
			groupDetails=mongoOperations.find(query, MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);
			
			if(groupDetails!=null && !groupDetails.isEmpty())
			{
				List<Integer> groupIds=new ArrayList<Integer>();
				for(MnGroupDetailsDomain details:groupDetails)
				{
				       if(groupIds!=null)
				    	   groupIds.add(details.getGroupId());
				}
				Query grpquery=new Query(Criteria.where("sharingGroupId").in(groupIds).and("status").is("A"));
				sharingDetails=mongoOperations.find(grpquery, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				if(sharingDetails!=null && !sharingDetails.isEmpty())
				{
					List<Integer> userIds=new ArrayList<Integer>();
					for(MnNotesSharingDetails details:sharingDetails)
					{
						Query query1=new Query(Criteria.where("listType").is("schedule").and("status").is("A").and("sharingStatus").is("A").and("sharingGroupId").is(details.getSharingGroupId()));
						List<MnEventsSharingDetails> sharedDetails=mongoOperations.find(query1, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
						if(sharedDetails!=null && !sharedDetails.isEmpty())
						{
							for(MnEventsSharingDetails det:sharedDetails)
							{
							if(userIds!=null)
						    	   userIds.add(det.getUserId());
							}
						}
					}
					Query usrquery=new Query(Criteria.where("userId").in(userIds).and("status").is("A"));
					users=mongoOperations.find(usrquery, MnUsers.class, JavaMessages.Mongo.MNUSERS);
				}
			}
			
			
			
		}
		catch (Exception e) {
			logger.error("Exception in fetchCalendarSharingGroups method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchCalendarSharingGroups method successfully returned");
		return users;	
	}

	@Override
	public List<MnEventDetails> fetchOwnEvents(Integer userId, String listType)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchOwnEvents method called " +userId);
		List<MnEventDetails> ownEvents=new ArrayList<MnEventDetails>();
		List<MnList> lists=new ArrayList<MnList>();
		List<MnEventDetails> events=new ArrayList<MnEventDetails>();
		try
		{
			Query query2=new Query(Criteria.where("userId").is(userId).and("listType").is("schedule").and("status").is("A"));
			lists=mongoOperations.find(query2,MnList.class,JavaMessages.Mongo.MNLIST);
			
			for(MnList mnList:lists)
			{
				if(mnList.isEnableDisableFlag())
				{
					if(mnList.getMnNotesDetails()!=null && !mnList.getMnNotesDetails().isEmpty())
	                {
	                	for(String detail:mnList.getMnNotesDetails())
	                	{
	                		JSONObject jsonObject=null;
	                		jsonObject=new JSONObject(detail);
	                			
	                			MnEventDetails eventDetails=new MnEventDetails();
	                			eventDetails.setListId(mnList.getListId());
	                			eventDetails.setEventId(Integer.parseInt(jsonObject.getString("eventId")));
	                			eventDetails.setUserId(userId);
	                			eventDetails.setListType(mnList.getListType());
	                			if(events!=null)
	                			{
	                				events.add(eventDetails);
	                			}
	                	}
	                }
				}
				
			}
			if(events!=null && !events.isEmpty())
			{
			       for(MnEventDetails det:events)
			       {
			    	   Query query3=new Query(Criteria.where("listId").is(det.getListId()).and("eventId").is(det.getEventId()).and("listType").is("schedule").and("status").is("A").and("userId").is(userId));
			    	   		if(ownEvents!=null)
			    	   			ownEvents.addAll(mongoOperations.find(query3, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS));
			       }
			}
		}
		catch (Exception e) {
			logger.error("Exception in fetchOwnEvents method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchOwnEvents method successfully returned");
		return ownEvents;
	}

	@Override
	public List<MnEventDetails> fetchSharingContactEvents(Integer selectedUserId, String listType,Integer userId)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchSharingContactEvents method called" +userId+" selectedUserId: "+selectedUserId);
		List<String> status=null;
		List<MnList> lists=new ArrayList<MnList>();
		List<MnEventDetails> events=new ArrayList<MnEventDetails>();
		List<MnEventDetails> completeEvents=new ArrayList<MnEventDetails>();
		
		try
		{	
					Query query2=new Query(Criteria.where("userId").is(selectedUserId).and("listType").is("schedule").and("status").is("A"));
					lists=mongoOperations.find(query2,MnList.class,JavaMessages.Mongo.MNLIST);
					if(lists!=null && !lists.isEmpty())
					{
						    for(MnList li:lists)
						    {
						    	if(li.isEnableDisableFlag()) // to check the list is eanble or not
						    	{
				                if(li.getMnNotesDetails()!=null && !li.getMnNotesDetails().isEmpty())
				                {
				                	for(String detail:li.getMnNotesDetails())
				                	{
				                		JSONObject jsonObject=null;
				                		jsonObject=new JSONObject(detail);
				                		if(jsonObject.getString("eventSharedAllContact").equals("1"))
				                		{
				                			
				                			MnEventDetails eventDetails=new MnEventDetails();
				                			eventDetails.setListId(li.getListId());
				                			eventDetails.setEventId(Integer.parseInt(jsonObject.getString("eventId")));
				                			eventDetails.setUserId(selectedUserId);
				                			eventDetails.setListType(li.getListType());
				                			if(events!=null)
				                			{
				                				events.add(eventDetails);
				                			}
				                			
				                		}
				                	}
				                }
						    	}
						    }
					}
					if(events!=null && !events.isEmpty())
					{
					       for(MnEventDetails det:events)
					       {
					    	   Query query3=new Query(Criteria.where("listId").is(det.getListId()).and("noteId").is(det.getEventId()).and("listType").is("schedule").and("status").is("A").and("sharingUserId").is(userId).and("userId").is(selectedUserId).and("sharingStatus").is("A").and("sharingGroupId").is(0));
					    	   List<MnEventsSharingDetails> sdetails=mongoOperations.find(query3, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
					    	   if(sdetails!=null && !sdetails.isEmpty())
					    	   {
					    		   for(MnEventsSharingDetails ssdet:sdetails)
					    		   {
					    			   Query query1=new Query(Criteria.where("listId").is(ssdet.getListId()).and("eventId").is(ssdet.getNoteId()).and("listType").is("schedule").and("status").is("A"));
							    	   if(completeEvents!=null)
							    	   {
							    		   completeEvents.addAll(mongoOperations.find(query1, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS));
							    	   }   
					    		   }
					    		      
					    	   }
					    	   
					       }
					}
		}
		catch (Exception e) {
			logger.error("Exception in fetchSharingContactEvents method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchSharingContactEvents method successfully returned");
		return completeEvents;
		
	}

	@Override
	public List<MnEventDetails> fetchSharingGroupAndMemberEvents(Integer userId,String listType,Integer SelectedUserId)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchSharingGroupAndMemberEvents method called "+userId+" SelectedUserId: "+SelectedUserId);
		List<MnEventDetails> memberAndGroupBaseEvents=new ArrayList<MnEventDetails>();
		
		List<MnEventDetails> totalEvents=new ArrayList<MnEventDetails>();
		List<MnNotesSharingDetails> sharingEvents=new ArrayList<MnNotesSharingDetails>();
		List<MnNotesSharingDetails> sharingDetails=new ArrayList<MnNotesSharingDetails>();
		List<MnGroupDetailsDomain> groupDetails=new ArrayList<MnGroupDetailsDomain>();
		List<MnEventsSharingDetails> events=new ArrayList<MnEventsSharingDetails>();
		try
		{
			Query query1=new Query(Criteria.where("userId").is(SelectedUserId).and("sharingUserId").is(userId).and("status").is("A").and("listType").is("schedule"));
			sharingEvents=mongoOperations.find(query1, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
			if(sharingEvents!=null && !sharingEvents.isEmpty())
			{
				for(MnNotesSharingDetails det:sharingEvents)
				{
					MnEventDetails details=new MnEventDetails();
					details.setListId(det.getListId());
					details.setEventId(det.getNoteId());
					details.setListType(det.getListType());
					if(memberAndGroupBaseEvents!=null)
					{
						memberAndGroupBaseEvents.add(details);
					}
				}
			}
			if(memberAndGroupBaseEvents!=null && !memberAndGroupBaseEvents.isEmpty())
			{
				for(MnEventDetails medetails:memberAndGroupBaseEvents)
				{
					Query query3=new Query(Criteria.where("listId").is(medetails.getListId()).and("noteId").is(medetails.getEventId()).and("listType").is("schedule").and("status").is("A").and("sharingUserId").is(userId).and("userId").is(SelectedUserId).and("sharingStatus").is("A").and("sharingGroupId").is(0));
			    	   List<MnEventsSharingDetails> sdetails=mongoOperations.find(query3, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
			    	   if(sdetails!=null && !sdetails.isEmpty())
			    	   {
			    		   for(MnEventsSharingDetails ssdet:sdetails)
			    		   {
			    			   if(events!=null)
			    				   events.add(ssdet);
			    		   }
			    	   }
				}
			}
			
			
			Query query2=new Query(Criteria.where("userId").is(userId).and("status").is("A"));
			groupDetails=mongoOperations.find(query2, MnGroupDetailsDomain.class,JavaMessages.Mongo.MNGROUPDETAILS);
			
			if(groupDetails!=null && !groupDetails.isEmpty())
			{
				List<Integer> groupIds=new ArrayList<Integer>();
				for(MnGroupDetailsDomain details:groupDetails)
				{
				       if(groupIds!=null)
				    	   groupIds.add(details.getGroupId());
				}
				if(groupIds!=null && !groupIds.isEmpty())
				{
					for(Integer Id:groupIds)
					{
						Query grpquery=new Query(Criteria.where("sharingGroupId").is(Id).and("status").is("A").and("userId").is(SelectedUserId));
						sharingDetails.addAll(mongoOperations.find(grpquery, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS));
					}
				}
				if(memberAndGroupBaseEvents!=null && !memberAndGroupBaseEvents.isEmpty())
				{
					memberAndGroupBaseEvents.clear();
				}
				if(sharingDetails!=null && !sharingDetails.isEmpty())
				{
					for(MnNotesSharingDetails det:sharingDetails)
					{
						MnEventDetails details=new MnEventDetails();
						details.setListId(det.getListId());
						details.setEventId(det.getNoteId());
						details.setListType(det.getListType());
						details.setGroupId(det.getSharingGroupId());
						if(memberAndGroupBaseEvents!=null)
						{
							memberAndGroupBaseEvents.add(details);
						}
					}
				}
			}
			if(memberAndGroupBaseEvents!=null && !memberAndGroupBaseEvents.isEmpty())
			{
				for(MnEventDetails medetails:memberAndGroupBaseEvents)
				{
					Query query3=new Query(Criteria.where("listId").is(medetails.getListId()).and("noteId").is(medetails.getEventId()).and("listType").is("schedule").and("status").is("A").and("sharingUserId").is(userId).and("userId").is(SelectedUserId).and("sharingStatus").is("A").and("sharingGroupId").is(medetails.getGroupId()));
			    	   List<MnEventsSharingDetails> sdetails=mongoOperations.find(query3, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
			    	   if(sdetails!=null && !sdetails.isEmpty())
			    	   {
			    		   for(MnEventsSharingDetails ssdet:sdetails)
			    		   {
			    			   if(events!=null)
			    				   events.add(ssdet);
			    		   }
			    	   }
				}
			}
			
			if(events!=null && !events.isEmpty())
			{
				for(MnEventsSharingDetails eventDet:events)
				{
					Query query=new Query(Criteria.where("listId").is(eventDet.getListId()).and("listType").is("schedule").and("status").is("A"));
						MnList mnList=mongoOperations.findOne(query, MnList.class,JavaMessages.Mongo.MNLIST);
						
						if(mnList!=null && mnList.isEnableDisableFlag())
						{
							Query query3=new Query(Criteria.where("listId").is(eventDet.getListId()).and("eventId").is(eventDet.getNoteId()).and("listType").is("schedule").and("status").is("A"));
							if(totalEvents!=null)
							{
								totalEvents.addAll(mongoOperations.find(query3, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS));
							}
						}
				}
			}
		}
		catch (Exception e) {
			logger.error("Exception in fetchSharingGroupAndMemberEvents method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchSharingGroupAndMemberEvents method successfully returned");
		return totalEvents;
	}	

	public MnEventDetails getEvent(String listId, String noteId,String userId)
	{
		if(logger.isDebugEnabled())
		logger.debug("getEvent method called"+userId);
		MnEventDetails event=null;
		try{
			Query query = new Query(Criteria.where("eventId").is(Integer.parseInt(noteId)).and("listId").is(Integer.parseInt(listId)).and("userId").is(Integer.parseInt(userId)).and("status").is("A"));
			
			event = mongoOperations.findOne(query, MnEventDetails.class, JavaMessages.Mongo.MNEVENTS);
		}
		catch (Exception e) {
			logger.error("Exception in getEvent method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("getEvent method successfully returned");
		return event;
	}

	@Override
	public MnEventDetails getEventForMail(String listId, String noteId) 
	{
		if(logger.isDebugEnabled())
		logger.debug("getEventForMail method called noteId" +noteId);
		MnEventDetails event=null;
		try{
			
			Query query = new Query(Criteria.where("eventId").is(Integer.parseInt(noteId)).and("listId").is(Integer.parseInt(listId)).and("status").is("A"));
			event = mongoOperations.findOne(query, MnEventDetails.class, JavaMessages.Mongo.MNEVENTS);
		}
		catch (Exception e) {
			logger.error("Exception in getEventForMail method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("getEventForMail method successfully returned");
		return event;
	}
	
	@Override
	public String updateEvent(MnEventDetails mnEvents ,String emailFlag) 
	{
		if(logger.isDebugEnabled())
		logger.debug("updateEvent method called "+emailFlag);
		Query query2=null;
		Integer id;
		boolean mailFlag=false;
		String offSet=null;
		try
		{
			
			Query query=new Query(Criteria.where("eventId").is((mnEvents.getEventId())).and("listId").is(mnEvents.getListId()));
			Update update=new Update();
			
			update.set("eventName", mnEvents.getEventName());
			update.set("description", mnEvents.getDescription());
			update.set("alldayevent", mnEvents.getAlldayevent());
			update.set("startDate", mnEvents.getStartDate());
			update.set("endDate", mnEvents.getEndDate());
			update.set("location", mnEvents.getLocation());
			
			update.set("eventStartDate", mnEvents.getEventStartDate());
			update.set("eventEndDate", mnEvents.getEventEndDate());
			update.set("repeatEvent", mnEvents.getRepeatEvent());
			
			mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNEVENTS);
			
			MnEventDetails mnEventDetails=mongoOperations.findOne(query, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS);
			
			
			Query query1 = new Query(Criteria.where("listId").is(mnEvents.getListId()).and("eventId").is(mnEvents.getEventId()).and("userId").is(mnEvents.getUserId()).and("cId").is(mnEventDetails.getcId()));
			mongoOperations.remove(query1, JavaMessages.Mongo.MNESUBVENTS);
			query2=new Query();
			 List<MnSubEventDetails> subEventList=mongoOperations.find(query2, MnSubEventDetails.class,JavaMessages.Mongo.MNESUBVENTS);
		        if(subEventList!=null && !subEventList.isEmpty())
				{
					MnSubEventDetails subEventdetail=subEventList.get(subEventList.size()-1);
					id=subEventdetail.getId()+0;
				}
				else
				{
					id=0;
				}
					
			List<Date> dates = new ArrayList<Date>();
			
			if(emailFlag.equalsIgnoreCase("True"))
				mailFlag=true;
			
			Query query3 = new Query(Criteria.where("userId").is(mnEventDetails.getUserId()));
   			MnUsers mnUser = mongoOperations.findOne(query3, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			offSet=mnUser.getTimeZone();
			
			SimpleDateFormat formatter = new SimpleDateFormat("MM/d/yyyy");
			if (mnEvents.getRepeatEvent() != null && mnEvents.getRepeatEvent().equals("Daily"))
			{
				try
				{
					Date startDate = (Date) formatter.parse(mnEvents.getEventStartDate());
					Date endDate = (Date) formatter.parse(mnEvents.getEventEndDate());
					long interval = 24 * 1000 * 60 * 60; 
					long endTime = endDate.getTime(); 
					long curTime = startDate.getTime();
					while (curTime <= endTime)
					{
						dates.add(new Date(curTime));
						curTime += interval;
					}
					String startTime[] = mnEvents.getStartDate().split(" ");
					String endTimes[] = mnEvents.getEndDate().split(" ");
				
			       
					for (int i = 0; i < dates.size(); i++)
					{
						
						MnSubEventDetails details2 = new MnSubEventDetails();
						details2.setEventId(mnEvents.getEventId());
						details2.setAlldayevent(mnEvents.getAlldayevent());
						details2.setEventName(mnEvents.getEventName());
						details2.setDescription(mnEvents.getDescription());
						details2.setListId(mnEvents.getListId());
						details2.setLocation(mnEvents.getLocation());
						details2.setUserId(mnEventDetails.getUserId());
						details2.setC_id(mnEventDetails.getcId());
						details2.setRepeatEvent(mnEventDetails.getRepeatEvent());
						details2.setEmailFlag(mailFlag);
						details2.setTimeZone(offSet);
						Date lDate = (Date) dates.get(i);
						String ds = formatter.format(lDate);
						if (mnEvents.getAlldayevent())
						{
							details2.setStartDate(ds);
							details2.setEndDate(ds);
						}
						else
						{
							details2.setStartDate(ds + " " + startTime[1]);
							details2.setEndDate(ds + " " + endTimes[1]);
						}
						details2.setStatus("A");
						
						
						details2.setId(++id);
						mongoOperations.insert(details2, JavaMessages.Mongo.MNESUBVENTS);
						
						
						
					}
				}
				catch (Exception e)
				{
					logger.error("Exception in updateEvent method:"+e);
				}
			}
			else if (mnEventDetails.getRepeatEvent() != null && mnEventDetails.getRepeatEvent().equals("Weekly"))
			{
				try
				{
					Date startDate = (Date) formatter.parse(mnEventDetails.getEventStartDate());
					Date endDate = (Date) formatter.parse(mnEventDetails.getEventEndDate());
					long interval = 24 * 1000 * 60 * 60 * 7; 
					long endTime = endDate.getTime(); 
					long curTime = startDate.getTime();
					while (curTime <= endTime)
					{
						dates.add(new Date(curTime));
						curTime += interval;
					}
					String startTime[] = mnEventDetails.getStartDate().split(" ");
					String endTimes[] = mnEventDetails.getEndDate().split(" ");
					for (int i = 0; i < dates.size(); i++)
					{
						MnSubEventDetails details2 = new MnSubEventDetails();
						details2.setEventId(mnEventDetails.getEventId());
						details2.setAlldayevent(mnEventDetails.getAlldayevent());
						details2.setEventName(mnEventDetails.getEventName());
						details2.setDescription(mnEventDetails.getDescription());
						details2.setListId(mnEventDetails.getListId());
						details2.setLocation(mnEventDetails.getLocation());
						details2.setUserId(mnEventDetails.getUserId());
						details2.setC_id(mnEventDetails.getcId());
						details2.setRepeatEvent(mnEventDetails.getRepeatEvent());
						details2.setEmailFlag(mailFlag);
						details2.setTimeZone(offSet);
						Date lDate = (Date) dates.get(i);
						String ds = formatter.format(lDate);
						if (mnEventDetails.getAlldayevent())
						{
							details2.setStartDate(ds);
							details2.setEndDate(ds);
						}
						else
						{
							details2.setStartDate(ds + " " + startTime[1]);
							details2.setEndDate(ds + " " + endTimes[1]);
						}
						details2.setStatus("A");
						
						
						details2.setId(++id);
						mongoOperations.insert(details2, JavaMessages.Mongo.MNESUBVENTS);
					}
				}
				catch (Exception e)
				{
					logger.error("Exception in updateEvent method:"+e);
				}
			}
			else if (mnEventDetails.getRepeatEvent() != null && mnEventDetails.getRepeatEvent().equals("Monthly"))
			{
				try
				{
					Date startDate = (Date) formatter.parse(mnEventDetails.getEventStartDate());
					Date endDate = (Date) formatter.parse(mnEventDetails.getEventEndDate());
					Calendar cal2 = Calendar.getInstance();
					cal2.setTime(startDate);
					Calendar cal3 = Calendar.getInstance();
					cal3.setTime(endDate);
					int difInYear = cal3.get(Calendar.YEAR)- cal2.get(Calendar.YEAR);
					int difInMonths = cal3.get(Calendar.MONTH)-cal2.get(Calendar.MONTH);
					difInMonths+=difInYear*12;
					String startTime[] = mnEventDetails.getStartDate().split(" ");
					String endTimes[] = mnEventDetails.getEndDate().split(" ");
					for (int i = 0; i <= difInMonths; i++)
					{
						Calendar calStart = Calendar.getInstance();
						calStart.setTime(startDate);
						calStart.add(Calendar.MONTH, i);
						Calendar calEnd = Calendar.getInstance();
						calEnd.setTime((Date)formatter.parse(mnEventDetails.getEndDate()));
						calEnd.add(Calendar.MONTH, i);
						MnSubEventDetails details2 = new MnSubEventDetails();
						details2.setEventId(mnEventDetails.getEventId());
						details2.setAlldayevent(mnEventDetails.getAlldayevent());
						details2.setEventName(mnEventDetails.getEventName());
						details2.setDescription(mnEventDetails.getDescription());
						details2.setListId(mnEventDetails.getListId());
						details2.setLocation(mnEventDetails.getLocation());
						details2.setUserId(mnEventDetails.getUserId());
						details2.setC_id(mnEventDetails.getcId());
						details2.setRepeatEvent(mnEventDetails.getRepeatEvent());
						details2.setEmailFlag(mailFlag);
						details2.setTimeZone(offSet);
						Date sDate = (Date) calStart.getTime();
						Date eDate = (Date) calEnd.getTime();
						String ds = formatter.format(sDate);
						String es=formatter.format(eDate);
						if (mnEventDetails.getAlldayevent())
						{
							details2.setStartDate(ds);
							details2.setEndDate(es);
						}
						else
						{
							details2.setStartDate(ds + " " + startTime[1]);
							details2.setEndDate(es + " " + endTimes[1]);
						}
						details2.setStatus("A");
						
						
						details2.setId(++id);
						mongoOperations.insert(details2, JavaMessages.Mongo.MNESUBVENTS);
						
						
					}
				}
				catch (Exception e)
				{
					logger.error("Exception in updateEvent method:"+e);
				}
			}
			else
			{
				MnSubEventDetails details2 = new MnSubEventDetails();
				details2.setEventId(mnEventDetails.getEventId());
				details2.setAlldayevent(mnEventDetails.getAlldayevent());
				details2.setEventName(mnEventDetails.getEventName());
				details2.setDescription(mnEventDetails.getDescription());
				details2.setListId(mnEventDetails.getListId());
				details2.setLocation(mnEventDetails.getLocation());
				details2.setUserId(mnEventDetails.getUserId());
				details2.setRepeatEvent(mnEventDetails.getRepeatEvent());
				details2.setC_id(mnEventDetails.getcId());
				details2.setEmailFlag(mailFlag);
				details2.setTimeZone(offSet);
				
				String startD=mnEventDetails.getStartDate();
				String endD=mnEventDetails.getEndDate();
				
				if (mnEventDetails.getAlldayevent())
				{
					Date startDate = (Date) formatter.parse(startD);
					Date endDate = (Date) formatter.parse(endD);
					
					details2.setStartDate(formatter.format(startDate));
					details2.setEndDate(formatter.format(endDate));
					
				}
				else
				{
					SimpleDateFormat sdf = new SimpleDateFormat("MM/d/yyyy HH:mm");
					Date startDate = (Date) sdf.parse(startD);
					Date endDate = (Date) sdf.parse(endD);
					
					details2.setStartDate(sdf.format(startDate));
					details2.setEndDate(sdf.format(endDate));
				}
				details2.setStatus("A");
				
				
				details2.setId(++id);
				mongoOperations.insert(details2, JavaMessages.Mongo.MNESUBVENTS);
			}
			
			
			
			MnUsers mnUsers = getUserDetailsObject(mnEventDetails.getUserId());
			writeLog(mnEventDetails.getUserId(),  "A", "Updated details of event: "+changedNoteNameWithDouble(mnEventDetails.getEventName())+" in "+ mnEventDetails.getListName() ,  mnEventDetails.getListType(),  mnEventDetails.getListId().toString() , mnEventDetails.getcId().toString() , null,null);
			
			
		}catch (Exception e) {
			logger.error("Exception in updateEvent method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("updateEvent method successfully returned");
		return "success";
	}
	public String changedNoteNameWithDouble(String noteName){
		if(logger.isDebugEnabled())
			logger.debug("changedNoteNameWithDouble method called "+noteName);
		if(noteName.indexOf("\"")!= -1){
			noteName = noteName.replace("\"", "\\\"");
		}
		if(logger.isDebugEnabled())
		logger.debug("changedNoteNameWithDouble method successfully returned");
		return noteName;
	}
	@Override
	public String changeAcceptDecline(String userId, String listId,String noteId, String access) 
	{
		if(logger.isDebugEnabled())
		logger.debug("changeAcceptDecline method called "+userId);
		try
		{
		Query query = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("listId").is(Integer.parseInt(listId)).and("noteId").is(Integer.parseInt(noteId)).and("status").is("A"));
		Update update = new Update();
		update.set("sharingStatus", access);
		mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
		
		
		Query query1 = new Query(Criteria.where("notUserId").is(Integer.parseInt(userId)).and("noteId").is(noteId).and("listId").is(listId).and("pageName").is("schedule").and("logType").is("N"));
		MnLog mnlog=mongoOperations.findOne(query1,MnLog.class,JavaMessages.Mongo.MNLOG);
		
		Update update1=null;
		if(mnlog!=null && !mnlog.equals(""))
		{
		if(mnlog.getLogDescription().charAt(0)!='X')
		{
		String updated_logdesc="X"+mnlog.getLogDescription();
		update1 = new Update();
		update1.set("logDescription", updated_logdesc);
		mongoOperations.updateMulti(query1, update1,JavaMessages.Mongo.MNLOG );	
		}
		}
		
	}catch (Exception e) {
		logger.error("Exception in changeAcceptDecline method:"+e);
	}
	if(logger.isDebugEnabled())
	logger.debug("changeAcceptDecline method successfully returned");
	return "success";
	}

	@Override
	public List<MnEventDetails> fetchListsforCalendarCombo(Integer userId, String listType, Integer listId)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchListsforCalendarCombo method called "+userId);
		List<MnEventDetails> events=new ArrayList<MnEventDetails>();
		List<MnList> lists=new ArrayList<MnList>();
		List<MnEventsSharingDetails> sharingDetails=new ArrayList<MnEventsSharingDetails>();
		try
		{
			Query query = new Query(Criteria.where("listId").is(listId).and("listType").is("schedule").and("status").is("A").and("userId").is(userId).and("enableDisableFlag").is(true));
			MnList mnList1=mongoOperations.findOne(query, MnList.class, JavaMessages.Mongo.MNLIST);
			lists.add(mnList1);
			
			if(lists!=null && !lists.isEmpty() && !lists.contains(null))
			{
				if(userId.equals(mnList1.getUserId()))
				{
					for(String str :mnList1.getMnNotesDetails())
						{
							if (str.contains("\"ownerEventStatus\":\"A\"")&& str.contains("\"status\":\"A\""))
							{
								 JSONObject jsonObject=new JSONObject(str);
								 String eventId=(String) jsonObject.get("eventId");
								 Query query1 = new Query(Criteria.where("listId").is(listId).and("eventId").is(Integer.parseInt(eventId)).and("listType").is("schedule").and("status").is("A").and("userId").is(userId));
								 events.addAll(mongoOperations.find(query1, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS));
							}
						}	
				}
			}
			else
			{
				Query query2 = new Query(Criteria.where("listId").is(listId).and("status").is("A"));
				MnList list=mongoOperations.findOne(query2, MnList.class,JavaMessages.Mongo.MNLIST);
				if(list!=null)
				{
					if(list.getUserId().equals(userId))//Same user but whether it is not enabled and whether it is not a schedule type like that
					{
						//Nothing do..
					}
					else                              //Different user, this event is a shared one for this user.
					{
						if(list.isEnableDisableFlag())
						{
							Query query3 = new Query(Criteria.where("listId").is(listId).and("status").is("A").and("sharingStatus").is("A").and("userId").is(list.getUserId()).and("sharingUserId").is(userId).and("listType").is("schedule"));
							sharingDetails.addAll(mongoOperations.find(query3, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS));
							if(sharingDetails!=null && !sharingDetails.isEmpty())
							{
							     	for(MnEventsSharingDetails det:sharingDetails)
							     	{
							     		Query query4 = new Query(Criteria.where("listId").is(det.getListId()).and("eventId").is(det.getNoteId()).and("status").is("A"));
							     		if(events!=null)
							     			events.addAll(mongoOperations.find(query4, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS));
							     	}
							}
						}
						
					}
				}
				
				
			}
			
		}
		catch (Exception e) {
			logger.error("Exception in fetchListsforCalendarCombo method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchListsforCalendarCombo method successfully returned");
		return events;
	}

	@Override
	public List<MnEventDetails> fetchParticularEvent(Integer listId, Integer eventId, Integer userId)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchParticularEvent method called "+userId);
		List<MnEventDetails> details=new ArrayList<MnEventDetails>();
		try
		{
			Query query=new Query(Criteria.where("listId").is(listId).and("eventId").is(eventId).and("userId").is(userId).and("status").is("A"));
			details=mongoOperations.find(query, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS);
			if(details!=null && !details.isEmpty())
			{
				//Nothing do..
			}
			else
			{
				Query query1 = new Query(Criteria.where("listId").is(listId).and("noteId").is(eventId).and("sharingUserId").in(userId).and("status").is("A").and("sharingGroupId").is(0));
				List<MnNotesSharingDetails> sharing=mongoOperations.find(query1, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				if(sharing!=null && !sharing.isEmpty())
				{
					for(MnNotesSharingDetails det:sharing)
					{
						Query query5 = new Query(Criteria.where("listId").is(det.getListId()).and("noteId").is(det.getNoteId()).and("status").is("A").and("sharingUserId").is(userId).and("sharingGroupId").is(0));
						List<MnEventsSharingDetails> eventDetails=mongoOperations.find(query5, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
						if(eventDetails!=null && !eventDetails.isEmpty())
						{
							for(MnEventsSharingDetails evnt:eventDetails)
							{
								Query query2=new Query(Criteria.where("listId").is(evnt.getListId()).and("eventId").is(evnt.getNoteId()).and("status").is("A"));
								details.addAll(mongoOperations.find(query2, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS));
							}
						
						}
					}
					
				}
				else
				{
					Query query3 = new Query(Criteria.where("listId").is(listId).and("noteId").is(eventId).and("sharingUserId").is(0).and("status").is("A"));
					List<MnNotesSharingDetails> sharing1=mongoOperations.find(query3, MnNotesSharingDetails.class,JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
					if(sharing1!=null && !sharing1.isEmpty())
					{
						for(MnNotesSharingDetails det:sharing1)
						{
						Query query6 = new Query(Criteria.where("listId").is(det.getListId()).and("noteId").is(det.getNoteId()).and("status").is("A").and("sharingUserId").is(userId));
						List<MnEventsSharingDetails> eventDetails=mongoOperations.find(query6, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
						if(eventDetails!=null && !eventDetails.isEmpty())
						{
							for(MnEventsSharingDetails evnt:eventDetails)
							{
								Query query4=new Query(Criteria.where("listId").is(evnt.getListId()).and("eventId").is(evnt.getNoteId()).and("status").is("A"));
								details.addAll(mongoOperations.find(query4, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS));
							}
						}
						
						}
					}
					
				}
			}
		}
		catch (Exception e) {
			logger.error("Exception in fetchParticularEvent method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchParticularEvent method successfully returned");
		return details;
	}
	
	public List<MnEventDetails> getEventsFromList(Integer listId, List<Integer> eventId, Integer userId){
		if(logger.isDebugEnabled())
		logger.debug("getEventsFromList method called "+userId);
		List<MnEventDetails> details = null;
		List<MnEventDetails> detailsReturn = new ArrayList<MnEventDetails>();
		try{
			
			Query query=new Query(Criteria.where("listId").is(listId).and("eventId").in(eventId).and("status").is("A"));
			details=mongoOperations.find(query, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS);
			
			
			for(MnEventDetails event:details)
			{
				if(userId.equals(event.getUserId()))
				{
						detailsReturn.add(event);
				}
				else
				{
					MnEventsSharingDetails sharedDetails=null;
					Query query1=new Query(Criteria.where("sharingUserId").is(userId).and("status").is("A").and("sharingStatus").is("A").and("listId").is(event.getListId()).and("noteId").is(event.getEventId()));
					sharedDetails=mongoOperations.findOne(query1, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
					
					if(sharedDetails!=null)
					detailsReturn.add(event);
				}
				
			}
			
		}catch (Exception e) {
			logger.error("Exception in getEventsFromList method:"+e);
		}
		if(logger.isDebugEnabled())
			logger.debug("getEventsFromList method successfully returned");
		return detailsReturn;
		
	}

	@Override
	public List<MnSubEventDetails> getSubEventDetails(List<Integer> eventIds)
	{
		if(logger.isDebugEnabled())
		logger.debug("getSubEventDetails method called");
		List<MnSubEventDetails> details = null;
		List<MnSubEventDetails> detailsReturn = new ArrayList<MnSubEventDetails>();
		MnEventDetails details1 = null;
		try{
			Query query=new Query(Criteria.where("cId").in(eventIds).and("status").is("A"));
			details=mongoOperations.find(query, MnSubEventDetails.class,JavaMessages.Mongo.MNESUBVENTS);
			
			if(details!=null)
			{
			for(MnSubEventDetails detail:details)
			{
				Query query1=new Query(Criteria.where("listId").is(detail.getListId()).and("eventId").in(detail.getEventId()).and("status").is("A"));
				details1=mongoOperations.findOne(query1, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS);
				detail.setEventEndDate(details1.getEventEndDate());
				
				detailsReturn.add(detail);
			}
			}
			
		}catch (Exception e) {
			logger.error("Exception in getSubEventDetails method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("getSubEventDetails method successfully returned");
		return detailsReturn;
		
	}
	
	@Override
	public MnSubEventDetails fetchSubEvents(Integer subEventId,Integer listId,Integer eventId)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchSubEvents method called eventId: "+eventId+" subEventId: "+subEventId);
		MnSubEventDetails details = null;
		try{
			Query query=new Query(Criteria.where("Id").is(subEventId).and("listId").is(listId).and("eventId").is(eventId).and("status").is("A"));
			details=mongoOperations.findOne(query, MnSubEventDetails.class,JavaMessages.Mongo.MNESUBVENTS);
		}catch (Exception e) {
			logger.error("Exception in fetchSubEvents method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchSubEvents method successfully returned");
		return details;
		
	}

	@Override
	public String updateMnSubEvent(String params) {
		if(logger.isDebugEnabled())
		logger.debug("updateMnSubEvent method called");
		String string=null;
		String listId=null;
		String eventId=null;
		String eventName=null;
		String description=null;
		String subEventId=null;
		String startDate=null;
		String endDate=null;
		String alldayevent=null;
		String location=null;
		String userId=null;
		boolean mailFlag=false;
		String sendEmailFlag=null;
		JSONObject jsonObject=null;
		try
		{
			jsonObject=new JSONObject(params);
			listId=(String)jsonObject.get("listId");
			eventId=(String)jsonObject.get("eventId");
			eventName=(String)jsonObject.get("eventName");
			description=(String)jsonObject.get("description");
			subEventId=(String)jsonObject.get("subEventId");
			startDate=(String)jsonObject.get("startDate");
			endDate=(String)jsonObject.get("endDate");
			alldayevent=(String)jsonObject.get("alldayevent");
			location=(String)jsonObject.get("location");
			userId=(String)jsonObject.get("userId");
			sendEmailFlag=(String) jsonObject.get("sendEmailFlag");
		
			if(sendEmailFlag.equalsIgnoreCase("True"))
				mailFlag=true;
			
		Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("eventId").is(Integer.parseInt(eventId)).and("Id").is(Integer.parseInt(subEventId)).and("userId").is(Integer.parseInt(userId)));
		Query query3 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("eventId").is(Integer.parseInt(eventId)).and("userId").is(Integer.parseInt(userId)));
		MnEventDetails mnEventDetails=mongoOperations.findOne(query3, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS);

		Update update=new Update();
		update.set("eventName", eventName);
		update.set("description", description);
		update.set("alldayevent", alldayevent);
		
		
		if (alldayevent.equalsIgnoreCase("True"))
		{
			SimpleDateFormat formatter = new SimpleDateFormat("MM/d/yyyy");
			Date startDate1 = (Date) formatter.parse(startDate);
			Date endDate1 = (Date) formatter.parse(endDate);
			
			update.set("startDate", formatter.format(startDate1));
			update.set("endDate", formatter.format(endDate1));
		}
		else
		{
			SimpleDateFormat sdf = new SimpleDateFormat("MM/d/yyyy HH:mm");
			Date startDate1 = (Date) sdf.parse(startDate);
			Date endDate1 = (Date) sdf.parse(endDate);
			
			update.set("startDate", sdf.format(startDate1));
			update.set("endDate", sdf.format(endDate1));
		}
		
		
		
		update.set("location", location);
		update.set("cId", mnEventDetails.getcId());
		update.set("repeatEvent", mnEventDetails.getRepeatEvent());
		update.set("mailFlag", mailFlag);
		
	    mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNESUBVENTS);
	    
		string="{\"status\":\"success\"}";
			}
			
		
		catch (Exception e) {
			logger.error("Exception in updateMnSubEvent method:"+e);
			string="{\"status\":\"error\"}";
		}
		if(logger.isDebugEnabled())
		logger.debug("updateMnSubEvent method successfully returned");
		return string;
	}

	@Override
	public String deleteMnSubEvent(String params) {
		if(logger.isDebugEnabled())
		logger.debug("deleteMnSubEvent method called");
		String string=null;
		String listId=null;
		String eventId=null;
		String subEventId=null;
		String userId=null;
		JSONObject jsonObject=null;
		try
		{
		jsonObject=new JSONObject(params);
		listId=(String)jsonObject.get("listId");
		eventId=(String)jsonObject.get("eventId");
		subEventId=(String)jsonObject.get("subEventId");
		userId=(String)jsonObject.get("userId");
		Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("eventId").is(Integer.parseInt(eventId)).and("Id").is(Integer.parseInt(subEventId)).and("userId").is(Integer.parseInt(userId)));
		mongoOperations.remove(query, JavaMessages.Mongo.MNESUBVENTS);
		string="{\"status\":\"success\"}";
		}
		catch (Exception e) {
			logger.error("Exception in deleteMnSubEvent method:"+e);
			string="{\"status\":\"error\"}";
		}
		if(logger.isDebugEnabled())
		logger.debug("deleteMnSubEvent method successfully returned");
		return string;
	}

	@Override
	public String updateAllMnSubEvent(String params) {
		
		if(logger.isDebugEnabled())
		logger.debug("updateAllMnSubEvent method called");
		String string=null;
		String listId=null;
		String eventId=null;
		String eventName=null;
		String description=null;
		String subEventId=null;
		String startDate=null;
		String endDate=null;
		String alldayevent=null;
		String location=null;
		String userId=null;
		String overlapping=null;
		JSONObject jsonObject=null;
		String startdates="";
		String enddates="";
		boolean mailFlag=false;
		String sendEmailFlag=null;
		String offSet=null;
			
			
			try
			{
				Update update=new Update();
				jsonObject=new JSONObject(params);
				listId=(String)jsonObject.get("listId");
				eventId=(String)jsonObject.get("eventId");
				eventName=(String)jsonObject.get("eventName");
				description=(String)jsonObject.get("description");
				subEventId=(String)jsonObject.get("subEventId");
				startDate=(String)jsonObject.get("startDate");
				endDate=(String)jsonObject.get("endDate");
				alldayevent=(String)jsonObject.get("alldayevent");
				location=(String)jsonObject.get("location");
				userId=(String)jsonObject.get("userId");
				overlapping=(String)jsonObject.get("overlappingEvent");
				
				sendEmailFlag=(String) jsonObject.get("sendEmailFlag");
				
				if(sendEmailFlag.equalsIgnoreCase("True"))
					mailFlag=true;
				
				Query query=new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("eventId").is(Integer.parseInt(eventId)));
				MnEventDetails mnEventDetail=mongoOperations.findOne(query, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS);
				
				Query query4 = new Query(Criteria.where("userId").is(mnEventDetail.getUserId()));
	   			MnUsers mnUser = mongoOperations.findOne(query4, MnUsers.class,JavaMessages.Mongo.MNUSERS);
	   			offSet=mnUser.getTimeZone();
				
				
				Query query3=new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("eventId").is(Integer.parseInt(eventId)).and("Id").is(Integer.parseInt(subEventId)).and("userId").is(Integer.parseInt(userId)));
				MnSubEventDetails mnSubEventDetails=mongoOperations.findOne(query3, MnSubEventDetails.class,JavaMessages.Mongo.MNESUBVENTS);

				
				if(alldayevent.equals("true"))
				{
				
				String startDate1[]=mnEventDetail.getStartDate().split(" ");
				String endDate1[]=mnEventDetail.getEndDate().split(" ");
				
				String startDate2[]=startDate.split(" ");
				String endDate2[]=endDate.split(" ");
				
				update.set("startDate", startDate2[0]);
				update.set("eventStartDate", endDate2[0]);
				update.set("endDate", endDate2[0]);
				string="{\"status\":\"success\",\"startDate\":\""+startDate1[0]+"\",\"endDate\":\""+endDate1[0]+"\",\"eventName\":\""+eventName+"\",\"location\":\""+location+"\",\"description\":\""+description+"\"}";
				
				}
				else
				{

					String startDate2[]=startDate.split(" ");
					String startDate1[]=mnEventDetail.getStartDate().split(" ");
					String endDate1[]=mnEventDetail.getEndDate().split(" ");
					String endDate2[]=endDate.split(" ");
					String startDates=null;
					  String endDates=null;
					if(startDate2[1]!=null)
					{
					String eventstartDates=startDate2[0]+" "+startDate2[1];
					startDates= startDate2[0]+" "+startDate2[1];
					update.set("startDate", startDates);
					update.set("eventStartDate", eventstartDates);
					}
					
					if(endDate2[1]!=null)
					{
				    endDates=endDate1[0]+" "+endDate1[1];
					update.set("endDate", endDates);
					}
					string="{\"status\":\"success\",\"startDate\":\""+startDates+"\",\"endDate\":\""+endDates+"\",\"eventName\":\""+eventName+"\",\"location\":\""+location+"\",\"description\":\""+description+"\"}";

				}
				
				update.set("eventName", eventName);
				update.set("description", description);
				update.set("alldayevent", alldayevent);
				update.set("location", location);
				
				mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNEVENTS);
				MnEventDetails mnEventDetails=mongoOperations.findOne(query, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS);
				
				
				// updates in subEvent table 
				
				Query query1 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("eventId").is(Integer.parseInt(eventId)).and("userId").is(Integer.parseInt(userId)).and("cId").is(mnEventDetails.getcId()));
				mongoOperations.remove(query1, JavaMessages.Mongo.MNESUBVENTS);
				
				Integer id;
				Query query2=new Query();
				 List<MnSubEventDetails> subEventList=mongoOperations.find(query2, MnSubEventDetails.class,JavaMessages.Mongo.MNESUBVENTS);
			        if(subEventList!=null && !subEventList.isEmpty())
					{
						MnSubEventDetails subEventdetail=subEventList.get(subEventList.size()-1);
						id=subEventdetail.getId()+0;
					}
					else
					{
						id=0;
					}
						
				List<Date> dates = new ArrayList<Date>();
				
				SimpleDateFormat formatter = new SimpleDateFormat("MM/d/yyyy");
				if (mnEventDetails.getRepeatEvent() != null && mnEventDetails.getRepeatEvent().equals("Daily"))
				{
					try
					{
						Date subEventStartDate = (Date) formatter.parse(mnEventDetails.getEventStartDate());
						Date subEndDate = (Date) formatter.parse(mnEventDetails.getEventEndDate());
						long interval = 24 * 1000 * 60 * 60; 
						long endTime = subEndDate.getTime(); 
						long curTime = subEventStartDate.getTime();
						while (curTime <= endTime)
						{
							dates.add(new Date(curTime));
							curTime += interval;
						}
						String startTime[] = mnEventDetails.getStartDate().split(" ");
						String endTimes[] = mnEventDetails.getEndDate().split(" ");
					
				       
						for (int i = 0; i < dates.size(); i++)
						{
							
							MnSubEventDetails details2 = new MnSubEventDetails();
							details2.setEventId(mnEventDetails.getEventId());
							details2.setAlldayevent(mnEventDetails.getAlldayevent());
							details2.setEventName(mnEventDetails.getEventName());
							
							details2.setDescription(mnEventDetails.getDescription());
							details2.setListId(mnEventDetails.getListId());
							details2.setLocation(mnEventDetails.getLocation());
							details2.setUserId(mnEventDetails.getUserId());
							details2.setC_id(mnEventDetails.getcId());
							details2.setRepeatEvent(mnEventDetails.getRepeatEvent());
							details2.setEmailFlag(mailFlag);
							details2.setTimeZone(offSet);
							Date lDate = (Date) dates.get(i);
							String ds = formatter.format(lDate);
							if (mnEventDetails.getAlldayevent())
							{
								details2.setStartDate(ds);
								details2.setEndDate(ds);
							}
							else
							{
								details2.setStartDate(ds + " " + startTime[1]);
								details2.setEndDate(ds + " " + endTimes[1]);
							}
							details2.setStatus("A");
							
							details2.setId(++id);
							mongoOperations.insert(details2, JavaMessages.Mongo.MNESUBVENTS);
						}
					}
					catch (Exception e)
					{
						logger.error("Exception in updateAllMnSubEvent method:"+e);
					}
				}
				else if (mnEventDetails.getRepeatEvent() != null && mnEventDetails.getRepeatEvent().equals("Weekly"))
				{
					try
					{
						Date subEventStartDate = (Date) formatter.parse(mnEventDetails.getEventStartDate());
						Date subEndDate = (Date) formatter.parse(mnEventDetails.getEventEndDate());
						long interval = 24 * 1000 * 60 * 60 * 7; 
						long endTime = subEndDate.getTime(); 
						long curTime = subEventStartDate.getTime();
						while (curTime <= endTime)
						{
							dates.add(new Date(curTime));
							curTime += interval;
						}
						String startTime[] = mnEventDetails.getStartDate().split(" ");
						String endTimes[] = mnEventDetails.getEndDate().split(" ");
						for (int i = 0; i < dates.size(); i++)
						{
							MnSubEventDetails details2 = new MnSubEventDetails();
							details2.setEventId(mnEventDetails.getEventId());
							details2.setAlldayevent(mnEventDetails.getAlldayevent());
							details2.setEventName(mnEventDetails.getEventName());
							details2.setDescription(mnEventDetails.getDescription());
							details2.setListId(mnEventDetails.getListId());
							details2.setLocation(mnEventDetails.getLocation());
							details2.setUserId(mnEventDetails.getUserId());
							details2.setC_id(mnEventDetails.getcId());
							details2.setRepeatEvent(mnEventDetails.getRepeatEvent());
							details2.setEmailFlag(mailFlag);
							details2.setTimeZone(offSet);
							Date lDate = (Date) dates.get(i);
							String ds = formatter.format(lDate);
							if (mnEventDetails.getAlldayevent())
							{
								details2.setStartDate(ds);
								details2.setEndDate(ds);
							}
							else
							{
								details2.setStartDate(ds + " " + startTime[1]);
								details2.setEndDate(ds + " " + endTimes[1]);
							}
							details2.setStatus("A");
							
							
							details2.setId(++id);
							mongoOperations.insert(details2, JavaMessages.Mongo.MNESUBVENTS);
						}
					}
					catch (Exception e)
					{
						logger.error("Exception in updateAllMnSubEvent method:"+e);
					}
				}
				else if (mnEventDetails.getRepeatEvent() != null && mnEventDetails.getRepeatEvent().equals("Monthly"))
				{
					try
					{
						Date subEventStartDate = (Date) formatter.parse(mnEventDetails.getEventStartDate());
						Date subEndDate = (Date) formatter.parse(mnEventDetails.getEventEndDate());
						Calendar cal2 = Calendar.getInstance();
						cal2.setTime(subEventStartDate);
						Calendar cal3 = Calendar.getInstance();
						cal3.setTime(subEndDate);
						int difInYear = cal3.get(Calendar.YEAR)- cal2.get(Calendar.YEAR);
						int difInMonths = cal3.get(Calendar.MONTH)-cal2.get(Calendar.MONTH);
						difInMonths+=difInYear*12;
						String startTime[] = mnEventDetails.getStartDate().split(" ");
						String endTimes[] = mnEventDetails.getEndDate().split(" ");
						for (int i = 0; i <= difInMonths; i++)
						{
							Calendar calStart = Calendar.getInstance();
							calStart.setTime(subEventStartDate);
							calStart.add(Calendar.MONTH, i);
							Calendar calEnd = Calendar.getInstance();
							calEnd.setTime((Date)formatter.parse(mnEventDetails.getEndDate()));
							calEnd.add(Calendar.MONTH, i);
							MnSubEventDetails details2 = new MnSubEventDetails();
							details2.setEventId(mnEventDetails.getEventId());
							details2.setAlldayevent(mnEventDetails.getAlldayevent());
							details2.setEventName(mnEventDetails.getEventName());
							details2.setDescription(mnEventDetails.getDescription());
							details2.setListId(mnEventDetails.getListId());
							details2.setLocation(mnEventDetails.getLocation());
							details2.setUserId(mnEventDetails.getUserId());
							details2.setC_id(mnEventDetails.getcId());
							details2.setRepeatEvent(mnEventDetails.getRepeatEvent());
							details2.setEmailFlag(mailFlag);
							details2.setTimeZone(offSet);
							Date sDate = (Date) calStart.getTime();
							Date eDate = (Date) calEnd.getTime();
							String ds = formatter.format(sDate);
							String es=formatter.format(eDate);
							if (mnEventDetails.getAlldayevent())
							{
								details2.setStartDate(ds);
								details2.setEndDate(es);
							}
							else
							{
								details2.setStartDate(ds + " " + startTime[1]);
								details2.setEndDate(es + " " + endTimes[1]);
							}
							details2.setStatus("A");
							
							
							details2.setId(++id);
							mongoOperations.insert(details2, JavaMessages.Mongo.MNESUBVENTS);
							
							
						}
					}
					catch (Exception e)
					{
						logger.error("Exception in updateAllMnSubEvent method:"+e);
					}
				}
				else
				{
					MnSubEventDetails details2 = new MnSubEventDetails();
					details2.setEventId(mnEventDetails.getEventId());
					details2.setAlldayevent(mnEventDetails.getAlldayevent());
					details2.setEventName(mnEventDetails.getEventName());
					details2.setDescription(mnEventDetails.getDescription());
					details2.setListId(mnEventDetails.getListId());
					details2.setLocation(mnEventDetails.getLocation());
					details2.setUserId(mnEventDetails.getUserId());
					details2.setC_id(mnEventDetails.getcId());
					details2.setRepeatEvent(mnEventDetails.getRepeatEvent());
					details2.setEmailFlag(mailFlag);
					details2.setTimeZone(offSet);
					
					String startD=mnEventDetails.getStartDate();
					String endD=mnEventDetails.getEndDate();
					
					if (mnEventDetails.getAlldayevent())
					{
						Date startDate1 = (Date) formatter.parse(startD);
						Date endDate1 = (Date) formatter.parse(endD);
						
						details2.setStartDate(formatter.format(startDate1));
						details2.setEndDate(formatter.format(endDate1));
					}
					else
					{
						SimpleDateFormat sdf = new SimpleDateFormat("MM/d/yyyy HH:mm");
						Date startDate1 = (Date) sdf.parse(startD);
						Date endDate1 = (Date) sdf.parse(endD);
						
						details2.setStartDate(sdf.format(startDate1));
						details2.setEndDate(sdf.format(endDate1));
					}
					details2.setStatus("A");
					
					
					details2.setId(++id);
					mongoOperations.insert(details2, JavaMessages.Mongo.MNESUBVENTS);
				}
				
				
			
			}catch (Exception e) {
				logger.error("Exception in updateAllMnSubEvent method:"+e);
			}
			if(logger.isDebugEnabled())
			logger.debug("updateAllMnSubEvent method successfully returned");
			return string;
			
			
	}
	public List<MnList> getListSeachByListId(String userId, String listId){
		if(logger.isDebugEnabled())
		logger.debug("getListSeachByListId method called "+userId);
		Query query = null;
		List<MnList> mnListList = null;
		List<MnGroupDetailsDomain> mnGroupDetails=null;
		List<MnNotesSharingDetails> details=null;
		List<Integer> sharingGroupIds=null;
		try{
			query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("listId").is(Integer.parseInt(listId)).and("status").is("A"));

			mnListList = mongoOperations.find(query, MnList.class,JavaMessages.Mongo.MNLIST);
			
				query = new Query(Criteria.where("sharingUserId").is(Integer.parseInt(userId)).and("listType").is("schedule").and("sharingGroupId").is(0).and("listId").is(Integer.parseInt(listId)).and("status").is("A"));
				query.sort().on("noteId", Order.ASCENDING);
				details = mongoOperations.find(query, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				if(details!= null && !details.isEmpty()){
					Set<String> orginalNotes = new TreeSet<String>();
					MnList mnList = getMnList(listId.toString());
					for(String str :mnList.getMnNotesDetails()){
						for(MnNotesSharingDetails details2 : details){
							if (str.contains("\"eventId\":\""+ details2.getNoteId().toString()+ "\"")&& str.contains("\"status\":\"A\"")){
								
									orginalNotes.add(str);
								
										
								break;
							}
						}	
					}
					if(!orginalNotes.isEmpty()){
						mnList.setMnNotesDetails(orginalNotes);
						mnListList.add(mnList);
					}
				}
			//group level sharing
				query = new Query(Criteria.where("userId").is(Integer.parseInt(userId)).and("status").is("A"));
				mnGroupDetails = mongoOperations.find(query, MnGroupDetailsDomain.class, JavaMessages.Mongo.MNGROUPDETAILS);
				if(mnGroupDetails!=null && !mnGroupDetails.isEmpty()){
					sharingGroupIds=new ArrayList<Integer>();
					for(MnGroupDetailsDomain domain: mnGroupDetails){
						sharingGroupIds.add(domain.getGroupId());
					}
				}
				if(sharingGroupIds!=null && !sharingGroupIds.isEmpty()){
					query = new Query(Criteria.where("sharingGroupId").ne(0).and("sharingUserId").is(Integer.parseInt(userId)).and("listType").is("schedule").and("listId").is(Integer.parseInt(listId)).and("status").is("A"));
					query.sort().on("listId", Order.ASCENDING);
					details = mongoOperations.find(query, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
				
					if(details!=null && !details.isEmpty()){

						Set<String> orginalNotes = new TreeSet<String>();
						MnList mnList = getMnList(listId.toString());
						for(String str :mnList.getMnNotesDetails()){
							for(MnNotesSharingDetails details2 : details){
								if (str.contains("\"eventId\":\""+ details2.getNoteId().toString()+ "\"")&& str.contains("\"status\":\"A\"")){
									orginalNotes.add(str);
									break;
								}
							}	
						}
						if(!orginalNotes.isEmpty()){
							mnList.setMnNotesDetails(orginalNotes);
							mnListList.add(mnList);
						}
					}
				}
		}catch (Exception e) {
			logger.error("Exception in getListSeachByListId method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("getListSeachByListId method successfully returned");
		return mnListList;
	}
	public List<MnList> byKewordConvertMnListTo(List<MnList> mnListList,String keyWord ){
		if(logger.isDebugEnabled())
		logger.debug("byKewordConvertMnListTo method called keyWord "+keyWord);
		List<MnList> newMnListList = new ArrayList<MnList>();
		String eventName=null;
		String description=null;
		JSONObject jsonObject;
		try{
			for(MnList mnList : mnListList){
				Set<String> orginalNotes = new TreeSet<String>();
				for(String str:mnList.getMnNotesDetails())
				{	
					if(str!=null && !str.isEmpty()){
						jsonObject = new JSONObject(str);
						
						eventName=(String) jsonObject.get("eventName");
						description=(String) jsonObject.get("description");
									
						if(keyWord!=null && (eventName.toLowerCase().contains(keyWord.toLowerCase()) || description.toLowerCase().contains(keyWord.toLowerCase())))
						{
							orginalNotes.add(str);
						}	
					}
				}
				if(!orginalNotes.isEmpty()){
					mnList.setMnNotesDetails(orginalNotes);
					newMnListList.add(mnList);
				}
			}
		}catch (Exception e) {
			logger.error("Exception in byKewordConvertMnListTo method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("byKewordConvertMnListTo method successfully returned");
		return newMnListList;	
	}

	@Override
	public MnSubEventDetails getSubEvents(String params) {
		if(logger.isDebugEnabled())
		logger.debug("getSubEvents method called");
		String success=null;
		MnSubEventDetails details=null;
		JSONObject jsonObject=null;
		try{
			jsonObject=new JSONObject(params);
			Integer subEventId=Integer.parseInt((String)jsonObject.get("subEventId"));
			Integer eventId=Integer.parseInt((String)jsonObject.get("eventId"));
			Integer listId=Integer.parseInt((String)jsonObject.get("listId"));
			String startDate=(String)jsonObject.get("startDate");
			String endDate=(String)jsonObject.get("endDate");
			Query query=new Query(Criteria.where("Id").is(subEventId).and("listId").is(listId).and("eventId").is(eventId).and("status").is("A"));
			details=mongoOperations.findOne(query, MnSubEventDetails.class,JavaMessages.Mongo.MNESUBVENTS);
			
		}catch (Exception e) {
			logger.error("Exception in getSubEvents method:"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("getSubEvents method successfully returned");
		return details;
		
	}

	@Override
	public String updateSubFollowingEvents(String params) {
		if(logger.isDebugEnabled())
		logger.debug("updateSubFollowingEvents method called");
		String string=null;
		String listId=null;
		String eventId=null;
		String eventName=null;
		String description=null;
		String subEventId=null;
		String startDate=null;
		String endDate=null;
		String alldayevent=null;
		String location=null;
		String userId=null;
		String overlapping=null;
		JSONObject jsonObject=null;
		String startdates="";
		String enddates="";
		String emailFlag=null;
		boolean mailFlag=false;
		String offSet=null;
			
			
			try
			{
				Update update=new Update();
				jsonObject=new JSONObject(params);
				listId=(String)jsonObject.get("listId");
				eventId=(String)jsonObject.get("eventId");
				eventName=(String)jsonObject.get("eventName");
				description=(String)jsonObject.get("description");
				subEventId=(String)jsonObject.get("subEventId");
				startDate=(String)jsonObject.get("startDate");
				endDate=(String)jsonObject.get("endDate");
				alldayevent=(String)jsonObject.get("alldayevent");
				location=(String)jsonObject.get("location");
				userId=(String)jsonObject.get("userId");
				overlapping=(String)jsonObject.get("overlappingEvent");
				
				emailFlag=(String) jsonObject.get("sendEmailFlag");
				
				if(emailFlag.equalsIgnoreCase("True"))
					mailFlag=true;
				
				Query query=new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("eventId").is(Integer.parseInt(eventId)));
				MnEventDetails mnEventDetail=mongoOperations.findOne(query, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS);
				
				Query query3=new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("eventId").is(Integer.parseInt(eventId)).and("Id").is(Integer.parseInt(subEventId)).and("userId").is(Integer.parseInt(userId)));
				MnSubEventDetails mnSubEventDetails=mongoOperations.findOne(query3, MnSubEventDetails.class,JavaMessages.Mongo.MNESUBVENTS);
				
				Query query4 = new Query(Criteria.where("userId").is(mnEventDetail.getUserId()));
	   			MnUsers mnUser = mongoOperations.findOne(query4, MnUsers.class,JavaMessages.Mongo.MNUSERS);
	   			offSet=mnUser.getTimeZone();
				
				if(alldayevent.equals("true"))
				{
				
			
				update.set("startDate", startDate);
				update.set("eventStartDate", startDate);
				update.set("endDate", endDate);
				
				}
				else
			     	{

					update.set("startDate", startDate);
					update.set("eventStartDate", startDate);
					update.set("endDate", endDate);
					}
					
				
			
			
			
				
				update.set("eventName", eventName);
				update.set("description", description);
				update.set("alldayevent", alldayevent);
				update.set("location", location);
				
				mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNEVENTS);
				MnEventDetails mnEventDetails=mongoOperations.findOne(query, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS);

				Query query1 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("eventId").is(Integer.parseInt(eventId)).and("userId").is(Integer.parseInt(userId)).and("cId").is(mnEventDetails.getcId()));
				mongoOperations.remove(query1, JavaMessages.Mongo.MNESUBVENTS);
				Integer id;
				Query query2=new Query();
				 List<MnSubEventDetails> subEventList=mongoOperations.find(query2, MnSubEventDetails.class,JavaMessages.Mongo.MNESUBVENTS);
			        if(subEventList!=null && !subEventList.isEmpty())
					{
						MnSubEventDetails subEventdetail=subEventList.get(subEventList.size()-1);
						id=subEventdetail.getId()+0;
					}
					else
					{
						id=0;
					}
						
				List<Date> dates = new ArrayList<Date>();
				
				SimpleDateFormat formatter = new SimpleDateFormat("MM/d/yyyy");
				if (mnEventDetails.getRepeatEvent() != null && mnEventDetails.getRepeatEvent().equals("Daily"))
				{
					try
					{
						Date subEventStartDate = (Date) formatter.parse(mnEventDetails.getEventStartDate());
						Date subEndDate = (Date) formatter.parse(mnEventDetails.getEventEndDate());
						long interval = 24 * 1000 * 60 * 60; 
						long endTime = subEndDate.getTime(); 
						long curTime = subEventStartDate.getTime();
						while (curTime <= endTime)
						{
							dates.add(new Date(curTime));
							curTime += interval;
						}
						String startTime[] = mnEventDetails.getStartDate().split(" ");
						String endTimes[] = mnEventDetails.getEndDate().split(" ");
					
				       
						for (int i = 0; i < dates.size(); i++)
						{
							
							MnSubEventDetails details2 = new MnSubEventDetails();
							details2.setEventId(mnEventDetails.getEventId());
							details2.setAlldayevent(mnEventDetails.getAlldayevent());
							details2.setEventName(mnEventDetails.getEventName());
							details2.setDescription(mnEventDetails.getDescription());
							details2.setListId(mnEventDetails.getListId());
							details2.setLocation(mnEventDetails.getLocation());
							details2.setUserId(mnEventDetails.getUserId());
							details2.setC_id(mnEventDetails.getcId());
							details2.setRepeatEvent(mnEventDetails.getRepeatEvent());
							details2.setEmailFlag(mailFlag);
							details2.setTimeZone(offSet);
							Date lDate = (Date) dates.get(i);
							String ds = formatter.format(lDate);
							if (mnEventDetails.getAlldayevent())
							{
								details2.setStartDate(ds);
								details2.setEndDate(ds);
							}
							else
							{
								details2.setStartDate(ds + " " + startTime[1]);
								details2.setEndDate(ds + " " + endTimes[1]);
							}
							details2.setStatus("A");
							
							
							details2.setId(++id);
							mongoOperations.insert(details2, JavaMessages.Mongo.MNESUBVENTS);
							
							
							
							
						}
					}
					catch (Exception e)
					{
						logger.error("Exception in updateSubFollowingEvents method:"+e);
					}
				}
				else if (mnEventDetails.getRepeatEvent() != null && mnEventDetails.getRepeatEvent().equals("Weekly"))
				{
					try
					{
						Date subEventStartDate = (Date) formatter.parse(mnEventDetails.getEventStartDate());
						Date subEndDate = (Date) formatter.parse(mnEventDetails.getEventEndDate());
						long interval = 24 * 1000 * 60 * 60 * 7; 
						long endTime = subEndDate.getTime(); 
						long curTime = subEventStartDate.getTime();
						while (curTime <= endTime)
						{
							dates.add(new Date(curTime));
							curTime += interval;
						}
						String startTime[] = mnEventDetails.getStartDate().split(" ");
						String endTimes[] = mnEventDetails.getEndDate().split(" ");
						for (int i = 0; i < dates.size(); i++)
						{
							MnSubEventDetails details2 = new MnSubEventDetails();
							details2.setEventId(mnEventDetails.getEventId());
							details2.setAlldayevent(mnEventDetails.getAlldayevent());
							details2.setEventName(mnEventDetails.getEventName());
							details2.setDescription(mnEventDetails.getDescription());
							details2.setListId(mnEventDetails.getListId());
							details2.setLocation(mnEventDetails.getLocation());
							details2.setUserId(mnEventDetails.getUserId());
							details2.setC_id(mnEventDetails.getcId());
							details2.setRepeatEvent(mnEventDetails.getRepeatEvent());
							details2.setEmailFlag(mailFlag);
							details2.setTimeZone(offSet);
							Date lDate = (Date) dates.get(i);
							String ds = formatter.format(lDate);
							if (mnEventDetails.getAlldayevent())
							{
								details2.setStartDate(ds);
								details2.setEndDate(ds);
							}
							else
							{
								details2.setStartDate(ds + " " + startTime[1]);
								details2.setEndDate(ds + " " + endTimes[1]);
							}
							details2.setStatus("A");
							
							
							details2.setId(++id);
							mongoOperations.insert(details2, JavaMessages.Mongo.MNESUBVENTS);
						}
					}
					catch (Exception e)
					{
						logger.error("Exception in updateSubFollowingEvents method:"+e);
					}
				}
				else if (mnEventDetails.getRepeatEvent() != null && mnEventDetails.getRepeatEvent().equals("Monthly"))
				{
					try
					{
						Date subEventStartDate = (Date) formatter.parse(mnEventDetails.getEventStartDate());
						Date subEndDate = (Date) formatter.parse(mnEventDetails.getEventEndDate());
						Calendar cal2 = Calendar.getInstance();
						cal2.setTime(subEventStartDate);
						Calendar cal3 = Calendar.getInstance();
						cal3.setTime(subEndDate);
						int difInYear = cal3.get(Calendar.YEAR)- cal2.get(Calendar.YEAR);
						int difInMonths = cal3.get(Calendar.MONTH)-cal2.get(Calendar.MONTH);
						difInMonths+=difInYear*12;
						String startTime[] = mnEventDetails.getStartDate().split(" ");
						String endTimes[] = mnEventDetails.getEndDate().split(" ");
						for (int i = 0; i <= difInMonths; i++)
						{
							Calendar calStart = Calendar.getInstance();
							calStart.setTime(subEventStartDate);
							calStart.add(Calendar.MONTH, i);
							Calendar calEnd = Calendar.getInstance();
							calEnd.setTime((Date)formatter.parse(mnEventDetails.getEndDate()));
							calEnd.add(Calendar.MONTH, i);
							MnSubEventDetails details2 = new MnSubEventDetails();
							details2.setEventId(mnEventDetails.getEventId());
							details2.setAlldayevent(mnEventDetails.getAlldayevent());
							details2.setEventName(mnEventDetails.getEventName());
							details2.setDescription(mnEventDetails.getDescription());
							details2.setListId(mnEventDetails.getListId());
							details2.setLocation(mnEventDetails.getLocation());
							details2.setUserId(mnEventDetails.getUserId());
							details2.setC_id(mnEventDetails.getcId());
							details2.setRepeatEvent(mnEventDetails.getRepeatEvent());
							details2.setEmailFlag(mailFlag);
							details2.setTimeZone(offSet);
							Date sDate = (Date) calStart.getTime();
							Date eDate = (Date) calEnd.getTime();
							String ds = formatter.format(sDate);
							String es=formatter.format(eDate);
							if (mnEventDetails.getAlldayevent())
							{
								details2.setStartDate(ds);
								details2.setEndDate(es);
							}
							else
							{
								details2.setStartDate(ds + " " + startTime[1]);
								details2.setEndDate(es + " " + endTimes[1]);
							}
							details2.setStatus("A");
							
							
							details2.setId(++id);
							mongoOperations.insert(details2, JavaMessages.Mongo.MNESUBVENTS);
							
							
						}
					}
					catch (Exception e)
					{
						logger.error("Exception in updateSubFollowingEvents method:"+e);
					}
				}
				else
				{
					MnSubEventDetails details2 = new MnSubEventDetails();
					details2.setEventId(mnEventDetails.getEventId());
					details2.setAlldayevent(mnEventDetails.getAlldayevent());
					details2.setEventName(mnEventDetails.getEventName());
					details2.setDescription(mnEventDetails.getDescription());
					details2.setListId(mnEventDetails.getListId());
					details2.setLocation(mnEventDetails.getLocation());
					details2.setUserId(mnEventDetails.getUserId());
					details2.setC_id(mnEventDetails.getcId());
					details2.setRepeatEvent(mnEventDetails.getRepeatEvent());
					details2.setEmailFlag(mailFlag);
					details2.setTimeZone(offSet);
					
					String startD=mnEventDetails.getStartDate();
					String endD=mnEventDetails.getEndDate();
					
					if (mnEventDetails.getAlldayevent())
					{
						Date startDate1 = (Date) formatter.parse(startD);
						Date endDate1 = (Date) formatter.parse(endD);
						
						details2.setStartDate(formatter.format(startDate1));
						details2.setEndDate(formatter.format(endDate1));
					}
					else
					{
						SimpleDateFormat sdf = new SimpleDateFormat("MM/d/yyyy HH:mm");
						Date startDate1 = (Date) sdf.parse(startD);
						Date endDate1 = (Date) sdf.parse(endD);
						
						details2.setStartDate(sdf.format(startDate1));
						details2.setEndDate(sdf.format(endDate1));
					}
					
					details2.setStatus("A");
					
					
					details2.setId(++id);
					mongoOperations.insert(details2, JavaMessages.Mongo.MNESUBVENTS);
				}
				
				string="{\"status\":\"success\"}";
				
			
			}catch (Exception e) {
				logger.error("Exception in updateSubFollowingEvents method:"+e);
			}
			if(logger.isDebugEnabled())
			logger.debug("updateSubFollowingEvents method successfully returned");
			return string;
			
			
	}

	@Override
	public String updateMnSingleEvent(String params) {
		if(logger.isDebugEnabled())
			logger.debug("updateMnSingleEvent method called");
		String string=null;
		String listId=null;
		String eventId=null;
		String eventName=null;
		String description=null;
		String subEventId=null;
		String startDate=null;
		String endDate=null;
		String alldayevent=null;
		String location=null;
		String userId=null;
		String emailFlag=null;
		boolean mailFlag=false;
		
		JSONObject jsonObject=null;
		try
		{
			
			jsonObject=new JSONObject(params);
			listId=(String)jsonObject.get("listId");
			eventId=(String)jsonObject.get("eventId");
			eventName=(String)jsonObject.get("eventName");
			description=(String)jsonObject.get("description");
			subEventId=(String)jsonObject.get("subEventId");
			startDate=(String)jsonObject.get("startDate");
			endDate=(String)jsonObject.get("endDate");
			alldayevent=(String)jsonObject.get("alldayevent");
			location=(String)jsonObject.get("location");
			userId=(String)jsonObject.get("userId");
			emailFlag=(String) jsonObject.get("sendEmailFlag");
		
			if(emailFlag.equalsIgnoreCase("True"))
				mailFlag=true;
			
			
		Query query = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("eventId").is(Integer.parseInt(eventId)).and("Id").is(Integer.parseInt(subEventId)).and("userId").is(Integer.parseInt(userId)));
		Query query3 = new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("eventId").is(Integer.parseInt(eventId)).and("userId").is(Integer.parseInt(userId)));
		MnEventDetails mnEventDetails=mongoOperations.findOne(query3, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS);
		Update updates=new Update();
		
		updates.set("eventName", eventName);
		updates.set("description", description);
		updates.set("alldayevent", alldayevent);
		updates.set("location", location);
		updates.set("startDate", startDate);
		updates.set("endDate", endDate);
		mongoOperations.updateFirst(query3, updates, JavaMessages.Mongo.MNEVENTS);
		
		
		Update update=new Update();
		update.set("eventName", eventName);
		update.set("description", description);
		update.set("alldayevent", alldayevent);
		
		if (alldayevent.equalsIgnoreCase("True"))
		{
			SimpleDateFormat formatter = new SimpleDateFormat("MM/d/yyyy");
			Date startDate1 = (Date) formatter.parse(startDate);
			Date endDate1 = (Date) formatter.parse(endDate);
			
			update.set("startDate", formatter.format(startDate1));
			update.set("endDate", formatter.format(endDate1));
		}
		else
		{
			SimpleDateFormat sdf = new SimpleDateFormat("MM/d/yyyy HH:mm");
			Date startDate1 = (Date) sdf.parse(startDate);
			Date endDate1 = (Date) sdf.parse(endDate);
			
			update.set("startDate", sdf.format(startDate1));
			update.set("endDate", sdf.format(endDate1));
		}
		
		update.set("location", location);
		update.set("cId", mnEventDetails.getcId());
		update.set("repeatEvent", mnEventDetails.getRepeatEvent());
		update.set("mailFlag",mailFlag);
	    mongoOperations.updateFirst(query, update, JavaMessages.Mongo.MNESUBVENTS);
	    
		string="{\"status\":\"success\"}";
			}
			
		
		catch (Exception e) {
			logger.error("Exception in updateMnSingleEvent method:"+e);
			string="{\"status\":\"error\"}";
		}
		if(logger.isDebugEnabled())
		logger.debug("updateMnSingleEvent method successfully returned");
		return string;
	}

	@Override
	public String fetchParticularEvent(String params) {
		if(logger.isDebugEnabled())
		logger.debug("fetchParticularEvent method called");
	String status="";
	String listId=null;
	String eventId=null;
    String userId=null;
	JSONObject jsonObject=null;
	
		
		
		try
		{
		
			jsonObject=new JSONObject(params);
			listId=(String)jsonObject.get("listId");
			eventId=(String)jsonObject.get("eventId");
			userId=(String)jsonObject.get("userId");
	
	    Query query=new Query(Criteria.where("listId").is(Integer.parseInt(listId)).and("eventId").is(Integer.parseInt(eventId)).and("userId").is(Integer.parseInt(userId)));
	   MnEventDetails mnEventDetail=mongoOperations.findOne(query, MnEventDetails.class,JavaMessages.Mongo.MNEVENTS);
	   if(mnEventDetail!=null)
	  {
      status="{\"status\":\"success\",\"startDate\":\""+mnEventDetail.getEventStartDate()+"\",\"endDate\":\""+mnEventDetail.getEventEndDate()+"\",\"eventName\":\""+mnEventDetail.getEventName()+"\",\"location\":\""+mnEventDetail.getLocation()+"\",\"description\":\""+mnEventDetail.getDescription()+"\"}";
	  }
		}
		catch (Exception e) {
			logger.error("Exception in fetchParticularEvent method:"+e);
			status="{\"status\":\"error\"}";
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchParticularEvent method successfully returned");
		return status;
	}
	
	
	@Override
	public String getEventMailValues() {
		if(logger.isDebugEnabled())
		logger.debug("getEventMailValues method called");
		String []name=TimeZone.getAvailableIDs();
		Set<String> timeZones=new HashSet<String>();
		
		for(String s:name){
			SimpleDateFormat format = new SimpleDateFormat("Z");
			Date datetime = new Date();
			format.setTimeZone(TimeZone.getTimeZone(s));
			timeZones.add(format.format(datetime));
			}
		
		//////////////
	    List<MnSubEventDetails> finalList=new ArrayList<MnSubEventDetails>();
		String Sdate=null;
		String Edate=null;
		
		// fetch data based on timeZone
		
		for(String s:timeZones)
		{
			
			try
			{
			Calendar currentdate = Calendar.getInstance();
			DateFormat formatter = new SimpleDateFormat("MM/d/yyyy HH:mm");
			DateFormat alldayFormatter = new SimpleDateFormat("MM/d/yyyy");
			Update update=new Update();
			
			List<MnSubEventDetails> timeBasedList=new ArrayList<MnSubEventDetails>();
			TimeZone obj = TimeZone.getTimeZone("GMT"+s);
			
			// fetch time based event 
			formatter.setTimeZone(obj);
			currentdate.add(Calendar.HOUR, 1);
			Sdate=formatter.format(currentdate.getTime());
			currentdate.add(Calendar.MINUTE,05);
			Edate=formatter.format(currentdate.getTime());
			Query query=new Query(Criteria.where("startDate").gte(Sdate).lte(Edate).and("mailFlag").is(true).and("alldayevent").is(false).and("timeZone").is(s));
			timeBasedList=mongoOperations.find(query,MnSubEventDetails.class,JavaMessages.Mongo.MNESUBVENTS);
	   		
	   		if(timeBasedList !=null && !timeBasedList.isEmpty())
	   		{
	   			finalList.addAll(timeBasedList);
	   			update.set("mailFlag", false);
	   			mongoOperations.updateMulti(query,update,JavaMessages.Mongo.MNESUBVENTS);
	   		}
	   		
	   		
	   		// fetch time based past events (for today only)
	   		Calendar calPast = Calendar.getInstance();
	   		calPast.set(Calendar.HOUR_OF_DAY,01);
	   		calPast.set(Calendar.MINUTE,0);
	   		DateFormat dF=new SimpleDateFormat("MM/d/yyyy HH:mm");
			Edate=dF.format(calPast.getTime());
			List<MnSubEventDetails> quickMailListTime=new ArrayList<MnSubEventDetails>();
			Query query1=new Query(Criteria.where("startDate").gte(Edate).lte(Sdate).and("mailFlag").is(true).and("alldayevent").is(false).and("timeZone").is(s));
			quickMailListTime=mongoOperations.find(query1,MnSubEventDetails.class,JavaMessages.Mongo.MNESUBVENTS);
			
	   		if(quickMailListTime !=null && !quickMailListTime.isEmpty())
	   		{
	   			finalList.addAll(quickMailListTime);
	   			update.set("mailFlag", false);
	   			mongoOperations.updateMulti(query1,update,JavaMessages.Mongo.MNESUBVENTS);
	   		}
	   		
	   		
	   		// fetch past all day event
	   		alldayFormatter.setTimeZone(obj);
			Sdate=alldayFormatter.format(currentdate.getTime());
	   		List<MnSubEventDetails> quickMailList=new ArrayList<MnSubEventDetails>();
	   		Query query2=new Query(Criteria.where("startDate").is(Sdate).and("mailFlag").is(true).and("alldayevent").is(true).and("timeZone").is(s));
	   		quickMailList=mongoOperations.find(query2,MnSubEventDetails.class,JavaMessages.Mongo.MNESUBVENTS);
	   		
	   		if(quickMailList !=null && !quickMailList.isEmpty())
	   		{
	   			finalList.addAll(quickMailList);
	   			update.set("mailFlag", false);
	   			mongoOperations.updateMulti(query2,update,JavaMessages.Mongo.MNESUBVENTS);
	   		}
	   		
	   		
	   		// fetch all day event
	   		Calendar curDate= Calendar.getInstance();
	   		boolean elvenClock=false;
			formatter.setTimeZone(obj);
			Sdate=formatter.format(curDate.getTime());
			Date date=new Date(Sdate);
			Calendar cal1 = Calendar.getInstance();
			cal1.set(Calendar.HOUR_OF_DAY,22);
			cal1.set(Calendar.MINUTE,55);
			cal1.set(Calendar.SECOND,0);
			cal1.set(Calendar.MILLISECOND,0);
			Date start = cal1.getTime();
			
			Calendar cal2 = Calendar.getInstance();
			cal2.set(Calendar.HOUR_OF_DAY,23);
			cal2.set(Calendar.MINUTE,05);
			cal2.set(Calendar.SECOND,0);
			cal2.set(Calendar.MILLISECOND,0);
			Date end = cal2.getTime();
			
		    if (start.after( date ) && end.before(date)) {
		    	elvenClock=true;
		    }
	   		
		    
	   		if(elvenClock)
	   		{
	   			List<MnSubEventDetails> allEventList=new ArrayList<MnSubEventDetails>();
	   			
	   			Calendar cal = Calendar.getInstance();  
	   			cal.setTime(date);  
	   			cal.add(Calendar.DATE, 1); // add 1 days  
	   			date = cal.getTime();  
	   			Sdate=formatter.format(date);
	   			Query query3=new Query(Criteria.where("startDate").is(Sdate).and("alldayevent").is(true).and("mailFlag").is(true).and("timeZone").is(s));
	   			allEventList=mongoOperations.find(query3,MnSubEventDetails.class,JavaMessages.Mongo.MNESUBVENTS);
	   	   		
	   	   		if(allEventList !=null && !allEventList.isEmpty())
	   	   		{
	   	   			finalList.addAll(allEventList);
	   	   			update.set("mailFlag", false);
	   	   			mongoOperations.updateMulti(query3,update,JavaMessages.Mongo.MNESUBVENTS);
	   	   		}
	   	   		
	   		}
			}
			catch (Exception e) {
				logger.error("Exception in getEventMailValues method:"+e);
			}
		}
   		
   		MnUsers mnUser=null;
   		String allday="No" ,location="-";
   		for(MnSubEventDetails mnSubEventDetails:finalList)
   		{
   			Query query4 = new Query(Criteria.where("userId").is(mnSubEventDetails.getUserId()).and("notificationFlag").is("yes"));
   			mnUser = mongoOperations.findOne(query4, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			
   			SendMail sendMail=new SendMail(); 	
			String recipients[]={mnUser.getEmailId()};
			try{
				if(mnSubEventDetails.getAlldayevent())
				{
					allday="Yes";
				}
				if((mnSubEventDetails.getLocation()!=null && !mnSubEventDetails.getLocation().isEmpty()))
				{
					location=mnSubEventDetails.getLocation();
				}
				
				String message="<div style=\"margin:0;padding:0\" dir=\"ltr\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:98%\" border=\"0\"><tbody><tr><td style=\"font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;font-size:12px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"font-size:16px;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;background:#3b5998;color:#ffffff;font-weight:bold;vertical-align:baseline;letter-spacing:-0.03em;text-align:left;padding:10px 38px 4px\"><img src=https://www.musicnoteapp.com/musicnote/pics/logo.png>&nbsp;&nbsp;<a style=\"text-decoration:none\" href=\"https://www.musicnoteapp.com/musicnote/index.html\" target=\"_blank\"><span style=\"background:#3b5998;color:#ffffff;font-weight:bold;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;vertical-align:middle;font-size:16px;letter-spacing:-0.03em;text-align:left;vertical-align:baseline\">Musicnote</span></a></td></tr></tbody></table>"+"<table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse: collapse; width: 620px\"><tbody><tr><td style=\"border-right: 1px solid #ccc; line-height: 16px; font-size: 11px; border-bottom: 1px solid #ccc; font-family: LucidaGrande, tahoma, verdana, arial, sans-serif; border-top: 1px solid #ccc; padding: 10px 20px; border-left: 1px solid #ccc\">"+"<table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse: collapse\"><tbody><tr><td style=\"font-size: 11px; font-family: LucidaGrande, tahoma, verdana, arial, sans-serif; padding: 10px 20px; line-height: 16px; width: 620px\"><table cellspacing=\"7\" cellpadding=\"7\" style=\"border-collapse: collapse\"><tbody><tr><td>Event Name</td><td> : </td><td>"+mnSubEventDetails.getEventName()+"</td></tr><tr><td>Location</td><td> : </td><td>"+location+"</td></tr><tr><td>All Day</td><td> : </td><td>"+allday+"</td></tr><tr><td>Start Date</td><td> : </td><td>"+mnSubEventDetails.getStartDate()+"</td></tr><tr><td>End Date</td><td> : </td><td>"+mnSubEventDetails.getEndDate()+"</td></tr><tr><td>Description</td><td> : </td><td>"+mnSubEventDetails.getDescription()+"</td></tr></tbody></table>"+"<br /><table cellspacing=0 cellpadding=0 style=\"border-collapse: \"collapse><tbody><tr><td></td></tr></tbody></table><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse: collapse\"><tbody><tr><td style=\"font-size: 11px; background: #3b5998; font-family: LucidaGrande, tahoma, verdana, arial, sans-serif; padding: 2px 6px 4px; border-top: 1px solid #8a9cc2\"><a href=\"https://www.musicnoteapp.com/musicnote/index.html\" style=\"color: #c0c0c0; text-decoration: none\" target=\"_blank\"><span style=\"font-weight: bold; white-space: nowrap; color: #fff; font-size: 13px\">Go To Musicnote</span></a></td> </tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></div>";
				
				sendMail.postEmail(recipients, "Event Reminder "+mnSubEventDetails.getEventName(),message);
			}catch (Exception e) {
				logger.error("Exception in getEventMailValues method:"+e);
			}
   		}
   		if(logger.isDebugEnabled())
   		logger.debug("getEventMailValues method successfully returned");
		return "mail has been sent to selected ids";
	}

	@Override
	public String convertedZoneEvents(Integer userId, Integer ownerId,String date1) {
		if(logger.isDebugEnabled())
		logger.debug("convertedZoneEvents method called "+userId);
		String convertedDate=null;
		SimpleDateFormat formatter = new SimpleDateFormat("MM/d/yyyy HH:mm");
		try
		{
			Query query2 = new Query(Criteria.where("userId").is(userId));
   			MnUsers mnUser = mongoOperations.findOne(query2, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			String toZone=mnUser.getTimeZone();
			
			Query query3 = new Query(Criteria.where("userId").is(ownerId));
   			MnUsers mnUser1 = mongoOperations.findOne(query3, MnUsers.class,JavaMessages.Mongo.MNUSERS);
   			String fromZone=mnUser1.getTimeZone();
   			
			Date date = formatter.parse(date1);
		
		String fromTimeZone="GMT"+fromZone;
        String toTimeZone="GMT"+toZone;
        
        TimeZone fromTZ = TimeZone.getTimeZone(fromTimeZone);
	    TimeZone toTZ = TimeZone.getTimeZone(toTimeZone);
	    
	    long fromTZDst = 0;  
        if(fromTZ.inDaylightTime(date))  
        {  
            fromTZDst = fromTZ.getDSTSavings();  
        }  
  
        long fromTZOffset = fromTZ.getRawOffset() + fromTZDst;  
  
        long toTZDst = 0;  
        if(toTZ.inDaylightTime(date))  
        {  
            toTZDst = toTZ.getDSTSavings();  
        }  
        long toTZOffset = toTZ.getRawOffset() + toTZDst;
        
        convertedDate=formatter.format(date.getTime() + (toTZOffset - fromTZOffset));
		}catch (Exception e) {
			logger.error("Exception in convertedZoneEvents method:"+e);
		}
		if(logger.isDebugEnabled())
	   	logger.debug("convertedZoneEvents method successfully returned");
		return convertedDate;
	}

}
