package com.musicnotes.apis.dao.impl;

import java.text.DateFormat;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.musicnotes.apis.dao.interfaces.ILoginDao;
import com.musicnotes.apis.domain.MnEventsSharingDetails;
import com.musicnotes.apis.domain.MnLoginData;
import com.musicnotes.apis.domain.MnNonUserSharingDetails;
import com.musicnotes.apis.domain.MnNotesSharingDetails;
import com.musicnotes.apis.domain.MnUserLogs;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.domain.MnUsersToken;
import com.musicnotes.apis.domain.MnBlockedUsers;
import com.musicnotes.apis.tokens.TokenGenerator;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.MailContent;
import com.musicnotes.apis.util.PasswordGenerator;
import com.musicnotes.apis.util.SendMail;


public class LoginDaoImpl extends BaseDaoImpl implements ILoginDao
{
	Logger logger = Logger.getLogger(LoginDaoImpl.class);

	@Override
	public String CheckLogin(String userName)
	{
		if(logger.isDebugEnabled())logger.debug("Checklogin called"+userName);

		MnUsers user = null;
		String userCheckId = "";
		try
		{
			Query query = new Query(Criteria.where("userName").is(userName));
			user = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			if (user != null)
			{
				userCheckId=""+user.getUserId();
			}
			else
			{
				userCheckId="0";

			}
		}
		catch (Exception e)
		{
			logger.error("Login checking  : " + e);
		}
		if(logger.isDebugEnabled())logger.debug("checkLogin terminated and returned userId");

		return userCheckId;
	}

	@Override
	public String getLogin(String userName, String password,HttpServletRequest request)
	{
		if(logger.isDebugEnabled())logger.debug("getLogin entered with userName : "+userName);

	String checkUser="";
	Set<String> userIdSet = new HashSet<String>();
	Set<String> sharedIdSet = new HashSet<String>();
	MnUsers user;
	Query query,usernameQuery=null;
	boolean studentCheck=false; 
	boolean adminVideoFlag=false;
	try{
		
		// check if user enter username or email
		usernameQuery=new Query(Criteria.where("status").is("A").and("emailId").is(userName));
		user = mongoOperations.findOne(usernameQuery, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		if(user==null)
		{
			usernameQuery=new Query(Criteria.where("status").is("A").and("userName").is(userName));
			user = mongoOperations.findOne(usernameQuery, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		}
		
		// check if the user is available in db insert intime in db
		if (user != null)
		{
			byte[] encryptedPassword = PasswordGenerator.encoder(user.getUserId().toString(), password);
			if (Arrays.equals(encryptedPassword, user.getPassword()))
			{
				//Check if user is Blocked or not
				
				String status=validateBlockedList(user.getUserId());
				if(status.equals("unBlocked"))
				{
					JSONObject arbitraryPayload = new JSONObject();
					try {
					    arbitraryPayload.put("some", "arbitrary");
					    arbitraryPayload.put("data", "here");
					} catch (JSONException e) {
					    e.printStackTrace();
					}   
					DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
					Date today = Calendar.getInstance().getTime();        
					String date = df.format(today);
					TokenGenerator tokenGenerator=new  TokenGenerator(userName+""+password+""+date);
					String token = tokenGenerator.createToken(arbitraryPayload);
					
					MnUsersToken usersToken=new MnUsersToken();
					usersToken.setUserId(user.getUserId());
					usersToken.setLoginTime(date);
					usersToken.setTokens(token);
					usersToken.setCreatedDateTime(date);
					usersToken.setIpaddress(request.getRemoteHost()+request.getRemoteAddr());
					mongoOperations.insert(usersToken, JavaMessages.Mongo.MNUsersToken);
					
					// update user login status if added students only
					if(user.getUserRole().equalsIgnoreCase("Music Student") && user.getAddedUserStatus()!=null && user.getAddedUserStatus().equalsIgnoreCase("Inactive"))
					{
						Update update=new Update();
						update.set("addedUserStatus","Active");
						mongoOperations.updateFirst(usernameQuery, update, JavaMessages.Mongo.MNUSERS);
						studentCheck=true; // this flag used to check student via add usser to navigate profile page
					}
					
					// used to update adminNotification flag
					if(user.isAdminNotificationFlag())
					{
						Update update=new Update();
						update.set("adminNotificationFlag",false);
						mongoOperations.updateFirst(usernameQuery, update, JavaMessages.Mongo.MNUSERS);
						adminVideoFlag=true; // this flag used to check student via add usser to navigate profile page
					}
					
					//if(mailflag) 
					//{
					
					// check if the mail flag is true or not
					if(user.isMailCheckFlag())
							{
							if(user.isRequestPending()){ // check if any one shared or copied via email
								
								// sharing book level note level
								Query query2 = new Query(Criteria.where("sharingUserId").is(user.getUserId()).and("status").is("E"));
								List<MnNotesSharingDetails> mnNotesSharingDetailsList = mongoOperations.find(query2, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
								if(mnNotesSharingDetailsList!= null && !mnNotesSharingDetailsList.isEmpty()){
									for(MnNotesSharingDetails details : mnNotesSharingDetailsList){
										if(!userIdSet.contains(details.getUserId())){
											userIdSet.add(details.getUserId()+"~"+details.getSharingLevel());
										}
									}
								}
								
								// copy note level
								query = new Query(Criteria.where("emailId").in(user.getEmailId()).and("sharingLevel").is("copy").and("status").is("P"));
								List<MnNonUserSharingDetails> mnNonUserSharingDetailsList = mongoOperations.find(query, MnNonUserSharingDetails.class, JavaMessages.Mongo.MNNONUSERSHARINGDETAILS);
								
								for(MnNonUserSharingDetails details :mnNonUserSharingDetailsList){
									if(!userIdSet.contains(details.getUserId())){
										userIdSet.add(details.getUserId()+"~"+details.getSharingLevel());
									}
								}
								
							}
							
							//get normal sharing event to show at the time of login
							Query shareId = new Query(Criteria.where("sharingUserId").is(user.getUserId()).and("sharingStatus").is("P").and("status").is("E"));
							List<MnEventsSharingDetails> eventList = mongoOperations.find(shareId, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
							
							if(eventList!=null && !eventList.isEmpty())
							{
								for(MnEventsSharingDetails details:eventList)
								{
									sharedIdSet.add(details.getUserId()+"~"+details.getListId()+"~"+details.getNoteId());
								}
							}
							
							if(userIdSet.isEmpty()){
								checkUser="{\"adminVideoFlag\":\""+adminVideoFlag+"\",\"studentCheck\":\""+studentCheck+"\",\"userId\":\""+user.getUserId()+"\",\"requestPending\":"+user.isRequestPending()+",\"userIdSet\":\"\",\"sharedIdSet\":\""+sharedIdSet.toString()+"\",\"adminFlag\":\""+user.isAdminFlag()+"\",\"token\":\""+token+"\",\"datess\":\""+date+"\"}";
							}else{
								checkUser="{\"adminVideoFlag\":\""+adminVideoFlag+"\",\"studentCheck\":\""+studentCheck+"\",\"userId\":\""+user.getUserId()+"\",\"requestPending\":"+user.isRequestPending()+",\"userIdSet\":\""+userIdSet.toString()+"\",\"adminFlag\":\""+user.isAdminFlag()+"\",\"sharedIdSet\":\""+sharedIdSet.toString()+"\",\"token\":\""+token+"\",\"datess\":\""+date+"\"}";
							}
							
							/* commented by ramaraj this is executed only for student via adding in contact page
							if(userIdSet.isEmpty() && user != null && user.getUserFirstName()==null &&user.getUserLastName()==null){
								studentCheck=true;
								checkUser="{\"studentCheck\":\""+studentCheck+"\",\"userId\":\""+user.getUserId()+"\",\"requestPending\":"+user.isRequestPending()+",\"userIdSet\":\"\",\"sharedIdSet\":\""+sharedIdSet.toString()+"\",\"adminFlag\":\""+user.isAdminFlag()+"\",\"token\":\""+token+"\",\"datess\":\""+date+"\"}";	
							}*/
							
						}else
							{
								checkUser="{\"adminVideoFlag\":\""+adminVideoFlag+"\",\"studentCheck\":\""+studentCheck+"\",\"userId\":\"mailcheck\",\"token\":\""+token+"\",\"datess\":\""+date+"\"}";
							}
					// commented by ramaraj for removing code was handled data issue
					/*}
					else
					{
						if(user.isRequestPending())
						{
							// sharing book level note level
							Query query2 = new Query(Criteria.where("sharingUserId").is(user.getUserId()).and("status").is("E"));
							List<MnNotesSharingDetails> mnNotesSharingDetailsList = mongoOperations.find(query2, MnNotesSharingDetails.class, JavaMessages.Mongo.MNNOTESSHARINGDETAILS);
							if(mnNotesSharingDetailsList!= null && !mnNotesSharingDetailsList.isEmpty()){
								for(MnNotesSharingDetails details : mnNotesSharingDetailsList){
									if(!userIdSet.contains(details.getUserId())){
										userIdSet.add(details.getUserId()+"~"+details.getSharingLevel());
									}
								}
							}
							
							// copy note level
							query = new Query(Criteria.where("emailId").in(user.getEmailId()).and("sharingLevel").is("copy").and("status").is("P"));
							List<MnNonUserSharingDetails> mnNonUserSharingDetailsList = mongoOperations.find(query, MnNonUserSharingDetails.class, JavaMessages.Mongo.MNNONUSERSHARINGDETAILS);
							
							for(MnNonUserSharingDetails details :mnNonUserSharingDetailsList){
								if(!userIdSet.contains(details.getUserId())){
									userIdSet.add(details.getUserId()+"~"+details.getSharingLevel());
								}
							}
						}
						
						//get normal sharing event to show at the time of login
						
						Query shareId = new Query(Criteria.where("sharingUserId").is(user.getUserId()).and("sharingStatus").ne("A"));
						List<MnEventsSharingDetails> eventList = mongoOperations.find(shareId, MnEventsSharingDetails.class,JavaMessages.Mongo.MNEVENTSSHARINGDETAILS);
						
						if(eventList!=null && !eventList.isEmpty())
						{
							for(MnEventsSharingDetails details:eventList)
							{
								sharedIdSet.add(details.getUserId()+"~"+details.getListId()+"~"+details.getNoteId());
							}
						}
						
						
						if(userIdSet.isEmpty()){
							checkUser="{\"studentCheck\":\""+studentCheck+"\",\"userId\":\""+user.getUserId()+"\",\"requestPending\":"+user.isRequestPending()+",\"userIdSet\":\"\",\"sharedIdSet\":\""+sharedIdSet.toString()+"\",\"adminFlag\":\""+user.isAdminFlag()+"\",\"token\":\""+token+"\",\"datess\":\""+date+"\"}";
						}else{
							checkUser="{\"studentCheck\":\""+studentCheck+"\",\"userId\":\""+user.getUserId()+"\",\"requestPending\":"+user.isRequestPending()+",\"userIdSet\":\""+userIdSet.toString()+"\",\"adminFlag\":\""+user.isAdminFlag()+"\",\"sharedIdSet\":\""+sharedIdSet.toString()+"\",\"token\":\""+token+"\",\"datess\":\""+date+"\"}";
						}
						if(userIdSet.isEmpty() && user != null && user.getUserFirstName()==null &&user.getUserLastName()==null){	
							studentCheck=true;
							checkUser="{\"studentCheck\":\""+studentCheck+"\",\"userId\":\""+user.getUserId()+"\",\"userId\":\""+user.getUserId()+"\",\"requestPending\":"+user.isRequestPending()+",\"userIdSet\":\"\",\"sharedIdSet\":\""+sharedIdSet.toString()+"\",\"adminFlag\":\""+user.isAdminFlag()+"\",\"token\":\""+token+"\",\"datess\":\""+date+"\"}";
						}
					}*/
				}
				else if(status.equals("blocked"))
				{
					checkUser="{\"studentCheck\":\""+studentCheck+"\",\"userId\":\"blocked\"}";
				}
			}
			else
			{
				checkUser="{\"studentCheck\":\""+studentCheck+"\",\"userId\":\"0\"}";
			}
			
		
		}else{
			checkUser="{\"studentCheck\":\""+studentCheck+"\",\"userId\":\"0\"}";
		}
		
	}catch(Exception e){
		logger.error(e.getMessage());
	}
	if(logger.isDebugEnabled())logger.debug("getLogin has terminated and returned with user details And note/event sharing details");

	return checkUser;
}

	private String validateBlockedList(Integer userId)
	{
		Query query=null;
		String status="";
		MnBlockedUsers mnBlockedUsers=null;
		query=new Query(Criteria.where("userId").is(userId).and("status").is("A"));
		mnBlockedUsers = mongoOperations.findOne(query, MnBlockedUsers.class,JavaMessages.Mongo.MNBLOCKEDUSERS);
		if(mnBlockedUsers!=null && !mnBlockedUsers.equals("") && mnBlockedUsers.getStatus().equals("A"))
		{
			if( mnBlockedUsers.getNumberOfDays().equals("10"))
			{
				try
				{
					Date date=new Date();
					Date currentDate=new Date();
					String pattern = "dd MMM yyyy HH:mm:ss a";
					SimpleDateFormat dateFormat=new SimpleDateFormat(pattern);
					date=dateFormat.parse(mnBlockedUsers.getBlockedDate());
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(date); 
					calendar.add(Calendar.DATE, 10); 
					if(currentDate.after(calendar.getTime()))
					{
						mnBlockedUsers.setBlockedLevel("-");
						query=new Query(Criteria.where("userId").is(mnBlockedUsers.getUserId()));
						Update update=new Update();
						update.set("status","I");
						mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNBLOCKEDUSERS);
						status="unBlocked";
					}
					else
					{
						status="blocked";
					}
				}
				catch (Exception e) 
				{
					logger.error("Login checking  : " + e);
				}
				
			}
			else if(mnBlockedUsers.getNumberOfDays().equals("20"))
			{
				try
				{
					Date date=new Date();
					Date currentDate=new Date();
					String pattern = "dd MMM yyyy HH:mm:ss a";
					SimpleDateFormat dateFormat=new SimpleDateFormat(pattern);
					date=dateFormat.parse(mnBlockedUsers.getBlockedDate());
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(date); 
					calendar.add(Calendar.DATE, 20); 
					if(currentDate.after(calendar.getTime()))
					{
						mnBlockedUsers.setBlockedLevel("-");
						query=new Query(Criteria.where("userId").is(mnBlockedUsers.getUserId()));
						Update update=new Update();
						update.set("status","I");
						mongoOperations.updateFirst(query, update,JavaMessages.Mongo.MNBLOCKEDUSERS);
						status="unBlocked";
					}
					else
					{
						status="blocked";
					}
				}
				
				catch (Exception e) 
				{
					logger.error("Login checking  : " + e);
				}
			}
			else if(mnBlockedUsers.getNumberOfDays().equals("fullyBlocked"))
			{
				status="blocked";
			}
			else
			{
				status="unBlocked";
			}
		}
		else 
		{
			status="unBlocked";
		}
		return status;
	}

	@SuppressWarnings("null")
	@Override
	public String unsubcribeNotification(MnUsers mnUsers)
	{
		if(logger.isDebugEnabled())logger.debug("unsubcribeNotification has entered");
		String userName=mnUsers.getEmailId().toLowerCase();
		String password=mnUsers.getPasswordText();
		MnUsers user = null;
		String userCheckId = "";
		try
		{
			Query query = new Query(Criteria.where("emailId").is( mnUsers.getEmailId().toLowerCase()));
			user = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
			
			byte[] encryptedPassword = PasswordGenerator.encoder(user.getUserId().toString(), password);
			if ((user != null)&&(Arrays.equals(encryptedPassword, user.getPassword())))
			{
				Query query1=new Query(Criteria.where("userId").is((user.getUserId())));
			
				Update update=new Update();
				update.set("notificationFlag","no");
				
				mongoOperations.updateFirst(query1, update, JavaMessages.Mongo.MNUSERS);
				
				userCheckId="success";
				
			}
			else
			{
				userCheckId="nonuser";

			}
			writeLog(user.getUserId(), "A", "Unsubcribed Notification From All the activities", "settingss", null, null, null, null);
		}
		catch (Exception e)
		{
			logger.error("Login checking  : " + e);
		}
		if(logger.isDebugEnabled())logger.debug("unsubcribeNotification has terminated with success/fail message"+userCheckId);

		return userCheckId;
	}

	@Override
	public String updateLoginCheck(MnUsers mnUsers)
	{
		if(logger.isDebugEnabled())logger.debug("updateLoginCheck has entered for update mail Flag");

		MnUsers user = new MnUsers();
	String userCheckId = "";
	try
	{
		Query query = new Query(Criteria.where("emailId").is( mnUsers.getEmailId()).and("mailCheckId").is(mnUsers.getMailCheckId()));
		user = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		if (user != null)
		{
			Query query1=new Query(Criteria.where("userId").is((user.getUserId())));
		
			Update update=new Update();
			update.set("mailCheckFlag",true);
			
			mongoOperations.updateFirst(query1, update, JavaMessages.Mongo.MNUSERS);
			
			userCheckId="success";
		}
		else
		{
			userCheckId="nonuser";

		}
	}
	catch (Exception e)
	{
		logger.error("Login checking  : " + e);
	}
	if(logger.isDebugEnabled())logger.debug("updateLoginCheck has terminated with updated user details");

	return userCheckId;
	}
	
	
	@Override
	public String resetVerificationCode(MnUsers user)
	{
		if(logger.isDebugEnabled())logger.debug("resetVerificationCode method called");

		//MnUsers user = new MnUsers();
	String userCheckId = "";
	try
	{
		Query query = new Query(Criteria.where("emailId").is(user.getEmailId().toLowerCase()));
		MnUsers users = mongoOperations.findOne(query, MnUsers.class,JavaMessages.Mongo.MNUSERS);
		if (users != null)
		{
			Query query1=new Query(Criteria.where("emailId").is((users.getEmailId().toLowerCase())).and("status").is("A"));
		
			Update update=new Update();
			update.set("mailCheckId",user.getMailCheckId());
			
			mongoOperations.updateFirst(query1, update, JavaMessages.Mongo.MNUSERS);
			
				SendMail sendMail=new SendMail(); 	
				SendMail sendMail1=new SendMail();
				String recipients[]={users.getEmailId().toLowerCase()};
				String userLevel=users.getUserLevel();
				try{
				String message=MailContent.sharingNotification+users.getUserFirstName()+" "+users.getUserLastName()+MailContent.sharingNotification1+"\nWelcome to Musicnote! Whether you are a music student, teacher or musician our goal is to make your musical journey a better one."+ "<br><br>\nYour login details for Musicnote," +
					"<br> User Name : " + users.getEmailId().toLowerCase() +
					"<br> Verification Code : " + user.getMailCheckId() +
					"<br><br> Click here to <a style=\"text-decoration:none\" href=\"https://www.musicnoteapp.com/musicnote/index.html\" target=\"_blank\"> Log in </a>to Musicnote now and start enjoying the benefits of digitally saving, sharing your music resources."+
					"<br><br> Your Musicnote Trial account is completely free as we beta test. Please feel free to contact us at any time with any questions, concerns, issues or feedback."+
					MailContent.signature;
		
				String messageForTrailUser=MailContent.sharingNotification+users.getUserFirstName()+" "+users.getUserLastName()+MailContent.sharingNotification1+"\nWelcome to Musicnote! Whether you are a music student, teacher or musician our goal is to make your musical journey a better one."+ "<br><br>\nYour login details for Musicnote," +
					"<br> User Name : " + users.getEmailId().toLowerCase() +
					"<br> Verification Code : " + user.getMailCheckId() +
					"<br><br> Click here to <a style=\"text-decoration:none\" href=\"https://www.musicnoteapp.com/musicnote/index.html\" target=\"_blank\"> Log in </a>to Musicnote now and start enjoying the benefits of digitally saving, sharing your music resources."+
					"<br><br> Your Musicnote Trial account is completely free as we beta test. Please feel free to contact us at any time with any questions, concerns, issues or feedback."+
					MailContent.signature;
				//Sending mail to inform users that they are on trail period
				if(userLevel.equals("Trial-Start")){
					sendMail1.postEmail(recipients, users.getUserFirstName()+" welcome to Musicnote! ",messageForTrailUser);	
				}else{
					sendMail.postEmail(recipients, users.getUserFirstName()+" welcome to Musicnote! ",message);
				}
				if(logger.isDebugEnabled())
					logger.debug("Return response for resetVerificationCode method : ");
				
				
				userCheckId="success";
				}catch(Exception e){
					logger.error("Exception occured in resetVerificationCode method"+e);
				}
			
			
		}
		else
		{
			userCheckId="nonuser";

		}
	}
	catch (Exception e)
	{
		logger.error("Login checking  : " + e);
	}
	if(logger.isDebugEnabled())logger.debug("resetVerificationCode has terminated with updated user details");

	return userCheckId;
	}
	
	
	@Override
	public String doUserLoginDetails(MnUserLogs mnUserLogs)
	{
		if(logger.isDebugEnabled())logger.debug("Users Login details has entered");

		try
		{
			mongoOperations.insert(mnUserLogs, JavaMessages.Mongo.MNUSERLOGDETAIL);
		}
		catch (Exception e)
		{
			logger.error(e);
		}
		if(logger.isDebugEnabled())logger.debug("User login details has inserted and returned success");

		return "success";
	}

	@Override
	public String loginAuthentication(String userId, String inTime, String role) {
		if(logger.isDebugEnabled())logger.debug("loginAuthendication has entered "+userId+" inTime: "+inTime);

		MnLoginData loginDetails = null;
		String flag=""; 

		try
		{
			Query query = new Query(Criteria.where("inTime").is(inTime));
			loginDetails = mongoOperations.findOne(query, MnLoginData.class, JavaMessages.Mongo.MNLoginData);
		}
		catch (Exception e)
		{
			logger.error("loginAuthentication Info   :" + e);
		}
		if (loginDetails != null && loginDetails.getOutTime().equals("0"))
		{
			if(role.equals("Administrator")){
				flag="true";
			}else{
				flag="false";
			}
			if(logger.isDebugEnabled())logger.debug("loginAuthendication has terminated with returned of userId/Admin-yes");

			return loginDetails.getUserId().toString()+"&"+flag;
		}
		else
		{
			if(logger.isDebugEnabled())logger.debug("loginAuthendication has terminated with returned zero");

			return "0";
		}
	}

	@Override
	public String toGenerateTokens(MnUsersToken mnUsersToken) {
		if(logger.isDebugEnabled())logger.debug("Users token generations has entered");
		String status="success";
		try
		{
			mongoOperations.insert(mnUsersToken, JavaMessages.Mongo.MNUsersToken);
		}
		catch (Exception e)
		{
			status="fail";
			logger.error(e);
		}
		if(logger.isDebugEnabled())logger.debug("User Token details has been changed and returned success");

		return status;
	}

}
