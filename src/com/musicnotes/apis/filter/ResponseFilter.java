package com.musicnotes.apis.filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.musicnotes.apis.controllers.FilterController;
import com.musicnotes.apis.util.JavaMessages;

public class ResponseFilter extends FilterController implements Filter {
	Logger logger = Logger.getLogger(ResponseFilter.class);

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws ServletException, java.io.IOException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;

		// This should be added in response to both the preflight and the actual
		// request
		logger.info("API request Call : " + request.getPathInfo());
		logger.info("ContextPath : " + request.getContextPath());
		logger.info("Server IP  : " + request.getLocalAddr() + ":Server NAme :"
				+ request.getLocalName());
		logger.info("Client Address : " + request.getRemoteAddr());
		logger.info("Client Host : " + request.getRemoteHost());
		logger.info("RequestURI  : " + request.getRequestURI());

		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Headers",
						"Origin,Content-type,Accept,Connection,Content-length,Mn-Callers,Mn-time,Mn-Callers1");

		if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
			response.addHeader("Access-Control-Allow-Credentials", "true");
		}
		if (request.getHeader("Mn-Callers") != null) {
			String requestHeader = request.getHeader("Mn-Callers");
			if(request.getRequestURI().trim().contains(JavaMessages.requestLoginURI)){
				byte[] byteArray = Base64.decodeBase64(requestHeader.getBytes());
				String decodedString = new String(byteArray);
				String value = decodedString.substring(decodedString
						.indexOf(JavaMessages.requestURI) - 1, decodedString.length());
				if (value.trim().equalsIgnoreCase(request.getRequestURI().trim()))
					chain.doFilter(req, resp);	
			}
			// to check the token only exist for create new token
			else if(request.getRequestURI().trim().contains(JavaMessages.requestTokenURI))
			{
				String token = request.getHeader("Mn-Callers");
				String time = request.getHeader("Mn-time");
				boolean valid=isValidTokenForNewGenerate(token,time,request);
				logger.info("valid  "+valid);
				if(valid){
					chain.doFilter(req, resp);
				}
				else
					logger.info("valid   "+valid);
			}
			
			else{
				String token = request.getHeader("Mn-Callers");
				String time = request.getHeader("Mn-time");
				boolean valid=isValidToken(token,time,request);
				logger.info("valid   "+valid);
				if(valid){
					chain.doFilter(req, resp);
				}
				else
					logger.info("valid   "+valid);
			}
			
		}
		else if(request.getHeader("Mn-Callers1") != null) {
			String requestHeader = request.getHeader("Mn-Callers1");
			if(request.getRequestURI().trim().contains(JavaMessages.requestUserURI)){
				byte[] byteArray = Base64.decodeBase64(requestHeader.getBytes());
				String decodedString = new String(byteArray);
				String value = decodedString.substring(decodedString
						.indexOf(JavaMessages.requestURI) - 1, decodedString.length());
				if (value.trim().equalsIgnoreCase(request.getRequestURI().trim()))
					chain.doFilter(req, resp);	
			
		}
		else{
				String token = request.getHeader("Mn-Callers1");
				String time = request.getHeader("Mn-time");
				boolean valid=isValidTokenForUser(token,time,request);
				logger.info("valid   "+valid);
				if(valid){
					chain.doFilter(req, resp);
				}
				else
					logger.info("Invalid "+valid);
			}
		}
		else{
			if(isValidTokenForUpload(request))
				chain.doFilter(req, resp);
		}
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

	@Override
	public void destroy() {

	}

}
