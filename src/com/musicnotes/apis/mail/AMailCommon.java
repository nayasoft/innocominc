/**
 * 
 */
package com.musicnotes.apis.mail;

import java.util.Properties;

import com.musicnotes.apis.util.JavaMessages;

/**
 * @author sbashid
 * 
 */
public abstract class AMailCommon {

	/**
	 * 
	 */
	public AMailCommon() {
		// TODO Auto-generated constructor stub
	}

	public Properties loadMailProperties(String portNo, String host, String mode) {
		Properties props = new Properties();

		if (mode.equalsIgnoreCase("incoming")) {

			 props.setProperty("mail.store.protocol", "imaps");
			 props.setProperty("http.proxyHost", "proxyhost");
			 props.setProperty("http.proxyPort", "proxynumber");
			 props.setProperty("mail.imap.socketFactory.fallback", "false");
			 props.setProperty("mail.imap.port", portNo);
			 props.setProperty("mail.imap.socketFactory.port", portNo);
			 props.put("mail.imap.host", host);

		} else {

			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", portNo);
			props.put("mail.transport.protocol", "smtp");
			
			    props.put("mail.transport.protocol", "smtps");
			    props.put("mail.smtps.host", "smtpout.secureserver.net");
			    props.put("mail.smtps.auth", "true");
			    props.put("mail.smtps.port",JavaMessages.portNo);
			    props.put("mail.smtp.starttls.enable", "true");
			    
		}

		return props;
	}

}
