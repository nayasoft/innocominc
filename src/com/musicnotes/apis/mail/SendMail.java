package com.musicnotes.apis.mail;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;


import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.MongoOperations;
import com.musicnotes.apis.domain.MnEventDetails;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.MailContent;

import javax.mail.internet.MimeBodyPart;

public class SendMail extends AMailCommon{

	private Properties props ;
	private String user;
	private String pass;
	Logger logger;
	String checkURL;

	HttpSession session;
	private String portNo;
	private String hostName;
	
	String folderType;
	String host;
	String userName;
	String password;
	String jsonString;
	String mode;
	
	MongoOperations mongoOperations;

	public MongoOperations getMongoOperations()
	{
		return mongoOperations;
	}

	public void setMongoOperations(MongoOperations mongoOperations)
	{
		this.mongoOperations = mongoOperations;
	}

	public SendMail() {
		
		logger = Logger.getLogger(SendMail.class);
//		
//		user = userId;
//		pass = userPwd;
//		portNo = port;
//		hostName = host;
//		
//
//		props.put("mail.smtp.starttls.enable", "true");
//		if (checkURL != ""
//				&& (checkURL.contains("localhost") || checkURL
//						.equalsIgnoreCase("192.168.1.65:8080"))) {
//			props.put("mail.smtp.port", this.portNo);
//			props.put("mail.smtp.host", this.hostName);
//			props.put("mail.smtp.auth", "true");
//		} else {
//			props.put("mail.smtp.port", this.portNo);
//			props.put("mail.smtp.host", this.hostName);
//			props.put("mail.smtp.auth", "true");
//		}
//		props.put("mail.transport.protocol", "smtp");
//		props.put("mail.smtp.sendpartial", "true");
	}

	public void initialize(String folderType, String portNo, String host,String userName, String password,String mode)
	{
		
		
		this.folderType = folderType;
		this.portNo = portNo;
		this.host = host;
		this.userName = userName;
		this.password = password;
		this.mode=mode;
		
		logger.info("send mail : folderType : " + folderType + "portNo :" + portNo + " host : " + host +" mode : " + mode +" userName :" +userName +" password :  " +password);
	}
	
	
	
	private static class SMTPAuthenticator extends javax.mail.Authenticator
	{

		private String username;

		private String password;

		public SMTPAuthenticator(String username, String password)
		{
			this.username = username;
			this.password = password;
		}

		public PasswordAuthentication getPasswordAuthentication()
		{
			return new PasswordAuthentication(username, password);
		}
	}
	
	public void sendEmail(String recipients, String subject, String message,String mode)
			throws Exception {
		
		String splittedRecipients[]=recipients.split(",");
		logger.info("Before Authendicate "  );
		boolean debug = false;
		Authenticator auth = new SMTPAuthenticator(userName, password);
		 props=super.loadMailProperties(portNo, host,mode);
		try {
			//Authenticator auth = new SMTPAuthenticator(userName, password);
			Session session = null;
			
				session = Session.getDefaultInstance(props, auth);
			session.setDebug(debug);
			// create a message
			Message msg = new MimeMessage(session);
			// set the from and to address
			InternetAddress addressFrom = new InternetAddress(userName);
			msg.setFrom(addressFrom);

			List<InternetAddress> add = new ArrayList<InternetAddress>(0);
			List<InternetAddress> add1 = new ArrayList<InternetAddress>(0);
			add1.add(new InternetAddress(userName));
			int k = 0;
			for (int i = 0; i < splittedRecipients.length; i++) {
				// check if k reached 230
				if (k == JavaMessages.emailCount) {
					// msg.setRecipients(Message.RecipientType.TO,
					// add.toArray(new InternetAddress[0]));
					// Above setRecipients comment out for Hide To address of
					// Recipients for Bulk mails.
					// msg.setRecipient(Message.RecipientType.TO, new
					// InternetAddress(JavaMessages.noreplyemail));
					msg.setRecipients(Message.RecipientType.BCC, add
							.toArray(new InternetAddress[0]));
					// Setting the Subject and Content Type
					msg.setSubject(subject);
					msg.setContent(message, "text/html");
					Transport.send(msg);
					// reseting the k
					k = 0;

					add = new ArrayList<InternetAddress>(0);
				}
				add.add(new InternetAddress(StringUtils.deleteWhitespace(
						splittedRecipients[i]).toLowerCase()));
				k++;

			}
			if ((k != 0) && ((add != null) && (!add.isEmpty()))) {
				msg.setRecipients(Message.RecipientType.TO, add
						.toArray(new InternetAddress[0]));
				
				msg.setSubject(subject);
				msg.setContent(message, "text/html");
				Transport.send(msg);

			}
		} catch (Exception e) {
			throw e;
		}

	}

	// group
	public void sendEmail(String[] recipients, String subject,
			List<String> message) throws Exception {
		try {
			Authenticator auth = new SMTPAuthenticator(user, pass);
			Session session;
			if (checkURL != ""
					&& (checkURL.contains("localhost") || checkURL
							.equalsIgnoreCase("192.168.1.65:8080"))) {
				session = Session.getInstance(props, auth);
			} else {
				session = Session.getInstance(props);
			}
			session.setDebug(false);
			// create a message
			Message msg = new MimeMessage(session);

			// set the from and to address
			InternetAddress addressFrom = new InternetAddress(user);
			msg.setFrom(addressFrom);

			List<InternetAddress> add = new ArrayList<InternetAddress>(0);
			List<InternetAddress> add1 = new ArrayList<InternetAddress>(0);
			add1.add(new InternetAddress(user)); // This is for one copy should
			// go for From address..
			int k = 0;

			for (int i = 0; i < recipients.length; i++) {
				msg = new MimeMessage(session);
				InternetAddress addressFroms = new InternetAddress(user);
				msg.setFrom(addressFroms);
				// check if k reached 230
				if (k == JavaMessages.emailCount) {
					msg.setRecipients(Message.RecipientType.TO, add
							.toArray(new InternetAddress[0]));
					// Setting the Subject and Content Type
					msg.setSubject(subject);
					msg.setContent(message.get(i).toString(), "text/html");
					Transport.send(msg);
					// reseting the k
					k = 0;

					add = new ArrayList<InternetAddress>(0);
				}
				add.add(new InternetAddress(recipients[i]));
				k++;

				if ((k != 0) && ((add != null) && (!add.isEmpty()))) {

					msg.setRecipients(Message.RecipientType.TO, add
							.toArray(new InternetAddress[0]));
					msg.setRecipients(Message.RecipientType.BCC, add1
							.toArray(new InternetAddress[0]));
					// Setting the Subject and Content Type
					msg.setSubject(subject);
					@SuppressWarnings("unused")
					String sample = message.get(i);
					msg.setContent(message.get(i).toString(), "text/html");
					Transport.send(msg);

					add.clear();
					msg.setContent("", "");
					sample = "";

				}

			}
		} catch (Exception e) {
			throw e;
		}

	}

	/***
	 * <P>
	 * This method is useful for sending bulk email with same message and
	 * subject to all recipients.
	 * <p>
	 * Example: To send email to 3 users with following content:
	 * <p>
	 * Email ID Email Message content Subject
	 * <Li>1. aaa@email.com your fees is due on 1/2/2010 Mail from school 1
	 * <Li>2. bbb@email.com your fees is due on 1/2/2010 Mail from school 1
	 * <Li>3. ccc@email.com your fees is due on 1/2/2010 Mail from school 1
	 * <p>
	 * 
	 * You should call this method with three hashmaps and common subject as
	 * follows.
	 * <p>
	 * <Li>HashMap<String, String> emailMap = new HashMap(1, "aaa@email.com");
	 * <Li>emailMap.set(2, "bbb@email.com")
	 * <Li>emailMap.set(3, "ccc@email.com")
	 * <p>
	 * 
	 * message = "your fees is due on 1/2/2010"
	 * <p>
	 * 
	 * subject = "Mail from "
	 * <p>
	 * 
	 * @param emailMap
	 *            <Integer, String> ID Email ID
	 * @param messageMap
	 *            <String, String> Email ID Message
	 * @param schoolNameMap
	 *            <String, String> Email ID School Name
	 * @param subject
	 *            common email subject
	 * @throws MessagingException
	 */
	public void sendBulkEmail(HashMap<Integer, String> emailMap,
			String message, String subject) throws MessagingException {

		Authenticator auth = new SMTPAuthenticator(user, pass);
		Session session = Session.getInstance(props, auth);

		// create a message
		Message msg = new MimeMessage(session);

		// set the from and to address
		InternetAddress addressFrom = new InternetAddress(user);
		msg.setFrom(addressFrom);

		List<InternetAddress> add = new ArrayList<InternetAddress>(0);
		int k = 0;
		for (int i = 0; i < emailMap.size(); i++) {
			// check if k reached 230
			if (k == JavaMessages.emailCount) {
				msg.setRecipients(Message.RecipientType.TO, add
						.toArray(new InternetAddress[0]));
				// Setting the Subject and Content Type
				msg.setSubject(subject);
				msg.setContent(message, "text/html");
				Transport.send(msg);
				// reseting the k
				k = 0;

				add = new ArrayList<InternetAddress>(0);
			}
			add.add(new InternetAddress(emailMap.get(i)));
			k++;
		}
		if ((k != 0) && ((add != null) && (!add.isEmpty()))) {
			msg.setRecipients(Message.RecipientType.TO, add
					.toArray(new InternetAddress[0]));
			// Setting the Subject and Content Type
			msg.setSubject(subject);
			msg.setContent(message, "text/html");
			Transport.send(msg);

		}

		InternetAddress[] addressTo = new InternetAddress[emailMap.size()];
		for (int id : emailMap.keySet()) {
			addressTo[id] = new InternetAddress(emailMap.get(id));
		}
		msg.setRecipients(Message.RecipientType.TO, addressTo);
		// Setting the Subject and Content Type
		msg.setSubject(subject);
		msg.setContent(message, "text/html");
		Transport.send(msg);
	}

	/**
	 * This method is useful for sending bulk email with dynamic message and
	 * subject to each recipients.
	 * <p>
	 * Example:
	 * <p>
	 * <Li>To send email to 3 users with following content:
	 * <Li>Email ID Email Message content Subject
	 * <Li>1. aaa@email.com your fees is due on 1/2/2010 Mail from school 1
	 * <Li>2. bbb@email.com your fees is due on 4/4/2010 Mail from school 2
	 * <Li>3. ccc@email.com your fees is due on 5/5/2010 Mail from school 3
	 * <p>
	 * 
	 * You should call this method with three hashmaps and common subject as
	 * follows.
	 * <p>
	 * HashMap<String, String> emailMap = new HashMap(1, "aaa@email.com");
	 * emailMap.set(2, "bbb@email.com") emailMap.set(3, "ccc@email.com")
	 * <p>
	 * 
	 * HashMap<String, String> messageMap = new HashMap("aaa@email.com",
	 * "your fees is due on 1/2/2010"); emailMap.set("aaa@email.com",
	 * "your fees is due on 4/4/2010") emailMap.set("aaa@email.com",
	 * "your fees is due on 5/5/2010")
	 * <p>
	 * 
	 * HashMap<String, String> emailMap = new HashMap("aaa@email.com",
	 * "school 1"); emailMap.set("aaa@email.com", "school 2")
	 * emailMap.set("aaa@email.com", "school 3")
	 * <p>
	 * 
	 * subject = "Mail from "
	 * <p>
	 * 
	 * @param emailMap
	 *            <Integer, String> ID Email ID
	 * @param messageMap
	 *            <String, String> Email ID Message
	 * @param schoolNameMap
	 *            <String, String> Email ID School Name
	 * @param subject
	 *            common email subject
	 * @throws MessagingException
	 */
	public void sendBulkEmail(HashMap<Integer, String> emailMap,
			HashMap<String, String> messageMap,
			HashMap<String, String> schoolNameMap, String subject)
			throws MessagingException {

		Authenticator auth = new SMTPAuthenticator(user, pass);
		Session session = Session.getInstance(props, auth);

		// create a message
		Message msg = new MimeMessage(session);

		// set the from and to address
		InternetAddress addressFrom = new InternetAddress(user);
		msg.setFrom(addressFrom);

		InternetAddress addressTo = new InternetAddress();
		StringBuffer emailID = new StringBuffer();
		StringBuffer message = new StringBuffer();
		StringBuffer emailSubject = new StringBuffer();
		for (int id : emailMap.keySet()) {
			emailSubject.append(subject);

			emailID.append(emailMap.get(id));
			message.append(messageMap.get(emailID));
			emailSubject.append(schoolNameMap.get(emailID));

			addressTo.setAddress(emailID.toString());
			msg.setRecipient(Message.RecipientType.TO, addressTo);
			// Setting the Subject and Content Type
			msg.setSubject(emailSubject.toString());
			msg.setContent(message, "text/html");
			Transport.send(msg);

			// To clear the content from string buffer variables
			emailID.delete(0, emailID.length());
			message.delete(0, message.length());
			emailSubject.delete(0, emailSubject.length());
		}
	}

	public void setPortNo(String portNo) {
		this.portNo = portNo;
	}

	public String getPortNo() {
		return portNo;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getHostName() {
		return hostName;
	}

	/**
	 * SimpleAuthenticator is used to do simple authentication when the SMTP
	 * server requires it.
	 */
//	private static class SMTPAuthenticator extends javax.mail.Authenticator {
//
//		private String username;
//
//		private String password;
//
//		public SMTPAuthenticator(String username, String password) {
//			this.username = username;
//			this.password = password;
//		}
//
//		public PasswordAuthentication getPasswordAuthentication() {
//			return new PasswordAuthentication(username, password);
//		}
//	}

	// add by lakshmi for send mail in support@nayasoft
	public void sendSupportEmail(String[] recipients, String subject,
			String message, String Name, String emailId) throws Exception {

		// user="support@nayasoft.in";
		// pass="Password@123";
		// recipients[0]="support@nayasoft.in";
		user = "testing@ncums.com";
		pass = "Password@123";
		recipients[0] = "testing@ncums.com";
		try {
			Authenticator auth = new SMTPAuthenticator(user, pass);
			Session session;
			if (checkURL != ""
					&& (checkURL.contains("localhost") || checkURL
							.equalsIgnoreCase("192.168.1.65:8080"))) {
				session = Session.getInstance(props, auth);
			} else {
				session = Session.getInstance(props);
			}
			session.setDebug(false);
			// create a message
			Message msg = new MimeMessage(session);

			// set the from and to address
			InternetAddress addressFrom = new InternetAddress(emailId);
			msg.setFrom(addressFrom);

			List<InternetAddress> add = new ArrayList<InternetAddress>(0);
			int k = 0;
			for (int i = 0; i < recipients.length; i++) {
				// check if k reached 230
				if (k == JavaMessages.emailCount) {
					// msg.setRecipients(Message.RecipientType.TO,
					// add.toArray(new InternetAddress[0]));
					// Above setRecipients comment out for Hide To address of
					// Recipients for Bulk mails.
					// msg.setRecipient(Message.RecipientType.TO, new
					// InternetAddress(JavaMessages.noreplyemail));
					msg.setRecipients(Message.RecipientType.TO, add
							.toArray(new InternetAddress[0]));
					// Setting the Subject and Content Type
					msg.setSubject(subject);
					msg.setContent(message, "text/html");
					Transport.send(msg);
					// reseting the k
					k = 0;

					add = new ArrayList<InternetAddress>(0);
				}
				add.add(new InternetAddress(StringUtils.deleteWhitespace(
						recipients[i]).toLowerCase()));
				k++;

			}
			if ((k != 0) && ((add != null) && (!add.isEmpty()))) {
				// msg.setRecipients(Message.RecipientType.TO, add.toArray(new
				// InternetAddress[0]));
				// Above setRecipients comment out for Hide To address of
				// Recipients for Bulk mails.
				// msg.setRecipient(Message.RecipientType.TO, new
				// InternetAddress(JavaMessages.noreplyemail));
				msg.setRecipients(Message.RecipientType.TO, add
						.toArray(new InternetAddress[0]));
				// Setting the Subject and Content Type
				msg.setSubject(subject);
				msg.setContent(message, "text/html");
				Transport.send(msg);

			}
		} catch (Exception e) {
			throw e;
		}

	}

	public void sendCirEmail(String[] recipients, String subject,
			List<String> message) throws Exception {

		try {
			Authenticator auth = new SMTPAuthenticator(user, pass);
			Session session;
			if (checkURL != ""
					&& (checkURL.contains("localhost") || checkURL
							.equalsIgnoreCase("192.168.1.65:8080"))) {
				session = Session.getInstance(props, auth);
			} else {
				session = Session.getInstance(props, auth);
			}
			session.setDebug(false);
			// create a message
			Message msg = new MimeMessage(session);

			// set the from and to address
			InternetAddress addressFrom = new InternetAddress(user);
			msg.setFrom(addressFrom);

			List<InternetAddress> add = new ArrayList<InternetAddress>(0);
			// List<InternetAddress> add1 = new ArrayList<InternetAddress>(0);
			// add1.add(new InternetAddress(user)); //This is for one copy
			// should go for From address..
			int k = 0;
			for (int i = 0; i < recipients.length; i++) {
				logger.info("email recepient :::::----> " + i + "." + " "
						+ recipients[i]);
				// check if k reached 230
				if (k == JavaMessages.emailCount) {
					// msg.setRecipients(Message.RecipientType.TO,
					// add.toArray(new InternetAddress[0]));
					// Above setRecipients comment out for Hide To address of
					// Recipients for Bulk mails.
					// msg.setRecipient(Message.RecipientType.TO, new
					// InternetAddress(JavaMessages.noreplyemail));
					msg.setRecipients(Message.RecipientType.TO, add
							.toArray(new InternetAddress[0]));
					// Setting the Subject and Content Type
					msg.setSubject(subject);
					msg.setContent(message.get(i).toString(), "text/html");
					Transport.send(msg);

					// reseting the k
					k = 0;

					add = new ArrayList<InternetAddress>(0);
				}
				add.add(new InternetAddress(StringUtils.deleteWhitespace(
						recipients[i]).toLowerCase()));
				k++;

				// add.add(new
				// InternetAddress(StringUtils.deleteWhitespace(user)));
				// k++;

				if ((k != 0) && ((add != null) && (!add.isEmpty()))) {
					// logger.info("Mail recepient :::::----> "+ i +"." +" " +
					// recipients[i]);

					// msg.setRecipients(Message.RecipientType.TO,
					// add.toArray(new InternetAddress[0]));
					// Above setRecipients comment out for Hide To address of
					// Recipients for Bulk mails.
					msg.setRecipients(Message.RecipientType.TO, add
							.toArray(new InternetAddress[0]));
					msg.setRecipients(Message.RecipientType.BCC, add
							.toArray(new InternetAddress[0]));
					// msg.setRecipients(Message.RecipientType.BCC,
					// add1.toArray(new InternetAddress[0]));
					// Setting the Subject and Content Type
					msg.setSubject(subject);

					msg.setContent(message.get(i).toString(), "text/html");
					Transport.send(msg);
					add.clear();
					msg.setContent("", "");

				}

			}
		} catch (Exception e) {
			throw e;
		}

	}

	public void sendEmails(String[] recipients, String subject, String message)
			throws Exception {
		try {

			Authenticator auth = new SMTPAuthenticator(user, pass);
			Session session;
			if (checkURL != ""
					&& (checkURL.contains("localhost") || checkURL
							.equalsIgnoreCase("192.168.1.65:8080"))) {
				session = Session.getInstance(props, auth);
			} else {
				session = Session.getInstance(props);
			}
			session.setDebug(false);
			// create a message
			Message msg = new MimeMessage(session);

			// set the from and to address
			InternetAddress addressFrom = new InternetAddress(user);
			msg.setFrom(addressFrom);

			List<InternetAddress> add = new ArrayList<InternetAddress>(0);
			int k = 0;
			for (int i = 0; i < recipients.length; i++) {
				// check if k reached 230
				if (k == JavaMessages.emailCount) {
					// msg.setRecipients(Message.RecipientType.TO,
					// add.toArray(new InternetAddress[0]));
					// Above setRecipients comment out for Hide To address of
					// Recipients for Bulk mails.
					// msg.setRecipient(Message.RecipientType.TO, new
					// InternetAddress(JavaMessages.noreplyemail));
					msg.setRecipients(Message.RecipientType.BCC, add
							.toArray(new InternetAddress[0]));
					// Setting the Subject and Content Type
					msg.setSubject(subject);
					msg.setContent(message, "text/html");
					Transport.send(msg);
					// reseting the k
					k = 0;

					add = new ArrayList<InternetAddress>(0);
				}
				add.add(new InternetAddress(StringUtils.deleteWhitespace(
						recipients[i]).toLowerCase()));
				k++;

			}
			// add.add(new InternetAddress(StringUtils.deleteWhitespace(user)));
			// k++;

			if ((k != 0) && ((add != null) && (!add.isEmpty()))) {
				msg.setRecipients(Message.RecipientType.TO, add
						.toArray(new InternetAddress[0]));

				// Above setRecipients comment out for Hide To address of
				// Recipients for Bulk mails.
				// msg.setRecipient(Message.RecipientType.TO, new
				// InternetAddress(JavaMessages.noreplyemail));
				// msg.setRecipients(Message.RecipientType.TO, add
				// .toArray(new InternetAddress[0]));

				msg.setSubject(subject);
				msg.setContent(message, "text/html");
				Transport.send(msg);

			}
		} catch (Exception e) {
			throw e;
		}

	}

	private String checkURL(String url) {
		String str = "";
		try {

			List<String> list = recordSplitFile("/", url);

			str = list.get(1).toString();

		} catch (Exception e) {
			str = "";
			logger.error(JavaMessages.EXCEPTION_ERROR, e);
		}

		return str;

	}

	public List<String> recordSplitFile(String delimit, String url)
			throws Exception {
		String DELIM = delimit;
		// Unless you ask StringTokenizer to give you the tokens, it silently
		// discards multiple tokens.
		StringTokenizer st = new StringTokenizer(url, DELIM, false);
		int MAXFIELDS = st.countTokens();
		List<String> results = new ArrayList<String>(MAXFIELDS);
		String s = "";
		while (st.hasMoreTokens()) {
			s = st.nextToken();
			if (s.length() != 0)
				results.add(s.trim());
			else
				results.add(s.trim());

		}
		return results;
	}
	
	
	
	
	public void postEmail(String recipients,String message,String mode, String noteUserName, String noteName, Map<String, String> map, String type, String noteUserFirstName, String listType, MnEventDetails mnEventDetails)throws MessagingException
	{
		String subject="";
		Date start=null,end=null;
		try{
			String splittedRecipients[]=recipients.split(",");
			logger.info("Before Authendicate "  );
		//Authenticator auth = new SMTPAuthenticator(userName, password);
		logger.info("After Authendicate "  );
		logger.info("User>>>"+userName  );
		logger.info("Password>>>"+password );
		logger.info("host>>>"+host );
		logger.info("portNo>>>"+portNo );
		SimpleDateFormat dateFormat  = new SimpleDateFormat("MMM/dd/yyyy hh:mm a");
		SimpleDateFormat dateFormatEvents  = new SimpleDateFormat("MMM/dd/yyyy");
		if(mnEventDetails != null)
		{
		 start=new Date(mnEventDetails.getStartDate());
		 end=new Date(mnEventDetails.getEndDate());
		}
		boolean debug = false;
		Properties props=super.loadMailProperties(portNo, host,mode);
		
		logger.info("Before Session instance "  );
		// create some properties and get the default Session
		Session session = Session.getDefaultInstance(props);
		session.setDebug(debug);
		logger.info("after debug "  );
		Message msg = new MimeMessage(session);    
		 Transport transport = session.getTransport();
		MimeBodyPart messageBodyPart =  new MimeBodyPart();	       
		
		InternetAddress addressFrom = new InternetAddress("support@musicnoteapp.com");
		msg.setFrom(addressFrom);
		InternetAddress[] addressTo = new InternetAddress[splittedRecipients.length]; 
		for (int i = 0; i < splittedRecipients.length; i++)
		{
			addressTo[i] = new InternetAddress(splittedRecipients[i]);
			logger.info("addressTo[i] :"+addressTo[i]);
			
			String nameNo=map.get(splittedRecipients[i]);
			if(nameNo!=null)
			{
			String arr[]=nameNo.split("~");
			String firstName=arr[0].toString();
			if(listType.equalsIgnoreCase("schedule"))
			{
				if(mnEventDetails.getAlldayevent()==true)
				{
					message="<div style=\"margin:0;padding:0\" dir=\"ltr\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:98%\" border=\"0\"><tbody><tr><td style=\"font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;font-size:12px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"font-size:16px;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;background:#3b5998;color:#ffffff;font-weight:bold;vertical-align:baseline;letter-spacing:-0.03em;text-align:left;padding:10px 38px 4px\"><img src=https://www.musicnoteapp.com/musicnote/pics/logo.png>&nbsp;&nbsp;<a style=\"text-decoration:none\" href=\"https://www.musicnoteapp.com/musicnote/index.html\" target=\"_blank\"><span style=\"background:#3b5998;color:#ffffff;font-weight:bold;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;vertical-align:middle;font-size:16px;letter-spacing:-0.03em;text-align:left;vertical-align:baseline\">Musicnote</span></a></td></tr></tbody></table><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"border-right:1px solid #ccc;line-height:16px;font-size:11px;border-bottom:1px solid #ccc;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;border-top:1px solid #ccc;padding:10px 20px;border-left:1px solid #ccc\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 20px;line-height:16px;width:620px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\">Hi "+firstName+",</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">"+noteUserName+" shared a new event with you on Musicnote.</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\"> Event Name : "+mnEventDetails.getEventName()+"</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">Start Date : "+dateFormatEvents.format(start)+"</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">End date : "+dateFormatEvents.format(end)+"</td></tr><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\"></td></tr><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\"><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse:collapse;width:100%\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px;background-color:#f2f2f2;border-left:none;border-right:none;border-top:1px solid #ccc;border-bottom:1px solid #ccc\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding-right:10px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"border-width:1px;border-style:solid;border-color:#29447e #29447e #1a356e;background-color:#5b74a8\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:2px 6px 4px;border-top:1px solid #8a9cc2\"><a href=\"https://www.musicnoteapp.com/musicnote/index.html\" style=\"color:#3b5998;text-decoration:none\" target=\"_blank\"><span style=\"font-weight:bold;white-space:nowrap;color:#fff;font-size:13px\">Go To Musicnote</span></a></td></tr></tbody></table></td></tr></tbody></table></td><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\"></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"border-right:none;color:#999999;font-size:11px;border-bottom:none;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;border:none;border-top:none;border-left:none\">This message was sent to you from Musicnote .If you don\'t want to receive these emails from Musicnote in the future, please <a href=\"https://www.musicnoteapp.com/musicnote/unsubscribe.html\" style=\"color:#3b5998;text-decoration:none\" target=\"_blank\">unsubscribe.</a><br>Musicnote, Inc., 3240 E. State St. Ext., Hamilton NJ, 08619.</td></tr></tbody></table></div>";
				}
				else
				{
					message="<div style=\"margin:0;padding:0\" dir=\"ltr\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:98%\" border=\"0\"><tbody><tr><td style=\"font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;font-size:12px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"font-size:16px;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;background:#3b5998;color:#ffffff;font-weight:bold;vertical-align:baseline;letter-spacing:-0.03em;text-align:left;padding:10px 38px 4px\"><img src=https://www.musicnoteapp.com/musicnote/pics/logo.png>&nbsp;&nbsp;<a style=\"text-decoration:none\" href=\"https://www.musicnoteapp.com/musicnote/index.html\" target=\"_blank\"><span style=\"background:#3b5998;color:#ffffff;font-weight:bold;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;vertical-align:middle;font-size:16px;letter-spacing:-0.03em;text-align:left;vertical-align:baseline\">Musicnote</span></a></td></tr></tbody></table><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"border-right:1px solid #ccc;line-height:16px;font-size:11px;border-bottom:1px solid #ccc;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;border-top:1px solid #ccc;padding:10px 20px;border-left:1px solid #ccc\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 20px;line-height:16px;width:620px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\">Hi "+firstName+",</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">"+noteUserName+" shared a new event with you on Musicnote.</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\"> Event Name : "+mnEventDetails.getEventName()+"</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">Start Date : "+dateFormat.format(start)+"</td></tr><tr><td style=\"font-size:13px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\">End date : "+dateFormat.format(end)+"</td></tr><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\"></td></tr><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px 0 15px 0\"><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse:collapse;width:100%\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px;background-color:#f2f2f2;border-left:none;border-right:none;border-top:1px solid #ccc;border-bottom:1px solid #ccc\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding-right:10px\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"border-width:1px;border-style:solid;border-color:#29447e #29447e #1a356e;background-color:#5b74a8\"><table cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse\"><tbody><tr><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:2px 6px 4px;border-top:1px solid #8a9cc2\"><a href=\"https://www.musicnoteapp.com/musicnote/index.html\" style=\"color:#3b5998;text-decoration:none\" target=\"_blank\"><span style=\"font-weight:bold;white-space:nowrap;color:#fff;font-size:13px\">Go To Musicnote</span></a></td></tr></tbody></table></td></tr></tbody></table></td><td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif\"></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse:collapse;width:620px\"><tbody><tr><td style=\"border-right:none;color:#999999;font-size:11px;border-bottom:none;font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;border:none;border-top:none;border-left:none\">This message was sent to you from Musicnote .If you don\'t want to receive these emails from Musicnote in the future, please <a href=\"https://www.musicnoteapp.com/musicnote/unsubscribe.html\" style=\"color:#3b5998;text-decoration:none\" target=\"_blank\">unsubscribe.</a><br>Musicnote, Inc., 3240 E. State St. Ext., Hamilton NJ, 08619.</td></tr></tbody></table></div>";
				}
				subject=noteUserFirstName+" shared a new event with you";
			}
			else
			{
			if(type.equalsIgnoreCase("Note"))
			{
				message=MailContent.sharingNotification+firstName+MailContent.sharingNotification1+noteUserName+"  shared a new note with you on Musicnote : "+ noteName+" "+MailContent.sharingNotification2;;
				subject=noteUserFirstName+" shared a new note with you";
			}
			else if(type.equalsIgnoreCase("Copy"))
			{
				message=MailContent.sharingNotification+firstName+MailContent.sharingNotification1+noteUserName+"  shared a new note with you on Musicnote : "+ noteName+" "+MailContent.sharingNotification2;;
				subject=noteUserFirstName+" shared a copy of a note with you";
			}
			else
			{
				message=MailContent.sharingNotification+firstName+MailContent.sharingNotification1+noteUserName+"  shared a new Notebook with you on Musicnote : "+ noteName+" "+MailContent.sharingNotification2;;
				subject=noteUserFirstName+" shared a new Notebook with you";
			}
			}
			
			messageBodyPart.setText(message);
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);	    
			msg.setContent(message, "text/html");
			
			logger.info("message :"+message);
			msg.setRecipient(Message.RecipientType.TO, addressTo[i]);
			logger.info("after set receipients "  );
			// Setting the Subject and Content Type
			msg.setSubject(subject);	    
			msg.setSentDate(getDateNow());	    
			// msg.setContent(message, "text/plain");
			 //String toAddress=addressTo[i];
			msg.addRecipient(Message.RecipientType.TO,addressTo[i]);
		    transport.connect(host, Integer.parseInt(portNo), userName, password);
		    transport.sendMessage(msg, msg.getRecipients(Message.RecipientType.TO));
		    transport.close();
			}
		}
		}catch(Exception e){
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public  Date  getDateNow() {
		Calendar currentDate = Calendar.getInstance();
		return currentDate.getTime();
	}
	
	/*public String checkURL()
	{
		String str = "";
		try
		{
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			String url = request.getRequestURL().toString();

			List<String> list = recordSplitFile("/", url);

			str = list.get(1).toString();

		}
		catch (Exception e)
		{
			str = "";
			logger.error("Error", e);
		}

		return str;

	}*/
	
	
}
