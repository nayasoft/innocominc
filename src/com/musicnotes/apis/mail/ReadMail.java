package com.musicnotes.apis.mail;

import java.io.IOException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

import org.apache.log4j.Logger;

public class ReadMail extends AMailCommon{

	String folderType;
	String portNo;
	String host;
	String userName;
	String password;
	Logger logger;
	String jsonString;
	String mode;

	public ReadMail() {
		
		logger = Logger.getLogger(ReadMail.class);
	}
	
	public void initialize(String folderType, String portNo, String host,String userName, String password,String mode)
	{
		
		
		this.folderType = folderType;
		this.portNo = portNo;
		this.host = host;
		this.userName = userName;
		this.password = password;
		this.mode=mode;
		if(logger.isDebugEnabled())
		logger.debug("folderType : " + folderType + "portNo :" + portNo + " host : " + host +" userName :" +userName +" password :  " +password);
	}

	/**
	 * @param args
	 */
	public String readMail(String mailCount) throws IOException  {
		StringBuffer jsonMailList=new StringBuffer("[");
		Properties props=super.loadMailProperties(portNo, host,mode);
       
		try {
			Session session = Session.getDefaultInstance(props, null);
			Store store = session.getStore("imaps");
			store.connect(host, userName, password);

			Folder folder = store.getFolder(folderType);
			folder.open(Folder.READ_WRITE);
			if (folder.getFullName().equalsIgnoreCase("inbox")) {
				if(logger.isDebugEnabled())
				logger.debug("No. of unRead Messages : "
						+ folder.getUnreadMessageCount());
			}
		
			    		
			 int messageCount = folder.getMessageCount();
		        int startMessage = messageCount - 5;
		        int endMessage = messageCount;
		 
		        if (messageCount < 5) {
		            startMessage = 0;
		        }
		        else
		        {
		        	mailCount="5";
		        	startMessage=Integer.parseInt(mailCount);
		        	endMessage=startMessage+5;
		        }
		 
		        Message[] messages = folder.getMessages(startMessage, endMessage);
		 
			for (Message message : messages) {
				Object content = message.getContent();
				String mailSubject=message.getSubject();
				Address[] mailFrom=message.getFrom();
				if (message.getContentType().equals("multipart")) {
					Multipart multipart = (Multipart) message.getContent();

					for (int x = 0; x < multipart.getCount(); x++) {
						BodyPart bodyPart = multipart.getBodyPart(x);
						String disposition = bodyPart.getDisposition();

						if (disposition != null
								&& (disposition.equals(BodyPart.ATTACHMENT))) {
							if(logger.isDebugEnabled())
								logger.debug("Mail have some attachment : ");
							DataHandler handler = bodyPart.getDataHandler();
							logger.info("file name : "
									+ handler.getName());
						} else {
							logger.info("body content : \t "
									+ bodyPart.getContent());
						}
					}

				} else if (content instanceof String) {
					jsonMailList.append("{");
					jsonMailList.append("\"body\"");
					jsonMailList.append(":");
					jsonMailList.append("\"");
					jsonMailList.append(java.net.URLEncoder.encode((String) content,"UTF-8"));
					jsonMailList.append("\"");					
					jsonMailList.append(",");
					
					jsonMailList.append("\"subject\"");
					jsonMailList.append(":");
					jsonMailList.append("\"");
					jsonMailList.append(java.net.URLEncoder.encode(mailSubject,"UTF-8"));
					jsonMailList.append("\"");
					jsonMailList.append(",");
					
					jsonMailList.append("\"from\"");
					jsonMailList.append(":");
					jsonMailList.append("\"");
					jsonMailList.append(java.net.URLEncoder.encode(mailFrom[0].toString(),"UTF-8"));
					jsonMailList.append("\"");

					jsonMailList.append("},");

				} else {
					logger.info("-----------------------------------------------------------------");
				}
			}
			int i=jsonMailList.lastIndexOf(",");
			jsonMailList = jsonMailList.deleteCharAt(i);
			
			String jsonObj = "{\"unreadMailCount\":\""+folder.getUnreadMessageCount()+"\",\"folderMessages\":"+jsonMailList.toString()+"]}";
			
			jsonString=jsonObj.toString();
			logger.info("i :" + i);
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} 
		return jsonString;
	}

}
