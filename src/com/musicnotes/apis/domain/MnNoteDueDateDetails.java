package com.musicnotes.apis.domain;

public class MnNoteDueDateDetails {
	private String _id;

	private Integer userId;
	private Integer listId;
	private String listType;
	private Integer noteId;
	private String dueDate;
	private String status;
	
	@Override
	public String toString() {
		return "MnNotesDuedateDetails [_id:" + get_id() + ", userId:" + getUserId()+ ", listId:" + getListId()+ ", listType:" + getListType()+ ", noteId:" + getNoteId() + ", dueDate:" + getDueDate() + ", status:" + getStatus() + "]";
	}
	
	
	public String get_id() {
		return _id;
	}
	public void set_id(String id) {
		_id = id;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getListId() {
		return listId;
	}
	public void setListId(Integer listId) {
		this.listId = listId;
	}
	public String getListType() {
		return listType;
	}
	public void setListType(String listType) {
		this.listType = listType;
	}
	public Integer getNoteId() {
		return noteId;
	}
	public void setNoteId(Integer noteId) {
		this.noteId = noteId;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
