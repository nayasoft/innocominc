package com.musicnotes.apis.domain;

public class MnUsersToken {

	private Integer userId;
	private String tokens;
	private String loginTime;
	private String createdDateTime;
	private String Ipaddress;


	@Override
	public String toString() {
		return "MnUsersToken [userId:" + getUserId() + ", tokens:"
				+ getTokens() + ", loginTime:" + getLoginTime() + ", Ipaddress:" + getIpaddress() +  ",createdDateTime:"
				+ getCreatedDateTime() + "]";
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setTokens(String tokens) {
		this.tokens = tokens;
	}

	public String getTokens() {
		return tokens;
	}

	public void setLoginTime(String loginTime) {
		this.loginTime = loginTime;
	}

	public String getLoginTime() {
		return loginTime;
	}

	public void setCreatedDateTime(String createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getCreatedDateTime() {
		return createdDateTime;
	}

	public void setIpaddress(String ipaddress) {
		Ipaddress = ipaddress;
	}

	public String getIpaddress() {
		return Ipaddress;
	}
}
