package com.musicnotes.apis.domain;

public class MnStudents
{
	private Integer studentId;
	private String studentName;
	private String studentFirstName;
	private String studentLastName;
	private String studentEmailId;
	private String studentContactNumber;
	private String emergencyContactName;
	private String emergencyContactNumber;
	private String parentName;
	private String parentContactNumber;

	public Integer getStudentId()
	{
		return studentId;
	}

	public void setStudentId(Integer studentId)
	{
		this.studentId = studentId;
	}

	public String getStudentName()
	{
		return studentName;
	}

	public void setStudentName(String studentName)
	{
		this.studentName = studentName;
	}

	public String getStudentFirstName()
	{
		return studentFirstName;
	}

	public void setStudentFirstName(String studentFirstName)
	{
		this.studentFirstName = studentFirstName;
	}

	public String getStudentLastName()
	{
		return studentLastName;
	}

	public void setStudentLastName(String studentLastName)
	{
		this.studentLastName = studentLastName;
	}

	public String getStudentEmailId()
	{
		return studentEmailId;
	}

	public void setStudentEmailId(String studentEmailId)
	{
		this.studentEmailId = studentEmailId;
	}

	public String getStudentContactNumber()
	{
		return studentContactNumber;
	}

	public void setStudentContactNumber(String studentContactNumber)
	{
		this.studentContactNumber = studentContactNumber;
	}

	public String getEmergencyContactName()
	{
		return emergencyContactName;
	}

	public void setEmergencyContactName(String emergencyContactName)
	{
		this.emergencyContactName = emergencyContactName;
	}

	public String getEmergencyContactNumber()
	{
		return emergencyContactNumber;
	}

	public void setEmergencyContactNumber(String emergencyContactNumber)
	{
		this.emergencyContactNumber = emergencyContactNumber;
	}

	public String getParentName()
	{
		return parentName;
	}

	public void setParentName(String parentName)
	{
		this.parentName = parentName;
	}

	public String getParentContactNumber()
	{
		return parentContactNumber;
	}

	public void setParentContactNumber(String parentContactNumber)
	{
		this.parentContactNumber = parentContactNumber;
	}

}
