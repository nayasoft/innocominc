package com.musicnotes.apis.domain;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MnNoteDetails {
	private String _id;
	private Integer listId;
	private String listName;
	private String listType;
	private Integer userId;
	private String noteAccess;
	private boolean defaultNote;
	private boolean enableDisableFlag;
	private List<Integer> sharedIndividuals=new ArrayList<Integer>();
	private List<Integer> sharedGroups=new ArrayList<Integer>();
	private boolean shareAllContactFlag;
	private String listOwnerStatus;
	private String publicDescription;
	private String status;
	private Integer noteId;
    private String noteName;
    private Date startDate;
    private List<Integer> notesMembers= new ArrayList<Integer>();
    private List<Integer> noteGroups= new ArrayList<Integer>();
    private boolean noteSharedAllContact;
    private List<Integer> noteSharedAllContactMembers= new ArrayList<Integer>();
    private String ownerNoteStatus;
    private List<Integer> tag= new ArrayList<Integer>();
    private List<Integer> remainders= new ArrayList<Integer>();
    private List<Integer> vote= new ArrayList<Integer>();
    private String links;
    private Date endDate;
    private String dueDate;
    private String dueTime;
    private String access;
    private List<Integer> attachFilePath= new ArrayList<Integer>();
    private String description;
    private List<Integer> comments = new ArrayList<Integer>();
    private List<Integer> pcomments= new ArrayList<Integer>();
    private List<Integer> copyToMember= new ArrayList<Integer>();
    private List<Integer> copyToGroup= new ArrayList<Integer>();
    private boolean copyToAllContact;
    private String copyFromMember;
    private String publicUser;
    private Date publicDate;
    private String shareType;
    private List<Integer> privateTags= new ArrayList<Integer>();
    private List<Integer> privateAttachFilePath= new ArrayList<Integer>();
    private Date tempSharedDate;
    private Integer originalListId ;
    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    
	@Override
	public String toString() {
		
		return "[{\"_id\":\"" + _id + "\", \"access\":\"" + access
				+ "\", \"attachFilePath\":\"" + attachFilePath + "\", \"comments\":\""
				+ comments + "\", \"copyFromMember\":\"" + copyFromMember
				+ "\", \"copyToAllContact\":\"" + copyToAllContact + "\", \"copyToGroup\":\""
				+ copyToGroup + "\", \"copyToMember\":\"" + copyToMember
				+ "\", \"defaultNote\":\"" + defaultNote + "\", \"description\":\""
				+ description + "\", \"dueDate\":\"" + dueDate + "\", \"dueTime\":\"" + dueTime
				+ "\", \"enableDisableFlag\":\"" + enableDisableFlag + "\", \"endDate\":\""
				+ endDate + "\", \"links\":\"" + links + "\", \"listId\":\"" + listId
				+ "\", \"listName\":\"" + listName + "\", \"listType\":\"" + listType
				+ "\", \"noteAccess\":\"" + noteAccess + "\", \"noteGroups\":\"" + noteGroups
				+ "\", \"noteId\":\"" + noteId + "\", \"noteName\":\"" + noteName
				+ "\", \"noteSharedAllContact\":\"" + noteSharedAllContact
				+ "\", \"noteSharedAllContactMembers\":\""
				+ noteSharedAllContactMembers  
				+ "\", \"notesMembers\":\"" + notesMembers + "\", \"ownerNoteStatus\":\""
				+ ownerNoteStatus + "\", \"pcomments\":\"" + pcomments+"\", \"privateAttachFilePath\":\"" + privateAttachFilePath
				+ "\", \"privateTags\":\"" + privateTags + "\", \"publicDate\":\"" + publicDate
				+ "\", \"publicUser\":\"" + publicUser + "\", \"remainders\":\"" + remainders
				+ "\", \"shareAllContactFlag\":\"" + shareAllContactFlag
				+ "\", \"shareType\":\"" + shareType + "\", \"sharedGroups\":\"" + sharedGroups
				+ "\", \"sharedIndividuals\":\"" + sharedIndividuals + "\", \"startDate\":\""
				+ formatter.format(startDate) + "\", \"status\":\"" + status + "\", \"tag\":\"" + tag
				+ "\", \"userId\":\"" + userId + "\", \"vote\":\"" + vote + "\"}]";
	}
	
	public String get_id() {
		return _id;
	}
	public void set_id(String id) {
		_id = id;
	}
	public Integer getListId() {
		return listId;
	}
	public void setListId(Integer listId) {
		this.listId = listId;
	}
	public String getListName() {
		return listName;
	}
	public void setListName(String listName) {
		this.listName = listName;
	}
	public String getListType() {
		return listType;
	}
	public void setListType(String listType) {
		this.listType = listType;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getNoteAccess() {
		return noteAccess;
	}
	public void setNoteAccess(String noteAccess) {
		this.noteAccess = noteAccess;
	}
	public boolean isDefaultNote() {
		return defaultNote;
	}
	public void setDefaultNote(boolean defaultNote) {
		this.defaultNote = defaultNote;
	}
	public boolean isEnableDisableFlag() {
		return enableDisableFlag;
	}
	public void setEnableDisableFlag(boolean enableDisableFlag) {
		this.enableDisableFlag = enableDisableFlag;
	}
	public List<Integer> getSharedIndividuals() {
		return sharedIndividuals;
	}
	public void setSharedIndividuals(List<Integer> sharedIndividuals) {
		this.sharedIndividuals = sharedIndividuals;
	}
	public List<Integer> getSharedGroups() {
		return sharedGroups;
	}
	public void setSharedGroups(List<Integer> sharedGroups) {
		this.sharedGroups = sharedGroups;
	}
	public boolean isShareAllContactFlag() {
		return shareAllContactFlag;
	}
	public void setShareAllContactFlag(boolean shareAllContactFlag) {
		this.shareAllContactFlag = shareAllContactFlag;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getNoteId() {
		return noteId;
	}
	public void setNoteId(Integer noteId) {
		this.noteId = noteId;
	}
	public String getNoteName() {
		return noteName;
	}
	public void setNoteName(String noteName) {
		this.noteName = noteName;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public List<Integer> getNotesMembers() {
		return notesMembers;
	}
	public void setNotesMembers(List<Integer> notesMembers) {
		this.notesMembers = notesMembers;
	}
	public List<Integer> getNoteGroups() {
		return noteGroups;
	}
	public void setNoteGroups(List<Integer> noteGroups) {
		this.noteGroups = noteGroups;
	}
	public List<Integer> getNoteSharedAllContactMembers() {
		return noteSharedAllContactMembers;
	}
	public void setNoteSharedAllContactMembers(
			List<Integer> noteSharedAllContactMembers) {
		this.noteSharedAllContactMembers = noteSharedAllContactMembers;
	}
	public String getOwnerNoteStatus() {
		return ownerNoteStatus;
	}
	public void setOwnerNoteStatus(String ownerNoteStatus) {
		this.ownerNoteStatus = ownerNoteStatus;
	}
	public List<Integer> getTag() {
		return tag;
	}
	public void setTag(List<Integer> tag) {
		this.tag = tag;
	}
	public List<Integer> getRemainders() {
		return remainders;
	}
	public void setRemainders(List<Integer> remainders) {
		this.remainders = remainders;
	}
	public List<Integer> getVote() {
		return vote;
	}
	public void setVote(List<Integer> vote) {
		this.vote = vote;
	}
	public String getLinks() {
		return links;
	}
	public void setLinks(String links) {
		this.links = links;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getDueTime() {
		return dueTime;
	}
	public void setDueTime(String dueTime) {
		this.dueTime = dueTime;
	}
	public String getAccess() {
		return access;
	}
	public void setAccess(String access) {
		this.access = access;
	}
	public List<Integer> getAttachFilePath() {
		return attachFilePath;
	}
	public void setAttachFilePath(List<Integer> attachFilePath) {
		this.attachFilePath = attachFilePath;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Integer> getComments() {
		return comments;
	}
	public void setComments(List<Integer> comments) {
		this.comments = comments;
	}
	public List<Integer> getPcomments() {
		return pcomments;
	}
	public void setPcomments(List<Integer> pcomments) {
		this.pcomments = pcomments;
	}
	public List<Integer> getCopyToMember() {
		return copyToMember;
	}
	public void setCopyToMember(List<Integer> copyToMember) {
		this.copyToMember = copyToMember;
	}
	public List<Integer> getCopyToGroup() {
		return copyToGroup;
	}
	public void setCopyToGroup(List<Integer> copyToGroup) {
		this.copyToGroup = copyToGroup;
	}
	public String getCopyFromMember() {
		return copyFromMember;
	}
	public void setCopyFromMember(String copyFromMember) {
		this.copyFromMember = copyFromMember;
	}
	public String getPublicUser() {
		return publicUser;
	}
	public void setPublicUser(String publicUser) {
		this.publicUser = publicUser;
	}
	public Date getPublicDate() {
		return publicDate;
	}
	public void setPublicDate(Date publicDate) {
		this.publicDate = publicDate;
	}
	public String getShareType() {
		return shareType;
	}
	public void setShareType(String shareType) {
		this.shareType = shareType;
	}
	public List<Integer> getPrivateTags() {
		return privateTags;
	}
	public void setPrivateTags(List<Integer> privateTags) {
		this.privateTags = privateTags;
	}

	public boolean isNoteSharedAllContact()
	{
		return noteSharedAllContact;
	}

	public void setNoteSharedAllContact(boolean noteSharedAllContact)
	{
		this.noteSharedAllContact = noteSharedAllContact;
	}

	public boolean isCopyToAllContact()
	{
		return copyToAllContact;
	}

	public void setCopyToAllContact(boolean copyToAllContact)
	{
		this.copyToAllContact = copyToAllContact;
	}

	public void setTempSharedDate(Date tempSharedDate)
	{
		this.tempSharedDate = tempSharedDate;
	}

	public Date getTempSharedDate()
	{
		return tempSharedDate;
	}

	public void setOriginalListId(Integer originalListId) {
		this.originalListId = originalListId;
	}

	public Integer getOriginalListId() {
		return originalListId;
	}

	public void setListOwnerStatus(String listOwnerStatus) {
		this.listOwnerStatus = listOwnerStatus;
	}

	public String getListOwnerStatus() {
		return listOwnerStatus;
	}

	public void setPrivateAttachFilePath(List<Integer> privateAttachFilePath) {
		this.privateAttachFilePath = privateAttachFilePath;
	}

	public List<Integer> getPrivateAttachFilePath() {
		return privateAttachFilePath;
	}

	public String getPublicDescription() {
		return publicDescription;
	}

	public void setPublicDescription(String publicDescription) {
		this.publicDescription = publicDescription;
	} 

	
}
