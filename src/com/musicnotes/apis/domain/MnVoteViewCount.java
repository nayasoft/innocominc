package com.musicnotes.apis.domain;

public class MnVoteViewCount {
private Integer countId;
private String listId;
private String noteId;
private Integer vote;
private Integer viewed;
private String tempNote;
private String adminVote;
private String status;

public Integer getCountId() {
	return countId;
}
public String getListId() {
	return listId;
}
public String getNoteId() {
	return noteId;
}
public Integer getVote() {
	return vote;
}
public Integer getViewed() {
	return viewed;
}
public void setCountId(Integer countId) {
	this.countId = countId;
}
public void setListId(String listId) {
	this.listId = listId;
}
public void setNoteId(String noteId) {
	this.noteId = noteId;
}
public void setVote(Integer vote) {
	this.vote = vote;
}
public void setViewed(Integer viewed) {
	this.viewed = viewed;
}
public void setTempNote(String tempNote) {
	this.tempNote = tempNote;
}
public String getTempNote() {
	return tempNote;
}
public void setAdminVote(String adminVote)
{
	this.adminVote = adminVote;
}
public String getAdminVote()
{
	return adminVote;
}
public void setStatus(String status) {
	this.status = status;
}
public String getStatus() {
	return status;
}

}
