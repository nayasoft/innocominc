package com.musicnotes.apis.domain;

public class MnSubscriptionMulti {

	private Integer Id;
	private Integer userId;
	private String emailId;
	private String startDate;
	private String endDate;
	private Integer subId;
	private String status;
	private Integer paidUserId; 
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public Integer getSubId() {
		return subId;
	}
	public void setSubId(Integer subId) {
		this.subId = subId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getPaidUserId() {
		return paidUserId;
	}
	public void setPaidUserId(Integer paidUserId) {
		this.paidUserId = paidUserId;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getEndDate() {
		return endDate;
	}
}
