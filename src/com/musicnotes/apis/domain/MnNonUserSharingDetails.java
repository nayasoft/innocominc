package com.musicnotes.apis.domain;

public class MnNonUserSharingDetails {

	private Integer nsId;
	private String emailId;
	private Integer userId;
	private Integer noteId;
	private Integer listId;
	private String status;
	private String sharingLevel;
	private String noteLevel;
	private String listType;
	@Override
	public String toString() {
		return "Mn_Non-UserSharingDetails [Id:" + getNsId() + ", emailId : "+getEmailId()+", sharingUserId:" + getUserId()+ ", listId:" + getListId()+ ", noteId:" + getNoteId() + 
		", sharingLevel:" + getSharingLevel()+ ",listType : "+getListType()+", noteLevel: "+getNoteLevel()+" ,status:" + getStatus() + "]";
	}
	public Integer getNsId() {
		return nsId;
	}
	public void setNsId(Integer nsId) {
		this.nsId = nsId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getNoteId() {
		return noteId;
	}
	public void setNoteId(Integer noteId) {
		this.noteId = noteId;
	}
	public Integer getListId() {
		return listId;
	}
	public void setListId(Integer listId) {
		this.listId = listId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setSharingLevel(String sharingLevel) {
		this.sharingLevel = sharingLevel;
	}
	public String getSharingLevel() {
		return sharingLevel;
	}
	public void setNoteLevel(String noteLevel) {
		this.noteLevel = noteLevel;
	}
	public String getNoteLevel() {
		return noteLevel;
	}
	public void setListType(String listType) {
		this.listType = listType;
	}
	public String getListType() {
		return listType;
	}
	
}
