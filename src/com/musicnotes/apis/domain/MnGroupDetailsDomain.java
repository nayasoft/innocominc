package com.musicnotes.apis.domain;


public class MnGroupDetailsDomain {
	private String _id;
	
	private Integer gdId;
	private Integer groupId;
	private Integer userId;	
	private String startDate;
	private String endDate;
	private String status;
	private Integer ownerId;
	
	public MnGroupDetailsDomain() {
		
	}
	
	@Override
	public String toString() {
		return "{\"_id\":\""+get_id()+"\"gdId\":\""+getGdId()+"\"groupId\":\""+getGroupId()+"\"userId\":\""+getUserId()+"\",\" startDate\":\"" + getStartDate()+ "\",\"endDate\":\"" + getEndDate() + "\",\"status\":\""+getStatus()+"\",\"ownerId\":\""+getOwnerId()+"\"}";
	}
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public void setGdId(Integer gdId) {
		this.gdId = gdId;
	}

	public Integer getGdId() {
		return gdId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String get_id() {
		return _id;
	}

	public void setOwnerId(Integer ownerId)
	{
		this.ownerId = ownerId;
	}

	public Integer getOwnerId()
	{
		return ownerId;
	}
}
