package com.musicnotes.apis.domain;

public class MnAdminVotes {

	private Integer userId;
	private String listId;
	private String noteId;
	private String cDate;
	private String status;
	private String editDate;
	
	@Override
	public String toString() {
		return "{userId:"+getUserId()+", listId:"+getListId()+", noteId:"+getNoteId()+", cDate:"+getcDate()+", status:"+getStatus()+", editDate:"+getEditDate()+"}";
	}
	
	public String getListId() {
		return listId;
	}
	public void setListId(String listId) {
		this.listId = listId;
	}
	public String getNoteId() {
		return noteId;
	}
	public void setNoteId(String noteId) {
		this.noteId = noteId;
	}
	public String getcDate() {
		return cDate;
	}
	public void setcDate(String cDate) {
		this.cDate = cDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEditDate() {
		return editDate;
	}
	public void setEditDate(String editDate) {
		this.editDate = editDate;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getUserId() {
		return userId;
	}
}
