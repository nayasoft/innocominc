/**
 * 
 */
package com.musicnotes.apis.domain;

import java.util.HashSet;
import java.util.Set;

/**
 * @author sbashid
 *
 */
public class MnGroupDomain {
	private String _id;

	private Integer groupId;
	private String loginUserId;
	
	private String groupName;
	private String defaultGroup;
	private Set<Integer> groupDetails=new HashSet<Integer>();
	
	private MnGroupDetailsDomain mnGroupDetailsDomain;
	
	@Override
	public String toString() {
		return "MnGroupDomain [_id:" + get_id() + ", loginUserId:" + getLoginUserId()+ ", groupId:" + getGroupId()+ ", groupName:" + getGroupName() + ",groupDetails:"+getGroupDetails()+"]";
	}

	public String get_id() {
		return _id;
	}
	public void set_id(String id) {
		_id = id;
	}
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	
	public String getLoginUserId() {
		return loginUserId;
	}

	public void setLoginUserId(String loginUserId) {
		this.loginUserId = loginUserId;
	}

	
	public void setGroupDetails(Set<Integer> groupDetails)
	{
		this.groupDetails = groupDetails;
	}

	public Set<Integer> getGroupDetails()
	{
		return groupDetails;
	}

	
	public MnGroupDetailsDomain getMnGroupDetailsDomain() {
		return mnGroupDetailsDomain;
	}

	public void setMnGroupDetailsDomain(MnGroupDetailsDomain mnGroupDetailsDomain) {
		this.mnGroupDetailsDomain = mnGroupDetailsDomain;
	}

	public void setDefaultGroup(String defaultGroup) {
		this.defaultGroup = defaultGroup;
	}

	public String getDefaultGroup() {
		return defaultGroup;
	}

	}
