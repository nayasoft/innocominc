package com.musicnotes.apis.domain;

public class MnPublicSharedNoteCopyDetails
{
	private Integer userId;
	private String listId;
	private String noteId;
	private String startDate;
	private String endDate;
	private String status;
	
	@Override
	public String toString() {
		return "{userId:"+getUserId()+", listId:"+getListId()+", noteId:"+getNoteId()+", startDate:"+getStartDate()+", endDate:"+getEndDate()+", status:"+getStatus()+"}";
	}
	
	public Integer getUserId()
	{
		return userId;
	}
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	public String getListId()
	{
		return listId;
	}
	public void setListId(String listId)
	{
		this.listId = listId;
	}
	public String getNoteId()
	{
		return noteId;
	}
	public void setNoteId(String noteId)
	{
		this.noteId = noteId;
	}
	public String getStartDate()
	{
		return startDate;
	}
	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}
	public String getEndDate()
	{
		return endDate;
	}
	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
}
