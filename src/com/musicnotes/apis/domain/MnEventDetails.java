package com.musicnotes.apis.domain;

public class MnEventDetails
{
	private String _id;
	private Integer cId;
	private Integer userId;
	private String eventName;
	private String startDate;
	private String endDate;
	private Integer eventId;
	private Boolean alldayevent;
	private String listType;
	private String description;
	private Integer listId;
	private String listName;
	private String status;
	private String location;
	private String eventStartDate;
	private String eventEndDate;
	
	//Temp variable for setting groupId
	private Integer groupId;
	private String dateRange;

	
	private String repeatEvent;
	
	@Override
	public String toString()
	{
		return "{cId:" + getcId() + ", userId:" + getUserId() +
		", eventName:" + getEventName() + ", startDate:" + getStartDate() +
		", endDate:" + getEndDate() + ",alldayevent:"+getAlldayevent()+",description:"+getDescription()+
		", eventId:"+getEventId()+",status:"+getStatus()+",listType:"+getListType()+",listId:"+getListId()+
		", listName:"+getListName()+",location:"+getLocation()+"}";
	}

	public Integer getcId()
	{
		return cId;
	}

	public void setcId(Integer cId)
	{
		this.cId = cId;
	}

	public Integer getUserId()
	{
		return userId;
	}

	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	public String getEventName()
	{
		return eventName;
	}

	public void setEventName(String eventName)
	{
		this.eventName = eventName;
	}

	public String getStartDate()
	{
		return startDate;
	}

	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}

	public String getEndDate()
	{
		return endDate;
	}

	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}

	public void set_id(String _id)
	{
		this._id = _id;
	}

	public String get_id()
	{
		return _id;
	}

	public void setEventId(Integer eventId)
	{
		this.eventId = eventId;
	}

	public Integer getEventId()
	{
		return eventId;
	}

	public void setListType(String listType)
	{
		this.listType = listType;
	}

	public String getListType()
	{
		return listType;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getDescription()
	{
		return description;
	}

	public void setListId(Integer listId)
	{
		this.listId = listId;
	}

	public Integer getListId()
	{
		return listId;
	}

	public void setAlldayevent(Boolean alldayevent)
	{
		this.alldayevent = alldayevent;
	}

	public Boolean getAlldayevent()
	{
		return alldayevent;
	}

	public void setListName(String listName)
	{
		this.listName = listName;
	}

	public String getListName()
	{
		return listName;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getStatus()
	{
		return status;
	}

	public void setGroupId(Integer groupId)
	{
		this.groupId = groupId;
	}

	public Integer getGroupId()
	{
		return groupId;
	}

	public void setDateRange(String dateRange)
	{
		this.dateRange = dateRange;
	}

	public String getDateRange()
	{
		return dateRange;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public String getLocation()
	{
		return location;
	}

	public void setEventStartDate(String eventStartDate)
	{
		this.eventStartDate = eventStartDate;
	}

	public String getEventStartDate()
	{
		return eventStartDate;
	}

	public void setEventEndDate(String eventEndDate)
	{
		this.eventEndDate = eventEndDate;
	}

	public String getEventEndDate()
	{
		return eventEndDate;
	}

	public void setRepeatEvent(String repeatEvent) {
		this.repeatEvent = repeatEvent;
	}

	public String getRepeatEvent() {
		return repeatEvent;
	}

}
