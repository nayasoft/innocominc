package com.musicnotes.apis.domain;

import java.util.Comparator;
import java.util.Date;


public class MnNotesDetails implements Comparable<MnNotesDetails>  {

	private String noteName;
	private Integer noteId;
	private String status;
	private String access;
	private String listId;
	private Integer userId;
	private Integer viewed;
	private Integer countId;
	private Integer vote;
	private String attachFile;
	private Date startDate;
	private String comments;
	private String publicUser;
	private String publicDate;
	private String tagIds;
	
	
	
	public String getStatus() {
		return status;
	}
	public String getAccess() {
		return access;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	public void setAccess(String access) {
		this.access = access;
	}
	public void setNoteName(String noteName) {
		this.noteName = noteName;
	}
	public String getNoteName() {
		return noteName;
	}
	public void setNoteId(Integer noteId) {
		this.noteId = noteId;
	}
	public Integer getNoteId() {
		return noteId;
	}
	public void setListId(String listId) {
		this.listId = listId;
	}
	public String getListId() {
		return listId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setViewed(Integer viewed) {
		this.viewed = viewed;
	}
	public Integer getViewed() {
		return viewed;
	}
	public void setCountId(Integer countId) {
		this.countId = countId;
	}
	public Integer getCountId() {
		return countId;
	}
	public void setVote(Integer vote) {
		this.vote = vote;
	}
	public Integer getVote() {
		return vote;
	}
	public void setAttachFile(String attachFile) {
		this.attachFile = attachFile;
	}
	public String getAttachFile() {
		return attachFile;
	}
	
	
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getStartDate() {
		return startDate;
	}
	public MnNotesDetails()
	{
		
	}
	public MnNotesDetails(String noteName, Integer noteId, String status,
			String access, String listId, Integer userId, Integer viewed,
			Integer countId, Integer vote, String attachFile, Date startDate) {
		super();
		this.noteName = noteName;
		this.noteId = noteId;
		this.status = status;
		this.access = access;
		this.listId = listId;
		this.userId = userId;
		this.viewed = viewed;
		this.countId = countId;
		this.vote = vote;
		this.attachFile = attachFile;
		this.startDate = startDate;
	}
	@Override
	public String toString() {
		return "MnNotesDetails [access=" + access + ", attachFile="
				+ attachFile + ", countId=" + countId + ", listId=" + listId
				+ ", noteId=" + noteId + ", noteName=" + noteName
				+ ", startDate=" + startDate + ", status=" + status
				+ ", userId=" + userId + ", viewed=" + viewed + ", vote="
				+ vote + "]";
	}
	


	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getComments() {
		return comments;
	}



	public void setPublicUser(String publicUser) {
		this.publicUser = publicUser;
	}
	public String getPublicUser() {
		return publicUser;
	}



	public void setPublicDate(String publicDate) {
		this.publicDate = publicDate;
	}
	public String getPublicDate() {
		return publicDate;
	}



	public void setTagIds(String tagIds)
	{
		this.tagIds = tagIds;
	}
	public String getTagIds()
	{
		return tagIds;
	}



	// Comparator
    public static class CompId implements Comparator<MnNotesDetails> {
        @Override
        public int compare(MnNotesDetails arg0, MnNotesDetails arg1) {
            return arg0.noteId - arg1.noteId;
        }
    }

    public static class CompDate implements Comparator<MnNotesDetails> {
        private int mod = 1;
        public CompDate(boolean desc) {
            if (desc) mod =-1;
        }
        @Override
        public int compare(MnNotesDetails arg0, MnNotesDetails arg1) {
            return mod*arg0.publicDate.compareTo(arg1.publicDate);
        }
    }
   
    public static Comparator<MnNotesDetails> SalaryComparator = new Comparator<MnNotesDetails>() {
    	 
        @Override
        public int compare(MnNotesDetails e1, MnNotesDetails e2) {
            return (int) (e1.getViewed() - e2.getViewed());
        }
    };

    public static Comparator<MnNotesDetails> SalaryComparator1 = new Comparator<MnNotesDetails>() {
   	 
        @Override
        public int compare(MnNotesDetails e1, MnNotesDetails e2) {
            return (int) (e1.getVote() - e2.getVote());
        }
    };


	@Override
	public int compareTo(MnNotesDetails o) {
		if(getVote()!=null)
		{

			return getVote().compareTo(o.getVote());
		
		}
		else
		{
			return getViewed().compareTo(o.getViewed());
		}
	}
	
	
	
}
	

