package com.musicnotes.apis.domain;

public class MnTeacher
{
	private Integer teacherId;
	private String teacherName;
	private String teacherFirstName;
	private String teacherLastName;
	private String teacherEmailId;
	private String teacherContactNumbers;
	private String emergencyContactName;
	private String emergencyContactNumber;

	public Integer getTeacherId()
	{
		return teacherId;
	}

	public void setTeacherId(Integer teacherId)
	{
		this.teacherId = teacherId;
	}

	public String getTeacherName()
	{
		return teacherName;
	}

	public void setTeacherName(String teacherName)
	{
		this.teacherName = teacherName;
	}

	public String getTeacherFirstName()
	{
		return teacherFirstName;
	}

	public void setTeacherFirstName(String teacherFirstName)
	{
		this.teacherFirstName = teacherFirstName;
	}

	public String getTeacherLastName()
	{
		return teacherLastName;
	}

	public void setTeacherLastName(String teacherLastName)
	{
		this.teacherLastName = teacherLastName;
	}

	public String getTeacherEmailId()
	{
		return teacherEmailId;
	}

	public void setTeacherEmailId(String teacherEmailId)
	{
		this.teacherEmailId = teacherEmailId;
	}

	public String getTeacherContactNumbers()
	{
		return teacherContactNumbers;
	}

	public void setTeacherContactNumbers(String teacherContactNumbers)
	{
		this.teacherContactNumbers = teacherContactNumbers;
	}

	public String getEmergencyContactName()
	{
		return emergencyContactName;
	}

	public void setEmergencyContactName(String emergencyContactName)
	{
		this.emergencyContactName = emergencyContactName;
	}

	public void setEmergencyContactNumber(String emergencyContactNumber)
	{
		this.emergencyContactNumber = emergencyContactNumber;
	}

	public String getEmergencyContactNumber()
	{
		return emergencyContactNumber;
	}

}
