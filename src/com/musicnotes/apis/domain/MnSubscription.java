package com.musicnotes.apis.domain;

public class MnSubscription {
	
	private Integer subId;
	private Integer userId;
	private String userName;
	private String emailId;
	private String subDate;
	private String subStartDate;
	private String subEndDate;
	private String subDays;
	private String subType;
	private String totAmount;
	private String paidFor;
	private String txnNo;
	private String txnType;
	private String subscrId;
	private String domain;
	
	public Integer getSubId() {
		return subId;
	}
	public void setSubId(Integer subId) {
		this.subId = subId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getSubStartDate() {
		return subStartDate;
	}
	public void setSubStartDate(String subStartDate) {
		this.subStartDate = subStartDate;
	}
	public String getSubEndDate() {
		return subEndDate;
	}
	public void setSubEndDate(String subEndDate) {
		this.subEndDate = subEndDate;
	}
	public String getSubDays() {
		return subDays;
	}
	public void setSubDays(String subDays) {
		this.subDays = subDays;
	}
	public void setSubDate(String subDate) {
		this.subDate = subDate;
	}
	public String getSubDate() {
		return subDate;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	public String getSubType() {
		return subType;
	}
	public void setTotAmount(String totAmount) {
		this.totAmount = totAmount;
	}
	public String getTotAmount() {
		return totAmount;
	}
	
	@Override
	public String toString() {
		return "MnSubscription [domain=" + domain + ", emailId=" + emailId
				+ ", paidFor=" + paidFor + ", subDate=" + subDate
				+ ", subDays=" + subDays + ", subEndDate=" + subEndDate
				+ ", subId=" + subId + ", subStartDate=" + subStartDate
				+ ", subType=" + subType + ", totAmount=" + totAmount
				+ ", txnNo=" + txnNo + ", userId=" + userId + ", userName="
				+ userName + "]";
	}
	
	public void setPaidFor(String paidFor) {
		this.paidFor = paidFor;
	}
	public String getPaidFor() {
		return paidFor;
	}
	public void setTxnNo(String txnNo) {
		this.txnNo = txnNo;
	}
	public String getTxnNo() {
		return txnNo;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getDomain() {
		return domain;
	}
	public void setSubscrId(String subscrId) {
		this.subscrId = subscrId;
	}
	public String getSubscrId() {
		return subscrId;
	}
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	public String getTxnType() {
		return txnType;
	}

}
