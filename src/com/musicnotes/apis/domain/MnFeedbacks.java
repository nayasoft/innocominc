package com.musicnotes.apis.domain;

public class MnFeedbacks {
	private String userFeedback;
	private String userMailId;
	private String feedbackType;
	private String status;
	private String userFirstName;
	private String userLastName;
	private Integer userId;
	private String date;
	
	public void setUserFeedback(String userFeedback) {
		this.userFeedback = userFeedback;
	}
	public String getUserFeedback() {
		return userFeedback;
		
	}
	public void setUserMailId(String userMailId) {
		this.userMailId = userMailId;
	}
	public String getUserMailId() {
		
		return userMailId;
	}
	public void setFeedbackType(String feedbackType) {
		this.feedbackType = feedbackType;
	}
	public String getFeedbackType() {
		return feedbackType;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus() {
		return status;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDate() {
		return date;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}
	public String getUserLastName() {
		return userLastName;
	}

}
