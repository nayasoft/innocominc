package com.musicnotes.apis.domain;

public class MnBlockedUsers
{
	private Integer blockedId;
	private String userName;
	private Integer userId;
	private Integer adminId;
	private String blockedLevel;
	private String crowdShareFlag;
	private String numberOfDays;
	private String blockedDate;
	private String endBlockedDate;
	private String status;
	public Integer getBlockedId() {
		return blockedId;
	}
	public void setBlockedId(Integer blockedId) {
		this.blockedId = blockedId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public Integer getAdminId() {
		return adminId;
	}
	public void setAdminId(Integer adminId) {
		this.adminId = adminId;
	}
	public String getBlockedLevel() {
		return blockedLevel;
	}
	public void setBlockedLevel(String blockedLevel) {
		this.blockedLevel = blockedLevel;
	}
	public String getCrowdShareFlag() {
		return crowdShareFlag;
	}
	public void setCrowdShareFlag(String crowdShareFlag) {
		this.crowdShareFlag = crowdShareFlag;
	}
	public String getNumberOfDays() {
		return numberOfDays;
	}
	public void setNumberOfDays(String numberOfDays) {
		this.numberOfDays = numberOfDays;
	}
	public String getBlockedDate() {
		return blockedDate;
	}
	public void setBlockedDate(String blockedDate) {
		this.blockedDate = blockedDate;
	}
	public String getEndBlockedDate() {
		return endBlockedDate;
	}
	public void setEndBlockedDate(String endBlockedDate) {
		this.endBlockedDate = endBlockedDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	

}
