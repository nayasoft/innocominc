package com.musicnotes.apis.domain;

import java.util.ArrayList;
import java.util.List;

public class MnRemainders {

	private Integer rId;
	private String rName;
	private Integer userId;
	private String listId;
	private String noteId;
	private String cDate;
	private boolean sendMail;
	private String eventDate;
	private String eventTime;
	private String status;
	private String editDate;
	private Integer edited;
	private List<Integer> inactiveRemainderUserId=new ArrayList<Integer>();
	
	@Override
	public String toString() {
		return "{rId:"+getrId()+", userId:"+getUserId()+", listId:"+getListId()+", noteId:"+getNoteId()+", rName:"+getrName()+", cDate:"+getcDate()+"," +
		"inactiveRemainderUserId:"+getInactiveRemainderUserId().toString()+", eventDate:"+getEventDate()+", eventTime:"+getEventTime()+", sendMail:"+isSendMail()+" status:"+getStatus()+", edited:"+getEdited()+",editDate:"+getEditDate()+"}";
	}
	
	public Integer getrId() {
		return rId;
	}
	public void setrId(Integer rId) {
		this.rId = rId;
	}
	public String getrName() {
		return rName;
	}
	public void setrName(String rName) {
		this.rName = rName;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getListId() {
		return listId;
	}
	public void setListId(String listId) {
		this.listId = listId;
	}
	public String getNoteId() {
		return noteId;
	}
	public void setNoteId(String noteId) {
		this.noteId = noteId;
	}
	public String getcDate() {
		return cDate;
	}
	public void setcDate(String cDate) {
		this.cDate = cDate;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEditDate() {
		return editDate;
	}
	public void setEditDate(String editDate) {
		this.editDate = editDate;
	}
	public Integer getEdited() {
		return edited;
	}
	public void setEdited(Integer edited) {
		this.edited = edited;
	}

	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	public String getEventTime() {
		return eventTime;
	}

	public void setSendMail(boolean sendMail) {
		this.sendMail = sendMail;
	}

	public boolean isSendMail() {
		return sendMail;
	}

	public void setInactiveRemainderUserId(List<Integer> inactiveRemainderUserId)
	{
		this.inactiveRemainderUserId = inactiveRemainderUserId;
	}

	public List<Integer> getInactiveRemainderUserId()
	{
		return inactiveRemainderUserId;
	}
}
