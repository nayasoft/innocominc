package com.musicnotes.apis.domain;

public class MnEventsSharingDetails
{
	private String _id;
	private Integer eventSharedId;
	private Integer userId;
	private Integer sharingUserId;
	private Integer listId;
	private Integer sharingGroupId;
	private String sharingLevel;
	private String noteLevel;
	private boolean shareAllContactFlag;
	private String listType;
	private Integer noteId;
	private String status;
	private String sharingStatus;
	
	
	public Integer getUserId()
	{
		return userId;
	}
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	public Integer getSharingUserId()
	{
		return sharingUserId;
	}
	public void setSharingUserId(Integer sharingUserId)
	{
		this.sharingUserId = sharingUserId;
	}
	public Integer getListId()
	{
		return listId;
	}
	public void setListId(Integer listId)
	{
		this.listId = listId;
	}
	public String getListType()
	{
		return listType;
	}
	public void setListType(String listType)
	{
		this.listType = listType;
	}
	public Integer getNoteId()
	{
		return noteId;
	}
	public void setNoteId(Integer noteId)
	{
		this.noteId = noteId;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public String getSharingStatus()
	{
		return sharingStatus;
	}
	public void setSharingStatus(String sharingStatus)
	{
		this.sharingStatus = sharingStatus;
	}
	public void setSharingGroupId(Integer sharingGroupId)
	{
		this.sharingGroupId = sharingGroupId;
	}
	public Integer getSharingGroupId()
	{
		return sharingGroupId;
	}
	
	@Override
	public String toString()
	{
		return "MnEventsSharingDetails [listId:" + getListId() + ", listType:" + getListType() + ", noteId:" + getNoteId() + ", sharingStatus:" + getSharingStatus() + ", sharingUserId:" + getSharingUserId() + ", status:" + getStatus() + ", noteLevel:" + getNoteLevel() + ", userId:" + getUserId() + ", sharingGroupId:"+getSharingGroupId()+",eventSharedId:"+getEventSharedId()+",_Id:"+get_id()+"]";
	}
	public void setEventSharedId(Integer eventSharedId)
	{
		this.eventSharedId = eventSharedId;
	}
	public Integer getEventSharedId()
	{
		return eventSharedId;
	}
	public void set_id(String _id)
	{
		this._id = _id;
	}
	public String get_id()
	{
		return _id;
	}
	public void setSharingLevel(String sharingLevel) {
		this.sharingLevel = sharingLevel;
	}
	public String getSharingLevel() {
		return sharingLevel;
	}
	public void setShareAllContactFlag(boolean shareAllContactFlag) {
		this.shareAllContactFlag = shareAllContactFlag;
	}
	public boolean isShareAllContactFlag() {
		return shareAllContactFlag;
	}
	public void setNoteLevel(String noteLevel)
	{
		this.noteLevel = noteLevel;
	}
	public String getNoteLevel()
	{
		return noteLevel;
	}
	
}
