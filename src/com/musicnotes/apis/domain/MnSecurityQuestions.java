package com.musicnotes.apis.domain;

public class MnSecurityQuestions {
	
	private String userId;
	private String QuestionId;
	private String answer;
	private String question; // this variable is only for pojo class purpose not in table 
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserId() {
		return userId;
	}
	public void setQuestionId(String questionId) {
		this.QuestionId = questionId;
	}
	public String getQuestionId() {
		return QuestionId;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getAnswer() {
		return answer;
	}
	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}
	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}
	

}
