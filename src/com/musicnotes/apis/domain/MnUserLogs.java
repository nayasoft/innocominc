package com.musicnotes.apis.domain;

public class MnUserLogs
{
	private Integer userId;
	private String version;
	private String browserNameVersion;
	private String osName;
	private String loginTime;
	private String logoutTime;
	private String screenSize;
	
	@Override
	public String toString()
	{
		return "MnUserLog [ userId=" + userId + ", version=" + version + ", osName="+ osName +
		", loginTime=" + loginTime + ", logoutTime=" + logoutTime+",browserNameVersion="+browserNameVersion+",screenSize="+screenSize;
	}
	
	public Integer getUserId()
	{
		return userId;
	}
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	public String getVersion()
	{
		return version;
	}
	public void setVersion(String version)
	{
		this.version = version;
	}
	
	public String getOsName()
	{
		return osName;
	}
	public void setOsName(String osName)
	{
		this.osName = osName;
	}
	public String getLoginTime()
	{
		return loginTime;
	}
	public void setLoginTime(String loginTime)
	{
		this.loginTime = loginTime;
	}
	public String getLogoutTime()
	{
		return logoutTime;
	}
	public void setLogoutTime(String logoutTime)
	{
		this.logoutTime = logoutTime;
	}

	public void setBrowserNameVersion(String browserNameVersion)
	{
		this.browserNameVersion = browserNameVersion;
	}

	public String getBrowserNameVersion()
	{
		return browserNameVersion;
	}

	public void setScreenSize(String screenSize)
	{
		this.screenSize = screenSize;
	}

	public String getScreenSize()
	{
		return screenSize;
	}

}
