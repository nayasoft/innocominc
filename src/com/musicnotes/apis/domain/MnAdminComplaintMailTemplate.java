package com.musicnotes.apis.domain;

public class MnAdminComplaintMailTemplate {
	
	private Integer templateId;
	private String templateName;
	private String templateText;
	private String status;
	
	
	@Override
	public String toString() {
		return "MnAdminComplaintMailTemplate [templateId=" + templateId
				+ ", templateName=" + templateName + ", templateText="
				+ templateText + ", status=" + status + "]";
	}
	
	public Integer getTemplateId() {
		return templateId;
	}
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public String getTemplateText() {
		return templateText;
	}
	public void setTemplateText(String templateText) {
		this.templateText = templateText;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

}
