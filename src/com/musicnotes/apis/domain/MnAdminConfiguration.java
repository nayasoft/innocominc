package com.musicnotes.apis.domain;

import java.util.ArrayList;
import java.util.List;

public class MnAdminConfiguration {
	
	
	private String uploadLimit;
	private boolean mailConfiguration;
	private String userId;
	private String startDate;
	private String endDate;
	private String status;
	private String configId;
	private List<String> paymentAmountDays=new ArrayList<String>();
	
	
	public void setUploadLimit(String uploadLimit) {
		this.uploadLimit = uploadLimit;
	}
	public String getUploadLimit() {
		return uploadLimit;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserId() {
		return userId;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus() {
		return status;
	}
	public void setPaymentAmountDays(List<String> paymentAmountDays) {
		this.paymentAmountDays = paymentAmountDays;
	}
	public List<String> getPaymentAmountDays() {
		return paymentAmountDays;
	}
	public void setConfigId(String configId) {
		this.configId = configId;
	}
	public String getConfigId() {
		return configId;
	}
	public void setMailConfiguration(boolean mailConfiguration) {
		this.mailConfiguration = mailConfiguration;
	}
	public boolean isMailConfiguration() {
		return mailConfiguration;
	}

}
