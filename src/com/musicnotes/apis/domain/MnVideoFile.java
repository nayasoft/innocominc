package com.musicnotes.apis.domain;

public class MnVideoFile
{
	String userId;
	String fileName;
	String _id;
	String fileType;
	String status;
	private Integer Id;
	private String fileDummyName;
	private String uploadDate;
	private String fullPath;
	private double fileSize;
	
	
	public String getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}

	public MnVideoFile(){
		
	}
	
	public MnVideoFile(String userId, String fileName, String type,
			String status, Integer id,String fileDummyName,String uploadDate,String fullpath,double fileSize) {
		
		this.userId = userId;
		this.fileName = fileName;
		this.fileType = type;
		this.status = status;
		Id = id;
		this.fileDummyName=fileDummyName;
		this.uploadDate=uploadDate;
		this.fullPath=fullpath;
		this.fileSize=fileSize;
	}

	public String getUserId()
	{
		return userId;
	}
	public void setUserId(String userId)
	{
		this.userId = userId;
	}
	public String getFileName()
	{
		return fileName;
	}
	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}
	
	
	public String get_id()
	{
		return _id;
	}
	public void set_id(String id)
	{
		_id = id;
	}
	
	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	

	@Override
	public String toString() {
		return "MnVideoFile [Id=" + Id + ", _id=" + _id + ", fileDummyName="
				+ fileDummyName + ", fileName=" + fileName + ", fileType="
				+ fileType + ", status=" + status + ", fullPath=" 
				+ fullPath + ", uploadDate=" 
				+ uploadDate + ", userId=" + userId + "]";
	}

	public void setId(Integer id) {
		Id = id;
	}

	public Integer getId() {
		return Id;
	}

	public void setFileDummyName(String fileDummyName) {
		this.fileDummyName = fileDummyName;
	}

	public String getFileDummyName() {
		return fileDummyName;
	}

	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}

	public String getFullPath() {
		return fullPath;
	}

	public void setFileSize(double fileSize) {
		this.fileSize = fileSize;
	}

	public double getFileSize() {
		return fileSize;
	}

	

	/*public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public Date getUploadDate() {
		return uploadDate;
	}*/
	
}
