package com.musicnotes.apis.domain;

public class MnSubEventDetails {
private Integer listId;
private Integer eventId;
private String _id;
private Integer Id;
private Integer userId;
private String eventName;
private String startDate;
private String endDate;
private Boolean alldayevent;
private Integer c_id;
private String listType;
private String description;
private String status;
private String location;
private String repeatEvent;
private boolean emailFlag;
private String timeZone;
private String eventEndDate;

//Tempvariable
private String dateRange;

public Integer getListId() {
	return listId;
}
public Integer getEventId() {
	return eventId;
}
public String get_id() {
	return _id;
}
public Integer getId() {
	return Id;
}
public Integer getUserId() {
	return userId;
}
public String getEventName() {
	return eventName;
}
public String getStartDate() {
	return startDate;
}
public String getEndDate() {
	return endDate;
}
public Boolean getAlldayevent() {
	return alldayevent;
}
public String getListType() {
	return listType;
}
public String getDescription() {
	return description;
}
public String getStatus() {
	return status;
}
public String getLocation() {
	return location;
}
public void setListId(Integer listId) {
	this.listId = listId;
}
public void setEventId(Integer eventId) {
	this.eventId = eventId;
}
public void set_id(String id) {
	_id = id;
}
public void setId(Integer Id) {
	this.Id = Id;
}
public void setUserId(Integer userId) {
	this.userId = userId;
}
public void setEventName(String eventName) {
	this.eventName = eventName;
}
public void setStartDate(String startDate) {
	this.startDate = startDate;
}
public void setEndDate(String endDate) {
	this.endDate = endDate;
}
public void setAlldayevent(Boolean alldayevent) {
	this.alldayevent = alldayevent;
}
public void setListType(String listType) {
	this.listType = listType;
}
public void setDescription(String description) {
	this.description = description;
}
public void setStatus(String status) {
	this.status = status;
}
public void setLocation(String location) {
	this.location = location;
}
public void setC_id(Integer c_id) {
	this.c_id = c_id;
}
public Integer getC_id() {
	return c_id;
}
public void setRepeatEvent(String repeatEvent) {
	this.repeatEvent = repeatEvent;
}
public String getRepeatEvent() {
	return repeatEvent;
}
public void setEmailFlag(boolean emailFlag) {
	this.emailFlag = emailFlag;
}
public boolean isEmailFlag() {
	return emailFlag;
}
public void setTimeZone(String timeZone) {
	this.timeZone = timeZone;
}
public String getTimeZone() {
	return timeZone;
}
public void setEventEndDate(String eventEndDate) {
	this.eventEndDate = eventEndDate;
}
public String getEventEndDate() {
	return eventEndDate;
}
/**
 * @return the dateRange
 */
public String getDateRange() {
	return dateRange;
}
/**
 * @param dateRange the dateRange to set
 */
public void setDateRange(String dateRange) {
	this.dateRange = dateRange;
}

 
}
