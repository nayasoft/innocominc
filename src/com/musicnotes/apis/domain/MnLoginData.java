package com.musicnotes.apis.domain;
public class MnLoginData implements java.io.Serializable
{
	private String userId;
	private String inTime;
	private String outTime;

	
	public MnLoginData(){
		
	}
	public MnLoginData(String userId, String inTime, String outTime) {
		this.userId=userId;
		this.inTime=inTime;
		this.outTime=outTime;
	}
	
	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getUserId()
	{
		return userId;
	}
	

	
	
	public String getInTime()
	{
		return inTime;
	}



	public void setInTime(String inTime)
	{
		this.inTime = inTime;
	}



	public String getOutTime()
	{
		return outTime;
	}



	public void setOutTime(String outTime)
	{
		this.outTime = outTime;
	}



	@Override
	public String toString() {
		return "MnLoginData [userId=" + userId + ", inTime=" + inTime + ",outTime="+outTime+"]";
	}
}