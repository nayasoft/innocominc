package com.musicnotes.apis.domain;

public class MnNotesSharingDetails {
	private String _id;

	private Integer userId;
	private Integer sharingUserId;
	private Integer sharingGroupId;
	private Integer listId;
	private String listType;
	private Integer noteId;
	private String sharedDate;
	private String endDate;
	private String sharingLevel;
	private String noteLevel;
	private boolean shareAllContactFlag;
	private String status;
	private Integer sharedBookId=0;
	
	
	@Override
	public String toString() {
		return "MnNotesSharingDetails [_id:" + get_id() + ", userId:" + getUserId()+ ", sharingUserId:" + getSharingUserId()+ ", sharingGroupId:" + getSharingGroupId()+ ", listId:" + getListId()+ ", listType:" + getListType()+ ", noteId:" + getNoteId() + ", shareAllContactFlag:" + isShareAllContactFlag() + ", sharedDate:" + getSharedDate() + ", endDate:" + getEndDate() + ", sharingLevel:" + getSharingLevel() + ",noteLevel:" + getNoteLevel() + ", status:" + getStatus() + ", sharedBookId:" + getSharedBookId() + "]";
	}
	
	public String get_id() {
		return _id;
	}
	public void set_id(String id) {
		_id = id;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getNoteId() {
		return noteId;
	}
	public void setNoteId(Integer noteId) {
		this.noteId = noteId;
	}
	public String getSharedDate() {
		return sharedDate;
	}
	public void setSharedDate(String sharedDate) {
		this.sharedDate = sharedDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public void setSharingUserId(Integer sharingUserId) {
		this.sharingUserId = sharingUserId;
	}

	public Integer getSharingUserId() {
		return sharingUserId;
	}

	public void setListId(Integer listId) {
		this.listId = listId;
	}

	public Integer getListId() {
		return listId;
	}

	public void setListType(String listType) {
		this.listType = listType;
	}

	public String getListType() {
		return listType;
	}

	public void setSharingGroupId(Integer sharingGroupId) {
		this.sharingGroupId = sharingGroupId;
	}

	public Integer getSharingGroupId() {
		return sharingGroupId;
	}

	public void setSharingLevel(String sharingLevel) {
		this.sharingLevel = sharingLevel;
	}

	public String getSharingLevel() {
		return sharingLevel;
	}

	public void setShareAllContactFlag(boolean shareAllContactFlag) {
		this.shareAllContactFlag = shareAllContactFlag;
	}

	public boolean isShareAllContactFlag() {
		return shareAllContactFlag;
	}

	public void setNoteLevel(String noteLevel)
	{
		this.noteLevel = noteLevel;
	}

	public String getNoteLevel()
	{
		return noteLevel;
	}

	public void setSharedBookId(Integer sharedBookId)
	{
		this.sharedBookId = sharedBookId;
	}

	public Integer getSharedBookId()
	{
		return sharedBookId;
	}
}
