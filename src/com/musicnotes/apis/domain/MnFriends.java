package com.musicnotes.apis.domain;

import java.util.Date;

public class MnFriends
{

	private String _id;
	private Integer userId;
	private Integer requestedUserId;
	private String status;
	private String requestedUserName;
	private Date requestedDate;
	private Date acceptedDate;
	private Date declineDate;

	@Override
	public String toString()
	{
		return "MnFriends [_id=" + _id + ", userId=" + userId + ", requestedUserId=" + requestedUserId 
				+ ", requestStatus=" + status + ", requestedDate="+ requestedDate 
				+ ", acceptedDate=" + acceptedDate + ", declineDate=" + declineDate;
	}

	public void set_id(String _id)
	{
		this._id = _id;
	}

	public String get_id()
	{
		return _id;
	}

	public Integer getUserId()
	{
		return userId;
	}

	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	/**
	 * @param requestedUserId
	 *            the requestedUserId to set
	 */
	public void setRequestedUserId(Integer requestedUserId)
	{
		this.requestedUserId = requestedUserId;
	}

	/**
	 * @return the requestedUserId
	 */
	public Integer getRequestedUserId()
	{
		return requestedUserId;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public void setRequestedUserName(String requestedUserName)
	{
		this.requestedUserName = requestedUserName;
	}

	public String getRequestedUserName()
	{
		return requestedUserName;
	}

	public Date getRequestedDate()
	{
		return requestedDate;
	}

	public void setRequestedDate(Date requestedDate)
	{
		this.requestedDate = requestedDate;
	}

	public Date getAcceptedDate()
	{
		return acceptedDate;
	}

	public void setAcceptedDate(Date acceptedDate)
	{
		this.acceptedDate = acceptedDate;
	}

	public Date getDeclineDate()
	{
		return declineDate;
	}

	public void setDeclineDate(Date declineDate)
	{
		this.declineDate = declineDate;
	}

}
