package com.musicnotes.apis.domain;

public class MnInvitedUsers {
	
	private Integer Id;
	private Integer userId;
	private String invitedMailID;
	private String date;
	private String status;
	private boolean userCreated;
	private Integer paidSubId;
	private String userSelectDate;
	
	@Override
	public String toString() {
		return "MnInvitedUsers [Id=" + Id + ", date=" + date
				+ ", invitedMailID=" + invitedMailID + ", status=" + status
				+ ", userId=" + userId + "]";
	}
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getInvitedMailID() {
		return invitedMailID;
	}
	public void setInvitedMailID(String invitedMailID) {
		this.invitedMailID = invitedMailID;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public void setUserCreated(boolean userCreated) {
		this.userCreated = userCreated;
	}

	public boolean isUserCreated() {
		return userCreated;
	}

	public void setPaidSubId(Integer paidSubId) {
		this.paidSubId = paidSubId;
	}

	public Integer getPaidSubId() {
		return paidSubId;
	}

	public void setUserSelectDate(String userSelectDate) {
		this.userSelectDate = userSelectDate;
	}

	public String getUserSelectDate() {
		return userSelectDate;
	}

}
