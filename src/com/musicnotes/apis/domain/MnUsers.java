package com.musicnotes.apis.domain;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class MnUsers
{
	private String securityId;
	private String _id;
	private Integer userId;
	private String userName;
	private byte[] password;
	private String passwordText;
	private String userFirstName;
	private String userLastName;
	private String contactNumber;
	private String emailId;
	private String userRole;
	private String emergencyContactName;
	private String emergencyContactNumber;
	private String termscheck;
	private String notificationFlag;
	private String noteCreateBasedOn;
	
	
	private boolean publicShareWarnMsgFlag;
	private String favoriteMusic;
	private boolean mailCheckFlag;
	private String status;
	private String startDate;
	private String endDate;
	private Set<Integer> followers=new HashSet<Integer>(0);
	private Set<Integer> friends=new HashSet<Integer>(0);
	private String tempEmail;
	
	private MnFriends mnFriends;
	private String instrument;
	private String skillLevel;
	private String filePath;
	
	private boolean requestPending;
	private String timeZone;
	private String userLevel;
	private Double uploadedSize;
	private Double limitedSize;
	private Integer requestedUser;
	private boolean multipleUsersRequest;
	private boolean adminFlag;
	private String mailCheckId;
	private Integer addedByUserId;
	private String addedUserStatus;
	private String displayUserName;
	private boolean cropFlag;
	private Integer complaintCount;
	private String blockedStatus;
	private String blockedDays;
	private String BlockedDate;
	private boolean addUserFlag;
	
	private boolean adminNotificationFlag;
	
	public MnUsers()
	{
		
	}

	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	public Integer getUserId()
	{
		return userId;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setPassword(byte[] password)
	{
		this.password = password;
	}

	public byte[] getPassword()
	{
		return password;
	}

	public void setUserFirstName(String userFirstName)
	{
		this.userFirstName = userFirstName;
	}

	public String getUserFirstName()
	{
		return userFirstName;
	}

	public void setUserLastName(String userLastName)
	{
		this.userLastName = userLastName;
	}

	public String getUserLastName()
	{
		return userLastName;
	}

	public void setContactNumber(String contactNumber)
	{
		this.contactNumber = contactNumber;
	}

	public String getContactNumber()
	{
		return contactNumber;
	}

	public void setEmailId(String emailId)
	{
		this.emailId = emailId;
	}

	public String getEmailId()
	{
		return emailId;
	}

	public void setUserRole(String userRole)
	{
		this.userRole = userRole;
	}

	public String getUserRole()
	{
		return userRole;
	}

	public void setEmergencyContactName(String emergencyContactName)
	{
		this.emergencyContactName = emergencyContactName;
	}

	public String getEmergencyContactName()
	{
		return emergencyContactName;
	}

	public void setEmergencyContactNumber(String emergencyContactNumber)
	{
		this.emergencyContactNumber = emergencyContactNumber;
	}

	public String getEmergencyContactNumber()
	{
		return emergencyContactNumber;
	}

	

	
	public Set<Integer> getFollowers()
	{
		return followers;
	}

	public void setFollowers(Set<Integer> followers)
	{
		this.followers = followers;
	}
	

	public MnUsers(String id, Integer userId, String userName, byte[] password,
			String passwordText, String userFirstName, String userLastName,
			String contactNumber, String emailId, String userRole,
			String emergencyContactName, String emergencyContactNumber,
			String gender, String birthDate, String address, String classId,
			String lesson, String experience, String qulification,
			String payperHours,String notificationFlag,String noteCreateBasedOn,String status,
			String startDate, String endDate,boolean publicShareWarnMsgFlag) {
		_id = id;
		this.userId = userId;
		this.userName = userName;
		this.password = password;
		this.passwordText = passwordText;
		this.userFirstName = userFirstName;
		this.userLastName = userLastName;
		this.contactNumber = contactNumber;
		this.emailId = emailId;
		this.userRole = userRole;
		this.emergencyContactName = emergencyContactName;
		this.emergencyContactNumber = emergencyContactNumber;
		this.notificationFlag=notificationFlag;
		this.noteCreateBasedOn=noteCreateBasedOn;
		this.status = status;
		this.startDate = startDate;
		this.endDate = endDate;
		this.publicShareWarnMsgFlag=publicShareWarnMsgFlag;
	}




	public void set_id(String _id) {
		this._id = _id;
	}
	public String get_id() {
		return _id;
	}


	public void setPasswordText(String passwordText)
	{
		this.passwordText = passwordText;
	}


	public String getPasswordText()
	{
		return passwordText;
	}


	


	public void setNotificationFlag(String notificationFlag) {
		this.notificationFlag = notificationFlag;
	}




	public String getNotificationFlag() {
		return notificationFlag;
	}

	

	public void setStatus(String status) {
		this.status = status;
	}


	public String getStatus() {
		return status;
	}


	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}




	public String getStartDate() {
		return startDate;
	}




	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}




	public String getEndDate() {
		return endDate;
	}






	/**
	 * @param friends the friends to set
	 */
	public void setFriends(Set<Integer> friends)
	{
		this.friends = friends;
	}




	/**
	 * @return the friends
	 */
	public Set<Integer> getFriends()
	{
		return friends;
	}

	/**
	 * @param mnFriends the mnFriends to set
	 */
	public void setMnFriends(MnFriends mnFriends)
	{
		this.mnFriends = mnFriends;
	}

	/**
	 * @return the mnFriends
	 */
	public MnFriends getMnFriends()
	{
		return mnFriends;
	}

	public void setNoteCreateBasedOn(String noteCreateBasedOn) {
		this.noteCreateBasedOn = noteCreateBasedOn;
	}

	public String getNoteCreateBasedOn() {
		return noteCreateBasedOn;
	}

	public void setTempEmail(String tempEmail) {
		this.tempEmail = tempEmail;
	}

	public String getTempEmail() {
		return tempEmail;
	}

	public void setPublicShareWarnMsgFlag(boolean publicShareWarnMsgFlag)
	{
		this.publicShareWarnMsgFlag = publicShareWarnMsgFlag;
	}

	public boolean isPublicShareWarnMsgFlag()
	{
		return publicShareWarnMsgFlag;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setSkillLevel(String skillLevel) {
		this.skillLevel = skillLevel;
	}

	public String getSkillLevel() {
		return skillLevel;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFavoriteMusic(String favoriteMusic) {
		this.favoriteMusic = favoriteMusic;
	}

	public String getFavoriteMusic() {
		return favoriteMusic;
	}

	public void setRequestPending(boolean requestPending) {
		this.requestPending = requestPending;
	}

	public boolean isRequestPending() {
		return requestPending;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTermscheck(String termscheck) {
		this.termscheck = termscheck;
	}

	public String getTermscheck() {
		return termscheck;
	}

	public void setUserLevel(String userLevel) {
		this.userLevel = userLevel;
	}

	public String getUserLevel() {
		return userLevel;
	}

	

	

	public void setRequestedUser(Integer requestedUser) {
		this.requestedUser = requestedUser;
	}

	public Integer getRequestedUser() {
		return requestedUser;
	}

	public void setMultipleUsersRequest(boolean multipleUsersRequest) {
		this.multipleUsersRequest = multipleUsersRequest;
	}

	public boolean isMultipleUsersRequest() {
		return multipleUsersRequest;
	}

	public void setAdminFlag(boolean adminFlag) {
		this.adminFlag = adminFlag;
	}

	public boolean isAdminFlag() {
		return adminFlag;
	}

	public void setMailCheckFlag(boolean mailCheckFlag) {
		this.mailCheckFlag = mailCheckFlag;
	}

	public boolean isMailCheckFlag() {
		return mailCheckFlag;
	}

	@Override
	public String toString() {
		return "MnUsers [_id=" + _id + ", adminFlag=" + adminFlag
				+ ", contactNumber=" + contactNumber + ", emailId=" + emailId
				+ ", emergencyContactName=" + emergencyContactName
				+ ", emergencyContactNumber=" + emergencyContactNumber
				+ ", endDate=" + endDate + ", favoriteMusic=" + favoriteMusic
				+ ", filePath=" + filePath + ", followers=" + followers
				+ ", friends=" + friends + ", instrument=" + instrument
				+ ", mailCheckFlag=" + mailCheckFlag + ", mnFriends="
				+ mnFriends + ", multipleUsersRequest=" + multipleUsersRequest
				+ ", noteCreateBasedOn=" + noteCreateBasedOn
				+ ", notificationFlag=" + notificationFlag + ", password="
				+ Arrays.toString(password) + ", passwordText=" + passwordText
				+ ", publicShareWarnMsgFlag=" + publicShareWarnMsgFlag
				+ ", requestPending=" + requestPending + ", requestedUser="
				+ requestedUser + ", skillLevel=" + skillLevel + ", startDate="
				+ startDate + ", status=" + status + ", tempEmail=" + tempEmail
				+ ", termscheck=" + termscheck + ", timeZone=" + timeZone
				+ ", uploadedSize=" + uploadedSize + ", userFirstName="
				+ userFirstName + ", userId=" + userId + ", userLastName="
				+ userLastName + ", userLevel=" + userLevel + ", userName="
				+ userName + ", userRole=" + userRole + "]";
	}

	public void setMailCheckId(String mailCheckId) {
		this.mailCheckId = mailCheckId;
	}

	public String getMailCheckId() {
		return mailCheckId;
	}

	public void setAddedByUserId(Integer addedByUserId) {
		this.addedByUserId = addedByUserId;
	}

	public Integer getAddedByUserId() {
		return addedByUserId;
	}

	public void setAddedUserStatus(String addedUserStatus) {
		this.addedUserStatus = addedUserStatus;
	}

	public String getAddedUserStatus() {
		return addedUserStatus;
	}

	public void setDisplayUserName(String displayUserName) {
		this.displayUserName = displayUserName;
	}

	public String getDisplayUserName() {
		return displayUserName;
	}

	public String getSecurityId() {
		return securityId;
	}

	public void setSecurityId(String securityId) {
		this.securityId = securityId;
	}

	public void setCropFlag(boolean cropFlag) {
		this.cropFlag = cropFlag;
	}

	public boolean isCropFlag() {
		return cropFlag;
	}

	public Double getUploadedSize() {
		return uploadedSize;
	}

	public void setUploadedSize(Double uploadedSize) {
		this.uploadedSize = uploadedSize;
	}

	public void setLimitedSize(Double limitedSize) {
		this.limitedSize = limitedSize;
	}

	public Double getLimitedSize() {
		return limitedSize;
	}

	public Integer getComplaintCount() {
		return complaintCount;
	}

	public void setComplaintCount(Integer complaintCount) {
		this.complaintCount = complaintCount;
	}

	public String getBlockedStatus() {
		return blockedStatus;
	}

	public void setBlockedStatus(String blockedStatus) {
		this.blockedStatus = blockedStatus;
	}

	public String getBlockedDays() {
		return blockedDays;
	}

	public void setBlockedDays(String blockedDays) {
		this.blockedDays = blockedDays;
	}

	public String getBlockedDate() {
		return BlockedDate;
	}

	public void setBlockedDate(String blockedDate) {
		BlockedDate = blockedDate;
	}

	public void setAddUserFlag(boolean addUserFlag) {
		this.addUserFlag = addUserFlag;
	}

	public boolean isAddUserFlag() {
		return addUserFlag;
	}

	/**
	 * @return the adminNotificationFlag
	 */
	public boolean isAdminNotificationFlag() {
		return adminNotificationFlag;
	}

	/**
	 * @param adminNotificationFlag the adminNotificationFlag to set
	 */
	public void setAdminNotificationFlag(boolean adminNotificationFlag) {
		this.adminNotificationFlag = adminNotificationFlag;
	}

}
