package com.musicnotes.apis.domain;

public class MnComments {
//	private String _id;
	
	private Integer cId;
	private Integer userId;
	private String listId;
	private String noteId;
	private String comments;
	private String cDate;
	private String cTime;
	private String status;
	private String editDate;
	private String editTime;
	private Integer edited;
	private String commLevel;
	
	@Override
	public String toString() {
		return "{cId:"+getcId()+", userId:"+getUserId()+", listId:"+getListId()+", noteId:"+getNoteId()+", comments:"+getComments()+", cDate:"+getcDate()+", cTime:"+getcTime()+", status:"+getStatus()+", edited:"+getEdited()+",editDate:"+getEditDate()+",editTime:"+getEditTime()+",commLevel:"+getCommLevel()+"}";
	}
	/*public String get_id() {
		return _id;
	}
	public void set_id(String id) {
		_id = id;
	}*/
	public Integer getcId() {
		return cId;
	}
	public void setcId(Integer cId) {
		this.cId = cId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getListId() {
		return listId;
	}
	public void setListId(String listId) {
		this.listId = listId;
	}
	public String getNoteId() {
		return noteId;
	}
	public void setNoteId(String noteId) {
		this.noteId = noteId;
	}
	public String getcDate() {
		return cDate;
	}
	public void setcDate(String cDate) {
		this.cDate = cDate;
	}
	public String getcTime() {
		return cTime;
	}
	public void setcTime(String cTime) {
		this.cTime = cTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getComments() {
		return comments;
	}
	public void setEditDate(String editDate) {
		this.editDate = editDate;
	}
	public String getEditDate() {
		return editDate;
	}
	public void setEditTime(String editTime) {
		this.editTime = editTime;
	}
	public String getEditTime() {
		return editTime;
	}
	public void setEdited(Integer edited) {
		this.edited = edited;
	}
	public Integer getEdited() {
		return edited;
	}
	public void setCommLevel(String commLevel) {
		this.commLevel = commLevel;
	}
	public String getCommLevel() {
		return commLevel;
	}
}
