package com.musicnotes.apis.domain;

public class MnInvoices {
	private String _id;
	private Integer invoiceId;
	private String classId;
	private String totalQty;
	private String subTotal;
	private String totalAmount;
	private String unitPrice;
	
	
	public MnInvoices()
	{
		
	}


	public String get_id() {
		return _id;
	}


	public Integer getInvoiceId() {
		return invoiceId;
	}


	public String getClassId() {
		return classId;
	}


	public String getTotalQty() {
		return totalQty;
	}


	public String getSubTotal() {
		return subTotal;
	}


	public String getTotalAmount() {
		return totalAmount;
	}


	public String getUnitPrice() {
		return unitPrice;
	}


	public void set_id(String id) {
		_id = id;
	}


	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}


	public void setClassId(String classId) {
		this.classId = classId;
	}


	public void setTotalQty(String totalQty) {
		this.totalQty = totalQty;
	}


	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}


	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}


	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}


	public MnInvoices(String id, Integer invoiceId, String classId,
			String totalQty, String subTotal, String totalAmount,
			String unitPrice) {
		super();
		_id = id;
		this.invoiceId = invoiceId;
		this.classId = classId;
		this.totalQty = totalQty;
		this.subTotal = subTotal;
		this.totalAmount = totalAmount;
		this.unitPrice = unitPrice;
	}


	@Override
	public String toString() {
		return "MnInvoices [_id=" + _id + ", classId=" + classId
				+ ", invoiceId=" + invoiceId + ", subTotal=" + subTotal
				+ ", totalAmount=" + totalAmount + ", totalQty=" + totalQty
				+ ", unitPrice=" + unitPrice + "]";
	}
	
	
	
}
