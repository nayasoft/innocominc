package com.musicnotes.apis.domain;

public class MnLog {
	private Integer logId;
	private Integer userId;
	private String logType;
	private String listId;
	private String noteId;
	private String logDescription;
	private String date;
	private String time;
	private String pageType;
	private Integer notUserId;
	private String status;
	private String pageName;
	//private Integer groupId;
	@Override
	public String toString() {
		return "{logId:"+getLogId()+", userId:"+getUserId()+",logType:"+getLogType()+", listId:"+getListId()+", noteId:"+getNoteId()+", logDescription:"+getLogDescription()+", date:"+getDate()+", time:"+getTime()+", pageType:"+getStatus()+", notUserId:"+getNotUserId()+", status:"+getStatus()+", pageName:"+getPageName()+"}";
	}
	
	public Integer getLogId() {
		return logId;
	}
	public void setLogId(Integer logId) {
		this.logId = logId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getLogType() {
		return logType;
	}
	public void setLogType(String logType) {
		this.logType = logType;
	}
	public String getListId() {
		return listId;
	}
	public void setListId(String listId) {
		this.listId = listId;
	}
	public String getNoteId() {
		return noteId;
	}
	public void setNoteId(String noteId) {
		this.noteId = noteId;
	}
	public String getLogDescription() {
		return logDescription;
	}
	public void setLogDescription(String logDescription) {
		this.logDescription = logDescription;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getPageType() {
		return pageType;
	}
	public void setPageType(String pageType) {
		this.pageType = pageType;
	}
	public Integer getNotUserId() {
		return notUserId;
	}
	public void setNotUserId(Integer notUserId) {
		this.notUserId = notUserId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public void setPageName(String pageName)
	{
		this.pageName = pageName;
	}

	public String getPageName()
	{
		return pageName;
	}

	/*public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Integer getGroupId() {
		return groupId;
	}*/
	

}
