package com.musicnotes.apis.domain;

import java.util.Date;

public class MnAttachmentDetails {
	
	private Integer attachId;
	private Integer fileId;
	private Integer listId;
	private String noteId;
	private Integer userId;
	private String aDate;
	private String aTime;
	private String status;
	private String endDate;
	private String endTime;
	private boolean shareInd;
	private boolean shareCon;
	private boolean shareGrp;
	private String fileName;
	private String currentTime;
	private String fileUploadName;
	private String uploadDate;
	private String fullPath;
	
	@Override
	public String toString() {
		return "{attachId:"+getAttachId()+", fileId:"+getFileId()+", userId:"+getUserId()+", listId:"+getListId()+", noteId:"+getNoteId()+", aDate:"+getaDate()+"," +", currentTime:"+getCurrentTime()+"," +", fileUploadName:"+getFileUploadName()+"," +", uploadDate:"+getUploadDate()+","+", fullPath:"+getFullPath()+","+ 
				"fileName:"+getFileName()+", aTime:"+getaTime()+", status:"+getStatus()+", endDate:"+getEndDate()+",endTime:"+getEndTime()+"," +
						" shareInd :"+isShareInd()+", shareCon:"+isShareCon()+", shareGrp:"+isShareGrp()+"}";
	}
	
	public Integer getFileId() {
		return fileId;
	}
	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getListId() {
		return listId;
	}
	public void setListId(Integer listId) {
		this.listId = listId;
	}
	public String getNoteId() {
		return noteId;
	}
	public void setNoteId(String noteId) {
		this.noteId = noteId;
	}
	public String getaDate() {
		return aDate;
	}
	public void setaDate(String aDate) {
		this.aDate = aDate;
	}
	public String getaTime() {
		return aTime;
	}
	public void setaTime(String aTime) {
		this.aTime = aTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public void setAttachId(Integer attachId) {
		this.attachId = attachId;
	}

	public Integer getAttachId() {
		return attachId;
	}
	public boolean isShareInd() {
		return shareInd;
	}

	public void setShareInd(boolean shareInd) {
		this.shareInd = shareInd;
	}

	public boolean isShareCon() {
		return shareCon;
	}

	public void setShareCon(boolean shareCon) {
		this.shareCon = shareCon;
	}

	public boolean isShareGrp() {
		return shareGrp;
	}

	public void setShareGrp(boolean shareGrp) {
		this.shareGrp = shareGrp;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

	

	

	public void setCurrentTime(String currentTime) {
		this.currentTime = currentTime;
	}

	public String getCurrentTime() {
		return currentTime;
	}

	public void setFileUploadName(String fileUploadName) {
		this.fileUploadName = fileUploadName;
	}

	public String getFileUploadName() {
		return fileUploadName;
	}

	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}

	public String getUploadDate() {
		return uploadDate;
	}

	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}

	public String getFullPath() {
		return fullPath;
	}

	/*public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public Date getUploadDate() {
		return uploadDate;
	}*/

}
