package com.musicnotes.apis.domain;

public class MnMailNotificationList {
	
	private Integer userId;
	private boolean noteBasisMail;
	private boolean eventBasisMail;
	private boolean memoBasisMail;
	private boolean contactBasisMail;
	private boolean crowdBasisMail;
	private boolean dueDateMail;
	
	
	
	
	
	
	public boolean isDueDateMail() {
		return dueDateMail;
	}
	public void setDueDateMail(boolean dueDateMail) {
		this.dueDateMail = dueDateMail;
	}
	public boolean isNoteBasisMail() {
		return noteBasisMail;
	}
	public void setNoteBasisMail(boolean noteBasisMail) {
		this.noteBasisMail = noteBasisMail;
	}
	public boolean isEventBasisMail() {
		return eventBasisMail;
	}
	public void setEventBasisMail(boolean eventBasisMail) {
		this.eventBasisMail = eventBasisMail;
	}
	public boolean isMemoBasisMail() {
		return memoBasisMail;
	}
	public void setMemoBasisMail(boolean memoBasisMail) {
		this.memoBasisMail = memoBasisMail;
	}
	public boolean isContactBasisMail() {
		return contactBasisMail;
	}
	public void setContactBasisMail(boolean contactBasisMail) {
		this.contactBasisMail = contactBasisMail;
	}
	public boolean isCrowdBasisMail() {
		return crowdBasisMail;
	}
	public void setCrowdBasisMail(boolean crowdBasisMail) {
		this.crowdBasisMail = crowdBasisMail;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
}
