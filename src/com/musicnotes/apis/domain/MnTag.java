package com.musicnotes.apis.domain;

public class MnTag {
	private String _id;

	private Integer tagId;
	private String tagName;
	private Integer userId;
	private String createDate;
	private String endDate;
	private String status;
	
	
	@Override
	public String toString() {
		return "MnList [_id:" + get_id() + ", tagId:" + getTagId()+ ", tagName:" + getTagName()+ ", createDate:" + getCreateDate() + ", endDate:" + getEndDate()+ ", userId:" + getUserId() + ",status:"+getStatus()+"]";
	}
	
	public String get_id() {
		return _id;
	}
	public void set_id(String id) {
		_id = id;
	}
	public Integer getTagId() {
		return tagId;
	}
	public void setTagId(Integer tagId) {
		this.tagId = tagId;
	}
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
