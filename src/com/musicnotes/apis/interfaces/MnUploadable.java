package com.musicnotes.apis.interfaces;

import com.musicnotes.apis.domain.MnUsers;

public interface MnUploadable
{
	public void saveVideoFileName(String fileName,String userId, String type, String status,String fullPath,double fileSize);
	public String getVideoFileNames(String userId);
	public String deleteVideoFileNames(String fileName,String userId);
	public void saveOtherFileName(String fileName,String userId, String type, String status,String fullPath,double fileSize);
	void saveOtherFileNameAttachWithNote(String fileName, String userId, String fileType, String status,String listId,String noteId,String fullPath,double fileSize);
	void saveOtherFileNameAttachWithNoteAudio(String fileName, String userId, String fileType, String status,String listId,String noteId,String fullPath,double fileSize);

	String getOtherFileNames(String userId);
	String deleteOtherFileNames(String fileName, String userId);
	void commonLog(String fileName, String userId,String label);
	public String deleteUploadFileNamesForNote(String params, String fileName, String userId);
	public String getVideoFileNamesForNote(String userId, String listId, String noteId);
	public String getAllFiles(String fileName,Integer userId);
	public MnUsers limiteSize(String userId);
}
