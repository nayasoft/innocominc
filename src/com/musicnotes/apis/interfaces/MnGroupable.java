package com.musicnotes.apis.interfaces;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.musicnotes.apis.domain.MnGroupDetailsDomain;
import com.musicnotes.apis.domain.MnGroupDomain;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.resources.MnGroup;

public interface MnGroupable {
	public abstract MnGroupDomain convertJsonToMnGroupDomainObj(String jsonStr);
	public abstract MnGroupDomain convertJsonToMnGroupDomainDetailsObj(String jsonStr); 
	public abstract MnGroupDetailsDomain convertJsonToMnGroupDomainDetailsObject(String jsonStr);
	String createGroup(MnGroupDomain mnGroupDomain);
	String updateGroup(MnGroupDomain mnGroupDomain);
	String deleteGroup(MnGroupDomain mnGroupDomain);
	String fetchGroupInfo(String criteria,String id);
	String updateGroupDetails(MnGroupDomain mnGroupDomain);
	
	String addGroupDetails(MnGroupDetailsDomain domain);
	String updateGroupDetails(MnGroupDetailsDomain domain);

	public String getCriteria();
	public String getGroupMembers(String params);
	public String getGroupSearch(String params);
	public String getContactSearch(String params,HttpServletRequest request);
	
	public String getGroupNamesForSearch(String params);
	public String addInvitedUser(Integer userId, String userSelectDate, String[] userMail,boolean trialCheck);
	public String getInvitedUser(String params);
	public String deletInvite(String params);
	public String updateInvitedUser(Integer userId, String newMail,String oldEmail);
	public String checkPaymentGroupForPremiumUsers(String params);
	public String createDefaultGroup(Integer userId);
	public abstract String userInformation(String params);
	public  String addFriendRequestForInviteUser(Integer userId,
			String[] userMail, String userFirstName, String userLastName);
	
}
