package com.musicnotes.apis.interfaces;

import javax.servlet.http.HttpServletRequest;

import com.musicnotes.apis.domain.MnBlockedUsers;
import com.musicnotes.apis.domain.MnMailConfiguration;

public interface MnAdminable {
	
	public String checkDefaultConfiguration();

	public String addDefaultConfiguration(String params);

	public String checkExistingUserLevel(String params);

	public MnMailConfiguration convertJsonToMailconfigurationObj(String params);

	public String createMailConfiguration(MnMailConfiguration configuration);

	public String updateMailConfiguration(MnMailConfiguration configuration);

	public String deleteMailConfiguration(String params);

	public String getMailconfiguration(String params);

	public String updateMailNotification(String params);

	public String viewDefaultConfiguration();

	public String updateDefaultConfiguration(String configId, String params);

	public String fetchUserDetailsBasedLevelRole(String param);

	public String fetchComplaintDetailsForAdmin(String param);

	public String getComplaintNote(String param);

	public String getComplaintDeleteNote(String param);

	public String fetchUserDetailsForChartView(String param);

	public String deleteComments(String param);

	public String fetchNoteComplaintDetailsForAdmin(String param);

	public String deleteAttachmentInCrowd(String param, String fileName,
			String userId);

	public String deleteAttachmentInBothNoteCrowd(String param,
			String fileName, String userId);

	public String adminStatusReport(String param);

	public String addMailTemplates(String param);

	public String fetchMailTemplates(String param, HttpServletRequest request);

	public String updateMailTemplates(String param);

	public String adminAddSecurityQuestions(String param);

	public String fetchsecurityQuestionView(String param,
			HttpServletRequest request);

	public String updateSecurityQuestions(String param);
	public String getMnUserDetails();

	public String FetchComplaintReports(String option);

	public MnBlockedUsers convertJsonToUserObj(String param);

	public String insertBlockedUser(MnBlockedUsers mnBlockingUsers);

	public String fetchBlockedUser(MnBlockedUsers mnBlockedUsers);

	public String updateBlockedUser(MnBlockedUsers mnBlockedUsers);

	public String addPrefrenceFlag(String userId,String userFlag);

	public String getUploadedFilesForAdmin();

	public String deleteUploadedFile(String fileId);

	public String convertJsonToStringForfileDetails(String param);

	public String getUploadedFilesForHelp();


	

}
