package com.musicnotes.apis.interfaces;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.musicnotes.apis.domain.MnFriends;
import com.musicnotes.apis.domain.MnUsers;

public interface MnFriendable
{
	public String addFriendRequest(MnFriends mnFriends);
	
	public String checkAlreadyFriends(MnFriends mnFriends);

	public String getFriendRequests(MnFriends mnFriends);

	public String acceptFriendRequest(MnFriends mnFriends);

	public String declineFriendRequest(MnFriends mnFriends);

	public String addToFriendsList(Integer userId, Integer requestedId);
	
	public String checkFollowers(MnFriends mnFriends);
	
	public String addFollowers(MnFriends mnFriends);
	
	public String unFollowers(MnFriends mnFriends);
	
	public MnUsers getUsers(MnFriends mnFriends);

	public MnFriends convertJsonToFriendObj(String jsonStr);

	public String convertFriendsToJsonString(List<MnFriends> mnFriends);
	
	public List<MnUsers> getFriendsList(MnUsers mnUsers);
	
	public String convertUserObjectToJsonObject(List<MnUsers> mnUsers,HttpServletRequest request);
	
	public String acceptSharedNoteByMail(String params);
		
}
