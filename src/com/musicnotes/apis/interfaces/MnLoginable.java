package com.musicnotes.apis.interfaces;

import javax.servlet.http.HttpServletRequest;

import com.musicnotes.apis.domain.MnUserLogs;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.domain.MnUsersToken;

public interface MnLoginable
{
	public String getUser(String userData, HttpServletRequest request);

	public String getLogin(String userData,String passWord, HttpServletRequest request);

	public abstract MnUsers convertJsonToUserObj(String jsonStr);

	public String unsubcribeNotification(MnUsers mnUsers);
	
	public abstract MnUserLogs convertJsonToUserDetailObj(String jsonStr);
	
	public String doUserLoginDetails(MnUserLogs mnUserLogs);
	public  String loginAuthentication(String param,HttpServletRequest request);

	public String updateLoginCheck(MnUsers user);

	public String resetVerificationCode(MnUsers user);

	MnUsers convertJsonToUserRObj(String jsonStr);
	
	public MnUsersToken convertJsonToTokenObj(String jsonStr);
	
	public String toGenerateTokens(MnUsersToken mnUsersToken);
}
