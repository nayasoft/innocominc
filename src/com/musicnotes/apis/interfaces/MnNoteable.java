package com.musicnotes.apis.interfaces;

import java.util.List;

import com.musicnotes.apis.domain.MnComments;
import com.musicnotes.apis.domain.MnComplaints;
import com.musicnotes.apis.domain.MnList;
import com.musicnotes.apis.domain.MnLog;
import com.musicnotes.apis.domain.MnRemainders;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.domain.MnVideoFile;
import com.musicnotes.apis.domain.MnVoteViewCount;

public interface MnNoteable {
	
	public abstract MnList convertJsonToMnListObj(String jsonStr);
	
	String createList(MnList mnList,String listId,String noteId);
	
	String createBook(MnList mnList);
	
	String fetchList(String params);
	
	String fetchTaggedBasedNotes(String params);
	
	String fetchDueDateBasedNotes(String params);
	
	String createNote(String mnNote,String listId,String userId);
	
	String updateDuedateForNote(String dueDate,String noteId,String listId,String userId);
	
	String updateMembersForNote(String members,String noteId,String listId, String userId);
	
	String updateMembersForBook(String params);
	
	String updateTagForNote(String params);
	
	String updateBookForNote(String params);
	
	String updateAllContactMembersForNote(String params);
	
	String deleteAllContactMembersForNote(String params);
	
	String updateAllContactMembersForBook(String params);
	
	String deleteAllContactMembersForBook(String params);
	
	String updateGroupsForNote(String groups,String noteId,String listId,String userId);
	
	String updateGroupsForBook(String params);
	
	String copyNoteToIndividual(String members,String noteId,String listId,String userId);
	
	String copyNoteToAllContact(String params);
	
	String copyNoteToGroups(String groups,String noteId,String listId, String userId);
	
	String updateList(MnList mnList);
	
	String archiveList(String params,String userId);
	
	String archiveNote(String params,String userId);
	
	String archiveAllNoteInList(String params);
	
	String moveNote(String params,String userId);
	
	String checkListMenuAccess(String params);
	
	String changeNoteAccess(String params, String userId);
	
	String copyNote(String params,String userId);
	
	String copyPublicNote(String params);
	
	String moveAllNote(String params,String userId);
	
	String copyList(String params);
	
	String updateAttachedFileForNote(String filenames,String noteId, String listId, String userId);
	
	String getAttachedFileForNote(String noteId, String listId);
	
	String getNoteDetails(String params);
	
	String getTags(String params);
	
	String createTag(String params);
	
	String updateTag(String params,String listId,String noteId);
	
	String deleteTag(String params);
	
	String updateNoteName(String listId,String noteId,String noteName,String userId, String type) ;
	
	String addNoteDrescription(String listId,String noteId,String description, String userId);
	
	String getNote(String listId,String noteId,String userId);
	
	MnComments convertJsonToMnCommentsObj(String jsonStr); 
	
	String addComments(MnComments mnComments,String listId,String noteId, String listType);
	
	String getComments(String params, String cmtsLevel, String userid);
	
	String getCommentsCountBasedOnNote(String params);
	
	String updateComments(String params,String listId,String noteId);
	
	String deleteComments(String cId,String listId,String noteId, String type);

	String deleteMembersForNote(String params, String noteId,String listId,String userId);
	
	String deleteMembersForBook(String params);
	
	String castUncastVotes(String params,String listId,String noteId,String userId);

	String deletegroupsForNote(String params, String noteId,String listId,String userId);
	
	String deletegroupsForBook(String params);
	
	String getList(String params);

	public MnLog convertJsonToMnLogObj(String jsonStr);
	
	public abstract String insertNewNoteLog(MnLog log);
	
	public MnUsers getUserDetails(Integer userId);
	
	public List<MnUsers> getUsersDetails(List<Integer> userIds);
	
	public MnVoteViewCount convertJsonToUpdateMnVoteViewCountObj(String jsonStr);
	
	public MnVoteViewCount convertJsonToMnVoteViewCountObj(String jsonStr);
	
	public abstract String insertMostViewed(MnVoteViewCount mnVoteViewCount,String userId);
	
	public abstract String updateMostViewed(MnVoteViewCount mnVoteViewCount,String userId);
	
	public abstract String fetchMostViewed(String params);
	
	public abstract String getSearchMostViewd(String params, String userId);
	
	public abstract String maxMostViewed(String params);
	
	String recentNotes(String params);
	
	String dateSearchNotes(String params);
	
	String MostVieweddateSearchNotes(String params);
	
	String MostVoteddateSearchNotes(String params);
	
	String addRemainder(MnRemainders mnRemainders);
	
	MnRemainders convertJsonToMnRwmainderObj(String params);
	
	String MostVoted(String params);
	
	String getSerachMostVoted(String params, String userId);
	
	String updateRemainders(String params,String listId,String noteId);
	
	String getRemaindersList(String params, String userId);
	
	String deleteRemainder(String rId,String listId,String noteId);
	
	String checkOwnerOfList(String listId,String noteId,String userId);
	
	String changePublicShareWarnMsgFlag(String params,String userId);
	
	public String getListBasedOnListId(String listId,String params);
	
	public  String updateScheduleNote(String params, String listId,String userId);
	
	String convertMnRamindObjectToJson(List<MnRemainders> mnRemainderList,Integer userId);
	
	String getVotesBasedNote(String listId,String noteId);

	public  String getListDetails(String params);
	
	String getFeatureNotes(String params);
	
	public String enabledisableBook(String params);
	
	public String getAttachByFileId(String params, String userId);
	
	public String getAttachedFileInNote(String listId,String noteId, String userId);
	
	String searchEngine(String searchingNote);
	
	String getSearch(String searchingNote, String userId);
	
	public String getNoteByKwywords(String params); 
	
	public String getBookNames(String params);
	
	String createNoteInDefualtList(String mnNote,String listType,String userId,String selectedBookId);
	
	public String getTagForAutoSearch(String params);
	
	public String getAllNoteFilters(String params);
	
	String getAllTags();

	public  String getSharedNotes(String params);
	
	List<MnList> fetchAllListBasedOnUserForSchedule(String userId,String listType)throws Exception;

	public MnComplaints MnComplaints(String params);

	
}
