package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnGroupDetailsDomain;

public class MnGroupDetailsConvertor implements Converter<MnGroupDetailsDomain, DBObject> {

		@Override
		public DBObject convert(MnGroupDetailsDomain mnGroupDetailsDomain) {
			DBObject dbObject = new BasicDBObject();
			dbObject.put("gdId", mnGroupDetailsDomain.getGdId());
			dbObject.put("groupId", mnGroupDetailsDomain.getGroupId());
			dbObject.put("userId", mnGroupDetailsDomain.getUserId());
			dbObject.put("startDate", mnGroupDetailsDomain.getStartDate());
			dbObject.put("endDate", mnGroupDetailsDomain.getEndDate());
			dbObject.put("status", mnGroupDetailsDomain.getStatus());
			dbObject.put("ownerId", mnGroupDetailsDomain.getOwnerId());

			return dbObject;
		}

}
