package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnNonUserSharingDetails;

public class MnNonUserSharingDetailsConverter implements Converter<MnNonUserSharingDetails, DBObject>{
	@Override
	public DBObject convert(MnNonUserSharingDetails converter) {
		
		DBObject dbObject = new BasicDBObject();
		dbObject.put("nsId", converter.getNsId());
		dbObject.put("listId", converter.getListId());
		dbObject.put("noteId", converter.getNoteId());
		dbObject.put("status", converter.getStatus());
		dbObject.put("userId", converter.getUserId());
		dbObject.put("emailId", converter.getEmailId());
		dbObject.put("sharingLevel", converter.getSharingLevel());
		dbObject.put("noteLevel", converter.getNoteLevel());
		dbObject.put("listType", converter.getListType());
		
		return dbObject;
	}
}
