package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnPublicSharedNoteCopyDetails;

public class MnPublicSharedNoteCopyDetailsConvertor implements Converter<MnPublicSharedNoteCopyDetails, DBObject>
{
	@Override
	public DBObject convert(MnPublicSharedNoteCopyDetails mnPublicSharedNoteCopyDetails) {
		DBObject dbObject = new BasicDBObject();
		dbObject.put("userId", mnPublicSharedNoteCopyDetails.getUserId());
		dbObject.put("listId", mnPublicSharedNoteCopyDetails.getListId());
		dbObject.put("noteId", mnPublicSharedNoteCopyDetails.getNoteId());
		dbObject.put("startDate", mnPublicSharedNoteCopyDetails.getStartDate());
		dbObject.put("endDate",mnPublicSharedNoteCopyDetails.getEndDate());
		dbObject.put("status", mnPublicSharedNoteCopyDetails.getStatus());
		return dbObject;
	}
}
