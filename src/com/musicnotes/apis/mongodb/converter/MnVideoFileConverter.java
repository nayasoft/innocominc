package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnVideoFile;

public class MnVideoFileConverter implements Converter<MnVideoFile, DBObject>
{

	@Override
	public DBObject convert(MnVideoFile source)
	{
		DBObject dbo = new BasicDBObject();
		   
	    dbo.put("userId", source.getUserId());
	    dbo.put("fileName", source.getFileName());
	    dbo.put("fileType", source.getFileType());
	    dbo.put("status", source.getStatus());
	    dbo.put("Id", source.getId());
	    dbo.put("fileDummyName", source.getFileDummyName());
	    dbo.put("uploadDate", source.getUploadDate());
	    dbo.put("fullPath", source.getFullPath());
	    dbo.put("fileSize", source.getFileSize());
	    return dbo;
	}

}
