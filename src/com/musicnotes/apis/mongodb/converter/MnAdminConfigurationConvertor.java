package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnAdminConfiguration;

public class MnAdminConfigurationConvertor implements Converter<MnAdminConfiguration,DBObject> {

	@Override
	public DBObject convert(MnAdminConfiguration mnAdmin) {
		DBObject dbo=new BasicDBObject();
		dbo.put("paymentAmountDays", mnAdmin.getPaymentAmountDays());
		dbo.put("startDate",mnAdmin.getStartDate());
		dbo.put("EndDate",mnAdmin.getEndDate());
		dbo.put("mailConfiguration", mnAdmin.isMailConfiguration());
		dbo.put("status",mnAdmin.getStatus());
		dbo.put("uploadLimit",mnAdmin.getUploadLimit());
		dbo.put("userId", mnAdmin.getUserId());
		dbo.put("configId", mnAdmin.getConfigId());
		return dbo;
	}
	
	

}
