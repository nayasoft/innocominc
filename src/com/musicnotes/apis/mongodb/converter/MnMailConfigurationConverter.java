package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnMailConfiguration;

public class MnMailConfigurationConverter implements Converter<MnMailConfiguration, DBObject>{

	@Override
	public DBObject convert(MnMailConfiguration source) 
	{
		DBObject dbObject = new BasicDBObject();
		dbObject.put("mailNo",source.getMailNo());
		dbObject.put("userId", source.getUserId());
		dbObject.put("userLevel", source.getUserLevel());
		dbObject.put("intervel", source.getIntervel());
		dbObject.put("startDate", source.getStartDate());
		dbObject.put("endDate", source.getEndDate());
		dbObject.put("mailSubject", source.getMailSubject());
		dbObject.put("mailMessage", source.getMailMessage());
		dbObject.put("status", source.getStatus());
		dbObject.put("amountDays",source.getAmountDays());
		dbObject.put("userSubject", source.getUserSubject());
		dbObject.put("userMessage",source.getUserMessage());
		return dbObject;
	}

	
	
}
