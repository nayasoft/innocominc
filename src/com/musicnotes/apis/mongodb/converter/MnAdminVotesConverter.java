package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnAdminVotes;

public class MnAdminVotesConverter implements Converter<MnAdminVotes, DBObject>{

	@Override
	public DBObject convert(MnAdminVotes mnAdminVotes) {
		
		DBObject dbObject = new BasicDBObject();
		dbObject.put("userId", mnAdminVotes.getUserId());
		dbObject.put("listId", mnAdminVotes.getListId());
		dbObject.put("noteId", mnAdminVotes.getNoteId());
		dbObject.put("status", mnAdminVotes.getStatus());
		dbObject.put("cDate", mnAdminVotes.getcDate());
		dbObject.put("editDate", mnAdminVotes.getEditDate());
		return dbObject;
	}

}
