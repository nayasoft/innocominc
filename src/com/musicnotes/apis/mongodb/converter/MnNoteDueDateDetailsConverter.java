package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnNoteDueDateDetails;

public class MnNoteDueDateDetailsConverter implements Converter<MnNoteDueDateDetails, DBObject>{

	@Override
	public DBObject convert(MnNoteDueDateDetails mnNoteDueDateDetails) {
		DBObject dbObject = new BasicDBObject();
		dbObject.put("userId", mnNoteDueDateDetails.getUserId());
		dbObject.put("listId", mnNoteDueDateDetails.getListId());
		dbObject.put("listType", mnNoteDueDateDetails.getListType());
		dbObject.put("noteId", mnNoteDueDateDetails.getNoteId());
		dbObject.put("dueDate", mnNoteDueDateDetails.getDueDate());
		dbObject.put("status", mnNoteDueDateDetails.getStatus());

		return dbObject;
	}

}
