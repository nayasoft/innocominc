package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnAdminFiles;

public class MnAdminFilesConverter implements Converter<MnAdminFiles, DBObject>{

	@Override
	public DBObject convert(MnAdminFiles source) 
	{
		DBObject dbo = new BasicDBObject();
		
		dbo.put("adminFileId", source.getAdminFileId());
		dbo.put("adminFileName", source.getAdminFileName());
		dbo.put("title", source.getTitle());
		dbo.put("discription", source.getDiscription());
		dbo.put("status", source.getStatus());
		dbo.put("uploadDate", source.getUploadDate());
		dbo.put("deleteDate", source.getDeleteDate());
		dbo.put("adminFileType", source.getAdminFileType());
		dbo.put("adminFilePath", source.getAdminFilePath());
		return dbo;
	}
	

}
