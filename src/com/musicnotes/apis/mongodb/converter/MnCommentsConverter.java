package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnComments;
import com.musicnotes.apis.domain.MnGroupDomain;

public class MnCommentsConverter implements Converter<MnComments, DBObject>{

	@Override
	public DBObject convert(MnComments mnComments) {
		DBObject dbObject = new BasicDBObject();
		dbObject.put("cId", mnComments.getcId());
		dbObject.put("userId", mnComments.getUserId());
		dbObject.put("listId", mnComments.getListId());
		dbObject.put("noteId", mnComments.getNoteId());
		dbObject.put("comments", mnComments.getComments());
		dbObject.put("cDate", mnComments.getcDate());
		dbObject.put("cTime",mnComments.getcTime());
		dbObject.put("status", mnComments.getStatus());
		dbObject.put("edited", mnComments.getEdited());
		dbObject.put("editDate", mnComments.getEditDate());
		dbObject.put("editTime", mnComments.getEditTime());
		dbObject.put("commLevel", mnComments.getCommLevel());
		return dbObject;
	}

}
