package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnStudents;

public class MnStudentConverter implements Converter<MnStudents, DBObject>
{

	public DBObject convert(MnStudents source)
	{
		DBObject dbObject = new BasicDBObject();
		dbObject.put("studentId", source.getStudentId());
		dbObject.put("studentName", source.getStudentName());
		dbObject.put("studentFirstName", source.getStudentFirstName());
		dbObject.put("studentLastName", source.getStudentLastName());
		dbObject.put("studetnEmailId", source.getStudentEmailId());
		dbObject.put("studentContactNumber", source.getStudentContactNumber());
		dbObject.put("emergencyContactName", source.getEmergencyContactName());
		dbObject.put("emergencyContactNumber", source.getEmergencyContactNumber());
		dbObject.put("parentName", source.getParentName());
		dbObject.put("parentContactNumber", source.getParentContactNumber());

		return dbObject;
	}

}
