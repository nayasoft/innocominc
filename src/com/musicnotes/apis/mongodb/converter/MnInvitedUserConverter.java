package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnInvitedUsers;

public class MnInvitedUserConverter implements Converter<MnInvitedUsers, DBObject>{

	@Override
	public DBObject convert(MnInvitedUsers source) 
	{
		DBObject dbObject = new BasicDBObject();
		dbObject.put("Id",source.getId());
		dbObject.put("userId", source.getUserId());
		dbObject.put("invitedMailID", source.getInvitedMailID());
		dbObject.put("date", source.getDate());
		dbObject.put("status", source.getStatus());
		dbObject.put("userCreated", source.isUserCreated());
		dbObject.put("paidSubId",source.getPaidSubId());
		dbObject.put("userSelectDate",source.getUserSelectDate());
		return dbObject;
	}

	
	
}