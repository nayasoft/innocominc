package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnTeacher;

public class MnTeacherConverter implements Converter<MnTeacher, DBObject>
{

	public DBObject convert(MnTeacher source)
	{
		DBObject dbObject = new BasicDBObject();
		dbObject.put("teacherId", source.getTeacherId());
		dbObject.put("teacherName", source.getTeacherName());
		dbObject.put("teacherFirstName", source.getTeacherFirstName());
		dbObject.put("teacherLastName", source.getTeacherLastName());
		dbObject.put("teacherEmailId", source.getTeacherEmailId());
		dbObject.put("teacherContactNumbers", source.getTeacherContactNumbers());
		dbObject.put("emergencyContactName", source.getEmergencyContactName());
		dbObject.put("emergencyContactNumber", source.getEmergencyContactNumber());

		return dbObject;
	}

}
