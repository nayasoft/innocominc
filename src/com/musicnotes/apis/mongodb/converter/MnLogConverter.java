package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnLog;

public class MnLogConverter  implements Converter<MnLog, DBObject>{
	@Override
	public DBObject convert(MnLog mnLog) {
		DBObject dbObject = new BasicDBObject();
		dbObject.put("logId", mnLog.getLogId());
		dbObject.put("userId", mnLog.getUserId());
		dbObject.put("logType", mnLog.getLogType());
		dbObject.put("listId", mnLog.getListId());
		dbObject.put("noteId", mnLog.getNoteId());
		dbObject.put("logDescription", mnLog.getLogDescription());
		dbObject.put("date", mnLog.getDate());
		dbObject.put("time",mnLog.getTime());
		dbObject.put("pageType", mnLog.getPageType());
		dbObject.put("notUserId", mnLog.getNotUserId());
		dbObject.put("status", mnLog.getStatus());
		dbObject.put("pageName", mnLog.getPageName());
		
		return dbObject;
	}
}
