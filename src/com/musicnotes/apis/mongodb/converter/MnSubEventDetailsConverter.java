package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnSubEventDetails;

public class MnSubEventDetailsConverter implements Converter<MnSubEventDetails, DBObject>{

	@Override
	public DBObject convert(MnSubEventDetails eventDetails)
	{
		DBObject dbObject = new BasicDBObject();
		dbObject.put("Id", eventDetails.getId());
		dbObject.put("eventName", eventDetails.getEventName());
		dbObject.put("startDate", eventDetails.getStartDate());
		dbObject.put("endDate", eventDetails.getEndDate());
		dbObject.put("description", eventDetails.getDescription());
		dbObject.put("alldayevent", eventDetails.getAlldayevent());
		dbObject.put("eventId",eventDetails.getEventId());
		dbObject.put("listId", eventDetails.getListId());
		dbObject.put("status", eventDetails.getStatus());
		dbObject.put("location", eventDetails.getLocation());
		dbObject.put("userId", eventDetails.getUserId());
		dbObject.put("cId", eventDetails.getC_id());
		dbObject.put("repeatEvent", eventDetails.getRepeatEvent());
		dbObject.put("mailFlag", eventDetails.isEmailFlag());
		dbObject.put("timeZone", eventDetails.getTimeZone());
		
		
		return dbObject;
	}


}
