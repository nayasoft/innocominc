package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnFriends;

public class MnFriendConvertor implements Converter<MnFriends, DBObject>
{

	@Override
	public DBObject convert(MnFriends source)
	{
		DBObject dbObject = new BasicDBObject();
		dbObject.put("userId", source.getUserId());
		dbObject.put("requestedUserId", source.getRequestedUserId());
		dbObject.put("status", source.getStatus());
		dbObject.put("requestedDate", source.getRequestedDate());
		dbObject.put("acceptedDate", source.getAcceptedDate());
		dbObject.put("declineDate", source.getDeclineDate());

		return dbObject;
	}

}
