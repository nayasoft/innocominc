package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnInvoices;

public class MnInvoiceConverter implements Converter<MnInvoices, DBObject>{

	@Override
	public DBObject convert(MnInvoices source) {
		DBObject dbo = new BasicDBObject();

		dbo.put("invoiceId", source.getInvoiceId());
		dbo.put("classId", source.getClassId());
		dbo.put("totalQty", source.getTotalQty());
		dbo.put("subTotal", source.getSubTotal());
		dbo.put("totalAmount", source.getTotalAmount());
		dbo.put("unitPrice", source.getUnitPrice());
		
    	return dbo;
	}

}
