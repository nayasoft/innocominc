package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnAdminSecurityQuestion;

public class MnAdminSecurityQuestionConverter implements Converter<MnAdminSecurityQuestion, DBObject>
{

	@Override
	public DBObject convert(MnAdminSecurityQuestion securityQuestion) 
	{
		DBObject dbo=new BasicDBObject();
		dbo.put("qId", securityQuestion.getqId());
		dbo.put("questionId",securityQuestion.getQuestionId());
		dbo.put("question",securityQuestion.getQuestion());
		dbo.put("status",securityQuestion.getStatus());
		return dbo;
	}

}
