package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnGroupDomain;

public class MnGroupConvertor implements Converter<MnGroupDomain, DBObject> {

	@Override
	public DBObject convert(MnGroupDomain mnGroupDomain) {
		DBObject dbObject = new BasicDBObject();
		dbObject.put("loginUserId", mnGroupDomain.getLoginUserId());
		dbObject.put("groupId", mnGroupDomain.getGroupId());
		dbObject.put("groupName", mnGroupDomain.getGroupName());
		dbObject.put("groupDetails", mnGroupDomain.getGroupDetails());
		dbObject.put("defaultGroup", mnGroupDomain.getDefaultGroup());
		return dbObject;
	}

}
