package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnComplaints;

public class MnComplaintsConverter implements Converter<MnComplaints, DBObject> {

	public DBObject convert(MnComplaints report)
	{
		DBObject dbo = new BasicDBObject();
		
		dbo.put("compliantId", report.getCompliantId());
		dbo.put("complaintUserId", report.getComplaintUserId());
		dbo.put("userName", report.getUserName());
		dbo.put("userFirstName", report.getUserFirstName());
		dbo.put("userLastName", report.getUserLastName());
		dbo.put("userMailId", report.getUserMailId());
		dbo.put("userComplaints", report.getUserComplaints());
		dbo.put("noteName", report.getNoteName());
		dbo.put("status", report.getStatus());
		dbo.put("date", report.getDate());
		dbo.put("listId",report.getListId());
		dbo.put("noteId",report.getNoteId());
		dbo.put("ownerId",report.getOwnerId());
		dbo.put("reportLevel",report.getReportLevel());
		dbo.put("commentId",report.getCommentId());
		dbo.put("reportPage",report.getReportPage());
		dbo.put("adminComment",report.getAdminComment());
		dbo.put("adminAction",report.getAdminAction());
		dbo.put("mailTemplateId", report.getMailTemplateId());
		return dbo;
	}
}