package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnSubscription;

public class MnSubscriptionConverter implements Converter<MnSubscription, DBObject>{

	@Override
	public DBObject convert(MnSubscription source) {
		DBObject dbo = new BasicDBObject();
		
		dbo.put("subId", source.getSubId());
		dbo.put("userId", source.getUserId());
		dbo.put("userName", source.getUserName());
		dbo.put("emailId", source.getEmailId());
		dbo.put("subDate", source.getSubDate());
		dbo.put("subStartDate", source.getSubStartDate());
		dbo.put("subEndDate", source.getSubEndDate());
		dbo.put("subDays", source.getSubDays());
		dbo.put("subType", source.getSubType());
		dbo.put("totAmount", source.getTotAmount());
		dbo.put("paidFor", source.getPaidFor());
		dbo.put("txnNo", source.getTxnNo());
		dbo.put("txnType", source.getTxnType());
		dbo.put("subscrId", source.getSubscrId());
		dbo.put("domain", source.getDomain());
		return dbo;
		
	}

}
