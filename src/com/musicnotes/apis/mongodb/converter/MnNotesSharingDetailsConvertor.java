package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnNotesSharingDetails;

public class MnNotesSharingDetailsConvertor implements Converter<MnNotesSharingDetails, DBObject> {
	@Override
	public DBObject convert(MnNotesSharingDetails mnSharingDeta) {
		DBObject dbObject = new BasicDBObject();
		dbObject.put("userId", mnSharingDeta.getUserId());
		dbObject.put("sharingUserId", mnSharingDeta.getSharingUserId());
		dbObject.put("sharingGroupId", mnSharingDeta.getSharingGroupId());
		dbObject.put("listId", mnSharingDeta.getListId());
		dbObject.put("listType", mnSharingDeta.getListType());
		dbObject.put("noteId", mnSharingDeta.getNoteId());
		dbObject.put("sharedDate", mnSharingDeta.getSharedDate());
		dbObject.put("endDate", mnSharingDeta.getEndDate());
		dbObject.put("sharingLevel", mnSharingDeta.getSharingLevel());
		dbObject.put("noteLevel", mnSharingDeta.getNoteLevel());
		dbObject.put("shareAllContactFlag", mnSharingDeta.isShareAllContactFlag());
		dbObject.put("status", mnSharingDeta.getStatus());
		dbObject.put("sharedBookId", mnSharingDeta.getSharedBookId());

		return dbObject;
	}

	
}
