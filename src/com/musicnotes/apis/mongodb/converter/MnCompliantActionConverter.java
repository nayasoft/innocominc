package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnComplaintAction;

public class MnCompliantActionConverter implements Converter<MnComplaintAction, DBObject> {

	public DBObject convert(MnComplaintAction report)
	{
		DBObject dbo = new BasicDBObject();
		dbo.put("actionId", report.getActionId());
		dbo.put("compliantId", report.getCompliantId());
		dbo.put("userId", report.getUserId());
		dbo.put("commentId", report.getCommentId());
		dbo.put("noteName", report.getNoteName());
		dbo.put("deleteFor", report.getDeleteFor());
		dbo.put("status", report.getStatus());
		dbo.put("date", report.getDate());
		dbo.put("listId",report.getListId());
		dbo.put("noteId",report.getNoteId());
		dbo.put("attachId",report.getAttachId());
		dbo.put("adminComment",report.getAdminComment());
		return dbo;
	}

	
}
