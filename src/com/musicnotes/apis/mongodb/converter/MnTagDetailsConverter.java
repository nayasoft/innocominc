package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnTagDetails;

public class MnTagDetailsConverter implements Converter<MnTagDetails, DBObject>{

	@Override
	public DBObject convert(MnTagDetails mnTagDetails) {
		DBObject dbObject = new BasicDBObject();
		dbObject.put("tagId", mnTagDetails.getTagId());
		dbObject.put("userId", mnTagDetails.getUserId());
		dbObject.put("listId", mnTagDetails.getListId());
		dbObject.put("noteId", mnTagDetails.getNoteId());
		dbObject.put("listType", mnTagDetails.getListType());
		dbObject.put("sharedDate", mnTagDetails.getTagedDate());
		dbObject.put("endDate", mnTagDetails.getEndDate());
		dbObject.put("status", mnTagDetails.getStatus());

		return dbObject;
	}

}
