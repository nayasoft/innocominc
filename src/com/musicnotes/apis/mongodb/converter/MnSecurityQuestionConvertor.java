package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnSecurityQuestions;

public class MnSecurityQuestionConvertor implements Converter<MnSecurityQuestions,DBObject> {

	@Override
	public DBObject convert(MnSecurityQuestions mnSecurity) {
		// TODO Auto-generated method stub
		DBObject dbo=new BasicDBObject();
		dbo.put("userId",mnSecurity.getUserId());
		dbo.put("QuestionId",mnSecurity.getQuestionId());
		dbo.put("answer",mnSecurity.getAnswer());
		return dbo;
	}
	

}
