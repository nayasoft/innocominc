package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnEventsSharingDetails;

public class MnEventSharingDetailsConverter implements Converter<MnEventsSharingDetails,DBObject>
{

	@Override
	public DBObject convert(MnEventsSharingDetails mnEventSharingDetails)
	{
		DBObject dbObject = new BasicDBObject();
		dbObject.put("eventSharedId", mnEventSharingDetails.getEventSharedId());
		dbObject.put("userId", mnEventSharingDetails.getUserId());
		dbObject.put("listId", mnEventSharingDetails.getListId());
		dbObject.put("noteId", mnEventSharingDetails.getNoteId());
		dbObject.put("status", mnEventSharingDetails.getStatus());
		dbObject.put("listType", mnEventSharingDetails.getListType());
		dbObject.put("sharingUserId", mnEventSharingDetails.getSharingUserId());
		dbObject.put("sharingGroupId", mnEventSharingDetails.getSharingGroupId());
		dbObject.put("sharingLevel", mnEventSharingDetails.getSharingLevel());
		dbObject.put("noteLevel", mnEventSharingDetails.getNoteLevel());
		dbObject.put("shareAllContactFlag", mnEventSharingDetails.isShareAllContactFlag());
		dbObject.put("sharingStatus", mnEventSharingDetails.getSharingStatus());
		
		return dbObject;
	}

}
