package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnEventDetails;

public class MnEventDetailConverter implements Converter<MnEventDetails, DBObject>{

	@Override
	public DBObject convert(MnEventDetails eventDetails)
	{
		DBObject dbObject = new BasicDBObject();
		dbObject.put("cId", eventDetails.getcId());
		dbObject.put("userId", eventDetails.getUserId());
		dbObject.put("eventName", eventDetails.getEventName());
		dbObject.put("startDate", eventDetails.getStartDate());
		dbObject.put("endDate", eventDetails.getEndDate());
		dbObject.put("description", eventDetails.getDescription());
		dbObject.put("alldayevent", eventDetails.getAlldayevent());
		dbObject.put("listType",eventDetails.getListType());
		dbObject.put("eventId",eventDetails.getEventId());
		dbObject.put("listId", eventDetails.getListId());
		dbObject.put("listName", eventDetails.getListName());
		dbObject.put("status", eventDetails.getStatus());
		dbObject.put("location", eventDetails.getLocation());
		dbObject.put("repeatEvent", eventDetails.getRepeatEvent());
		dbObject.put("eventStartDate", eventDetails.getEventStartDate());
		dbObject.put("eventEndDate", eventDetails.getEventEndDate());
		
		return dbObject;
	}

}
