package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnRemainders;

public class MnRemaindersConverter implements Converter<MnRemainders, DBObject>{
//cDate:"+getcDate()+",eventDate:"+getEventDate()+", status:"+getStatus()+", edited:"+getEdited()+",editDate:"+getEditDate()+"}";
	@Override
	public DBObject convert(MnRemainders mnRemainders) {
		DBObject dbObject = new BasicDBObject();
		dbObject.put("rId", mnRemainders.getrId());
		dbObject.put("userId", mnRemainders.getUserId());
		dbObject.put("listId", mnRemainders.getListId());
		dbObject.put("noteId", mnRemainders.getNoteId());
		dbObject.put("rName", mnRemainders.getrName());
		dbObject.put("eventDate", mnRemainders.getEventDate());
		dbObject.put("eventTime", mnRemainders.getEventTime());
		dbObject.put("sendMail", mnRemainders.isSendMail() );
		dbObject.put("cDate", mnRemainders.getcDate());
		dbObject.put("status", mnRemainders.getStatus());
		dbObject.put("edited", mnRemainders.getEdited());
		dbObject.put("editDate", mnRemainders.getEditDate());
		dbObject.put("inactiveRemainderUserId", mnRemainders.getInactiveRemainderUserId());
		return dbObject;
	}
}
