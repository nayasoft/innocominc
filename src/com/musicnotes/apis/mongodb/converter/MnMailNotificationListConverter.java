package com.musicnotes.apis.mongodb.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.musicnotes.apis.domain.MnMailNotificationList;;

public class MnMailNotificationListConverter implements Converter<MnMailNotificationList, DBObject> {
	public DBObject convert(MnMailNotificationList mailLists)
	{
		DBObject dbo = new BasicDBObject();

		dbo.put("userId", mailLists.getUserId());
		dbo.put("noteBasisMail", mailLists.isNoteBasisMail());
		dbo.put("eventBasisMail", mailLists.isEventBasisMail());
		dbo.put("memoBasisMail", mailLists.isMemoBasisMail());
		dbo.put("contactBasisMail", mailLists.isContactBasisMail());
		dbo.put("crowdBasisMail", mailLists.isCrowdBasisMail());
		dbo.put("dueDateMail", mailLists.isDueDateMail());
		return dbo;
	}

}