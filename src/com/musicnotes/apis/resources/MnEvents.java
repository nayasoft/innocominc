package com.musicnotes.apis.resources;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import com.musicnotes.apis.dao.interfaces.IEventDao;
import com.musicnotes.apis.domain.MnEventDetails;
import com.musicnotes.apis.domain.MnList;
import com.musicnotes.apis.domain.MnSubEventDetails;
import com.musicnotes.apis.interfaces.MnEventable;
import com.musicnotes.apis.interfaces.MnNoteable;
import com.musicnotes.apis.util.EventDao;

public class MnEvents implements MnEventable
{
	IEventDao iEventDao;
	Logger logger = Logger.getLogger(MnEvents.class);
	
	@Override
	public String createNote(String mnEvent, String listId)
	{
		if(logger.isDebugEnabled())
		logger.debug("createNote method called listId: "+listId);		
		String status = "";
		String emailFlag="";
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(mnEvent);
			MnEventDetails mnEvents=new MnEventDetails();
		
			mnEvents.setEventName((String)jsonObject.get("eventName"));
			if(mnEvents.getEventName().indexOf("`*`") != -1){
				mnEvents.setEventName(mnEvents.getEventName().replace("`*`", "\\\""));
			}	

			String sdate="";
			String edate="";
			String check=(String)jsonObject.get("allDay");
			sdate=(String) jsonObject.get("startDate");
			edate=(String) jsonObject.get("endDate");
			
			mnEvents.setStartDate(sdate);
			mnEvents.setEndDate(edate);
			mnEvents.setUserId(Integer.parseInt((String)jsonObject.get("userId")));
			mnEvents.setAlldayevent(Boolean.parseBoolean((String)jsonObject.get("allDay")));
			mnEvents.setListType((String)jsonObject.get("listType"));
			mnEvents.setEventId(Integer.parseInt((String)jsonObject.get("eventId")));
			mnEvents.setListId(Integer.parseInt(listId));
			mnEvents.setDescription((String)jsonObject.get("description"));
			if(mnEvents.getDescription().indexOf("`*`") != -1){
				mnEvents.setDescription(mnEvents.getDescription().replace("`*`", "\\\""));
			}	
			mnEvents.setListName((String)jsonObject.get("listName"));
			mnEvents.setLocation((String)jsonObject.get("location"));
			if(mnEvents.getLocation().indexOf("`*`") != -1){
				mnEvents.setLocation(mnEvents.getLocation().replace("`*`", "\\\""));
			}	
			mnEvents.setStatus("A");
			mnEvents.setRepeatEvent((String)jsonObject.get("repeatEvent"));
			mnEvents.setEventStartDate((String)jsonObject.get("eventStartDate"));
			mnEvents.setEventEndDate((String)jsonObject.get("eventEndDate"));
			emailFlag=(String)jsonObject.get("sendEmailFlag");
			
			if(mnEvents!=null)
				status = iEventDao.createEvent(mnEvents,emailFlag);
		} catch (Exception e) {
			logger.error("Exception in createNote method  : ", e);
            status="0";
		}
		if(logger.isDebugEnabled())
		logger.debug("createNote method returned successfully");
		return status;
		
	}

	public String updateNote(String mnEvent, String listId)
	{
		if(logger.isDebugEnabled())
		logger.debug("updateNote method called "+listId);	
		String status = "";
		String emailFlag="";
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(mnEvent);
			MnEventDetails mnEvents=new MnEventDetails();
			mnEvents.setEventName((String)jsonObject.get("eventName"));
			if(mnEvents.getEventName().indexOf("`*`") != -1){
				mnEvents.setEventName(mnEvents.getEventName().replace("`*`", "\\\""));
			}
			String sdate="";
			String edate="";
			String check=(String)jsonObject.get("allDay");
			sdate=(String) jsonObject.get("startDate");
			edate=(String) jsonObject.get("endDate");
			mnEvents.setStartDate(sdate);
			mnEvents.setEndDate(edate);
			mnEvents.setUserId(Integer.parseInt((String)jsonObject.get("userId")));
			mnEvents.setAlldayevent(Boolean.parseBoolean((String)jsonObject.get("allDay")));
			mnEvents.setListType((String)jsonObject.get("listType"));
			mnEvents.setEventId(Integer.parseInt((String)jsonObject.get("eventId")));
			mnEvents.setListId(Integer.parseInt(listId));
			mnEvents.setDescription((String)jsonObject.get("description"));
			if(mnEvents.getDescription().indexOf("`*`") != -1){
				mnEvents.setDescription(mnEvents.getDescription().replace("`*`", "\\\""));
			}
			mnEvents.setListName((String)jsonObject.get("listName"));
			mnEvents.setLocation((String)jsonObject.get("location"));
			if(mnEvents.getLocation().indexOf("`*`") != -1){
				mnEvents.setLocation(mnEvents.getLocation().replace("`*`", "\\\""));
			}
			mnEvents.setStatus("A");
			mnEvents.setRepeatEvent((String)jsonObject.get("repeatEvent"));
			mnEvents.setEventStartDate((String)jsonObject.get("eventStartDate"));
			mnEvents.setEventEndDate((String)jsonObject.get("eventEndDate"));
			emailFlag=(String)jsonObject.get("sendEmailFlag");
			if(mnEvents!=null)
				status = iEventDao.updateEvent(mnEvents,emailFlag);
		} catch (Exception e) {
			logger.error("Exception in updateNote method  : ", e);
            status="0";
		}
		if(logger.isDebugEnabled())
		logger.debug("updateNote method returned successfully");
		return status;
		
	}
	
	
	public IEventDao getiEventDao()
	{
		return iEventDao;
	}

	public void setiEventDao(IEventDao iEventDao)
	{
		this.iEventDao = iEventDao;
	}

	@Override
	public String fetchEvents(String params)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchEvents method called");	
		JSONObject jsonObject=null;
		String jsonString=null;
		String type="InitialLoad";
		try
		{
			jsonObject=new JSONObject(params);
			Integer userId=Integer.parseInt((String)jsonObject.get("userId"));
			List<MnEventDetails> mnevents =iEventDao.fetchEvents(userId);
			
			//fetch individual events  based on list 
			if (mnevents != null && !mnevents.isEmpty())
			{
				List<MnEventDetails> lists=iEventDao.fetchSharedEventsBasedOnList(userId,type);
				if(lists!=null && !lists.isEmpty())
					mnevents.addAll(iEventDao.fetchSharedEventsBasedOnList(userId,type));
			}else{
				mnevents = iEventDao.fetchSharedEventsBasedOnList(userId,type);
			}
			//fetch group share notes based on list
			if (mnevents != null && !mnevents.isEmpty())
			{
				List<MnEventDetails> lists=iEventDao.fetchGroupSharedEventBasedOnList(userId,type);
				if(lists!=null && !lists.isEmpty())
					mnevents.addAll(iEventDao.fetchGroupSharedEventBasedOnList(userId,type));
			}else{
				mnevents = iEventDao.fetchGroupSharedEventBasedOnList(userId,type);
			}
			
			if (mnevents != null && !mnevents.isEmpty()) {
				Collections.sort(mnevents, SENIORITY_ORDER_EVENT);
			}
			
			if(mnevents!=null && !mnevents.isEmpty())
			{
				jsonString=convertToJsonListOfList(mnevents,userId);
			}
		}
		catch (Exception e) {
			logger.error("Exception in fetchEvents method  : ", e);
			jsonString="0";
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchEvents method returned successfully");
		return jsonString;
	}
	static final Comparator<MnEventDetails> SENIORITY_ORDER_EVENT = new Comparator<MnEventDetails>() {
		public int compare(MnEventDetails e1, MnEventDetails e2) {
			Date i1=null,i2=null;
			try {
				 i1 = new Date(e1.getStartDate());
				 i2 = new Date(e2.getStartDate());
				
			} catch (Exception e) {
				
			}
			return i1.compareTo(i2);
		}
	};
	
	private String convertToJsonListOfList(List<MnEventDetails> mnEventDetails,Integer userId) {
		if(logger.isDebugEnabled())
		logger.debug("convertToJsonListOfList method called "+userId);
		StringBuffer jsonStr = new StringBuffer("[");
		try
		{
			if(mnEventDetails!=null && !mnEventDetails.isEmpty())
			{
				for (MnEventDetails list : mnEventDetails)
				{
					jsonStr.append("{\"id\" : \"" + list.getcId() + "\",");
					jsonStr.append("\"title\" : \"" + list.getEventName() + "\",");
					jsonStr.append("\"description\" : \"" + list.getDescription() + "\",");
					jsonStr.append("\"allDay\" : \"" + list.getAlldayevent() + "\",");
					jsonStr.append("\"location\" : \"" + list.getLocation() + "\",");
					// time zone conversion for timed event
					if(userId.equals(list.getUserId()) || list.getAlldayevent())
					{
						jsonStr.append("\"start\" : \"" + list.getStartDate() + "\",");
						jsonStr.append("\"end\" : \"" + list.getEndDate() + "\",");
					
					}
					else
					{
						String stDate = iEventDao.convertedZoneEvents(userId, list.getUserId(), list.getStartDate());
						jsonStr.append("\"start\" : \"" + stDate + "\",");
						String endDate = iEventDao.convertedZoneEvents(userId, list.getUserId(), list.getEndDate());
						jsonStr.append("\"end\" : \"" + endDate + "\",");
					}
					
					jsonStr.append("\"eventStartDate\" : \"" + list.getEventStartDate() + "\",");
					jsonStr.append("\"eventEndDate\" : \"" + list.getEventEndDate() + "\",");
					jsonStr.append("\"repeatEvent\" : \"" + list.getRepeatEvent() + "\",");
					jsonStr.append("\"eventId\" : \"" + list.getEventId() + "\",");
					jsonStr.append("\"listId\" : \"" + list.getListId() + "\",");
					jsonStr.append("\"ownerId\" : \"" + list.getUserId()+ "\"},");
					
				}
				jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
				jsonStr.append("]");
			}
			if(logger.isDebugEnabled())
			logger.debug("convertToJsonListOfList method returned successfully");
			return jsonStr.toString();
		}
		catch (Exception e) {
			logger.error("Exception in convertToJsonListOfList method  : ", e);
			return "";
		}
		
	}
	
	private String convertToJsonObjectOfObject(MnEventDetails list) {
		if(logger.isDebugEnabled())
		logger.debug("convertToJsonObjectOfObject method called");
		StringBuffer jsonStr = new StringBuffer("");
		try
		{
			
					jsonStr.append("{\"id\" : \"" + list.getcId() + "\",");
					jsonStr.append("\"eventName\" : \"" + list.getEventName() + "\",");
					jsonStr.append("\"startDate\" : \"" + list.getStartDate() + "\",");
					jsonStr.append("\"description\" : \"" + list.getDescription() + "\",");
					jsonStr.append("\"alldayevent\" : \"" + list.getAlldayevent() + "\",");
					jsonStr.append("\"location\" : \"" + list.getLocation() + "\",");
					jsonStr.append("\"endDate\" : \"" + list.getEndDate() + "\",");
					jsonStr.append("\"eventStartDate\" : \"" + list.getEventStartDate() + "\",");
					jsonStr.append("\"eventEndDate\" : \"" + list.getEventEndDate() + "\",");
					jsonStr.append("\"repeatEvent\" : \"" + list.getRepeatEvent() + "\",");
					jsonStr.append("\"eventId\" : \"" + list.getEventId() + "\",");
					jsonStr.append("\"listId\" : \"" + list.getListId() + "\",");
					jsonStr.append("\"ownerId\" : \"" + list.getUserId()+ "\"}");
					jsonStr.append("");
				if(logger.isDebugEnabled())
				logger.debug("convertToJsonObjectOfObject method returned successfully");
			return jsonStr.toString();
		}
		catch (Exception e) {
			logger.error("Exception in convertToJsonObjectOfObject method  : ", e);
			return "";
		}
		
	}
	
	private String convertToTodayScheduleEventsJsonListOfList(List<MnEventDetails> mnEventDetails) {
		if(logger.isDebugEnabled())
		logger.debug("convertToTodayScheduleEventsJsonListOfList method called");
		StringBuffer jsonStr = new StringBuffer("[");
		
		for (MnEventDetails list : mnEventDetails)
		{
			String[] startTiming=null;
			String[] endTiming=null;
			String startTimeString="";
			String endTimeString="";
			if(!list.getAlldayevent()){
			startTiming=list.getStartDate().split(" ");
			endTiming=list.getEndDate().split(" ");
			if(startTiming.length==2)
			{
				if(list.getDateRange()!=null && !list.getDateRange().isEmpty())
				{
					if(list.getDateRange().equalsIgnoreCase("between"))
					{
						
						if (list.getRepeatEvent() != null && list.getRepeatEvent().equals("Daily"))
						{
						startTimeString=convertTwentyFourHourToTwelveHourTime(startTiming[1]);
						endTimeString=convertTwentyFourHourToTwelveHourTime(endTiming[1]);
						}
						else if (list.getRepeatEvent() != null && list.getRepeatEvent().equals("Weekly"))
						{
							 startTimeString=convertTwentyFourHourToTwelveHourTime(startTiming[1]);
							 endTimeString=convertTwentyFourHourToTwelveHourTime(endTiming[1]);
						}
					 else if (list.getRepeatEvent() != null && list.getRepeatEvent().equals("Monthly"))
					 {
						 startTimeString=convertTwentyFourHourToTwelveHourTime(startTiming[1]);
						 endTimeString=convertTwentyFourHourToTwelveHourTime(endTiming[1]);
					 }
					else
						{
							startTimeString="12:00 AM";
							endTimeString="11:59 PM";
						}
					}
					else if(list.getDateRange().equalsIgnoreCase("startend"))
					{
						if (list.getRepeatEvent() != null && list.getRepeatEvent().equals("Daily"))
						{
						startTimeString=convertTwentyFourHourToTwelveHourTime(startTiming[1]);
						endTimeString=convertTwentyFourHourToTwelveHourTime(endTiming[1]);
						}
						else if (list.getRepeatEvent() != null && list.getRepeatEvent().equals("Weekly"))
						{
							 startTimeString=convertTwentyFourHourToTwelveHourTime(startTiming[1]);
							 endTimeString=convertTwentyFourHourToTwelveHourTime(endTiming[1]);
						}
					 else if (list.getRepeatEvent() != null && list.getRepeatEvent().equals("Monthly"))
					 {
						 startTimeString=convertTwentyFourHourToTwelveHourTime(startTiming[1]);
						 endTimeString=convertTwentyFourHourToTwelveHourTime(endTiming[1]);
					 }
					else
						{
							 startTimeString=convertTwentyFourHourToTwelveHourTime(startTiming[1]);
							 endTimeString=convertTwentyFourHourToTwelveHourTime(endTiming[1]);
						 }
						
					}
					else if(list.getDateRange().equalsIgnoreCase("start"))
					{
						if (list.getRepeatEvent() != null && list.getRepeatEvent().equals("Daily"))
						{
						startTimeString=convertTwentyFourHourToTwelveHourTime(startTiming[1]);
						endTimeString=convertTwentyFourHourToTwelveHourTime(endTiming[1]);
						}
						else if (list.getRepeatEvent() != null && list.getRepeatEvent().equals("Weekly"))
						{
							 startTimeString=convertTwentyFourHourToTwelveHourTime(startTiming[1]);
							 endTimeString=convertTwentyFourHourToTwelveHourTime(endTiming[1]);
						}
					 else if (list.getRepeatEvent() != null && list.getRepeatEvent().equals("Monthly"))
					 {
						 startTimeString=convertTwentyFourHourToTwelveHourTime(startTiming[1]);
						 endTimeString=convertTwentyFourHourToTwelveHourTime(endTiming[1]);
					 }
					else
						{
						startTimeString=convertTwentyFourHourToTwelveHourTime(startTiming[1]);
						endTimeString="11:59 PM";
						}
					}
					else if(list.getDateRange().equalsIgnoreCase("end"))
					{
						if (list.getRepeatEvent() != null && list.getRepeatEvent().equals("Daily"))
						{
						startTimeString=convertTwentyFourHourToTwelveHourTime(startTiming[1]);
						endTimeString=convertTwentyFourHourToTwelveHourTime(endTiming[1]);
						}
						else if (list.getRepeatEvent() != null && list.getRepeatEvent().equals("Weekly"))
						{
							 startTimeString=convertTwentyFourHourToTwelveHourTime(startTiming[1]);
							 endTimeString=convertTwentyFourHourToTwelveHourTime(endTiming[1]);
						}
					 else if (list.getRepeatEvent() != null && list.getRepeatEvent().equals("Monthly"))
					 {
						 startTimeString=convertTwentyFourHourToTwelveHourTime(startTiming[1]);
						 endTimeString=convertTwentyFourHourToTwelveHourTime(endTiming[1]);
					 }
					else
						{
							 startTimeString="12:00 AM";
							 endTimeString=convertTwentyFourHourToTwelveHourTime(endTiming[1]);
						 }
						
					}
					
				}
			}
			}

			if(!list.getAlldayevent())
				jsonStr.append("{\"Description\":\""+list.getEventName()+" "+startTimeString+"-"+endTimeString+"\",\"ClassName\":\""+list.getListName()+"\"},");
			else
				jsonStr.append("{\"Description\":\""+list.getEventName()+"\",\"ClassName\":\""+list.getListName()+"\"},");

		}
		
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		if(logger.isDebugEnabled())
		logger.debug("convertToTodayScheduleEventsJsonListOfList method returned successfully");
		return jsonStr.toString();
		
	}
	private String convertTwentyFourHourToTwelveHourTime(String stimings)
	{
		if(logger.isDebugEnabled())
		logger.debug("convertTwentyFourHourToTwelveHourTime method called "+stimings);
		String timee="";
		int cHour=0;
		String cMinutes="";
		try
		{
			String[] mtiming=stimings.split(":");
			if(Integer.parseInt(mtiming[0])>12)
			{
				cHour=Integer.parseInt(mtiming[0])-12;
				cMinutes=mtiming[1]+" PM";
			} else if(Integer.parseInt(mtiming[0])==12){
				cHour=Integer.parseInt(mtiming[0]);
				cMinutes=mtiming[1]+" PM";
			}else if(Integer.parseInt(mtiming[0])==0){
				cHour=Integer.parseInt(mtiming[0])+12;
				cMinutes=mtiming[1]+" AM";
			}
			else{
				cHour=Integer.parseInt(mtiming[0]);
				cMinutes=mtiming[1]+" AM";
			}
			timee=cHour+":"+cMinutes;
		}
		catch (Exception e) {
			logger.error("Exception in convertTwentyFourHourToTwelveHourTime method  : ", e);
		}
		if(logger.isDebugEnabled())
		logger.debug("convertTwentyFourHourToTwelveHourTime method returned successfully");
		return timee;
	}

	@Override
	public String fetchTodayEvents(String params)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchTodayEvents method called");
		JSONObject jsonObject=null;
		String jsonString=null;
		try
		{
			Integer userId=0;
			jsonObject=new JSONObject(params);
			String sdate=((String)jsonObject.get("date")).trim();
			String user=(String)jsonObject.get("userId");
			if(user!=null && !user.trim().equals(""))
			{
			userId=Integer.parseInt((String)jsonObject.get("userId"));
			String edate=sdate;
			sdate+=" "+"00:00";
			edate+=" "+"23:59";
			List<MnEventDetails> mnevents =iEventDao.fetchTodayEvents(sdate, userId,edate);
			if(mnevents!=null && !mnevents.isEmpty()){
			jsonString=convertToTodayScheduleEventsJsonListOfList(mnevents);
			
			}
			else
			{
				StringBuffer jsonStr = new StringBuffer("[");	
				jsonStr.append("{\"Description\":\"No events scheduled\"}");
				jsonStr.append("]");
				jsonString=jsonStr.toString();
				
			}
			
			}
		}
		catch (Exception e) {
			logger.error("Exception in fetchTodayEvents method  : ", e);
			jsonString="0";
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchTodayEvents method returned successfully");
		return jsonString;
	}

	@Override
	public String fetchCalendarSharingUsers(String params)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchCalendarSharingUsers method called");
		String status="";
		JSONObject jsonObject=null;
		Integer userId=null;
		String listType=null;
		Map<Integer,String> listName=new HashMap<Integer,String>();
		List<String> lists=new ArrayList<String>();
		String calendarColors[]={"Blue","BlueViolet","Brown","BurlyWood","CadetBlue","Chartreuse","Chocolate","Coral","CornflowerBlue","Crimson","DarkBlue","DarkCyan","DarkGoldenRod","DarkGreen","DarkMagenta","DarkOliveGreen","DarkOrange","DarkOrchid","DarkRed","DarkSlateBlue","DarkViolet","DeepPink","DeepSkyBlue","FireBrick","ForestGreen"};
		
		try
		{
			jsonObject=new JSONObject(params);
			userId=jsonObject.getInt("userId");
			listType=jsonObject.getString("listType");
			List<MnList> Lists=iEventDao.fetchCalendarSharingMembers(userId, listType);
			int i=0;
		    if(Lists!=null && !Lists.isEmpty())
		    {
		      for(MnList lst:Lists)
		      {
		    	  if(!listName.containsKey(lst.getListId()))
		    	  {
		    	  listName.put(lst.getListId(),lst.getListName());
		    	  lists.add(lst.getListId()+","+lst.getListName()+","+calendarColors[i]);
		    	  i++;
		    	  }
		      }
		    }
		    
		    StringBuffer jsonStr = new StringBuffer("[");
		    if(lists!=null && !lists.isEmpty())
		    {
		    	for(String mylist:lists)
		    	{
		    		String listDetails[]=mylist.split(",");
		    		jsonStr.append("{\"listId\":\""+listDetails[0]+"\",\"listName\":\""+listDetails[1]+"\",\"color\":\""+listDetails[2]+"\"},");
		    	}
		    	jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		    	jsonStr.append("]");
		    	status=jsonStr.toString();
		    }
		    else
		    {
		    	status="0";
		    }
		}
		catch (Exception e) {
			logger.error("Exception in fetchCalendarSharingUsers method  : ", e);
			status="0";
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchCalendarSharingUsers method returned successfully");
		return status;
	}

	@Override
	public String fetchCalendarEvents(String params)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchCalendarEvents method called");
		String status="";
		JSONObject jsonObject=null;
		Integer userId=null;
		String listType=null;
		Integer listId=null;
		try
		{
		   	jsonObject=new JSONObject(params);
		   	userId=Integer.parseInt(jsonObject.getString("userId"));
		   	listType=jsonObject.getString("listType");
	        listId=Integer.parseInt(jsonObject.getString("listId"));
	        List<MnEventDetails> eventDetails=iEventDao.fetchListsforCalendarCombo(userId, listType, listId);
	        if(eventDetails!=null && !eventDetails.isEmpty())
	        {	
	        	if(eventDetails!=null && !eventDetails.isEmpty())
	        	{
					List<Integer> eventIdss=new ArrayList<Integer>();
					for(MnEventDetails eventDetail:eventDetails){
						eventIdss.add(eventDetail.getcId());
					}
					
					List<MnSubEventDetails> subEvents=iEventDao.getSubEventDetails(eventIdss);
					status=converteSubEventDetailstoJson(subEvents,userId);
	        	}
	        }
	        else
	        {
	        	status="0";
	        }

		}
		catch (Exception e) {
			logger.error("Exception in fetchCalendarEvents method  : ", e);
			status="0";
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchCalendarEvents method returned successfully");
		return status;
	}

	@Override
	public String getEvents(String listId, String noteId,String userId) {
		if(logger.isDebugEnabled())
		logger.debug("getEvents method called "+userId+" noteId: "+noteId);
		String status = "";
		MnEventDetails events =iEventDao.getEvent(listId, noteId,userId);
		if(events!=null)
		{
			status=convertToJsonObjectOfObject(events);
		}
		if(logger.isDebugEnabled())
		logger.debug("getEvents method returned successfully");
		return status;
		
	}

	@Override
	public MnEventDetails getEventsForMail(String listId, String noteId, String userId) {
		if(logger.isDebugEnabled())
		logger.debug("getEventsForMail method called "+userId+" noteId: "+noteId);
		MnEventDetails events = null;
		try
		{
		 events =iEventDao.getEventForMail(listId, noteId);
		 
		 if(events!=null && !events.getAlldayevent())
			{
				
				String stDate = iEventDao.convertedZoneEvents(Integer.parseInt(userId), events.getUserId(), events.getStartDate());
				events.setStartDate(stDate);
				String endDate = iEventDao.convertedZoneEvents(Integer.parseInt(userId), events.getUserId(), events.getEndDate());
				events.setEndDate(endDate);
			}
		 
		}catch (Exception e) {
			logger.error("Exception in getEventsForMail method  : ", e);
		}
		if(logger.isDebugEnabled())
		logger.debug("getEventsForMail method returned successfully");
		return events;
		
	}
	
	
	@Override
	public String eventAccesDecline(String params) 
	{
		if(logger.isDebugEnabled())
		logger.debug("eventAccesDecline method called");
		String status="";
		JSONObject jsonObject=null;
		String userId=null;
		String noteId=null;
		String listId=null;
		String access=null;
		try 
		{
		jsonObject = new JSONObject(params);
		userId= (String) jsonObject.get("userId");
		noteId= (String) jsonObject.get("noteId");
		listId= (String) jsonObject.get("listId");
		access= (String) jsonObject.get("sharingStatus");
		status =iEventDao.changeAcceptDecline(userId,listId, noteId,access);
		} catch (JSONException e) {
			logger.error("Exception in eventAccesDecline method  : ", e);
		}
		if(logger.isDebugEnabled())
		logger.debug("eventAccesDecline method returned successfully");
		return status;
	}

	@Override
	public String fetchParticularEvents(Integer listId, Integer eventId, Integer userId)
	{
		if(logger.isDebugEnabled())
		logger.debug("fetchParticularEvents method called "+userId);
		String status="";
		List<MnEventDetails> events=new ArrayList<MnEventDetails>();
		try
		{
			events=iEventDao.fetchParticularEvent(listId, eventId, userId);
			status=convertToJsonListOfList(events,userId);
		}
		catch (Exception e) {
			logger.error("Exception in fetchParticularEvents method  : ", e);
			status="0";
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchParticularEvents method returned successfully");
		return status;
	}

	public String getEventsFromList(String params){
		if(logger.isDebugEnabled())
		logger.debug("getEventsFromList method called");
		List<MnEventDetails> events= null; 
		try{
			JSONObject jsonObject = new JSONObject(params);
			Integer listId = Integer.parseInt(((String)jsonObject.get("listId")).trim());
			String eventIsWithOutSplit =(String) jsonObject.get("eventIds");
			Integer userId =  Integer.parseInt(((String)jsonObject.get("userId")).trim());
			String[] eventArray = eventIsWithOutSplit.split(",");
			if(eventArray.length>0 && !eventArray[0].trim().equals("")){
				List<Integer> eventListIds = new ArrayList<Integer>();
				for(String str:eventArray){
					eventListIds.add(Integer.parseInt(str.trim()));
				}
				events =iEventDao.getEventsFromList(listId,eventListIds,userId);
				if(events!= null && !events.isEmpty()){
					if(events!=null && !events.isEmpty())
					{
						List<Integer> eventIds=new ArrayList<Integer>();
						for(MnEventDetails eventDetails:events){
							eventIds.add(eventDetails.getcId());
						}
						
						List<MnSubEventDetails> subEvents=iEventDao.getSubEventDetails(eventIds);
						params=converteSubEventDetailstoJson(subEvents,userId);
						
					return params;
					}
				}
				return "0";
			}
		}catch (Exception e) {
			logger.error("Exception in getEventsFromList method  : ", e);
		}
		if(logger.isDebugEnabled())
		logger.debug("getEventsFromList method returned successfully");
		return "0";
		
	}

	@Override
	public String fetchSchdulingEvents(String params) {
		if(logger.isDebugEnabled())
		logger.debug("fetchSchdulingEvents method called");
		JSONObject jsonObject=null;
		String jsonString=null;
		String type="loading";
		try
		{
			jsonObject=new JSONObject(params);
			//String sdate=(String)jsonObject.get("date");
			Integer userId=Integer.parseInt((String)jsonObject.get("userId"));
			List<MnEventDetails> mnevents =iEventDao.fetchEvents( userId);
			
			//fetch individual events  based on list 
			if (mnevents != null && !mnevents.isEmpty())
			{
				List<MnEventDetails> lists=iEventDao.fetchSharedEventsBasedOnList(userId,type);
				if(lists!=null && !lists.isEmpty())
					mnevents.addAll(iEventDao.fetchSharedEventsBasedOnList(userId,type));
			}else{
				mnevents = iEventDao.fetchSharedEventsBasedOnList(userId,type);
			}
			//fetch group share notes based on list
			if (mnevents != null && !mnevents.isEmpty())
			{
				List<MnEventDetails> lists=iEventDao.fetchGroupSharedEventBasedOnList(userId,type);
				if(lists!=null && !lists.isEmpty())
					mnevents.addAll(iEventDao.fetchGroupSharedEventBasedOnList(userId,type));
			}else{
				mnevents = iEventDao.fetchGroupSharedEventBasedOnList(userId,type);
			}
			
			if (mnevents != null && !mnevents.isEmpty()) {
				Collections.sort(mnevents, SENIORITY_ORDER_EVENT);
			}
			
			if(mnevents!=null && !mnevents.isEmpty())
			{
				List<MnEventDetails> detailss=new ArrayList<MnEventDetails>();
				List<Integer> eventIds=new ArrayList<Integer>();
				for(MnEventDetails events:mnevents){
					eventIds.add(events.getcId());
				}
					List<MnSubEventDetails> subEvents=iEventDao.getSubEventDetails(eventIds);
					jsonString=converteSubEventDetailstoJson(subEvents,userId);
					
				
			}
		}
		catch (Exception e) {
			logger.error("Exception in fetchSchdulingEvents method  : ", e);
			jsonString="0";
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchSchdulingEvents method returned successfully");
		return jsonString;
	}
	

@Override
public String updateMnSubEvent(String params) {
	if(logger.isDebugEnabled())
	logger.debug("updateMnSubEvent method called");
	String jsonString=null;
	try
	{
		
		jsonString=iEventDao.updateMnSubEvent(params);
	}
	catch (Exception e) {
		logger.error("Exception in updateMnSubEvent method  : ", e);
		jsonString="0";
	}
	if(logger.isDebugEnabled())
	logger.debug("updateMnSubEvent method returned successfully");
	return jsonString;
}

@Override
public String deleteMnSubEvent(String params) {
	if(logger.isDebugEnabled())
	logger.debug("deleteMnSubEvent method called");
	String jsonString=null;
	try
	{
		jsonString=iEventDao.deleteMnSubEvent(params);
	}
	catch (Exception e) {
		logger.error("Exception in deleteMnSubEvent method  : ", e);
		jsonString="0";
	}
	if(logger.isDebugEnabled())
	logger.debug("deleteMnSubEvent method returned successfully");
	return jsonString;
}

private String converteSubEventDetailstoJson(List<MnSubEventDetails> subEvents,Integer userId){
	if(logger.isDebugEnabled())
	logger.debug("converteSubEventDetailstoJson method called "+userId);
	StringBuffer jsonStr = new StringBuffer("[");
	try
	{
		if(subEvents!=null && !subEvents.isEmpty())
		{
			for (MnSubEventDetails list : subEvents)
			{
				jsonStr.append("{\"subEventId\" : \"" + list.getId() + "\",");
				jsonStr.append("\"title\" : \"" + list.getEventName() + "\",");
				jsonStr.append("\"description\" : \"" + list.getDescription() + "\",");
				jsonStr.append("\"allDay\" : \"" + list.getAlldayevent() + "\",");
				jsonStr.append("\"location\" : \"" + list.getLocation() + "\",");
				
				// time zone conversion for timed event
				if(userId.equals(list.getUserId()) || list.getAlldayevent())
				{
					jsonStr.append("\"start\" : \"" + list.getStartDate() + "\",");
					jsonStr.append("\"end\" : \"" + list.getEndDate() + "\",");
				}
				else
				{
					String stDate = iEventDao.convertedZoneEvents(userId, list.getUserId(), list.getStartDate());
					jsonStr.append("\"start\" : \"" + stDate + "\",");
					String endDate = iEventDao.convertedZoneEvents(userId, list.getUserId(), list.getEndDate());
					jsonStr.append("\"end\" : \"" + endDate + "\",");
				}
				
				jsonStr.append("\"eventStartDate\" : \"" + list.getStartDate() + "\",");
				jsonStr.append("\"eventEndDate\" : \"" + list.getEventEndDate() + "\",");
				jsonStr.append("\"eventId\" : \"" + list.getEventId() + "\",");
				jsonStr.append("\"listId\" : \"" + list.getListId() + "\",");
				jsonStr.append("\"repeatType\" : \"" + list.getRepeatEvent() + "\",");
				jsonStr.append("\"ownerId\" : \"" + list.getUserId()+ "\"},");
			}
			jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
			jsonStr.append("]");
		}
		if(logger.isDebugEnabled())
		logger.debug("converteSubEventDetailstoJson method returned successfully");
		return jsonStr.toString();
	}
	catch (Exception e) {
		logger.error("Exception in converteSubEventDetailstoJson method  : ", e);
		return "";
	}
	
}

@Override
public String fetchSubEvents(String params)
{
	if(logger.isDebugEnabled())
	logger.debug("fetchSubEvents method called");
	String jsonString=null;
	JSONObject jsonObject=null;
	try{
			jsonObject=new JSONObject(params);
			Integer subEventId=Integer.parseInt((String)jsonObject.get("subEventId"));
			Integer eventId=Integer.parseInt((String)jsonObject.get("eventId"));
			Integer listId=Integer.parseInt((String)jsonObject.get("listId"));
			MnSubEventDetails subEvents=iEventDao.fetchSubEvents(subEventId,listId,eventId);
			jsonString=getSubEventDetailsToJson(subEvents);
			
	}catch(Exception e){
		logger.error("Exception in fetchSubEvents method  : ", e);
	}
	if(logger.isDebugEnabled())
	logger.debug("fetchSubEvents method returned successfully");
	return jsonString;
}

private String getSubEventDetailsToJson(MnSubEventDetails details){
	if(logger.isDebugEnabled())
	logger.debug("getSubEventDetailsToJson method called");
	StringBuffer jsonStr = new StringBuffer("");
	try
	{
		if(details!=null)
		{
				jsonStr.append("{\"subEventId\" : \"" + details.getId() + "\",");
				jsonStr.append("\"title\" : \"" + details.getEventName() + "\",");
				jsonStr.append("\"start\" : \"" + details.getStartDate() + "\",");
				jsonStr.append("\"description\" : \"" + details.getDescription() + "\",");
				jsonStr.append("\"allDay\" : \"" + details.getAlldayevent() + "\",");
				jsonStr.append("\"location\" : \"" + details.getLocation() + "\",");
				jsonStr.append("\"end\" : \"" + details.getEndDate() + "\",");
				jsonStr.append("\"eventStartDate\" : \"" + details.getStartDate() + "\",");
				jsonStr.append("\"eventEndDate\" : \"" + details.getEndDate() + "\",");
				jsonStr.append("\"eventId\" : \"" + details.getEventId() + "\",");
				jsonStr.append("\"listId\" : \"" + details.getListId() + "\",");
				jsonStr.append("\"ownerId\" : \"" + details.getUserId()+ "\"}");
			}
		
	}
	catch (Exception e) {
		logger.error("Exception in getSubEventDetailsToJson method  : ", e);
	}
	if(logger.isDebugEnabled())
	logger.debug("getSubEventDetailsToJson method returned successfully");
	return jsonStr.toString();

}
	public String getListBasedOnListIdForSchedule(String listId,String params){
		if(logger.isDebugEnabled())
		logger.debug("getListBasedOnListIdForSchedule method called listId: "+listId);
		String userId="";
		String listType="";
		String multiFilterBaseTagId="";
		String multiFilterBaseNoteName="";
		List<MnList> searchResults = null;
		String jsonString = "";
		
		try{
			if (params != null && !params.equals("")) {
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				userId=(String) jsonObject.get("userId");
				listType=(String) jsonObject.get("listType");
				multiFilterBaseTagId=(String) jsonObject.get("tagId");// no need for schdule
				multiFilterBaseNoteName=(String) jsonObject.get("noteName");
				
				searchResults = iEventDao.getListSeachByListId(userId, listId);
				if(multiFilterBaseNoteName!=null && !multiFilterBaseNoteName.isEmpty()){
					if(searchResults!= null && !searchResults.isEmpty())
						searchResults = iEventDao.byKewordConvertMnListTo(searchResults, multiFilterBaseNoteName);
				}
				if(searchResults!= null && !searchResults.isEmpty()){
					searchResults = convertMnListToEventDoa(searchResults,Integer.parseInt(userId));
					if(searchResults!= null && !searchResults.isEmpty()){
						jsonString = convertToJsonMnList (searchResults);
					}
				}
			}
		}catch(Exception e){
			logger.error("Exception in getListBasedOnListIdForSchedule method  : ", e);
		}
		if(logger.isDebugEnabled())
		logger.debug("getListBasedOnListIdForSchedule method returned successfully");
		return jsonString;
	}
	
	public String getNoteByKwywordsForSchedule(String params, MnNoteable mnNoteable){
		if(logger.isDebugEnabled())
		logger.debug("getNoteByKwywordsForSchedule method called");
		String userId="";
		String listType="";
		String keyword= "";
		String multiFilterBaseBookId="";
		String multiFilterBaseTagId="";
		List<MnList> searchResults = null;
		String jsonString = "";
		try{
			if (params != null && !params.equals("")) {
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				userId=(String) jsonObject.get("userId");
				listType=(String) jsonObject.get("listType");
				keyword = (String) jsonObject.get("keyword");
				multiFilterBaseBookId = (String) jsonObject.get("listId");
				multiFilterBaseTagId = (String) jsonObject.get("tagId");// no need for schdule
				
				if(multiFilterBaseBookId!=null && multiFilterBaseBookId.isEmpty()){
					//book id is empty
					searchResults =  mnNoteable.fetchAllListBasedOnUserForSchedule(userId, listType);
				}else{
					searchResults = iEventDao.getListSeachByListId(userId, multiFilterBaseBookId);
				}
				if(searchResults!= null && !searchResults.isEmpty()){
					searchResults = iEventDao.byKewordConvertMnListTo(searchResults, keyword);
				}
				if(multiFilterBaseBookId!=null && multiFilterBaseBookId.isEmpty()){
					List<MnEventDetails> eventDetailsList = new ArrayList<MnEventDetails>();
					for(MnList list :searchResults){
						//change note to event when handling  
						 for(String str :list.getMnNotesDetails()){
							JSONObject object = new JSONObject(str);
							eventDetailsList.addAll(iEventDao.fetchParticularEvent(list.getListId(), Integer.parseInt(object.getString("eventId").trim()), Integer.parseInt(userId)));
						}
					}
					if(eventDetailsList!= null && !eventDetailsList.isEmpty()){
						Collections.sort(eventDetailsList, SENIORITY_ORDER_EVENT_DETAILS);
						jsonString = convertToJsonListOfList(eventDetailsList, Integer.parseInt(userId));
					}
				}else{
					searchResults = convertMnListToEventDoa(searchResults,Integer.parseInt(userId));
					if(searchResults!= null && !searchResults.isEmpty()){
						jsonString = convertToJsonMnList (searchResults);
					}
				}
			}
		}catch (Exception e) {
			logger.error("Exception in getNoteByKwywordsForSchedule method  : ", e);
		}	
		if(logger.isDebugEnabled())
		logger.debug("getNoteByKwywordsForSchedule method returned successfully");
		return jsonString;
	}
	
	private String convertToJsonMnList(List<MnList> mnListList) {
		if(logger.isDebugEnabled())
		logger.debug("convertToJsonMnList method called");
		StringBuffer jsonStr = new StringBuffer("[");
		try{
			for (MnList list : mnListList)
			{
				jsonStr.append("{\"_id\" : \"" + list.get_id() + "\",");
				jsonStr.append("\"listId\" : \"" + list.getListId() + "\",");
				jsonStr.append("\"listName\" : \"" + list.getListName() + "\",");
				jsonStr.append("\"listType\" : \"" + list.getListType() + "\",");
				jsonStr.append("\"noteAccess\" : \"" + list.getNoteAccess() + "\",");
				jsonStr.append("\"defaultNote\" : " + list.isDefaultNote() + ",");
				jsonStr.append("\"enableDisableFlag\" : " + list.isEnableDisableFlag() + ",");
				jsonStr.append("\"userId\" : \"" + list.getUserId() + "\",");
				jsonStr.append("\"status\" : \"" + list.getStatus() + "\",");
				jsonStr.append("\"sharedIndividuals\" : \"" + list.getSharedIndividuals().toString() + "\",");
				jsonStr.append("\"sharedGroups\" : \"" + list.getSharedGroups().toString() + "\",");
				jsonStr.append("\"shareAllContactFlag\" : " + list.isShareAllContactFlag() + ",");
				if(list.getMnNotesDetailsList()== null || list.getMnNotesDetailsList().isEmpty()){
					jsonStr.append("\"mnNotesDetails\" : " + list.getMnNotesDetails().toString() + "},");
				}else{
					jsonStr.append("\"mnNotesDetails\" : " + list.getMnNotesDetailsList().toString() + "},");
				}
			}
			jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
			jsonStr.append("]");
		} catch (Exception e) {
			logger.error("Exception in convertToJsonMnList method  : ", e);
		}
		if(logger.isDebugEnabled())
		logger.debug("convertToJsonMnList method returned successfully");
		return jsonStr.toString();
	}
	public List<MnList> convertMnListToEventDoa(List<MnList> mnListList, Integer userId){
		if(logger.isDebugEnabled())
		logger.debug("convertMnListToEventDoa method called "+userId);
		List<MnList> mnLists=new ArrayList<MnList>();
		try{
			List<EventDao> eventDaoList = new ArrayList<EventDao>();
			if(mnListList.size() ==1){
				for(MnList mnList:mnListList){
					Integer ownerId=mnList.getUserId();
					
					if(mnList.getMnNotesDetails()!= null && !mnList.getMnNotesDetails().isEmpty()){
						for(String str:mnList.getMnNotesDetails()){
							JSONObject jsonObject = new JSONObject(str);
							EventDao dao = new EventDao();
							dao.setEventName(jsonObject.getString("eventName"));
							dao.setEventMembers(jsonObject.getString("eventMembers"));               
							dao.setEventGroups(jsonObject.getString("eventGroups"));                  
							dao.setEventSharedAllContact(Integer.parseInt(jsonObject.getString("eventSharedAllContact").trim()));          
							dao.setEventSharedAllContactMembers(jsonObject.getString("eventSharedAllContactMembers").trim()); 
							dao.setStatus(jsonObject.getString("status"));                     
							dao.setOwnerEventStatus(jsonObject.getString("ownerEventStatus")); 
							dao.setStartDate(jsonObject.getString("startDate")); 
							dao.setEndDate(jsonObject.getString("endDate"));
							dao.setEventId(Integer.parseInt(jsonObject.getString("eventId").trim()));               
							dao.setDescription(jsonObject.getString("description"));       
							dao.setLocation(jsonObject.getString("location"));
							eventDaoList.add(dao);
						}
					}else{
						mnLists.add(mnList);
					}
					if(eventDaoList!=null && !eventDaoList.isEmpty()){
						Collections.sort(eventDaoList,SENIORITY_ORDER_EVENT_VO);
						mnList.setMnNotesDetails(new HashSet<String>(0));
						for(EventDao dao :eventDaoList)
						{
							//change timezone for events:
							String oldSt=dao.getStartDate();
							String oldEnd=dao.getEndDate();
							if(!(userId.equals(ownerId) || oldSt.indexOf(":")==-1))
							{
								
								String stDate = iEventDao.convertedZoneEvents(userId, ownerId, oldSt);
								dao.setStartDate(stDate);
								String endDate = iEventDao.convertedZoneEvents(userId, ownerId, oldEnd);
								dao.setEndDate(endDate);
							}
							
							mnList.getMnNotesDetailsList().add(dao.toString());
						}
						mnLists.add(mnList);
					}
				}
			}
			
		}catch (Exception e) {
			logger.error("Exception in convertMnListToEventDoa method  : ", e);
		} 
		if(logger.isDebugEnabled())
		logger.debug("convertMnListToEventDoa method returned successfully");
		return mnLists;
	}
	static final Comparator<EventDao> SENIORITY_ORDER_EVENT_VO = new Comparator<EventDao>() {
		@SuppressWarnings("deprecation")
		public int compare(EventDao e1, EventDao e2) {
			try {
				Date i1 = new Date(e1.getStartDate());
				Date i2 = new Date(e2.getStartDate());
				return i1.compareTo(i2);
			} catch (Exception e) {
				Date i1 = new Date(e1.getStartDate());
				Date i2 = new Date(e2.getStartDate());
				return i1.compareTo(i2);
				
			}
		}
	};
	static final Comparator<MnEventDetails> SENIORITY_ORDER_EVENT_DETAILS = new Comparator<MnEventDetails>() {
		@SuppressWarnings("deprecation")
		public int compare(MnEventDetails e1, MnEventDetails e2) {
			Date i1=null,i2=null;
			try {
				 i1 = new Date(e1.getStartDate());
				 i2 = new Date(e2.getStartDate());
				 return i1.compareTo(i2);
				
			} catch (Exception e) {
				i1 = new Date(e1.getStartDate());
				i2 = new Date(e2.getStartDate());
				return i1.compareTo(i2);
			}
			
		}
	};
	@Override
	public String updateAllMnSubEvent(String params) {
		if(logger.isDebugEnabled())
		logger.debug("updateAllMnSubEvent method called");
		String jsonString=null;
		try
		{
			jsonString=iEventDao.updateAllMnSubEvent(params);
		}
		catch (Exception e) {
			logger.error("Exception in updateAllMnSubEvent method  : ", e);
			jsonString="0";
		}
		if(logger.isDebugEnabled())
		logger.debug("updateAllMnSubEvent method returned successfully");
		return jsonString;
	}

	@Override
	public String getSubEvents(String params) {
		if(logger.isDebugEnabled())
		logger.debug("getSubEvents method called");
		String jsonString=null;
		JSONObject jsonObject=null;
		try{
				jsonObject=new JSONObject(params);
				Integer subEventId=Integer.parseInt((String)jsonObject.get("subEventId"));
				Integer eventId=Integer.parseInt((String)jsonObject.get("eventId"));
				Integer listId=Integer.parseInt((String)jsonObject.get("listId"));
				MnSubEventDetails details=iEventDao.getSubEvents(params);
				if(details!=null)
				{
					jsonString=getSubEventDetailsToJson(details);
				}
				
				
		}catch(Exception e){
			logger.error("Exception in getSubEvents method  : ", e);
		}
		if(logger.isDebugEnabled())
		logger.debug("getSubEvents method returned successfully");
		return jsonString;
}

	@Override
	public String updateSubFollowingEvents(String params) {
		if(logger.isDebugEnabled())
		logger.debug("updateSubFollowingEvents method called");
		String jsonString=null;
		try
		{
			jsonString=iEventDao.updateSubFollowingEvents(params);
		}
		catch (Exception e) {
			logger.error("Exception in updateSubFollowingEvents method  : ", e);
			jsonString="0";
		}
		if(logger.isDebugEnabled())
		logger.debug("updateSubFollowingEvents method returned successfully");
		return jsonString;
	}

	@Override
	public String updateMnSingleEvent(String params) {
		if(logger.isDebugEnabled())
		logger.debug("updateMnSingleEvent method called");
		String jsonString=null;
		try
		{
			jsonString=iEventDao.updateMnSingleEvent(params);
		}
		catch (Exception e) {
			logger.error("Exception in updateMnSingleEvent method  : ", e);
			jsonString="0";
		}
		if(logger.isDebugEnabled())
		logger.debug("updateMnSingleEvent method returned successfully");
		return jsonString;
}

	@Override
	public String fetchParticularEvents(String params) {
		if(logger.isDebugEnabled())
		logger.debug("fetchParticularEvents method called");
		String jsonString=null;
		try
		{
			jsonString=iEventDao.fetchParticularEvent(params);
		}
		catch (Exception e) {
			logger.error("Exception in fetchParticularEvents method  : ", e);
			jsonString="0";
		}
		if(logger.isDebugEnabled())
		logger.debug("fetchParticularEvents method returned successfully");
		return jsonString;
}

	@Override
	public String getEventMailValues() {
		
		iEventDao.getEventMailValues();
		
		return "Mail has been sent to selected Events";
	}
	

}