package com.musicnotes.apis.resources;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.data.mongodb.core.query.Order;
import org.springframework.data.mongodb.core.query.Query;

import com.musicnotes.apis.dao.impl.BaseDaoImpl;
import com.musicnotes.apis.dao.interfaces.INoteDao;
import com.musicnotes.apis.dao.interfaces.IUserDao;
import com.musicnotes.apis.domain.MnAdminConfiguration;
import com.musicnotes.apis.domain.MnAttachmentDetails;
import com.musicnotes.apis.domain.MnFeedbacks;
import com.musicnotes.apis.domain.MnComments;
import com.musicnotes.apis.domain.MnFriends;
import com.musicnotes.apis.domain.MnList;
import com.musicnotes.apis.domain.MnMailConfiguration;
import com.musicnotes.apis.domain.MnMailNotificationList;
import com.musicnotes.apis.domain.MnNoteDetails;
import com.musicnotes.apis.domain.MnSecurityQuestions;
import com.musicnotes.apis.domain.MnTag;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.interfaces.MnUserable;
import com.musicnotes.apis.util.JavaMessages;
import com.musicnotes.apis.util.PasswordGenerator;
import com.musicnotes.apis.util.RandomPassGenerator;

public class MnUser extends BaseDaoImpl implements MnUserable 
{
		
	IUserDao userDao;
	INoteDao iNoteDao,iNoteDao1;
	String comment3="Create notebooks to group notes together in a way that makes sense to you: <br> " +
			"1. You can create notebooks from within a note or from by clicking the [Add new notebook] button. <br> " +
			"2. Name the notebook. 3. Add notes to as many notebooks as you'd like. <br> ";
	String comment2=" If you want to create a note around a particular lesson you might do the following: <br> " +
			"1. Add the audio or video recording of your lesson to the note so you can always reference it in the future. <br>  " +
			"2. Add a description of what was covered during the lesson to make it easy to know what you did during each lesson. <br> " +
			"3. Add your practice list to the note - add comments about what you struggled with and what you got to keep track of your progress. <br> " +
			"4. Add any music you went over during that class such as sheet music, images of something from a notebook, fingerings on an instrument, mp3s or any other music resource from the lesson. <br> " +
			"5. Maybe there are some links, songs or videos that relate that you want to reference. Add those in the comments section. <br> " +
			"6. Add tags to the note with the most important topics covered - this will help you easily find notes related to a particular topic in the future. <br> " +
			"7. Add reminders if you need practice reminders.  <br> ";
	
	String comment1=" Start by creating a new note: <br> " +
			"1. Click the [+ New Note] button. <br> " +
			"2. Give the note a title (you can always edit this in the future). <br> " +
			"3. Once inside the note, you can describe it, tag it, add comments, links, videos, attach files, set due dates and reminders. Then you can share the note or a copy of it with individuals, groups or all of your contacts. <br> ";
	DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	DateFormat datetimeFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	Date date = new Date();
	String greetingnotedesc="Musicnote lets you create smarter notes and notebooks around everything you do on your musical journey. We have split up your notes into 3 types to help keep you organized: Music notes (for all things music), Scheduling notes (to keep your music schedule organized), and Memos (for correspondence and general communication with your music contacts).";
	String greetingnotecomment="";
	static Logger logger = Logger.getLogger(MnUser.class);

	@Override
	public MnUsers convertJsonToUserObj(String jsonStr)
	{
		if(logger.isDebugEnabled())
		logger.debug("while creating a convertJsonToUserObj method : ");
		MnUsers user = new MnUsers();
		String question = null;
		Integer userId = null;
		try
		{
			if (jsonStr != null && !jsonStr.equals(""))
			{
				jsonStr = jsonStr.replace("{", "");
				jsonStr = jsonStr.replace("}", "");
				jsonStr = jsonStr.replace("\"", "");
				String params[] = jsonStr.split(",");
				
				for (String param : params)
				{
					
					String[] values = param.split(":");
					
					if (param!=null && param.contains("userFirstName"))
						user.setUserFirstName(values[1]);
					else if (param!=null && param.contains("userLastName"))
						user.setUserLastName(values[1]);
					else if (param!=null && param.contains("userName"))
						user.setDisplayUserName(values[1]);
					else if (param!=null && param.contains("userId")){
						user.setUserId(Integer.parseInt(values[1]));
						userId=Integer.parseInt(values[1]);
					}	
					else if (param!=null && param.contains("emailId"))
						user.setEmailId(values[1].toLowerCase());
					else if (param!=null && param.contains("securityId"))
						user.setSecurityId(values[1]);
					else if (param!=null && param.contains("password"))
						user.setPasswordText(values[1]);
					else if (param!=null && param.contains("userRole"))
						user.setUserRole(values[1]);
					else if (param!=null && param.contains("contactNumber"))
						user.setContactNumber(values[1]);
					else if (param!=null && param.contains("notificationFlag"))
						user.setNotificationFlag("yes");
					else if (param!=null && param.contains("termscheck"))
						user.setTermscheck("true");
					else if (param!=null && param.contains("noteCreateBasedOn"))
						user.setNoteCreateBasedOn(values[1]);
					else if (param!=null && param.contains("publicShareWarnMsgFlag"))
						user.setPublicShareWarnMsgFlag(Boolean.parseBoolean(values[1]));
					else if (param.contains("instrument"))
						user.setInstrument(values[1]);					
					else if (param.contains("skillLevel"))
						user.setSkillLevel(values[1]);
					else if (param.contains("favoriteMusic"))
						user.setFavoriteMusic(values[1]);
					else if (param.contains("offSet"))
						user.setTimeZone(values[1]);
					
				}
				if(user.getDisplayUserName()!=null && !user.getDisplayUserName().equals("")){
					user.setUserName(user.getDisplayUserName().toLowerCase());
				}
				// commented by ramaraj password is generated based on userId
				/*
				if(user.getUserRole().contains("Music Student"))
				{
					if(user.getEmailId()!=null && !user.getEmailId().equals(""))
							user.setNotificationFlag("no");
					
		    	user.setPassword(PasswordGenerator.encoder(user.getUserName(),user.getPasswordText()));
				}else{
					   user.setPassword(PasswordGenerator.encoder(user.getEmailId(),user.getPasswordText()));
		   		}*/
				
				 Date date=new Date(); 
				SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
				sdf.format(date);
				user.setStartDate(sdf.format(date));
				user.setStatus("A");
				user.setRequestPending(false);
				user.setUploadedSize(0.0);
				user.setLimitedSize(5120.00);
			}
		}
		catch (Exception e)
		{
			logger.error("Exception while converting json to user object!", e);

		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for convertJsonToUserObj method : ");
		return user;
	}
	public MnFeedbacks feedbackDetails(String param) {
		if(logger.isDebugEnabled())
		logger.debug("while user feedback for userFeedback method:");
		MnFeedbacks user = new MnFeedbacks();
		String status = "";
		try
		{
			if (param != null && !param.equals(""))
			{
				JSONObject Jobj=new JSONObject(param);
				user.setUserId(Integer.parseInt(Jobj.getString("userId")));
				user.setUserFirstName((String) Jobj.get("userFirstName"));
				user.setUserLastName((String) Jobj.get("userLastName"));
				user.setUserMailId((String) Jobj.get("userMailId"));
				user.setUserFeedback((String) Jobj.get("userfeedback"));
				if(user.getUserFeedback().indexOf("`*`") != -1){
					user.setUserFeedback(user.getUserFeedback().replace("`*`", "\\\""));
				}
				user.setFeedbackType((String) Jobj.get("feedbackType"));
				user.setStatus((String) Jobj.get("status"));
				user.setDate((String) Jobj.get("date"));
				status= userDao.feedback(user);
				if(status!= null && status.equals("success")){
					return user;
				}
			}
		}catch (Exception e){
				logger.error("Exception while converting json to feedbackDetails object !", e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for userFeedback method : ");
		return null;
	}
	public String fetchAddusersDetails(String param,HttpServletRequest request) {
		if(logger.isDebugEnabled())
			logger.debug("while getting a user for  fetchAddusersDetails method:");
		JSONObject jsonObject;
		String userId=null;
		String jsonString = null;
		List<MnUsers> mnuser=null;
		try
		{
		jsonObject=new JSONObject(param);
		
		userId=(String) jsonObject.get("userId");
		mnuser = userDao.fetchAddusersDetails(userId);
		if(mnuser!=null && !mnuser.isEmpty()){
			jsonString = convertJson(mnuser,request);
		}
		
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for fetchAddusersDetails method : ");
		return jsonString;
	}
	public String addNewUser(String param) {
		if(logger.isDebugEnabled())
		logger.debug("while user addNewUser for addNewUser method:");
		MnUsers user = new MnUsers();
		int i;
		String status = "";
		MnUsers mnUser =null;
		List<String>userlist=new ArrayList<String>();
		Map<String,String> userMapForMail=new HashMap<String, String>();
		try
		{
			if (param != null && !param.equals(""))
			{
				JSONObject Jobj=new JSONObject(param);
				String userName=((String) Jobj.get("userName"));
				String password=((String) Jobj.get("password"));
				Integer addedByUserId=(Integer.parseInt(Jobj.getString("userId")));
				
				String Username[]=userName.split(",");
				
		    	 for(i=0;i<Username.length;i++){
		    		 mnUser= userDao.ExitsUser(Username[i].toLowerCase().trim());
		    		 if(mnUser==null){
		    		 mnUser=userDao.getExitsEmail(Username[i].toLowerCase().trim());	 
		    		 }
					 if(mnUser!=null){
						 userlist.add(Username[i]);
					 }
		    	if(mnUser==null){	
		    		//mnUser=userDao.getViewUserDetails(addedByUserId.toString());
		    		userMapForMail.put(Username[i], password);
		    	//commented-added users only by username not by email id
		    	 /*if(((Username[i]).indexOf('@')!=-1)&&((Username[i]).indexOf('.')!=-1)){
		    		 user.setEmailId(Username[i].toLowerCase().trim());
		    		 user.setUserName(null);
		    	 }else{
		    		 user.setUserName(Username[i].toLowerCase().trim());
		    		 user.setDisplayUserName(Username[i]);
		    		 user.setEmailId(null);
		    	 }*/
		    		
		    		user.setUserName(Username[i].toLowerCase().trim());
		    		user.setDisplayUserName(Username[i].trim());
		    		user.setUserFirstName(Username[i].trim());
		    		user.setUserLastName(Username[i].trim());
		    	 	//user.setTimeZone(mnUser.getTimeZone());
		    	 	
		    		user.setAdminNotificationFlag(true); // flag used to show the users video first time login
		    		user.setMailCheckFlag(true);
		    	 	user.setNotificationFlag("no");
		    	 	user.setStatus("A");
		    	 	Date date=new Date(); 
					SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
					sdf.format(date);
					user.setStartDate(sdf.format(date));
		    	 	user.setEndDate("07/01/2014");
		    	    user.setUserLevel("Premium-Silver");
		    	    user.setTermscheck("true");
		    		user.setPasswordText(password);
		    		user.setAddedByUserId(addedByUserId);
		    		user.setUserRole("Music Student");
		    		user.setAddedUserStatus("Inactive");
		    		user.setNoteCreateBasedOn("private");
		    		user.setUploadedSize(0.0);
		    		user.setLimitedSize(5120.00);
				   // user.setPassword(PasswordGenerator.encoder(user.getUserName(),user.getPasswordText()));
		    		
		    		 status= userDao.createUser(user);
		    		 status=DefaultNoteCreate(user,status);
		    		 MnFriends friend = new MnFriends();
					 friend.setUserId(user.getUserId());
					 friend.setRequestedUserId(addedByUserId);
					 friend.setStatus("R");
					 friend.setRequestedDate(new Date());
		    		 status=userDao.addFriendRequestForAddingStudents(friend);
		    	 }
			} 
		    	if(!userMapForMail.isEmpty()){
		    		 status=userDao.sendMailForAddingUser(userMapForMail,addedByUserId);
		    	}
		    	
			if(userlist.isEmpty()){
				return "Usernames does not exist";
			}
			}
		}catch (Exception e){
				logger.error("Exception while converting json to addNewUser object !", e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for addNewUser method : ");
		return userlist.toString();
	}
	@Override
	public String insertNewUser(MnUsers user,String params)
	{   if(logger.isDebugEnabled())
		logger.debug("while creating a insertNewUser method called: ");	
		String status = "";
		try
		{
			if(user.isPublicShareWarnMsgFlag())
				user.setPublicShareWarnMsgFlag(true);
			else
				user.setPublicShareWarnMsgFlag(false);
				
				user.setAdminNotificationFlag(true); // flag used to show the users video first time login
				user.setNotificationFlag("yes");
				user.setEndDate("07/01/2014");
				user.setUserLevel("Premium-Silver");
		    if(user.getUserRole().contains("Music Student")){
		    	if(user.getEmailId()==null){
		    		user.setMailCheckFlag(true);
		    		user.setMailCheckId(null);
		    		user.setNotificationFlag("no");
		    	}
		    }
			status = userDao.createUser(user);
			if(status!=null && !status.equals("")){
				JSONObject object = new JSONObject(status);
				
				for(int i=0;i<3;i++){
					
					MnList mnList=new MnList();
					
					if(i==0){
						//code for notes page
						String musicBookName=user.getUserFirstName()+" Notebook";
						mnList.setListName(musicBookName);
						mnList.setListType("music");
						mnList.setEnableDisableFlag(false);

						//NoteDetails for Getting Started note to be saved in MnList
						JSONObject jobj2=new JSONObject();
						jobj2.put("noteName","Getting Started");
						jobj2.put("startDate",datetimeFormat.format(date));
						jobj2.put("notesMembers","");
						jobj2.put("noteGroups","");
						jobj2.put("noteSharedAllContact",0);
						jobj2.put("noteSharedAllContactMembers","");
						jobj2.put("status","A");
						jobj2.put("tag","");
						jobj2.put("remainders","");
						jobj2.put("vote","");
						jobj2.put("links","");
						jobj2.put("endDate","");
						jobj2.put("noteId","1");
						jobj2.put("dueDate","");
						jobj2.put("dueTime","");
						jobj2.put("access","private");
						jobj2.put("attachFilePath","");
						jobj2.put("description",greetingnotedesc);
						jobj2.put("publicDescription","");
						jobj2.put("comments","");
						jobj2.put("pcomments","");
						jobj2.put("copyToMember","");
						jobj2.put("copyToGroup","");
						jobj2.put("copyToAllContact",0);
						jobj2.put("copyFromMember","");
						jobj2.put("ownerNoteStatus","A");
						jobj2.put("publicUser","");
						jobj2.put("publicDate","");
						jobj2.put("shareType","");
						jobj2.put("privateTags","");
						jobj2.put("privateAttachFilePath","");
						Set<String> s1=new HashSet<String>();
						String job1=jobj2.toString();
						s1.add(job1);
						mnList.setMnNotesDetails(s1);
					}else if(i==1){
						//code for schedule page 
						String schdeuleBookName=user.getUserFirstName()+" Calendar";
						mnList.setListName(schdeuleBookName);
						mnList.setListType("schedule");
						mnList.setEnableDisableFlag(true);
						mnList.setMnNotesDetails(new HashSet<String>());
					}else{
						//code for memos
						String memoBookName=user.getUserFirstName()+" Memos";
						mnList.setListName(memoBookName);
						mnList.setListType("bill");
						mnList.setEnableDisableFlag(false);
						mnList.setMnNotesDetails(new HashSet<String>());
					}
					
					mnList.setStatus("A");
					mnList.setUserId(Integer.parseInt(object.getString("userId")));
					mnList.setDefaultNote(true);
					mnList.setNoteAccess(user.getNoteCreateBasedOn());
					mnList.setSharedIndividuals(new ArrayList<Integer>());
					mnList.setSharedGroups(new ArrayList<Integer>());
					mnList.setShareAllContactFlag(false);
					iNoteDao.createList(mnList,"","");
					
					addSecurityQuestion(user.getSecurityId(),object.getString("userId"));
					
					
					
					if(i==0){
						//code for notedetails to be added to MnNoteDetails for getting started note
						MnNoteDetails mndetail=new MnNoteDetails();
						mndetail.setListId(mnList.getListId());
						mndetail.setListName(mnList.getListName());
						mndetail.setListType(mnList.getListType());
						mndetail.setAccess(mnList.getNoteAccess());
						mndetail.setUserId(mnList.getUserId());
						mndetail.setDefaultNote(mnList.isDefaultNote());
						mndetail.setEnableDisableFlag(mnList.isEnableDisableFlag());
						mndetail.setSharedIndividuals(mnList.getSharedIndividuals());
						mndetail.setSharedGroups(mnList.getSharedGroups());
						mndetail.setShareAllContactFlag(mnList.isShareAllContactFlag());
						mndetail.setNoteAccess("private");
						mndetail.setStatus(mnList.getStatus());
						mndetail.setNoteId(1);
						mndetail.setNoteName("Getting Started");
						mndetail.setNoteSharedAllContact(false);
						mndetail.setOwnerNoteStatus("A");
						mndetail.setDueDate("");
						mndetail.setDueTime("");
						mndetail.setStartDate(date);
						mndetail.setAccess("private");
						mndetail.setCopyToAllContact(false);
						mndetail.setDescription(greetingnotedesc);
						mndetail.setPublicDescription("");
						iNoteDao.createGreetNote(mndetail);	

						//code for tag to be added to getting started note
						String str="Getting Started";
						MnTag mnTag=new MnTag();
						mnTag.setTagName(str);
						mnTag.setUserId( mndetail.getUserId());
						mnTag.setCreateDate(dateFormat.format(date));
						mnTag.setStatus("A");
						String status1 = "0";
						status1 = iNoteDao.createTag(mnTag);
						
						//code for tag data to be updated in tagdetails
						if(status1!=null && !status1.isEmpty() && !status1.equals("0")){
							iNoteDao.updateTagForNote(status1, mndetail.getListId().toString(), mndetail.getNoteId().toString(),mnTag.getUserId().toString(),mndetail.getListType().toString());
						}
						
						//code for file attach
						//iNoteDao.attachDefaultVideo(mnList);
						iNoteDao.updateAttachedFileForNote("overview-of-musicnote-web-app_Jul-21-201414-03-16.mp4", mndetail.getListId().toString(), "1", mndetail.getUserId().toString());
						
						//Code for Comments to be added to getting Started note
						MnComments mncomment=new MnComments();
						mncomment.setUserId(mndetail.getUserId());
						mncomment.setListId(mndetail.getListId().toString());
						mncomment.setNoteId(mndetail.getNoteId().toString());
						mncomment.setComments(comment3);
						mncomment.setcDate(dateFormat.format(date));
						mncomment.setcTime(timeFormat.format(date));
						mncomment.setStatus("A");
						mncomment.setEdited(0);
						mncomment.setEditDate("");
						mncomment.setEditTime("");
						mncomment.setCommLevel("I");
						iNoteDao.addComments(mncomment,mndetail.getListId().toString(),mndetail.getNoteId().toString(),mndetail.getListType());
						mncomment.setComments(comment2);
						iNoteDao.addComments(mncomment,mndetail.getListId().toString(),mndetail.getNoteId().toString(),mndetail.getListType());
						mncomment.setComments(comment1);
						iNoteDao.addComments(mncomment,mndetail.getListId().toString(),mndetail.getNoteId().toString(),mndetail.getListType());

					}
				}
			}
		}
		
		catch (Exception e)
		{
			logger.error("Exception in insertNewUser method  : ", e);

		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for insertUser method : ");
		return status;
	}
	
	public String DefaultNoteCreate(MnUsers user,String status){	
		if(status!=null && !status.equals("")){
			try{
				JSONObject object = new JSONObject(status);
				String username="";
				for(int i=0;i<3;i++){
					
					MnList mnList=new MnList();
					
					if(user.getDisplayUserName()!=null)
						username=user.getDisplayUserName();
					else
						username=user.getUserName();
					
					if(i==0){
						//code for notes page
						String musicBookName=username+" Notebook";
						mnList.setListName(musicBookName);
						mnList.setListType("music");
						mnList.setEnableDisableFlag(false);

						//NoteDetails for Getting Started note to be saved in MnList
						JSONObject jobj2=new JSONObject();
						jobj2.put("noteName","Getting Started");
						jobj2.put("startDate",datetimeFormat.format(date));
						jobj2.put("notesMembers","");
						jobj2.put("noteGroups","");
						jobj2.put("noteSharedAllContact",0);
						jobj2.put("noteSharedAllContactMembers","");
						jobj2.put("status","A");
						jobj2.put("tag","");
						jobj2.put("remainders","");
						jobj2.put("vote","");
						jobj2.put("links","");
						jobj2.put("endDate","");
						jobj2.put("noteId","1");
						jobj2.put("dueDate","");
						jobj2.put("dueTime","");
						jobj2.put("access","private");
						jobj2.put("attachFilePath","");
						jobj2.put("description",greetingnotedesc);
						jobj2.put("comments","");
						jobj2.put("pcomments","");
						jobj2.put("copyToMember","");
						jobj2.put("copyToGroup","");
						jobj2.put("copyToAllContact",0);
						jobj2.put("copyFromMember","");
						jobj2.put("ownerNoteStatus","A");
						jobj2.put("publicUser","");
						jobj2.put("publicDate","");
						jobj2.put("shareType","");
						jobj2.put("privateTags","");
						jobj2.put("privateAttachFilePath","");
						Set<String> s1=new HashSet<String>();
						String job1=jobj2.toString();
						s1.add(job1);
						mnList.setMnNotesDetails(s1);
					}else if(i==1){
						//code for schedule page 
						String schdeuleBookName=username+" Calendar";
						mnList.setListName(schdeuleBookName);
						mnList.setListType("schedule");
						mnList.setEnableDisableFlag(true);
						mnList.setMnNotesDetails(new HashSet<String>());
					}else{
						//code for memos
						String memoBookName=username+" Memos";
						mnList.setListName(memoBookName);
						mnList.setListType("bill");
						mnList.setEnableDisableFlag(false);
						mnList.setMnNotesDetails(new HashSet<String>());
					}
					
					mnList.setStatus("A");
					mnList.setUserId(Integer.parseInt(object.getString("userId")));
					mnList.setDefaultNote(true);
					mnList.setNoteAccess(user.getNoteCreateBasedOn());
					mnList.setSharedIndividuals(new ArrayList<Integer>());
					mnList.setSharedGroups(new ArrayList<Integer>());
					mnList.setShareAllContactFlag(false);
					iNoteDao.createList(mnList,"","");
					
					
					//convertJsonToaddSecurityQuestion(params,object.getString("userId"));
					
					
					if(i==0){
						//code for notedetails to be added to MnNoteDetails for getting started note
						MnNoteDetails mndetail=new MnNoteDetails();
						mndetail.setListId(mnList.getListId());
						mndetail.setListName(mnList.getListName());
						mndetail.setListType(mnList.getListType());
						mndetail.setAccess(mnList.getNoteAccess());
						mndetail.setUserId(mnList.getUserId());
						mndetail.setDefaultNote(mnList.isDefaultNote());
						mndetail.setEnableDisableFlag(mnList.isEnableDisableFlag());
						mndetail.setSharedIndividuals(mnList.getSharedIndividuals());
						mndetail.setSharedGroups(mnList.getSharedGroups());
						mndetail.setShareAllContactFlag(mnList.isShareAllContactFlag());
						mndetail.setNoteAccess("private");
						mndetail.setStatus(mnList.getStatus());
						mndetail.setNoteId(1);
						mndetail.setNoteName("Getting Started");
						mndetail.setNoteSharedAllContact(false);
						mndetail.setOwnerNoteStatus("A");
						mndetail.setDueDate("");
						mndetail.setDueTime("");
						mndetail.setStartDate(date);
						mndetail.setAccess("private");
						mndetail.setCopyToAllContact(false);
						mndetail.setDescription(greetingnotedesc);
						iNoteDao.createGreetNote(mndetail);	

						//code for tag to be added to getting started note
						String str="Getting Started";
						MnTag mnTag=new MnTag();
						mnTag.setTagName(str);
						mnTag.setUserId( mndetail.getUserId());
						mnTag.setCreateDate(dateFormat.format(date));
						mnTag.setStatus("A");
						String status1 = "0";
						status1 = iNoteDao.createTag(mnTag);
						
						//code for tag data to be updated in tagdetails
						if(status1!=null && !status1.isEmpty() && !status1.equals("0")){
							iNoteDao.updateTagForNote(status1, mndetail.getListId().toString(), mndetail.getNoteId().toString(),mnTag.getUserId().toString(),mndetail.getListType().toString());
						}
						
						//Code for Comments to be added to getting Started note
						
						//code for file attach
						iNoteDao.updateAttachedFileForNote("overview-of-musicnote-web-app_Jul-21-201414-03-16.mp4", mndetail.getListId().toString(), "1", mndetail.getUserId().toString());
						
						MnComments mncomment=new MnComments();
						mncomment.setUserId(mndetail.getUserId());
						mncomment.setListId(mndetail.getListId().toString());
						mncomment.setNoteId(mndetail.getNoteId().toString());
						mncomment.setComments(comment3);
						mncomment.setcDate(dateFormat.format(date));
						mncomment.setcTime(timeFormat.format(date));
						mncomment.setStatus("A");
						mncomment.setEdited(0);
						mncomment.setEditDate("");
						mncomment.setEditTime("");
						mncomment.setCommLevel("I");
						iNoteDao.addComments(mncomment,mndetail.getListId().toString(),mndetail.getNoteId().toString(),mndetail.getListType());
						mncomment.setComments(comment2);
						iNoteDao.addComments(mncomment,mndetail.getListId().toString(),mndetail.getNoteId().toString(),mndetail.getListType());
						mncomment.setComments(comment1);
						iNoteDao.addComments(mncomment,mndetail.getListId().toString(),mndetail.getNoteId().toString(),mndetail.getListType());

					}
				}
			}catch(Exception e){
				
			}
		
		}return status;
	}
	
	
	@Override
	public String checkInviteuser(MnUsers user){
		if(logger.isDebugEnabled())
		logger.debug("while check invite user for checkInviteuser method : ");	
		MnUsers mnUserList =null;
		try
		{
			mnUserList = userDao.checkInviteuser(user);
		}
		catch (Exception e)
		{
			logger.error("Exception in checkInviteuser method  : ", e);

		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for checkInviteuser method : ");
		return "";
		
	}
	public IUserDao getUserDao()
	{
		return userDao;
	}

	public void setUserDao(IUserDao userDao)
	{
		this.userDao = userDao;
	}

	@Override
	public String getUserDetails(String userId,HttpServletRequest request) {
		if(logger.isDebugEnabled())
			logger.debug("while fetching getUserDetails method: "+userId);
			String jsonString = null;
			MnUsers mnUserList = userDao.getViewUserDetails(userId);
			if (mnUserList != null)
			{
				List<MnUsers> mnUsers=new ArrayList<MnUsers>();
				mnUsers.add(mnUserList);
				jsonString = convertToJson(mnUsers,request);

			}
			
			if(logger.isDebugEnabled())
			  logger.debug("Return response for getUserDetails method : ");
			return jsonString;
	}
	private String convertToJson(List<MnUsers> mnUserList,HttpServletRequest request) {

		StringBuffer jsonStr = new StringBuffer("[");
		for (MnUsers userList : mnUserList)
		{
			String photoPath="";
			String osName=System.getProperty("os.name");
			
		    if(osName.toLowerCase().indexOf("windows") > -1){
		    	if(userList.getFilePath()!=null && !userList.getFilePath().isEmpty())
		    		photoPath = "file:///"+userList.getFilePath().replaceAll("\\\\", "/");
		    	else
		    		photoPath = "";
		    }else{
		    	if(userList.getFilePath()!=null && !userList.getFilePath().isEmpty())
					photoPath= convertPhotoPathNewFormat(userList.getFilePath(), userList.getUserId().toString());
		    	else
		    		photoPath = "";
		    }
		    userList.getTimeZone();
			jsonStr.append("{\"_id\" : \"" + userList.get_id() + "\",");
			jsonStr.append("\"userFirstName\" : \"" + userList.getUserFirstName() + "\",");
			jsonStr.append("\"userLastName\" : \"" + userList.getUserLastName() + "\",");
			jsonStr.append("\"userName\" : \"" + userList.getUserName() + "\",");
			jsonStr.append("\"emailId\" : \"" + userList.getEmailId() + "\",");
			jsonStr.append("\"userId\" : \"" + userList.getUserId() + "\",");
			jsonStr.append("\"notificationFlag\" : \"" + userList.getNotificationFlag() + "\",");
			jsonStr.append("\"noteCreateBasedOn\" : \"" + userList.getNoteCreateBasedOn() + "\",");
			jsonStr.append("\"filePath\" : \"" + userList.getFilePath() + "\",");
			jsonStr.append("\"contactNumber\" : \"" + userList.getContactNumber() + "\",");
			jsonStr.append("\"publicShareWarnMsgFlag\" : \"" + userList.isPublicShareWarnMsgFlag() + "\",");
			jsonStr.append("\"instrument\" : \"" + userList.getInstrument() + "\",");
			jsonStr.append("\"skillLevel\" : \"" + userList.getSkillLevel() + "\",");
			jsonStr.append("\"favoriteMusic\" : \"" + userList.getFavoriteMusic() + "\",");
			jsonStr.append("\"timeZone\" : \"" +  userList.getTimeZone() + "\",");
			MnSecurityQuestions security=userDao.getSecurityQuestions(userList.getUserName());
			if(security==null){
				jsonStr.append("\"securityCheck\" : \"" + true + "\",");
			}else{
				jsonStr.append("\"securityCheck\" : \"" + false + "\",");	
			}
			jsonStr.append("\"followers\" : \"" + userList.getFollowers() + "\",");
			jsonStr.append("\"endDate\" : \"" + userList.getEndDate() + "\",");
			jsonStr.append("\"adminFlag\" : \"" + userList.isAdminFlag() + "\",");
			//adduserFlag
			jsonStr.append("\"addUserFlag\" : \"" + userList.isAddUserFlag() + "\",");
			if(userList.getUserLevel().contains("Trail"))
				jsonStr.append("\"level\" : \"Trial\",");
			else
				jsonStr.append("\"level\" : \"Premium\",");
		    jsonStr.append("\"userRole\" : \"" + userList.getUserRole() + "\"},");
		   

		}
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		if(logger.isDebugEnabled())
		logger.debug(" jsonStr while convertToJson after deleting last ','--->>and returned json{}" );
		jsonStr.append("]");
		return jsonStr.toString();
		
	}

	private String convertJson(List<MnUsers> mnUserList,HttpServletRequest request) {
		
		StringBuffer jsonStr = new StringBuffer("[");
		if(mnUserList!=null){
		for (MnUsers userList : mnUserList)
		{
			if(userList.getUserName()!=null && !userList.getUserName().isEmpty()){
				jsonStr.append("{\"userName\" : \"" + userList.getUserName() + "\",");
			}else{
				jsonStr.append("{\"userName\" : \"" + userList.getEmailId() + "\",");
			}
			if(userList.getAddedUserStatus()!=null && !userList.getAddedUserStatus().isEmpty()){
				jsonStr.append("\"status\" : \"" + userList.getAddedUserStatus() + "\"},");
			}else{
				jsonStr.append("\"status\" : \"Inactive\"},");
			}
		}
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		if(logger.isDebugEnabled())
		logger.debug(" jsonStr while convertToJson after deleting last ','--->>and returned json{}" );
		jsonStr.append("]");
		}
		
	return jsonStr.toString();
}

	@Override
	public String fetchUserDetails(String roleId,HttpServletRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("while fetching  a fetchUserDetails method with roleId:"+roleId);

		String jsonString = null;
		List<MnUsers> mnUserList = userDao.fetchUserDetails(roleId);
		if (mnUserList != null && !mnUserList.isEmpty())
		{
			jsonString = convertToJson(mnUserList,request);

		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for fetchUserDetails method : ");


		return jsonString;
	}
	
	/**
	 * Retrieve MemberList(like friends list in FaceBook) for particular user
	 * @param userId
	 * @author senthil
	 * @since 21/5/2013 2.13 PM
	 */
	@Override
	public String memberList(String userId,HttpServletRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("while fetching memberList method: "+userId);
		String jsonString = null;
		List<MnUsers> mnUserList=new ArrayList<MnUsers>();
		MnUsers mnUsers= userDao.memberList(Integer.parseInt(userId));
		if (mnUsers!=null && mnUsers.getFriends()!=null && !mnUsers.getFriends().isEmpty())
		{
			for(Integer uid:mnUsers.getFriends())
			{
				MnUsers member= userDao.memberList(uid);
				if(member!=null)
				{
					mnUserList.add(member);
				}
			}
			jsonString = convertToJson(mnUserList,request);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for memberList method : ");
		return jsonString;
	}

	
	@Override
	public String updateUser(MnUsers user) {
		if(logger.isDebugEnabled())
		logger.debug("while updating updateUser method: ");
		String status = "";
		try
		{
			status = userDao.updateUser(user);
		}
		catch (Exception e)
		{
			logger.error("Exception in updateUser method  : ", e);

		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for updateUser method : ");
		return status;
	}

	@Override
	public String getViewAllActiveUserListForSearch(MnUsers user,HttpServletRequest request) {
		if(logger.isDebugEnabled())
		logger.debug("while getting a getAllActiveUsersListForSearch method: ");
		String jsonString = null;
		List<MnUsers> mnUserList = userDao.getAllActiveUserDetailsForSearch(user);
		if (mnUserList != null && !mnUserList.isEmpty())
		{
			jsonString = convertToJson(mnUserList,request);

		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for getAllActiveUsersListForSearch method : ");
		return jsonString;
	}

	public INoteDao getiNoteDao() {
		return iNoteDao;
	}

	public void setiNoteDao(INoteDao iNoteDao) {
		this.iNoteDao = iNoteDao;
	}
	
	@Override
	public MnUsers convertJsonToUpdateUserObj(String jsonStr) {
		if(logger.isDebugEnabled())
		logger.debug("while fetching convertJsonToUpdateUserObj method: ");
		MnUsers user = new MnUsers();
		JSONObject jsonObject;
		
		try
		{
			jsonObject=new JSONObject(jsonStr);
			String emailId=(String) jsonObject.get("emailId");
			addSecurityQuestion((String)jsonObject.get("securityId"),(String) jsonObject.get("userId"));
			user.setUserFirstName((String) jsonObject.get("userFirstName"));
			user.setUserLastName((String) jsonObject.get("userLastName"));
			user.setUserName((String) jsonObject.get("userName"));
			user.setUserId(Integer.parseInt((String) jsonObject.get("userId")));
			user.setEmailId((String) jsonObject.get("emailId"));
			user.setUserRole((String) jsonObject.get("userRole"));
			user.setContactNumber((String) jsonObject.get("contactNumber"));
			user.setPublicShareWarnMsgFlag(Boolean.parseBoolean((String) jsonObject.get("publicShareWarnMsgFlag")));
			user.setInstrument((String) jsonObject.get("instrument"));
			user.setSkillLevel((String) jsonObject.get("skillLevel"));
			if(emailId.equals("") || emailId.equals("undefined")){
				user.setNotificationFlag("no");	
			}else{
				user.setNotificationFlag("yes");
			}
			user.setNoteCreateBasedOn((String) jsonObject.get("noteCreateBasedOn"));
			user.setFavoriteMusic((String) jsonObject.get("favoriteMusic"));
			user.setTempEmail((String) jsonObject.get("tempEmail"));
			String timeZone=(String) jsonObject.get("timeZone");
			String valueTimeZone=timeZone.substring(0,10).replace("(GMT","").replace(":","");
			user.setTimeZone(valueTimeZone);
			
			//for update username
			/*if(user.getUserName()!=null && !user.getUserName().equals("")){
				user.setDisplayUserName(user.getUserName().toLowerCase());
			}
			if(!user.getUserRole().equals("Music Student")){
				
				if(!user.getTempEmail().toLowerCase().equals(user.getEmailId().toLowerCase()))
				{
				String randomPass = RandomPassGenerator.getRandomPassword();
				user.setPasswordText(randomPass);
				
				byte[] encryptedPassword = PasswordGenerator.encoder(user.getEmailId().toLowerCase(), randomPass);
				user.setPassword(encryptedPassword);
			
				}
			}*/	
				Date date=new Date(); 
				SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
				sdf.format(date);
				user.setStartDate(sdf.format(date));
				user.setStatus("A");
				
			
		}
		catch (Exception e)
		{
			logger.error("Exception while converting json to user object !", e);

		}
		finally
		{
			
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for convertJsonToUpdateUserObj method : ");
		return user;
	}
	
	public String addSecurityQuestion(String question,String userId){
		try{
			String[] split=question.split("~");
			String questionId=split[0];
			String answer=split[1];
			MnSecurityQuestions security=new MnSecurityQuestions();
			security.setAnswer(answer);
			security.setQuestionId(questionId);
			security.setUserId(userId);
			userDao.addSecurityQuestion(security);
		}catch(Exception e){
		logger.error("Error while adding security question");	
		}
		return "success";
	}
	

	public String convertTimeZoneToGmt(String timeZone){
		Map<String,String> timezoneMap=new HashMap<String,String>();
		timezoneMap.put("(GMT-11:00) Midway Island, Samoa ", "-1100");
		timezoneMap.put("(GMT-10:00) Hawaii ", "-1000");
		timezoneMap.put("(GMT-09:00) Alaska", "-0900");
		timezoneMap.put("(GMT-08:00) Pacific Time (US and Canada)", "-0800");
		timezoneMap.put("(GMT-07:00) Mountain Time (US and Canada)", "-0700");
		timezoneMap.put("(GMT-06:00) Central Time (US and Canada),Saskatchewan", "-0600");
		timezoneMap.put("(GMT-05:00) Eastern Time (US and Canada),Indiana (East)", "-0500");
		timezoneMap.put("(GMT-04:00) Atlantic Time (Canada),Caracas, La Paz, Santiago", "-0400");
		timezoneMap.put("(GMT-03:30) Newfoundland and Labrador", "-0330");
		timezoneMap.put("(GMT-03:00) Brasilia,Buenos Aires, Georgetown,Greenland", "-0300");
		timezoneMap.put("(GMT-02:00) Mid-Atlantic", "-0200");
		timezoneMap.put("(GMT-01:00) Azores,Cape Verde Islands ", "0100");
		timezoneMap.put("(GMT-00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London ", "0000");
		timezoneMap.put("(GMT-00:00) Casablanca, Monrovia ", "-0000");
		timezoneMap.put("(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague", "+0100");
		timezoneMap.put("(GMT+02:00) Bucharest", "+0200");
		timezoneMap.put("(GMT+02:00) Helsinki, Kiev, Riga, Sofia, Tallinn, Vilnius ", "+0200");
		timezoneMap.put("(GMT+03:00) Moscow, St. Petersburg, Volgograd", "+0300");
		timezoneMap.put("(GMT+04:00) Abu Dhabi, Muscat ", "+0400");
		timezoneMap.put("(GMT+04:30) Kabul", "+0430");
		timezoneMap.put("(GMT+05:00) Ekaterinburg,Islamabad, Karachi, Tashkent", "+0500");
		timezoneMap.put("(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi", "+0530");
		timezoneMap.put("(GMT+05:45) Kathmandu ", "+0545");
		timezoneMap.put("(GMT+06:00) Astana, Dhaka ,Sri Jayawardenepura ,Almaty, Novosibirsk", "+0600");
		timezoneMap.put("(GMT+06:30) Yangon (Rangoon)", "+0630");
		timezoneMap.put("(GMT+07:00) Bangkok, Hanoi, Jakarta, Krasnoyarsk ", "+0700");
		timezoneMap.put("(GMT+08:00) Beijing, Chongqing, Hong Kong SAR, Urumqi", "+0800");
		timezoneMap.put("(GMT+09:00) Seoul ,Osaka, Sapporo, Tokyo ", "+0900");
		timezoneMap.put("(GMT+09:30) Darwin, Adelaide", "+0930");
		timezoneMap.put("(GMT+10:00) Canberra, Melbourne, Sydney", "+1000");
		timezoneMap.put("(GMT+11:00) Magadan, Solomon Islands, New Caledonia ", "+1100");
		timezoneMap.put("(GMT+12:00) Fiji Islands, Kamchatka, Marshall Islands ", "+1200");
		timezoneMap.put("(GMT+13:00) Nuku'alofa", "+1300");
		
		String valueTimeZone=timezoneMap.get(timeZone); 
		
		return valueTimeZone;
		
	}
	
	@Override
	public String getTeachersList(HttpServletRequest request)
	{
		if(logger.isDebugEnabled())
		logger.debug("while fetching  a getTeachersList method: ");

		String jsonString = null;
		try
		{
			List<MnUsers> mnUserList = userDao.getTeacherDetrails();
			if (mnUserList != null && !mnUserList.isEmpty())
			{
				jsonString = convertToJson(mnUserList,request);
			}
		
		}
		catch (Exception e)
		{
			logger.error("Exception in getTeachersList method "+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for getTeachersList method : ");

		return jsonString;
	}
	
	
	@Override
	public String getExistsEmail(String jsonStr) {
		if(logger.isDebugEnabled())
		logger.debug("while fetching  a getExistsEmail method: ");
		JSONObject jsonObject;
		String jsonString = null;
		String emailId=null;
		try
		{
		jsonObject=new JSONObject(jsonStr);
		emailId=(String) jsonObject.get("emailId");
		MnUsers mnUser = userDao.getExitsEmail(emailId.toLowerCase());
		if (mnUser != null)
		{
			jsonString = convertToMnUsersJson(mnUser,"");

		}
		
		}
		catch (Exception e) {
			logger.error("Exception in getExistsEmail method "+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for getExistsEmail method : ");

		return jsonString;
	}
	
	@Override
	public String resetPassword(String params) {
		if(logger.isDebugEnabled())
		logger.debug("while reseting  a resetPassword method: ");
		String status ="";
		try {
			JSONObject jsonObject = new JSONObject(params);
			String userId = (String) jsonObject.get("userId");
			String crfmPassword = (String) jsonObject.get("crfmPassword");
			String oldPassword = (String) jsonObject.get("oldPassword");
			
			if(userId != null ) {
				status = userDao.resetPassword(Integer.parseInt(userId), crfmPassword, oldPassword);
			}
		} catch (Exception e) {
			logger.error("reset Password  : " + e);
			status = "wrong";
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for resetPassword method : ");
		return status;
	}
	
	private String convertToMnUsersJson(MnUsers mnUser,String photoPath) {
		String emailId="";
		
		if(mnUser.getEmailId()==null || mnUser.getEmailId().equals("")){
			
		emailId="empty";	
		}else{
			emailId=mnUser.getEmailId();
		}
		StringBuffer jsonStr = new StringBuffer("[");

			jsonStr.append("{\"_id\" : \"" + mnUser.get_id() + "\",");
			jsonStr.append("\"userFirstName\" : \"" + mnUser.getUserFirstName() + "\",");
			jsonStr.append("\"userId\" : \"" + mnUser.getUserId() + "\",");
			jsonStr.append("\"userLastName\" : \"" + mnUser.getUserLastName() + "\",");
			jsonStr.append("\"userName\" : \"" + mnUser.getUserName() + "\",");
			
			if(photoPath!=null && !photoPath.equals(""))
				jsonStr.append("\"filePath\" : \"" + photoPath + "\",");
			else
				jsonStr.append("\"filePath\" : \"" + mnUser.getFilePath() + "\",");
			
			jsonStr.append("\"emailId\" : \"" +emailId + "\",");
		    jsonStr.append("\"userRole\" : \"" + mnUser.getUserRole() + "\"},");
			jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
			jsonStr.append("]");
			return jsonStr.toString();
	}

	@Override
	public String forgetPassword(String emailId) {
		if(logger.isDebugEnabled())
		logger.debug("while forgoting  a forgetPassword method: "+emailId);
		String status ="";
		try {
			status = userDao.forgetPassword(emailId);
		}catch (Exception e) {
			logger.error("forget Password  : " + e);
			status = "invalid";
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for forgetPassword method : ");
		return status;
	}

	@Override
	public String updateEmail(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("while updating a user for  updateEmail method:");
		JSONObject jsonObject;
		String jsonString = null;
		String emailId=null;
		String userId=null;
		try
		{
		jsonObject=new JSONObject(jsonStr);
		emailId=(String) jsonObject.get("emailId");
		userId=(String) jsonObject.get("userId");
		MnUsers mnUser = userDao.updateEmail(emailId.toLowerCase(), userId);
		if (mnUser != null && !mnUser.getUserId().toString().equals(userId))
		{
			jsonString = convertToMnUsersJson(mnUser,"");

		}
		
		}
		catch (Exception e) {
			logger.error("Exception in updateEmail method "+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for updateEmail method : ");
		return jsonString;
	}

	@Override
	public String updateProfilePicture(String userId, String filePath,HttpServletRequest request,boolean cropFlag) {
		if(logger.isDebugEnabled())
			logger.debug("while updating a user for  updateProfilePicture method:"+userId+" filePath: "+filePath);
		String filePaths="";
		try
		{
		MnUsers mnUser =userDao.updateProfilePath(userId, filePath,cropFlag);
		if(mnUser!=null && mnUser.getFilePath()!=null)
		{
			String osName=System.getProperty("os.name");
		    if(osName.toLowerCase().indexOf("windows") > -1){
		    	if(!mnUser.getFilePath().isEmpty())
		    		filePaths = convertToMnUsersJson(mnUser,"file:///"+mnUser.getFilePath().replaceAll("\\\\", "/"));
		    	else
		    		filePaths = convertToMnUsersJson(mnUser,"");
		    }else{
		    	filePaths = convertToMnUsersJson(mnUser,convertPhotoPathNewFormat(filePath,userId));
		    }
			
		}
		
		}
		catch (Exception e) {
			logger.error("update Profile Picture error !!!!!!"+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for updateProfilePicture method : ");
		return filePaths;
		
	}
	
	public static String convertPhotoPath(String filePath,HttpServletRequest request){
		if(logger.isDebugEnabled())
			logger.debug("convertPhotoPath method called: "+filePath);
 		String photo="";
 		String contextPath = request.getSession().getServletContext().getRealPath("/");
		try{
			InputStream in=null;
			boolean value = createInputStream(filePath);
			if(value){
				in =new FileInputStream(filePath);
			}else {
				return "";
			}
			String serverLocation= contextPath + "image/photo/";
			File file = new File(serverLocation);

			if (!file.exists())
			{
				file.mkdirs();
			}
			String fileName = filePath;
			String actualFileName = null;

			CharSequence separator1 = "\\";
			CharSequence separator2 = "/";

			if (fileName.contains(separator1))
			{
				int lastslash = fileName.lastIndexOf("\\");
				actualFileName = fileName.substring(lastslash + 1);
			}
			else if (fileName.contains(separator2))
			{
				int lastslash = fileName.lastIndexOf("/");
				actualFileName = fileName.substring(lastslash + 1);
			}
			else
			{
				actualFileName = fileName;
			}
			
			String formate = "";
			BufferedImage image = null;
	        try {
	        	
	        	File sourceimage = new File(filePath);
	        	 
	        	String type = null;
	        	URL u = new URL("file://"+filePath);
	        	URLConnection uc = null;
	        	uc = u.openConnection();
	        	type = uc.getContentType();
	        	if(type.contains("image")){
	        		formate = type.split("/")[1];
	        	}
	        	image = ImageIO.read(sourceimage);
	        	if(formate.equals("png")){
	        		ImageIO.write(image, "png",new File(serverLocation + actualFileName));
	        	}else
	        		ImageIO.write(image, formate,new File(serverLocation + actualFileName));
	        } catch (IOException e) {
	        	logger.error("Exception in convertPhotoPath method "+e);
	        }
		   
		   	String warName = new File(request.getSession().getServletContext().getRealPath("/")).getName();
		   	if(warName.indexOf(".")!= -1){
		   		warName = warName.substring(0,warName.indexOf("."));
		   	}
		   	photo="/"+warName+"/image/photo/"+actualFileName;
		   
			
		}catch (Exception e) {
			logger.error("Exception in convertPhotoPath method "+e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for convertPhotoPath method : ");
		return photo;
 	}
	
	public static String convertPhotoPathNewFormat(String filePath,String userId){
		if(logger.isDebugEnabled())
			logger.debug("convertPhotoPathNewFormat method called: "+userId);
		String photo="";
		try{
			String fileName = filePath;
			String actualFileName = null;

			CharSequence separator1 = "\\";
			CharSequence separator2 = "/";

			if (fileName.contains(separator1))
			{
				int lastslash = fileName.lastIndexOf("\\");
				actualFileName = fileName.substring(lastslash + 1);
			}
			else if (fileName.contains(separator2))
			{
				int lastslash = fileName.lastIndexOf("/");
				actualFileName = fileName.substring(lastslash + 1);
			}
			else
			{
				actualFileName = fileName;
			}
			String fileExtension="";
			if(actualFileName!=null && !actualFileName.isEmpty()){
				int lastIndex = actualFileName.lastIndexOf(".");
				fileExtension=actualFileName.substring(lastIndex + 1);
			}
		   	photo="/UploadMusicFiles/"+userId+"/files/"+actualFileName;

		   
			
		}catch (Exception e) {
			logger.error("Exception in convertPhotoPathNewFormat method "+e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for convertPhotoPathNewFormat method :");
		return photo;
	}
	public static boolean createInputStream(String logoPath){
		if(logger.isDebugEnabled())
			logger.debug("createInputStream method called: "+logoPath);
		InputStream in =null;
		try{
			in = new FileInputStream(logoPath);
		}catch (FileNotFoundException e) {
			return false;
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for createInputStream method : ");
		return true;
	}
	
	@Override
	public String ExistsUser(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("while checking a user for  ExistsUser method:");
		JSONObject jsonObject;
		String jsonString = null;
		String userName=null;
		try
		{
		jsonObject=new JSONObject(jsonStr);
		userName=(String) jsonObject.get("userName");
		MnUsers mnUser = userDao.ExitsUsers(userName);
		if (mnUser != null)
		{
			jsonString = convertToMnUsersJson(mnUser,"");
		}

		}
		catch (Exception e) {
			logger.error("Exception in ExistsUser method "+e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for ExistsUser method : ");
		return jsonString;
	}

	@Override
	public String updateUserName(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("while updating a user for  updateUserName method:");
		JSONObject jsonObject;
		String jsonString = null;
		String userName=null;
		String userId=null;
		try
		{
		jsonObject=new JSONObject(jsonStr);
		userName=(String) jsonObject.get("userName");
		userId=(String) jsonObject.get("userId");
		MnUsers mnUser = userDao.updateUserName(userName, userId);
		if (mnUser != null && !mnUser.getUserId().toString().equals(userId))
		{
			jsonString = convertToMnUsersJson(mnUser,"");

		}
		
		}
		catch (Exception e) {
			logger.error("Exception in updateUserName method "+e);
		}
		if(logger.isDebugEnabled())
			logger.debug("Return response for updateUserName method : ");
		return jsonString;
	}

	@Override
	public Map<String, String> getUserName(String recipients,String listType) {
		if(logger.isDebugEnabled())
			logger.debug("getUserName method called: "+recipients);
		Map<String,String> map=null;
		try
		{
		 map = userDao.fetchUserName(recipients,listType);
		
		}
		catch (Exception e) {
			logger.error(e);	
			
		}
		
		if(logger.isDebugEnabled())
			logger.debug("Return response for getUserNamet method : ");
		return map;

		
	}
	
	@Override
	public String getTrailEndMailValues() {
		userDao.getTrailEndMailValues();
		
		return "success";
	}
	
	@Override
	public String getTrailcount(String params) {
		if(logger.isDebugEnabled())
			logger.debug("while getting a user for  getTrailcount method:");
		JSONObject jsonObject;
		String userId=null;
		String status="";
		try
		{
		jsonObject=new JSONObject(params);
		
		userId=(String) jsonObject.get("userId");
		status = userDao.getTrailcount(userId);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for getTrailcount method : ");
		return status;
	}
	
	@Override
	public String sendMailNotificationsForAutomatically()
	{
		if(logger.isDebugEnabled())
			logger.debug("while sending a user for  sendMailNotificationsForAutomatically method:");
		String status="";
		try
		{
			status=userDao.sendMailNotificationsForAutomatically();
			userDao.inActiveMailconfiguration();
		}
		catch (Exception e) {
			logger.error("Error in sendMailNotificationsForDefaultTrialUsers method :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for sendMailNotificationsForAutomatically method : ");
		return status;
		
	}

	
	@Override
	public String setUserLevelAutomatically() {
		String status="";
		try
		{
			status=userDao.setUserLevelChanges();
		}catch (Exception e) {
			// TODO: handle exception
		}
		return status;
	}
	
	public MnMailConfiguration convertJsonToMailconfigurationObj(String param) 
	{
		MnMailConfiguration user = new MnMailConfiguration();
		try
		{
			if (param != null && !param.equals(""))
			{
				JSONObject Jobj=new JSONObject(param);
				user.setUserId(Integer.parseInt(Jobj.getString("userId")));
				user.setUserLevel((String) Jobj.get("userLevel"));
				user.setIntervel((String) Jobj.get("interval"));
				user.setStartDate((String) Jobj.get("startDate"));
				user.setEndDate((String) Jobj.get("endDate"));
				user.setMailSubject((String) Jobj.get("mailSubject"));
				user.setMailMessage((String) Jobj.get("mailMessage"));
				user.setUserMessage((String)Jobj.get("userMessage"));
				user.setUserSubject((String)Jobj.get("userSubject"));
				user.setAmountDays((String)Jobj.getString("amountDays"));
			}
		}catch (Exception e){
				logger.error("Exception while converting json to feedbackDetails object !", e);
		}
		return user;
	}
	
	
	@Override
	public String sendUserCreationMail(String param) {
		if(logger.isDebugEnabled())
		logger.debug("while sending amail for sendingUserCreationMail method for MnUser:  ");
		String status="";
		try
		{
		JSONObject jsonstr=new JSONObject(param);
		String userId=(String)jsonstr.get("userId");
		status=userDao.sendUserCreationMail(userId);	
			
		}catch (Exception e) {
			
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for sendingUserCreationMail method : ");
		return status;
		
	}
	@Override
	public String getSecurityQuestion(String params) {
		if(logger.isDebugEnabled())
			logger.debug("while sending amail for getSequrity method for MnUser:  ");
			MnSecurityQuestions question;
			StringBuffer jsonStr = new StringBuffer();
			String status="";
			try
			{
			JSONObject jsonstring=new JSONObject(params);
			String userName=(String)jsonstring.get("userName");
			question=userDao.getSecurityQuestions(userName);
			
			//String data=getQuestion(question.getQuestionId());
			if(question!=null){
				jsonStr.append("{\"questionId\" : \"" + question.getQuestionId()+ "\",");
				jsonStr.append("\"answer\" : \"" +question.getAnswer() + "\",");
				jsonStr.append("\"question\" : \"" + question.getQuestion() + "\"},");
				jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
			}else{
			}
			}catch (Exception e) {
			logger.error("Error while fetching security question");	
			}
			if(logger.isDebugEnabled())
			logger.debug("Return response for getSequrity question method : ");
			return jsonStr.toString();
		
	}
	
	@Override
	public String getAdminSecurityQuestion() 
	{
		String status=null;
		try
		{
			status=userDao.getAdminSecurityQuestion();
			
		}catch(Exception e)
		{
			
		}
		return status;
	}
	
	public String getQuestion(String questionKey){
		String status="";
		Map<String,String> map=new HashMap<String,String>();
		String data=null;
		try{
			map.put("Q1", JavaMessages.Q1);
			map.put("Q2", JavaMessages.Q2);
			map.put("Q3", JavaMessages.Q3);
			map.put("Q4", JavaMessages.Q4);
			map.put("Q5", JavaMessages.Q5);
			data=map.get(questionKey);
		}catch(Exception e){
			logger.error("Exception while getting questions");	
		}
		return data;
		
	}
	@Override
	public String setResetPassword(String params) {
		String status="";
		
		try{
			JSONObject jsonstring=new JSONObject(params);
			String userName=(String)jsonstring.get("userName");
			String password=(String)jsonstring.get("password");
			status=userDao.setResetPassword(userName,password);
		}catch(Exception e){
			logger.error("Exception while setResetPassword");	
		}
		return status;
	}
	@Override
	public String setResetPass() {
		String status="";
		
		try{
		status=userDao.setResetPass();
		
	}catch(Exception e){
		logger.error("Exception while setResetPassword");	
	}
		return status;
	}
	@Override
	public String newToken(HttpServletRequest request ) {
		String status="";
	
	try{
	   status=userDao.newToken(request);
	
      }catch(Exception e){
	logger.error("Exception while new Token");	
     }
	return status;
	}
	
	public String userMailNotificationList(Integer userId,String jsonStr){
		if(logger.isDebugEnabled())
			logger.debug("userMailNotificationList method called:");
			MnMailNotificationList mailNotify = new MnMailNotificationList();
			String status = "";
			try
			{
				if (jsonStr != null && !jsonStr.equals(""))
				{
					JSONObject Jobj=new JSONObject(jsonStr);
					
					mailNotify.setUserId(userId);
					String noteMail=(String) Jobj.get("noteMail");
					String eventMail=(String) Jobj.get("eventMail");
					String memoMail=(String) Jobj.get("memoMail");
					String contactMail=(String) Jobj.get("contactMail");
					String crowdMail=(String) Jobj.get("crowdMail");
					String dueDateMail=(String) Jobj.get("dueDateMail");
					if(noteMail!=null && noteMail.contains("true"))
					{
					mailNotify.setNoteBasisMail(true);
					}
					else
					{
					mailNotify.setNoteBasisMail(false);
					}
					
					if(eventMail!=null && eventMail.contains("true"))
					{
						mailNotify.setEventBasisMail(true);
					}
					else
					{
						mailNotify.setEventBasisMail(false);
					}
					
					if(contactMail!=null && contactMail.contains("true"))
					{
						mailNotify.setContactBasisMail(true);
					}
					else
					{
						mailNotify.setContactBasisMail(false);
					}
					
					if(crowdMail!=null && crowdMail.contains("true"))
					{
						mailNotify.setCrowdBasisMail(true);
					}
					else
					{
						mailNotify.setCrowdBasisMail(false);
					}
					if(memoMail!=null && memoMail.contains("true"))
					{
						mailNotify.setMemoBasisMail(true);
					}
					else
					{
						mailNotify.setMemoBasisMail(false);
					}
					
					if(dueDateMail!=null && dueDateMail.contains("true"))
					{
						mailNotify.setDueDateMail(true);
					}
					else
					{
						mailNotify.setDueDateMail(false);
					}
					
					status= userDao.MnMailNotifications(mailNotify);
					
					
				}
			}catch (Exception e){
					logger.error("Exception in userMailNotificationList method", e);
			}
			if(logger.isDebugEnabled())
			logger.debug("Return response for userMailNotificationList method : ");
			return status;
		
	}
	@Override
	public String mailConfig(String userId) {
		String status="";
	
		if(logger.isDebugEnabled())
			logger.debug("while fetching mailconfig method: ");
			try
			{
				MnMailNotificationList list = userDao.fetchMailNotification(userId);
				if(list!=null)
				{
					status=convertJson(list);
				}
			}
			catch (Exception e)
			{
				logger.error("Exception in fetching method  : ", e);
			}
			if(logger.isDebugEnabled())
			logger.debug("Return response for fetching method : ");
			return status;
	}
	
	
	private String convertJson(MnMailNotificationList mailNotificationList) {
		
		StringBuffer jsonStr = new StringBuffer("[");
		if(mailNotificationList!=null){
			
			jsonStr.append("{\"userId\" : \"" + mailNotificationList.getUserId() + "\",");
			jsonStr.append("\"noteBasisMail\" : \"" + mailNotificationList.isNoteBasisMail() + "\",");
			jsonStr.append("\"eventBasisMail\" : \"" + mailNotificationList.isEventBasisMail() + "\",");
			jsonStr.append("\"memoBasisMail\" : \"" + mailNotificationList.isMemoBasisMail() + "\",");
			jsonStr.append("\"contactBasisMail\" : \"" +mailNotificationList.isContactBasisMail()+ "\",");
			jsonStr.append("\"dueDateMail\" : \"" +mailNotificationList.isDueDateMail()+ "\",");
		    jsonStr.append("\"crowdBasisMail\" : \"" + mailNotificationList.isCrowdBasisMail() + "\"},");
		   
		
		
			}
		
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		if(logger.isDebugEnabled())
		logger.debug(" jsonStr while convertToJson after deleting last ','--->>and returned json{}"+jsonStr.toString());
		jsonStr.append("]");
		
		
	return jsonStr.toString();
}
	@Override
	public String updatUserMailNotificationList(Integer userId,
			String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("userMailNotificationList method called:");
			MnMailNotificationList mailNotify = new MnMailNotificationList();
			String status = "";
			try
			{
				if (jsonStr != null && !jsonStr.equals(""))
				{
					JSONObject Jobj=new JSONObject(jsonStr);
					
					mailNotify.setUserId(userId);
					String noteMail=(String) Jobj.get("noteMail");
					String eventMail=(String) Jobj.get("eventMail");
					String memoMail=(String) Jobj.get("memoMail");
					String contactMail=(String) Jobj.get("contactMail");
					String crowdMail=(String) Jobj.get("crowdMail");
					String dueDateMail=(String) Jobj.get("dueDateMail");
					if(noteMail!=null && noteMail.equals("true"))
					{
					mailNotify.setNoteBasisMail(true);
					}
					else
					{
					mailNotify.setNoteBasisMail(false);
					}
					
					if(eventMail!=null && eventMail.equals("true"))
					{
						mailNotify.setEventBasisMail(true);
					}
					else
					{
						mailNotify.setEventBasisMail(false);
					}
					
					if(contactMail!=null && contactMail.equals("true"))
					{
						mailNotify.setContactBasisMail(true);
					}
					else
					{
						mailNotify.setContactBasisMail(false);
					}
					
					if(crowdMail!=null && crowdMail.equals("true"))
					{
						mailNotify.setCrowdBasisMail(true);
					}
					else
					{
						mailNotify.setCrowdBasisMail(false);
					}
					if(memoMail!=null && memoMail.equals("true"))
					{
						mailNotify.setMemoBasisMail(true);
					}
					else
					{
						mailNotify.setMemoBasisMail(false);
					}
					if(dueDateMail!=null && dueDateMail.equals("true"))
					{
						mailNotify.setDueDateMail(true);
					}
					else
					{
						mailNotify.setDueDateMail(false);
					}
					
					
					
					
					
				
						status= userDao.updateMnMailNotifications(mailNotify);
					
				}
			}catch (Exception e){
					logger.error("Exception in userMailNotificationList method", e);
			}
			if(logger.isDebugEnabled())
			logger.debug("Return response for userMailNotificationList method : ");
			return status;
		
	}
	@Override
	public MnUsers limiteSize(String userId) {
		if(logger.isDebugEnabled())logger.debug("Limited Size upload: "+userId);
		MnUsers mnUser=null;
		try
		{
			mnUser=userDao.limitedUpload(userId);	
		}
		catch (Exception e)
		{
			logger.error("Error in Limited Size upload : ", e);
		}
		if(logger.isDebugEnabled())logger.debug("Limited Size upload");
		return mnUser;
		
	}
	
	
}
