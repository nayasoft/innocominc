package com.musicnotes.apis.resources;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.musicnotes.apis.dao.impl.BaseDaoImpl;
import com.musicnotes.apis.dao.interfaces.IAdminDao;
import com.musicnotes.apis.domain.MnAdminComplaintMailTemplate;
import com.musicnotes.apis.domain.MnAdminConfiguration;
import com.musicnotes.apis.domain.MnAdminFiles;
import com.musicnotes.apis.domain.MnAdminSecurityQuestion;
import com.musicnotes.apis.domain.MnBlockedUsers;
import com.musicnotes.apis.domain.MnComplaintAction;
import com.musicnotes.apis.domain.MnComplaints;
import com.musicnotes.apis.domain.MnMailConfiguration;
import com.musicnotes.apis.domain.MnNoteDetails;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.interfaces.MnAdminable;

public class MnAdmin extends BaseDaoImpl implements MnAdminable {
	
	IAdminDao iAdminDao;
	static Logger logger = Logger.getLogger(MnAdmin.class);
	public IAdminDao getiAdminDao() {
		return iAdminDao;
	}

	public void setiAdminDao(IAdminDao iAdminDao) {
		this.iAdminDao = iAdminDao;
	}

	@Override
	public String checkDefaultConfiguration() {

		if(logger.isDebugEnabled())
			logger.debug("while checking a user for  checkDefaultConfiguration method:");
		String status="";
		try
		{
		
		status=iAdminDao.updateMacheckDefaultConfiguration();	
			
		}catch (Exception e) {
			
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for checkDefaultConfiguration method : ");
		return status;
	
	}
	
	@Override
	public String addDefaultConfiguration(String params){
		if(logger.isDebugEnabled())
			logger.debug("while adding a user for  addDefaultConfiguration method:");
		String status="";
		List<String>adminList=new ArrayList<String>();
		Date dateToday=new Date();
		SimpleDateFormat formatter=new SimpleDateFormat("mm/dd/yyyy");
		String todayDate=formatter.format(dateToday);
		try
		{
		
		JSONObject jsonstr=new JSONObject(params);
		String paymentAmountDays=jsonstr.getString("paymentAmountDays");
		String uploadLimit=(String)jsonstr.getString("uploadLimit");
		String userId=(String)jsonstr.getString("userId");
		String userStatus=(String)jsonstr.getString("status");
		String[] split=paymentAmountDays.split(",");
		for(int i=0;i<split.length;i++){
		 adminList.add(split[i]);
		}
		MnAdminConfiguration admin=new MnAdminConfiguration();
		admin.setPaymentAmountDays(adminList);
		admin.setUploadLimit(uploadLimit);
		admin.setUserId(userId);
		admin.setStatus(userStatus);
		admin.setStartDate(todayDate);
		status=iAdminDao.addDefaultConfiguration(admin);	
			
		}catch (Exception e) {
			
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for addDefaultConfiguration method : ");
		return status;
	}
	
	@Override
	public String checkExistingUserLevel(String params){
		if(logger.isDebugEnabled())
			logger.debug("while checking a user for  checkExistingUserLevel method:");
		String status="";
		try
		{
		
		JSONObject jsonstr=new JSONObject(params);
		String userLevel=jsonstr.getString("userLevel");
		Integer mailNo=(Integer)jsonstr.getInt("mailNo");
		status=iAdminDao.checkExistingUserLevel(userLevel,mailNo);	
			
		}catch (Exception e) {
			
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for checkExistingUserLevel method : ");
		return status;
	}

	@Override
	public MnMailConfiguration convertJsonToMailconfigurationObj(String param) {
		MnMailConfiguration user = new MnMailConfiguration();
		try
		{
			if (param != null && !param.equals(""))
			{
				JSONObject Jobj=new JSONObject(param);
				user.setUserId(Integer.parseInt(Jobj.getString("userId")));
				user.setUserLevel((String) Jobj.get("userLevel"));
				user.setIntervel((String) Jobj.get("interval"));
				user.setStartDate((String) Jobj.get("startDate"));
				user.setEndDate((String) Jobj.get("endDate"));
				user.setMailSubject((String) Jobj.get("mailSubject"));
				user.setMailMessage((String) Jobj.get("mailMessage"));
				user.setUserMessage((String)Jobj.get("userMessage"));
				user.setUserSubject((String)Jobj.get("userSubject"));
				user.setAmountDays((String)Jobj.getString("amountDays"));
			}
		}catch (Exception e){
				logger.error("Exception while converting json to feedbackDetails object !", e);
		}
		return user;
	}

	@Override
	public String updateMailConfiguration(MnMailConfiguration configuration) {
		if(logger.isDebugEnabled())
			logger.debug("while updating a user for  updateMailConfiguration method:");
		String status="";
		try
		{
			configuration.setStatus("A");
			status=iAdminDao.updateMailConfigurations(configuration);
			
		}catch (Exception e) {
			logger.error("Error in createMailConfiguration method :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for updateMailConfiguration method : ");
		return status;
	
	}
	
	@Override
	public String deleteMailConfiguration(String params) {
		if(logger.isDebugEnabled())
			logger.debug("while deleting a user for  deleteMailConfiguration method:");
		String status=null;
		try
		{
			JSONObject object=new JSONObject(params);
			String mailNo=(String) object.get("mailNo");
			status=iAdminDao.deletMailConfigure(mailNo);
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for deleteMailConfiguration method : ");
		return status;
	}
	
	@Override
	public String getMailconfiguration(String params) 
	{
		if(logger.isDebugEnabled())
			logger.debug("while getting a user for  getMailconfiguration method:");
		List<MnMailConfiguration> get=null;
		String status = null;
		try
		{
			get=iAdminDao.getMailConfiguration();
			status=convertMnMailConfigurationToJson(get);
			
		}catch (Exception e) {
			logger.debug("Error at getMailconfiguration method :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for getMailconfiguration method : ");
		return status;
		
	}
	
	private String convertMnMailConfigurationToJson(List<MnMailConfiguration> configuration) {

		StringBuffer jsonStr = new StringBuffer("[");
		for (MnMailConfiguration configure : configuration)
		{
			jsonStr.append("{\"mailNo\" : \"" + configure.getMailNo() + "\",");
			jsonStr.append("\"userId\" : \"" + configure.getUserId() + "\",");
			jsonStr.append("\"userLevel\" : \"" + configure.getUserLevel() + "\",");
			jsonStr.append("\"intervel\" : \"" + configure.getIntervel() + "\",");
			jsonStr.append("\"startDate\" : \"" + configure.getStartDate() + "\",");
			jsonStr.append("\"endDate\" : \"" + configure.getEndDate() + "\",");
			jsonStr.append("\"mailSubject\" : \"" + configure.getMailSubject() + "\",");
			jsonStr.append("\"mailMessage\" : \"" + configure.getMailMessage() + "\",");
			jsonStr.append("\"userSubject\" : \"" + configure.getUserSubject() + "\",");
			jsonStr.append("\"userMessage\" : \"" + configure.getUserMessage() + "\",");
			jsonStr.append("\"amountDays\" : \"" + configure.getAmountDays() + "\",");
			jsonStr.append("\"status\" : \"" + configure.getStatus() + "\"},");
		   
		}
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		return jsonStr.toString();
	}
	
	public String updateMailNotification(String params)
	{
		if(logger.isDebugEnabled())
			logger.debug("while updating a user for  updateMailNotification method:");
		String status="";
		try
		{
		
		JSONObject jsonstr=new JSONObject(params);
		Boolean status1=(Boolean)jsonstr.get("status");
		Boolean status2=(Boolean)jsonstr.get("checkFlag");
		status=iAdminDao.updateMailNotification(status1,status2);	
			
		}catch (Exception e) {
			
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for updateMailNotification method : ");
		return status;
		
	}
	
	@Override
	public String viewDefaultConfiguration(){
		
		MnAdminConfiguration admin;
		JSONObject jobj=new JSONObject();
		try
		{
		//Integer mailNo=(Integer)jsonstr.getInt("mailNo");
		admin=iAdminDao.viewDefaultConfiguration();	
		
		jobj.put("paymentAmount",admin.getPaymentAmountDays());
		jobj.put("uploadLimit", admin.getUploadLimit());
		jobj.put("configId", admin.getConfigId());
		}catch (Exception e) {
			
		}
		return jobj.toString();
		
	}
	
	@Override
	public String updateDefaultConfiguration(String configId,String params){
		
		String status="";
		try
		{
			JSONObject jobj=new JSONObject(params);
			String paymentAmountDays=jobj.getString("paymentAmountDays");
			String status1=jobj.getString("status");
			String uploadLimit=jobj.getString("uploadLimit");
			String userId=jobj.getString("userId");
			
			status=iAdminDao.updateDefaultConfiguration(configId,paymentAmountDays,status1,uploadLimit,userId);
		}catch (Exception e) {
			
		}
		return status;
		
	}
	
	@Override
		public String createMailConfiguration(MnMailConfiguration mailConfiguration)
		{
			if(logger.isDebugEnabled())
				logger.debug("while creating a user for  createMailConfiguration method:");
			String status="";
			try
			{
				mailConfiguration.setStatus("A");
				status=iAdminDao.createMailConfigurations(mailConfiguration);
				
			}catch (Exception e) {
				logger.error("Error in createMailConfiguration method :"+e.getMessage());
			}
			if(logger.isDebugEnabled())
			logger.debug("Return response for createMailConfiguration method : ");
			return status;
		}
		
	@Override
	public String fetchUserDetailsBasedLevelRole(String param) {
		if(logger.isDebugEnabled())
			logger.debug(" fetchUserDetailsBasedLevelRole method called:");
		MnUsers user=new MnUsers();
		String jsonString = null;
		List<MnUsers> mnUserList = null;
		try
		{
			JSONObject Jobj=new JSONObject(param);
			user.setUserLevel((String) Jobj.get("userLevel"));
			user.setUserRole((String) Jobj.get("userRole"));
			mnUserList =iAdminDao.fetchUserDetailsBasedLevelRole(user);
			if (mnUserList != null && !mnUserList.isEmpty())
			{
				jsonString = convertToJsonforAdmin(mnUserList);
			}
			else{
				
			}
				
		}catch (Exception e){
			logger.error("Exception while converting json to fetchUserDetailsBasedLevelRole object !", e);
		}
	
		return jsonString;
	}
	private String convertToJsonforAdmin(List<MnUsers> mnUserList) {
		if(logger.isDebugEnabled())
			logger.debug(" convertToJsonforAdmin method called:");
		StringBuffer jsonStr = new StringBuffer("[");
		for (MnUsers userList : mnUserList)
		{
			String photoPath="";
			String osName=System.getProperty("os.name");
		    userList.getTimeZone();
			jsonStr.append("{\"userName\" : \"" + userList.getUserName() + "\",");
			if(userList.getEmailId()!=null){
				jsonStr.append("\"emailId\" : \"" + userList.getEmailId().toLowerCase() + "\",");
			}else{
				jsonStr.append("\"emailId\" : \"-\",");
			}
			jsonStr.append("\"startDate\" : \"" + userList.getStartDate() + "\",");
			jsonStr.append("\"userRole\" : \"" + userList.getUserRole() + "\",");
			jsonStr.append("\"endDate\" : \"" + userList.getEndDate() + "\",");
			jsonStr.append("\"level\" : \"" + userList.getUserLevel() + "\",");
			if(userList.getStatus().contains("A")){
			jsonStr.append("\"status\" : \" Active\",");
			}
			else{
				jsonStr.append("\"status\" : \" Inactive\",");
			}
			/*if(userList.getUserLevel().contains("Trial")){
				jsonStr.append("\"level\" : \"Trial\",");
			}
			else if(userList.getUserLevel().contains("Expired")){
				jsonStr.append("\"level\" : \"Expired\",");
			}else{
				jsonStr.append("\"level\" : \"Premium\",");
			}*/
			jsonStr.append("\"userId\" : \"" + userList.getUserId() +"\"},");
		   

		}
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		return jsonStr.toString();
	}
	
	@Override
	public String fetchComplaintDetailsForAdmin(String param) {
		if(logger.isDebugEnabled())
			logger.debug(" fetchComplaintDetailsForAdmin method called:");
		//String user=new MnUsers();
		String jsonString = null;
		List<MnComplaints> mnComplaint = null;
		try
		{
			JSONObject Jobj=new JSONObject(param);
			String complaints=(String) Jobj.get("userComplaint");
			//user.setUserRole((String) Jobj.get("userRole"));
			mnComplaint =iAdminDao.fetchComplaintDetailsForAdmin(complaints);
			if (mnComplaint != null && !mnComplaint.isEmpty())
			{
				//Collections.sort(mnComplaint, SENIORITY_ORDER_CROWD);
				jsonString = convertToComplaintJsonforAdmin(mnComplaint);
			}
			else
			{
				
			}
		}catch (Exception e){
			logger.error("Exception while converting json to userDetails fetchComplaintDetailsForAdmin object !");
		}
	
		return jsonString;
	}
	@Override
	public String fetchNoteComplaintDetailsForAdmin(String param) {
		if(logger.isDebugEnabled())
			logger.debug(" fetchComplaintDetailsForAdmin method called:");
		//String user=new MnUsers();
		String jsonString = null;
		List<MnComplaints> mnComplaint = null;
		try
		{
			JSONObject Jobj=new JSONObject(param);
			String complaints=(String) Jobj.get("noteComplaint");
			//user.setUserRole((String) Jobj.get("userRole"));
			mnComplaint =iAdminDao.fetchNoteComplaintDetailsForAdmin(complaints);
			if (mnComplaint != null && !mnComplaint.isEmpty())
			{
				Collections.sort(mnComplaint, SENIORITY_ORDER_CROWD_COMP);
				jsonString = convertToComplaintJsonforAdmin(mnComplaint);
			}
			else
			{
				
			}
		}catch (Exception e){
			logger.error("Exception while converting json to userDetails fetchComplaintDetailsForAdmin object !", e);
		}
	
		return jsonString;
	}
	static final Comparator<MnComplaints> SENIORITY_ORDER_CROWD_COMP = new Comparator<MnComplaints>() {
		public int compare(MnComplaints e1, MnComplaints e2) {
			Date i1=null,i2=null;
			try {
				
				 i1 = new Date(e1.getDate());
				 i2 = new Date(e2.getDate());
				 
			
			} catch (Exception e) {
				
			}
			 return i1.compareTo(i2);
			
		}
	};
	
	
	private String convertToComplaintJsonforAdmin(List<MnComplaints> complaint) {
		if(logger.isDebugEnabled())
			logger.debug(" convertToComplaintJsonforAdmin method called:");
		StringBuffer jsonStr = new StringBuffer("[");
		for (MnComplaints userList : complaint)
		{
			jsonStr.append("{\"userName\" : \"" + userList.getUserName()+"\",");
			if(userList.getUserMailId()!=null){
				jsonStr.append("\"emailId\" : \"" + userList.getUserMailId().toLowerCase() + "\",");
			}else{
				jsonStr.append("\"emailId\" : \"-\",");
			}
			jsonStr.append("\"noteName\" : \"" + userList.getNoteName() + "\",");
			jsonStr.append("\"date\" : \"" + userList.getDate() + "\",");
			if(userList.getReportLevel().equals("description"))
				jsonStr.append("\"reportLevel\" : \"Description\",");
			else
				jsonStr.append("\"reportLevel\" : \"" + userList.getReportLevel() + "\",");
			jsonStr.append("\"listId\" : \"" + userList.getListId() + "\",");
			jsonStr.append("\"noteId\" : \"" + userList.getNoteId() + "\",");
			jsonStr.append("\"userId\" : \"" + userList.getComplaintUserId()+ "\",");
			jsonStr.append("\"commentId\" : \"" + userList.getCommentId()+ "\",");
			jsonStr.append("\"complaintId\" : \"" + userList.getCompliantId()+ "\",");
			jsonStr.append("\"mailTemplateId\" : \"" + userList.getMailTemplateId()+ "\",");
			jsonStr.append("\"adminComment\" : \"" + userList.getAdminComment()+ "\",");
			if(userList.getReportPage().equals("crowd")){
				if(userList.getStatus().contains("A")){
					jsonStr.append("\"status\" : \" Unresolved\",");
				}
				else{
					jsonStr.append("\"status\" : \" Resolved\",");
				}
			}else{
				DateFormat formatter = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
				if(userList.getStatus().contains("A")){
					try
					{
					Calendar c = Calendar.getInstance();
					c.setTime(formatter.parse(userList.getDate()));
					c.add(Calendar.DATE, 7); 
					String reportDate = formatter.format(c.getTime());
					Date todayDate=formatter.parse(formatter.format(new Date()));
					Date reportedDate=formatter.parse(reportDate);
					if(todayDate.before(reportedDate)){
						jsonStr.append("\"status\" : \" Pending\",");
					}else{
						jsonStr.append("\"status\" : \" Unresolved\",");
					}
					}catch(Exception e){
						logger.error("Exception while converting json to userDetails convertToComplaintJsonforAdmin object !", e);
					}
				}
				else{
					jsonStr.append("\"status\" : \" Resolved\",");
				}
			}
			if(!userList.getAdminAction().equals("-")){
				jsonStr.append("\"adminAction\" : \"" + userList.getAdminAction()+ "\",");
			}else{
				jsonStr.append("\"adminAction\" :  \" Open \",");
			}
			jsonStr.append("\"Description\" : \"" + userList.getUserComplaints() +"\"},");
		   

		}
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		return jsonStr.toString();
	}
	
	
	
	
	
	
	@Override
	public String getComplaintNote(String param){
		List<MnNoteDetails> noteDetail = null;
		String jsonString = null;
		if(logger.isDebugEnabled())
			logger.debug(" getComplaintNote method called");
		try {
			JSONObject jsonObject;
			jsonObject = new JSONObject(param);
			String noteId = (String) jsonObject.get("noteId");
			String listId=(String)jsonObject.get("listId");
			String userId=(String)jsonObject.get("userId");
			noteDetail = iAdminDao.getComplaintNote(listId,noteId,userId);
			if (noteDetail != null && !noteDetail.isEmpty())
			{
				jsonString = convertToComplaintNoteJsonforAdmin(noteDetail);
			}
			else
			{
				
			}
		} catch (Exception e) {
			logger.error("Exception in getComplaintNote method  : ", e);
		}
		return jsonString;

	}

	private String convertToComplaintNoteJsonforAdmin(List<MnNoteDetails>  mnNote){
		String publicUserName="";
		if(logger.isDebugEnabled())
			logger.debug(" convertToComplaintNoteJsonforAdmin method called");
			StringBuffer jsonStr = new StringBuffer("[");
			for (MnNoteDetails userList : mnNote)
			{
				if(userList.getNoteAccess().equals("public")){
					jsonStr.append("{\"noteName\" : \"" + userList.getNoteName()+"\",");
					
					/// for public user change
					String publicUserId=userList.getPublicUser();
					boolean isUserId = publicUserId.matches("[0-9]*");
					if(isUserId){
						MnUsers mnUser =iAdminDao.getUserDetailsObject(Integer.parseInt(publicUserId));
					publicUserName=mnUser.getUserFirstName()+" "+mnUser.getUserLastName();
					}else{
						publicUserName=userList.getPublicUser();
					}
					
					jsonStr.append("\"publicUser\" : \"" + publicUserName + "\",");
					jsonStr.append("\"publicDate\" : \"" + userList.getPublicDate() + "\",");
					jsonStr.append("\"tagIds\" : \"" + userList.getTag() + "\",");
					jsonStr.append("\"listId\" : \"" + userList.getListId() + "\",");
					jsonStr.append("\"noteId\" : \"" + userList.getNoteId() + "\",");
					jsonStr.append("\"dueDate\" : \"" + userList.getDueDate() + "\",");
					jsonStr.append("\"dueTime\" : \"" + userList.getDueTime() + "\",");
					jsonStr.append("\"description\" : \"" + userList.getDescription() + "\",");
					jsonStr.append("\"userId\" : \"" + userList.getUserId() +"\"},");
				}else{
					jsonStr.append("{\"noteName\" : \"" + userList.getNoteName()+"\",");
					jsonStr.append("\"dueDate\" : \"" + userList.getDueDate() + "\",");
					jsonStr.append("\"dueTime\" : \"" + userList.getDueTime() + "\",");
					jsonStr.append("\"description\" : \"" + userList.getDescription() + "\",");
					
					/// for public user change
					String publicUserId=userList.getPublicUser();
					boolean isUserId = publicUserId.matches("[0-9]*");
					if(isUserId){
						MnUsers mnUser = getUserDetailsObject(Integer.parseInt(publicUserId));
					publicUserName=mnUser.getUserFirstName()+" "+mnUser.getUserLastName();
					}else{
						publicUserName=userList.getPublicUser();
					}
					jsonStr.append("\"publicUser\" : \"" + publicUserName + "\",");
					jsonStr.append("\"publicDate\" : \"" + userList.getRemainders() + "\",");
					jsonStr.append("\"links\" : \"" + userList.getLinks() + "\",");
					jsonStr.append("\"attachFilePath\" : \"" + userList.getAttachFilePath() + "\",");
					jsonStr.append("\"comments\" : \"" + userList.getComments() + "\",");
					jsonStr.append("\"sharedDate\" : \"" + userList.getTempSharedDate()+ "\",");
					jsonStr.append("\"vote\" : \"" + userList.getVote()+ "\",");
					jsonStr.append("\"tagIds\" : \"" + userList.getTag() + "\",");
					jsonStr.append("\"listId\" : \"" + userList.getListId() + "\",");
					jsonStr.append("\"noteId\" : \"" + userList.getNoteId() + "\",");
					
					jsonStr.append("\"userId\" : \"" + userList.getUserId() +"\"},");
				}

			}
			jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
			jsonStr.append("]");
			return jsonStr.toString();
	}
	@Override
	public String getComplaintDeleteNote(String param){
		List<MnNoteDetails> noteDetail = null;
		String status = null;
		if(logger.isDebugEnabled())
			logger.debug(" getComplaintDeleteNote method called");
		try {
			JSONObject jsonObject;
			jsonObject = new JSONObject(param);
			String noteId = (String) jsonObject.get("noteId");
			String listId=(String)jsonObject.get("listId");
			String userId=(String)jsonObject.get("userId");
			String noteName=(String)jsonObject.get("noteName");
			String adminAction=(String)jsonObject.get("action");
			String adminComment=(String)jsonObject.get("adminComment");
			String actionFrom=(String)jsonObject.get("actionFrom");
			String compliantId=(String)jsonObject.get("compliantId");
			status = iAdminDao.getComplaintDeleteNote(listId,noteId,userId,noteName,adminAction,adminComment,actionFrom,compliantId);
			
		} catch (Exception e) {
			logger.error("Exception in getComplaintDeleteNote method  : ", e);
		}
		return status;

	}
	@Override
	public String fetchUserDetailsForChartView(String param) {
		if(logger.isDebugEnabled())
			logger.debug(" fetchUserDetailsForChartView method called");
		MnUsers user=new MnUsers();
		String mnUserList = null;
		try
		{
			JSONObject Jobj=new JSONObject(param);
			user.setUserLevel((String) Jobj.get("userLevel"));
			mnUserList =iAdminDao.fetchUserDetailsForChartView(user);
			
		}catch (Exception e){
			logger.error("Exception in fetchUserDetailsForChartView!", e);
		}
	
		return mnUserList;
	}
	public String deleteComments(String param){
		String status ="";
		String result ="";
		if(logger.isDebugEnabled())
			logger.debug(" admin deleteComments method called ");
		try{
			JSONObject jsonObject;
			jsonObject = new JSONObject(param);
			String noteId = (String) jsonObject.get("noteId");
			String listId=(String)jsonObject.get("listId");
			String cId=(String)jsonObject.get("cIdFromDiv");
			String adminAction=(String)jsonObject.get("action");
			String adminComment=(String)jsonObject.get("adminComment");
			String compliantId=(String)jsonObject.get("compliantId");
			if(adminAction.equalsIgnoreCase("Delete"))
			{
			status =iAdminDao.deleteComments(cId, listId, noteId,compliantId,adminComment);
			}
			/*if(cId!= null){
				result = iAdminDao.adminDeleteReportComment(cId,listId,noteId,adminAction,adminComment);
			}*/
		}catch(Exception e){
			logger.error("Exception while admin delete comments ",e);
		}
		return ""+status;
	}

	@Override
	public String deleteAttachmentInCrowd(String param,String fileName, String userId) {
		String status ="";
		String result ="";
		if(logger.isDebugEnabled())
			logger.debug(" admin deleteComments method called "+fileName);
		try{
			JSONObject jsonObject;
			jsonObject = new JSONObject(param);
			String noteId = (String) jsonObject.get("noteId");
			String listId=(String)jsonObject.get("listId");
			String adminComment=(String)jsonObject.get("adminComment");
			String noteName=(String)jsonObject.get("noteName");
			String action=(String)jsonObject.get("action");
			String compliantId=(String)jsonObject.get("compliantId");
			status =iAdminDao.deleteAttachmentInCrowd(fileName,userId,noteId,listId,noteName,adminComment,action,compliantId);
			
		}catch(Exception e){
			logger.error("Exception while admin delete comments ",e);
		}
		return status;
	}
	@Override
	public String deleteAttachmentInBothNoteCrowd(String param,String fileName, String userId) {
		String status ="";
		String result ="";
		if(logger.isDebugEnabled())
			logger.debug(" admin deleteAttachmentInBothNoteCrowd method called "+fileName);
		try{
			JSONObject jsonObject;
			jsonObject = new JSONObject(param);
			String noteId = (String) jsonObject.get("noteId");
			String listId=(String)jsonObject.get("listId");
			String adminComment=(String)jsonObject.get("adminComment");
			String noteName=(String)jsonObject.get("noteName");
			String action=(String)jsonObject.get("action");
			status =iAdminDao.deleteAttachmentInBothNoteCrowd(fileName,userId,noteId,listId,noteName,adminComment,action);
			/*if(status!= null && !status.equals("") && status==cId){
				result = iAdminDao.adminDeleteReportComment(cId,listId,noteId);
			}*/
		}catch(Exception e){
			logger.error("Exception while admin deleteAttachmentInBothNoteCrowd ",e);
		}
		return status;
	}
	@Override
	public String adminStatusReport(String param){
		List<MnNoteDetails> noteDetail = null;
		String status = null;
		if(logger.isDebugEnabled())
			logger.debug(" adminStatusReport method called");
		try {
			JSONObject jsonObject;
			jsonObject = new JSONObject(param);
			String noteId = (String) jsonObject.get("noteId");
			String listId=(String)jsonObject.get("listId");
			String userId=(String)jsonObject.get("userId");
			String noteName=(String)jsonObject.get("noteName");
			String adminAction=(String)jsonObject.get("action");
			String adminComment=(String)jsonObject.get("adminComment");
			adminComment=adminComment.replace("`*`", "\"");
			String compliantId=(String)jsonObject.get("compliantId");
			String mailTemplateId=(String)jsonObject.get("mailTemplateId");
			status = iAdminDao.adminStatusReport(listId,noteId,userId,noteName,adminAction,adminComment,compliantId,mailTemplateId);
			/*if (noteDetail != null && !noteDetail.isEmpty())
			{
				jsonString = convertToComplaintNoteJsonforAdmin(noteDetail,request);
				logger.info("user details list details as jsonString : " + jsonString);
			}
			else
				logger.info("No student details list found for this user !!!!!!");*/
		} catch (Exception e) {
			logger.error("Exception in adminStatusReport method  : ", e);
		}
		return status;

	}

	@Override
	public String addMailTemplates(String param) {
		if(logger.isDebugEnabled())
			logger.debug(" addMailTemplates method called");
		String status = "";
		try {
			JSONObject jsonObject;
			jsonObject = new JSONObject(param);
			String templateName = (String) jsonObject.get("templateName");
			String templateText = (String)jsonObject.get("templateText");
			status = iAdminDao.addMailTemplates(templateName,templateText);
		} catch (Exception e) {
			logger.error("Exception in adminStatusReport method  : ", e);
		}
		return status;
	}
	@Override
	public String updateMailTemplates(String param) {
		if(logger.isDebugEnabled())
			logger.debug(" updateMailTemplates method called");
		String status = "";
		try {
			JSONObject jsonObject;
			jsonObject = new JSONObject(param);
			String templateId = (String) jsonObject.get("templateId");
			String templateName = (String) jsonObject.get("templateName");
			String templateText = (String)jsonObject.get("templateText");
			status = iAdminDao.updateMailTemplates(templateName,templateText,templateId);
		} catch (Exception e) {
			logger.error("Exception in updateMailTemplates method  : ", e);
		}
		return status;
	}
	@Override
	public String fetchMailTemplates(String param,HttpServletRequest request) {
		if(logger.isDebugEnabled())
			logger.debug(" fetchMailTemplates method called");
		MnUsers user=new MnUsers();
		String jsonString = null;
		List<MnAdminComplaintMailTemplate> MailTemplate = null;
		try
		{
			JSONObject Jobj=new JSONObject(param);
			String userId=(String) Jobj.get("userId");
			MailTemplate =iAdminDao.fetchMailTemplates(userId);
			if (MailTemplate != null && !MailTemplate.isEmpty())
			{
				jsonString = convertToJsonMailTemplate(MailTemplate,request);

			}
		}catch (Exception e){
			logger.error("Exception in fetchMailTemplates!", e);
		}
	
		return jsonString;
	}
	private String convertToJsonMailTemplate(List<MnAdminComplaintMailTemplate> mailTemplate,HttpServletRequest request) {
		if(logger.isDebugEnabled())
			logger.debug(" convertToJsonMailTemplate method called");
		StringBuffer jsonStr = new StringBuffer("[");
		try
		{
		
		for (MnAdminComplaintMailTemplate mailTemplates : mailTemplate)
		{
			jsonStr.append("{\"templateId\" : \"" + mailTemplates.getTemplateId() + "\",");
			jsonStr.append("\"templateName\" : \"" + mailTemplates.getTemplateName() + "\",");
			jsonStr.append("\"templateStatus\" : \"" + mailTemplates.getStatus() + "\",");
		    jsonStr.append("\"templateText\" : \"" + mailTemplates.getTemplateText() + "\"},");

		}
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		}catch (Exception e){
			logger.error("Exception in convertToJsonMailTemplate!", e);
		}
		return jsonStr.toString();
		
	}
	@Override
	public String adminAddSecurityQuestions(String param) {
		if(logger.isDebugEnabled())
			logger.debug(" adminAddSecurityQuestions method called");
		String status = "";
		try {
			JSONObject jsonObject;
			jsonObject = new JSONObject(param);
			String question = (String) jsonObject.get("question");
			status = iAdminDao.adminAddSecurityQuestions(question);
		} catch (Exception e) {
			logger.error("Exception in adminAddSecurityQuestions method  : ", e);
		}
		return status;
	}
	@Override
	public String fetchsecurityQuestionView(String param,HttpServletRequest request) {
		if(logger.isDebugEnabled())
			logger.debug(" fetchsecurityQuestionView method called");
		String jsonString = null;
		List<MnAdminSecurityQuestion> SecurityQuestions = null;
		try
		{
			JSONObject Jobj=new JSONObject(param);
			String userId=(String) Jobj.get("userId");
			SecurityQuestions =iAdminDao.fetchsecurityQuestionView(userId);
			if (SecurityQuestions != null && !SecurityQuestions.isEmpty())
			{
				jsonString = convertToJsonsecurityQuestionList(SecurityQuestions,request);

			}
		}catch (Exception e){
			logger.error("Exception in fetchsecurityQuestionView!", e);
		}
	
		return jsonString;
	}
	private String convertToJsonsecurityQuestionList(List<MnAdminSecurityQuestion> SecurityQuestions,HttpServletRequest request) {
		if(logger.isDebugEnabled())
			logger.debug(" convertToJsonsecurityQuestionList method called");
		StringBuffer jsonStr = new StringBuffer("[");
		try {
		for (MnAdminSecurityQuestion SecurityQuestion : SecurityQuestions)
		{
			jsonStr.append("{\"qId\" : \"" + SecurityQuestion.getqId() + "\",");
			jsonStr.append("\"QuestionId\" : \"" + SecurityQuestion.getQuestionId() + "\",");
			jsonStr.append("\"Question\" : \"" + SecurityQuestion.getQuestion() + "\",");
		    jsonStr.append("\"Status\" : \"" + SecurityQuestion.getStatus() + "\"},");

		}
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		} catch (Exception e) {
			logger.error("Exception in convertToJsonsecurityQuestionList method  : ", e);
		}
		return jsonStr.toString();
		
	}
	@Override
	public String updateSecurityQuestions(String param) {
		if(logger.isDebugEnabled())
			logger.debug(" updateSecurityQuestions method called");
		String status = "";
		try {
			JSONObject jsonObject;
			jsonObject = new JSONObject(param);
			String qId = (String) jsonObject.get("qId");
			String QuestionId = (String) jsonObject.get("QuestionId");
			String question = (String)jsonObject.get("question");
			status = iAdminDao.updateSecurityQuestions(qId,QuestionId,question);
		} catch (Exception e) {
			logger.error("Exception in updateSecurityQuestions method  : ", e);
		}
		return status;
	}

	@Override
	public String FetchComplaintReports(String option) {
		if(logger.isDebugEnabled())
			logger.debug(" FetchComplaintReports method called");
		String jsonString = null;
		//List<MnComplaints> mnComplaint = null;
		List<MnComplaintAction> mnComplaintAction=null;
		List<MnUsers> mnUsersList=new ArrayList<MnUsers>();
		
		try
		{
			JSONObject jsonObject;
			jsonObject = new JSONObject(option);
			if (jsonObject != null )
			{
				option = (String)jsonObject.get("selectBlockOption");
			}
			if(option.equals("all"))
			{
				mnComplaintAction=iAdminDao.fetchComplaints();
				if (mnComplaintAction != null && !mnComplaintAction.isEmpty())
				{
					List<Integer> list=new ArrayList<Integer>();
					for(MnComplaintAction mnAction:mnComplaintAction)
					{
						if(mnAction.getUserId()!=null)
						{
						list.add(mnAction.getUserId());	
						}
					}
					Set<Integer> uniqueSet = new HashSet<Integer>(list);
					for (Integer uniqueUserId : uniqueSet)
					{
						int userId=uniqueUserId;
						MnUsers mnUsers=iAdminDao.fetchUserForComplaint(userId);
						mnUsers.setComplaintCount(Collections.frequency(list, uniqueUserId));
						MnBlockedUsers mnBlockedUsersList=iAdminDao.fetchBlockedUserList(userId);
						if(mnBlockedUsersList!=null)
						{
							if(mnBlockedUsersList.getBlockedLevel().equals("Crowd"))
							{
								mnUsers.setBlockedDate(mnBlockedUsersList.getBlockedDate());
								mnUsers.setBlockedStatus("on");
								mnUsers.setBlockedDays("OFF");
							}
							if(mnBlockedUsersList.getBlockedLevel().equals("All"))
							{
								mnUsers.setBlockedDate(mnBlockedUsersList.getBlockedDate());
								mnUsers.setBlockedStatus("off");
								mnUsers.setBlockedDays(mnBlockedUsersList.getNumberOfDays());
							}
						}
						else
						{
							mnUsers.setBlockedStatus("off");
							mnUsers.setBlockedDays("OFF");
							mnUsers.setBlockedDate("-");
						}
						mnUsersList.add(mnUsers);
					}
				}
			}
			else if(option.equals("unBlocked"))
			{
				mnComplaintAction=iAdminDao.fetchComplaints();
				if (mnComplaintAction != null && !mnComplaintAction.isEmpty())
				{
					List<Integer> list=new ArrayList<Integer>();
					for(MnComplaintAction mnAction:mnComplaintAction)
					{
						if(mnAction.getUserId()!=null)
						{
						list.add(mnAction.getUserId());	
						}				
					}
					Set<Integer> uniqueSet = new HashSet<Integer>(list);
					for (Integer uniqueUserId : uniqueSet)
					{
						int userId=uniqueUserId;
						MnUsers mnUsers=iAdminDao.fetchUserForComplaint(userId);
						mnUsers.setComplaintCount(Collections.frequency(list, uniqueUserId));
						MnBlockedUsers mnBlockedUsersList=iAdminDao.fetchBlockedUserList(userId);
						if(mnBlockedUsersList==null)
						{							
							mnUsers.setBlockedStatus("off");
							mnUsers.setBlockedDays("OFF");
							mnUsers.setBlockedDate("-");
							mnUsersList.add(mnUsers);
						}
					}
				}
			}
			else if(option.equals("blocked"))
			{
				mnComplaintAction=iAdminDao.fetchComplaints();
				if (mnComplaintAction != null && !mnComplaintAction.isEmpty())
				{
					List<Integer> list=new ArrayList<Integer>();
					for(MnComplaintAction mnAction:mnComplaintAction)
					{
						if(mnAction.getUserId()!=null)
						{
						list.add(mnAction.getUserId());	
						}					
					}
					Set<Integer> uniqueSet = new HashSet<Integer>(list);
					for (Integer uniqueUserId : uniqueSet)
					{
						int userId=uniqueUserId;
						MnUsers mnUsers=iAdminDao.fetchUserForComplaint(userId);
						mnUsers.setComplaintCount(Collections.frequency(list, uniqueUserId));
						MnBlockedUsers mnBlockedUsersList=iAdminDao.fetchBlockedUserList(userId);
						if(mnBlockedUsersList!=null)
						{	
							if(mnBlockedUsersList.getStatus().equals("A"))
							{
								if(mnBlockedUsersList.getBlockedLevel().equals("Crowd"))
								{
									mnUsers.setBlockedDate(mnBlockedUsersList.getBlockedDate());
									mnUsers.setBlockedStatus("on");
									mnUsers.setBlockedDays("OFF");
								}
								if(mnBlockedUsersList.getBlockedLevel().equals("All"))
								{
									mnUsers.setBlockedDate(mnBlockedUsersList.getBlockedDate());
									mnUsers.setBlockedStatus("off");
									mnUsers.setBlockedDays(mnBlockedUsersList.getNumberOfDays());
								}
								mnUsersList.add(mnUsers);
							}
						}
					}
				}	
			}
			jsonString = convertToComplaintReportJsonforAdmin(mnUsersList);
		} catch (Exception e) {
			logger.error("Exception in FetchComplaintReports method  : ", e);
		}
		return jsonString;
	}
	private String convertToComplaintReportJsonforAdmin(List<MnUsers> mnUsersList) {

		if(logger.isDebugEnabled())
			logger.debug(" convertToComplaintReportJsonforAdmin method called:");
		StringBuffer jsonStr = new StringBuffer("[");
		for (MnUsers userList : mnUsersList)
		{
			jsonStr.append("{\"userName\" : \"" + userList.getUserName()+"\",");
			if(userList.getEmailId()!=null){
				jsonStr.append("\"emailId\" : \"" + userList.getEmailId().toLowerCase() + "\",");
			}else{
				jsonStr.append("\"emailId\" : \"-\",");
			}	
			jsonStr.append("\"noOfComplaints\" : \"" + userList.getComplaintCount() + "\",");
			jsonStr.append("\"ContactNumber\" : \"" + userList.getContactNumber() + "\",");
			jsonStr.append("\"blockCrowdShare\" : \"" + userList.getBlockedStatus() + "\",");
			jsonStr.append("\"blockingUser\" : \"" + userList.getBlockedDays() + "\",");
			jsonStr.append("\"blockedDate\" : \"" + userList.getBlockedDate() + "\",");
			jsonStr.append("\"userId\" : \"" + userList.getUserId()+ "\"},");
			
		}
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		return jsonStr.toString();
	}
//inserting Blocked user 
	@Override
	public String insertBlockedUser(MnBlockedUsers mnBlockedUsers) {
		if(logger.isDebugEnabled())
			logger.debug(" insertBlockedUser method called: ");	
			String status = "";
			Date blockedDate=new Date();
			Date endBlockedDate=null;
			Calendar calendar=null;
			String pattern = "dd MMM yyyy HH:mm:ss a";
		    SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
			try
			{
				mnBlockedUsers.setBlockedDate(dateFormat.format(blockedDate));
				if(mnBlockedUsers.getNumberOfDays().equals("10"))
				{
					calendar = Calendar.getInstance();
					calendar.setTime(blockedDate); 
					calendar.add(Calendar.DATE, 10); 
					endBlockedDate=calendar.getTime();
					mnBlockedUsers.setEndBlockedDate(dateFormat.format(endBlockedDate));
				}
				else if(mnBlockedUsers.getNumberOfDays().equals("20"))
				{
					calendar = Calendar.getInstance();
					calendar.setTime(blockedDate); 
					calendar.add(Calendar.DATE, 20);
					endBlockedDate=calendar.getTime();
					mnBlockedUsers.setEndBlockedDate(dateFormat.format(endBlockedDate));
				}
				else
				{
					mnBlockedUsers.setEndBlockedDate("-");
				}
				status=iAdminDao.insertBlockedUser(mnBlockedUsers);
			}
			catch (Exception e)
			{
				logger.error("Exception while inserting Blocked user!", e);
			}
			if(logger.isDebugEnabled())
			logger.debug("Return response for insertBlockedUser method : ");
			
			return status;
}
//converting Json to object
	@Override
	public MnBlockedUsers convertJsonToUserObj(String jsonStr) {
		if(logger.isDebugEnabled())
			logger.debug("while creating a convertJsonToUserObj method : ");
		MnBlockedUsers mnBlockedUsers = new MnBlockedUsers();
		try
		{
			JSONObject jsonObject;
			jsonObject = new JSONObject(jsonStr);
			if (jsonObject != null )
			{
				
				String userName = (String) jsonObject.get("userName");
				String selectBlockStatus=(String)jsonObject.get("selectBlockStatus");
				int userId=(Integer)jsonObject.getInt("userId");
				int adminId=(Integer)jsonObject.getInt("adminId");
				mnBlockedUsers.setUserName(userName);
				mnBlockedUsers.setUserId(userId);
				
				if(selectBlockStatus.equals("on"))
				{
					mnBlockedUsers.setAdminId(adminId);
					mnBlockedUsers.setBlockedLevel("Crowd");
					mnBlockedUsers.setCrowdShareFlag("True");
					mnBlockedUsers.setStatus("A");
					mnBlockedUsers.setNumberOfDays("-");
				}
				else if(selectBlockStatus.equals("off"))
				{
					mnBlockedUsers.setCrowdShareFlag("False");
				}
				else if(selectBlockStatus.equals("OFF"))
				{
					mnBlockedUsers.setNumberOfDays("OFF");
					mnBlockedUsers.setStatus("I");
				}
				else 
				{
					mnBlockedUsers.setCrowdShareFlag("-");
					mnBlockedUsers.setStatus("A");
					mnBlockedUsers.setNumberOfDays(selectBlockStatus);
					mnBlockedUsers.setBlockedLevel("All");
					mnBlockedUsers.setAdminId(adminId);
				}
			}
		}
		catch (Exception e)
		{
			logger.error("Exception while converting json to user object!", e);
		}
		if(logger.isDebugEnabled())
		logger.debug("Return response for convertJsonToUserObj method : ");
		return mnBlockedUsers;
	}

	@Override
	public String fetchBlockedUser(MnBlockedUsers mnBlockedUsers) {
		if(logger.isDebugEnabled())
			logger.debug("while fetching blocked user method : ");
		MnBlockedUsers mnBlockedUsersFromDB=null;
		String status="";
		try
		{		
			mnBlockedUsersFromDB=iAdminDao.fetchBlockedUserList(mnBlockedUsers.getUserId());
			if(mnBlockedUsersFromDB!=null)
			{
				Date date=new Date();
				String pattern = "dd MMM yyyy HH:mm:ss a";
			    SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
			    Date endBlockedDate=null;
				Calendar calendar=null;
				if(mnBlockedUsersFromDB.getBlockedLevel().equals("All"))
				{
					if(mnBlockedUsers.getNumberOfDays().equals("OFF"))
					{
						mnBlockedUsersFromDB.setStatus(mnBlockedUsers.getStatus());
						status=updateBlockedUser(mnBlockedUsersFromDB);
					}
					else if(mnBlockedUsers.getNumberOfDays().equals("fullyBlocked"))
					{
						mnBlockedUsersFromDB.setEndBlockedDate("-");
						mnBlockedUsersFromDB.setBlockedDate(dateFormat.format(date));
					    mnBlockedUsersFromDB.setNumberOfDays(mnBlockedUsers.getNumberOfDays());
						status =iAdminDao.updateBlockedUser(mnBlockedUsersFromDB);
					}
					else if(mnBlockedUsers.getNumberOfDays().equals("10"))
					{
						calendar = Calendar.getInstance();
						calendar.setTime(date); 
						calendar.add(Calendar.DATE, 10); 
						endBlockedDate=calendar.getTime();
						mnBlockedUsersFromDB.setEndBlockedDate(dateFormat.format(endBlockedDate));
						mnBlockedUsersFromDB.setNumberOfDays(mnBlockedUsers.getNumberOfDays());
						mnBlockedUsersFromDB.setBlockedDate(dateFormat.format(date));
						status=iAdminDao.updateBlockedUser(mnBlockedUsersFromDB);
					}
					else if(mnBlockedUsers.getNumberOfDays().equals("20"))
					{
						calendar = Calendar.getInstance();
						calendar.setTime(date); 
						calendar.add(Calendar.DATE, 20); 
						endBlockedDate=calendar.getTime();
						mnBlockedUsersFromDB.setEndBlockedDate(dateFormat.format(endBlockedDate));
						mnBlockedUsersFromDB.setNumberOfDays(mnBlockedUsers.getNumberOfDays());
						mnBlockedUsersFromDB.setBlockedDate(dateFormat.format(date));
						status=iAdminDao.updateBlockedUser(mnBlockedUsersFromDB);
					}
				}
				else if(mnBlockedUsersFromDB.getBlockedLevel().equals("Crowd"))
				{
					if(mnBlockedUsers.getCrowdShareFlag().equals("False"))
					{
						mnBlockedUsersFromDB.setStatus("I");
						mnBlockedUsersFromDB.setCrowdShareFlag("False");
						status=updateBlockedUser(mnBlockedUsersFromDB);
					}
					else if(mnBlockedUsers.getNumberOfDays().equals("10"))
					{
						calendar = Calendar.getInstance();
						calendar.setTime(date); 
						calendar.add(Calendar.DATE, 10); 
						endBlockedDate=calendar.getTime();
						mnBlockedUsersFromDB.setBlockedLevel("All");
						mnBlockedUsersFromDB.setCrowdShareFlag("-");
						mnBlockedUsersFromDB.setEndBlockedDate(dateFormat.format(endBlockedDate));
						mnBlockedUsersFromDB.setNumberOfDays(mnBlockedUsers.getNumberOfDays());
						mnBlockedUsersFromDB.setBlockedDate(dateFormat.format(date));
						status=iAdminDao.updateBlockedUser(mnBlockedUsersFromDB);
					}
					else if(mnBlockedUsers.getNumberOfDays().equals("20"))
					{
						calendar = Calendar.getInstance();
						calendar.setTime(date); 
						calendar.add(Calendar.DATE, 20); 
						endBlockedDate=calendar.getTime();
						mnBlockedUsersFromDB.setBlockedLevel("All");
						mnBlockedUsersFromDB.setCrowdShareFlag("-");
						mnBlockedUsersFromDB.setEndBlockedDate(dateFormat.format(endBlockedDate));
						mnBlockedUsersFromDB.setNumberOfDays(mnBlockedUsers.getNumberOfDays());
						mnBlockedUsersFromDB.setBlockedDate(dateFormat.format(date));
						status=updateBlockedUser(mnBlockedUsersFromDB);
					}
					else if(mnBlockedUsers.getNumberOfDays().equals("fullyBlocked"))
					{
						mnBlockedUsersFromDB.setBlockedLevel("All");
						mnBlockedUsersFromDB.setCrowdShareFlag("-");
						mnBlockedUsersFromDB.setNumberOfDays(mnBlockedUsers.getNumberOfDays());
						mnBlockedUsersFromDB.setBlockedDate(dateFormat.format(date));
						mnBlockedUsersFromDB.setEndBlockedDate("-");
						status=updateBlockedUser(mnBlockedUsersFromDB);
					}
					
				}
			}
			else
			{
				status=insertBlockedUser(mnBlockedUsers);
			}
		}
		catch(Exception e)
		{
			logger.error("Exception while fetching Blocked User!", e);
		}
		return status;
	}

	@Override
	public String updateBlockedUser(MnBlockedUsers mnBlockedUsers) {
		if(logger.isDebugEnabled())
			logger.debug("while creating a updateBlockedUser method called: ");	
			String status = "";
			Date date=new Date();
			String pattern = "dd MMM yyyy HH:mm:ss a";
		    SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
			try
			{
				if(mnBlockedUsers.getStatus().equals("I"))
				{
					mnBlockedUsers.setEndBlockedDate(dateFormat.format(date));
				}
				status=iAdminDao.updateBlockedUser(mnBlockedUsers);
			}
			catch (Exception e)
			{
				logger.error("Exception while Updating Blocked User Detailes!", e);
			}
			if(logger.isDebugEnabled())
			logger.debug("Return response for updateBlockedUser method : ");
			
			return status;
	}
	//getMnUserDetails for Admin AddPrefrences
	@Override
	public String getMnUserDetails() 
	{
		if(logger.isDebugEnabled())
			logger.debug(" getMnUserDetails method called");
		List<MnUsers> mnUser = null;
		StringBuffer jsonStr = new StringBuffer("[");
		try
		{
			mnUser=iAdminDao.getMnUserDetails();
			if(mnUser!=null)
			{
				for(MnUsers mnUsers:mnUser)
				{
					jsonStr.append("{\"Name\" : \"" + mnUsers.getUserName() + "\",");
					jsonStr.append("\"UserId\" : \"" + mnUsers.getUserId() + "\",");
					jsonStr.append("\"MailId\" : \"" + mnUsers.getEmailId() + "\",");
					jsonStr.append("\"Level\" : \"" + mnUsers.getUserLevel() + "\",");
					jsonStr.append("\"Status\" : \"" + mnUsers.getStatus() + "\",");
					jsonStr.append("\"Role\" : \"" + mnUsers.getUserRole() + "\"},");
				}
				jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
				jsonStr.append("]");
			}
			else
			{
				jsonStr.append("no user Details");
			}
		}
		catch (Exception e) {
			logger.error("Exception in getMnUserDetails method  : ", e);
		}
		return jsonStr.toString();
	}
	//Admin add prefrenceflag on MnUserTable
	@Override
	public String addPrefrenceFlag(String userId,String userFlag) 
	{
		String status="";
		try
		{
			status=iAdminDao.addPrefrenceFlag(userId, userFlag);
		}
		catch (Exception e) {
			logger.error("Exception in addPrefrenceFlag method  : ", e);
		}
		return status;
	}

	@Override
	public String getUploadedFilesForAdmin() {
		if(logger.isDebugEnabled())
			logger.debug(" getUploadedfiles method called");
		List<MnAdminFiles> mnAdminFileList=null;
		StringBuffer jsonStr = new StringBuffer("[");
		StringBuffer jsonNull = new StringBuffer("failed");
		try
		{
			mnAdminFileList=iAdminDao.getMnAdminFilesDetails();
			if(!mnAdminFileList.isEmpty() && mnAdminFileList != null)
			{
				for(MnAdminFiles mnAdminFiles:mnAdminFileList)
				{
					jsonStr.append("{\"fileName\" : \"" + mnAdminFiles.getAdminFileName() + "\",");
					jsonStr.append("\"fileId\" : \"" + mnAdminFiles.getAdminFileId() + "\",");
					jsonStr.append("\"fileType\" : \"" + mnAdminFiles.getAdminFileType() + "\",");
					jsonStr.append("\"fileTitle\" : \"" + mnAdminFiles.getTitle() + "\",");
					jsonStr.append("\"status\" : \"" + mnAdminFiles.getStatus() + "\",");
					jsonStr.append("\"description\" : \"" + mnAdminFiles.getDiscription() + "\",");
					jsonStr.append("\"uploadDate\" : \"" + mnAdminFiles.getUploadDate() + "\",");
					jsonStr.append("\"deleteDate\" : \"" + mnAdminFiles.getDeleteDate() + "\",");
					jsonStr.append("\"filePath\" : \"" + mnAdminFiles.getAdminFilePath() + "\"},");
				}
				jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
				jsonStr.append("]");
			}
			else
			{
				jsonStr=jsonNull;
			}
		}
		catch (Exception e) {
			logger.error("Exception in fetching uploded method : ", e);
		}
		return jsonStr.toString();
	}

	@Override
	public String getUploadedFilesForHelp() {
		if(logger.isDebugEnabled())
			logger.debug(" getUploadedFilesForHelp method called");
		List<MnAdminFiles> mnAdminFileList=null;
		StringBuffer jsonStr = new StringBuffer("[");
		StringBuffer jsonNull = new StringBuffer("failed");
		try
		{
			mnAdminFileList=iAdminDao.getUploadedFilesForHelp();
			if(!mnAdminFileList.isEmpty() && mnAdminFileList != null)
			{
				for(MnAdminFiles mnAdminFiles:mnAdminFileList)
				{
					jsonStr.append("{\"fileName\" : \"" + mnAdminFiles.getAdminFileName() + "\",");
					jsonStr.append("\"fileId\" : \"" + mnAdminFiles.getAdminFileId() + "\",");
					jsonStr.append("\"fileType\" : \"" + mnAdminFiles.getAdminFileType() + "\",");
					jsonStr.append("\"fileTitle\" : \"" + mnAdminFiles.getTitle() + "\",");
					jsonStr.append("\"status\" : \"" + mnAdminFiles.getStatus() + "\",");
					jsonStr.append("\"description\" : \"" + mnAdminFiles.getDiscription() + "\",");
					jsonStr.append("\"uploadDate\" : \"" + mnAdminFiles.getUploadDate() + "\",");
					jsonStr.append("\"deleteDate\" : \"" + mnAdminFiles.getDeleteDate() + "\",");
					jsonStr.append("\"filePath\" : \"" + mnAdminFiles.getAdminFilePath() + "\"},");
				}
				jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
				jsonStr.append("]");
			}
			else
			{
				jsonStr=jsonNull;
			}
		}
		catch (Exception e) {
			logger.error("Exception in fetching getUploadedFilesForHelp method : ", e);
		}
		return jsonStr.toString();
	}

	@Override
	public String deleteUploadedFile(String fileId) {
		Date date=new Date();
		String pattern = "dd MMM yyyy HH:mm:ss a";
	    SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
	    String status="";
	    MnAdminFiles mnAdminFiles=new MnAdminFiles();
	    try
	    {
			mnAdminFiles.setAdminFileId(Integer.parseInt(fileId));
			mnAdminFiles.setDeleteDate(dateFormat.format(date));
			status=iAdminDao.deleteUploadedFile(mnAdminFiles);
		} 
	    catch (Exception e)
	    {
			logger.error("Exception in Delete uploded file method : ", e);
		}
		return status;
	}

	@Override
	public String convertJsonToStringForfileDetails(String param)
	{
		if(logger.isDebugEnabled())
			logger.debug(" convertJsonToStringForfileDetails method called");
		String status = "";
		try
		{
			JSONObject jsonObject;
			jsonObject = new JSONObject(param);
			String name = (String) jsonObject.get("name");
			String type = (String) jsonObject.get("type");
			String path = (String)jsonObject.get("path");
			String title = (String)jsonObject.get("title");
			String description = (String)jsonObject.get("description");
			status =insertAdminFileDetails(name,type,path,title,description);
		}
		catch (Exception e)
		{
			logger.error("Exception in convertJsonToStringForfileDetails method  : ", e);
		}
		return status;
	}
	public String insertAdminFileDetails(String name, String type,
			String filePath, String title, String discription) {
		if(logger.isDebugEnabled())
			logger.debug(" getUploadedfiles method called");
		MnAdminFiles mnAdminFiles=new MnAdminFiles();
		Date uploadDate=new Date();
		String pattern = "dd MMM yyyy HH:mm:ss a";
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		String status="";
		try
		{
			mnAdminFiles.setAdminFileName(name);
			mnAdminFiles.setAdminFilePath(filePath);
			mnAdminFiles.setAdminFileType(type);
			mnAdminFiles.setDiscription(discription);
			mnAdminFiles.setTitle(title);
			mnAdminFiles.setStatus("A");
			mnAdminFiles.setDeleteDate("-");
			mnAdminFiles.setUploadDate(dateFormat.format(uploadDate));
			status=iAdminDao.insertFileDetailsForAdmin(mnAdminFiles);
		}
		catch(Exception e)
		{
			logger.error("Exception in uploded method : ", e);
		}
		return status;
	}


}
