package com.musicnotes.apis.resources;

import javax.servlet.http.HttpServletRequest;

import com.musicnotes.apis.dao.interfaces.IFilterDao;
import com.musicnotes.apis.interfaces.MnFilterable;

public class MnFilter implements MnFilterable {

	IFilterDao iFilterDao;

	@Override
	public boolean isValidToken(String token, String date,
			HttpServletRequest request) {
		boolean status = iFilterDao.isValidToken(token, date, request);

		return status;
	}

	@Override
	public boolean isValidTokenForUpload(String token, String date) {
		boolean status = iFilterDao.isValidTokenForUpload(token, date);
		return status;
	}

	public IFilterDao getiFilterDao() {
		return iFilterDao;
	}

	public void setiFilterDao(IFilterDao iFilterDao) {
		this.iFilterDao = iFilterDao;
	}

	@Override
	public boolean isValidTokenForUser(String token, String date,HttpServletRequest request) {
		boolean status = iFilterDao.isValidTokenForUser(token, date, request);
	return status;
	}

	@Override
	public boolean isValidTokenForNewGenerate(String token, String date,HttpServletRequest request) 
	{
		boolean status = iFilterDao.isValidTokenForNewGenerate(token, date, request);
		return status;
	}

}
