package com.musicnotes.apis.resources;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.musicnotes.apis.dao.interfaces.IFriendDao;
import com.musicnotes.apis.dao.interfaces.IGroupDao;
import com.musicnotes.apis.domain.MnFriends;
import com.musicnotes.apis.domain.MnGroupDetailsDomain;
import com.musicnotes.apis.domain.MnGroupDomain;
import com.musicnotes.apis.domain.MnInvitedUsers;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.interfaces.MnGroupable;
import com.musicnotes.apis.util.MailContent;
import com.musicnotes.apis.util.SendMail;

public class MnGroup implements MnGroupable {

	IGroupDao iGroupDao;
	IFriendDao iFriendDao;
	Logger logger = Logger.getLogger(MnGroup.class);
	
	String criteria;

	public MnGroupDomain convertJsonToMnGroupDomainObj(String jsonStr) {
		MnGroupDomain mnGroupDomain = new MnGroupDomain();
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				if(logger.isDebugEnabled()){logger.debug("convertJsonToMnGroupDomainObj method called");}
				jsonObject = new JSONObject(jsonStr);
				Integer groupId=Integer.parseInt((String) jsonObject.get("groupId"));
				mnGroupDomain.setGroupId(groupId);
				mnGroupDomain.setLoginUserId((String) jsonObject.get("loginUserId"));
				mnGroupDomain.setGroupName((String) jsonObject.get("groupName"));
				String crieteria=(String)jsonObject.get("criteria");
				if(crieteria!=null)
				{
					this.setCriteria((String)jsonObject.get("criteria"));
				}
				if(logger.isDebugEnabled()){logger.debug("converted json to mnGroup domain object");}

			}

			catch (Exception e) {
				logger.error(
						"Exception while converting json to group domain object !", e);

			}
		}
		return mnGroupDomain;
	}

	
	public MnGroupDomain convertJsonToMnGroupDomainDetailsObj(String jsonStr) {
		MnGroupDomain mnGroupDomain = new MnGroupDomain();
		MnGroupDetailsDomain mnGroupDetailsDomain=new MnGroupDetailsDomain();
		
		
		Set<String> mnGroupDetails=new HashSet<String>();
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {

				if(logger.isDebugEnabled()){logger.debug("convertJsonToMnGroupDomainDetailsObj method called");}
				jsonObject = new JSONObject(jsonStr);
				Integer groupId=Integer.parseInt((String) jsonObject.get("groupId"));
				mnGroupDomain.setGroupId(groupId);
				mnGroupDomain.setLoginUserId((String) jsonObject.get("loginUserId"));
				mnGroupDomain.setGroupName((String) jsonObject.get("groupName"));
				
				String crieteria=(String)jsonObject.get("criteria");
				if(crieteria!=null)
				{
					this.setCriteria((String)jsonObject.get("criteria"));
				}
				
				JSONArray groupDetails = jsonObject
						.getJSONArray("groupDetails");
				
				String parseString=groupDetails.toString();
				
				parseString=parseString.replace("[","");
				parseString=parseString.replace("]","");
				
				jsonObject=new JSONObject(parseString);
			
						mnGroupDetailsDomain.setStartDate((String)jsonObject.get("startDate"));
					
						mnGroupDetailsDomain.setEndDate((String)jsonObject.get("endDate"));
					
						mnGroupDetailsDomain.setStatus((String)jsonObject.get("status"));
					
						mnGroupDetailsDomain.setUserId(Integer.parseInt((String)jsonObject.get("userId")));
					
				
				mnGroupDetails.add(parseString);
				
				
				mnGroupDomain.setMnGroupDetailsDomain(mnGroupDetailsDomain);
				if(logger.isDebugEnabled()){logger.debug("Converted json to MnGroupDomainDetails object");}

			}

			catch (Exception e) {
				logger.error(
						"Exception while converting json to group domain object !", e);

			}
		}
		return mnGroupDomain;
	}
	
	
	
	public IGroupDao getiGroupDao() {
		return iGroupDao;
	}

	public void setiGroupDao(IGroupDao iGroupDao) {
		this.iGroupDao = iGroupDao;
	}

	@Override
	public String createGroup(MnGroupDomain mnGroupDomain) {
		String status = "";
		try {
			if(logger.isDebugEnabled()){logger.debug("createGroup method called");}
			status = iGroupDao.createGroup(mnGroupDomain);
			
		} catch (Exception e) {
			logger.error("Exception in createGroup method  : ", e);

		}
		if(logger.isDebugEnabled()){logger.debug("created group");}	
		return status;
	}

	@Override
	public String updateGroup(MnGroupDomain mnGroupDomain) {
		String status = "";
		try {
			if(logger.isDebugEnabled()){logger.debug("updateGroup method called");}
			status = iGroupDao.updateGroup(mnGroupDomain);
			if(logger.isDebugEnabled()){logger.debug("Group updated");}
		} catch (Exception e) {
			logger.error("Exception in updateGroup method  : ", e);

		}
		return status;
	}
	
	
	@Override
	public String updateGroupDetails(MnGroupDomain mnGroupDomain) {
		String status = "";
		try {
			if(logger.isDebugEnabled()){logger.debug("updateGroupDetails method called");}
			status = iGroupDao.updateGroupDetails(mnGroupDomain);
			if(logger.isDebugEnabled()){logger.debug("updated group details successfully");}
		} catch (Exception e) {
			logger.error("Exception in updateGroupDetails method  : ", e);

		}
		return status;

	}

	@Override
	public String deleteGroup(MnGroupDomain mnGroupDomain) {
		String status = "";
		try {
			if(logger.isDebugEnabled()){logger.debug("deleteGroup method called");}
			status = iGroupDao.deleteGroup(mnGroupDomain);
			if(logger.isDebugEnabled()){logger.debug("Deleted group");}
		} catch (Exception e) {
			logger.error("Exception in insertNewUser method  : ", e);

		}
		return status;
	}

	@Override
	public String fetchGroupInfo(String criteria,String id) {
		List<MnGroupDomain> mnGroupsDomains = null;
		String groupJsonString=null;
		try {
			if(logger.isDebugEnabled()){logger.debug("fetchGroupinfo method called");}
			mnGroupsDomains = iGroupDao.SearchGroup(criteria, id);
			if(mnGroupsDomains!=null && !mnGroupsDomains.isEmpty())
			groupJsonString=convertMnGroupDomainObjToJson(mnGroupsDomains);
			if(logger.isDebugEnabled()){logger.debug("Fetched group information");}
			
		} catch (Exception e) {
			logger.error("Exception in fetchGroupInfo method  : ", e);

		}
		return groupJsonString;

	}

	
	private String convertMnGroupDomainObjToJson(List<MnGroupDomain> mnGroupDomains) {

		StringBuffer jsonStr = new StringBuffer("[");
		try{
		if(logger.isDebugEnabled()){logger.debug("convertMnGroupDomainObjToJson method called");}
		for (MnGroupDomain groupDomain : mnGroupDomains)
		{
			jsonStr.append("{\"_id\" : \"" + groupDomain.get_id() + "\",");
			jsonStr.append("\"groupId\" : \"" + groupDomain.getGroupId()+ "\",");
			jsonStr.append("\"loginUserId\" : \"" + groupDomain.getLoginUserId() + "\",");
			jsonStr.append("\"groupName\" : \"" + groupDomain.getGroupName() + "\",");
			jsonStr.append("\"defaultGroup\" : \"" + groupDomain.getDefaultGroup() + "\",");
			jsonStr.append("\"groupDetails\" : " + groupDomain.getGroupDetails() + "},");
			
			if(logger.isDebugEnabled()){logger.debug("jsonStr while convertToJson group details--->>Json{}" );}

		}
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		}catch(Exception e){
			logger.error("Exception in convertMnGroupDomainObjToJson method"+e);
		}
		return jsonStr.toString();
	}

	public String getCriteria() {
		return criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	
	//Added by r.veeraprathaban for Group details
	
    public MnGroupDetailsDomain convertJsonToMnGroupDomainDetailsObject(String jsonStr){
    	MnGroupDetailsDomain detailsDomain=new MnGroupDetailsDomain();
    	JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(jsonStr);
			if(logger.isDebugEnabled()){logger.debug("convertJsonToMnGroupDomainDetailsObject method called");}
			Integer groupId=Integer.parseInt((String) jsonObject.get("groupId"));
			Integer userId=Integer.parseInt((String)jsonObject.get("groupUserId"));
			Integer ownerId=Integer.parseInt((String)jsonObject.get("userId"));
			
			detailsDomain.setGroupId(groupId);
			detailsDomain.setUserId(userId);
			detailsDomain.setOwnerId(ownerId);
			
			if(logger.isDebugEnabled()){logger.debug("converted json to MnGroupDomainDetails object");}
			
		}catch(Exception e){
			logger.error("Exception in convertJsonToMnGroupDomainDetailsObject method"+e);
		}
    	
    	return detailsDomain;
    }


	@Override
	public String addGroupDetails(MnGroupDetailsDomain domain)
	{
		String status=null;
		
		try{
			if(logger.isDebugEnabled()){logger.debug("addGroupDetails method called");}
			status=iGroupDao.addGroupDetails(domain);
			if(logger.isDebugEnabled()){logger.debug("Added group details");}
			
		}catch(Exception e){
			logger.error("Error while adding group details"+e);
		}
		
		return status;
	}


	@Override
	public String updateGroupDetails(MnGroupDetailsDomain domain)
	{
		String status=null;
		
		try{
			if(logger.isDebugEnabled()){logger.debug("updateGroupDetails method called");}
			status=iGroupDao.updateGroupDetails(domain);
			if(logger.isDebugEnabled()){logger.debug("Group Details updated");}
			
		}catch(Exception e){
			logger.error("Error while updating group details"+e);
			e.printStackTrace();
		}
		
		return status;
	}
	

	@Override
	public String getGroupMembers(String params) {
		String result=null;
		String groupIds=null;
		StringBuffer jsonStr=null;
		try {
			if(logger.isDebugEnabled()){logger.debug("getGroupMembers method called");}
			JSONObject jsonObject;
			jsonObject = new JSONObject(params);
			groupIds=(String) jsonObject.get("groups");
			
			
			String [] group=groupIds.split(",");
			result = iGroupDao.getGroupMembers(group);
			jsonStr = new StringBuffer("{");
				jsonStr.append("\"groupId\" : \"" + result + "\"}");
				
			if(logger.isDebugEnabled()){logger.debug("Group members fetched");}
		} catch (Exception e) {
			logger.error("Exception in getGroupMembers method  : "+ e);

		}
		return jsonStr.toString();
	}


	@Override
	public String getGroupSearch(String params) {
		String result=null;
		String searchText=null;
		Integer userId;
		try {
			if(logger.isDebugEnabled()){logger.debug("getGroupSearch method called");}
			JSONObject jsonObject;
			jsonObject = new JSONObject(params);
			searchText=(String) jsonObject.get("Searching");
			userId=Integer.parseInt((String) jsonObject.get("userId"));
			result = iGroupDao.getGroupSearch(searchText,userId);
			if(logger.isDebugEnabled()){logger.debug("Got Searched group");}
			
		} catch (Exception e) {
			logger.error("Exception in getGroupSearch method  : "+ e);

		}
		return result;
	}


	@Override
	public String getContactSearch(String params,HttpServletRequest request) {
		String result=null;
		String searchText=null;
	    MnUsers mnUsers=null;
		try {
			if(logger.isDebugEnabled()){logger.debug("getContactSearch method called");}
			JSONObject jsonObject;
			jsonObject = new JSONObject(params);
			searchText=(String) jsonObject.get("Searching");
			mnUsers = iGroupDao.getContactSearch(searchText);
		if(mnUsers!=null)
		{
			result=convertToMnUsersJson(mnUsers,request);
			
		}
			if(logger.isDebugEnabled()){logger.debug("Got contacts based on search");}
			
		} catch (Exception e) {
			logger.error("Exception in getContactsSearch method  : "+ e);

		}
		return result;
	}

	private String convertToMnUsersJson(MnUsers mnUser,HttpServletRequest request) {

		StringBuffer jsonStr = new StringBuffer("");
			try{
			if(logger.isDebugEnabled()){logger.debug("convertToMnUsersJson method called");}	
			jsonStr.append("{\"_id\" : \"" + mnUser.get_id() + "\",");
			jsonStr.append("\"name\" : \"" + mnUser.getUserFirstName().trim()+" "+mnUser.getUserLastName().trim() + "\",");
			jsonStr.append("\"userId\" : \"" + mnUser.getUserId() + "\",");
			jsonStr.append("\"userName\" : \"" + mnUser.getUserName() + "\",");
			
			jsonStr.append("\"filePath\" : \"" +  mnUser.getFilePath() + "\",");
			jsonStr.append("\"emailId\" : \"" + mnUser.getEmailId() + "\",");
		    jsonStr.append("\"userRole\" : \"" + mnUser.getUserRole() + "\"}");
			 
				if(logger.isDebugEnabled()){logger.debug("converted to MnUsers Json Object");}
			}catch(Exception e){
				logger.error("Exception in converting MnUsers to json"+e);
			}

			return jsonStr.toString();
	}


	@Override
	public String getGroupNamesForSearch(String params)
	{
		if(logger.isDebugEnabled()){logger.debug("getGroupNamesForSearch method called");}
		String result=null;
		String searchText=null;
		String userId=null;
	    List<MnGroupDomain> mnGroup=null;
	    JSONArray array=new JSONArray();
		try {
			JSONObject jsonObject;
			jsonObject = new JSONObject(params);
			searchText=(String) jsonObject.get("Searching");
			userId=(String) jsonObject.get("userId");
			
			mnGroup = iGroupDao.getGroupNamesForSearch(userId);
			if(mnGroup!=null){
			for(MnGroupDomain domain: mnGroup){
				if(domain.getGroupName().toLowerCase().contains(searchText.toLowerCase())){
					array.put(domain.getGroupName());
				}
			}
			}
			if(logger.isDebugEnabled()){logger.debug("Got group names based on search");}
			
		} catch (Exception e) {
			logger.error("Exception in getGroupnamesForSearch method  : "+ e);

		}
		return array.toString();
	}


	@Override
	public String addInvitedUser(Integer userId,String userSelectDate, String[] userMail,boolean trialCheck) {
		String status="";
		try
		{
			if(logger.isDebugEnabled()){logger.debug("addInvitedUser method called "+userId);}
			status = iGroupDao.addInvitedUser(userId,userSelectDate,userMail,trialCheck);
			if(logger.isDebugEnabled()){logger.debug("Invited users added");}
			
		}catch (Exception e) {
			logger.error("Error in addInvitedUser method :"+e);
		}
		return status;
	}
	
	@Override
	public String updateInvitedUser(Integer userId, String newMail,String oldEmail) {
		String status="";
		try
		{
			if(logger.isDebugEnabled()){logger.debug("updateInvitedUser method called "+userId);}
			status = iGroupDao.updateInvitedUser(userId,newMail,oldEmail);
			if(logger.isDebugEnabled()){}logger.debug("Updated invited users");
			
		}catch (Exception e) {
			logger.error("Error in updateInvitedUser method :"+e.getMessage());
		}
		return status;
	}


	@Override
	public String getInvitedUser(String params) {
		String status="";
		Integer userId=0;
		List<MnInvitedUsers> users=null;
		try
		{
			if(logger.isDebugEnabled()){logger.debug("getInvitedUser method called");}
			JSONObject object=new JSONObject(params);
			userId=(Integer)object.getInt("userId");
			users = iGroupDao.getInvitedUser(userId);
			if(users!=null && !users.isEmpty())
				status=convertToMnInviteUser(users);
			else if(users==null)
				status="false";
			
		}catch (Exception e) {
			logger.error("Error in getInvitedUser method :"+e);
		}
		if(logger.isDebugEnabled()){logger.debug("Got invited users");}
		return status;
		
	}
	
	private String convertToMnInviteUser(List<MnInvitedUsers> invitedUsers) 
	{
		StringBuffer jsonStr = new StringBuffer("[");
		try
		{
			if(logger.isDebugEnabled()){logger.debug("convertTomninvitedUser method called");}
		for (MnInvitedUsers invite : invitedUsers)
		{
			jsonStr.append("{\"Id\" : \"" + invite.getId() + "\",");
			jsonStr.append("\"userId\" : \"" + invite.getUserId()+ "\",");
			jsonStr.append("\"invitedMailID\" : \"" + invite.getInvitedMailID() + "\",");
			jsonStr.append("\"date\" : \"" + invite.getDate() + "\",");
			jsonStr.append("\"userCreated\" : \"" + invite.isUserCreated() + "\",");
			jsonStr.append("\"status\" : \"" + invite.getStatus() + "\"},");

		}
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		if(logger.isDebugEnabled()){logger.debug(" json conversion to mninviteduser ");}
		jsonStr.append("]");
	}catch (Exception e) {
		logger.error("Exception in convertToMninvitedUser "+e);
	}
		return jsonStr.toString();
	}

	@Override
	public String deletInvite(String params) {
		String status="";
		Integer userId=0;
		String mailId="";
		try
		{
			if(logger.isDebugEnabled()){logger.debug("deletinvite method called");}
			JSONObject object=new JSONObject(params);
			userId=(Integer)object.getInt("userId");
			mailId=(String)object.get("mailId");
			status = iGroupDao.deletInviteUser(userId,mailId);
			if(logger.isDebugEnabled()){logger.debug("Deleted invited user");}
		}catch (Exception e) {
			logger.error("Error in deletInvite :"+e);
		}
		return status;
	}
	
	@Override
	public String checkPaymentGroupForPremiumUsers(String params){
	String status="";
	
	try{
		if(logger.isDebugEnabled()){logger.debug("checkPaymentGroupForPremiumUsers method called");}
		JSONObject jsonObject=new JSONObject(params);
		String userId=(String)jsonObject.get("userId");
		status=iGroupDao.checkPaymentGroupForPremiumUsers(userId);
		if(logger.isDebugEnabled()){logger.debug("checkPaymentGroupForPremiumUsers method executed and verified");}
	}catch(Exception e){
	logger.error("Exception while fetching payment user group"+e);	
	}
	return status;
	}
	
	@Override
	public String createDefaultGroup(Integer userId)
	{

		String status="";
		
		try{
			if(logger.isDebugEnabled()){logger.debug("createDefaultGroup method called "+userId);}
			status=iGroupDao.createDefaultGroup(userId);
			if(logger.isDebugEnabled()){logger.debug("created Default Group");}
		
		}catch(Exception e){
		logger.error("Exception while creating default group"+e);	
		}
		return status;
		
		
	}
	
	@Override
	public String userInformation(String params){
	String status="";
	try{
		if(logger.isDebugEnabled()){logger.debug("userInformation method called");}
		JSONObject jsonObject=new JSONObject(params);
		String userId=(String)jsonObject.get("userId");
		status=iGroupDao.userInformation(userId);
		if(logger.isDebugEnabled()){logger.debug("userInformation method is executed");}
	
	}catch(Exception e){
	logger.error("Exception while fetching payment user group");	
	}
	return status;
	}


	@Override
	public String addFriendRequestForInviteUser(Integer userId, String[] userMail,String userFirstName,String userLastName) {
		String status="";
		try{
			for(int i=0;i<userMail.length;i++)
			{
			MnUsers mnuser=	iGroupDao.getExitsEmail(userMail[i].toLowerCase().trim());
			if(mnuser==null){
				String mail=userMail[i].toLowerCase().trim();
				status=iGroupDao.addFriendRequestForInviteUser(mail,userFirstName,userLastName);
			}else{
				if(!userId.equals(mnuser.getUserId())){
				status=iFriendDao.addFriendInvitedUsers(userMail[i].toLowerCase().trim(),userId.toString(), mnuser.getUserId().toString());
				}
			}
			
			}
				
		}catch(Exception e){
		logger.error("Exception in addFriendRequestForInviteUser method");	
		}
		return status;
	}


	public IFriendDao getiFriendDao() {
		return iFriendDao;
	}


	public void setiFriendDao(IFriendDao iFriendDao) {
		this.iFriendDao = iFriendDao;
	}
	
}
