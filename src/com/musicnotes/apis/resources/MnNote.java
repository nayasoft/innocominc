package com.musicnotes.apis.resources;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.musicnotes.apis.dao.interfaces.IEventDao;
import com.musicnotes.apis.dao.interfaces.INoteDao;
import com.musicnotes.apis.domain.MnAttachmentDetails;
import com.musicnotes.apis.domain.MnComments;
import com.musicnotes.apis.domain.MnEventDetails;
import com.musicnotes.apis.domain.MnFeedbacks;
import com.musicnotes.apis.domain.MnList;
import com.musicnotes.apis.domain.MnLog;
import com.musicnotes.apis.domain.MnNoteDetails;
import com.musicnotes.apis.domain.MnNotesDetails;
import com.musicnotes.apis.domain.MnNotesSharingDetails;
import com.musicnotes.apis.domain.MnRemainders;
import com.musicnotes.apis.domain.MnTag;
import com.musicnotes.apis.domain.MnUsers;
import com.musicnotes.apis.domain.MnVoteViewCount;
import com.musicnotes.apis.domain.MnComplaints;
import com.musicnotes.apis.interfaces.MnNoteable;
import com.musicnotes.apis.util.JavaMessages;

public class MnNote implements MnNoteable {
	INoteDao iNoteDao;
	IEventDao iEventDao;
	Logger logger = Logger.getLogger(MnNote.class);

	public MnList convertJsonToMnListObj(String jsonStr) {
		
		if(logger.isDebugEnabled())
			logger.debug(" convertJsonToMnListObj method called");
		
		MnList mnList = new MnList();
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				mnList.setListId(1);
				mnList.setListName((String) jsonObject.get("listName"));
				mnList.setListType((String) jsonObject.get("listType"));
				mnList.setUserId(Integer.parseInt((String)jsonObject.get("userId")));
				mnList.setNoteAccess((String)jsonObject.get("noteAccess"));
				mnList.setStatus((String)jsonObject.get("status"));
			}
			catch (Exception e) {
				logger.error("Exception while converting json to List object !", e);
			}
		}
		if(logger.isDebugEnabled())
			logger.debug(" convertJsonToMnListObj method returned result ");
		return mnList;
	}

	public INoteDao getiNoteDao() {
		return iNoteDao;
	}

	public void setiNoteDao(INoteDao iNoteDao) {
		this.iNoteDao = iNoteDao;
	}

	@Override
	public String createList(MnList mnList,String listId,String noteId) 
	{
		if(logger.isDebugEnabled())
		logger.debug(" CreateList method called listId: "+listId+" noteId: "+noteId);
		
		String status = "";
		MnList list=null;
		try {
			mnList.setDefaultNote(false);
			if(mnList.getListType().equalsIgnoreCase("schedule"))
			{
				mnList.setEnableDisableFlag(true);
			}
			else
			{
				mnList.setEnableDisableFlag(false);
			}
			
			mnList.setMnNotesDetails(new HashSet<String>());
			mnList.setSharedIndividuals(new ArrayList<Integer>());
			mnList.setSharedGroups(new ArrayList<Integer>());
			mnList.setShareAllContactFlag(false);
			list=iNoteDao.createList(mnList,listId,noteId);
			if(list!=null){
				status=convertToJsonListObject(list);
			}else{
				status="0";
			}
		} catch (Exception e) {
			logger.error("Exception in CreateList method  : "+ e.getMessage());

		}
		if(logger.isDebugEnabled())
			logger.debug(" CreateList method return result");
		return status;
	}

	@Override
	public String updateList(MnList mnList) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createNote(String mnNote,String listId,String userId) {
		if(logger.isDebugEnabled())
			logger.debug(" createNote method called "+userId+" listId: "+listId);
		String status = "";
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(mnNote);
			
			if(!jsonObject.has("noteName"))
			{
				if(jsonObject.has("eventName") && jsonObject.get("eventName")!=null)
					{
					String listName=(String) jsonObject.get("eventName");
					if(listName.indexOf("`*`") != -1){
						listName = listName.replace("`*`", "\\\"");
					}
					if(listName!=null && !listName.equals(""))
						status = iNoteDao.createNote(mnNote,listId,userId,"");
					}
			}
			else
			{
				String listName=(String) jsonObject.get("noteName");
				String access=(String) jsonObject.get("access");
				if(listName.indexOf("`*`") != -1){
					listName = listName.replace("`*`", "\\\"");
				}
				if(listName!=null && !listName.equals("")){
					status = iNoteDao.createNote(mnNote,listId,userId,access);
					status="{\"noteId\":\""+status+"\"}";
				}
			}
		} catch (Exception e) {
			logger.error("Exception in Create Note method  : ", e);
            status="0";
		}
		if(logger.isDebugEnabled())
			logger.debug(" createNote method returned result");
		return status;
	}

	public String updateScheduleNote(String mnNote,String listId,String userId) 
	{
		if(logger.isDebugEnabled())
			logger.debug(" updateScheduleNote method called "+userId+" listId: "+listId);
		String status = "";
		try {
					status = iNoteDao.updatenoteSchedule(mnNote ,listId,userId);
			}
		 catch (Exception e) {
			logger.error("Exception in Update Schedule Note method  : ", e);

		}
		return status;
	}

	
	@Override
	public String updateDuedateForNote(String dueDate,String noteId, String listId,String userId) 
	{
		if(logger.isDebugEnabled())
			logger.debug(" updateDuedateForNote method called" +userId+" listId: "+listId);
		String status = "";
		String date="";
		String time="";
		
		try {
			
			if (dueDate != null && !dueDate.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(dueDate);
					date=(String) jsonObject.get("dueDate");
					time=(String)jsonObject.get("dueTime");
					String ownerName=(String) jsonObject.get("fullName");
					
					
					status = iNoteDao.updateDuedateForNote(date, time, listId, noteId,userId,ownerName);
			}
		} catch (Exception e) {
			logger.error("Exception in Update Due Date For Note method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" updateDuedateForNote method returned result ");
		return status;
	}

	@Override
	public String getNoteDetails(String params) 
	{
		if(logger.isDebugEnabled())
			logger.debug(" getNoteDetails method called");
		String status = "";
		String listId="";
		String noteId="";
		String type="";
		String userId="";
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					
					listId=(String) jsonObject.get("listId");
					noteId=(String)jsonObject.get("noteId");
					type=(String) jsonObject.get("listType");
					userId=(String) jsonObject.get("userId");
					status = iNoteDao.noteDetails(listId, noteId, type, userId);
			}
		} catch (Exception e) {
			logger.error("Exception in getNoteDetails method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" getNoteDetails method returned result ");
		return status;
	}
	
	public String getListDetails(String params) 
	{
		if(logger.isDebugEnabled())
			logger.debug(" getListDetails method called");
		String status = "";
		String listId="";
		String type="";
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					
					listId=(String) jsonObject.get("listId");
					type=(String) jsonObject.get("type");
					status = iNoteDao.listDetails(listId,type);
			}
		} catch (Exception e) {
			logger.error("Exception in getListDetails method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" getListDetails method returned result ");
		return status;
	}
	
	@Override
	public String updateMembersForNote(String members, String noteId,String listId, String userId) 
	{
		if(logger.isDebugEnabled())
			logger.debug(" updateMembersForNote method called "+userId+" listId: "+listId);
		String status = "";
		String userIds="";
		String noteType="";
		String noteLevel="";
		String nonMailIds = ""; 
		String fullName="";
		String noteName="";
		try {
			if (members != null && !members.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(members);
					userIds=(String) jsonObject.get("members");
					noteType=(String) jsonObject.get("listType");
					noteLevel=(String) jsonObject.get("noteLevel");
					nonMailIds =(String) jsonObject.get("nonMailIds");
					fullName =(String) jsonObject.get("fullName");
					noteName =(String) jsonObject.get("noteName");
					if(noteName.indexOf("`*`") != -1){
						noteName = noteName.replace("`*`", "\\\"");
					}
					if(userIds!= null && !userIds.equals("")){
						status = iNoteDao.updateMembersForNote(userIds, listId, noteId,userId,noteType,noteLevel);
					}
					if(nonMailIds!= null && !nonMailIds.equals("")){
						iNoteDao.sahreBasedOnEmailId(nonMailIds, listId, Integer.parseInt(noteId), userId, noteLevel,fullName,noteName, noteType);
					}
					
			}
		} catch (Exception e) {
			logger.error("Exception in Update Members For Note method  : ", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" updateMembersForNote method returned result ");
		return status;
	}
	
	@Override
	public String archiveList(String params,String userId) {
		if(logger.isDebugEnabled())
			logger.debug(" archiveList method called "+userId);
		String status = "";
		String listId="";
		String listType="";
		String sharedType = "";
		MnList mnList=null;
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					listId=(String) jsonObject.get("listId");
					listType=(String) jsonObject.get("listType");
					sharedType =(String) jsonObject.get("sharedType");
					mnList = iNoteDao.archiveList(listId,userId,sharedType,listType);
					if(status!="" && status!="0")
					{
						if(listType!="" && listType.equals("schedule"))
							status=iEventDao.deleteEvents(Integer.parseInt(listId), Integer.parseInt(userId));
					}
					if(mnList!=null){
						status=convertToJsonListObject(mnList);
					}else{
						status="0";
					}
			}
		} catch (Exception e) {
			logger.error("Exception in archiveList method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" archiveList method returned result ");
		return status;
	}

	@Override
	public String archiveNote(String params,String userId) {
		if(logger.isDebugEnabled())
			logger.debug(" archiveNote method called "+userId);
		String status = "";
		String listId="";
		String noteId="";
		String eventId="";
		String shareType="";
		String groupId="";
		String shareUserId="";
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					listId=(String) jsonObject.get("listId");
					shareType=(String) jsonObject.get("shareType");
					shareUserId=(String) jsonObject.get("shareUserId");
					
					if(shareType.contains("group")){
						groupId=shareType.replace("group", "");
						shareType="group";
					}
					
					if(shareType.contains("noteDiv"))
						shareType="";
					
					if(jsonObject.has("noteId"))
						noteId=(String) jsonObject.get("noteId");
					else
						eventId=(String) jsonObject.get("eventId");
					
					if(noteId!="")
						status = iNoteDao.archiveNote(listId, noteId,userId,"notes",shareType,groupId,shareUserId);
					else if(eventId!="")
						status = iNoteDao.archiveNote(listId, eventId,userId,"events",shareType,groupId,shareUserId);
					
			}
		} catch (Exception e) {
			logger.error("Exception in archiveNote method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" archiveNote method returned result ");
		return status;
	}

	@Override
	public String archiveAllNoteInList(String params) {
		if(logger.isDebugEnabled())
			logger.debug(" archiveAllNoteInList method called");
		String status = "";
		String listId="";
		String userId="";
		String listType="";
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					listId=(String) jsonObject.get("listId");
					userId=(String) jsonObject.get("userId");
					listType=(String) jsonObject.get("listType");
					
					status = iNoteDao.archiveAllNoteInList(listId,userId,listType);
					logger.info("userId   :"+userId);
					if(status!="" && status!="0")
					{
						logger.info("listId   :"+listId);
						if(listType!="" && listType.equals("schedule"))
							status=iEventDao.deleteEvents(Integer.parseInt(listId), Integer.parseInt(userId));
					}
			}
		} catch (Exception e) {
			logger.error("Exception in archive All Note In This List method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" archiveAllNoteInList method returned result ");
		return status;
	}

	@Override
	public String fetchList(String params) {
		if(logger.isDebugEnabled())
			logger.debug(" fetchList method called");
		String userId="";
		String listType="";
		List<MnList> mnListList = null;
		List<MnNoteDetails> mnNoteDetailsList = null;
		String jsonString = null;
		String jsonString2=null;
		try {
			if (params != null && !params.equals("")) {
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				userId=(String) jsonObject.get("userId");
				listType=(String) jsonObject.get("listType");
				boolean eventFlag = jsonObject.getBoolean("fetchEvent");
				
				if(!eventFlag && listType.equals("schedule"))
				{
					mnListList = fetchAllListBasedOnUserForSchedule(userId,listType);
					if (mnListList != null && !mnListList.isEmpty())
					{						
						jsonString = "["+convertToJsonListOfList(mnListList);
					}
				}else{
					mnNoteDetailsList = fetchAllListBasedOnUser(userId,listType);
					mnListList = iNoteDao.fetchEmptyList(userId,listType);
					if (mnNoteDetailsList != null && !mnNoteDetailsList.isEmpty())
					{						
						jsonString = convertToJsonNoteDetailsToMnList(mnNoteDetailsList);
						
						if(mnListList!=null && !mnListList.isEmpty())
							jsonString = jsonString+","+convertToJsonListOfList(mnListList);
						else
							jsonString=jsonString+"]";
						
	
					}else if(mnListList!=null && !mnListList.isEmpty()){
						jsonString = "["+convertToJsonListOfList(mnListList);
					}
				}
				//shared note details...
				
				List<MnNotesSharingDetails> mnNotesSharingDetails=iNoteDao.fetchSharedNotes(userId, listType);
				if(mnNotesSharingDetails !=null && !mnNotesSharingDetails.isEmpty())
				{
					jsonString2=convertToJsonShare(mnNotesSharingDetails);
				}
			}
		} catch (Exception e) {
			logger.error("Exception in Fetching List method in MnNote  : "+ e.getMessage());

		}
		if(logger.isDebugEnabled())
			logger.debug(" fetchList method returned result ");
		return jsonString; //+'~'+jsonString2;
	}
	
	
	public List<MnList> fetchAllListBasedOnUserForSchedule(String userId,String listType)throws Exception{
		List<MnList> mnListList = null;
		if(logger.isDebugEnabled())
			logger.debug(" fetchAllListBasedOnUserForSchedule method called "+userId);
		try{
			//fetch own list and notes
			mnListList = iNoteDao.fetchScheduleList(userId,listType);
			
			//fetch individual share notes based on list 
			if (mnListList != null && !mnListList.isEmpty())
			{
				List<MnList> lists=iNoteDao.fetchSharedNotesBasedOnScheduleList(userId, listType);
				if(lists!=null && !lists.isEmpty())
					mnListList.addAll(lists);
			}else{
				mnListList = iNoteDao.fetchSharedNotesBasedOnScheduleList(userId, listType);
			}
			//fetch group share notes based on list
			if (mnListList != null && !mnListList.isEmpty())
			{
				List<MnList> lists=iNoteDao.fetchGroupSharedNotesBasedOnScheduleList(userId, listType);
				if(lists!=null && !lists.isEmpty())
					mnListList.addAll(lists);
			}else{
				mnListList = iNoteDao.fetchGroupSharedNotesBasedOnScheduleList(userId, listType);
			}
			
		}catch (Exception e) {
			logger.error("Exception in fetchAllListBasedOnUserForSchedule :"+ e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug(" fetchAllListBasedOnUserForSchedule method returned result ");
		return mnListList;
	}

	private List<MnNoteDetails> fetchAllListBasedOnUser(String userId,String listType)throws Exception{
		List<MnNoteDetails> ownMnNotesDetailsList = null;
		if(logger.isDebugEnabled())
			logger.debug(" fetchAllListBasedOnUser method called "+userId);
		try{
						
			//Own Notes Fetch Method
			ownMnNotesDetailsList = iNoteDao.fetchList(userId,listType);

			//fetch individual share notes based on list 
			if (ownMnNotesDetailsList != null && !ownMnNotesDetailsList.isEmpty())
			{
				List<MnNoteDetails> lists=iNoteDao.fetchSharedNotesBasedOnList(userId, listType);
				if(lists!=null && !lists.isEmpty())
					ownMnNotesDetailsList.addAll(lists);
			}else{
				ownMnNotesDetailsList = iNoteDao.fetchSharedNotesBasedOnList(userId, listType);
			}
			//fetch group share notes based on list
			if (ownMnNotesDetailsList != null && !ownMnNotesDetailsList.isEmpty())
			{
				List<MnNoteDetails> lists=iNoteDao.fetchGroupSharedNotesBasedOnList(userId, listType);
				if(lists!=null && !lists.isEmpty())
					ownMnNotesDetailsList.addAll(lists);
			}else{
				ownMnNotesDetailsList = iNoteDao.fetchGroupSharedNotesBasedOnList(userId, listType);
			}
			if(ownMnNotesDetailsList != null && !ownMnNotesDetailsList.isEmpty()){
				for(MnNoteDetails temp:ownMnNotesDetailsList){
				if(temp.getUserId()!=Integer.parseInt(userId)){
				String dueDate=temp.getDueDate();
				String dueTime=temp.getDueTime();
				if(dueTime !=null && !dueTime.equals(""))
				{
				SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
			       SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
			       Date date1 = parseFormat.parse(dueTime);
			       dueTime=displayFormat.format(date1);
			       String date=dueDate+" "+dueTime;
			
					String stDate = iEventDao.convertedZoneEvents(Integer.parseInt(userId),temp.getUserId(), date);
					String arr[]=stDate.split(" ");
					
					SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
				    Date dateObj = sdf.parse(arr[1]);
				    String time=new SimpleDateFormat("hh:mm a").format(dateObj);
				
				    temp.setDueDate(arr[0]);
				    temp.setDueTime(time);
				}
				}
				}
			}
			
			
			if (ownMnNotesDetailsList != null && !ownMnNotesDetailsList.isEmpty()) {
				Collections.sort(ownMnNotesDetailsList, SENIORITY_ORDER_NOTE);
			}
			
			
			
		}catch (Exception e) {
			logger.error("Exception in fetchAllListBasedOnUser :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug(" fetchAllListBasedOnUser method returned result ");
		return ownMnNotesDetailsList;
	}
	
	//Compartor For MnNoteDetails
	static final Comparator<MnNoteDetails> SENIORITY_ORDER_NOTE = new Comparator<MnNoteDetails>() {
		public int compare(MnNoteDetails e1, MnNoteDetails e2) {
			Date i1=null,i2=null;
			try {
				 
				 if(e1.getTempSharedDate()!= null && !e1.getTempSharedDate().equals(""))
					 i1 = e1.getTempSharedDate();
				 else
					 i1 = e1.getStartDate();
				 
				 
				 if(e2.getTempSharedDate()!= null && !e2.getTempSharedDate().equals(""))
					 i2 = e2.getTempSharedDate();
				 else
				 	 i2 = e2.getStartDate();
				 	 
				
			} catch (Exception e) {
			}
			
			return i2.compareTo(i1);
		}
	};
	
	private String convertToJsonNoteDetailsToMnList(List<MnNoteDetails> mnNoteDetails) {
		StringBuffer jsonStr = new StringBuffer("[");
		if(logger.isDebugEnabled())
			logger.debug(" convertToJsonNoteDetailsToMnList method called");
		try{
			for (MnNoteDetails list : mnNoteDetails)
			{
				jsonStr.append("{\"_id\" : \"" + list.get_id() + "\",");
				jsonStr.append("\"listId\" : \"" + list.getListId() + "\",");
				jsonStr.append("\"listName\" : \"" + list.getListName() + "\",");
				jsonStr.append("\"listType\" : \"" + list.getListType() + "\",");
				jsonStr.append("\"noteAccess\" : \"" + list.getNoteAccess() + "\",");
				jsonStr.append("\"defaultNote\" : " + list.isDefaultNote() + ",");
				jsonStr.append("\"enableDisableFlag\" : " + list.isEnableDisableFlag() + ",");
				jsonStr.append("\"userId\" : \"" + list.getUserId() + "\",");
				jsonStr.append("\"status\" : \"" + list.getStatus() + "\",");
				jsonStr.append("\"sharedIndividuals\" : \"" + list.getSharedIndividuals().toString() + "\",");
				jsonStr.append("\"sharedGroups\" : \"" + list.getSharedGroups().toString() + "\",");
				jsonStr.append("\"shareAllContactFlag\" : " + list.isShareAllContactFlag() + ",");
				jsonStr.append("\"originalListId\" : " + list.getOriginalListId() + ",");
				jsonStr.append("\"mnNotesDetails\" : " + list.toString() + "},");
			}
			jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
			
		} catch (Exception e) {
			logger.error("Exception in Convert Mnnotedetails List Object To MnList Json Object method  : ", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" convertToJsonNoteDetailsToMnList method returned result ");
		return jsonStr.toString();
	}
	
	private String convertToJsonNoteDetailsToSingleMnList(List<MnNoteDetails> mnNoteDetails, MnList mnList) {
		if(logger.isDebugEnabled())
			logger.debug(" convertToJsonNoteDetailsToSingleMnList method called");
		StringBuffer jsonStr = new StringBuffer("[");
		StringBuffer noteDetailsJsonStr = new StringBuffer();
		try{
			Integer i=0;
			for (MnNoteDetails list : mnNoteDetails)
			{
				if(i==0){
					jsonStr.append("{\"_id\" : \"" + mnList.get_id() + "\",");
					jsonStr.append("\"listId\" : \"" + mnList.getListId() + "\",");
					jsonStr.append("\"listName\" : \"" + mnList.getListName() + "\",");
					jsonStr.append("\"listType\" : \"" + mnList.getListType() + "\",");
					jsonStr.append("\"noteAccess\" : \"" + mnList.getNoteAccess() + "\",");
					jsonStr.append("\"defaultNote\" : " + mnList.isDefaultNote() + ",");
					jsonStr.append("\"enableDisableFlag\" : " + mnList.isEnableDisableFlag() + ",");
					jsonStr.append("\"userId\" : \"" + mnList.getUserId() + "\",");
					jsonStr.append("\"status\" : \"" + mnList.getStatus() + "\",");
					jsonStr.append("\"sharedIndividuals\" : \"" + mnList.getSharedIndividuals().toString() + "\",");
					jsonStr.append("\"sharedGroups\" : \"" + mnList.getSharedGroups().toString() + "\",");
					jsonStr.append("\"shareAllContactFlag\" : " + mnList.isShareAllContactFlag() + ",");
					i=1;
				}
				StringBuffer tempNotes = new StringBuffer();
				tempNotes.append(list.toString());
				tempNotes = tempNotes.deleteCharAt(tempNotes.lastIndexOf("]"));
				tempNotes = tempNotes.deleteCharAt(0);
				noteDetailsJsonStr.append(tempNotes).append(",");
			}
			noteDetailsJsonStr = noteDetailsJsonStr.deleteCharAt(noteDetailsJsonStr.lastIndexOf(","));
			jsonStr.append("\"mnNotesDetails\" : [" + noteDetailsJsonStr.toString() + "]},");
			jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
			
		} catch (Exception e) {
			logger.error("Exception in Convert Mnnotedetails List Object To Single MnList Json Object method  : ", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" convertToJsonNoteDetailsToSingleMnList method returned result ");
		return jsonStr.toString();
	}
	
	
	private String convertToJsonListOfList(List<MnList> mnListList) {
		if(logger.isDebugEnabled())
			logger.debug(" convertToJsonListOfList method called");
		StringBuffer jsonStr = new StringBuffer();
		try{
			for (MnList list : mnListList)
			{
				jsonStr.append("{\"_id\" : \"" + list.get_id() + "\",");
				jsonStr.append("\"listId\" : \"" + list.getListId() + "\",");
				jsonStr.append("\"listName\" : \"" + list.getListName() + "\",");
				jsonStr.append("\"listType\" : \"" + list.getListType() + "\",");
				jsonStr.append("\"noteAccess\" : \"" + list.getNoteAccess() + "\",");
				jsonStr.append("\"defaultNote\" : " + list.isDefaultNote() + ",");
				jsonStr.append("\"enableDisableFlag\" : " + list.isEnableDisableFlag() + ",");
				jsonStr.append("\"userId\" : \"" + list.getUserId() + "\",");
				jsonStr.append("\"status\" : \"" + list.getStatus() + "\",");
				jsonStr.append("\"sharedIndividuals\" : \"" + list.getSharedIndividuals().toString() + "\",");
				jsonStr.append("\"sharedGroups\" : \"" + list.getSharedGroups().toString() + "\",");
				jsonStr.append("\"shareAllContactFlag\" : " + list.isShareAllContactFlag() + ",");
				if(list.getMnNotesDetailsList()== null || list.getMnNotesDetailsList().isEmpty()){
					jsonStr.append("\"mnNotesDetails\" : " + list.getMnNotesDetails().toString() + "},");
				}else{
					jsonStr.append("\"mnNotesDetails\" : " + list.getMnNotesDetailsList().toString() + "},");
				}
			}
			jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
			jsonStr.append("]");
		} catch (Exception e) {
			logger.error("Exception in Convert Mnlist List Object To Json method  : ", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" convertToJsonListOfList method returned result ");
		return jsonStr.toString();
	}
	
	private String convertToJsonShare(List<MnNotesSharingDetails> details) {
		StringBuffer jsonStr = new StringBuffer("[");
		if(logger.isDebugEnabled())
			logger.debug(" convertToJsonShare method called");
		try{
			for (MnNotesSharingDetails list : details)
			{
				jsonStr.append("{\"_id\" : \"" + list.get_id() + "\",");
				jsonStr.append("\"listId\" : \"" + list.getListId() + "\",");
				jsonStr.append("\"noteId\" : \"" + list.getNoteId() + "\",");
				jsonStr.append("\"listType\" : \"" + list.getListType() + "\",");
				jsonStr.append("\"shareUserId\" : \"" + list.getSharingUserId() + "\",");
				jsonStr.append("\"userId\" : \"" + list.getUserId() + "\",");
				jsonStr.append("\"status\" : \"" + list.getStatus() + "\",");
				jsonStr.append("\"shareGroupId\" : \"" + list.getSharingGroupId() + "\"},");
			}
			jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
			jsonStr.append("]");
		} catch (Exception e) {
			logger.error("Exception in convertToJsonShare method  : ", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" convertToJsonShare method returned result ");
		return jsonStr.toString();
	}
	
	private String convertToJsonListObject(MnList mnList) {

		if(logger.isDebugEnabled())
			logger.debug(" convertToJsonListObject method called");
		StringBuffer jsonStr = new StringBuffer("[");
		try{
			jsonStr.append("{\"_id\" : \"" + mnList.get_id() + "\",");
			jsonStr.append("\"listId\" : \"" + mnList.getListId() + "\",");
			jsonStr.append("\"listName\" : \"" + mnList.getListName() + "\",");
			jsonStr.append("\"listType\" : \"" + mnList.getListType() + "\",");
			jsonStr.append("\"noteAccess\" : \"" + mnList.getNoteAccess() + "\",");
			jsonStr.append("\"defaultNote\" : " + mnList.isDefaultNote() + ",");
			jsonStr.append("\"enableDisableFlag\" : " + mnList.isEnableDisableFlag() + ",");
			jsonStr.append("\"userId\" : \"" + mnList.getUserId() + "\",");
			jsonStr.append("\"status\" : \"" + mnList.getStatus() + "\",");
			jsonStr.append("\"sharedIndividuals\" : \"" + mnList.getSharedIndividuals().toString() + "\",");
			jsonStr.append("\"sharedGroups\" : \"" + mnList.getSharedGroups().toString() + "\",");
			jsonStr.append("\"shareAllContactFlag\" : " + mnList.isShareAllContactFlag() + ",");
			jsonStr.append("\"mnNotesDetails\" : " + mnList.getMnNotesDetails().toString() + "},");  
			
			jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
			jsonStr.append("]");
		} catch (Exception e) {
			logger.error("Exception in convertToJsonListObject method  : ", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" convertToJsonListObject method returned result ");
		return jsonStr.toString();
	}

	@Override
	public String moveNote(String params,String userId) {
		if(logger.isDebugEnabled())
			logger.debug(" moveNote method called "+userId);
		String status = "";
		String listId="";
		String noteId="";
		String selectedListId="";
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					listId=(String) jsonObject.get("listId");
					noteId=(String) jsonObject.get("noteId");
					selectedListId=(String) jsonObject.get("selectedListId");
					
					status = iNoteDao.moveNote(listId, noteId, selectedListId,userId);
			}
		} catch (Exception e) {
			logger.error("Exception in moveNote method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" moveNote method returned result ");
		return status;
	}

	@Override
	public String moveAllNote(String params,String userId) {
		if(logger.isDebugEnabled())
			logger.debug(" moveAllNote method called "+userId);
		String status = "";
		String listId="";
		String selectedListId="";
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					listId=(String) jsonObject.get("listId");
					selectedListId=(String) jsonObject.get("selectedListId");
					
					status = iNoteDao.moveAllNote(listId, selectedListId,userId);
			}
		} catch (Exception e) {
			logger.error("Exception in Move All Note method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" moveAllNote method returned result ");
		return status;
	}

	@Override
	public String copyList(String params) {
		if(logger.isDebugEnabled())
			logger.debug(" copyList method called");
		String status = "";
		String listId="";
		String newListName="";
		String userId="";
		String listType ="";
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					listId=(String) jsonObject.get("listId");
					listType=(String) jsonObject.get("listType");
					newListName=(String)jsonObject.get("newListName");
					userId=(String)jsonObject.get("userId");
					MnList mnList=iNoteDao.copyList(listId, newListName,userId);
					if(mnList!=null){
						if (listType.equals("schedule")){
							// not functionality for schedule
						}else{
							status = fetchList(params);
						}
						
					}
			}
		} catch (Exception e) {
			logger.error("Exception in Copy List method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" copyList method returned result ");
		return status;
	}
	
	@Override
	public String updateAttachedFileForNote(String filenames,String noteId, String listId, String userId) {
		
		String status = "";
		if(logger.isDebugEnabled())
			logger.debug(" updateAttachedFileForNote method called "+userId+" filenames:"+filenames);
		try {
			if (filenames != null && !filenames.equals("")) {
					status = iNoteDao.updateAttachedFileForNote(filenames.trim(), listId, noteId,userId);
			}
		} catch (Exception e) {
			logger.error("Exception in Update Attached File For Note method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" updateAttachedFileForNote method returned result ");
		return status;
	}
	
	@Override
	public String getAttachedFileForNote(String noteId, String listId){
		String attachFilePath = "";
		if(logger.isDebugEnabled())
			logger.debug(" getAttachedFileForNote method called and noteId: "+noteId);
		try {
			
			attachFilePath = iNoteDao.getAttachedFileForNote(listId, noteId);
			
		} catch (Exception e) {
			logger.error("Exception in Get Attached File For Note method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" getAttachedFileForNote method returned result ");
		return attachFilePath;
	}
	
	@Override
	public String copyNote(String params,String userId) {
		String status = "";
		String listId="";
		String noteId="";
		String selectedListId="";
		if(logger.isDebugEnabled())
			logger.debug(" copyNote method called "+userId);
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					listId=(String) jsonObject.get("listId");
					noteId=(String) jsonObject.get("noteId");
					selectedListId=(String) jsonObject.get("selectedListId");
					
					status = iNoteDao.copyNote(listId, noteId, selectedListId,userId,true);
			}
		} catch (Exception e) {
			logger.error("Exception in copy Note method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" getAttachedFileForNote method returned result ");
		return status;
	}

	@Override
	public String updateNoteName(String listId,String noteId,String noteName,String userId,String type) {
		String status = "";
		if(logger.isDebugEnabled())
			logger.debug(" updateNoteName method called "+userId+" noteName:"+noteName);
		try {
			status = iNoteDao.updateNoteName(listId,noteId,noteName,userId,type);
		} catch (Exception e) {
			logger.error("Exception in update note name method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" getAttachedFileForNote method returned result ");
		return status;
	}
	@Override
	public String deleteMembersForNote(String params, String noteId,String listId,String userId) 
	{
		String status = "";
		String userIds="";
		if(logger.isDebugEnabled())
			logger.debug(" deleteMembersForNote method called "+userId+" noteId:"+noteId);
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					userIds=(String) jsonObject.get("members");
					
					 status = iNoteDao.deleteMembersForNote(userIds, listId, noteId,userId);
			}
		} catch (Exception e) {
			logger.error("Exception in Delete Members In Note method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" deleteMembersForNote method returned result ");
		return status;
	}
	
	public String deletegroupsForNote(String params, String noteId,String listId,String userId) 
	{
		String status = "";
		String userIds="";
		if(logger.isDebugEnabled())
			logger.debug(" deletegroupsForNote method called "+userId+" noteId:"+noteId);
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					userIds=(String) jsonObject.get("groupid");
					
					status = iNoteDao.deletegroupForNote(userIds, listId, noteId,userId);
			}
		} catch (Exception e) {
			logger.error("Exception in Delete Groups In Note method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" deletegroupsForNote method returned result ");
		return status;
	}

	
	@Override
	public String addNoteDrescription(String listId,String noteId,String description, String userId) {
		String status = "";
		if(logger.isDebugEnabled())
			logger.debug(" addNoteDrescription method called "+userId+" noteId: "+noteId);
		try {
			if(description.indexOf("\n") != -1){
				description = description.replaceAll("\n"," <br> ");
			}
			status = iNoteDao.addNoteDescription(listId,noteId,findUrlInComments(description),userId);
			
			
		} catch (Exception e) {
			logger.error("Exception in add Note Drescription method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" addNoteDrescription method returned result ");
		return "{\"desc\":\"" +status +"\"}";
	}
	
	@Override
	public String getNote(String listId,String noteId,String userId){
		String status = "";
		if(logger.isDebugEnabled())
			logger.debug(" getNote method called "+userId+" noteId: "+noteId);
		try {
			status = iNoteDao.getNote(listId,noteId,userId);
			String ownerId=null;
			// time zone change for user
			JSONObject jsonObject;
			jsonObject = new JSONObject(status);
			String oldSt = (String) jsonObject.get("startDate");
			String oldEnd=(String)jsonObject.get("endDate");
			if(jsonObject.has("userId"))
			ownerId=(String)jsonObject.get("userId");
			else
				ownerId=(String)jsonObject.get("userId");
			if (status.contains("\"eventId\":\"" + noteId+ "\"")) {
			if(!(userId.equalsIgnoreCase(ownerId) || oldSt.indexOf(":")==-1))
			{
			String stDate = iEventDao.convertedZoneEvents(Integer.parseInt(userId), Integer.parseInt(ownerId), oldSt);
			status = status.replace("\"startDate\":\""+oldSt+"\"","\"startDate\":\"" + stDate + "\"");
			String endDate = iEventDao.convertedZoneEvents(Integer.parseInt(userId), Integer.parseInt(ownerId), oldEnd);
			status = status.replace("\"endDate\":\""+oldEnd+"\"","\"endDate\":\"" + endDate + "\"");
			}
			}
			
		} catch (Exception e) {
			logger.error("Exception in get note method  : ", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" getNote method returned result ");
		return status;

	}
	
	@Override
	public String addComments(MnComments mnComments,String listId,String noteId,String listType){
		String status="";
		if(logger.isDebugEnabled())
			logger.debug(" addComments method called listId: "+listId+" noteId: "+noteId);
		try{
			status = iNoteDao.addComments(mnComments, listId, noteId,listType);
		}catch (Exception e) {
			logger.error("Exception in adding comments :", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" addComments method returned result ");
		return status;
	}
	
	@Override
	public MnComments convertJsonToMnCommentsObj(String jsonStr) {
		MnComments mnComments = new MnComments();
		if(logger.isDebugEnabled())
			logger.debug(" convertJsonToMnCommentsObj method called");
		@SuppressWarnings("unused")
		Set<String> mnNotesDetails=new HashSet<String>();
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				Date d=new Date();
				DateFormat formatter = new SimpleDateFormat("MM/d/yyyy HH:mm:ss");
				String ss=formatter.format(d);
				String arr[]=ss.split(" ");
				jsonObject = new JSONObject(jsonStr);
				mnComments.setcId(1);
				mnComments.setListId((String) jsonObject.get("listId"));
				mnComments.setNoteId((String) jsonObject.get("noteId"));
				mnComments.setUserId(Integer.parseInt((String)jsonObject.get("userId")));
				
				String cmt = (String) jsonObject.get("comments");
				if(cmt.indexOf("`*`") != -1){
					cmt = cmt.replace("`*`", "\\\"");
				}
				mnComments.setComments(findUrlInComments(cmt));
				
				mnComments.setStatus((String)jsonObject.get("status"));
				mnComments.setcDate(arr[0]);
				mnComments.setcTime(arr[1]);
				mnComments.setEdited(Integer.parseInt((String) jsonObject.get("edited")));
				mnComments.setEditDate((String) jsonObject.get("editDate"));
				mnComments.setEditTime((String) jsonObject.get("editTime"));
				mnComments.setCommLevel((String) jsonObject.get("commLevel"));
			}
			catch (Exception e) {
				logger.error("Exception while converting json to List object !", e);
			}
		}
		if(logger.isDebugEnabled())
			logger.debug(" convertJsonToMnCommentsObj method returned result ");
		return mnComments;
	}
	
	public String findUrlInComments(String commentsOriginal){
		if(logger.isDebugEnabled())
			logger.debug(" findUrlInComments method called");
		String coments = "";
		try{
			String[] dns = commentsOriginal.split(" ");
			for(String str:dns){
				if(str.contains("http") ||str.contains("https")){
					
					if(urlCheck(str)){
						coments =coments+" <a target=\\\"_blank\\\" href=\\\""+str.trim()+"\\\" >"+str.trim()+"</a>";

					}else{
						if(isURLValid(str)){
							coments =coments+" <a target=\\\"_blank\\\" href=\\\"http://"+str.trim()+"\\\" >"+str.trim()+"</a>";

						}else{
							coments = coments+" "+str; 
						}
					}
				}else{
					if(isURLValid(str)){
						coments =coments+" <a target=\\\"_blank\\\" href=\\\"http://"+str.trim()+"\\\" >"+str.trim()+"</a>";

					}else{
						coments = coments+" "+str; 
					}
				}
					
			}
		}catch (Exception e) {
			logger.error("Error in findUrlInComments",e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" findUrlInComments method returned result ");
		return coments+" ";
	}
	@Override
	public String getComments(String params, String cmtsLevel,String userid){
		String status="";
		if(logger.isDebugEnabled())
			logger.debug(" getComments method called "+userid);
		List<MnComments> mnCommentsList=null;
		try{
			String[] paramArray=params.split(",");
			List<Integer> paramsList = new ArrayList<Integer>();
			if(paramArray.length >0 && paramArray[0]!=null){
				for(String par:paramArray){
					paramsList.add(Integer.parseInt(par.trim()));
				}
			}
			mnCommentsList = iNoteDao.getCommentsList(paramsList,cmtsLevel,userid);
			if(mnCommentsList!=null && !mnCommentsList.isEmpty())
			{
				status = convertMnComtObjectToJson(mnCommentsList);
			}
		}catch (Exception e) {
			logger.error("Exception in get MNCommnets List into json ",e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" getComments method returned result ");
		return status;
	}
	
	public String convertMnComtObjectToJson(List<MnComments> mnCommentsList){
		try{
			if(logger.isDebugEnabled())
				logger.debug(" convertMnComtObjectToJson method called");
			StringBuffer jsonStr = new StringBuffer("[");
			for (MnComments list : mnCommentsList)
			{
				jsonStr.append("{\"cId\" : \"" + list.getcId() + "\",");
				jsonStr.append("\"userId\" : \"" + list.getUserId() + "\",");
				jsonStr.append("\"listId\" : \"" + list.getListId() + "\",");
				jsonStr.append("\"noteId\" : \"" + list.getNoteId() + "\",");
				jsonStr.append("\"comments\" : \"" + list.getComments() + "\",");
				jsonStr.append("\"cDate\" : \"" + list.getcDate() + "\",");
				jsonStr.append("\"cTime\" : \"" + list.getcTime() + "\",");
				jsonStr.append("\"edited\" : \"" + list.getEdited() + "\",");
				jsonStr.append("\"editDate\" : \"" + list.getEditDate() + "\",");
				jsonStr.append("\"editTime\" : \"" + list.getEditTime() + "\",");
				jsonStr.append("\"commLevel\" : \"" + list.getCommLevel() + "\",");
				jsonStr.append("\"status\" : \"" + list.getStatus() + "\"},");
				
			}
			jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
			jsonStr.append("]");
			if(logger.isDebugEnabled())
				logger.debug(" convertMnComtObjectToJson method returned result ");
			return jsonStr.toString();
		}catch (Exception e) {
			logger.error("Exception while convert convertMnComtObjectToJson method ",e);
		}
		return "";
	}
	public String getAttachByFileId(String params,String userId){
		String status = "";
		if(logger.isDebugEnabled())
			logger.debug(" getAttachByFileId method called "+userId);
		List<MnAttachmentDetails> mnAttachmentsList = null;
		try{
			String[] paramArray=params.split(",");
			List<Integer> paramsList = new ArrayList<Integer>();
			if(paramArray.length >0 && paramArray[0]!=null){
				for(String par:paramArray){
					paramsList.add(Integer.parseInt(par.trim()));
				}
			}
			mnAttachmentsList = iNoteDao.getAttachedFileForNote(paramsList,userId);
			if(mnAttachmentsList!=null && !mnAttachmentsList.isEmpty()){
				status = convertAttachedJsonObj(mnAttachmentsList);
			}
		}catch (Exception e) {
			logger.error("Exception while ge tAttac hBy FileId",e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" getAttachByFileId method returned result ");
		return ""+status;	
	}
	public String getAttachedFileInNote(String listId,String noteId,String userId){
		String status = "";
		if(logger.isDebugEnabled())
			logger.debug(" getAttachedFileInNote method called "+userId+" noteId "+noteId);
		List<MnAttachmentDetails> mnAttachmentsList = null;
		try{
			
			mnAttachmentsList = iNoteDao.getAttachedFileInNote(listId, noteId, userId);
			if(mnAttachmentsList!=null && !mnAttachmentsList.isEmpty()){
				status = convertAttachedJsonObj(mnAttachmentsList);
			}
		}catch (Exception e) {
			logger.error("Exception while ge tAttac hBy FileId",e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" getAttachedFileInNote method returned result ");
		return ""+status;	
	}
	public String convertAttachedJsonObj(List<MnAttachmentDetails> typeList){
		try{
			if(logger.isDebugEnabled())
				logger.debug(" convertAttachedJsonObj method called");
			StringBuffer jsonStr = new StringBuffer("[");
			
			for (MnAttachmentDetails list : typeList){
				

				jsonStr.append("{\"attachId\" : \"" + list.getAttachId() + "\",");
				jsonStr.append("\"userId\" : \"" + list.getUserId() + "\",");
				jsonStr.append("\"listId\" : \"" + list.getListId() + "\",");
				jsonStr.append("\"noteId\" : \"" + list.getNoteId() + "\",");
				jsonStr.append("\"fileId\" : \"" + list.getFileId() + "\",");
				jsonStr.append("\"fullPath\" : \"" + list.getFullPath() + "\",");
				jsonStr.append("\"fileName\" : \"" + list.getFileName() + "\",");
				jsonStr.append("\"cDate\" : \"" + list.getaDate() + "\",");
				jsonStr.append("\"currentTime\" : \"" + list.getCurrentTime() + "\",");
				jsonStr.append("\"fileUploadName\" : \"" + list.getFileUploadName() + "\",");
				jsonStr.append("\"uploadDate\" : \"" + list.getUploadDate()+ "\",");
				jsonStr.append("\"cTime\" : \"" + list.getaTime() + "\",");
				jsonStr.append("\"status\" : \"" + list.getStatus() + "\"},");
			}
			jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
			jsonStr.append("]");
			if(logger.isDebugEnabled())
				logger.debug(" convertJsonToMnListObj method returned result ");
			return jsonStr.toString();
		}catch (Exception e) {
			logger.error("Exception while convert convertAttachedJsonObj  ",e);
		}
		return "";	
	}
	public String updateComments(String jsonStr,String listId,String noteId){
		JSONObject jsonObject=null;
		if(logger.isDebugEnabled())
			logger.debug(" updateComments method called "+noteId);
		String status = "";
		try{
			jsonObject = new JSONObject(jsonStr);
			String cmt = jsonObject.getString("comments");
			if(cmt.indexOf("`*`") != -1){
				cmt = cmt.replace("`*`", "\\\"");
			}
			status = iNoteDao.updateComments(jsonObject.getString("cId"), findUrlInComments(cmt), jsonObject.getString("editDate"), jsonObject.getString("editTime"), listId, noteId);
			
		}catch (Exception e) {
			logger.error("Exception while update comments ",e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" updateComments method returned result ");
		return status;	
	}
	
	public String deleteComments(String cId,String listId,String noteId,String type){
		String status ="";
		if(logger.isDebugEnabled())
			logger.debug(" deleteComments method called "+noteId);
		try{
			status =iNoteDao.deleteComments(cId, listId, noteId,type);
		}catch(Exception e){
			logger.error("Exception while delete comments ",e);
		}
		return ""+status;
	}

	@Override
	public String checkListMenuAccess(String params) {
		String status = "";
		String listId="";
		String userId="";
		String noteId="";
		String eventId="";
		if(logger.isDebugEnabled())
			logger.debug(" checkListMenuAccess method called");
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					listId=(String) jsonObject.get("listId");
					userId=(String) jsonObject.get("userId");
					if(jsonObject.has("noteId"))
						noteId=(String) jsonObject.get("noteId");
					else
						eventId=(String) jsonObject.get("eventId");
					if(listId!=null && !listId.equals("") && userId!=null && !userId.equals(""))
						if(noteId!="")
							status = iNoteDao.checkListMenuAccess(noteId,listId, userId,"notes");
						else if(eventId!="")
							status = iNoteDao.checkListMenuAccess(eventId,listId, userId,"events");
			}
		} catch (Exception e) {
			logger.error("Exception in Check list for Menu Access method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" checkListMenuAccess method returned result ");
		return status;
	}
	
	public String castUncastVotes(String params,String listId,String noteId,String userId){
		String status = "";
		if(logger.isDebugEnabled())
			logger.debug(" castUncastVotes method called "+userId+" noteId "+noteId);
		try {
			status = iNoteDao.castUncastVotes(params.trim(), listId, noteId,userId);

		} catch (Exception e) {
			logger.error("Exception in Cast Uncast Votes method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" castUncastVotes method returned result ");
		return status;
	}

	@Override
	public String changeNoteAccess(String params, String userId) {
		String status = "";
		String cmdStatus="";
		String listId="";
		String access="";
		String noteId="";
		String userName="";
		String date="";
		String CommentsID="";
		if(logger.isDebugEnabled())
			logger.debug(" changeNoteAccess method called "+userId);
		
		DateFormat formatter = new SimpleDateFormat("MMM/d/yyyy hh:mm a");
		Date dateApi=new Date();
		String apiDate=formatter.format(dateApi);
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					listId=(String) jsonObject.get("listId");
					access=(String) jsonObject.get("access");
					noteId=(String) jsonObject.get("noteId");
					userName=(String) jsonObject.get("publicUser");
					date=(String) apiDate;
					
					
					if(listId!=null && !listId.equals("") && access!=null && !access.equals(""))
					{
						status = iNoteDao.changeNoteAccess(listId,noteId,access.toLowerCase(),userId,userName,date);
						if(status!=null && !status.equalsIgnoreCase("")){
							JSONObject jsonObject1;
							jsonObject1 = new JSONObject(status);
							CommentsID=(String) jsonObject1.get("comments");
							cmdStatus = iNoteDao.updateCommentLevel(listId,noteId,CommentsID);
							
							String noteName=(String) jsonObject1.get("noteName");
							if(access.equals("public"))
							{
								MnUsers mnUsers=iNoteDao.getUserDetails(Integer.parseInt(userId));
								// sending mail to followers
								if(mnUsers!=null ){
									iNoteDao.sendingMailToFollower(mnUsers,noteName);
								}
							}
							
						}
					}
			}
		} catch (Exception e) {
			logger.error("Exception in check note access method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" changeNoteAccess method returned result ");
		return status;
	}

	@Override
	public String updateGroupsForNote(String groups, String noteId,String listId,String userId) {
		String status = "";
		String groupIds="";
		String noteType="";
		String fullName="";
		String noteLevel="";
		String firstName="";
		String noteName="";
		if(logger.isDebugEnabled())
			logger.debug(" updateGroupsForNote method called "+userId+" noteId: "+noteId);
		try {
			if (groups != null && !groups.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(groups);
					groupIds=(String) jsonObject.get("groups");
					noteType=(String) jsonObject.get("listType");
					noteLevel=(String) jsonObject.get("noteLevel");
					fullName=(String) jsonObject.get("fullName");
					firstName=(String) jsonObject.get("firstName");
					noteName=(String) jsonObject.get("noteName");
					if(noteName.indexOf("`*`") != -1){
						noteName = noteName.replace("`*`", "\\\"");
					}
					
					status = iNoteDao.updateGroupsForNote(groupIds, listId, noteId,userId,noteType,fullName,firstName,noteName,noteLevel);
			}
		} catch (Exception e) {
			logger.error("Exception in update groups for note method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" updateGroupsForNote method returned result ");
		return status;
	}

	@Override
	public String updateAllContactMembersForNote(String params) {
		String status = "";
		String userId="";
		String listId="";
		String noteId="";
		String noteType="";
		String noteLevel="";
		String fullName="";
		String firstName="";
		String noteName="";
		if(logger.isDebugEnabled())
			logger.debug(" updateAllContactMembersForNote method called");
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					userId=(String) jsonObject.get("userId");
					listId=(String) jsonObject.get("listId");
					noteId=(String) jsonObject.get("noteId");
					noteType=(String) jsonObject.get("listType");
					fullName=(String) jsonObject.get("fullName");
					firstName=(String) jsonObject.get("firstName");
					noteLevel=(String) jsonObject.get("noteLevel");
					noteName=(String) jsonObject.get("noteName");
					if(noteName.indexOf("`*`") != -1){
						noteName = noteName.replace("`*`", "\\\"");
					}
					status = iNoteDao.updateAllContactMembersForNote(userId, listId, noteId,noteType,fullName,firstName,noteName,noteLevel);
			}
		} catch (Exception e) {
			logger.error("Exception in Upadte All Contact Members For Note method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" updateAllContactMembersForNote method returned result ");
		return status;
	}

	@Override
	public String copyNoteToGroups(String groups, String noteId,String listId, String userId) {
		String status = "";
		String groupId="";
		String fullName="";
		String firstName="";
		String noteName="";
		boolean withComments=false;
		if(logger.isDebugEnabled())
			logger.debug(" copyNoteToGroups method called "+userId+" groups: "+groups);
		try {
			if (groups != null && !groups.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(groups);
					groupId=(String) jsonObject.get("groups");
					fullName=(String) jsonObject.get("fullName");
					firstName=(String) jsonObject.get("firstName");
					noteName=(String) jsonObject.get("noteName");
					
					
					withComments=(boolean) jsonObject.getBoolean("comments");
					List<Integer> groupIds=new ArrayList<Integer>();
					if(groupId!=null && !groupId.equals("")){
						String str[]=groupId.split(",");
						if(str.length != -1){
							for(int i=0;i<str.length;i++){
								groupIds.add(Integer.parseInt(str[i]));
							}
						}else{
							groupIds.add(Integer.parseInt(groupId));
						}
					}
					
					status = iNoteDao.copyNoteToGroups(groupIds, listId, noteId, withComments,userId,fullName,firstName,noteName);
			}
		} catch (Exception e) {
			logger.error("Exception in update groups for note method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" copyNoteToGroups method returned result ");
		return status;
	}

	@Override
	public String copyNoteToAllContact(String params) {
		String status = "";
		String userId="";
		String listId="";
		String noteId="";
		String fullName="";
		String firstName="";
		String noteName="";
		boolean withComments=false;
		if(logger.isDebugEnabled())
			logger.debug(" copyNoteToAllContact method called");
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					userId=(String) jsonObject.get("userId");
					listId=(String) jsonObject.get("listId");
					noteId=(String) jsonObject.get("noteId");
					fullName=(String) jsonObject.get("fullName");
					firstName=(String) jsonObject.get("firstName");
					noteName=(String) jsonObject.get("noteName");
					
					withComments=(boolean) jsonObject.getBoolean("comments");
					
					status = iNoteDao.copyNoteToAllContact(userId, listId, noteId, withComments,fullName,firstName,noteName);
			}
		} catch (Exception e) {
			logger.error("Exception in Copy Note To All Contact method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" copyNoteToAllContact method returned result ");
		return status;
	}

	@Override
	public String copyNoteToIndividual(String members, String noteId,String listId,String fromuserId) {
		String status = "";
		String userId="";
		String nonMailIds = ""; 
		String fullName="";
		String noteName="";
		String noteLevel="";
		boolean withComments=false;
		if(logger.isDebugEnabled())
			logger.debug(" copyNoteToIndividual method called "+fromuserId+" noteId: "+noteId);
		try {
			
					JSONObject jsonObject;
					jsonObject = new JSONObject(members);
					userId=(String) jsonObject.get("members");
					withComments=(boolean) jsonObject.getBoolean("comments");
					
					noteLevel=(String) jsonObject.get("noteLevel");
					nonMailIds =(String) jsonObject.get("nonMailIds");
					fullName =(String) jsonObject.get("fullName");
					noteName =(String) jsonObject.get("noteName");
					
					List<Integer> userIds=new ArrayList<Integer>();
					if(userId!=null && !userId.equals("")){
						String str[]=userId.split(",");
						if(str.length != -1){
							for(int i=0;i<str.length;i++){
								userIds.add(Integer.parseInt(str[i]));
							}
						}else{
							userIds.add(Integer.parseInt(userId));
						}
					}
					
					
					if(userId!= null && !userId.equals("")){
						status = iNoteDao.copyNoteToIndividual(userIds, listId, noteId,withComments,fromuserId);
					}
					if(nonMailIds!= null && !nonMailIds.equals("")){
						
						
						iNoteDao.copyBasedOnEmailId(nonMailIds, listId, Integer.parseInt(noteId), fromuserId, noteLevel,fullName,noteName,withComments);
					}
					
		
		} catch (Exception e) {
			logger.error("Exception in Copy Note In Individual method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" copyNoteToIndividual method returned result ");
		return status;
	}

	@Override
	public String getList(String params) {
		
		if(logger.isDebugEnabled())
			logger.debug(" getList method called");
		String userId = "";
		String noteAccess = "";
		String access="";
		List<MnList> mnListList = null;
		List<MnNotesDetails> details=new ArrayList<MnNotesDetails>();
	
		String jsonString = null;
		try {
			if (params != null && !params.equals("")) {
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				userId = (String) jsonObject.get("userId");
				noteAccess = (String) jsonObject.get("noteAccess");

				mnListList = iNoteDao.getList(userId, noteAccess);
			
				if(mnListList!=null && !mnListList.isEmpty())
				{
					for(MnList list:mnListList)
					{
						if(list.getMnNotesDetails()!=null && !list.getMnNotesDetails().isEmpty())
						{
							for(String str:list.getMnNotesDetails())
							{
								jsonObject = new JSONObject(str);
								access = (String) jsonObject.get("access");
								
								if(access!=null && !access.isEmpty() &&  access.contains("public"))
								{
								    MnNotesDetails mnNotesDetails=new MnNotesDetails();
								    mnNotesDetails.setNoteName((String) jsonObject.get("noteName"));
									mnNotesDetails.setStatus((String) jsonObject.get("status"));
									mnNotesDetails.setNoteId(Integer.parseInt((String) jsonObject.get("noteId")));
									mnNotesDetails.setAccess((String) jsonObject.get("access"));
									mnNotesDetails.setUserId(list.getUserId());
									mnNotesDetails.setListId(list.getListId().toString());
									details.add(mnNotesDetails);
								}
							}
							
						}
					}
					
				}
				if (details != null && !details.isEmpty())
				{
					jsonString = convertMnNotesDetailsToJson(details);
   
				}
				
			}
		} catch (Exception e) {
			logger.error(" error getList :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug(" getList method returned result ");
		return jsonString;
}

public IEventDao getiEventDao()
	{
		return iEventDao;
	}

	public void setiEventDao(IEventDao iEventDao)
	{
		this.iEventDao = iEventDao;
}
	
	private String convertMnNotesDetailsToJson(List<MnNotesDetails> mnNotesDetails) {

		if(logger.isDebugEnabled())
			logger.debug(" convertMnNotesDetailsToJson method called");
		StringBuffer jsonStr = new StringBuffer("[");
		try
		{
		for (MnNotesDetails details : mnNotesDetails)
		{
			jsonStr.append("{\"noteName\" : \"" + details.getNoteName() + "\",");
			jsonStr.append("\"noteId\" : \"" + details.getNoteId() + "\",");
			jsonStr.append("\"access\" : \"" + details.getAccess() + "\",");
			jsonStr.append("\"publicUser\" : \"" + details.getPublicUser() + "\",");
			jsonStr.append("\"publicDate\" : \"" + details.getPublicDate() + "\",");
			jsonStr.append("\"userId\" : \"" + details.getUserId() + "\",");
			jsonStr.append("\"comments\" : \"" + details.getComments() + "\",");
			jsonStr.append("\"listId\" : \"" + details.getListId() + "\",");
			jsonStr.append("\"tagIds\" : \"" + details.getTagIds() + "\",");
		    jsonStr.append("\"status\" : \"" + details.getStatus() + "\"},");
		    

		}
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		}
		catch (Exception e) {
			logger.error("convertMnNotesDetailsToJson "+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug(" convertMnNotesDetailsToJson method returned result ");
		return jsonStr.toString();
	}

	@Override
	public MnLog convertJsonToMnLogObj(String jsonStr) {
	
	JSONObject jsonObject;
	MnLog log=new MnLog();
	if(logger.isDebugEnabled())
		logger.debug(" convertJsonToMnLogObj method called");
	try
	{
		jsonObject=new JSONObject(jsonStr);
		log.setUserId(Integer.parseInt((String) jsonObject.get("userId")));
		log.setListId((String) jsonObject.get("listId"));
		log.setNoteId((String) jsonObject.get("noteId"));
		log.setPageType((String) jsonObject.get("pageType"));
		log.setLogDescription((String) jsonObject.get("logDescription"));
		log.setDate((String) jsonObject.get("date"));
		log.setTime((String) jsonObject.get("time"));
		log.setStatus((String) jsonObject.get("status"));
		log.setLogType((String) jsonObject.get("logType"));
		
		
		}
	catch (Exception e) {
	logger.info(" convert convertJsonToMnLogObj  New Note Log ','--->>"+e.getMessage() );
	}
	if(logger.isDebugEnabled())
		logger.debug(" convertJsonToMnLogObj method returned result ");
	return log;
	}

	@Override
	public String insertNewNoteLog(MnLog log) {
		String status = "";
	try
	{
		status=iNoteDao.insertNewNoteLog(log);
	}
	catch (Exception e) {
		logger.info(" Insert New Note Log ','--->>"+status );
	}
	return status;
	}

	@Override
	public MnVoteViewCount convertJsonToMnVoteViewCountObj(String jsonStr) {
		
		if(logger.isDebugEnabled())
			logger.debug(" convertJsonToMnVoteViewCountObj method called");
		JSONObject jsonObject;
		MnVoteViewCount mnVoteViewCount=new MnVoteViewCount();
		try
		{
			jsonObject=new JSONObject(jsonStr);
			mnVoteViewCount.setListId((String) jsonObject.get("listId"));
			mnVoteViewCount.setNoteId((String) jsonObject.get("noteId"));
			mnVoteViewCount.setVote(Integer.parseInt((String) jsonObject.get("vote")));
			mnVoteViewCount.setViewed(Integer.parseInt((String) jsonObject.get("viewed")));
			mnVoteViewCount.setStatus("A");
			
			}
		catch (Exception e) {
		logger.error(" error convertJsonToMnVoteViewCountObj','--->>"+e.getMessage() );
		}
		if(logger.isDebugEnabled())
			logger.debug(" convertJsonToMnVoteViewCountObj method returned result ");
		return mnVoteViewCount;
		}
	
	@Override
	public MnVoteViewCount convertJsonToUpdateMnVoteViewCountObj(String jsonStr) {
		
		JSONObject jsonObject;
		if(logger.isDebugEnabled())
			logger.debug(" convertJsonToUpdateMnVoteViewCountObj method called");
		MnVoteViewCount mnVoteViewCount=new MnVoteViewCount();
		try
		{
			jsonObject=new JSONObject(jsonStr);
			mnVoteViewCount.setListId((String) jsonObject.get("listId"));
			mnVoteViewCount.setNoteId((String) jsonObject.get("noteId"));
			mnVoteViewCount.setVote(Integer.parseInt((String) jsonObject.get("vote")));
			mnVoteViewCount.setViewed(Integer.parseInt((String) jsonObject.get("viewed")));
			mnVoteViewCount.setCountId(Integer.parseInt((String) jsonObject.get("countId")));
			mnVoteViewCount.setStatus("A");
			
			
			}
		catch (Exception e) {
		logger.error(" convertJsonToUpdateMnVoteViewCountObj ','--->>"+e.getMessage() );
		}
		if(logger.isDebugEnabled())
			logger.debug(" convertJsonToUpdateMnVoteViewCountObj method returned result ");
		return mnVoteViewCount;
		}

	@Override
	public String insertMostViewed(MnVoteViewCount mnVoteViewCount,String userId) {
		if(logger.isDebugEnabled())
			logger.debug(" insertMostViewed method called "+userId);
		String status = "";
		try
		{
			status=iNoteDao.insertMostViewed(mnVoteViewCount,userId);
		}
		catch (Exception e) {
			logger.error(" Insert Most Viewed  ','--->>"+e.getMessage() );
		}
		if(logger.isDebugEnabled())
			logger.debug(" insertMostViewed method returned result ");
		return status;
		}

	@Override
	public String fetchMostViewed(String params) {
		String userId = "";
		String noteAccess = "";
				
		List<MnNotesDetails> viewCounts=new ArrayList<MnNotesDetails>();
		if(logger.isDebugEnabled())
			logger.debug(" fetchMostViewed method called");
	
		String jsonString = null;
		try {
			if (params != null && !params.equals("")) {
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				userId = (String) jsonObject.get("userId");
				noteAccess = (String) jsonObject.get("noteAccess");
				
				viewCounts = iNoteDao.getMnMostViewed(userId, noteAccess);
				
				if (viewCounts != null && !viewCounts.isEmpty())
				{
					jsonString = convertMnVoteCountToJson(viewCounts);
   
				}

			}
		} catch (Exception e) {
			logger.error("fetchMostViewed :"+e.getMessage());
		}
		return jsonString;
}
	private String convertMnVoteViewCountToJson(List<MnVoteViewCount> viewCounts) {
		
		if(logger.isDebugEnabled())
			logger.debug(" convertMnVoteViewCountToJson method called");
		StringBuffer jsonStr = new StringBuffer("[");
		try
		{
		for (MnVoteViewCount details : viewCounts)
		{
			jsonStr.append("{\"countId\" : \"" + details.getCountId() + "\",");
			jsonStr.append("\"listId\" : \"" + details.getListId() + "\",");
			jsonStr.append("\"noteId\" : \"" + details.getNoteId() + "\",");
			jsonStr.append("\"vote\" : \"" + details.getVote()+ "\",");
		    jsonStr.append("\"viewed\" : \"" + details.getViewed() + "\"},");

		}
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		}catch (Exception e) {
			logger.error("convertMnVoteViewCountToJson :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug(" convertMnVoteViewCountToJson method returned result ");
		return jsonStr.toString();
	}

	
	private String convertMnVoteCountToJson(List<MnNotesDetails> viewCounts) {

		if(logger.isDebugEnabled())
			logger.debug(" convertMnVoteCountToJson method called");
		StringBuffer jsonStr = new StringBuffer("[");
		try
		{
		for (MnNotesDetails details : viewCounts)
		{
			jsonStr.append("{\"countId\" : \"" + details.getCountId() + "\",");
			jsonStr.append("\"listId\" : \"" + details.getListId() + "\",");
			jsonStr.append("\"noteId\" : \"" + details.getNoteId() + "\",");
			jsonStr.append("\"userId\" : \"" + details.getUserId() + "\",");
			jsonStr.append("\"vote\" : \"" + details.getVote()+ "\",");
			jsonStr.append("\"publicUser\" : \"" + details.getPublicUser()+ "\",");
			jsonStr.append("\"publicDate\" : \"" + details.getPublicDate()+ "\",");
			jsonStr.append("\"noteName\" : \"" + details.getNoteName()+ "\",");
			jsonStr.append("\"tagIds\" : \"" + details.getTagIds()+ "\",");
		    jsonStr.append("\"viewed\" : \"" + details.getViewed() + "\"},");
		    

		}
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		}catch (Exception e) {
			logger.info(" convertMnVoteCountToJson  "+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug(" convertMnVoteCountToJson method returned result ");
		return jsonStr.toString();
	}

	@Override
	public String updateMostViewed(MnVoteViewCount mnVoteViewCount,String userId) {
		String status = "";
		if(logger.isDebugEnabled())
			logger.debug(" updateMostViewed method called "+userId);
		try
		{
			status=iNoteDao.updateMostViewed(mnVoteViewCount,userId);
		}
		catch (Exception e) {
			logger.error(" Updated Most Viewed  ','--->>"+e.getMessage() );
		}
		return status;
		}

	@Override
	public String maxMostViewed(String params) {
		String noteId = "";
		String ListId = "";
		
		if(logger.isDebugEnabled())
			logger.debug(" maxMostViewed method called");
		
		List<MnVoteViewCount> viewCounts=new ArrayList<MnVoteViewCount>();
	
		String jsonString = null;
		try {
			if (params != null && !params.equals("")) {
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				noteId = (String) jsonObject.get("noteId");
				ListId = (String) jsonObject.get("listId");
				viewCounts = iNoteDao.maxMnMostViewed(noteId, ListId);
				if (viewCounts != null && !viewCounts.isEmpty())
				{
					jsonString = convertMnVoteViewCountToJson(viewCounts);
				}

			}
		} catch (Exception e) {
			logger.error("maxMostViewed "+e.getMessage());
		}
		return jsonString;
	}
	
	@Override
	public String checkOwnerOfList(String listId,String noteId,String userId){
		String status = "";
		if(logger.isDebugEnabled())
			logger.debug(" checkOwnerOfList method called "+userId+" noteId: "+noteId);
		try
		{
			status=iNoteDao.checkOwnerOfList(listId,noteId,userId);
		}
		catch (Exception e) {
			logger.error(" checkOwnerOfList  ','--->>"+e.getMessage() );
		}
		return status;
	}

	@Override
	public MnUsers getUserDetails(Integer userId)
	{
		if(logger.isDebugEnabled())
			logger.debug(" getUserDetails method called "+userId);
		MnUsers mnUsers=null;
		try{
			mnUsers=iNoteDao.getUserDetails(userId);
			
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return mnUsers ;
	}

	@Override
	public List<MnUsers> getUsersDetails(List<Integer> userIds)
	{
		List<MnUsers> mnUsers=null;
		if(logger.isDebugEnabled())
			logger.debug(" getUsersDetails method called");
		try{
			mnUsers=iNoteDao.getUsersDetails(userIds);
			
		}catch(Exception e){
			logger.error("getUsersDetails :"+e.getMessage());
		}
		
		return mnUsers;
	}
	public String getTags(String params) {
		String status = "";
		String userId="";
		if(logger.isDebugEnabled())
			logger.debug(" getTags method called");
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					
					userId=(String) jsonObject.get("userId");
					List<MnTag> tags = iNoteDao.getTags(userId);
					if(tags!=null && !tags.isEmpty()){
						status=convertToJsonTagList(tags);
					}
					
			}
		} catch (Exception e) {
			logger.error("Exception in getNoteDetails method  : ", e);
		}
		
		if(logger.isDebugEnabled())
			logger.debug(" getTags method returned result ");
		return status;
	}

	private String convertToJsonTagList(List<MnTag> mnTags) {

		if(logger.isDebugEnabled())
			logger.debug(" convertToJsonTagList method called");
		StringBuffer jsonStr = new StringBuffer("[");
		try
		{
		for (MnTag tag : mnTags)
		{
			jsonStr.append("{\"_id\" : \"" + tag.get_id() + "\",");
			jsonStr.append("\"tagId\" : \"" + tag.getTagId() + "\",");
			jsonStr.append("\"tagName\" : \"" + tag.getTagName() + "\",");
			jsonStr.append("\"userId\" : \"" + tag.getUserId() + "\",");
			jsonStr.append("\"createDate\" : \"" + tag.getCreateDate() + "\",");
			jsonStr.append("\"endDate\" : \"" + tag.getEndDate() + "\",");
			jsonStr.append("\"status\" : \"" + tag.getStatus()+ "\"},");
		}
		jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
		jsonStr.append("]");
		}catch (Exception e) {
			logger.error("convertToJsonTagList :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug(" convertToJsonTagList method returned result ");
		return jsonStr.toString();
	}
	
	@Override
	public String createTag(String params) {
		String status = "0";
		if(logger.isDebugEnabled())
			logger.debug(" createTag method called");
		Date date=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
		MnTag mnTag;
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					String tagName=(String) jsonObject.get("tagName");
					List<String> tagNames=new ArrayList<String>();
					if(tagName.contains(",")){
						String temp[]=tagName.split(",");
						for(int i=0;i<temp.length;i++){
							if(temp[i]!=null && !temp[i].equals(""))
								tagNames.add(temp[i].trim());
						}
					}else{
						tagNames.add(tagName);
					}
					if(tagNames!=null && !tagNames.isEmpty()){	
						String alreadyExistTagNames="";
						for(String str:tagNames){
							mnTag=new MnTag();
							mnTag.setTagName(str);
							mnTag.setUserId((Integer) jsonObject.get("userId"));
							mnTag.setCreateDate(sdf.format(date));
							mnTag.setStatus("A");
							status = iNoteDao.createTag(mnTag);
					
							if(status!=null && !status.isEmpty() && !status.equals("0")){
								iNoteDao.updateTagForNote(status, (String) jsonObject.get("listId"), (String) jsonObject.get("noteId"),mnTag.getUserId().toString(),(String) jsonObject.get("listType"));
							}else{
								if(alreadyExistTagNames.isEmpty())
									alreadyExistTagNames=alreadyExistTagNames+str;
								else
									alreadyExistTagNames=alreadyExistTagNames+","+str;
							}
						}
						if(!alreadyExistTagNames.isEmpty()){
							status="{\"tag\":\"" + alreadyExistTagNames + "\",\"status\":\"0\"}";
						}else{
							status="{\"status\":\"1\"}";
						}
					}
			}
		} catch (Exception e) {
			logger.error("Exception in Create Tag Method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" createTag method returned result ");
		return status;
	}

	@Override
	public String deleteTag(String params) {
		if(logger.isDebugEnabled())
			logger.debug(" deleteTag method called");
		String status = "";
		Integer tagId;
		String userId="";
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					
					userId=(String) jsonObject.get("userId");
					tagId=(Integer) jsonObject.get("tagId");
					status = iNoteDao.deleteTag(userId, tagId);
			}
		} catch (Exception e) {
			logger.error("Exception in Delete Tag method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" deleteTag method returned result ");
		return status;
	}

	@Override
	public String updateTag(String params,String listId,String noteId) {
		String status = "";
		Integer tagId;
		String tagName="";
		String userId="";
		if(logger.isDebugEnabled())
			logger.debug(" updateTag method called noteId: "+noteId);
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					
					userId=(String) jsonObject.get("userId");
					tagName=(String) jsonObject.get("tagName");
					tagId=(Integer) jsonObject.get("tagId");
					status = iNoteDao.updateTag(userId, tagName, tagId,listId,noteId);
			}
		} catch (Exception e) {
			logger.error("Exception in Update Tag method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" updateTag method returned result ");
		return status;
	}

	@Override
	public String updateTagForNote(String params) {
		String status = "";
		String tagId="";
		String noteId="";
		String listId="";
		String userId="";
		String listType="";
		if(logger.isDebugEnabled())
			logger.debug(" updateTagForNote method called");
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					tagId=(String) jsonObject.get("tagId");
					listId=(String) jsonObject.get("listId");
					noteId=(String) jsonObject.get("noteId");
					userId=(String) jsonObject.get("userId");
					listType=(String) jsonObject.get("listType");
					status = iNoteDao.updateTagForNote(tagId, listId, noteId,userId,listType);
			}
		} catch (Exception e) {
			logger.error("Exception in Update Tag For Note method  : ", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" updateTagForNote method returned result ");
		return status;
	}

	
	@Override
	public String recentNotes(String params) {
		if(logger.isDebugEnabled())
			logger.debug(" recentNotes method called");
		 String userId = "";
		 String noteAccess = "";
		 String access="";
		 String startDate="";
		 String publicUserName;
		 String status="";
		 List<MnList> mnListList = null;
		 String loginUserZone=null;
		 List<MnNotesDetails> details=new ArrayList<MnNotesDetails>();
		 DateFormat df = new SimpleDateFormat("M/dd/yyyy"); 
		 
    	Date date1 = new Date();
    	@SuppressWarnings("unused")
		String currenytDate=df.format(date1);
		String jsonString = null;
		String tdate=null;
		String loginTimeZone=null;
		try {
			if (params != null && !params.equals("")) {
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				userId = (String) jsonObject.get("userId");
				noteAccess = (String) jsonObject.get("noteAccess");

				mnListList = iNoteDao.getList(userId, noteAccess);
				if(mnListList!=null && !mnListList.isEmpty())
				{
					for(MnList list:mnListList)
					{
						
						
						if(list.getMnNotesDetails()!=null && !list.getMnNotesDetails().isEmpty())
						{
							
							
							for(String str:list.getMnNotesDetails())
							{
								jsonObject = new JSONObject(str);
								access = (String) jsonObject.get("access");
								startDate=(String) jsonObject.get("startDate");
								status = (String) jsonObject.get("status");
								
								Date recentDate =null;
								String jdate=(String)jsonObject.get("publicDate");
								if(jdate !=null && !jdate.equals(""))
								{
								tdate=iNoteDao.getTimeZone(jdate,userId);
								}
								String convertedDate=null;
							
								recentDate= new Date(startDate) ;
								if(status.equals("A")  && access!=null && !access.isEmpty() &&  access.contains("public"))
								{
									
								    MnNotesDetails mnNotesDetails=new MnNotesDetails();
								    mnNotesDetails.setNoteName(changedNoteNameWithDouble((String) jsonObject.get("noteName")));
									mnNotesDetails.setStatus((String) jsonObject.get("status"));
									mnNotesDetails.setNoteId(Integer.parseInt((String) jsonObject.get("noteId")));
									mnNotesDetails.setAccess((String) jsonObject.get("access"));
									/// for public user change
									String publicUserId=(String) jsonObject.get("publicUser");
									boolean isUserId = publicUserId.matches("[0-9]*");
									if(isUserId){
									MnUsers mnUsers=getUserDetails(Integer.parseInt(publicUserId));
									publicUserName=mnUsers.getUserFirstName()+" "+mnUsers.getUserLastName();
									}else{
										publicUserName=(String) jsonObject.get("publicUser");
									}
									
									mnNotesDetails.setPublicUser(publicUserName);
									mnNotesDetails.setPublicDate(tdate);
									mnNotesDetails.setUserId(list.getUserId());
									mnNotesDetails.setStartDate(recentDate);
									mnNotesDetails.setListId(list.getListId().toString());
									mnNotesDetails.setTagIds((String)jsonObject.get("privateTags"));
									details.add(mnNotesDetails);
									
								}
							}
							
						}
					}
					
				}
				
				if (details != null && !details.isEmpty())
				{
					Collections.sort(details, SENIORITY_ORDER_CROWD);
					jsonString = convertMnNotesDetailsToJson(details);
				}
			}
		} catch (Exception e) {
			logger.error(" recentNotes :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug(" recentNotes method returned result ");
		return jsonString;

}
	static final Comparator<MnNotesDetails> SENIORITY_ORDER_CROWD = new Comparator<MnNotesDetails>() {
		@SuppressWarnings("deprecation")
		public int compare(MnNotesDetails e1, MnNotesDetails e2) {
			Date i1=null,i2=null;
			try {
				 i1 = new Date(e1.getPublicDate());
				 i2 = new Date(e2.getPublicDate());
				 return i2.compareTo(i1);
				
			} catch (Exception e) {
				i1 = new Date(e1.getPublicDate());
				i2 = new Date(e2.getPublicDate());
				return i2.compareTo(i1);
			}
			
		}
	};
	

	@SuppressWarnings("deprecation")
	@Override
	public String dateSearchNotes(String params) {
		if(logger.isDebugEnabled())
			logger.debug(" dateSearchNotes method called");
		String userId = "";
		String noteAccess = "";
		String access="";
		String startDate="";
		String publicUserName;
		List<MnList> mnListList = null;
		List<MnNotesDetails> details=new ArrayList<MnNotesDetails>();
		DateFormat df = new SimpleDateFormat("M/dd/yyyy"); 
	    String tDate=null;	
		String jsonString = null;
		try {
			if (params != null && !params.equals(""))
			{
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				userId = (String) jsonObject.get("userId");
				noteAccess = (String) jsonObject.get("noteAccess");
				startDate=(String)jsonObject.get("startDate");

				mnListList = iNoteDao.getList(userId, noteAccess);
				
				if(mnListList!=null && !mnListList.isEmpty())
				{
					for(MnList list:mnListList)
					{
										
						if(list.getMnNotesDetails()!=null && !list.getMnNotesDetails().isEmpty())
						{
					
							for(String str:list.getMnNotesDetails())
							{
								jsonObject = new JSONObject(str);
								access = (String) jsonObject.get("access");
								String startDate1=(String) jsonObject.get("startDate");
								String jdate=(String)jsonObject.get("publicDate");
								
								tDate=iNoteDao.getTimeZone(jdate,userId);
								Date recentDate=new Date(startDate1);
								String startDates=df.format(recentDate);
								if(access!=null && !access.isEmpty() &&  access.contains("public"))
								{
									if(startDate.equalsIgnoreCase(startDates))
									{
								    MnNotesDetails mnNotesDetails=new MnNotesDetails();
								    mnNotesDetails.setNoteName((String) jsonObject.get("noteName"));
									mnNotesDetails.setStatus((String) jsonObject.get("status"));
									mnNotesDetails.setNoteId(Integer.parseInt((String) jsonObject.get("noteId")));
									mnNotesDetails.setAccess((String) jsonObject.get("access"));
									/// for public user change
									String publicUserId=(String) jsonObject.get("publicUser");
									boolean isUserId = publicUserId.matches("[0-9]*");
									if(isUserId){
									MnUsers mnUsers=getUserDetails(Integer.parseInt(publicUserId));
									publicUserName=mnUsers.getUserFirstName()+" "+mnUsers.getUserLastName();
									}else{
										publicUserName=(String) jsonObject.get("publicUser");
									}
									
									mnNotesDetails.setPublicUser(publicUserName);
									mnNotesDetails.setPublicDate(tDate);
									mnNotesDetails.setUserId(list.getUserId());
									mnNotesDetails.setListId(list.getListId().toString());
									details.add(mnNotesDetails);
									}
									
								}
							}
							
						}
					}
					
				}
			
				if (details != null && !details.isEmpty())
				{
					jsonString = convertMnNotesDetailsToJson(details);
				}
			}
		} catch (Exception e) {
			logger.error(" dateSearchNotes :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug(" dateSearchNotes method returned result ");
		return jsonString;
}

	@Override
	public String MostVieweddateSearchNotes(String params) {
		if(logger.isDebugEnabled())
			logger.debug(" MostVieweddateSearchNotes method called");
		String userId = "";
		String noteAccess = "";
		String startDate="";
		List<MnNotesDetails> viewCounts=new ArrayList<MnNotesDetails>();
	
		String jsonString = null;
		try {
			if (params != null && !params.equals("")) {
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				userId = (String) jsonObject.get("userId");
				noteAccess = (String) jsonObject.get("noteAccess");
				startDate=(String)jsonObject.get("startDate");
				viewCounts = iNoteDao.getMostViewedDateSearch(userId, noteAccess,startDate);
				
				if (viewCounts != null && !viewCounts.isEmpty())
				{
					jsonString = convertMnVoteCountToJson(viewCounts);
				}
			}
		} catch (Exception e) {
			logger.error("MostVieweddateSearchNotes :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug(" MostVieweddateSearchNotes method returned result ");
		return jsonString;
}

	
	@Override
	public String MostVoted(String params) {
		if(logger.isDebugEnabled())
			logger.debug(" MostVoted method called");
		String userId = "";
		String noteAccess = "";
				
		List<MnNotesDetails> viewCounts=new ArrayList<MnNotesDetails>();
		String jsonString = null;
		try {
			if (params != null && !params.equals("")) {
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				userId = (String) jsonObject.get("userId");
				noteAccess = (String) jsonObject.get("noteAccess");
				
				viewCounts = iNoteDao.getMnMostVoted(userId, noteAccess);
				
				if (viewCounts != null && !viewCounts.isEmpty())
				{
					jsonString = convertMnVoteCountToJson(viewCounts);
				}
				
			}
		} catch (Exception e) {
			logger.info("MostVoted :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug(" MostVoted  method returned result ");
		return jsonString;
	}

	@Override
	public String MostVoteddateSearchNotes(String params) {
		if(logger.isDebugEnabled())
			logger.debug(" MostVoteddateSearchNotes method called");
		String userId = "";
		String noteAccess = "";
		String startDate="";
		List<MnNotesDetails> viewCounts=new ArrayList<MnNotesDetails>();
		String jsonString = null;
		try {
			if (params != null && !params.equals("")) {
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				userId = (String) jsonObject.get("userId");
				noteAccess = (String) jsonObject.get("noteAccess");
				startDate=(String)jsonObject.get("startDate");
			
				viewCounts = iNoteDao.getMostVotedDateSearch(userId, noteAccess,startDate);
				
				if (viewCounts != null && !viewCounts.isEmpty())
				{
					jsonString = convertMnVoteCountToJson(viewCounts);
				}
			}
		} catch (Exception e) {
			logger.error("MostVoteddateSearchNotes :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug(" MostVoteddateSearchNotes method returned result ");
		return jsonString;
	}
	@Override
	public String getListBasedOnListId(String listId, String params)
	{
		if(logger.isDebugEnabled())
			logger.debug(" getListBasedOnListId method called listId:"+listId);
		String userId="";
		String listType="";
		String multiFilterBaseTagId="";
		String multiFilterBaseNoteName="";
		List<MnList> mnListList = null;
		List<MnNoteDetails> mnNoteDetails = null;
		List<MnNoteDetails> searchResults = null;
		String jsonString = "";
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					userId=(String) jsonObject.get("userId");
					listType=(String) jsonObject.get("listType");
					multiFilterBaseTagId=(String) jsonObject.get("tagId");
					multiFilterBaseNoteName=(String) jsonObject.get("noteName");
					Boolean flag=false;
					//Both TagId And NoteName Empty
					MnList mnList = iEventDao.getMnList(listId);
					if(multiFilterBaseTagId!=null && multiFilterBaseTagId.isEmpty() && multiFilterBaseNoteName!=null && multiFilterBaseNoteName.isEmpty()){
						mnNoteDetails=iNoteDao.getListBasedOnListId(userId, listId, listType);
					}
					//TagId Not Empty and NoteName Empty
					else if(multiFilterBaseTagId!=null && !multiFilterBaseTagId.isEmpty() && multiFilterBaseNoteName!=null && multiFilterBaseNoteName.isEmpty()){
						mnNoteDetails = iNoteDao.fetchSharedNotesBasedTag(userId, listType,multiFilterBaseTagId,multiFilterBaseNoteName,listId);
					}
					//TagId Empty and NoteName Not Empty
					else if(multiFilterBaseTagId!=null && multiFilterBaseTagId.isEmpty() && multiFilterBaseNoteName!=null && !multiFilterBaseNoteName.isEmpty()){
						//schedule mutli search
						mnNoteDetails = iNoteDao.getListBasedOnListId(userId, listId, listType);
						
						//Individual And All Contacts Shared Note Fetch
						if(mnNoteDetails!=null && !mnNoteDetails.isEmpty()){
							List<MnNoteDetails> tempList = iNoteDao.fetchSharingNotesBasedOnListId(userId,listType,listId);
							if(tempList!=null && !tempList.isEmpty()){
								mnNoteDetails.addAll(tempList);
							}
						}else{ 
							mnNoteDetails = iNoteDao.fetchSharingNotesBasedOnListId(userId,listType,listId);
						}
						
						//Group Shared Note Fetch
						if(mnNoteDetails!=null && !mnNoteDetails.isEmpty()){
							List<MnNoteDetails> tempList= iNoteDao.fetchGroupSharedNotesBasedOnListId(userId, listType, listId);
							if(tempList!=null && !tempList.isEmpty()){
								mnNoteDetails.addAll(tempList);
							}
						}else{
							mnNoteDetails = iNoteDao.fetchGroupSharedNotesBasedOnListId(userId, listType, listId);
						}
						
						//Filter Book List Based on Key word.
						if(mnNoteDetails!=null && !mnNoteDetails.isEmpty()){
							searchResults = iNoteDao.byKewordConvertMnListTo(mnNoteDetails,multiFilterBaseNoteName,listType);
							
							if(searchResults!= null && !searchResults.isEmpty())
								Collections.sort(searchResults, SENIORITY_ORDER_NOTE);
								jsonString = convertToJsonNoteDetailsToSingleMnList(searchResults,mnList);
						}
						flag=true;
					}
					//Both TagId And NoteName Note Empty
					else{
						mnNoteDetails = iNoteDao.fetchSharedNotesBasedTag(userId, listType,multiFilterBaseTagId,multiFilterBaseNoteName,listId);
						
						searchResults = iNoteDao.byKewordConvertMnListTo(mnNoteDetails,multiFilterBaseNoteName,listType);
						if(searchResults!= null && !searchResults.isEmpty())
							Collections.sort(searchResults, SENIORITY_ORDER_NOTE);
						
						jsonString = convertToJsonNoteDetailsToSingleMnList(searchResults,mnList);
						flag=true;
					}
					if(!flag){
						if(mnNoteDetails!=null && !mnNoteDetails.isEmpty()){
							
								Collections.sort(mnNoteDetails, SENIORITY_ORDER_NOTE);
								jsonString=convertToJsonNoteDetailsToSingleMnList(mnNoteDetails,mnList);
						}else{
							if(multiFilterBaseTagId!=null && multiFilterBaseTagId.isEmpty() && multiFilterBaseNoteName!=null && multiFilterBaseNoteName.isEmpty()){
								mnNoteDetails=iNoteDao.fetchSharingNotesBasedOnListId(userId,listType,listId);
								if(mnNoteDetails!=null && !mnNoteDetails.isEmpty()){
									Collections.sort(mnNoteDetails, SENIORITY_ORDER_NOTE);
									jsonString=convertToJsonNoteDetailsToSingleMnList(mnNoteDetails,mnList);
								}else{
									mnNoteDetails = iNoteDao.fetchGroupSharedNotesBasedOnListId(userId, listType, listId);
									if(mnNoteDetails!=null && !mnNoteDetails.isEmpty()){
										Collections.sort(mnNoteDetails, SENIORITY_ORDER_NOTE);
										jsonString=convertToJsonNoteDetailsToSingleMnList(mnNoteDetails,mnList);
									}
								}
							}
						}
					}
					if(!jsonString.equals("")){
						jsonString=jsonString+"]";
					}
			}
		}catch(Exception e){
			logger.error("Error while getListBasedOnListId "+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug(" getListBasedOnListId method returned result ");
		return jsonString;
	}
	
	@Override
	public String fetchTaggedBasedNotes(String params) {
		
		if(logger.isDebugEnabled())
			logger.debug(" fetchTaggedBasedNotes method called");
		
		String userId="";
		String listType="";
		String tagId="";
		String multiFilterBaseNoteName="";
		String multiFilterBaseListId="";
		List<MnList> mnListList = null;
		List<MnNoteDetails> mnNoteDetails = null;
		List<MnNoteDetails> searchResults = null;
		String jsonString = "";
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					userId=(String) jsonObject.get("userId");
					listType=(String) jsonObject.get("listType");
					tagId=(String) jsonObject.getString("tagId");
					multiFilterBaseListId=(String) jsonObject.getString("listId");
					multiFilterBaseNoteName=(String) jsonObject.getString("noteName");
					
					//fetch Tagged Note and list based on tagId 				
					if((multiFilterBaseListId!=null && !multiFilterBaseListId.isEmpty()) || (multiFilterBaseNoteName!=null && !multiFilterBaseNoteName.isEmpty())){
						mnNoteDetails = iNoteDao.fetchSharedNotesBasedTag(userId, listType,tagId,multiFilterBaseNoteName,multiFilterBaseListId);
						if(multiFilterBaseNoteName!=null && !multiFilterBaseNoteName.isEmpty()){
							searchResults = iNoteDao.byKewordConvertMnListTo(mnNoteDetails,multiFilterBaseNoteName,listType);
							if(searchResults!= null && !searchResults.isEmpty())
								Collections.sort(searchResults, SENIORITY_ORDER_NOTE);
							
							jsonString = convertToJsonNoteDetailsToMnList(searchResults);
						}else if (mnNoteDetails != null && !mnNoteDetails.isEmpty())
						{
							Collections.sort(mnNoteDetails, SENIORITY_ORDER_NOTE);
							jsonString = convertToJsonNoteDetailsToMnList(mnNoteDetails);
						}
					}else{ 
						mnNoteDetails = iNoteDao.fetchSharedNotesBasedTag(userId, listType,tagId,multiFilterBaseNoteName,multiFilterBaseListId);
						if (mnNoteDetails != null && !mnNoteDetails.isEmpty())
						{
							Collections.sort(mnNoteDetails, SENIORITY_ORDER_NOTE);
							jsonString = convertToJsonNoteDetailsToMnList(mnNoteDetails);
						}	
					}
					if(!jsonString.equals("")){
						jsonString=jsonString+"]";
					}
			}
		} catch (Exception e) {
			logger.error("Exception in Fetching Tagged List method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" fetchTaggedBasedNotes method returned result ");
		return jsonString;
	}
	@Override
	public String addRemainder(MnRemainders mnRemainders) {
		String status=""; 
		if(logger.isDebugEnabled())
			logger.debug(" addRemainder method called");
		try{
			status= iNoteDao.addRemainder(mnRemainders,mnRemainders.getListId(),mnRemainders.getNoteId(),mnRemainders.getUserId());
			
		}catch (Exception e) {
			logger.error("Exception in adding remainders :", e);
		}
		return status;
	}
	public MnRemainders convertJsonToMnRwmainderObj(String jsonStr) {
		MnRemainders mnRemainders = new MnRemainders();
		if(logger.isDebugEnabled())
			logger.debug(" convertJsonToMnRwmainderObj method called");
		@SuppressWarnings("unused")
		Set<String> mnNotesDetails=new HashSet<String>();
		if (jsonStr != null && !jsonStr.equals("")) {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(jsonStr);
				mnRemainders.setrId(1);
				mnRemainders.setListId((String) jsonObject.get("listId"));
				mnRemainders.setNoteId((String) jsonObject.get("noteId"));
				mnRemainders.setUserId(Integer.parseInt((String)jsonObject.get("userId")));
				mnRemainders.setrName((String) jsonObject.get("rName"));
				mnRemainders.setStatus((String)jsonObject.get("status"));
				mnRemainders.setcDate((String) jsonObject.get("cDate"));
				mnRemainders.setEventDate((String) jsonObject.get("eventDate"));
				mnRemainders.setEventTime((String) jsonObject.get("eventTime"));
				mnRemainders.setSendMail(jsonObject.getBoolean("sendMail"));
				mnRemainders.setEdited(Integer.parseInt((String) jsonObject.get("edited")));
				mnRemainders.setEditDate((String) jsonObject.get("editDate"));
				mnRemainders.setInactiveRemainderUserId(new ArrayList<Integer>());
			}
			catch (Exception e) {
				logger.error("Exception while converting convertJsonToMnRwmainderObj object !", e);
			}
		}
		if(logger.isDebugEnabled())
			logger.debug(" convertJsonToMnRwmainderObj method returned result ");
		return mnRemainders;
	}

	@Override
	public String deleteRemainder(String rId, String listId, String noteId) {
		String status=""; 
		if(logger.isDebugEnabled())
			logger.debug(" deleteRemainder method called listId: "+listId+" noteId "+noteId);
		try{
			status =iNoteDao.deleteRemainder(rId, listId, noteId);
		}catch(Exception e){
			logger.error("Exception while delete remainders ",e);
		}
		return status;
	}

	@Override
	public String getRemaindersList(String params,String userId) {
		String status="";
		if(logger.isDebugEnabled())
			logger.debug(" getRemaindersList method called "+userId);
		List<MnRemainders> mnRemaindersList=null;
		try{
			String[] paramArray=params.split(",");
			List<Integer> paramsList = new ArrayList<Integer>();
			if(paramArray.length >0 && paramArray[0]!=null){
				for(String par:paramArray){
					paramsList.add(Integer.parseInt(par.trim()));
				}
			}
			mnRemaindersList = iNoteDao.getRemaindersList(paramsList);
			if(mnRemaindersList!=null && !mnRemaindersList.isEmpty()){
				status = convertMnRamindObjectToJson(mnRemaindersList,Integer.parseInt(userId));
			}
		}catch (Exception e) {
			logger.error("Exception in get remainders List into json ",e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" getRemaindersList method returned result ");
		return status;
	}

	@Override
	public String updateRemainders(String params, String listId, String noteId) {
		String status="";
		if(logger.isDebugEnabled())
			logger.debug(" updateRemainders method called noteId: "+noteId);
		JSONObject jsonObject=null;
		try{
			jsonObject = new JSONObject(params);
			status = iNoteDao.updateRemainders(jsonObject.getString("rId"), jsonObject.getString("rName"), jsonObject.getString("eventDate"),  jsonObject.getString("eventTime"),  jsonObject.getBoolean("sendMail"),jsonObject.getString("editDate"), listId, noteId,jsonObject.getString("userId"));
			
		}catch (Exception e) {
			logger.error("Exception while update remaind ",e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" updateRemainders method returned result ");
		return status;	
	
	}

	public String convertMnRamindObjectToJson(List<MnRemainders> mnRemainderList,Integer userId){
		try{
			StringBuffer jsonStr = new StringBuffer("[");
			if(logger.isDebugEnabled())
				logger.debug(" convertMnRamindObjectToJson method called "+userId);
			for (MnRemainders list : mnRemainderList)
			{
				JSONObject jsonObject = null;
				String name=iNoteDao.getListNoteNamebyId( list.getListId(), list.getNoteId());
				if(name!= null && !name.equals("")){
					jsonObject = new JSONObject(name);
				}
				jsonStr.append("{\"rId\" : \"" + list.getrId() + "\",");
				jsonStr.append("\"userId\" : \"" + list.getUserId() + "\",");
				jsonStr.append("\"listId\" : \"" + list.getListId() + "\",");
				jsonStr.append("\"listName\" : \"" + jsonObject.get("listName") + "\",");
				jsonStr.append("\"listType\" : \"" + jsonObject.get("listType") + "\",");
				jsonStr.append("\"noteId\" : \"" + list.getNoteId() + "\",");
				jsonStr.append("\"noteName\" : \"" + changedNoteNameWithDouble((String) jsonObject.get("noteName"))+ "\",");
				jsonStr.append("\"rName\" : \"" + list.getrName() + "\",");
				
				if(list.getUserId().equals(userId))
				{
					jsonStr.append("\"eventDate\" : \"" + list.getEventDate() + "\",");
					
					SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
				    Date dateObj = sdf.parse(list.getEventTime());
				    String time=new SimpleDateFormat("h:mm a").format(dateObj);
					jsonStr.append("\"eventTime\" : \"" + time + "\",");
				}else
				{
					String date=list.getEventDate();
					String time=list.getEventTime();
					date=date+" "+time;
					String stDate = iEventDao.convertedZoneEvents(userId, list.getUserId(), date);
					String arr[]=stDate.split(" ");
					jsonStr.append("\"eventDate\" : \"" + arr[0] + "\",");
					SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
				    Date dateObj = sdf.parse(arr[1].toString());
				    time=new SimpleDateFormat("h:mm a").format(dateObj);
					jsonStr.append("\"eventTime\" : \"" + time + "\",");
				}
				
				
				jsonStr.append("\"sendMail\" : " + list.isSendMail() + ",");
				jsonStr.append("\"cDate\" : \"" + list.getcDate() + "\",");
				jsonStr.append("\"edited\" : \"" + list.getEdited() + "\",");
				jsonStr.append("\"editDate\" : \"" + list.getEditDate() + "\",");
				jsonStr.append("\"inactiveRemainderUserId\" : \"" + list.getInactiveRemainderUserId().toString() + "\",");
				jsonStr.append("\"status\" : \"" + list.getStatus() + "\"},");
				
			}
			jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
			jsonStr.append("]");
			return jsonStr.toString();
		}catch (Exception e) {
			logger.error("Exception while convert MNRemainders List into json ",e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" convertMnRamindObjectToJson method returned result ");
		return "";
	}

	@Override
	public String fetchDueDateBasedNotes(String params) {
		if(logger.isDebugEnabled())
			logger.debug(" fetchDueDateBasedNotes method called");
		String userId="";
		String listType="";
		String dueDate="";
		List<MnList> mnListList = null;
		String jsonString = null;
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					userId=(String) jsonObject.get("userId");
					listType=(String) jsonObject.get("listType");
					dueDate=(String) jsonObject.get("dueDate");
					
					//fetch Note and list based on Duedate 
						mnListList = iNoteDao.fetchNotesBasedDueDate(userId, listType,dueDate);
					
					if (mnListList != null && !mnListList.isEmpty())
					{
						jsonString = convertToJsonListOfList(mnListList);
					}
			}
		} catch (Exception e) {
			logger.error("Exception in Fetching Duedate Based List method  : ", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" fetchDueDateBasedNotes method returned result ");
		return jsonString;
	}

	
	@Override
	public String deleteMembersForBook(String params) {
		if(logger.isDebugEnabled())
			logger.debug(" deleteMembersForBook method called");
		String status = "";
		String userIds="";
		String listId="";
		String userId="";
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					userIds=(String) jsonObject.get("members");
					listId=(String) jsonObject.get("listId");
					userId=(String) jsonObject.get("userId");
					
					 status = iNoteDao.deleteMembersForBook(userIds,listId,userId);
			}
		} catch (Exception e) {
			logger.error("Exception in Delete Members In Book method  : ", e);

		}
		return status;
	}

	@Override
	public String deletegroupsForBook(String params) {
		if(logger.isDebugEnabled())
			logger.debug(" deletegroupsForBook method called");
		String status = "";
		String groupIds="";
		String listId="";
		String userId="";
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					groupIds=(String) jsonObject.get("groupid");
					listId=(String) jsonObject.get("listId");
					userId=(String) jsonObject.get("userId");
					
					status = iNoteDao.deletegroupForBook(groupIds,listId,userId);
			}
		} catch (Exception e) {
			logger.error("Exception in Delete Group In Book method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" deletegroupsForBook method returned result ");
		return status;
	}

	@Override
	public String updateGroupsForBook(String params) {
		String status = "";
		String groupIds="";
		String noteType="";
		String listId="";
		String userId="";
		String noteLevel="";
		String fullName="";
		String firstName="";
		String bookName="";
		if(logger.isDebugEnabled())
			logger.debug(" updateGroupsForBook method called");
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					groupIds=(String) jsonObject.get("groups");
					noteType=(String) jsonObject.get("listType");
					listId=(String) jsonObject.get("listId");
					userId=(String) jsonObject.get("userId");
					noteLevel=(String) jsonObject.get("noteLevel");
					fullName=(String) jsonObject.get("fullName");
					firstName=(String) jsonObject.get("firstName");
					bookName=(String) jsonObject.get("bookName");
					
					status = iNoteDao.updateGroupsForBook(groupIds,listId,userId,noteType,fullName,firstName,bookName,noteLevel);
			}
		} catch (Exception e) {
			logger.error("Exception in Update Groups For Book method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" updateGroupsForBook method returned result ");
		return status;
	}

	@Override
	public String updateMembersForBook(String params) {
		String status = "";
		String userIds="";
		String noteType="";
		String listId="";
		String userId="";
		String noteLevel="";
		String nonMailIds = ""; 
		String fullName="";
		String noteName="";
		Integer noteId=0;
		if(logger.isDebugEnabled())
			logger.debug(" updateMembersForBook method called");
		
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					userIds=(String) jsonObject.get("members");
					noteType=(String) jsonObject.get("listType");
					listId=(String) jsonObject.get("listId");
					userId=(String) jsonObject.get("userId");
					noteLevel=(String) jsonObject.get("noteLevel");
					nonMailIds =(String) jsonObject.get("nonMailIds");
					fullName =(String) jsonObject.get("fullName");
					noteName =(String) jsonObject.get("noteName");
					
					
					if(userIds!= null && !userIds.equals("")){
						status = iNoteDao.updateMembersForBook(userIds,listId,userId,noteType,noteLevel);
					}
					if(nonMailIds!= null && !nonMailIds.equals("")){
						iNoteDao.sahreBasedOnEmailId(nonMailIds, listId, noteId, userId, noteLevel,fullName,noteName,noteType);
					}
					
					
			}
		} catch (Exception e) {
			logger.error("Exception in Update Members For Book method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" updateMembersForBook method returned result ");
		return status;
	}

	@Override
	public String updateAllContactMembersForBook(String params) {
		String status = "";
		String userId="";
		String listId="";
		String noteType="";
		String noteLevel="";
		String fullName="";
		String firstName="";
		String bookName="";
		if(logger.isDebugEnabled())
			logger.debug(" updateAllContactMembersForBook method called");
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					userId=(String) jsonObject.get("userId");
					listId=(String) jsonObject.get("listId");
					noteType=(String) jsonObject.get("listType");
					fullName=(String) jsonObject.get("fullName");
					firstName=(String) jsonObject.get("firstName");
					bookName=(String) jsonObject.get("bookName");
					noteLevel=(String) jsonObject.get("noteLevel");
					status = iNoteDao.updateAllContactMembersForBook(userId, listId,noteType,fullName,firstName,bookName,noteLevel);
			}
		} catch (Exception e) {
			logger.error("Exception in Insert All Contact Members Share For Book method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" updateAllContactMembersForBook method returned result ");
		return status;
	}
	
	@Override
	public String getVotesBasedNote(String listId,String noteId){
		String status="";
		if(logger.isDebugEnabled())
			logger.debug(" getVotesBasedNote method called noteId: "+noteId);
		try{
			status = iNoteDao.getVotesBasedNote(listId, noteId);
		}catch (Exception e) {
			logger.error("Exception in getVotesBasedNote method  : ", e);
		}
		return status;
	}
	@Override
	public String deleteAllContactMembersForNote(String params) {
		String status = "";
		String userId="";
		String listId="";
		String noteId="";
		String noteType="";
		if(logger.isDebugEnabled())
			logger.debug(" deleteAllContactMembersForNote method called");
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					userId=(String) jsonObject.get("userId");
					listId=(String) jsonObject.get("listId");
					noteId=(String) jsonObject.get("noteId");
					noteType=(String) jsonObject.get("listType");
					status = iNoteDao.deleteAllContactMembersForNote(userId, listId, noteId,noteType);
			}
		} catch (Exception e) {
			logger.error("Exception in Delete All Contact Share For Note method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" deleteAllContactMembersForNote method returned result ");
		return status;
	}

	@Override
	public String deleteAllContactMembersForBook(String params) {
		String status = "";
		String userId="";
		String listId="";
		String noteType="";
		if(logger.isDebugEnabled())
			logger.debug(" deleteAllContactMembersForBook method called");
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					userId=(String) jsonObject.get("userId");
					listId=(String) jsonObject.get("listId");
					noteType=(String) jsonObject.get("listType");
					status = iNoteDao.deleteAllContactMembersForBook(userId, listId,noteType);
			}
		} catch (Exception e) {
			logger.error("Exception in Insert All Contact Members Share For Book method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" deleteAllContactMembersForBook method returned result ");
		return status;
	}

	@Override
	public String enabledisableBook(String params) {
		String status = "";
		String userId="";
		String listId="";
		if(logger.isDebugEnabled())
			logger.debug(" enabledisableBook method called");
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					userId=(String) jsonObject.get("userId");
					listId=(String) jsonObject.get("listId");
					status = iNoteDao.bookenableDisable(userId, listId);
			}
		} catch (Exception e) {
			logger.error("Exception in Book enable and disable method  : ", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" enabledisableBook method returned result ");
		return status;
	}

	@Override
	public String getFeatureNotes(String params)
	{
		String status="";
		String userId="";
		JSONObject jsonObject=null;
		if(logger.isDebugEnabled())
			logger.debug(" getFeatureNotes method called");
		try{
			jsonObject = new JSONObject(params);
			userId=(String) jsonObject.get("userId");
			List<MnNotesDetails> mnNotesDetails = iNoteDao.getFeatureNotes(userId);
			if(mnNotesDetails!=null && !mnNotesDetails.isEmpty()){
				status=convertMnVoteCountToJson(mnNotesDetails);
			}
			
		}catch (Exception e) {
			logger.error("Exception while get features Note ",e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" getFeatureNotes method returned result ");
		return status;	
	
	}

	@Override
	public String getCommentsCountBasedOnNote(String params)
	{
		String status="";
		JSONObject jsonObject=null;
		if(logger.isDebugEnabled())
			logger.debug(" getCommentsCountBasedOnNote method called");
		try{
			jsonObject = new JSONObject(params);
			
			status = iNoteDao.getCommentsCountBasedOnNote((String) jsonObject.get("listId"),(String) jsonObject.get("noteId"),(String) jsonObject.get("userId"));
			
		}catch (Exception e) {
			logger.error("Exception in get MNCommnets Count ",e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" getCommentsCountBasedOnNote method returned result ");
		return status;
	}

	@Override
	public String copyPublicNote(String params)
	{
		String status = "";
		String listId="";
		String noteId="";
		String userId="";
		if(logger.isDebugEnabled())
			logger.debug(" copyPublicNote method called");
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					listId=(String) jsonObject.get("listId");
					noteId=(String) jsonObject.get("noteId");
					userId=(String) jsonObject.get("userId");
					
					status = iNoteDao.copyPublicNote(listId, noteId, userId);
			}
		} catch (Exception e) {
			logger.error("Exception in copy Public Note method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" copyPublicNote method returned result ");
		return status;
	}

	@Override
	public String changePublicShareWarnMsgFlag(String params, String userId)
	{
		String status = "";
		if(logger.isDebugEnabled())
			logger.debug(" changePublicShareWarnMsgFlag method called "+userId);
		try
		{
			status=iNoteDao.changePublicShareWarnMsgFlag(userId);
		}
		catch (Exception e) {
			logger.error(" Updated Public Share Warn Msg Flag Method :"+e );
		}
		return status;
	}

	@Override
	public String searchEngine(String searchingNote) {
		String jsonString=null;
		if(logger.isDebugEnabled())
			logger.debug(" searchEngine method called searchingNote: "+searchingNote);
		try {
			JSONObject jsonObject;
			jsonObject = new JSONObject(searchingNote);
			String  searchingNotes= (String) jsonObject.get("Searching");
			jsonString = iNoteDao.searchEngine(searchingNotes);
			}
		 catch (Exception e) {
			logger.error("searchEngine :"+e.getMessage());
		}
			if(logger.isDebugEnabled())
				logger.debug(" searchEngine method returned result ");
		return jsonString;

}

	@Override
	public String getSearch(String searchingNote,String userId) {
		String jsonString=null;
		 List<MnList> mnListList = null;
		 if(logger.isDebugEnabled())
				logger.debug(" getSearch method called "+userId+" searchingNote: "+searchingNote);
		 List<MnNotesDetails> details=new ArrayList<MnNotesDetails>();
		try {
			JSONObject jsonObject;
			jsonObject = new JSONObject(searchingNote);
			String  searchingNotes= (String) jsonObject.get("Searching");
			details = iNoteDao.getSearch(searchingNotes,userId);
			if(details!=null && !details.isEmpty())
			{
				jsonString=convertMnNotesDetailsToJson(details);
			}
			}
		 catch (Exception e) {
			logger.error(" getSearch :"+e.getMessage());
		}
			if(logger.isDebugEnabled())
				logger.debug(" getSearch method returned result ");
		return jsonString;

}

	@Override
	public String getSearchMostViewd(String params,String userId) {
		String searchText = "";
		String noteAccess = "";
		if(logger.isDebugEnabled())
			logger.debug(" getSearchMostViewd method called "+userId);
		List<MnNotesDetails> viewCounts=new ArrayList<MnNotesDetails>();
		
	
		String jsonString = null;
		try {
			if (params != null && !params.equals("")) {
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				searchText = (String) jsonObject.get("Searching");
				noteAccess = (String) jsonObject.get("noteAccess");
				
				viewCounts = iNoteDao.getSearchMnMostViewed(searchText, noteAccess,userId);
				
				if (viewCounts != null && !viewCounts.isEmpty())
				{
					Collections.sort(viewCounts, Collections.reverseOrder());
					jsonString = convertMnVoteCountToJson(viewCounts);
				}
				
			}
		} catch (Exception e) {
			logger.error("getSearchMostViewd :"+e.getMessage());
		}
		return jsonString;
}

	@Override
	public String getSerachMostVoted(String params,String userId) {
		String searchText = "";
		String noteAccess = "";
		if(logger.isDebugEnabled())
			logger.debug(" getSerachMostVoted method called "+userId);
		
		List<MnNotesDetails> viewCounts=new ArrayList<MnNotesDetails>();
		String jsonString = null;
		try {
			if (params != null && !params.equals("")) {
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				searchText = (String) jsonObject.get("Searching");
				noteAccess = (String) jsonObject.get("noteAccess");
				
				viewCounts = iNoteDao.getSearchMnMostVoted(searchText, noteAccess ,userId);
				
				if (viewCounts != null && !viewCounts.isEmpty())
				{
					Collections.sort(viewCounts, Collections.reverseOrder());
					jsonString = convertMnVoteCountToJson(viewCounts);
   
				}
				
			}
		} catch (Exception e) {
			logger.error("getSerachMostVoted :"+e.getMessage());
		}
		return jsonString;
	}

	public String getNoteByKwywords(String params){
		String userId="";
		String listType="";
		String keyword= "";
		List<MnNoteDetails> mnNoteDetails = null;
		List<MnNoteDetails> searchResults = null;
		String multiFilterBaseBookId="";
		String multiFilterBaseTagId="";
		String jsonString = "";
		
		if(logger.isDebugEnabled())
			logger.debug(" getNoteByKwywords method called");
		
		try {
			if (params != null && !params.equals("")) {
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				userId=(String) jsonObject.get("userId");
				listType=(String) jsonObject.get("listType");
				keyword = (String) jsonObject.get("keyword");
				multiFilterBaseBookId = (String) jsonObject.get("listId");
				multiFilterBaseTagId = (String) jsonObject.get("tagId");
				
				if(multiFilterBaseBookId!=null && multiFilterBaseBookId.isEmpty() && multiFilterBaseTagId!=null && multiFilterBaseTagId.isEmpty())
					// both tag && book Id - 0
					mnNoteDetails = fetchAllListBasedOnUser(userId, listType);
					
				else if((multiFilterBaseBookId!=null && !multiFilterBaseBookId.isEmpty()) ||(multiFilterBaseTagId!=null && !multiFilterBaseTagId.isEmpty())){
					if(multiFilterBaseTagId.isEmpty())
						// tag id - 0 && book Id - 1  - now looking
						mnNoteDetails=iNoteDao.getListBasedOnListId(userId, multiFilterBaseBookId, listType);
					else
						// both tag - 1 && book Id - 1
						mnNoteDetails = iNoteDao.fetchSharedNotesBasedTag(userId, listType,multiFilterBaseTagId,keyword,multiFilterBaseBookId);
				}
				if(mnNoteDetails!= null && !mnNoteDetails.isEmpty()){
					searchResults = iNoteDao.byKewordConvertMnListTo(mnNoteDetails,keyword,listType);
					if(searchResults!=null && !searchResults.isEmpty()){
						Collections.sort(searchResults, SENIORITY_ORDER_NOTE);
						jsonString = convertToJsonNoteDetailsToMnList(searchResults);
							
						jsonString=jsonString+"]";
					}
				}
			}
		}catch (Exception e) {
			logger.error("Error in get searching Note by keyword from list",e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" getNoteByKwywords method returned result ");
		return jsonString;
	}
	static final Comparator<MnEventDetails> SENIORITY_ORDER_EVENT_DETAILS = new Comparator<MnEventDetails>() {
		public int compare(MnEventDetails e1, MnEventDetails e2) {
			Date i1=null,i2=null;
			try {
				 i1 = new Date(e1.getStartDate());
				 i2 = new Date(e2.getStartDate());
				
			} catch (Exception e) {
			}
			return i1.compareTo(i2);
		}
	};
	private String convertToJsonEventOfString(List<MnEventDetails> mnEventDetails) {
		StringBuffer jsonStr = new StringBuffer("[");
		if(logger.isDebugEnabled())
			logger.debug(" convertToJsonEventOfString method called");
		try
		{
			if(mnEventDetails!=null && !mnEventDetails.isEmpty())
			{
				for (MnEventDetails list : mnEventDetails)
				{
					jsonStr.append("{\"id\" : \"" + list.getcId() + "\",");
					jsonStr.append("\"title\" : \"" + list.getEventName() + "\",");
					jsonStr.append("\"start\" : \"" + list.getStartDate() + "\",");
					jsonStr.append("\"description\" : \"" + list.getDescription() + "\",");
					jsonStr.append("\"allDay\" : \"" + list.getAlldayevent() + "\",");
					jsonStr.append("\"location\" : \"" + list.getLocation() + "\",");
					jsonStr.append("\"end\" : \"" + list.getEndDate() + "\",");
					jsonStr.append("\"eventId\" : \"" + list.getEventId() + "\",");
					jsonStr.append("\"listId\" : \"" + list.getListId() + "\",");
					jsonStr.append("\"ownerId\" : \"" + list.getUserId()+ "\"},");
					
				}
				jsonStr = jsonStr.deleteCharAt(jsonStr.lastIndexOf(","));
				jsonStr.append("]");
			}
			if(logger.isDebugEnabled())
				logger.debug(" convertToJsonEventOfString method returned result ");
			return jsonStr.toString();
		}
		catch (Exception e) {
			logger.error("Error:"+e);
			return "";
		}
		
	}
	@Override
	public String getBookNames(String params) 
	{
		if(logger.isDebugEnabled())
			logger.debug(" getBookNames method called");
		String status=null;
		String userId="";
		String listType="";
		try{
			if (params != null && !params.equals("")) {
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				userId=(String) jsonObject.get("userId");
				listType=(String) jsonObject.get("listType");
			status = iNoteDao.getBookNames(userId,listType);
			}
		}catch (Exception e) {
			logger.error("Exception in getBookNames method  : ", e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" getBookNames method returned result ");
		return status;
		}
	@Override
	public String createNoteInDefualtList(String mnNote,String listType,String userId,String selectedBookId) {
		String status = "";
		String replacedNoteName="";
		JSONObject jsonObject;
		if(logger.isDebugEnabled())
			logger.debug(" createNoteInDefualtList method called "+userId+" selectedBookId: "+selectedBookId);
		try {
			jsonObject = new JSONObject(mnNote);
			
			if(jsonObject.has("noteName"))
			{
				String noteName=(String) jsonObject.get("noteName");
				String access=(String) jsonObject.get("access");
				
				if(noteName!=null && !noteName.equals("")){
					replacedNoteName = noteName;
					if(replacedNoteName.indexOf("`*`") != -1){
						replacedNoteName = replacedNoteName.replace("`*`", "\\\"");
						mnNote = mnNote.replace("\"noteName\":\""+ noteName + "\"", "\"noteName\":\""+ replacedNoteName + "\"");
					}
					status = iNoteDao.createNoteInDefualtList(mnNote,listType,userId,access,selectedBookId);
					status="{\"noteId\":\""+status+"\"}";
				}
			}else{
				if(jsonObject.has("eventName") && jsonObject.get("eventName")!=null)
				{
					String listName=(String) jsonObject.get("eventName");
				
					if(listName!=null && !listName.equals("")){
						if(mnNote.indexOf("`*`") != -1){
							mnNote = mnNote.replace("`*`", "\\\"");
						}
						status = iNoteDao.createNoteInDefualtList(mnNote,listType,userId,"",selectedBookId);
						status="{\"eventId\":\""+status+"\"}";
					}
				}
			}
		} catch (Exception e) {
			logger.error("Exception in Create Note In Defualt List method  : ", e);
            status="0";
		}
		if(logger.isDebugEnabled())
			logger.debug(" createNoteInDefualtList method returned result ");
		return status;
	}

	@Override
	public String getTagForAutoSearch(String params)
	{
		String result=null;
		String searchText=null;
		String userId;
		if(logger.isDebugEnabled())
			logger.debug(" getTagForAutoSearch method called");
		try {
			JSONObject jsonObject;
			jsonObject = new JSONObject(params);
			searchText=(String) jsonObject.get("Searching");
			userId=(String)jsonObject.get("userId");
			JSONArray array=new JSONArray();
			List<MnTag> tags = iNoteDao.getTags(userId);
			
			if(tags!=null && !tags.isEmpty()){
				for(MnTag tag:tags){
					if(tag.getTagName()!=null && tag.getTagName().toLowerCase().contains(searchText.toLowerCase())){
						array.put(tag.getTagName());
					}
				}
				result=array.toString();
			}
			
		} catch (Exception e) {
			logger.error("Exception in searching Text method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" getTagForAutoSearch method returned result ");
		return result;
	}

	@Override
	public String getAllNoteFilters(String params)
	{
		if(logger.isDebugEnabled())
			logger.debug(" getAllNoteFilters method called");
		String userId="";
		String listType="";
		String keyWord= "";
		org.json.JSONArray result = new org.json.JSONArray();
		List<MnList> mnListList = null;
		String noteName=null;
		boolean searchingFalg=false;
		try {
			if (params != null && !params.equals("")) {
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				userId=(String) jsonObject.get("userId");
				listType=(String) jsonObject.get("listType");
				keyWord = (String) jsonObject.get("keyword");
				
					mnListList = null;//fetchAllListBasedOnUser(userId, listType);
				
				if(mnListList!= null && !mnListList.isEmpty()){
					for(MnList list:mnListList)
					{
						for(String str:list.getMnNotesDetails())
						{
							if(str!=null && !str.isEmpty()){
								searchingFalg=false;
								jsonObject = new JSONObject(str);
								
									noteName=(String) jsonObject.get("noteName");
							
								if(keyWord!=null && (noteName.toLowerCase().trim().contains(keyWord.toLowerCase())))
								{
									searchingFalg=true;
								}
								if(searchingFalg)
								{
									result.put(noteName);
								}
							}
						}
					}
				}
			}
		}catch (Exception e) {
			logger.error("Error in get searching Note by keyword from list",e);
		}
		if(logger.isDebugEnabled())
			logger.debug(" getAllNoteFilters method returned result ");
		return result.toString();
	}

	@Override
	public String updateBookForNote(String params)
	{
		if(logger.isDebugEnabled())
			logger.debug(" updateBookForNote method called");
		String status = "";
		String bookId="";
		String noteId="";
		String listId="";
		String userId="";
		String listType="";
		try {
			if (params != null && !params.equals("")) {
					JSONObject jsonObject;
					jsonObject = new JSONObject(params);
					bookId=(String) jsonObject.get("bookId");
					listId=(String) jsonObject.get("listId");
					noteId=(String) jsonObject.get("noteId");
					userId=(String) jsonObject.get("userId");
					listType=(String) jsonObject.get("listType");
					status = iNoteDao.updateBookForNote(bookId, listId, noteId,userId,listType);
			}
		} catch (Exception e) {
			logger.error("Exception In Book Select and UnSelect For Particular Note method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" updateBookForNote method returned result ");
		return status;
	}

	@Override
	public String getAllTags()
	{
		if(logger.isDebugEnabled())
			logger.debug(" getAllTags method called");
		String status = "";
		String userId="";
		try {
					List<MnTag> tags = iNoteDao.getAllTags();
					if(tags!=null && !tags.isEmpty()){
						status=convertToJsonTagList(tags);
					
			}
		} catch (Exception e) {
			logger.error("Exception in getAllTags method  : ", e);

		}
		if(logger.isDebugEnabled())
			logger.debug(" getAllTags method returned result ");
		return status;
	}

	@Override
	public String getSharedNotes(String params) {
		String jsonString=null;
		String userId="";
		String listType="";
		if(logger.isDebugEnabled())
			logger.debug(" getSharedNotes method called");
		try
		{
			if (params != null && !params.equals("")) {
				JSONObject jsonObject;
				jsonObject = new JSONObject(params);
				userId=(String) jsonObject.get("userId");
				listType=(String) jsonObject.get("listType");
			
			List<MnNotesSharingDetails> mnNotesSharingDetails=iNoteDao.fetchSharedNotes(userId, listType);
			if(mnNotesSharingDetails !=null && !mnNotesSharingDetails.isEmpty())
			{
				jsonString=convertToJsonShare(mnNotesSharingDetails);
			}
			}
		}catch(Exception e){
			logger.error("getSharedNotes :"+e.getMessage());
		}
		if(logger.isDebugEnabled())
			logger.debug(" getSharedNotes method returned result ");
		return jsonString;
	}

	@Override
	public String createBook(MnList mnList) {
	String status = "";
	MnList list=null;
	if(logger.isDebugEnabled())
		logger.debug(" createBook method called");
	try {
		mnList.setDefaultNote(false);
		if(mnList.getListType().equalsIgnoreCase("schedule"))
		{
			mnList.setEnableDisableFlag(true);
		}
		else
		{
			mnList.setEnableDisableFlag(false);
		}
		
		mnList.setMnNotesDetails(new HashSet<String>());
		mnList.setSharedIndividuals(new ArrayList<Integer>());
		mnList.setSharedGroups(new ArrayList<Integer>());
		mnList.setShareAllContactFlag(false);
		list=iNoteDao.createBook(mnList);
		if(list!=null){
			status=convertToJsonListObject(list);
		}else{
			status="0";
		}
	} catch (Exception e) {
		logger.error("Exception in createBook  method  in MnNote: "+ e.getMessage());

	}
	if(logger.isDebugEnabled())
		logger.debug(" createBook method returned result ");
	return status;
	}
	
	public String changedNoteNameWithDouble(String noteName){
		if(noteName.indexOf("\"")!= -1){
			noteName = noteName.replace("\"", "\\\"");
		}
		return noteName;
	}
	
	private  boolean urlCheck(String paramString3){
		 try
		    {
		      URL con = new URL(paramString3);
		      return true; 
		    }
		    catch (MalformedURLException e)
		    {
		      return false; 
		    }
	}
	private  boolean isURLValid(String url) {
		if ((url != null) && (!url.isEmpty())) {
			String urlTrimmed = url.trim();
			if (!urlTrimmed.contains(" ")) {
				String expression = "((http|https)(:\\/\\/))?([a-zA-Z0-9]+[.]{1}){2}[a-zA-z0-9]+(\\/{1}[a-zA-Z0-9]+)*\\/?";
				
				CharSequence inputStr = urlTrimmed;
				
				Pattern pattern = Pattern.compile(expression);
				Matcher matcher = pattern.matcher(inputStr);
				if (matcher.matches()) {
					return true;
				} else {
					expression ="[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*";
					pattern = Pattern.compile(expression);
					matcher = pattern.matcher(inputStr);
					if (matcher.matches()){
						return true;
					}else{
						expression ="^(http|https)\\://([a-zA-Z0-9\\.\\-]+(\\:[a-zA-Z0-9\\.&amp;%\\$\\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|localhost|([a-zA-Z0-9\\-]+\\.)*[a-zA-Z0-9\\-]+\\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\\:[0-9]+)*(/($|[a-zA-Z0-9\\.\\,\\?\\'\\\\+&amp;%\\$#\\=~_\\-]+))*$";
						pattern = Pattern.compile(expression);
						matcher = pattern.matcher(inputStr);
						if (matcher.matches()){
							return true;
						}else{
							expression ="(www)+\\.+[a-z]+\\.+[a-z]+";
							pattern = Pattern.compile(expression);
							matcher = pattern.matcher(inputStr);
							if (matcher.matches()){
								return true;
							}else{
								expression ="^((https?|ftp)://|(www|ftp)\\.)[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";;
								pattern = Pattern.compile(expression);
								matcher = pattern.matcher(inputStr);
								if (matcher.matches())
									return true;
								else
									return false;
							}
						}
					}
					
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public MnComplaints MnComplaints(String params) {
		MnComplaints report = new MnComplaints();
		if(logger.isDebugEnabled())
			logger.debug("MnComplaints method called:");
		String status = "";
		String ownerUserName="";
		String sharedByUserName="";
		String ownerMailId="";
		List<String>commentList=new ArrayList<String>();
		try
		{
			if (params != null && !params.equals(""))
			{
				JSONObject Jobj=new JSONObject(params);
				report.setComplaintUserId(Integer.parseInt(Jobj.getString("userId")));
				report.setUserComplaints((String) Jobj.get("userComplaint"));
				report.setNoteName((String) Jobj.get("noteName"));
				report.setOwnerId((String) Jobj.get("noteUserId"));
				report.setListId((String) Jobj.get("noteListId"));
				report.setNoteId((String) Jobj.get("noteNoteId"));
				report.setCommentId((String) Jobj.get("commentId"));
				report.setReportLevel((String) Jobj.getString("reportLevel"));
				report.setReportPage((String) Jobj.getString("reportpage"));
				ownerUserName=iNoteDao.getUserName((String) Jobj.get("noteUserId"));
				sharedByUserName=iNoteDao.getSharedByUserName((String) Jobj.get("noteUserId"),(String) Jobj.get("noteListId"),(String) Jobj.get("noteNoteId"));
				ownerMailId=iNoteDao.getUserMailId((String) Jobj.get("noteUserId"));
				//commentList=iNoteDao.getCommentsListForMail((String) Jobj.get("noteListId"),(String) Jobj.get("noteNoteId"),(String) Jobj.get("noteUserId"),(String) Jobj.getString("reportLevel"),(String) Jobj.get("commentId"));
				report.setOwnerUserName(ownerUserName);
				report.setOwnerMailId(ownerMailId);
				report.setCommentsList(commentList);
				report.setSharedByUserName(sharedByUserName);
				report.setAdminAction("-");
				report.setAdminComment("-");
				report.setMailTemplateId("-");
				status= iNoteDao.complaintReportMail(report);
				if(status!= null && status.equals("success")){
					return report;
				}
		
			}

		}catch(Exception e){
			logger.error("Exception in MnComplaints method :" + e);
		}
		return report;
		
	}
	
}